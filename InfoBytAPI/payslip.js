var fs = require('fs');
const path = require('path');
var pdf = require('html-pdf');
var format = require('format-number')
var dateFormat = require('dateformat');
var mailer = require('nodemailer');
var pdfoptions = {

  // Papersize Options: http://phantomjs.org/api/webpage/property/paper-size.html
  "height": "10.5in",        // allowed units: mm, cm, in, px
  "width": "8in",            // allowed units: mm, cm, in, px
  "format": "Letter",        // allowed units: A3, A4, A5, Legal, Letter, Tabloid
  "orientation": "portrait", // portrait or landscape

  "border": {
    "top": "0.5in",            // default is 0, units: mm, cm, in, px
    "right": "0.5in",
    "bottom": "0.5in",
    "left": "0.5in"
  },

};
var config = require(__dirname + '/init_app');
var milliseconds = Date.now();
var attendanceString = "<b>DAYS WERE YOU DON'T HAVE FULL ATTENDANCE INCLUDING WEEKOFFS DURING THE MONTH ARE MENTIONED BELOW ↓</b>";
var file;
var toMail;
var backSLash = '\\'
var string = "<html lang='en'><head><meta charset='UTF-8'><link rel='stylesheet' type='text/css' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' integrity='sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm' /><h2>Pay Slip </h2><br><br><table class='table'><thead><div class='row'><div class='col-md-3'>Attendance Date</div><div class='col-md-3'>In Time</div><div class='col-md-3'>Out Time</div><div class='col-md-3'>Status</div></div>"
const ABSPATH = path.dirname(process.mainModule.filename);
var dir = 'public/' + 'PaySlip';
const dbc = require('./db.js');
const db = dbc.db;
//const pgp = db.$config.pgp;
var userData;
var userPaySlipData;
var wd = 0;
const transporter = mailer.createTransport({ // Use an app specific password here
  service: config.mailServer,
  auth: {
    user: config.email,
    pass: config.password
  }
});
module.exports = function sendPaySlip(userId, month, Usersession, callback) {
  dir = 'public/' + 'PaySlip';
  dir += '/' + userId;
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
  var ym = month;
  console.log('ym')
  console.log(ym)
  ym = ym + '';
  var y = ym.substring(0, 4);
  var m = ym.substring(5, 7);
  console.log(y + '     ' + m);
  var mindate, maxdate;
  var sessionparam = Usersession;
  file = "<html><head><link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' integrity='sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm' crossorigin='anonymous'>" +
    "<style>boby{text-align:center;padding:5px;font-size:8px;}.container{padding:0;width:600px;}div{text-align:left;}.row{width:600px;border-bottom: 0px solid #000;border-right: 0px solid #000;}.col-md-3{width:150px;height:20px;border-left: 0px solid #000;}</style></head><body><br/><br/>";
  db.any('select * from fn_viewpayslipofuser($1)', [userId]).then((data) => {
    userData = data;
    try {
      var db2 = dbc.db;
      db2.any("select * from paydetailsview p join usermapping u on p.userid=cast(u.appuserid as int) where p.userid=$1 and p.month=$2", [userId, ym]).then((paydetails) => {
        userPaySlipData = paydetails;
        wd = parseInt(userPaySlipData[0].absent) + parseInt(userPaySlipData[0].present);
        db2.any('select * from salaryallowancestable where patternId=(select splallowances from salary where staffid=(select id from staff where userid=$1))', [userId]).then((allowancesList) => {
          db2.any('select * from extraallowances where userid=$1', [userId]).then((extraAllowancesList) => {
            db2.any('select * from tdsreports where userid=$1 and month=$2', [userId, month]).then((tdsList) => {
              db2.any('select * from payroll where $1 between startrange and endrange', paydetails[0].salary).then((payrollList) => {
                db2.any("select * from salarypay where usrcode=(select deviceuserid from usermapping where appuserid=$1) and to_char(attendancedate, 'YYYY-MM')=$2 and (approvedstatus='H' or approvedstatus='A' or approvedstatus='WO')", [userId + '', month]).then((attendanceList1) => {
                  for (var z = 0; z < attendanceList1.length; z++)
                    string = string + "<div class='row' style='margin-top: 10px;'><div class='col-md-3'>" + dateFormat(attendanceList1[z].attendancedate, 'dd,mmmm,yyyy') + "</div><div class='col-md-3'>" + dateFormat(attendanceList1[z].intime, 'h:MM:ss') + "</div><div class='col-md-3'>" + dateFormat(attendanceList1[z].outtime, 'h:MM:ss') + "</div><div class='col-md-3'>" + attendanceList1[z].approvedstatus + "</div></div>"
                  maxdate = dateFormat(userData[0].month, "mmmm , yyyy");
                  toMail = userData[0].email;
                  file += "<div class='text-center' style='width:600px;'>" +
                    "<img src='" + config.api + '/Misc/images/slogo/' + userData[0].organizationid + "' style='height:100px;width:100px'/>\n" +
                    "<h2 style='display:inline;'>" + userData[0].organizationname + "</h2>\n" +
                    "<div class='container' background='" + config.server + "/Misc/images/slogo/" + userData[0].organizationid + ".png' style='margin: auto 50px;'>\n" +
                    " <div class='row' style='margin-top: 10px;border-top: 1px solid #000;border-left: 0px solid #000;'>\n" +
                    "   <div class='col-md-6' style='width:300px;'><b>Salary Slip</b></div>\n" +
                    "   <div class='col-md-6' style='width:300px;'><b>Month : " + dateFormat(userData[0].month, "mmmm , yyyy") + "</b></div>\n<div></div>" +
                    " </div>\n" +
                    
                    " <div class='row' style='margin-top: 10px;border-top: 1px solid #000;border-left: 0px solid #000;'>\n" +
                    "   <div class='col-md-3'><b>Name:</b> </div>\n" +
                    "   <div class='col-md-3'>" + userData[0].firstname + ' ' + userData[0].lastname + "</div>\n" +
                    "   <div class='col-md-4' style='width:200px;border-left:0px solid #000;'><b>Working Days:</b></div>\n" +
                    "   <div class='col-md-2' style='width:100px;border-left:0px solid #000;'>" + wd + "</div>\n" +
                    " </div>\n" +
                    " <div class='row' style='margin-top: 10px;'>\n" +
                    "   <div class='col-md-3'><b>FacultyID:</b></div>\n" +
                    "   <div class='col-md-3'>" + userPaySlipData[0].deviceuserid + "</div>\n" +
                    "   <div class='col-md-4' style='width:200px;border-left:0px solid #000;'><b>Public Holidays:</b></div>\n" +
                    "   <div class='col-md-2' style='width:100px;border-left:0px solid #000;'>" + userPaySlipData[0].weekoff + "</div>\n" +
                    " </div>\n" +
                    " <div class='row' style='margin-top: 10px;'>\n" +
                    "  <div class='col-md-3'><b>Designation: </b></div>\n" +
                    "  <div class='col-md-3'>" + userData[0].rolename + "</div>\n" +
                    "  <div class='col-md-4' style='width:200px;border-left:0px solid #000;'><b>Days Present:</b></div>\n" +
                    "  <div class='col-md-2' style='width:100px;border-left:0px solid #000;'>" + userPaySlipData[0].present + "</div>\n" +
                    " </div>\n" +
                    " <div class='row' style='margin-top: 10px;'>\n" +
                    "    <div class='col-md-3'><b>DOJ:</b></div>\n" +
                    "    <div class='col-md-6'  style='width:200px;border-left:0px solid #000'>" + dateFormat(userData[0].doj, "mmmm dS, yyyy") + "</div>\n" +
                    " </div>\n" +
                    " <div class='row' style='margin-top: 10px;'>\n" +
                    "    <div class='col-md-3'><b>PAN Number</b></div>\n" +
                    "    <div class='col-md-3'>" + userPaySlipData[0].pannumber + "</div>\n" +
                    " </div>\n" +
                    " <div class='row' style='margin-top: 10px;'>\n" +
                    "   <div class='col-md-6' style='border-left:0px solid #000'><b>Payment Details:</b></div>\n" +
                    " </div>\n" +
                    " <div class='row' style='margin-top: 10px;'>\n" +
                    "   <div class='col-md-3'><b>PF A/C No:</b></div>\n" +
                    "   <div class='col-md-3'>" + userPaySlipData[0].pfnumber + "</div>\n" +
                    "   <div class='col-md-3'><b>UAN No:</b></div>\n" +
                    "   <div class='col-md-3'></div>\n" +
                    " </div>\n" +
                    " <div class='row' style='margin-top: 10px;'>\n" +
                    "   <div class='col-md-3'><b>A/C No:</b></div>\n" +
                    "   <div class='col-md-3'>" + userPaySlipData[0].accountnumber + "</div>\n" +
                    "   <div class='col-md-3'><b>Bank</b></div>\n" +
                    "   <div class='col-md-3'>" + userPaySlipData[0].bankname + "</div>\n" +
                    " </div>\n" +
                    " <div class='row' style='margin-top: 10px;border-top: 1px solid #000;border-left: 0px solid #000;'>\n" +
                    "  <div class='col-md-3'><b>EMOLUMENTS</b></div>\n" +
                    "  <div class='col-md-3'><b>Amount </b></div>\n" +
                    "  <div class='col-md-3'><b>DEDUCTIONS</b></div>\n" +
                    "  <div class='col-md-3'><b>Amount </b></div>\n" +
                    " </div>\n" +
                    "<div class='row' style='margin-top: 10px;border-top: 1px solid #000;border-left: 0px solid #000;'></div>"+
                    "\n<div class='row' style='margin-top: 10px;'><div class='col-md-6' style='width:300px;border-left: 0px solid #000;'>";
                  allowancesList.forEach(element => {
                    if (element.type == 'Allowance') {
                      file += "<div class='row' style='margin-top: 10px;width:300px;'><div class='col-md-6' style='width:150px;;border-right: 0px solid #000;'><b>" + element.allowancestype + "</b></div>" +
                        "<div class='col-md-6' style='width:150px;;'><i>" + format({
                          prefix: '',
                          suffix: ''
                        })(Math.round(((element.percentage * userData[0].salary) / 100) * 100) / 100) + "</i></div>\n";
                      file += " </div>\n";
                    }
                  });
                  extraAllowancesList.forEach(element => {
                    if (element.type == 'Allowance') {
                      file += "<div class='row' style='margin-top: 10px;width:300px;'><div class='col-md-6' style='width:150px;;border-right: 0px solid #000;'><b>" + element.allowancename + "</b></div>" +
                        "<div class='col-md-6' style='width:150px;;'><i>" + format({
                          prefix: '',
                          suffix: ''
                        })(Math.round((element.amount) * 100) / 100) + "</i></div></div>\n";
                    }
                  });
                  file += "</div><div class='col-md-6' style='width:300px;'>\n";
                  allowancesList.forEach(element => {
                    if (element.type == 'Deduction') {
                      file += "<div class='row' style='margin-top: 10px;width:300px;'><div class='col-md-6' style='width:150px;;'><b>" + element.allowancestype + "</b></div>" +
                        "<div class='col-md-6' style='width:150px;;border-left: 0px solid #000;'><i>" + format({
                          prefix: '',
                          suffix: ''
                        })(Math.round(((element.percentage * userData[0].salary) / 100) * 100) / 100) + "</i></div>\n";
                      file += " </div>\n";
                    }
                  });
                  extraAllowancesList.forEach(element => {
                    if (element.type == 'Deduction') {
                      file += "<div class='row' style='margin-top: 10px;width:300px;'><div class='col-md-6' style='width:150px;;'><b>" + element.allowancename + "</b></div>" +
                        "<div class='col-md-6' style='width:150px;;border-left: 0px solid #000;'><i>" + format({
                          prefix: '',
                          suffix: ''
                        })(Math.round((element.amount) * 100) / 100) + "</i></div>\n";
                      file += " </div>\n";
                    }
                  });
                  payrollList.forEach(element => {
                    file += "<div class='row' style='margin-top: 10px;width:300px;'><div class='col-md-6' style='width:150px;;'><b>" + element.name + "</b></div>" +
                      "<div class='col-md-6' style='width:150px;;border-left: 0px solid #000;'><i>" + format({
                        prefix: '',
                        suffix: ''
                      })(Math.round((element.amount) * 100) / 100) + "</i></div>\n";
                    file += " </div>\n";
                  });


                  file += "<div class='row' style='margin-top: 10px;width:300px;'><div class='col-md-6' style='width:150px;;'><b>" + 'TDS' + "</b></div>" +
                    "<div class='col-md-6' style='width:150px;;border-left: 0px solid #000;'><i>" + format({
                      prefix: '',
                      suffix: ''
                    })(Math.round((tdsList[0].taxamount) * 100) / 100) + "</i></div></div>\n";
                  "</div>\n";

                  file +=
                  
                    " </div></div><div class='row' style='margin-top: 10px;border-top: 1px solid #000;border-left: 0px solid #000;'>\n" +
                    "   <div class='col-md-3'><b>Gross Pay</b></div>\n" +
                    "   <div class='col-md-3'>Rs." + format({
                      prefix: '',
                      suffix: ''
                    })(Math.round(userData[0].salary * 100) / 100) + "</div>\n" +
                    "   <div class='col-md-3'><b>Net Pay</b></div>\n" +
                    "   <div class='col-md-3'> Rs." + format({
                      prefix: '',
                      suffix: ''
                    })(Math.round(userData[0].grosssalary * 100) / 100) + "</div>\n" +
                    " </div>\n" +
                    "<div class='row' style='margin-top: 10px;border-top: 1px solid #000;border-left: 0px solid #000;'>"+
                    "<div class='col-md-3'><b>Checked By</b></div>"+                  
                    "<div class='col-md-3'><b>Authorized By</b></div>"+
                    "<div class='col-md-3'></div><div class='col-md-3'><b>Received By</b></div></div>"+
                    "<div class='row' style='margin-top: 10px;border-top: 1px solid #000;border-left: 0px solid #000;'></div>"+
                  "</div>"+
                    "</div>" +
                    "</div></body></html>";
                  fs.writeFile(dir + '/' + month + '.html', file, function (err) {
                    var html = fs.readFileSync('./public/payslip/' + userId + '/' + month + '.html', 'utf8');
                    pdf.create(html, pdfoptions).toFile('./public/payslip/' + userId + '/' + month + '.pdf', function (err, res) {
                      if (err) return console.log(err);
                      else {
                        console.log('pdf is created successfully.');
                        const options = {
                          from: config.email,
                          to: toMail,
                          subject: 'Payslip ' + maxdate,
                          dsn: {
                            id: milliseconds,
                            return: 'headers',
                            notify: ['failure', 'delay', 'success'],
                            recipient: config.email
                          },
                          html: '<b>Please find your payslip for the month of ' + maxdate + '</b><br><br><br>' + attendanceString + '<br><br><br>' + string,
                          attachments: [{
                            path: __dirname + '/public/payslip/' + userId + '/' + month + '.pdf'
                          }]
                        };
                        transporter.sendMail(options, (error, info) => {
                          if (error) {
                            console.log('unable to send mail' + error);
                            callback(false);
                          } else {
                            console.log('sent ' + JSON.stringify(info));
                            callback(true);
                          }
                        });
                      }
                    });
                  });
                })
              })
            })
          })
        })
      })
    } catch (e) {
      console.log(e);
      userData = 'No Records found';
      console.log(userData);
    }
    console.log(data);
  })
}
//sendPaySlip(5);