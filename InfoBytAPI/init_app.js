var config = {
    "name": "InfoByt",
    "db": "Infobyt",
    "api": "http://183.82.111.183/node/InfoBytAPIV4",
    "angular": "http://www.infobyt.com",
    "dba": "postgres://postgres:ib1928@183.82.111.183:5432/Infobyt",
    "dbap":"postgres://postgres:ib1928@183.82.111.183:5432/Infobyt",
    "dbad":"postgres://postgres:ib1928@183.82.111.183:5432/Infobyt",
    "mailServer": "gmail",
    "email": "support@infobyt.com",
    "password": "infobyt@7",
    "faicon": [
        {
            "module": "app",
            "icon": "fa fa-sitemap"
        },
        {
            "module": "organization",
            "icon": "fa fa-sitemap"
        },
        {
            "module": "batch",
            "icon": "fa fa-group"
        },
        {
            "module": "appraisal",
            "icon": "fa fa-file-text"
        },
        {
            "module": "fee",
            "icon": "fa fa-money"
        },
        {
            "module": "salary",
            "icon": "fa fa-credit-card"
        },
        {
            "module": "transport",
            "icon": "fa fa-bus"
        },
        {
            "module": "attendance",
            "icon": "fa fa-clock-o"
        },
        {
            "module": "attendance",
            "icon": "fa fa-clock-o"
        },
        {
            "module": "career",
            "icon": "fa fa-graduation-cap"
        },
        {
            "module": "dashboard",
            "icon": "fa fa-group"
        },
        {
            "module": "login",
            "icon": "fa fa-sign-in"
        },
        {
            "module": "services",
            "icon": "fa fa-group"
        },
        {
            "module": "settings",
            "icon": "fa fa-ellipsis-v fa-2x"
        },
        {
            "module": "user",
            "icon": "fa fa-group"
        }
    ]
}
module.exports = config;