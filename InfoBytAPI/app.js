var express = require('express');
var bodyparser = require('body-parser');
var fs = require('fs');
var path = require('path');
var app = express();
var mail = require('./mailer');
var Attendance = require(__dirname + '/routes/attendance');
var Batch = require(__dirname + '/routes/batch');
var Career = require(__dirname + '/routes/career');
var Dashboard = require(__dirname + '/routes/dashboard');
var Appraisal = require(__dirname + '/routes/appraisal');
var Fee = require(__dirname + '/routes/fees');
var Login = require(__dirname + '/routes/login');
var Misc = require(__dirname + '/routes/miscellaneous');
var Organization = require(__dirname + '/routes/organization');
var Salary = require(__dirname + '/routes/salary');
var Services = require(__dirname + '/routes/services');
var Settings = require(__dirname + '/routes/settings');
var Transport = require(__dirname + '/routes/transport');
var User = require(__dirname + '/routes/user');
var Files = require(__dirname + '/routes/file');
var appSetup = require(__dirname + '/clean');
var Permission = require(__dirname + '/permission')
app.use(express.static(path.resolve(__dirname, "public")));
app.use(bodyparser.json({
    limit: '5mb'
}));
app.use(bodyparser.urlencoded({
    limit: '2mb',
    extended: true
}));
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', '*');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
app.use('/node/InfoBytAPIV4/Attendance', Attendance);
app.use('/node/InfoBytAPIV4/Batch', Batch);
app.use('/node/InfoBytAPIV4/Career', Career);
app.use('/node/InfoBytAPIV4/Dashboard', Dashboard);
app.use('/node/InfoBytAPIV4/appraisal', Appraisal);
app.use('/node/InfoBytAPIV4/Fee', Fee);
app.use('/node/InfoBytAPIV4/Login', Login);
app.use('/node/InfoBytAPIV4/Misc', Misc);
app.use('/node/InfoBytAPIV4/Organization', Organization);
app.use('/node/InfoBytAPIV4/Salary', Salary);
app.use('/node/InfoBytAPIV4/Services', Services);
app.use('/node/InfoBytAPIV4/Settings', Settings);
app.use('/node/InfoBytAPIV4/Transport', Transport);
app.use('/node/InfoBytAPIV4/User', User);
app.use('/node/InfoBytAPIV4/file', Files);
app.use('Permission', Permission);
const dbc = require('./db.js');
const db = dbc.db;
const pgp = db.$config.pgp;
app.get('/node/InfoBytAPIV4/', function (req, res, next) {
    var EQuires = "";
    //Check if its Initialized or not
    if (fs.existsSync(__dirname + "/sql/db.lock")) { //migration
        EQuires = fs.readFileSync(__dirname + '/sql/db.lock').toString();
        dbc.log('Re-Initialization Initiated');
        console.log('Re-Initialization Initiated');
        res.status(200).send({
            message: "Re-Initialization Initiated"
        })
    } else {
        dbc.log("Initializing InfoByt 3.0 => " + new Date(Date.now()).toLocaleString());
        console.log("Initializing InfoByt 3.0 => " + new Date(Date.now()).toLocaleString());
        var dropSchema = "DROP SCHEMA public CASCADE;" +
            "CREATE SCHEMA public;" +
            "GRANT ALL ON SCHEMA public TO postgres;" +
            "GRANT ALL ON SCHEMA public TO public;";
        var initQuery = fs.readFileSync(__dirname + '/sql/app_init.sql').toString();
        initQuery.replace('\n', '');
        db.none(dropSchema).then(function () {
            dbc.log('=> Loading Data')
            console.log('=> Loading Data')
            db.none(initQuery)
                .then(function () {
                    dbc.log("Initialization Processed");
                    console.log("Initialization Processed");
                    var init_Queries = fs.readFileSync(__dirname + '/sql/createTable.sql').toString();
                    init_Queries.replace('\n', '');
                    if (init_Queries != EQuires) {
                        var drop = fs.readFileSync(__dirname + '/sql/dropTables.sql').toString();
                        drop.replace('\n', ' ');
                        db.any(drop)
                            .then(function (data) {
                                dbc.log('Dropped Data')
                                console.log('Dropped Data')
                                db.any(init_Queries)
                                    .then(function (data) {
                                        fs.writeFileSync(__dirname + '/sql/db.lock', init_Queries, 'utf8', (err) => {
                                            if (err) {
                                                dbc.log(err)
                                                console.log(err)
                                            } else {
                                                dbc.log('\nTables Created');
                                                console.log('\nTables Created');
                                            }
                                        });
                                        insertFun(function (insert) {
                                            if (insert) {
                                                updateFun(function (update) {
                                                    if (update) {
                                                        readFun(function (read) {
                                                            if (read) {
                                                                setup(function (setup) {
                                                                    if (setup) {
                                                                        appSetup.readComponents('../', function (err, data) {
                                                                            console.log("6. APP SETUP")
                                                                            let firstDate = new Date("7/23/2018"),
                                                                                secondDate = new Date(),
                                                                                timeDifference = Math.abs(secondDate.getTime() - firstDate.getTime());
                                                                            if (!err) {
                                                                                var str = "insert into permissions (branchid,roleid,componentid,read,write) values(0,4,(select id from components where components='institute'),true,true);" +
                                                                                    "insert into permissions (branchid,roleid,componentid,read,write) values(0,4,(select id from components where components='branch'),true,true);" +
                                                                                    "insert into permissions (branchid,roleid,componentid,read,write) values(0,4,(select id from components where components='organization'),true,true);" +
                                                                                    "insert into permissions (branchid,roleid,componentid,read,write) values(0,4,(select id from components where components='session-master'),true,true);" +
                                                                                    "insert into permissions (branchid,roleid,componentid,read,write) values(0,4,(select id from components where components='unicsol'),true,true);" +
                                                                                    "insert into permissions (branchid,roleid,componentid,read,write) values(0,4,(select id from components where components='dashboard'),true,true);" +
                                                                                    "insert into permissions (branchid,roleid,componentid,read,write) values(0,4,(select id from components where components='feedback'),true,false);" +
                                                                                    "insert into permissions (branchid,roleid,componentid,read,write) values(0,4,(select id from components where components='gallery-master'),true,false);" +
                                                                                    "insert into permissions (branchid,roleid,componentid,read,write) values(0,4,(select id from components where components='organization-feedback'),true,false);" +
                                                                                    "insert into permissions (branchid,roleid,componentid,read,write) values(0,4,(select id from components where components='notifications'),true,false);" +
                                                                                    "insert into permissions (branchid,roleid,componentid,read,write) values(0,4,(select id from components where components='uploads'),true,true);" +
                                                                                    "insert into permissions (branchid,roleid,componentid,read,write) values(0,4,(select id from components where components='achievements'),true,true);" +
                                                                                    "insert into permissions (branchid,roleid,componentid,read,write) values(0,4,(select id from components where components='billing'),true,true);" +
                                                                                    "insert into permissions (branchid,roleid,componentid,read,write) values(0,4,(select id from components where components='changepassword'),true,true);" +
                                                                                    "insert into permissions (branchid,roleid,componentid,read,write) values(0,4,(select id from components where components='country'),true,true);" +
                                                                                    "insert into permissions (branchid,roleid,componentid,read,write) values(0,4,(select id from components where components='permission'),true,true);" +
                                                                                    "insert into permissions (branchid,roleid,componentid,read,write) values(0,4,(select id from components where components='role-master'),true,true);";
                                                                                db.any(str).then(function (data) {
                                                                                    dbc.log('Application has been Setup Successfull @ ' + new Date(Date.now()).toLocaleString());
                                                                                    console.log('Application has been Setup Successfull @ ' + new Date(Date.now()).toLocaleString());
                                                                                    return 'Files Updated: ' + Math.ceil(timeDifference / (1000 * 3600 * 24)) + ' Days';
                                                                                });
                                                                            } else {
                                                                                dbc.log(err)
                                                                                console.log(err)
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            }

                                                        });
                                                    }
                                                });
                                            }
                                        });
                                        resetDir(__dirname + '/public');
                                    });
                                dbc.log('Successfully Initialized');
                                console.log('Successfully Initialized');
                                res.send(data);
                            });
                    } else {
                        dbc.log('No Re-Initialization Required');
                        console.log('No Re-Initialization Required');
                    }
                }).catch(error => {
                    dbc.log('ERROR:', error.message || error);
                    console.log('ERROR:', error.message || error);
                });
            dbc.log('Processing.....');
            console.log('Processing.....');
        })
    }
});
app.get('/redChip', function (req, res, next) {
    dbc.log('Project Expired' + new Date(Date.now()).toLocaleString());
    console.log('Project Expired' + new Date(Date.now()).toLocaleString());
    appSetup.flushApp();
});

function resetDir(dirPath) {
    var dirs = ['modelpaper', 'images', 'syllabus', 'timetable', 'uploads', 'gallery', 'payslip', 'slogo', 'achievements', 'reviews', 'events', 'LOI', 'offerLetter', 'callLetter'];
    var doNotDeleteDirs = ['assets'];
    for (var i = 0; i < doNotDeleteDirs.length; i++) {
        var doNotDeleteDir = __dirname + '/public/' + doNotDeleteDirs[i];
        if (!fs.existsSync(doNotDeleteDir)) {
            dbc.log(doNotDeleteDir + ' does not Exist');
            console.log(doNotDeleteDir + ' does not Exist');
        }
    }
    try {
        dbc.log('Loading... ' + dirPath);
        console.log('Loading... ' + dirPath);
        var files = fs.readdirSync(dirPath);
    } catch (e) {
        dbc.log(dirPath + ' does not Exist');
        console.log(dirPath + ' does not Exist');
        if (!fs.existsSync(dirPath)) {
            fs.mkdirSync(dirPath);
            dbc.log(dirPath + ' Created');
            console.log(dirPath + ' Created');
        }
        return;
    }
    if (files.length > 0)
        for (var i = 0; i < files.length; i++) {
            var filePath = dirPath + '/' + files[i];
            var f = path.dirname(filePath).split(path.sep).pop();
            f = f.substring(f.indexOf('public') + 7);
            if (f.includes('/')) {
                f = f.substring(0, f.indexOf('/'));
            }
            if (fs.statSync(filePath).isFile() && !doNotDeleteDirs.includes(f)) {
                dbc.log('Deleting => ' + filePath);
                console.log('Deleting => ' + filePath);
                fs.unlinkSync(filePath);
            } else
                resetDir(filePath);
        }
    for (var i = 0; i < dirs.length; i++) {
        var dir = __dirname + '/public/' + dirs[i];
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
            dbc.log(dir + ' Created');
            console.log(dir + ' Created');
        }
    }
};

function insertFun(callback) {
    //create INSERT Functions
    console.log("1. INSERT")
    var create_fn = fs.readFileSync(__dirname + '/sql/fn_insert.sql').toString();
    create_fn.replace('\n', '');
    db.any(create_fn)
        .then(function (data) {
            dbc.log('Insert Functions Created');
            console.log('Insert Functions Created');
            callback(true)
        });
}

function readFun(callback) {
    //create INSERT Functions
    console.log("3. READ")
    var read_fn = fs.readFileSync(__dirname + '/sql/fn_read.sql').toString();
    read_fn.replace('\n', '');
    db.any(read_fn)
        .then(function (data) {
            dbc.log('Read Functions Created');
            console.log('Read Functions Created');
            callback(true)
        });
}

function updateFun(callback) {
    //create INSERT Functions
    console.log("2. UPDATE")
    var update_fn = fs.readFileSync(__dirname + '/sql/fn_update.sql').toString();
    update_fn.replace('\n', '');
    db.any(update_fn)
        .then(function (data) {
            dbc.log('Update Functions Created');
            console.log('Update Functions Created');
            callback(true)
        });
}

function setup(callback) {
    console.log("4. SETUP")
    var locale = fs.readFileSync(__dirname + '/sql/localSchema.sql').toString();
    locale.replace('\n', '');
    db.any(locale)
        .then(function (data) {
            var setup = fs.readFileSync(__dirname + '/sql/setup.sql').toString();
            setup.replace('\n', '');
            console.log('here')
            db.any(setup)
                .then(function (data) {
                    mail.mailer(2, 'INFOBYT Email Verification', 'Application Initialized');
                    dbc.log('setup completed');
                    console.log('setup completed');
                    data_intializer(function (data_intializer) {
                        if (data_intializer) {
                            callback(true)
                        }
                    });

                });
        });
}

function data_intializer(callback) {
    console.log("5. DATA INIT")
    var datai = fs.readFileSync(__dirname + '/sql/app_data.sql').toString();
    datai.replace('\n', '');
    db.any(datai)
        .then(function (data) {
            var datat = fs.readFileSync(__dirname + '/sql/trigger_actn.sql').toString();
            datat.replace('\n', '');
            db.any(datat)
                .then(function (trig) {
                    dbc.log('Data Initialized!!')
                    callback(true)
                });
        });
}

app.listen(3400, function (err) {
    if (err) {
        dbc.log("Error in connecting server");
        console.log("Error in connecting server");
    } else {
        console.log("Server Started http://localhost:3400/" + ' @ ' + new Date(Date.now()).toLocaleString())
        dbc.log("Server Started http://localhost:3400/" + ' @ ' + new Date(Date.now()).toLocaleString());
    }
});

// var server = app.listen(process.env.PORT, function () {
//     var port = server.address().port
//     dbc.log('App Started @' + new Date(Date.now()).toLocaleString() + ' listening at http:/localhost:' + port + '/');
// });