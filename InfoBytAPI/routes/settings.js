var express = require('express');
var bodyparser = require('body-parser');
var payslip = require('../payslip')
var fs = require('fs')
var path = require('path')
var mail = require('../mailer');
var settingsRouter = express.Router()
settingsRouter.use(express.static(path.resolve(__dirname, "public")));
settingsRouter.use(bodyparser.json({
    limit: '5mb'
}));
settingsRouter.use(bodyparser.urlencoded({
    limit: '5mb',
    extended: true
}));
var config = require('../init_app');
const dba = require('../db.js');
const db = dba.db;
const pgp = db.$config.pgp;
settingsRouter.get('/Board/Type/:OrganizationTypeId', (req, res, next) => {
    id = req.params.OrganizationTypeId;
    db.any('select * from fn_viewBoardOrTypeById($1)', id).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:OrganizationType Board','ITR','INFO','View of OrganizationType  Board Table successfull','Subject',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "OrganizationType Board Type Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:OrganizationType Board','ITR','ERR','View of OrganizationType Board Table successfull','Subject',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: " Unable to View OrganizationType Board Type"
                });
            });
        })
});
settingsRouter.get('/Board/', (req, res, next) => {
    db.any('select * from fn_viewboard()').then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Board','ITR','INFO','View of Board Table successfull','Subject',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Board Retrived Successfully"
                    });
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Board','ITR','ERR','View of Board Table successfull','Subject',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: " Unable to View Board"
                });
            });
        })
}); {
    //====================================Billing=============================
    {
        //Post for Billing
        settingsRouter.post('/Billing/Add', (req, res, next) => {
            var uname = req.body.userId;
            var iname = req.body.organizationId;
            var stdate = req.body.startDate;
            var endate = req.body.endDate;
            var amt = req.body.amount;
            db.any('select * from fn_savebillfree($1,$2,$3,$4,$5)', [uname, iname, stdate, endate, amt]).then((data) => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Projects','ITR','INFO','Addtion of Projects Table  successfull','Projects',true)").then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "Session Has Been Added Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
                .catch(error => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Projects','ITR','ERR','Addtion of Projects Table unsuccessfull','Projects',False)").then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to Add Billing"
                        })
                    })
                })
        })
        // Get Organization Name and User Name for Billing
        settingsRouter.get('/Billing/Details/:Id', (req, res) => {
            var sessionparam = req.params.Id;
            var userId = '';
            for (var i = 0; i < sessionparam.length; i++) {
                sessionparam = sessionparam.replace('-', '/');
            }
            db.any('select * from fn_getBilling($1)', sessionparam).then((data) => {
                    db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                        userId = session[0].fn_viewuserid;
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Projects','ITR','INFO','View of Projects Fee Table  successfull','Projects',true,$1)", userId).then((log) => {
                            if (data.length > 0) {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: data,
                                    message: "Billing Retrived  Successfully"
                                })
                            } else {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: "NDF",
                                    message: "No Data Found"
                                })
                            }
                        })
                    })
                })
                .catch(error => {
                    db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                        userId = dbsession[0].fn_viewuserid;
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Projects','ITR','ERR','View of Projects Table unsuccessfull','Projects',False,$1)", userId).then((log) => {
                            res.status(200).send({
                                result: false,
                                error: error.message,
                                data: "ERROR",
                                message: "Unable to View Billing"
                            })
                        })
                    })
                });
        })
    }
    //Post for Billing
    settingsRouter.post('/Billing/Update/:Id', (req, res, next) => {
        var id = req.params.Id;
        var uname = req.body.userId;
        var iname = req.body.organizationId;
        var stdate = req.body.startDate;
        var endate = req.body.endDate;
        var amt = req.body.amount;
        db.any('select * from fn_updatebill($1,$2,$3,$4,$5,$6)', [id, uname, iname, stdate, endate, amt]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Projects','ITR','INFO','Addtion of Projects Table  successfull','Projects',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Billing Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Projects','ITR','ERR','Addtion of Projects Table unsuccessfull','Projects',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Update Billing"
                    })
                })
            })
    })
}
// Get Organization Name and User Name for Billing
settingsRouter.get('/Billing/SAdmin/:Id', (req, res) => {
    var sessionparam = req.params.Id;
    var userId = '';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select * from fn_getOrganizationAdmin($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,userId) values ('Table-View:Projects','ITR','INFO','View of Projects Fee Table  successfull','Projects',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: " Billing Retrived  Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Projects','ITR','ERR','View of Projects Table unsuccessfull','Projects',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Billing"
                    })
                })
            });
        });
})
// ========= Change Password ============
{
    settingsRouter.get('/changepassword/:oldpassword/:userSession', function (req, res, next) {
        var sessionparam = req.params.userSession;
        var userId = '';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        var oldpassword = req.params.oldpassword
        var subString = ''
        db.any('select fn_viewuserid from fn_viewuserid($1)', [sessionparam]).then((session) => {
                userId = session[0].fn_viewuserid;
                db.any('select * from authenticationmaster where userid=(select fn_viewuserid from fn_viewuserid($1))', [sessionparam]).then((passcode) => {
                    var passcode1 = passcode[0].passcode
                    subString = passcode1.substring(0, 3)
                    oldpassword = passcode1.substring(0, 3) + oldpassword + '';
                    subString = passcode1.substring(3, 7);
                    oldpassword = oldpassword + subString;
                    db.any('select * from loginview where password=crypt($1,password) and id=$2', [oldpassword, userId]).then((pwd) => {
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Change password','ITR','INFO','View of Change password Table successfull','Change password',true,$1)", userId).then((log) => {
                            if (pwd.length > 0) {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: pwd,
                                    message: "Password Retrieved Successfully"
                                })
                            } else {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: "NDF",
                                    message: "NO DATA FOUND"
                                })
                            }
                        })
                    })
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                    userId = session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Change password','ITR','ERR','View of Change password Table unsuccessfull','Change password',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable To Change Password"
                        });
                    });
                });
            });
    })
    settingsRouter.post('/updatepassword/:newpassword/:userSession', function (req, res, next) {
        var sessionparam = req.params.userSession;
        var userId = '';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        var newpassword = req.params.newpassword
        db.any('select fn_viewuserid from fn_viewuserid($1)', [sessionparam]).then((session) => {
                userId = session[0].fn_viewuserid;
                db.any('select * from authenticationmaster where userid=(select fn_viewuserid from fn_viewuserid($1))', [sessionparam]).then((passcode) => {
                    var passcode1 = passcode[0].passcode
                    subString = passcode1.substring(0, 3)
                    newpassword = passcode1.substring(0, 3) + newpassword + '';
                    subString = passcode1.substring(3, 7);
                    newpassword = newpassword + subString;
                    db.any("update  login set password=crypt($1,gen_salt('md5')) where  userid=$2", [newpassword, userId]).then((pwd) => {
                        userId = session[0].fn_viewuserid;
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:updatepassword ','ITR','INFO','Update of updatepassword Table successfull','updatepassword',true)", userId).then((log) => {
                            if (pwd.length > 0) {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: pwd,
                                    message: "Password Has Been Updated Successfully"
                                })
                            } else {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: "NDF",
                                    message: "NO DATA FOUND"
                                })
                            }
                        })
                    })
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:updatepassword ','ITR','ERR','Update of updatepassword Table unsuccessfull','updatepassword',False)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Cannot Update Password"
                        })
                    });
                })
            });
    })
} {
    // POST FOR country
    settingsRouter.post('/data/country/enter', (req, res, next) => {
        var ccode = req.body.countryCode;
        var name = req.body.name;
        db.any('insert into countries (countrycode,name) values($1,$2)', [ccode, name])
            .then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:country','ITR','INFO','Addition of country Table successfull','country',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "country Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "NO DATA FOUND"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:country','ITR','ERR','Addition of country Table unsuccessfull','country',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable To Add country Details"
                    })
                });
            })
    })
    // POST FOR state
    settingsRouter.post('/data/State/enter', (req, res, next) => {
        var ccode = req.body.countryId;
        var name = req.body.name;
        db.any("select id from countries where countrycode=$1 ", [ccode]).then((data) => {
                sid = data[0].id;
                db.any('insert into states (name,countryid) values($1,$2)', [name, sid]).then(() => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:state','ITR','INFO','Addition of state Table successfull','state',true)").then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "State Has Been Added Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "NO DATA FOUND"
                            })
                        }
                    });
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:state','ITR','ERR','Addition of state Table unsuccessfull','state',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable To Add States"
                    });
                });
            })
    })
    // POST FOR city
    settingsRouter.post('/data/Cities/enter', (req, res, next) => {
        var ccode = req.body.stateId;
        var name = req.body.name;
        db.any("select id from states where name=$1 ", [ccode]).then((data) => {
                sid = data[0].id;
                db.any('insert into cities (name,stateid) values($1,$2)', [name, sid]).then(() => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:city','ITR','INFO','Addition of city Table successfull','city',true)").then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "City Has Been Added Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "NO DATA FOUND"
                            })
                        }
                    })
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:city','ITR','ERR','Addition of city Table unsuccessfull','city',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: "error.message",
                        data: "ERROR",
                        message: "Unable To Add City"
                    })
                })
            })
    })
}
// ============================ INSTITUTE ===============
{
    settingsRouter.post('/OrganizationType/Add', (req, res, next) => {
        name = req.body.instituteName;
        db.any('select * from fn_AddOrganizationType($1)', name).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:OrganizationType','ITR','INFO','Addition of OrganizationType Table successfull',' OrganizationType',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "OrganizationType Posted"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: " NO DATA FOUND"
                        });
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:OrganizationType','ITR','ERR','Addition of OrganizationType Table unsuccessfull',' OrganizationType',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add OrganizationType"
                    })
                })
            });
    })
    settingsRouter.post('/Board/Add', (req, res, next) => {
        boardname = req.body.boardName;
        OrganizationTypeid2 = req.body.organizationTypeId;
        db.any('select * from fn_addboard($1,$2)', [boardname, OrganizationTypeid2]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Board','ITR','INFO','Addition of Board Table successfull',' Board',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Board  Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Board','ITR','ERR','Addition of Board Table unsuccessfull',' Board',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Board"
                    })
                })
            });
    })
    settingsRouter.get('/OrganizationType/', (req, res, next) => {
        db.any('select * from fn_viewOrganizationType()').then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:OrganizationType','ITR','INFO','View of OrganizationType Table successfull','Subject',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "OrganizationType Retrived Successfully"
                        });
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:OrganizationType','ITR','ERR','View of OrganizationType Table unsuccessfull','Subject',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: " Unable to View OrganizationType"
                    });
                });
            })
    })
    settingsRouter.get('/Board/', (req, res, next) => {
        db.any('select * from fn_viewboard()').then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Board','ITR','INFO','View of Board Table successfull','Subject',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Board Retrived Successfully"
                        });
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                    // res.status(200).send(data);
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Board','ITR','ERR','View of Board Table successfull','Subject',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: " Unable to View Board"
                    });
                });
            })
    });
    settingsRouter.get('/OrganizationType/Verify/:inp/:inpData', (req, res, next) => {
        inp = req.params.inp;
        inpData = req.params.inpData;
        db.any('select * from OrganizationType where ' + inp + '=$1', [inpData]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:OrganizationType','ITR','INFO','View of OrganizationType Table successfull','OrganizationType',true)").then((log) => {
                    if (data.length > 0) {
                        var msg = inp + ' already Exists!!';
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: false
                        });
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: true
                        });
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:OrganizationType','ITR','ERR','View of OrganizationType Table unsuccessfull','OrganizationType',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View OrganizationType Details"
                    });
                });
            })
    })
    settingsRouter.get('/Board/Verify/:inp/:inpData', (req, res, next) => {
        inp = req.params.inp;
        inpData = req.params.inpData;
        db.any('select * from board where ' + inp + '=$1', [inpData]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:board','ITR','INFO','View of board Table successfull','board',true)").then((log) => {
                    if (data.length > 0) {
                        var msg = inp + ' already Exists!!';
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: false
                        });
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: true
                        });
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:board','ITR','ERR','View of board Table unsuccessfull','board',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Board Details"
                    });
                });
            })
    })
    settingsRouter.get('/instituteType/', (req, res, next) => {
        db.any('select * from fn_viewinstituteType()').then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:OrganizationType','ITR','INFO','View of  Institute Type Table successfull','Subject',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Institute Type Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Institute Type','ITR','ERR','View of  Institute Type Table successfull','Subject',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: " Unable to View Institute Type"
                    });
                });
            })
    })
    settingsRouter.post('/instituteType/Add', (req, res, next) => {
        name = req.body.name;
        OrganizationTypeid1 = req.body.organizationTypeId;
        db.any('select * from fn_addinstituteType($1,$2)', [name, OrganizationTypeid1]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition: Institute Type','ITR','INFO','Addition of  Institute Type Table successfull','  Institute Type',true)").then((log) => {
                    if (data) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Institute Type Has Been Added Succeessfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Institute Type','ITR','ERR','Addition of Institute Type Table unsuccessfull',' Institute Type',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Institute Type"
                    })
                })
            });
    })
}
// ========== PERMISSION ==============
{
    //permission
    settingsRouter.post('/permission/add', (req, res, next) => {
        roleid = req.body.roleId;
        branchid = req.body.branchId;
        componentid = req.body.componentId;
        read = req.body.read;
        write = req.body.write;
        db.any('select * from fn_addpermissions($1,$2,$3,$4,$5)', [roleid, branchid, componentid, read, write]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:permission','ITR','INFO','Addition of permission Table successfull','permission',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "User Permission Posted"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:permission','ITR','ERR','Addition of permission Table unsuccessfull','permission',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Permission "
                    });
                });
            })
    })

    // get permission
    /*
   settingsRouter.get('/permission/view/:Id', (req, res, next) => {
    var module = '';
    var firstList = [];
    var z = 0;
    var childData = [];
    var sessionparam = req.params.Id;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select * from fn_viewpermissions($1)', sessionparam).then((data) => {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
             userId = dbSession[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:permissiontable','ITR','INFO','View of  permission Table successfull','Subject',true,$1)", userId).then((log) => {
                firstList = data
                for (var i = 0; i < firstList.length; i++) {
                    if (module == firstList[i].modules) {
                        childData[z - 1].components.push({
                            "component": firstList[i].components,
                            "componentid": firstList[i].componentid,
                            "read": firstList[i].read,
                            "write": firstList[i].write,
                            "roleid": firstList[i].roleid,
                            "branchid": firstList[i].branchid
                        })
                    }
                    else {
                        module = firstList[i].modules
                        icon=firstList[i].icon
                        childData.push({
                            "module": module,"icon":icon,
                            "components": [{
                                "component": firstList[i].components,
                                "componentid": firstList[i].componentid,
                                "read": firstList[i].read,
                                "write": firstList[i].write,
                                "roleid": firstList[i].roleid,
                                "branchid": firstList[i].branchid
                            }]
                        })
                        z = z + 1;
                    }
                }
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: childData,
                        message: " Permission Retrived Sucessfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                 userId = dbSession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Permission','ITR','ERR','View of  Permission Table unsuccessfull','Subject',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: " Unable to View Permissions"
                    });
                });
            })
        })
})*/

    // get permission
    settingsRouter.get('/permission/view/:rId/:bId', (req, res, next) => {
        var module = '';
        var firstList = [];
        var z = 0;
        var childData = [];
        var roleName = req.params.rId;
        var branchId = req.params.bId;
        var roleId;
        var query = 'select id from rolemaster where rolename=$1';
        if (roleName.length < 5) {
            query = 'select id from rolemaster where id=$1';
        }
        db.any(query, roleName).then((role) => {
                roleId = role[0].id
                console.log(roleId)
                db.any('select * from fn_viewpermissions($1,$2)', [roleId, branchId]).then((data) => {
                    console.log(data)
                    firstList = data
                    for (var i = 0; i < firstList.length; i++) {
                        if (module == firstList[i].modules) {
                            childData[z - 1].components.push({
                                "component": firstList[i].components,
                                "componentid": firstList[i].componentid,
                                "read": firstList[i].read,
                                "write": firstList[i].write,
                                "roleid": firstList[i].roleid,
                                "branchid": firstList[i].branchid
                            })
                        } else {
                            module = firstList[i].modules
                            icon = firstList[i].icon
                            childData.push({
                                "module": module,
                                "icon": icon,
                                "components": [{
                                    "component": firstList[i].components,
                                    "componentid": firstList[i].componentid,
                                    "read": firstList[i].read,
                                    "write": firstList[i].write,
                                    "roleid": firstList[i].roleid,
                                    "branchid": firstList[i].branchid
                                }]
                            })
                            z = z + 1;
                        }
                    }

                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: childData,
                            message: " Permission Retrived Sucessfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }


                })
            })
            .catch(error => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: " Unable to View Permissions"
                });
            });


    })

   // get permission
   settingsRouter.get('/permission/view/:userSession', (req, res, next) => {
    var sessionparam = req.params.userSession;
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    var module = '';
    var firstList = [];
    var z = 0;
    var childData = [];
    var roleId;
    db.any('select * from userprofileview where userId=(select fn_viewuserid from fn_viewuserid($1))', sessionparam).then((userRole) => {
        roleId = userRole[0].roleid;
        branchId = userRole[0].branchid;
            
            db.any('select * from fn_viewpermissionsLogin($1,$2)', [roleId, branchId]).then((data) => {

                firstList = data
                for (var i = 0; i < firstList.length; i++) {
                    if (module == firstList[i].modules) {
                        childData[z - 1].components.push({
                            "component": firstList[i].components,
                            "componentid": firstList[i].componentid,
                            "read": firstList[i].read,
                            "write": firstList[i].write,
                            "roleid": firstList[i].roleid,
                            "branchid": firstList[i].branchid
                        })
                    } else {
                        module = firstList[i].modules
                        icon = firstList[i].icon
                        childData.push({
                            "module": module,
                            "icon": icon,
                            "components": [{
                                "component": firstList[i].components,
                                "componentid": firstList[i].componentid,
                                "read": firstList[i].read,
                                "write": firstList[i].write,
                                "roleid": firstList[i].roleid,
                                "branchid": firstList[i].branchid
                            }]
                        })
                        z = z + 1;
                    }
                }

                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: childData,
                        message: " Permission Retrived Sucessfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }


            })
        })
        .catch(error => {
            res.status(200).send({
                result: false,
                error: error.message,
                data: "ERROR",
                message: " Unable to View Permissions"
            });
        });


})



}
module.exports = settingsRouter;