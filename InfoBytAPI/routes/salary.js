var express = require('express');
var bodyparser = require('body-parser');
var payslip = require('../payslip')
var fs = require('fs')
var path = require('path')
var mail = require('../mailer');
var salaryRouter = express.Router()
salaryRouter.use(express.static(path.resolve(__dirname, "public")));
salaryRouter.use(bodyparser.json({
    limit: '5mb'
}));
salaryRouter.use(bodyparser.urlencoded({
    limit: '5mb',
    extended: true
}));
var config = require('../init_app');
const dba = require('../db.js');
const db = dba.db;
const pgp = db.$config.pgp;
// ============================== Leave Details=======================================
{
    //Done   
    // Post For Leave
    salaryRouter.post('/User/Leave/Add/:sessionparam', (req, res, next) => {
        var uid = req.params.sessionparam;
        var fd = req.body.fromDate;
        var td = req.body.toDate;
        var tp = req.body.type;
        var desc = req.body.description;
        var sts = 'Pending'
        var appby = req.params.sessionparam;
        for (var i = 0; i < uid.length; i++) {
            uid = uid.replace('-', '/');
        }
        db.any('select fn_viewuserid from fn_viewuserid($1)', uid).then((data) => {
                uid = data[0].fn_viewuserid;
                appby = data[0].fn_viewuserid;
                db.any('select fn_addleavedetails($1,$2,$3,$4,$5,$6,$7)', [uid, fd, td, tp, sts, desc, appby])
                    .then(function () {
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Leave  ','ITR','INFO','Addtion of  Leave  Table  successfull','Leave ',true)").then((log) => {
                            if (data.length > 0) {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: data,
                                    message: "Leave Has Been Added Successfully"
                                })
                            } else {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: "NDF",
                                    message: "No Data Found"
                                })
                            }
                        })
                    })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Leave  ','ITR','ERR','Addtion of  Leave  Table unsuccessfull','Leave ',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Leaves"
                    })
                })
            });
    })
    //Done
    //POST For leave master
    salaryRouter.post('/leaveType/Add', (req, res, next) => {
        type = req.body.type;
        db.any('select * from fn_addleavemaster($1)', [type]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:leave','ITR','INFO','Addition of leave  Table successfull','Leave Types',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "LeaveType Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:leave','ITR','ERR','Addition of leave Table unsuccessfull','Leave Types',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: " Unable to Add Leave Type"
                    });
                })
            })
    })
    //Done
    //View Leave Master
    salaryRouter.get('/leave/get', (req, res, next) => {
        db.any('select * from fn_viewleavemaster()').then((data) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "leave Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
            .catch(error => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: " Unable to View leave "
                })
            });
    })
    salaryRouter.post('/Add/account/details', (req, res, next) => {
        staffId = req.body.staffId;
        bank = req.body.bankName;
        account = req.body.accountNumber;
        pan = req.body.panNumber;
        pf = req.body.pfAccountNumber;
        db.any('select * from fn_addaccountdetails($1,$2,$3,$4,$5)', [bank, staffId, account, pan, pf]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Staff','ITR','INFO','Addition of STAFF Table successfull','Staff',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Account Details Has Been Added Succesfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Staff','ITR','ERR','Addition of STAFF Table unsuccessfull','Staff',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: " Unable to Account Details"
                    })
                });
            })
    });
    //Done
    //Get For Leave
    salaryRouter.get('/User/Leave/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId = '';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewleavesofall($1)', sessionparam).then((data) => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                    userId = Session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Leave  ','ITR','INFO','View of  Leave  Table  successfull','Leave ',true,$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "Leave Details Retrived Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                    userId = Session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Leave  ','ITR','ERR','View of  Leave  Table  unsuccessfull','Leave ',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unableto View Leave Details"
                        })
                    })
                })
            });
    })
    //Done
    //Half login request
    salaryRouter.post('/User/Half/Login/Add/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;

        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        var userid
        var rtime = req.body.requestTime
        var dscptn = req.body.description
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsesstion) => {
                userid = dbsesstion[0].fn_viewuserid
                db.any('select fn_addhalfloginrequest($1,$2,$3)', [userid, rtime, dscptn]).then((data) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Half Login Request Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    'message': 'Unable to Add Half Login Request'
                })
            });
    })
    //Done
    //Get By Id For Leave
    salaryRouter.get('/User/Leave/Details/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select * from fn_viewleavedetails($1)', i).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Leave  ','ITR','INFO','View of  Leave  Table  successfull','Leave ',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Leave Details Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Leave  ','ITR','ERR','View of  Leave  Table  unsuccessfull','Leave ',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Leave Details"
                    })
                })
            });
    })
    //Done
    //Update For Leave
    salaryRouter.post('/User/Leave/Update/:Id/:sessionparam', (req, res, next) => {
        id = req.params.Id;
        uid = req.body.userId;
        fd = req.body.fromDate;
        td = req.body.toDate;
        st = req.body.status;
        dc = req.body.description;
        ab = req.params.sessionparam;
        for (var i = 0; i < ab.length; i++) {
            ab = ab.replace('-', '/');
        }
        db.any('select id,firstname,lastname from users where id=(select fn_viewuserid from fn_viewuserid($1))', ab).then((data) => {
                ab = data[0].id;
                db.any('select * from fn_updateleavedetails($1,$2,$3,$4,$5,$6,$7)', [id, uid, fd, td, st, dc, ab]).then((data) => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Leave  ','ITR','INFO','Update of  Leave  Table  successfull','Leave ',true)").then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: id,
                                message: "Leave Has Been updated Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Leave  ','ITR','ERR','Update of  Leave  Table unsuccessfull','Leave ',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Cannot Update Leave"
                    })
                })
            })
    })
    //Done
    //GET FOR halfloginrequest
    salaryRouter.get('/Half/Login/Request/:Id', (req, res) => {
        var sessionparam = req.params.Id;
        var userId = '';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewHalfLoginRequests($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Half Login Request ','ITR','INFO','View of Half Login Request Table  successfull','Half Login Request',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Half Login Request Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    };
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Half Login Request ','ITR','ERR','View of Half Login Request Table unsuccessfull','Half Login Request',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Half Login Request"
                        })
                    })
                })
            });
    })
    //Done
    //GET FOR halfloginrequest by id
    salaryRouter.get('/Half/Request/Details/:Id', (req, res, next) => {
        id = req.params.Id
        db.any('select * from halfloginrequestview where id=$1', id).then((data) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Half Login Request Details Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
            .catch(error => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: " Unable to View Half Login Request Details"
                })
            });
    })
    //Done
    //update halfloginrequest
    salaryRouter.post('/Half/Login/Request/Update/:Id', (req, res, next) => {
        var id = req.params.Id
        var st = req.body.Status
        db.any('update halfloginrequets set status=$2 where id=$1', [id, st]).then((data) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Updated Half Login Request Details"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
            .catch(error => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    'message': 'Unable to Update Half Login Request Details'
                })
            });
    })
    //Left
    //notification for lev
    salaryRouter.post('/notification/leave/add/:Id', (req, res, next) => {
        sessionparam = req.params.Id
        var notification = "A new leave request";
        var rcvr = '';
        var notificationid = 0;
        var userId = '';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewLevreceiverIds($1)', sessionparam).then((data) => {
                rcvr = data;
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid;
                    db.any(' insert into notifications(notification) values ($1) returning id', notification).then((data) => {
                        notificationid = data[0].id;
                        for (var j = 0; j < rcvr.length; j++) {
                            db.any('select * from fn_addnotificationdetails($1,$2,$3,$4)', [rcvr[j].rolename, rcvr[j].userid, notificationid, sessionparam])
                                .then((data) => {
                                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','INFO','Addition of Notifications Table  successfull','Notifications',true,$1)", userId).then((log) => {
                                        if (data.length > 0) {
                                            res.status(200).send({
                                                result: true,
                                                error: "NOERROR",
                                                data: data,
                                                message: "Leave Detail Notification Added Successfully"
                                            })
                                        } else {
                                            res.status(200).send({
                                                result: true,
                                                error: "NOERROR",
                                                data: "NDF",
                                                message: "No Data Found"
                                            })
                                        }
                                    })
                                })
                        }
                    })
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','ERR','Addition of Notifications Table unsuccessfull','Notifications',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to Add Notifications"
                        })
                    })
                })
            })
    })
}
//Done
//========================================Pay Roll=======================================================
{
    //Done
    //add
    salaryRouter.post('/salary/payroll/Add/:Id', (req, res, next) => {
        var brnid
        var strtrng = req.body.startRange;
        var endrng = req.body.endRange;
        var nme = req.body.name;
        var amt = req.body.amount;
        var sessionparam = req.params.Id;
        var userId = '';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid($1))', [sessionparam])
            .then((data) => {
                this.brnid = data[0].branchid
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                    userId = session[0].fn_viewuserid;
                    db.any('select * from fn_checkpayrolldata($1,$2,$3,$4)', [nme, strtrng, endrng, this.brnid])
                        .then((checkdata) => {
                            if (checkdata.length == 0) {
                                db.any('select * from fn_addpayroll($1, $2, $3,$4,$5)', [this.brnid, strtrng, endrng, nme, amt])
                                    .then((data) => {
                                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-Addtion: Bus Driver','ITR','INFO','Addtion of  Bus Driver Table  successfull','Bus Driver',true,$1)", userId).then((log) => {
                                            if (data.length > 0) {
                                                res.status(200).send({
                                                    result: true,
                                                    error: "NOERROR",
                                                    data: data,
                                                    message: "Payroll Has Been Added Successfully"
                                                })
                                            } else {
                                                res.status(200).send({
                                                    result: true,
                                                    error: "NOERROR",
                                                    data: "NDF",
                                                    message: "No Data Found"
                                                })
                                            }
                                        })
                                    })
                            } else {
                                res.status(200).send({
                                    message: "duplicate"
                                })
                            }
                        })
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                    userId = session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-Addtion: Bus Driver','ITR','ERR','Addtion of  Bus Driver Table  unsuccessfull','Bus Driver',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to Add Payroll"
                        })
                    })
                });
            })
    })
    //done
    //get all
    salaryRouter.get('/Branch/salary/payroll/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId = '';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select  * from fn_viewPayroll($1)', sessionparam).then((data) => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Transport stops','ITR','INFO','View of  Transport stops Table  successfull','  Transport stops',true,$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "Payroll  Retrived Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                    userId = session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Transport stops','ITR','ERR','View of  Transport stops Table unsuccessfull','  Transport stops',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Payroll"
                        })
                    })
                });
            });
    })
    //Done
    //get staff of dept
    salaryRouter.get('/Branch/polici/staff/department/:Id', (req, res, next) => {
        var did = req.params.Id;
        db.any('select  * from staffview where departmentid=($1)', did).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Transport stops','ITR','INFO','View of  Transport stops Table  successfull','  Transport stops',true)").then((log) => {
                    for (var i = 0; i < data.length; i++) {
                        data[i].checkBoxStatus = true
                    }
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Session Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                /*  db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                     var userId = session[0].fn_viewuserid; */
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Transport stops','ITR','ERR','View of  Transport stops Table unsuccessfull','  Transport stops',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Department Policies"
                    })
                })
            });
    })
    //Done
    salaryRouter.post('/salary/Department/Policies/Add/', (req, res, next) => {
        console.log(req.body)
        var staffList = req.body.stfList
        var departmentList = req.body.dept
        var deptid = departmentList.departmentId;
        var nme = departmentList.allowanceName;
        var amt = departmentList.amount;
        var typ = departmentList.type;

        for (let i = 0; i < staffList.length; i++) {
            if (staffList[i].checkBoxStatus == true) {
                this.uid = staffList[i].userid;
                db.any('select * from fn_adddepartmentpolicies($1, $2, $3,$4,$5)', [deptid, this.uid, nme, amt, typ])
                    .then((data) => {
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-Addtion: Bus Driver','ITR','INFO','Addtion of  Bus Driver Table  successfull','Bus Driver',true,$1)", userId).then((log) => {
                            if (data.length > 0) {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: data,
                                    message: "Department Policies Has Been Added Successfully"
                                })
                            } else {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: "NDF",
                                    message: "No Data Found"
                                })
                            }
                        })
                    })
                    .catch(error => {
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: Bus Driver','ITR','ERR','Addtion of  Bus Driver Table  unsuccessfull','Bus Driver',False)").then((log) => {
                            res.status(200).send({
                                result: false,
                                error: error.message,
                                data: "ERROR",
                                message: "Unable to Add Department Policies"
                            })
                        })
                    });
            }
        }
    })
    //Done
    salaryRouter.get('/Branch/staff/department/policy/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId = '';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select  * from fn_viewdepartmentpolicies($1)', sessionparam).then((data) => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                    userId = session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Transport stops','ITR','INFO','View of  Transport stops Table  successfull','  Transport stops',true,$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "Department policy Details Retrived  Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                    userId = session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Transport stops','ITR','ERR','View of  Transport stops Table unsuccessfull','  Transport stops',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Department policy Details"
                        })
                    })
                });
            });
    })
}
//Done
//===================================================Pay Slip============================================
{
    //Done
    //generate Net Salary
    salaryRouter.post('/Staff/Salary/GrossSalary/:month/:userSession', (req, res, next) => {
        var idsList = []
        var totalAllowances = 0;
        var totalDeductions = 0;
        var madatoryDeductions = 0;
        var allExtraAllowances;
        var basicPercentage = 0;
        var totalPayRollDeductions = 0;
        var uId;
        var tds = 0;
        allList = req.body
        var sessionparam = req.params.userSession;
        month = req.params.month;
        for (var l = 0; l < sessionparam.length; l++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        getData().then((data => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Payslip Has Been Added Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        }))

        function delay() {
            return new Promise(resolve => setTimeout(resolve, 2000))
        }
        async function delayedLog(item) {
            if (item.checkboxStatus == true) {
                totalDeductions = 0;
                totalPayRollDeductions = 0;
                totalAllowances = 0;
                madatoryDeductionsPercentage = 0;
                db.any("select * from extraallowances where userid=$1", item.userid).then((data) => {
                        if (data.length != 0) {
                            allExtraAllowances = data;
                            for (var k = 0; k < allExtraAllowances.length; k++) {
                                if (allExtraAllowances[k].type == 'Allowance')
                                    totalAllowances = totalAllowances + allExtraAllowances[k].amount
                                else
                                    totalDeductions = totalDeductions + allExtraAllowances[k].amount
                            }
                        }
                        db.any('select * from salaryallowancestable where patternId=(select splallowances from salary where staffid=(select id from staff where userid=$1))', [item.userid]).then((allowancesList) => {
                            for (var z = 0; z < allowancesList.length; z++) {
                                if (allowancesList[z].type == 'Deduction')
                                    madatoryDeductionsPercentage = madatoryDeductionsPercentage + parseFloat(allowancesList[z].percentage);
                            }
                            db.any('select * from payroll where ((select salary from salary where id=(select salary from staff where userid=$1)) between startrange and endrange) and branchid=(select branchid from userprofileview where userid=$1)', item.userid).then((payrollList) => {
                                for (var m = 0; m < payrollList.length; m++)
                                    totalPayRollDeductions = totalPayRollDeductions + payrollList[m].amount
                                db.any('select * from fn_viewtotaltaxableamount($1,$2,$3)', [item.userid, month, month + '-unique-' + item.userid]).then((tds) => {
                                    tds = tds[0].fn_viewtotaltaxableamount;
                                    db.any(" select (((((ROUND(sum(totalminutes*payrate))+$3)-$4)-(cast(((sum(shiftminutes*payrate))*$5)as numeric)/100.0)+ROUND(sum(otminutes*otpayrate)))-$6)-$7) as net from salarypay where usrcode=(select cast(deviceuserid as int) from usermapping where appuserid=$1) and to_char(attendancedate, 'YYYY-MM')=$2", [item.userid + '', month, totalAllowances, totalDeductions, madatoryDeductionsPercentage, totalPayRollDeductions, tds]).then((net) => {
                                        db.any("update payslip p set grosssalary=$1, month='" + month + '-01' + "',status='Active' where p.userid = $2 and p.month=$3 returning p.grosssalary", [net[0].net, item.userid, month + '-01']).then((data) => {
                                            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                                                var userId = Session[0].fn_viewuserid;
                                                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-Addtion:Payslip','ITR','INFO','Addtion of Payslip Table  successfull','Payslip',true,$1)", userId).then((log) => {})
                                            })
                                        })
                                    })
                                })
                            })
                        })
                    })
                    .catch(error => {
                        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                            var userId = Session[0].fn_viewuserid;
                            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-Addtion:Payslip','ITR','ERR','Addtion of Payslip Table  unsuccessfull','Payslip',False,$1)", userId).then((log) => {
                                res.status(200).send({
                                    message: "Unable to Generate Salary Slip"
                                })
                            })
                        })
                    });
            }
            await delay()
        }
        async function getData() {
            for (i = 0; i < allList.length; i++) {
                await delayedLog(allList[i])
            }
            return {
                "data": "added"
            };
        }
    })
    salaryRouter.post('/Add/account/details', (req, res, next) => {
        staffId = req.body.staffId;
        bank = req.body.bankName;
        account = req.body.accountNumber;
        pan = req.body.panNumber;
        pf = req.body.pfAccountNumber;
        db.any('select * from fn_addaccountdetails($1,$2,$3,$4,$5)', [bank, staffId, account, pan, pf]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Staff','ITR','INFO','Addition of STAFF Table successfull','Staff',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Account Details Has Been Added Succesfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Staff','ITR','ERR','Addition of STAFF Table unsuccessfull','Staff',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: " Unable to Account Details"
                    })
                });
            })
    });
    //Done
    // get attendance of month for a user
    salaryRouter.get('/Staff/AttendanceDetails/PaySlip/:month/:userSession', (req, res, next) => {
        var t = req.params.month;
        t = t + '';
        var y = t.substring(0, 4);
        var m = t.substring(5, 7);
        var mindate, maxdate;
        var userId = '';
        var sessionparam = req.params.userSession;
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.tx(t => {
                return t.any("select min(attendanceDate) from attendancelog where date_part('month',cast(attendanceDate as date))=cast($1 as integer)  and date_part('year',cast(attendanceDate as date))= cast($2 as integer)", [m, y]).then(function (data) {
                    mindate = data[0].min;
                    db.tx(t => {
                        return t.any("select max(attendanceDate) from attendancelog where date_part('month',cast(attendanceDate as date))=cast($1 as integer)  and date_part('year',cast(attendanceDate as date))= cast($2 as integer)", [m, y]).then(function (data) {
                            maxdate = data[0].max;
                            mindate = "'" + mindate + "'";
                            maxdate = "'" + maxdate + "'";
                            db.tx(t => {
                                return t.any("create or replace view Paytmview as select usrcode,statusCode,count(statusCode) from attendanceLog where cast(attendanceDate as date) between " + mindate + " and " + maxdate + " group by 1,2 order by 1,2").then((view) => {
                                    db.any("select * from fn_viewpaydetails($1,$2,$3)", [sessionparam, m, y]).then((paydetails) => {
                                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Payslip','ITR','INFO','View of Payslip Table  successfull','Payslip',true)").then((log) => {
                                            if (paydetails.length > 0) {
                                                res.status(200).send({
                                                    result: true,
                                                    error: "NOERROR",
                                                    data: paydetails,
                                                    message: "Salary Slip Retrived Successfully"
                                                })
                                            } else {
                                                res.status(200).send({
                                                    result: true,
                                                    error: "NOERROR",
                                                    data: "NDF",
                                                    message: "Salary Slip Retrived Successfully"
                                                })
                                            }
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                    userId = session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Payslip','ITR','ERR','View of Payslip Table unsuccessfull','Payslip',False,$1)", userId).then((log) => {
                        console.log('ERROR:Unable to View Salary Slip', error.message || error);
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Salary Slip"
                        })
                    })
                })
            });
    })
    //Done
    //add Pay Details
    salaryRouter.post('/Paydetails/Add/:month', (req, res, next) => {
        payList = req.body

        function insertpaydetails(payList, callback) {
            for (var i = 0; i < payList.length; i++) {
                if (payList[i].grosssalary != null) {
                    db.any("INSERT INTO paydetails (userid, present,absent,weekoff,grosssalary,netsalary,month,uniquecode) VALUES ($1,$2,$3,$4,$5,$6,$7,$8) ON CONFLICT (uniquecode) DO UPDATE set netsalary=$6 ", [req.body[i].userid, req.body[i].present, req.body[i].absent, req.body[i].weekoffs, req.body[i].salary, req.body[i].grosssalary, req.params.month, req.body[i].userid + req.params.month + '']).then((data) => {
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Payslip','ITR','INFO','Addition of Payslip Table  successfull','Payslip',true)").then((log) => {})
                    }).catch(error => {
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Payslip','ITR','ERR','Addition of Payslip Table unsuccessfull','Payslip',False)").then((log) => {
                            callback(false)
                        })
                    })
                    if (i == payList.length - 1) {
                        callback(true)
                    }
                }
            }
        }
        insertpaydetails(payList, function (result) {
            if (result) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: result,
                    message: "Paydetails Added"
                })
            } else {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Add Paydetails"
                })
            }
        })
    })
    //done
    //payslip details of user
    salaryRouter.get('/Staff/Salary/PaySlip/:id', (req, res, next) => {
        var t = req.params.id;
        db.any('select * from fn_viewpayslipofuser($1)', [t]).then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Payslip','ITR','INFO','View of Payslip Table  successfull','Payslip',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Salary Slip Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Payslip','ITR','ERR','View of Payslip Table unsuccessfull','Payslip',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Salary Slip"
                    })
                })
            });
    })
    //Done
    //get salaryallowances of user
    salaryRouter.get('/Staff/Salary/SalaryAllowances/:id', (req, res, next) => {
        var t = req.params.id;
        db.any('select * from salaryallowancestable where patternId=(select splallowances from salary where id=(select salary from staff where userid=$1))', [t]).then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Salary Allowance ','ITR','INFO','View of  Salary Allowance Table  successfull','Salary Allowance',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Salary Payment by Allowances Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    };
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Salary Allowance ','ITR','ERR','View of  Salary Allowance Table unsuccessfull','Salary Allowance',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Salary Payment by Allowances"
                    })
                })
            });
    })
    //Done
    salaryRouter.post('/Staff/Salary/PaySlip/Send/:userSession', (req, res, next) => {
        var month = req.body.month
        var sessionparam = req.params.userSession;
        var userId = '';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        payslip(req.body.userId, month, sessionparam, function (pay) {
            if (pay) {
                db.any("update payslip set status='Sent' where userid=$1", req.body.userId).then((data) => {
                    db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                        userId = dbsession[0].fn_viewuserid;
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Payslip','ITR','INFO','Addition of Payslip Table  successfull','Payslip',true,$1)", userId).then((log) => {
                            if (data.length > 0) {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: data,
                                    message: "Pay Slip Sent"
                                })
                            } else {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: "NDF",
                                    message: "Pay Slip Sent"
                                })
                            }
                        })
                    })
                }).catch(error => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Payslip','ITR','ERR','Addition of Payslip Table unsuccessfull','Payslip',False)").then((log) => {
                        console.log('ERROR:Unable to Add Payslip', error.message || error);
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to Add Payslip"
                        })
                    })
                })
            } else {
                db.any("update payslip set status='Inactive' where userid=$1", req.body.userId).then((data) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Pay Slip Not Sent"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                }).catch(error => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Payslip','ITR','ERR','Addition of Payslip Table unsuccessfull','Payslip',False)").then((log) => {
                        console.log('ERROR:Unable to Add Payslip', error.message || error);
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to Add Payslip"
                        })
                    })
                })
            }
        })
    })
    //Done
    //Add Extra Allowances
    salaryRouter.post('/Add/ExtraAllowances', (req, res, next) => {
        db.any('insert into extraAllowances(userid,allowancename,amount,month,type) values($1,$2,$3,$4,$5)', [req.body.userId, req.body.allowanceName, req.body.amount, req.body.month + '-01', req.body.type]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:requestrecruitments','ITR','INFO','Addtion of Projects Table  successfull','Projects',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Session Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:requestrecruitments','ITR','ERR','Addtion of Projects Table unsuccessfull','Projects',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Extra Allowance "
                    })
                })
            })
    })
    //Done
    //Update Extra Allowances
    salaryRouter.post('/Update/ExtraAllowances/:userId/:month', (req, res, next) => {
        db.any("insert into payslip(userid,extraallowancesstatus,month) values($1,$2,$3)", [req.params.userId, 'Active', req.params.month + '-01']).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:requestrecruitments','ITR','INFO','Addtion of Projects Table  successfull','Projects',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Extra Allowances Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "Extra Allowances Has Been Added Successfully"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:requestrecruitments','ITR','ERR','Addtion of Projects Table unsuccessfull','Projects',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to update Extra Allowances "
                    })
                })
            })
    })
    salaryRouter.get('/Staff/Ispayslip/', (req, res, next) => {
        db.any("select userid from payslip where status='Active' or status='Sent'").then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Payslip','ITR','INFO','View of Payslip Table  successfull','Payslip',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Salary Slip of User Retrieved"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Payslip','ITR','ERR','View of Payslip Table unsuccessfull','Payslip',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Salary Slip"
                    })
                })
            });
    })
    //get user payslip
    salaryRouter.get('/Public/Payslip/:Id', (req, res, next) => {
        var userId = req.params.Id
        var filesList1 = [];
        const directoryPath = path.resolve(__dirname, '../public/payslip/' + userId + "/");
        try {
            fs.readdir(directoryPath, 'utf8', (err, files) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:Payslips','ITR','INFO','view of Payslips successfull','payslips',true)").then((log) => {
                    for (var i = 0; i < files.length; i++) {
                        if (path.extname(files[i]) == '.pdf') {
                            filesList1.push({
                                image: files[i],
                                view: false
                            })
                        }
                    }
                    if (files.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: filesList1,
                            message: "Payslips Has Been Retrieved succesfully "
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        } catch (error) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:Payslips','ITR','INFO','view of Payslips successfull','payslips',true)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Payslip"
                })
            })
        };
    })


    //get user payslip
    salaryRouter.get('/Public/Payslip/:Id/:Name', (req, res) => {
        var nme = req.params.Name;
        var i = req.params.Id;
        var fn = '../public/payslip/' + i + '/' + nme;
        res.status(200).sendFile(path.resolve(__dirname, fn))
    })

}
//Done
//================================================Reports==============================================
{
    //Done
    //view PF reports
    salaryRouter.get('/Branch/Deduction/Pf/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId = '';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select  * from fn_viewPfReport($1)', sessionparam).then((data) => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((sess) => {
                    userId = sess[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Transport stops','ITR','INFO','View of  Transport stops Table  successfull','  Transport stops',true,$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "PF Report  Retrived Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                    userId = session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Transport stops','ITR','ERR','View of  Transport stops Table unsuccessfull','  Transport stops',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View PF Report"
                        })
                    })
                });
            })
    })
    //Done
    //View ESI Reports
    salaryRouter.get('/Branch/Deduction/esi/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId = '';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select  * from fn_viewESIReport($1)', sessionparam).then((data) => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((sess) => {
                    userId = sess[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Transport stops','ITR','INFO','View of  Transport stops Table  successfull','  Transport stops',true,$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "ESI Report  Retrived Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((sess) => {
                    userId = sess[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Transport stops','ITR','ERR','View of  Transport stops Table unsuccessfull','  Transport stops',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View ESI Report"
                        })
                    })
                });
            })
    })
    //Done
    //View PT Reports
    salaryRouter.get('/Branch/Deduction/pt/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId = '';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select  * from fn_viewPtReport($1)', sessionparam).then((data) => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((sess) => {
                    userId = sess[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Transport stops','ITR','INFO','View of  Transport stops Table  successfull','  Transport stops',true,$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: " PT report Retrived  Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((sess) => {
                    userId = sess[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Transport stops','ITR','ERR','View of  Transport stops Table unsuccessfull','  Transport stops',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View PT report"
                        })
                    })
                });
            })
    })
    //Done
    salaryRouter.get('/Branch/Deduction/tds/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId = '';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select  * from fn_viewtdsreport($1)', sessionparam).then((data) => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Transport stops','ITR','INFO','View of  Transport stops Table  successfull','  Transport stops',true,$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: " TDS Details   Retrived Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Transport stops','ITR','ERR','View of  Transport stops Table unsuccessfull','  Transport stops',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View TDS Details"
                        })
                    })
                });
            });
    })
}
//=================================================Salary Master==============================================
{
    //Done
    //Post For Salary 
    salaryRouter.post('/Salary/Add', (req, res, next) => {
        stf = req.body.staffId;
        sal = req.body.salary;
        spall = req.body.specialAllowances;
        db.any('select fn_addsalary($1,$2,$3)', [stf, sal, spall]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Salary','ITR','INFO','Addition of SALARY Table successfull','Salary',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Salary Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Salary','ITR','ERR','Addition of SALARY Table unsuccessfull','Salary',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Salary"
                    })
                });
            })
    })
    //Done
    //get by id
    salaryRouter.get('/Salary/Details/:Id', (req, res, next) => {
        i = req.params.Id
        db.any('select * from fn_viewsalarydetails($1)', i).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Salary','ITR','INFO','View of SALARY Table successfull','Salary',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Salary Details Retrieved Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Salary','ITR','ERR','View of SALARY Table unsuccessfull','Salary',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: " Unable to View Salary Details"
                    })
                });
            })
    })
    //Done
    //get user salary transactions
    salaryRouter.get('/salary/transactions/:Id', (req, res) => {
        var sessionparam = req.params.Id;
        var userId = '';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        sessionparam = 8
        db.any('select * from fn_viewsalarytransactions($1)', sessionparam).then((data) => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    var userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view:  Salary','ITR','INFO','Retrive  of  Salary Table  successfull',' Salary table',$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "salary transactions Retrieved Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:  Salary','ITR','ERR','Retrive  of  Salary Table  unsuccessfull',' Salary table',False)").then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Salary"
                        })
                    })
                })
            });
    })
    //Done
    /* Get For  Salary */
    salaryRouter.get('/Salary/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId = '';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any("select fn_viewuserid from fn_viewuserid($1)", sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid
                db.any('select   * from fn_viewsalary($1)', sessionparam).then((data) => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Salary','ITR','INFO','View of SALARY Table successfull','Salary',true,$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "Salary Retrieved Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
            .catch(error => {
                db.any("select fn_viewuserid from fn_viewuserid($1)", sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Salary','ITR','ERR','View of SALARY Table unsuccessfull','Salary',False)").then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: " Unable to View Salary"
                        })
                    });
                })
            })
    })
}
//====================================================Salary Hike==========================================
{
    //Done
    //Post For Salary Hike 
    salaryRouter.post('/Salary/Upgrade', (req, res, next) => {
        si = req.body.staffId;
        hd = req.body.hikeDate;
        inc = req.body.increment;
        type = req.body.type;
        db.any('select fn_addsalaryhike($1,$2,$3,$4)', [si, hd, inc, type]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Salary-hike','ITR','INFO','Addition of SALARY HIKE Table successfull','Salary-hike',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Salary Hike Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Salary-hike','ITR','ERR','Addition of SALARY HIKE Table unsuccessfull','Salary-hike',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Salary Hike"
                    })
                });
            })
    })
    //Done
    //get user salaryhike transactions
    salaryRouter.get('/salary/hike/transactions/:Id', (req, res) => {
        var sessionparam = req.params.Id;
        var userId = '';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid;
                db.any('select * from fn_viewsalaryhiketransactions($1)', sessionparam).then((data) => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view:Salary-hike','ITR','INFO','Retrive  of  Salary-hike Table  successfull',' Salary-hike table',true,$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "salaryhike transactions Retrieved Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:  Salary-hike ','ITR','ERR','Retrive  of  Salary-hike Table  unsuccessfull',' Salary-hike table',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Salary Hike"
                        })
                    })
                })
            });
    })
    //Done
    // Get For  Salary Hike 
    salaryRouter.get('/Salary/Upgrade/All/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId = '';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid;
                db.any(' select * from fn_viewsalaryhike($1)', sessionparam).then((data) => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Salary-hike','ITR','INFO','View of SALARY HIKE Table successfull','Salary-hike',true,$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "Salary Hike Upgraded Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Salary-hike','ITR','ERR','View of SALARY HIKE Table unsuccessfull','Salary-hike',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Salary Hike"
                        })
                    });
                })
            })
    })
    //Done
    // Get By Id For  Salary Hike
    salaryRouter.get('/Salary/Upgrade/Details/:Id', (req, res, next) => {
        i = req.params.Id
        db.any('select * from fn_viewsalaryhikedetails($1)', i).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Salary-hike','ITR','INFO','View of SALARY HIKE Table successfull','Salary-hike',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Salary Hike Details Updated Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Salary-hike','ITR','ERR','View of SALARY HIKE Table unsuccessfull','Salary-hike',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Update Applicants Agreement"
                    })
                });
            })
    })
    //Done
    //tO SENT hikeletter
    salaryRouter.post('/Salary/Hike/Mail/Send/:uId/:id', (req, res, next) => {
        mail.letterHike(req.params.uId, req.params.id, function (data) {
            if (data) {
                id = req.params.id
                db.any("update salaryhike set hikemailstatus='Sent' where id=$1", id).then((data) => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Payslip','ITR','INFO','Addition of Payslip Table  successfull','Payslip',true)").then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "Pay Slip Sent"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            } else {
                id = req.params.id
                db.any("update salaryhike set hikemailstatus='Inactive' where id=$1", id).then((data) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Pay Slip not Sent"
                    })
                });
            }
        })
    })
}
//============================================Salary Pattern==============================================
{
    //Done
    salaryRouter.post('/SalaryPattern/patternnamename/', (req, res, next) => {
        var pName = req.body.key;
        db.any("select patternname from salarypattern where patternname = $1", pName).then(function (data) {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Salary Pattern Retrieved Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        }).catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Salary Pattern','ITR','ERR','View of Salary Pattern Table unsuccessfull','Salary Pattern',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Salary Pattern"
                })
            })
        });
    })
    //Done
    //get leave pattern by salarypatternid
    salaryRouter.get('/Staff/LeavePattern/Details/All/:Id', (req, res, next) => {
        spi = req.params.Id;
        db.any('select * from fn_getleavepattern($1)', [spi]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:leave Pattern','ITR','INFO','View of leave Pattern Table  successfull','leave Pattern',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Leave Pattern Retrieved Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:leave Pattern','ITR','ERR','View of leave Pattern Table unsuccessfull','leave Pattern',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View leave Pattern"
                    })
                })
            });
    })
    //Done
    //get salary pattern by salarypatternid
    salaryRouter.get('/Staff/SalaryPattern/Details/All/:Id', (req, res, next) => {
        spi = req.params.Id;
        db.any('select * from fn_getAllowances($1)', [spi]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Salary Pattern','ITR','INFO','View of Salary Pattern Table  successfull','Salary Pattern',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Salary Pattern Retrieved Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Salary Pattern','ITR','ERR','View of Salary Pattern Table unsuccessfull','Salary Pattern',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Salary Pattern"
                    })
                })
            });
    })
    //Done
    salaryRouter.post('/Staff/Branch/SalaryPattern/Add/:structureName/:Id', (req, res, next) => {
        var sName = req.params.structureName;
        var patternId = 0;
        var branchid
        var sessionparam = req.params.Id;
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select Id,branchId from userprofileview where id=(select fn_viewuserid from fn_viewuserid($1))', sessionparam).then((bid) => {
                branchid = bid[0].branchid
                userId = bid[0].Id
                db.any('insert into salarypattern(patternname,branchid) values($1,$2) returning id', [sName, branchid]).then((data) => {
                    patternId = data[0].id;
                    for (i = 0; i < req.body.aL.length; i++) {
                        db.any('insert into salaryallowancestable(patternid,AllowancesType,percentage,type) values($1,$2,$3,$4)', [patternId, req.body.aL[i].AlloanceName, req.body.aL[i].AllowancePercentage, 'Allowance']).then((data) => {
                            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addtion:Salary Pattern','ITR','INFO','Addtion of Salary Pattern(A) Table  successfull','Salary Pattern',true,$1)", userId).then((log) => {})
                        })
                    }
                    for (i = 0; i < req.body.dL.length; i++) {
                        db.any('insert into salaryallowancestable(patternid,AllowancesType,percentage,type) values($1,$2,$3,$4)', [patternId, req.body.dL[i].DeductionName, req.body.dL[i].DeductionPercentage, 'Deduction']).then((data) => {
                            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addtion:Salary Pattern','ITR','INFO','Addtion of Salary Pattern(D) Table  successfull','Salary Pattern',true,$1)", userId).then((log) => {})
                        })
                    }
                    for (i = 0; i < req.body.lL.length; i++) {
                        db.any('insert into leavepattern(patternid,LeaveType,numofdays,type) values($1,$2,$3,$4)', [patternId, req.body.lL[i].type, req.body.lL[i].numOfDays, 'Leaves']).then((data) => {
                            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addtion:Salary Pattern','ITR','INFO','Addtion of Salary Pattern(D) Table  successfull','Salary Pattern',true,$1)", userId).then((log) => {})
                        })
                    }
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Salary Pattern Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    var userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addtion:Salary Pattern','ITR','ERR','Addtion of Salary Pattern Table unsuccessfull','Salary Pattern',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to Add Salary Pattern"
                        })
                    })
                })
            })
    })
    //Done
    salaryRouter.get('/Staff/SalaryPattern/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId = '';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from salarypattern where branchid=(select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid($1)))', sessionparam).then(function (data) {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Salary Pattern','ITR','INFO','View of Salary Pattern Table  successfull','Salary Pattern',true,$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "Salary Pattern Retrieved Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Salary Pattern','ITR','ERR','View of Salary Pattern Table unsuccessfull','Salary Pattern',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Salary Pattern"
                        })
                    })
                })
            });
    })
}
//========================================Salary PAyments================================================
{
    //Done
    //Post For SalaryPayment
    salaryRouter.post('/Staff/Salary/Add', (req, res, next) => {
        var sid = parseInt(req.body.staffId);
        var paidamt = req.body.paidAmount;
        var pdate = req.body.payDate;
        pdate = pdate.substring(4, 15)
        var payble = req.body.payable;
        var allwnc = req.body.allowances;
        var ddctn = req.body.deductions;
        var bns = req.body.bonus;
        var payalwdedctndtl = req.body.payAllowanceDeductionDetails
        var rmks = req.body.remarks
        db.any('select * from fn_AddSalaryPayment($1,$2,$3,$4,$5,$6,$7,$8,$9)', [sid, paidamt, pdate, payble, allwnc, ddctn, bns, payalwdedctndtl, rmks]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Salary Payment ','ITR','INFO','Addtion of  Salary Payment Table  successfull','Salary Payment',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Salary Payment Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Salary Payment ','ITR','ERR','Addtion of  Salary Payment Table unsuccessfull','Salary Payment',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Salary Payments"
                    })
                })
            });
    })
    //Done
    //Get By Id For SalaryPayment
    salaryRouter.get('/Staff/Salary/Details/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select * from fn_viewsalarypaymentdetails($1)', i).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Salary Payment ','ITR','INFO','View of  Salary Payment Table  successfull','Salary Payment',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Salary Payment Details Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Salary Payment ','ITR','ERR','View of  Salary Payment Table  unsuccessfull','Salary Payment',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Salary Payment Details"
                    })
                })
            });
    })
    //Done
    //get by staff for salarypayment
    salaryRouter.get('/Staff/Salary/Staff/:Id/:month', (req, res, next) => {
        var i = req.params.Id;
        var month = req.params.month;
        y = month.substring(0, 4);
        m = month.substring(5, 7)
        db.any('select * from fn_viewsalarybystaff($1,$2,$3)', [i, y, m]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Salary Payment ','ITR','INFO','View of  Salary Payment Table  successfull','Salary Payment',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Salary Paymentby Staff Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Salary Payment ','ITR','ERR','View of  Salary Payment Table  unsuccessfull','Salary Payment',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Salary Paymentby Staff "
                    })
                })
            });
    })
    //Done
    //Get For Salary Payment
    salaryRouter.get('/Staff/Salary/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId = '';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewsalarypayment($1)', sessionparam).then((data) => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                    userId = Session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Salary Payment ','ITR','INFO','View of  Salary Payment Table  successfull','Salary Payment',true,$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: " Salary Payment Retrived Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                    userId = Session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Salary Payment ','ITR','ERR','View of  Salary Payment Table  unsuccessfull','Salary Payment',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Salary Payment"
                        })
                    })
                })
            });
    })
}
//=================================================TDS====================================================
{
    //Done
    salaryRouter.post('/salary/tds/Add/:Id', (req, res, next) => {
        var brnid
        var tdsstrtrng = req.body.tdsStartRange;
        var tdsendrng = req.body.tdsEndRange;
        var tdsnme = req.body.tdsName;
        var tdsamt = req.body.tdsAmount;
        var sessionparam = req.params.Id;
        var userId = '';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid($1))', [sessionparam])
            .then((data) => {
                this.brnid = data[0].branchid
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                    userId = session[0].fn_viewuserid;
                    db.any('select * from fn_addtds($1, $2, $3,$4,$5)', [this.brnid, tdsstrtrng, tdsendrng, tdsnme, tdsamt])
                        .then((data) => {
                            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-Addtion: Bus Driver','ITR','INFO','Addtion of  Bus Driver Table  successfull','Bus Driver',true,$1)", userid).then((log) => {
                                if (data.length > 0) {
                                    res.status(200).send({
                                        result: true,
                                        error: "NOERROR",
                                        data: data,
                                        message: "TDS Has Been Added Successfully"
                                    })
                                } else {
                                    res.status(200).send({
                                        result: true,
                                        error: "NOERROR",
                                        data: "NDF",
                                        message: "No Data Found"
                                    })
                                }
                            })
                        })
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                    userId = session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-Addtion: Bus Driver','ITR','ERR','Addtion of  Bus Driver Table  unsuccessfull','Bus Driver',False,$1)", userid).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to Add TDS"
                        })
                    })
                });
            })
    })
    //Done
    salaryRouter.get('/Branch/tax/tds/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId = '';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select  * from fn_viewtds($1)', sessionparam).then((data) => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((sess) => {
                    userId = sess[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Transport stops','ITR','INFO','View of  Transport stops Table  successfull','  Transport stops',true,$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "TDS Details  Retrived Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                    userId = session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Transport stops','ITR','ERR','View of  Transport stops Table unsuccessfull','  Transport stops',False,$1)", userid).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View TDS Details"
                        })
                    })
                });
            });
    })
}
module.exports = salaryRouter;