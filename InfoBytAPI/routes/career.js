var express = require('express');
var bodyparser = require('body-parser');
var payslip = require('../payslip')
var fs = require('fs')
var path = require('path')
var mail = require('../mailer');
var careerRouter = express.Router()
careerRouter.use(express.static(path.resolve(__dirname, "public")));
careerRouter.use(bodyparser.json({
    limit: '5mb'
}));
careerRouter.use(bodyparser.urlencoded({
    limit: '5mb',
    extended: true
}));
var config = require('../init_app');
const dba = require('../db.js');
const db = dba.db;
const pgp = db.$config.pgp;
//ADD Placements
careerRouter.post('/Placement/Trainee/Add', (req, res, next) => {
    var sid = req.body.traineeId;
    var pid = parseInt( req.body.placementId);
    db.any('select * from fn_allocplac($1,$2)', [sid, pid]).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Projects','ITR','INFO','Addtion of Projects Table  successfull','Projects',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Projects  Has Been Added Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Projects','ITR','ERR','Addtion of Projects Table unsuccessfull','Projects',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Add Projects"
                })
            })
        })
})
// Get For AllocatedPlacements
careerRouter.get('/Branch/AllocPlacement/:Id', (req, res) => {
    var sessionparam = req.params.Id;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select * from fn_viewallocatedplacement($1)', sessionparam).then((data) => {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
             userId = session[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Projects','ITR','INFO','View of Projects Fee Table  successfull','Projects',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: " Allocated Placements Retrived  Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Projects','ITR','ERR','View of Projects Table unsuccessfull','Projects',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Allocated Placements"
                    })
                })
            });
        });
})
//Get By Id AllocatedPlacements
careerRouter.get('/Branch/AllocPlacement/Details/:Id', (req, res, next) => {
    var id = req.params.Id;
    db.any('select * from fn_allocplacementbyid($1)', [id]).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-GET:Project','ITR','INFO','GET of Project Table  successfull','Project',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: " Allocated Placements Retrived  Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-GET:Project','ITR','ERR','get of Project Table  unsuccessfull','Project',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to view Allocated Placements "
                })
            })
        });
})
// Put For AllocatedPlacements
careerRouter.post('/Branch/AllocPlacement/Update/:Id', (req, res, next) => {
    var id = req.params.Id;
    var sid = req.body.studentId;
    var pid = req.body.placementId;
    db.any('select * from fn_updateallocplac($1,$2,$3)', [id, sid, pid]).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Projects','ITR','INFO','Update of Projects Table  successfull','Projects',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Session Has Been Added Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Projects','ITR','ERR','Update of Projects Table  unsuccessfull','Projects',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Update Allocated Placements "
                })
            })
        });
})
// ==============================Allocate Project====================================
//Post for Projects
careerRouter.post('/Project/Trainee/Add/', (req, res, next) => {
    console.log(req.body)
var pid=req.body.alocproj.projectId
var selectList=req.body.traineeList
function addProjectAllocation( selectList,pid,callback) {
    for (let i = 0; i < selectList.length; i++) {
     if (selectList[i].checkboxStatus == true) {
         db.any('select * from fn_allocproj($1,$2)', [selectList[i].userid,pid]).then((data) => {
            
         })
         .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Projects','ITR','ERR','Addtion of Projects Table unsuccessfull','Projects',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Add Projects"
                })
            })
        })
         if(i==selectList.length-1)
         {
             callback(true)
         }
     }
 }
    }

    addProjectAllocation(selectList,pid, function (apa) {
    if (data.length > 0) {
        res.status(200).send({
            result: true,
            error: "NOERROR",
            data: apa,
            message: "Session Has Been Added Successfully"
        })
    } else {
        res.status(200).send({
            result: true,
            error: "NOERROR",
            data: "NDF",
            message: "No Data Found"
        })
    }
})
})

//get trainee batch
careerRouter.get('/Trainee/Batch/Project/:Id', (req, res) => {
    var id = req.params.Id;
    db.any('select * from fn_traineebybatch($1)', id).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Projects','ITR','INFO','View of Projects Fee Table  successfull','Projects',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Trainees Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Projects','ITR','ERR','View of Projects Table unsuccessfull','Projects',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Trainees"
                })
            })
        });
})
//get by id for allocatedprojects
careerRouter.get('/Branch/AllocProject/Details/:Id', (req, res, next) => {
    var id = req.params.Id;
    db.any('select * from fn_allocprojectbyid($1)', [id]).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-GET:Project','ITR','INFO','GET of Project Table  successfull','Project',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Project Details retirved  Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-GET:Project','ITR','ERR','get of Project Table  unsuccessfull','Project',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to view Project Details"
                })
            })
        });
})
// Get For AllocatedProjects
careerRouter.get('/Branch/AllocProject/:Id', (req, res) => {
    var sessionparam = req.params.Id;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select * from fn_viewallocatedproject($1)', sessionparam).then((data) => {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
             userId = session[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Projects','ITR','INFO','View of Projects Fee Table  successfull','Projects',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Projects Retrived  Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Projects','ITR','ERR','View of Projects Table unsuccessfull','Projects',False,$1)", userId).then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Projects"
                })
            })
        })
        });
})
// Put For AllocatedProjects
careerRouter.post('/Branch/AllocProject/Update/:Id', (req, res, next) => {
    var id = req.params.Id;
    var sid = req.body.studentId;
    var pid = req.body.projectId;
    db.any('select * from fn_updateallocproj($1,$2,$3)', [id, sid, pid]).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Projects','ITR','INFO','Update of Projects Table  successfull','Projects',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Update Projects  Has Been Added Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Projects','ITR','ERR','Update of Projects Table  unsuccessfull','Projects',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Update Projects "
                })
            })
        });
})
//================================================Job=====================================================
//POST FOR JOB MASTER
//POST FOR JOB MASTER
careerRouter.post('/New/Job/Addtion', (req, res, next) => {
    var jt = req.body.jobTitles;
    var jd = req.body.jobDescription;
    var desg = req.body.designation;
    var minex = req.body.minimumExperience;
    var maxex = req.body.maximumExperience;
    var anc = req.body.anualCtc;
    var os = req.body.otherSalary;
    var vac = req.body.vacancy;
    var loc = req.body.location;
    // var fa = req.body.functionalArea;
    var joi = req.body.jobId;
    var ugq = req.body.ugQualificationId;
    var oops = req.body.pgQualificationId;
    var otq = req.body.otherQualificationId;
    var email = req.body.requiterEmail;
    var ori = req.body.organizationId;
    var abor = req.body.aboutOrganization;
    // var dt = req.body.postingDate;
    img = req.body.image;
    var keywords = []
    for (var i = 0; i < desg.length; i++) {
        keywords.push(desg[i].id)
    }
    console.log(keywords)
    console.log(jt, jd, keywords, minex, maxex, anc, os, vac, loc, joi, ugq, oops, otq, email, ori, abor)
    // console.log("data "+req.body)
    db.any('select * from fn_addjob($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16)', [jt, jd, keywords, minex, maxex, anc, os, vac, loc, joi, ugq, oops, otq, email, ori, abor]).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtions: Job','ITR','INFO','Retrive of Job Table successfull',' Job',true)").then((log) => {
            fn = data[0].fn_addjob + '.png';
            ps = './public/Job/' + fn;
            var dir = './public/' + 'Job';
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }
            fs.writeFile(ps, img, 'base64', (err) => { })
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Job Posted Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            console.log(error.message)
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtions: Job','ITR','ERR','Retrive of Job Table unsuccessfull',' Job',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Post a Job"
                })
            })
        })
});
// update jobmaster
careerRouter.post('/Job/Update/:id', (req, res, next) => {
    var i = req.params.id;
    var jt = req.body.jobTitles;
    var jd = req.body.jobDescription;
    var desg = req.body.designation;
    var minex = req.body.minimumExperience;
    var maxex = req.body.maximumExperience;
    var anc = req.body.anualCtc;
    var os = req.body.otherSalary;
    var vac = req.body.vacancy;
    var loc = req.body.location;
    var fa = req.body.functionalArea;
    var joi = req.body.jobId;
    var ugq = req.body.ugQualificationId;
    var oops = req.body.pgQualificationId;
    var otq = req.body.otherQualificationId;
    var email = req.body.requiterEmail;
    var ori = req.body.organizationId;
    var abor = req.body.aboutOrganization;
    var dt = req.body.postingDate;
    img = req.body.image;
    db.any('select * from fn_updatejob($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19)', [i, jt, jd, desg, minex, maxex, anc, os, vac, loc, fa, joi, ugq, oops, otq, email, ori, abor, dt]).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: job','ITR','INFO','Retrive of job  Table successfull',' job ',true)").then((log) => {
            fn = data[0].fn_updatejob + '.png';
            ps = './public/Job/' + fn;
            var dir = './public/' + 'Job';
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }
            fs.writeFile(ps, img, 'base64', (err) => { })
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Job Updated Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Job','ITR','ERR','Update of job Table unsuccessfull','Job ',false)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Update Job"
                })
            });
        })
})
// sending applicants request
careerRouter.get('/Job/Request/:placementId/:usersession', (req, res, next) => {
    var plcid = req.params.placementId;
    sessionparam = req.params.usersession;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select fn_viewuserid from fn_viewuserid($1)', [sessionparam]).then((dbsession) => {
        userId = dbsession[0].fn_viewuserid;
        db.any('select * from fn_addjobaplicant($1,$2)', [userId, plcid]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Jobs','ITR','INFO','Viewing of Job Table successfull','Job',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Job Requested Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', [sessionparam]).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Jobs','ITR','ERR','Viewing of Jobs Table unsuccessfull','Jobs',false,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Request Job"
                    })
                })
            })
        });
})
careerRouter.get('/Job/Applicants/Deatails/:id', (req, res, next) => {
    var Id = req.params.id;
    db.any('select * from fn_getjobapplicantdetails($1)', Id).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Jobs','ITR','INFO','Viewing of Job Table successfull','Job',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Job Applicant Details Retrieved Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Jobs','ITR','ERR','Viewing of Jobs Table unsuccessfull','Jobs',false)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Job Applicant Details"
                })
            })
        });
})
//get applicant details
careerRouter.get("/view/Branch/Applicants/:ussersession", (req, res, next) => {
    var sessionparam = req.params.ussersession;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select * from fn_getjobapplicants($1)', sessionparam).then((data) => {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
             userId = dbsession[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Applicants','ITR','INFO','Viewing of Applicants Table successfull','Applicants',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Branch Applicants Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Applicants','ITR','ERR','Viewing of Applicants Table unsuccessfull','Applicants',false,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Cannot View Applicants"
                    })
                })
            })
        })
})

// GEtting Requiter Email
careerRouter.get('/Branch/Hr/:Id', (req, res) => {
    var sessionparam = req.params.Id;
    var userId = '';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any("select email from userprofileview where rolename='Hod' and branchid=(select branchid from userprofileview where userid=(select fn_viewuserid from fn_viewuserid($1)))", sessionparam).then((data) => {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
            userId = session[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Projects','ITR','INFO','View of Projects Fee Table  successfull','Projects',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: " Allocated Placements Retrived  Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Projects','ITR','ERR','View of Projects Table unsuccessfull','Projects',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Allocated Placements"
                    })
                })
            });
        });
})


// Accepting aggrements
careerRouter.post('/Update/Applicants/Aggrement/:id', (req, res, next) => {
    var i = req.params.id;
    db.any('select * from fn_updateApplicantAggrement($1)', [i]).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Applicantaggrement','ITR','INFO','Retrive of job  Table successfull',' Applicantaggrement',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Updated Applicants Agreement Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Applicantaggrement','ITR','ERR','Update of job Table unsuccessfull','Applicantaggrement ',false)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Update Applicants Agreement"
                })
            });
        })
})
// Getting status Aggrements
careerRouter.get('/view/Aggrements/Details/:usersession', (req, res, next) => {
    
    var eid = req.params.eventid;
    sessionparam = req.params.usersession;
    var UserId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select fn_viewuserid from fn_viewuserid($1)', [sessionparam]).then((dbsession) => {
        userId = dbsession[0].fn_viewuserid
        db.any('select * from fn_viewapplicantaggrement($1)', [userId]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: Applicants Aggrements','ITR','INFO','View of applicants aggrements Table successfull',' applicants aggrements',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Agreements Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: applicantsaggrements','ITR','ERR','User of applicants aggrements Table unsuccessfull',' applicants aggrements',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: 'Unable to View Applicants Agreements '
                    })
                })
            })
        });
})


// Getting Agreed Candidate
careerRouter.get('/Job/Applicants/OfferLetter/:userSession', (req, res, next) => {
    var sessionparam = req.params.userSession;
    var userId;
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any("select * from applicantsaggrementsview where aggrementstatus='agreed' and branchid=(select branchid from userprofileview where userid=(select fn_viewuserid from fn_viewuserid($1)))",sessionparam).then((data) => {
        userId = data[0].userid;

        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: Job','ITR','INFO','Add of job Table successfull','Job ',true,$1)", userId).then((log) => {

            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Agreed Candidate Retrieved Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        
    })
})
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: Job','ITR','ERR','Add of job Table unsuccessfull','Job ',false,$1)", userId).then((log) => {

                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: 'Unable to View Agreed Candidate'
                })
            
        });
    })
})
careerRouter.post('/Branch/Accept/JobApplicant/:id/:placementsid', (req, res, next) => {
    var i = req.params.id;
    var plc = req.params.placementsid;
    var fromtime = req.body.fromtime;
    var totime = req.body.totime;
    var schedule = req.body.schedule;
    var fromid = req.body.fromid;
    db.any('select * from fn_updatejobApplicantstatus($1,$2)', [i, plc]).then((data) => {
        db.any('insert into interviewschedule (fromtime,totime,schedulerdata,fromid,toid) values($1,$2,$3,$4,$5) ', [fromtime, totime, schedule, fromid, i]).then((data) => {
            mail.letterCallLetter(fromid, i)
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtions: jon Management','ITR','INFO','Retrive of job  Table successfull',' job Management',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Job Updated Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Job','ITR','ERR','Update of job Table unsuccessfull','Job ',false)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Update Job"
                })
            });
        })
})
//  checking job status
careerRouter.get("/Applicants/Job/Status/:ussersession", (req, res, next) => {
    var sessionparam = req.params.ussersession;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select * from fn_getAppliedJobs($1)', sessionparam).then((data) => {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
             userId = dbsession[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Applicants','ITR','INFO','Viewing of Applicants Table successfull','Applicants',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Applicant Job Status Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Applicants','ITR','ERR','Viewing of Applicants Table unsuccessfull','Applicants',false,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Cannot View Applicants Job Status"
                    })
                })
            })
        })
})
// updating interview schedule
careerRouter.post('/User/Accept/InterviewSchedule/:id/:placementsid', (req, res, next) => {
    var i = req.params.id;
    var plc = req.params.placementsid;
    db.any('select * from fn_updatejobApplicantSchedule($1,$2)', [i, plc]).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: jon ','ITR','INFO','Retrive of job  Table successfull',' job ',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Interview Schedule Updated Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Job','ITR','ERR','Update of job Table unsuccessfull','Job ',false)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Update Interview Schedule"
                })
            });
        })
})
//Get for all job from jobmaster
careerRouter.get('/Jobs/All', (req, res, next) => {
    db.any('select * from fn_viewjob()').then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Job','ITR','INFO','Add of job Table successfull','Job ',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Jobs Retrieved Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Job','ITR','ERR','Add of job Table unsuccessfull','Job ',false)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Job"
                })
            });
        })
})
// Get recent job
careerRouter.get('/View/Recent/Job', (req, res, next) => {
    db.any('select * from fn_viewrecentjob()').then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Job','ITR','INFO','Add of job Table successfull','Job ',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Recent Job Retrieved Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Job','ITR','ERR','Add of job Table unsuccessfull','Job ',false)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Job"
                })
            });
        })
})
// Get for all qualification
careerRouter.get('/Qualifiation/All', (req, res, next) => {
    db.any('select * from degree').then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Degree','ITR','INFO','Add of Degree Table successfull','Degree ',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Qualifications Retrieved Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Degree','ITR','ERR','Add of Degree Table unsuccessfull','Degree ',false)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Qualification"
                })
            });
        })
})
//Get Organizatiuon name by ussersession
careerRouter.get('/Organization/:ussersession', (req, res, next) => {
    var sessionparam = req.params.ussersession;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select * from fn_getOrganizationName($1)', sessionparam).then((data) => {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
             userId = dbsession[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Jobs','ITR','INFO','Viewing of Job Table successfull','Job',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Organization Name Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Jobs','ITR','ERR','Viewing of Jobs Table unsuccessfull','Jobs',false,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Organization Name"
                    })
                })
            })
        })
})
careerRouter.get('/Job/Details/:id', (req, res, next) => {
    id = req.params.id;
    db.any('select * from fn_viewjobById($1)', id).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Jobs','ITR','INFO','View of Job Table successfull','Job',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Job Details Retrieved Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Jobs','ITR','ERR','View of Jobs Table unsuccessfull','Jobs',false)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Job Details"
                })
            })
        })
})
// Get All Type Of Job
careerRouter.get('/Job/type', (req, res, next) => {
    db.any('select * from joblist').then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Job','ITR','INFO','Add of job Table successfull','Job ',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Job Type Retrieved Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Job','ITR','ERR','Add of job Table unsuccessfull','Job ',false)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Job Type"
                })
            });
        })
})
// Get job by OrganizationType Id
careerRouter.get('/Job/View/:userSession', (req, res, next) => {
    var sessionparam = req.params.userSession;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select * from fn_viewByOrganizationTypeId($1)', sessionparam).then((data) => {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
             userId = dbsession[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: Job','ITR','INFO','Add of job Table successfull','Job ',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Job Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: Job','ITR','ERR','Add of job Table unsuccessfull','Job ',false,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Job"
                    })
                })
            });
        })
})
// Get Applicants Previous interview  Details
careerRouter.get('/User/Previous/InterviewDetails/:id', (req, res, next) => {
    var Id = req.params.id;
    db.any('select * from fn_getapplicantsprevinterview($1)', Id).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Jobs','ITR','INFO','Viewing of Job Table successfull','Job',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Previous Interview Details Retrieved Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Jobs','ITR','ERR','Viewing of Jobs Table unsuccessfull','Jobs',false)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: 'Unable to View Interview Details'
                })
            })
        });
})
// ==============================Project====================================
//Post for Projects
careerRouter.post('/Branch/Project/Add', (req, res, next) => {
    var bid = req.body.branchId;
    var cname = req.body.projectName;
    var stime = req.body.assignedDate;
    var etime = req.body.submissionDate;
    var type = req.body.type;
    var det = req.body.details;
    var pos = req.body.teamSize;
    var pac = req.body.fee;
    var cate = req.body.category;
    db.any('select * from fn_addprojects($1,$2,$3,$4,$5,$6,$7,$8,$9)', [bid, cname, stime, etime, type, det, pos, pac, cate]).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Projects','ITR','INFO','Addtion of Projects Table  successfull','Projects',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Session Has Been Added Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Projects','ITR','ERR','Addtion of Projects Table unsuccessfull','Projects',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Add Projects"
                })
            })
        })
})
// Get For Projects
careerRouter.get('/Branch/Project/:Id', (req, res) => {
    var sessionparam = req.params.Id;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select * from fn_viewprojects($1)', sessionparam).then((data) => {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
             userId = session[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Projects','ITR','INFO','View of Projects Fee Table  successfull','Projects',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Projects Retrived  Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Projects','ITR','ERR','View of Projects Table unsuccessfull','Projects',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Projects"
                    })
                })
            });
        });
})

// Put For Projects
careerRouter.post('/Branch/Project/Update/:Id', (req, res, next) => {
    var id = req.body.Id;
    var bid = req.body.branchId;
    var cname = req.body.projectName;
    var stime = req.body.assignedDate;
    var etime = req.body.submissionDate;
    var type = req.body.type;
    var det = req.body.details;
    var pos = req.body.teamSize;
    var pac = req.body.fee;
    console.log(req.body)
    console.log(id, bid, cname, stime, etime, type, det, pos, pac)
    db.any('select * from fn_updprojects($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)', [id, bid, cname, stime, etime, type, det, pos, pac,req.body.category]).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Projects','ITR','INFO','Update of Projects Table  successfull','Projects',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Session Has Been Added Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Projects','ITR','ERR','Update of Projects Table  unsuccessfull','Projects',False)").then((log) => {
               console.log(error.message)
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Update Projects "
                })
            })
        });
})

//get by id for projects
careerRouter.get('/Branch/Project/Details/:Id', (req, res, next) => {
    var id = req.params.Id;
    db.any('select * from fn_projectbyid($1)', [id]).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-GET:Project','ITR','INFO','GET of Project Table  successfull','Project',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Session Has Been Added Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-GET:Project','ITR','ERR','get of Project Table  unsuccessfull','Project',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to view Project Details"
                })
            })
        });
})
// ==============================Recruitment======================================================
//adding application for recruitment
careerRouter.post('/Selected/Candidate/Add', (req, res, next) => {
    var jobapplicantid = req.body.jobApplicantId;
    var permencertng = req.body.performanceRating;
    var ontimertng = req.body.onTimeRating;
    var bhvrrtng = req.body.behaviourRating;
    var appearncertng = req.body.appearenceRating;
    var cmmrtng = req.body.communicationRating;
    var intstatus = req.body.interviewStatus;
    db.any('select * from fn_addapplicantperformence($1,$2,$3,$4,$5,$6,$7)', [jobapplicantid, permencertng, ontimertng, bhvrrtng, appearncertng, cmmrtng, intstatus]).then((Id) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition: Recruitment','ITR','INFO','Addition   of  Recruitment Table  successfull',' Recruitment',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Interview Candidate Details  Has Been Added Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    }).catch(error => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Recruitment','ITR','ERR','Addtion of Recruitment Table unsuccessfull','Recruitment',False)").then((log) => {
            res.status(200).send({
                result: false,
                error: error.message,
                data: "ERROR",
                message: "Unable to Add Interview Candidate Details"
            })
        })
    })
})
//get By id applicants for recruitment
careerRouter.get('/Recruitments/Details/:id', (req, res) => {
    var id = req.params.id
    db.any('select * from fn_viewrecruitmentsdetails($1)', id).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Recruitment','ITR','INFO','View of Recruitment Table  successfull','Recruitment',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Recruitment Details Retrived  Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Recruitment','ITR','ERR','View of Recruitment Table unsuccessfull','Recruitment',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to  View Recruitment Details"
                })
            })
        });
})
//Post for requestRecruitment
careerRouter.post('/Recruitment/Request/Add/:Id', (req, res, next) => {
    var cmpid;
    var uid = req.body.userId
    var pstn = req.body.Position;
    var pkg = req.body.Package;
    var dtls = req.body.Details;
    var intvdt = req.body.interviewDate;
    var intvtim = req.body.interviewTime;
    var adrs = req.body.Address;
    var sta = "Pending";
    var sessionparam = req.params.Id;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select Organizationid from userprofileview where id=(select fn_viewuserid from fn_viewuserid($1))', sessionparam).then((data) => {
        this.cmpid = data[0].Organizationid
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
             userId = session[0].fn_viewuserid;
            db.any('select * from fn_addrequestrecruitments($1,$2,$3,$4,$5,$6,$7,$8,$9)', [this.cmpid, uid, pstn, pkg, dtls, intvdt, intvtim, adrs, sta]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status.userId) values ('Table-Addtion:requestRecruitment','ITR','INFO','Addtion of Projects Table  successfull','Projects',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Recruitment Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-Addtion:requestRecruitment','ITR','ERR','Addtion of Projects Table unsuccessfull','Projects',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Request  Recruitment"
                    })
                })
            });
        })
})
//get Notifications for recruitment
careerRouter.get('/Request/Recruitment/Notification/:Id', (req, res) => {
    var sessionparam = req.params.Id;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select * from fn_viewrequestrecruitment($1)', sessionparam).then((data) => {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
             userId = session[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:requestrecruitments','ITR','INFO','View of Projects Fee Table  successfull','Projects',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: " Recruitment Notifications  Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:requestrecruitments','ITR','ERR','View of Projects Table unsuccessfull','Projects',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Recruitment Notifications"
                    })
                })
            });
        });
})
//requestrecruitmeny get by id
careerRouter.get('/Request/Recruitment/Details/:Id', (req, res) => {
    var id = req.params.Id;
    db.any('select * from fn_viewrequestrecruitmentById($1)', id).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:requestrecruitments','ITR','INFO','View of Projects Fee Table  successfull','Projects',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Recruitment Details Retrived Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:requestrecruitments','ITR','ERR','View of Projects Table unsuccessfull','Projects',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Request  Recruitment Details"
                })
            })
        });
})
//  Applicant/Ratings/
careerRouter.get('/Applicants/Ratings/:Id', (req, res) => {
    var sessionparam = req.params.Id;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any("select * from fn_getapplicantsrating($1)", sessionparam).then((data) => {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
             userId = session[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:requestrecruitments','ITR','INFO','View of Projects Fee Table  successfull','Projects',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Request Recruitments Retrived  Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,,userId) values ('Table-View:requestrecruitments','ITR','ERR','View of Projects Table unsuccessfull','Projects',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Request Recruitments"
                    })
                })
            });
        });
})
//specific user rating
careerRouter.get('/Specific/Applicant/Ratings/:Id', (req, res) => {
    var sessionparam = req.params.Id;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any("select * from fn_getapplicantsratingbyid($1)", sessionparam).then((data) => {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
             userId = session[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:requestrecruitments','ITR','INFO','View of Projects Fee Table  successfull','Projects',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: " Request Recruitments Retirived  Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:requestrecruitments','ITR','ERR','View of Projects Table unsuccessfull','Projects',False,$1)", userId).then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Request Recruitments"
                })
            })
        });
    })
})
//Update for requestrecruitment
careerRouter.post('/Request/Recruitment/Update/:Id', (req, res, next) => {
    var id = req.params.Id;
    var st = req.body.Status;
    db.any('select * from fn_updaterequestrecruitments($1,$2)', [id, st]).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:requestrecruitments','ITR','INFO','Addtion of Projects Table  successfull','Projects',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Session Has Been Added Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:requestrecruitments','ITR','ERR','Addtion of Projects Table unsuccessfull','Projects',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Update Request Recruitment "
                })
            })
        })
})
//tO SENT LOI
careerRouter.post('/Recruitment/Loi/Send/:uId/:id', (req, res, next) => {
    mail.letterIntend(req.params.uId, req.params.id, function (data) {
        if (data) {
            id = req.params.id
            db.any("update requestrecruitments set loimailstatus='Sent' where id=$1", id).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Payslip','ITR','INFO','Addition of Payslip Table  successfull','Payslip',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Session Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        } else {
            id = req.params.id
            db.any("update requestrecruitments set loimailstatus='Inactive' where userid=$1", id).then((data) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Letter Of Intent Could't Sent"
                })
            });
        }
    })
})
careerRouter.get('/Applicants/Attend/Interview/:Id', (req, res) => {
    var sessionparam = req.params.Id;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any("select * from fn_getjobapplicantsSchedule($1)", sessionparam).then((data) => {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
             userId = session[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:requestrecruitments','ITR','INFO','View of Projects Fee Table  successfull','Projects',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Job Applicants Retrived  Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:requestrecruitments','ITR','ERR','View of Projects Table unsuccessfull','Projects',False.$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Job Applicants"
                    })
                })
            });
        });
})
//Accept Loi by userid where status is sent
careerRouter.post('/Recruitment/Loi/Accept/:uId/:id/:status', (req, res) => {
    var id = req.params.id
    var sessionparam = req.params.uId;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    var st = req.params.status
    db.any("update requestrecruitments set loistatus=$3 where id=$1 and userid=(select fn_viewuserid from fn_viewuserid($2))", [id, sessionparam, st]).then((data) => {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
             userId = session[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:requestrecruitments','ITR','INFO','View of Projects Fee Table  successfull','Projects',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Intent Acceptance  Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:requestrecruitments','ITR','ERR','View of Projects Table unsuccessfull','Projects',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to  Add Letter Of Intent Acceptance"
                    })
                })
            });
        });
})
//getAll Intrest candidates    Applicants/Ratings/
careerRouter.get('/Recruitment/Intrest/Details/:Id', (req, res) => {
    var sessionparam = req.params.Id;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any("select * from requestrecruitmentsview where status='Intrested' and companyid=(select Organizationid from userprofileview where id=(select fn_viewuserid from fn_viewuserid($1)))", sessionparam).then((data) => {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
             userId = session[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:requestrecruitments','ITR','INFO','View of Projects Fee Table  successfull','Projects',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Request Recruitments Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:requestrecruitments','ITR','ERR','View of Projects Table unsuccessfull','Projects',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Request Recruitments"
                    })
                })
            });
        });
})
//Documents verification
careerRouter.post('/Recruitment/Doc/Verification/', (req, res) => {
    for (var i = 0; i < req.body.length; i++) {
        db.any("update requestrecruitments set docverificationstatus=$2 where id=$1", [req.body[i].id, req.body[i].docverificationstatus]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:requestrecruitments','ITR','INFO','View of Projects Fee Table  successfull','Projects',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Recruitments Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:requestrecruitments','ITR','ERR','View of Projects Table unsuccessfull','Projects',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Recruitments"
                    })
                })
            });
    }
})
//tO SENT Offer Letter
careerRouter.post('/Recruitment/OfferLetter/Send/:id/:uid', (req, res, next) => {
    var appid = req.params.id
    var userid = req.params.uid
    db.any("update  applicantsaggrements set  aggrementstatus='Offer letter sent' where id=$1", appid).then((data) => {
        mail.letterOffer(appid, userid)
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:requestrecruitments','ITR','INFO','View of Projects Fee Table  successfull','Projects',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Offer Letter Has Been Added Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:requestrecruitments','ITR','ERR','View of Projects Table unsuccessfull','Projects',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Send Offer Letter"
                })
            })
        });
})

careerRouter.post('/Branch/Applicant/Send/Aggrements', (req, res, next) => {
    var ai = req.body.applicantId;
    var Sh = req.body.shiftId;
    var An = req.body.annualCTC;
    var Dte = req.body.doj;
    db.any('select * from fn_addapplicantsaggrements($1,$2,$3,$4)', [ai, An, Sh, Dte]).then((Id) => {
        mail.letterIntend(ai, req.body.recruitId)
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition: Recruitment','ITR','INFO','Addition   of  Recruitment Table  successfull',' Recruitment',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Session Has Been Added Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    }).catch(error => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Recruitment','ITR','ERR','Addtion of Recruitment Table unsuccessfull','Recruitment',False)").then((log) => {
            res.status(200).send({
                result: false,
                error: error.message,
                data: "ERROR",
                message: "Unable to send Aggremet"
            })
        })
    })
})
careerRouter.post('/Recruitment/Job/Acceptance/Send/:Id', (req, res, next) => {
    var id = req.params.Id;
    db.any('select * from fn_updateaplicantsaggrements($1)', id).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:requestrecruitments','ITR','INFO','Addtion of Projects Table  successfull','Projects',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Session Has Been Added Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:requestrecruitments','ITR','ERR','Addtion of Projects Table unsuccessfull','Projects',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to update Request Recruitment "
                })
            })
        })
})
//=========================================== Placements===========================================
//Post for Placements
careerRouter.post('/Branch/Placement/Add', (req, res, next) => {
    var bid = req.body.branchId;
    var cname = req.body.name;
    var jid=req.body.jobId;
    var stime = req.body.startTime;
    var etime = req.body.endTime;
    var type = req.body.designation;
    var det = req.body.details;
    var pos = req.body.positions;
    var pac = req.body.package;
    db.any('select * from fn_addplacements($1,$2,$3,$4,$5,$6,$7,$8,$9)', [bid, jid, stime, etime, type, det, pos, pac,cname]).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Placements','ITR','INFO','Addtion of Placements Table  successfull','Placements',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Session Has Been Added Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Placements','ITR','ERR','Addtion of Placements Table unsuccessfull','Placements',False)").then((log) => {
                res.status(404).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Add Placements"
                })
            })
        })
})
// Get For Placements
careerRouter.get('/Branch/Placements/:Id', (req, res) => {
    var sessionparam = req.params.Id;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select * from fn_viewplacements($1)', sessionparam).then((data) => {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
             userId = dbsession[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Placements','ITR','INFO','View of Placements Fee Table  successfull','Placements',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Session Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        });
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Placements','ITR','ERR','View of Placements Table unsuccessfull','Placements',False,$1)".userId).then((log) => {
                    res.status(404).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Placements"
                    })
                })
            });
        });
})
//get by id for placements
careerRouter.get('/Branch/Placement/Details/:Id', (req, res, next) => {
    var id = req.params.Id;
    db.any('select * from fn_placementbyid($1)', [id]).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-GET:Placements','ITR','INFO','GET of Placements Table  successfull','Placements',true)").then((log) => {
            if(data.length>0){
                res.status(200).send({
                    result:true,
                    error:"NOERROR",
                    data:data,
                    message: "Placement Retrived  Successfully"
                })
            }else{
                res.status(200).send({
                    result:true,
                    error:"NOERROR",
                    data:"NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-GET:Placements','ITR','ERR','GET of Placements Table  unsuccessfull','Placements',False)").then((log) => {
                res.status(404).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to view Placement "
                })
            })
        });
})
// Put For placements
careerRouter.put('/Branch/Placement/Update/:Id', (req, res, next) => {
    var id = req.params.Id;
    var bid = req.body.branchId;
    var cname = req.body.companyName;
    var stime = req.body.startTime;
    var etime = req.body.endTime;
    var type = req.body.designation;
    var det = req.body.details;
    var pos = req.body.positions;
    var pac = req.body.package;
    db.any('select * from fn_updplacements($1,$2,$3,$4,$5,$6,$7,$8,$9)', [id, bid, cname, stime, etime, type, det, pos, pac]).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Placements','ITR','INFO','Update of Placements Table  successfull','Placements',true)").then((log) => {
            if(data.length>0){
                res.status(200).send({
                    result:true,
                    error:"NOERROR",
                    data:data,
                    message: "Update Placements Has Been Added Successfully"
                })
            }else{
                res.status(200).send({
                    result:true,
                    error:"NOERROR",
                    data:"NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Placements','ITR','ERR','Update of Placements Table  unsuccessfull','Placements',False)").then((log) => {
                res.status(404).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Update Placements "
                })
            })
        });
})
module.exports = careerRouter;
