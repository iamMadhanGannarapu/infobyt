var express = require('express');
var fileRouter = express.Router();
var multer = require('multer');
var walk = require('fs-walk');
var path = require('path')
var config = require('../init_app');
var fs = require('fs');
var upload;
var uploads;
fileRouter.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', config.angular);
    res.setHeader('Access-Control-Allow-Methods', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});
const dba=require('../db.js');
const db = dba.db;
var images = [];
fileRouter.post('/Add/:Id', function (req, res, next) {
    var userId = req.params.Id;
    store = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, './public/uploads/' + userId);
        },
        filename: function (req, file, cb) {
            cb(null, file.originalname);
        }
    });
    upload = multer({
        storage: store
    }).single('file');
    var dir = './public/uploads/' + userId;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    upload(req, res, function (err) {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Uploads','ITR','INFO','Addition of Uploads Table successfull','Uploads',true)");
    })
});
fileRouter.post('/upload/:Id/:sessId', (req, res, next) => {
    var userId = req.params.Id
    var branchid;
    var description = req.params.sessId;
    db.any("select branchid from userprofileview where id=" + userId).then((data) => {
        branchid = data[0].branchid
        description = branchid + '_' + req.params.sessId;
        db.any('select * from fn_addgallery($1,$2,CURRENT_DATE)', [branchid, description]).then((Id) => {
            fn = Id[0].fn_addgallery;
            stores = multer.diskStorage({
                destination: function (req, file, cb) {
                    cb(null, './public/gallery/' + branchid + '/' + fn);
                },
                filename: function (req, file, cb) {
                    cb(null, file.originalname);
                }
            });
            uploads = multer({
                storage: stores
            }).single('file');
            var dire = './public/' + 'gallery/';
            if (!fs.existsSync(dire)) {
                fs.mkdirSync(dire);
            }
            var dir = './public/gallery/' + branchid
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }
            var dirs = './public/gallery/' + branchid + '/' + fn
            if (!fs.existsSync(dirs)) {
                fs.mkdirSync(dirs);
            }
            uploads(req, res, function (err) {
                if (err) {
                    return json({
                        error: err
                    });
                }
                res.status(200).send(Id);
            });
        })
    })
})

fileRouter.get('/Gallery/album/images/:usersession/:dir', (req, res, next) => {
    images = [];
    var dir = req.params.dir;
    sessionparam = req.params.usersession;
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any("select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid($1))", [sessionparam]).then((data) => {
        branchid = data[0].branchid
        const directoryPath = path.resolve(__dirname, '../public/gallery');
        var filePath = directoryPath + '/' + branchid + '/' + dir;
        for (var i = 0; i < filePath.length; i++) {
            filePath = filePath.replace(/\\/g, '/');
        }
        walk.walk(filePath, function (basedir, filename, stat, next) {
            var perm = stat.isDirectory() ? 0755 : 0644;
            var d = config.server+'/gallery/' + branchid + '/' + path.basename(basedir) + '/' + filename;
        //    console.log("{source:'" + d + "', alt:'Error', title:'Title 1'}");
            var img='{"source":"' + d + '", "alt":"'+filename+'", "title":"'+filename+'"}';
            images.push(JSON.parse(img));
            fs.chmod(path.join(basedir, filename), perm, next);
        }, function (err) {
          // console.log('no error'+err)
            if (err == null)
                res.send(images);
            else
                console.log(err)
        });
    })
})



//Delete Gallery
fileRouter.delete('/Gallery/delete/:userSession/:Id', (req, res, next) => {
    var galleryId = req.params.Id
    var filePath
    var replacePath
    sessionparam = req.params.userSession;
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any("select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid($1))", [sessionparam]).then((data) => {
        branchid = data[0].branchid
    const directoryPath = path.resolve(__dirname, '../public/gallery/' + branchid + "/" +galleryId);
    replacePath= directoryPath
    for (var i = 0; i < replacePath.length; i++) {
        replacePath = replacePath.replace(/\\/g, '/');
    }
    try {
        fs.readdir(replacePath, 'utf8', (err, files) => {
            if(files!=undefined){
            for (var i = 0; i < files.length; i++) {
                filePath= directoryPath+'/'+files[i]
                fs.unlink(filePath, (err) => {

                })
            }
        fs.rmdir(directoryPath, (err) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Delete uploads','ITR','INFO','Delete of File Table successfull','Deleted File',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Uploads Has Been Deleted succesfully "
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    }
    })
    }

    
    catch (err) {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Delete uploads','ITR','ERR',Delete of File Table successfull','Deleted File',false)").then((log) => {
            res.status(200).send({
                result: false,
                error: error.message,
                data: "ERROR",
                message: false
            });
        })
    }
})
})

 //get Videos
 fileRouter.get('/Public/Video/:Id/:flder/:Name', (req, res) => {
    var nme = req.params.Name;
    var i = req.params.Id;
    var folder = req.params.flder;
    var branchId;
  // console.log(nme + i+ folder)
    db.any("select * from userprofileview where id=$1", [i]).then((data) => {
        branchId = data[0].branchid
    var fn = '../public/gallery/' + branchId + '/' +folder+'/'+ nme;
    res.status(200).sendFile(path.resolve(__dirname, fn))
    })
})

//get Gallery
fileRouter.get('/Gallery/Images/:usersession', (req, res, next) => {
    var images = [];
    var list
    var albumName;
    var z = 0;
    var childData = [];
    sessionparam = req.params.usersession;
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any("select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid($1))", [sessionparam]).then((data) => {
            branchid = data[0].branchid
            db.any("select description as label from galleryview where branchId=" + branchid).then((albums) => {
                const directoryPath = path.resolve(__dirname, '../public/gallery/' + branchid);
                walk.walk(directoryPath, function (basedir, filename, stat, next) {
                    var perm = stat.isDirectory() ? 0755 : 0644;
                    if (!stat.isDirectory()) {
                        if (albumName == path.basename(basedir)) {
                            childData[z - 1].children.push({
                                "label": filename,
                                "icon": "fa fa-file-image-o",
                                "data": path.basename(basedir)
                            })
                        } else {
                            albumName = path.basename(basedir)
                            childData.push({
                                "label": path.basename(basedir),
                                "data": path.basename(basedir),
                                "expandedIcon": "fa fa-folder-open",
                                "collapsedIcon": "fa fa-folder",
                                "children": [{
                                    "label": filename,
                                    "icon": "fa fa-file-image-o",
                                    "data": path.basename(basedir)
                                }]
                            })
                            z = z + 1;
                        }
                    }
                    fs.chmod(path.join(basedir, filename), perm, next);
                }, function (err) {
                    if (err == null)
                        res.send(childData);
                    else
                    console.log(err)
                });
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Gallery','ITR','ERR','View of Gallery Table unsuccessfull','Gallery',False)").then((d) => {
                res.status(404).send({
                    message: "Unable to View Gallery"
                })
            })
        });
})
module.exports = fileRouter