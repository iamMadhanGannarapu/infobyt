var express = require('express');
var bodyparser = require('body-parser');
var payslip = require('../payslip')
var fs = require('fs')
var path = require('path')
var mail = require('../mailer');
var examRouter = express.Router()
examRouter.use(express.static(path.resolve(__dirname, "public")));
examRouter.use(bodyparser.json({
    limit: '5mb'
}));
examRouter.use(bodyparser.urlencoded({
    limit: '5mb',
    extended: true
}));
var config = require('../init_app');
const dba = require('../db.js');
const db = dba.db;
const pgp = db.$config.pgp;
examRouter.get('/Session/Appraisal/TimeTable/:userSession', (req, res, next) => {
    var sessionparam = req.params.userSession;
    var userId = '';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select  * from fn_viewappraisalTimeTables($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                userId = dbSession[0].fn_viewuserid;
                console.log(data)
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:  Time Table','ITR','INFO','Retrive   of  appraisal  successfull','  appraisal',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: " Appraisal Time Table Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:  Time Table','ITR','ERR','Retrive   of  appraisal  unsuccessfull','  appraisal',False,$1)", userId).then((log) => {
                    console.log('ERROR:Unable to View Appraisal Time Table', error.message || error);
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Appraisal Time Table"
                    })
                })
            })
        });
})
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
examRouter.post('/Session/Appraisal/TimeTable/Add', (req, res, next) => {
    var clsbjid = req.body.sessionSubjectId;
    var srtm = req.body.examStartDateTime;
    var et = req.body.examEndDateTime;
    var en = req.body.name;
    var stat = 'Active';
    var examid = req.body.examId;
    var tm = req.body.totalMarks;
    var file = req.body.fileUpload;
    db.any('select * from fn_addappraisaltimetable($1, $2, $3,$4,$5,$6,$7)', [clsbjid, srtm, et, en, stat, examid, tm, file])
        .then((Id) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:  Time Table','ITR','INFO','Addition   of  appraisal  successfull','  appraisal',true)").then((log) => {
                res.status(200).send(Id);
                var fn = Id[0].fn_addappraisaltimetable + '.pdf';
                var dire = './public/' + 'modelpaper'
                if (!fs.existsSync(dire)) {
                    fs.mkdirSync(dire);
                }
                fs.writeFile('./public/modelpaper/' + fn, file, (err) => {})
                res.status(200).send({
                    message: " Appraisal Timetable Added Successfully"
                })
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:  Time Table','ITR','ERR','Addition   of  appraisal  unsuccessfull','  appraisal',False)").then((log) => {
                console.log('ERROR:Unable to View Appraisal Time Table', error.message || error);
                res.status(200).send({
                    message: "Unable to View Appraisal Time Table"
                })
            })
        });
})
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
examRouter.get('/Session/Appraisal/TimeTable/:userSession', (req, res, next) => {
    var sessionparam = req.params.userSession;
    var userId = '';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select  * from fn_viewappraisalTimeTables($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:  Time Table','ITR','INFO','Retrive   of  appraisal Time Table  successfull','  appraisal Time table',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: " Appraisal Time Table Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:  Time Table','ITR','ERR','Retrive   of  appraisal Time Table  unsuccessfull','  appraisal Time table',False,$1)", userId).then((log) => {
                    console.log('ERROR:Unable to View Appraisal Time Table', error.message || error);
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Appraisal Time Table"
                    })
                })
            })
        });
})
//++++++++++++++++++++++++++++++++
examRouter.post('/notification/appraisalTimeTable/add/:Id/:appraisaltimetableId', (req, res, next) => {
    sessionparam = req.params.Id
    var notification = "  appraisal Timetable has been added ";
    var rcvr = '';
    var notificationid = 0;
    ettid = req.params.appraisaltimetableId
    var classsubjectId;
    db.any('select classsubjectid from appraisaltimetable where id=$1', ettid).then((data) => {
            classsubjectId = data[0].classsubjectid
            var userId = '';
            for (var i = 0; i < sessionparam.length; i++) {
                sessionparam = sessionparam.replace('-', '/');
            }
            db.any('select * from fn_viewappraisaltimetablereceiverIds($1,$2)', [sessionparam, classsubjectId]).then((data) => {
                rcvr = data;
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid;
                    db.any(' insert into notifications(notification) values ($1) returning id', notification).then((data) => {
                        notificationid = data[0].id;
                        for (var j = 0; j < rcvr.length; j++) {
                            db.any('select * from fn_addnotificationdetails($1,$2,$3,$4)', [rcvr[j].rolename, rcvr[j].userid, notificationid, sessionparam])
                                .then((data) => {
                                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','INFO','Addition of Notifications Table  successfull','Notifications',true,$1)", userId).then((log) => {
                                        if (data.length > 0) {
                                            res.status(200).send({
                                                result: true,
                                                error: "NOERROR",
                                                data: data,
                                                message: "Timetable Notification Added Successfully"
                                            })
                                        } else {
                                            res.status(200).send({
                                                result: true,
                                                error: "NOERROR",
                                                data: "NDF",
                                                message: "No Data Found"
                                            })
                                        }
                                    })
                                })
                        }
                    })
                })
            })
        })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                var userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','ERR','Addition of Notifications Table unsuccessfull','Notifications',False,$1)", userId).then((log) => {
                    console.log('ERROR:Unable to Add Notifications', error.message || error);
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Notifications"
                    })
                })
            })
        })
})
//+++++++++++++++++++++++++++++++++++++++
examRouter.get('/Trainee/Batch/Project/:Id', (req, res) => {
    var id = req.params.Id;
    db.any('select * from fn_traineebybatch($1)', id).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Projects','ITR','INFO','View of Projects Fee Table  successfull','Projects',true)").then((log) => {
                res.status(200).send(data);
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Projects','ITR','ERR','View of Projects Table unsuccessfull','Projects',False)").then((log) => {
                console.log('ERROR:Unable to View Projects', error.message || error);
                res.status(200).send({
                    message: "Unable to View Projects"
                })
            })
        });
})
//+++++++++++++++++++++++++++++++++++++++++
examRouter.get('/Session/Appraisal/TimeTable/Details/:Id', (req, res, next) => {
    var i = req.params.Id;
    db.any('select  * from fn_viewappraisalTimeTableDetails($1)', i).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:  Time Table','ITR','INFO','Retrive   of  appraisal Time Table  successfull','  appraisal',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Appraisal Time Table Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:  Time Table','ITR','ERR','Retrive   of  appraisal unsuccessfull','  appraisal',False)").then((log) => {
                console.log('ERROR:Unable to View Appraisal Time Table', error.message || error);
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Appraisal Time Table"
                })
            })
        });
})
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
examRouter.post('/Session/Appraisal/TimeTable/Update/:Id', (req, res, next) => {
    var examid = req.params.Id;
    var clsbid = req.body.classSubjectId;
    var examdt = req.body.examDate;
    var startt = req.body.startTime;
    var endt = req.body.endTime;
    var examn = req.body.name;
    var stat = 'Active'
    db.any('select * from fn_Updateappraisaltimetable($1,$2,$3,$4,$5,$6,$7)', [examid, clsbid, examdt, startt, endt, examn, stat]).then(() => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:  Time Table','ITR','INFO','Update  of  Appraisal Time Table  successfull','  Appraisal Time table',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Appraisal Timetable Updated Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:  Time Table','ITR','ERR','Update  of  Appraisal Time Table  unsuccessfull','  Appraisal Time table',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Cannot Update Appraisal Time Table"
                })
            })
        });
})
//+++++++++++++++++++++++++++++++++++++++++++
examRouter.get('/Branch/Session/Subject/Details/:Id', (req, res) => {
    var i = req.params.Id
    db.any('select * from sessionsubjectview where branchsessionid= $1', i).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: session Subject','ITR','INFO','View of session Subject Table  successfull','  session Subject',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Session Subject Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: session Subject','ITR','ERR','View of session Subject Table  unsuccessfull','  session Subject',False)").then((log) => {
                console.log('ERROR:Unable to View Session Subject ', error.message || error);
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Session Subject "
                })
            })
        });
})
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
examRouter.get('/Userid/:userSession', function (req, res, next) {
    var sessionparam = req.params.userSession;
    var userId = '';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any("select * from fn_viewuserid($1)", sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Login','ITR','INFO','View of LOGIN Table successfull','Login',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Userid by Session Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "NO DATA FOUND"
                        })
                    }
                })
            })
        })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Login','ITR','ERR','View of LOGIN Table unsuccessfull','Login',False,$1)", userId).then((log) => {
                    console.log('ERROR:Unable to View Userid by session', error.message || error);
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: " Unable to View Userid by session"
                    });
                });
            })
        })
});
//++++++++++++++++++++++++++++++++++++++++++++++++++++++
examRouter.post('/Trainee/Marks/Add', (req, res, next) => {
    var stdid = req.body.traineeId;
    var exmid = req.body.examId;
    var subId = req.body.subjectId;
    var mk = req.body.marks;
    db.any('select * from fn_AddtraineeMarks($1,$2,$3,$4)', [stdid, subId, exmid, mk])
        .then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: trainee Marks','ITR','INFO','Addtion of  trainee Marks Table  successfull','  trainee Marks',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Trainee Marks Posted"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: trainee Marks','ITR','ERR','Addtion of  trainee Marks Table unsuccessfull','  trainee Marks',False)").then((log) => {
                console.log('ERROR:Unable to Add Trainee Marks', error.message || error);
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Add Trainee Marks"
                })
            })
        });
})
//++++++++++++++++++++++++++++++++++++++++
examRouter.post('/notification/traineemarks/add/:Id/:traineemarksid', (req, res, next) => {
    sessionparam = req.params.Id
    var notification = " A new marks report   ";
    var rcvr = '';
    var notificationid = 0;
    smid = req.params.traineemarksid
    var traineemarkstraineeid;
    var userId = '';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select traineeid from traineemarks where id=$1', smid).then((data) => {
            traineemarkstraineeid = data[0].traineeid
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid;
                db.any('select * from fn_viewFeedBackreceiverids($1,$2)', [sessionparam, traineemarkstraineeid]).then((data) => {
                    rcvr = data;
                    db.any(' insert into notifications(notification) values ($1) returning id', notification).then((data) => {
                        notificationid = data[0].id;
                        for (var j = 0; j < rcvr.length; j++) {
                            db.any('select * from fn_addnotificationdetails($1,$2,$3,$4)', [rcvr[j].rolename, rcvr[j].userid, notificationid, sessionparam])
                                .then((data) => {
                                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','INFO','Addition of Notifications Table  successfull','Notifications',true,$1)", userId).then((log) => {
                                        if (data.length > 0) {
                                            res.status(200).send({
                                                result: true,
                                                error: "NOERROR",
                                                data: data,
                                                message: "Trainee Notifications Added Successfully"
                                            })
                                        } else {
                                            res.status(200).send({
                                                result: true,
                                                error: "NOERROR",
                                                data: "NDF",
                                                message: "No Data Found"
                                            })
                                        }
                                    })
                                })
                        }
                    })
                })
            })
        })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','ERR','Addition of Notifications Table unsuccessfull','Notifications',False,$1)", userId).then((log) => {
                    console.log('ERROR:Unable to Add Notifications', error.message || error);
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Trainee Notifications"
                    })
                })
            })
        })
})
//+++++++++++++++++++++++++++++++++++
examRouter.get('/Trainee/Marks/:Id', (req, res, next) => {
    var sessionparam = req.params.Id;
    var userId = '';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select  * from fn_viewtraineemarks($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: trainee Marks','ITR','INFO','View of  trainee Marks Table  successfull','  trainee Marks',true,$1)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Trainee Marks Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: trainee Marks','ITR','ERR','View of  trainee Marks Table  unsuccessfull','  trainee Marks',False,$1)",userId).then((log) => {
                    console.log('ERROR:Unable to View Trainee Marks', error.message || error);
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Trainee Marks"
                    })
                })
            })
        });
})
//+++++++++++++++++++++++++++++++++++++++++++++++++++
examRouter.get('/Permissions/ByRole/:rolename', (req, res, next) => {
    var rolename = req.params.rolename;
    db.any('select * from fn_viewpermissionsbyroles($1)', rolename).then(function (data) {
            var userId = data[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view: Permissions','ITR','INFO','Retrive of Permissions Table successfull','Permissions',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: " Permissions by role Alloacted Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view: Permissions','ITR','ERR','Retrive of Permissions Table unsuccessfull','Permissions',False,$1)", userId).then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "error",
                    message: 'Unable to view Permissions by role'
                })
            })
        });
})
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
examRouter.get('/Trainee/Marks/Details/:Id', (req, res, next) => {
    var i = req.params.Id;
    db.any('select  * from fn_viewtraineemarksdetails($1)', [i]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: trainee Marks','ITR','INFO','View of  trainee Marks Table  successfull','  trainee Marks',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: " Trainee Marks Details Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: trainee Marks','ITR','ERR','View of  trainee Marks Table  unsuccessfull','  trainee Marks',False)").then((log) => {
                console.log('ERROR:Unable to View Trainee Marks Details', error.message || error);
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Trainee Marks Details"
                })
            })
        });
})
//++++++++++++++++++++++++++++++++++++++++++
examRouter.post('/Trainee/Marks/Update/:Id', (req, res, next) => {
    var traineemarksid = req.params.Id;
    var stdid = req.body.traineeId;
    var exmid = req.body.examId;
    var mk = req.body.marks;
    db.any('select * from fn_UpdatetraineeMarks($1,$2,$3,$4)', [traineemarksid, stdid, exmid, mk])
        .then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: trainee Marks','ITR','INFO','Update of  trainee Marks Table  successfull','  trainee Marks',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: traineemarksid,
                        message: "trainee Marks Has Been Updated Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: trainee Marks','ITR','ERR','Update of  trainee Marks Table  unsuccessfull','  trainee Marks',False)").then((log) => {
                console.log('ERROR:Cannot Update trainee Marks', error.message || error);
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Cannot Update trainee Marks"
                })
            })
        });
});
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
examRouter.get('/Batch/traineesdetails/Details/:Id', (req, res) => {
    var i = req.params.Id
    db.any('select * from traineebatchview where batchid = $1   order by firstname asc;', i).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: trainee Batch','ITR','INFO','View of  trainee Batch Table  successfull',' trainee Batch',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Trainee Batch Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: trainee Batch','ITR','ERR','View of  trainee Batch Table  unsuccessfull',' trainee Batch',False)").then((log) => {
                console.log('ERROR:Unable to View Trainee Batch', error.message || error);
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Trainee Batch"
                })
            })
        });
})
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++
examRouter.get('/Batch/traineeappraisals/Details/:Id', (req, res) => {
    var i = req.params.Id
    db.any('select * from appraisaltimetableview where appraisalid=$1', i).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Appraisaltimetable','ITR','INFO','View of  Appraisaltimetable Table  successfull',' Appraisaltimetable',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Appraisal Time Table Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Appraisaltimetable','ITR','ERR','View of  Appraisaltimetable Table  unsuccessfull',' Appraisaltimetable',False)").then((log) => {
                console.log('ERROR:Unable to View Appraisal Time Table', error.message || error);
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Appraisal Time Table"
                })
            })
        });
})
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
examRouter.get('/Batch/traineeappraisals/subject/Details/:Id', (req, res) => {
    var i = req.params.Id
    db.any('select * from appraisaltimetableview where id=$1', i).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Appraisaltimetable','ITR','INFO','View of  Appraisaltimetable Table  successfull',' Appraisaltimetable',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Appraisal Time Table Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Appraisaltimetable','ITR','ERR','View of  Appraisaltimetable Table  unsuccessfull',' Appraisaltimetable',False)").then((log) => {
                console.log('ERROR:Unable to View Appraisal Time Table', error.message || error);
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Appraisal Time Table"
                })
            })
        });
})
// Get By Id for trainee Marks  for reports
examRouter.get('/trainee/marks/Details/Report/:Id/:examid', (req, res, next) => {
    var i = req.params.Id;
    var eid = req.params.examid;
    db.any('select  * from fn_viewtotalreportsBytrainee($1,$2)', [i, eid]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Trainee Marks','ITR','INFO','View of  Trainee Marks Table  successfull','  Trainee Marks',true)").then((d) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Trainee Report Card Details Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Trainee Marks','ITR','ERR','View of  Trainee Marks Table  unsuccessfull','  Trainee Marks',False)").then((d) => {
                console.log('ERROR:Unable to View trainee Marks by Id', error.message || error);
                res.status(404).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Trainee Report Card Details"
                })
            })
        });
})
//++++++++++++++++++++++++++++++++++
examRouter.get('/Trainee/Total/MarksDetails/:Id/:examid', (req, res, next) => {
    var i = req.params.Id;
    var eid = req.params.examid;
    db.any('select  * from fn_viewtotalReports($1,$2)', [i, eid]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: trainee Marks','ITR','INFO','View of  trainee Marks Table  successfull','  trainee Marks',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "trainee Marks by Id Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: trainee Marks','ITR','ERR','View of  trainee Marks Table  unsuccessfull','  trainee Marks',False)").then((log) => {
                console.log('ERROR:Unable to View trainee Marks by Id', error.message || error);
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View trainee Marks by Id"
                })
            })
        });
})
//+++++++++++++++++++++++++++++++++++++++++++++++
examRouter.get('/trainee/marks/batch/exam/:batchId/:examId', (req, res) => {
    var i = req.params.batchId;
    var examid = req.params.examId
    var arr = [];
    db.any('select  subjectname from appraisaltimetableview where batchid=$1 group by(subjectname)', [i]).then((data) => {
            db.any('select  count(subjectname) from appraisaltimetableview where batchid=$1 and appraisalid=$2', [i, examid]).then((subdata) => {
                let da = "select * from crosstab ('select firstname,name,max(marks) from traineemarksview where batchid=" + i + " and appraisalid=" + examid + " group by 1,2 order by 1,2'," +
                    "'select distinct name from traineemarksview order by 1') as newtable ( traineefirstname varchar";
                for (let i = 0; i < data.length; i++) {
                    da += ',' + data[i].subjectname + ' integer';
                }
                da += ");";
                db.any(da).then((data) => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:  Time Table','ITR','INFO','Retrive   of  Appraisal Time Table  successfull','  Appraisal Time table',true)").then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "Appraisal Time Table Retrived Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:  Time Table','ITR','ERR','Retrive   of  Appraisal Time Table unsuccessfull','  Appraisal Time table',False)").then((log) => {
                console.log('ERROR:Unable to View Appraisal Time Table', error.message || error);
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Appraisal Time Table"
                })
            })
        });
})
//++++++++++++++++++++++++++++++++++++++++
examRouter.get('/Trainee/Feestructure/view/', (req, res, next) => {
    db.any('select * from feestructureview').then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Fee Payments','ITR','INFO','Retrive  of Fee Payments Table successfull',' Fee Payments',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Fee Payments Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Fee Payments','ITR','ERR','Retrive  of Fee Payments Table unsuccessfull',' Fee Payments',False)").then((log) => {
                console.log('ERROR:Unable to View Fee Payments', error.message || error);
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Fee Payments"
                })
            })
        });
})
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++
examRouter.post('/Trainee/Batch/Add', (req, res, next) => {
    batchId = req.body.batchId;
    traineeId = req.body.traineeId;
    status = "Active"
    date = req.body.startDate;
    branchid = req.body.branchId;
    db.any('select * from fn_addtraineebatch($1,$2,$3,$4,$5)', [batchId, traineeId, status, date, branchid]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:  Trainee Batch','ITR','INFO','Addtion of trainee Batch  Table  successfull',' traineeBatch  table',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Trainee Batch Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:  Trainee Batch','ITR','ERR','Addtion of trainee Batch  Table  unsuccessfull',' traineeBatch  table',false)").then((log) => {
                console.log('ERROR:Unable to Add Trainee Btach', error.message || error);
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Add Trainee Batch"
                })
            })
        })
})
//++++++++++++++++++++++++++++++++++++++++
examRouter.get('/Trainee/Fee/Batch/:Id/:session', (req, res, next) => {
    var id = req.params.Id;
    var sessionparam = req.params.session;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select * from fn_viewfeepaymentsBybatch($1,$2)', [id, sessionparam]).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Fee Payments','ITR','INFO','Retrive  of Fee Payments Table successfull',' Fee Payments',true,$1)",userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Fee Payment Details by Batch Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Fee Payments','ITR','ERR','Retrive  of Fee Payments Table unsuccessfull',' Fee Payments',False$1)",userId).then((log) => {
                    console.log('ERROR:Unable to View Fee Payment Details by Batch ', error.message || error);
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Fee Payment Details by Batch "
                    })
                })
            })
        });
})
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
examRouter.get('/Session/Appraisal/TimeTable/Batch/Details/:Id', (req, res, next) => {
    var i = req.params.Id;
    db.any('select  * from fn_viewappraisaltimetableByBatchId($1)', i).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:  Time Table','ITR','INFO','Retrive   of  appraisal  successfull','  appraisal',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Appraisal Time Table Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:  Time Table','ITR','ERR','Retrive   of  appraisal unsuccessfull','  appraisal',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Appraisal Time Table"
                })
            })
        });
})
//+++++++++++++++++++++++
examRouter.get('/Session/Appraisal/TimeTable/Batch/subject/Details/:Id/:examId', (req, res, next) => {
    var i = req.params.Id;
    var examid = req.params.examId
    db.any('select  subjectname from appraisaltimetableview where batchid=$1 and appraisalid=$2 group by(subjectname)', [i, examid]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:  Time Table','ITR','INFO','Retrive   of  Appraisal Time Table  successfull','  Appraisal Time table',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Appraisal Time Table Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:  Time Table','ITR','ERR','Retrive   of  Appraisal Time Table unsuccessfull','  Appraisal Time table',False)").then((log) => {
                console.log('ERROR:Unable to View Appraisal Time Table', error.message || error);
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Appraisal Time Table"
                })
            })
        })
})
module.exports = examRouter;
//++++++++++++++++++++++++++++++++++++++++