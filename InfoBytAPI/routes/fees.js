var express = require('express');
var bodyparser = require('body-parser');
var payslip = require('../payslip')
var fs = require('fs')
var path = require('path')
var mail = require('../mailer');
var feeRouter = express.Router()
feeRouter.use(express.static(path.resolve(__dirname, "public")));
feeRouter.use(bodyparser.json({
    limit: '5mb'
}));
feeRouter.use(bodyparser.urlencoded({
    limit: '5mb',
    extended: true
}));
var config = require('../init_app');
const dba = require('../db.js');
const db = dba.db;
const pgp = db.$config.pgp;
// ==============================Concession=======================
//Post For  Concession
{
    feeRouter.post('/Concession/Add', (req, res, next) => {
        brni = req.body.branchId;
        consnm = req.body.name;
        db.any('select * from feestructurepatternview where id=$1', consnm).then(function (data) {
            var n = data[0].feepattern
            detl = req.body.details;
            cons = req.body.concession;
            cretd = req.body.date;
            amount = req.body.amount;
            db.any('select * from fn_AddConcession($1,$2,$3,$4,$5,$6)', [brni, n, detl, cons, cretd, amount]).then(() => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: Concession','ITR','INFO','Addtion of Concession Table successfull','Concession',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Concession Has Been Granted Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
                .catch(error => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: Concession','ITR','ERR','Addtion of Concession Table unsuccessfull','Concession',False)").then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to Add Concession Details."
                        })
                    })
                });
        });
    })
    //Get  For Concession
    feeRouter.get('/Concession/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_ViewConcession($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                 userId = Session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Concession','ITR','INFO','Retrive  of Concession Table successfull',' Concession',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: " Concession Details Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                     userId = Session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Concession','ITR','ERR','Retrive  of Concession Table unsuccessfull',' Concession',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                        error: error.message,
                        data: "ERROR",
                            message: "Unable to View Concession Details"
                        })
                    })
                })
            });
    })
    //GET By Id For Concession
    feeRouter.get('/Concession/Details/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select * from fn_ViewConcessionDetails($1)', i).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Concession','ITR','INFO','Retrive  of Concession Table successfull',' Concession',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: " Concession Details Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Concession','ITR','ERR','Retrive  of Concession Table unsuccessfull',' Concession',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Concession Details."
                    })
                })
            });
    })
    //Get  For Concession
    // Put For Concession
    feeRouter.post('/Concession/Update/:Id', (req, res, next) => {
        var id = req.params.Id;
        var bid = req.body.branchId;
        var cn = req.body.name;
        var dt = req.body.details;
        var cons = req.body.concession;
        var cdt = req.body.date;
        db.any('select * from feestructurepatternview where id=$1', cn).then(function (feedata) {
            var name = feedata[0].feepattern;
            db.any('select * from fn_updateconcession($1,$2,$3,$4,$5,$6)', [id, bid, name, dt, cons, cdt]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Concession','ITR','INFO','Retrive  of Concession Table successfull',' Concession',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: id,
                            message: "Concession Details Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
                .catch(error => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Concession','ITR','ERR','Update  of Concession Table unsuccessfull',' Concession',False)").then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Cannot Update Concession Details"
                        })
                    })
                });
        })
    })
}
// ================================================FeePayments======================
{
    //Post Fee Payments
    feeRouter.post('/Trainee/Fee/Add/', (req, res, next) => {
        var PaidAmountt = req.body.paidAmount;
        var PayDatee = req.body.payDate;
        var Duee = req.body.due;
        var PayModee = req.body.payMode;
        var Statuss = req.body.status;
        var StudentIdd = req.body.traineeId;
        db.any('select * from fn_AddFeePayments($1,$2,$3,$4,$5,$6)', [PaidAmountt, PayDatee, Duee, PayModee, Statuss, StudentIdd]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: Fee Payments','ITR','INFO','Addtion of Fee Payments Table successfull',' Fee Payments',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: " Fee Paid Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: Fee Payments','ITR','ERR','Addtion of Fee Payments Table unsuccessfull',' Fee Payments',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Fee Payments."
                    })
                })
            });
    })
    //GET For FEEPAYMENTS
    feeRouter.get('/Trainee/Fee/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_ViewFeePayments($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                 userId = Session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Fee Payments','ITR','INFO','Retrive  of Fee Payments Table successfull',' Fee Payments',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Fee Payments Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                     userId = Session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Fee Payments','ITR','ERR','Retrive  of Fee Payments Table unsuccessfull',' Fee Payments',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Fee Payments"
                        })
                    })
                })
            });
    })
    //GET By Id FEEPAYMENTS
    feeRouter.get('/Trainee/Fee/Details/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select * from fn_viewfeepaymentsbyId($1)', i).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Fee Payments','ITR','INFO','Update  of Fee Payments Table successfull',' Fee Payments',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Session Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Fee Payments','ITR','ERR','Update  of Fee Payments Table unsuccessfull',' Fee Payments',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Fee Payment Details"
                    })
                })
            });
    })
    //GET For FEEPAYMENTS by user id
    feeRouter.get('/Trainee/Fee/user/:Id', (req, res, next) => {
        var uid = req.params.Id;
        db.any('select * from fn_viewfeepaymentsByUserId($1)', uid).then(function (data) {
            console.log(data)
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Fee Payments','ITR','INFO','Retrive  of Fee Payments Table successfull',' Fee Payments',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Payment Details Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Fee Payments','ITR','ERR','Retrive  of Fee Payments Table unsuccessfull',' Fee Payments',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Fee Payment Details"
                    })
                })
            });
    })
    //GET By BatchId FEEPAYMENTS
    feeRouter.get('/Trainee/Fee/Batch/:Id/:session', (req, res, next) => {
        var id = req.params.Id;
        var sessionparam = req.params.session;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewfeepaymentsBybatch($1,$2)', [id, sessionparam]).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                 userId = Session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Fee Payments','ITR','INFO','Retrive  of Fee Payments Table successfull',' Fee Payments',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Fee Payment Details By Batch Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                     userId = Session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Fee Payments','ITR','ERR','Retrive  of Fee Payments Table unsuccessfull',' Fee Payments',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable To View Fee Payment Details By Batch "
                        })
                    })
                })
            });
    })
    //get financialreports
    feeRouter.get('/Financial/Details/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewfinancialreports($1)', sessionparam).then(function (data) {
            console.log(data)
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                 userId = Session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: Fee Payments','ITR','INFO','View  of Fee Payments Table successfull',' Fee Payments',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Financial Details Retived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((data) => {
                     userId = data[0].fn_viewuserid;
                    db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                        var userId = Session[0].fn_viewuserid;
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Fee Payments','ITR','ERR','Retrive  of Fee Payments Table unsuccessfull',' Fee Payments',False,$1)", userId).then((log) => {
                            res.status(200).send({
                                result: false,
                                error: error.message,
                                data: "ERROR",
                                message: "Unable to View Financial Details."
                            })
                        })
                    });
                })
            });
    })
    //get financial report by branch session
    feeRouter.get('/Financialreport/branchsession/Details/:Id', (req, res, next) => {
        var bsId = req.params.Id;
        db.any('select * from fn_viewfinancialreportsbybranchsession($1)', bsId).then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Fee Payments','ITR','INFO','View  of Fee Payments Table successfull',' Fee Payments',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Financial Report Details Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
               
            })
        })
            .catch(error => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Fee Payments','ITR','ERR','Retrive  of Fee Payments Table unsuccessfull',' Fee Payments',False)").then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Financial Report Details."
                        })
                    })
                });
            });
  
}
// ====================================================FEE==================================
{
    //Post For Fee
    feeRouter.post('/Fee/Add', (req, res, next) => {
        var feen = req.body.feeName;
        var bchid = req.body.branchId;
        var brnchid = req.body.branchSessionId;
        db.any('select * from feestructurepatternview where id=$1', feen).then(function (data) {
            var feeam = data[0].pattrenamount;
            var feename = data[0].feepattern
            db.any('select fn_AddFees($1,$2,$3,$4)', [feename, bchid, brnchid, feeam]).then(function () {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition: Fee ','ITR','INFO','Addition of Fee  Table successfull','FEE r',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Fee Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
                .catch(error => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition: Fee ','ITR','ERR','Addition  of Fee  Table unsuccessfull','FEE r',False)").then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to Add Fee"
                        })
                    })
                });
        })
    })
    // =========================
    //GET  For Fee
    feeRouter.get('/Fee/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_Viewfee($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Fee ','ITR','INFO','Retrive of Fee  Table successfull','FEE ',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Fee Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((data) => {
                     userId = data[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Fee ','ITR','ERR','Retrive  of Fee  Table unsuccessfull','FEE ',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Fee"
                        })
                    })
                });
            })
    })
    //GET BY ID  For Fee
    feeRouter.get('/Fee/Details/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select * from fn_ViewfeeDetails($1)', i).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Fee ','ITR','INFO','Retrive of Fee Maste Table successfull','Fee ',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Fee Details Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Fee ','ITR','ERR','Retrive  of Fee Maste Table unsuccessfull','Fee ',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Fee Details"
                    })
                })
            });
    })
    //PUT For Fee
    feeRouter.post('/Fee/Update/:Id', (req, res, next) => {
        var feeid = req.params.Id;
        var feename = req.body.feeName;
        var branchid = req.body.branchId;
        var branchsessionid = req.body.branchSessionId;
       // var feeamount = req.body.feeAmount;
        console.log(req.body)
        db.any('select * from feestructurepatternview where id=$1', feename).then(function (data) {
            var name = data[0].feepattern;
            console.log(feeid, name, branchid, branchsessionid)
            db.any('select * from fn_updateFees($1,$2,$3,$4)', [feeid, name, branchid, branchsessionid]).then((data) => {
                console.log(JSON.stringify(data))
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Fee ','ITR','INFO','Update of Fee  Table successfull','Fee ',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: feeid,
                            message: "Session Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
                .catch(error => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Fee ','ITR','ERR','Update  of Fee  Table unsuccessfull','Fee ',False)").then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Cannot Update Fee"
                        })
                    })
                });
        });
 })//GET BY ID  For Accontant in Fee
    feeRouter.get('/Fee/Accountant/Details/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewfeedetailsforaccountant($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                 userId = Session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: Fee Payments','ITR','INFO','Retrive  of Fee Payments Table successfull',' Fee Payments',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Session Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                     userId = Session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Fee Payments','ITR','ERR','Retrive  of Fee Payments Table unsuccessfull',' Fee Payments',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Fee Payment Details"
                        })
                    })
                })
            });
    })
}
// ===============================================FEESTRUCTURE=================================
{
    
    feeRouter.post('/Fee/Structure/Add/:id', (req, res, next) => {
        var sessionid = req.params.id;
        db.any('select * from fn_viewOrganizationsessiondetails($1)', sessionid).then(function (data) {
            var fees = ['Lumsum', 'Instalment'];
            feename = 'SomeFee';
            sessionname = data[0].sessionname;
            stringify = JSON.stringify(req.body)
            var structure = JSON.parse(stringify);
            feestructure = ''
            feesstructure = ''
            amount = 0;
            totalAmount=0
            for (let i = 0; i < fees.length; i++) {
                //amount = 0;

                totalAmount += parseFloat(structure[i].Amount);
                //console.log('i '+amount)
            }
            for (let i = 0; i < fees.length; i++) {
               // amount = 0;
                feestructure = fees[i] + '_Session_' + sessionname;
               amount += parseFloat(structure[0].Amount);
                db.any('select * from  fn_addfeestructure($1,$2,$3,$4,$5,$6)', [structure[0].classId, structure[0].FeeName, totalAmount, feestructure, structure[0].EMI, structure[0].Optional]).then((id) => {
                    var feestructureid = id[0].fn_addfeestructure;
                    console.log(feestructureid)
                // db.any('select * from  fn_addfeestructurepattern($1,$2,$3)', [feestructureid, feestructure, amount]).then((id) => {
                        for (var j = 0; j < structure.length; j++) {
                            console.log(j)
                            //if (structure[j].Optional) {
                                feestructure = fees[i] + '_Session_' + sessionname + '_' + structure[j].FeeName;
                                amount = parseFloat(structure[j].Amount);
                                console.log(feestructureid, feestructure, amount)
                                db.any('select * from  fn_addfeestructurepattern($1,$2,$3)', [feestructureid, feestructure, amount]).then((id) => {
                                    if ((j + 1) < structure.length) {
                                        amount += parseFloat(structure[j + 1].Amount);
                                        feestructure += feestructure + '_' + structure[j + 1].FeeName
                                        db.any('select * from  fn_addfeestructurepattern($1,$2,$3)', [feestructureid, feestructure, amount]).then((idd) => {
                                            if(idd.length>0)
                                           {
                                            res.send({
                                                result:true,
                                                error: "NOERROR",
                                                data: idd,
                                                message: "Fee Structure Added Successfully"
                                            })
                                        }
                                        else
                                        {
                                            res.send({
                                                result:true,
                                                error: "NOERROR",
                                                data: "NDF",
                                                message: "Fee Structure Added Successfully"
                                            })
                                        }
                                         })
                                    }
                                })
                           // }
                        }
                    })
    // })
            }
        })
    })
    // ================== view fee structure =====================
  
 feeRouter.get('/Trainee/Feestructure/view/:Id', (req, res, next) => {
    var sessionparam = req.params.Id;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select * from feestructureview where branchid=(select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid($1)))',sessionparam).then(function (data) {

        db.any('select fn_viewuserid from fn_viewuserid($1)',sessionparam).then(function (udata) {
            userId=udata[0].fn_viewuserid
db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: Fee Payments','ITR','INFO','Retrive  of Fee Payments Table successfull',' Fee Payments',true,$1)",userId).then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Fee Payments Retrived Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
})
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)',sessionparam).then(function (udata) {
                userId=udata[0].fn_viewuserid

            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: Fee Payments','ITR','ERR','Retrive  of Fee Payments Table unsuccessfull',' Fee Payments',False,$1)",userId).then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Fee Payments"
                })
            })
        })
        });
})
// ================== view fee structure pattern =============
feeRouter.get('/Trainee/Feestructurepattern/view/:Id', (req, res, next) => {
    var sessionparam = req.params.Id;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select * from feestructurepatternview where branchid=(select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid($1)))',sessionparam).then(function (data) {

        db.any('select fn_viewuserid from fn_viewuserid($1)',sessionparam).then(function (udata) {
            userId=udata[0].fn_viewuserid


        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: Fee Payments','ITR','INFO','Retrive  of Fee Payments Table successfull',' Fee Payments',true,$1)",userId).then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: " Fee Payments Retrived Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)',sessionparam).then(function (udata) {
                userId=udata[0].fn_viewuserid
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: Fee Payments','ITR','ERR','Retrive  of Fee Payments Table unsuccessfull',' Fee Payments',False,$1)",userId).then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Fee Payments"
                })
            })
            })
        });
})
    feeRouter.get('/Trainee/Feestructurepattern/viewbyId/:id', (req, res, next) => {
        var id = req.params.id
        db.any('select * from feestructurepatternview where id=$1', id).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Fee Payments','ITR','INFO','Retrive  of Fee Payments Table successfull',' Fee Payments',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Fee Payments Details Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Fee Payments','ITR','ERR','Retrive  of Fee Payments Table unsuccessfull',' Fee Payments',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Fee Payments Details"
                    })
                })
            });
    })
}
module.exports = feeRouter;