var express = require('express');
var bodyparser = require('body-parser');
var fs = require('fs')
var path = require('path')
var loginRouter = express.Router();
var mail = require('../mailer');
var get_ip = require('ipware')().get_ip;
var device = require('express-device');
var config = require('../init_app');
loginRouter.use(device.capture());
loginRouter.use(express.static(path.resolve(__dirname, "public")));
loginRouter.use(bodyparser.json({
    limit: '5mb'
}));
loginRouter.use(bodyparser.urlencoded({
    limit: '5mb',
    extended: true
}));
loginRouter.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', '*');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
const dba = require('../db.js');
const db = dba.db;
const pgp = db.$config.pgp;
global.user = '';
//================================================================== <Chat> ===================================================================================
// {
//     loginRouter.post('/Chat/', (req, res, next) => {
//         var supportuserId = req.body.support;
//         db.any('insert into chat(supportUserId) values($1) ON CONFLICT (supportUserId) DO UPDATE SET userId=null', supportuserId).then(function (data) {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:chat','ITR','INFO','Addition of chat Table successfull','chat',true)").then((log) => {
//                     if(data.length>0){
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:data,
//                             message: " Chat  Has Been Added Successfully"
//                         })
//                     }else{
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:"NDF",
//                             message: "No Data Found"
//                         })
//                     }
//                 })
//             })
//             .catch(error => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:chat','ITR','ERR','Addition of chat Table unsuccessfull','chat',False)").then((log) => {
//                     console.log('ERROR:Unable to  Add Chat', error.message || error);
//                     res.status(200).send({
//                         result: false,
//                         error: error.message,
//                         data: "ERROR",
//                         message: "Unable to  Add Chat"
//                     })
//                 });
//             })
//     })
//     loginRouter.post('/Chat/Queue/Add/', (req, res, next) => {
//         var userId = req.body.userId;
//         db.any("insert into chatqueue(userId,status,starttime) values($1,'Waiting',current_timestamp) returning userid", userId).then(function (data) {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:chat','ITR','INFO','Addition of chat Table successfull','chat',true)").then((log) => {
//                     if(data.length>0){
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:data,
//                             message: "Chat Has Been Added Successfully"
//                         })
//                     }else{
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:"NDF",
//                             message: "No Data Found"
//                         })
//                     }
//                 })
//             })
//             .catch(error => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:chat','ITR','ERR','Addition of chat Table unsuccessfull','chat',False)").then((log) => {
//                     res.status(200).send({
//                         result: false,
//                         error: error.message,
//                         data: "ERROR",
//                         message: "Unable to Add Chat"
//                     })
//                 });
//             })
//     })
//     loginRouter.get('/ChatClient/:convoId', (req, res, next) => {
//         var convoId = req.params.convoId;
//         db.any('select userid from conversationtable where Id=$1', convoId).then(function (data) {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatClient','ITR','INFO','view of chat Table successfull','ChatClient',true)").then((log) => {
//                     if(data.length>0){
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:data,
//                             message: "User  Retrived  Successfully"
//                         })
//                     }else{
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:"NDF",
//                             message: "No Data Found"
//                         })
//                     }
//                 })
//             })
//             .catch(error => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatClient','ITR','ERR','view of chat Table unsuccessfull','ChatClient',False)").then((log) => {
//                     console.log('ERROR:Unable to View  Users', error.message || error);
//                     res.status(200).send({
//                         result: false,
//                         error: error.message,
//                         data: "ERROR",
//                         message: "Unable to View  Users"
//                     })
//                 });
//             })
//     })
//     loginRouter.get('/Chat/Check/Support/:supportType', (req, res, next) => {
//         db.any("(with convoCount as(select supportid,count(userid) from conversationtable where supporttype=$1 and status='Inactive' group by 1)select COALESCE(supportid,0) from convoCount where count<3 order by count asc limit 1)", req.params.supportType).then(function (data) {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatClient','ITR','INFO','view of chat Table successfull','ChatClient',true)").then((log) => {
//                     if(data.length>0){
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:data,
//                             message: "Staff Users Retrived  Successfully"
//                         })
//                     }else{
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:"NDF",
//                             message: "No Data Found"
//                         })
//                     }
//                 })
//             })
//             .catch(error => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatClient','ITR','ERR','view of chat Table unsuccessfull','ChatClient',False)").then((log) => {
//                     console.log('ERROR:Unable to View Staff Users', error.message || error);
//                     res.status(200).send({
//                         result: false,
//                         error: error.message,
//                         data: "ERROR",
//                         message: "Unable to View Staff Users"
//                     })
//                 });
//             })
//     })
//     loginRouter.get('/ChatSupport/:Id', (req, res, next) => {
//         db.any('select supportId from conversationtable where userId=(($1))', req.params.Id).then(function (data) {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatSupport','ITR','INFO','view of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
//                     if(data.length>0){
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:data,
//                             message: "Staff Users Retrived  Successful ly"
//                         })
//                     }else{
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:"NDF",
//                             message: "No Data Found"
//                         })
//                     }
//                 })
//             })
//             .catch(error => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatSupport','ITR','ERR','view of ChatSupport Table unsuccessfull','ChatSupport',False)").then((log) => {
//                     console.log('ERROR:Unable to View Staff Users', error.message || error);
//                     res.status(200).send({
//                         result: false,
//                         error: error.message,
//                         data: "ERROR",
//                         message: "Unable to View Staff Users"
//                     })
//                 });
//             })
//     })
//     loginRouter.post('/Chat/', (req, res, next) => {
//         var sessionparam = req.body.user;
//         db.any('update chat set userId=(select fn_viewuserid from fn_viewuserid($1)) where supportuserId=(select max(supportuserId) from chat where userId is null) returning userId', sessionparam).then(function (data) {
//                 db.any("select * from chat where userid=$1", data[0].userid).then((chatdata) => {
//                     db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Chat','ITR','INFO','Update of Chat Table successfull','Chat',true)").then((log) => {
//                         if(data.length>0){
//                             res.status(200).send({
//                                 result:true,
//                                 error:"NOERROR",
//                                 data:data,
//                                 message: "Chat  Has Been Added Successfully"
//                             })
//                         }else{
//                             res.status(200).send({
//                                 result:true,
//                                 error:"NOERROR",
//                                 data:"NDF",
//                                 message: "No Data Found"
//                             })
//                         }
//                     })
//                 })
//             })
//             .catch(error => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Chat','ITR','ERR','Update of Chat Table unsuccessfull','Chat',False)").then((log) => {
//                     console.log('ERROR:Unable to Update Chat', error.message || error);
//                     res.status(200).send({
//                         result: false,
//                         error: error.message,
//                         data: "ERROR",
//                         message: "Unable to Update Chat"
//                     })
//                 });
//             })
//     })
//     loginRouter.post('/Chat/Close/:convoId', (req, res, next) => {
//         var userId = req.params.convoId;
//         db.any("update conversationtable set status='completed' where Id=$1", userId).then(function (data) {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:userId','ITR','INFO','Update of userId Table successfull','userId',true)").then((log) => {
//                     if(data.length>0){
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:data,
//                             message: "Staff Users  Has Been Added Successfully"
//                         })
//                     }else{
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:"NDF",
//                             message: "No Data Found"
//                         })
//                     }
//                 })
//             })
//             .catch(error => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:userId','ITR','ERR','Update of userId Table unsuccessfull','userId',False)").then((log) => {
//                     console.log('ERROR:Unable to Update Staff Users', error.message || error);
//                     res.status(200).send({
//                         result: false,
//                         error: error.message,
//                         data: "ERROR",
//                         message: "Unable to Update Staff Users"
//                     })
//                 });
//             })
//     })
//     loginRouter.get('/Chat/:chatId', (req, res, next) => {
//         db.any('select * from messages where chatid=($1) order by createddate', req.params.chatId).then(function (data) {
//                 db.any("update messages set status='R' where chatId=$1", req.params.chatId).then((update) => {
//                     db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:chatId','ITR','INFO','View of chatId Table successfull','chatId',true)").then((log) => {
//                         if(data.length>0){
//                             res.status(200).send({
//                                 result:true,
//                                 error:"NOERROR",
//                                 data:data,
//                                 message: "chat Retrived  Successfully"
//                             })
//                         }else{
//                             res.status(200).send({
//                                 result:true,
//                                 error:"NOERROR",
//                                 data:"NDF",
//                                 message: "No Data Found"
//                             })
//                         }
//                     })
//                 })
//             })
//             .catch(error => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:chatId','ITR','ERR','View of chatId Table unsuccessfull','chatId',False)").then((log) => {
//                     console.log('ERROR:Unable to View Chat', error.message || error);
//                     res.status(200).send({
//                         result: false,
//                         error: error.message,
//                         data: "ERROR",
//                         message: "Unable to View Chat"
//                     })
//                 });
//             })
//     })
//     loginRouter.get('/Chat/Support/:userSession', (req, res, next) => {
//         var sessionparam = req.params.userSession;
//         var userId='';
//         for (var i = 0; i < sessionparam.length; i++) {
//             sessionparam = sessionparam.replace('-', '/');
//         }
//         db.any('select * from chatview where pos=1 and cast(conversationid as int) in (select id from conversationtable where supportId=(select fn_viewuserid from fn_viewuserid($1)))', sessionparam).then(function (data) {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:chatview','ITR','INFO','View of chatview Table successfull','chatview',true,$1)",userId).then((log) => {
//                     db.any('select fn_viewuserid from fn_viewuserid($1)',sessionparam).then((dbsesstion)=>{
//                      userId=dbsesstion[0].fn_viewuserid;
//                         if(data.length>0){
//                             res.status(200).send({
//                                 result:true,
//                                 error:"NOERROR",
//                                 data:data,
//                                 message: "Staff Users Retrived  Successfully"
//                             })
//                         }else{
//                             res.status(200).send({
//                                 result:true,
//                                 error:"NOERROR",
//                                 data:"NDF",
//                                 message: "No Data Found"
//                             })
//                         }
//                     })
//                 })
//             })
//             .catch(error => {
//                 db.any('select fn_viewuserid from fn_viewuserid($1)',sessionparam).then((dbsesstion)=>{
//                      userId=dbsesstion[0].fn_viewuserid;
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:chatview','ITR','ERR','View of chatview Table unsuccessfull','chatview',False,$1)",userId).then((log) => {
//                     res.status(200).send({result: false,
//                         error: error.message,
//                         data: "ERROR",
//                         message: "Unable to View Staff Users"
//                     })
//                 });
//             })
//         })
//     })
//     loginRouter.get('/Chat/Support/Convo/:userId', (req, res, next) => {
//         db.any('select Id from conversationtable where userId=(($1))', req.params.userId).then(function (data) {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatSupport','ITR','INFO','view of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
//                     if(data.length>0){
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:data,
//                             message: "Staff Users Retrived   Successfully"
//                         })
//                     }else{
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:"NDF",
//                             message: "No Data Found"
//                         })
//                     }
//                 })
//             })
//             .catch(error => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatSupport','ITR','ERR','view of ChatSupport Table unsuccessfull','ChatSupport',False)").then((log) => {
//                     console.log('ERROR:Unable to View Staff Users', error.message || error);
//                     res.status(200).send({
//                         result: false,
//                         error: error.message,
//                         data: "ERROR",
//                         message: "Unable to View Staff Users"
//                     })
//                 });
//             })
//     })
//     loginRouter.get('/Chat/view/UsersChat/:Id', (req, res, next) => {
//         var i = req.params.Id;
//         db.any('select * from fn_viewuserschat($1)', i).then(function (data) {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatSupport','ITR','INFO','view of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
//                     if(data.length>0){
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:data,
//                             message: " users Chat Retrived  Successfully"
//                         })
//                     }else{
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:"NDF",
//                             message: "No Data Found"
//                         })
//                     }
//                 })
//             })
//             .catch(error => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatSupport','ITR','ERR','view of ChatSupport Table unsuccessfull','ChatSupport',False)").then((log) => {
//                     console.log('ERROR:Unable to View users Chat', error.message || error);
//                     res.status(200).send({
//                         result: false,
//                         error: error.message,
//                         data: "ERROR",
//                         message: "Unable to View users Chat"
//                     })
//                 })
//             });
//     })
//     loginRouter.get('/Chat/view/Conversation/:Id', (req, res, next) => {
//         var i = req.params.Id;
//         db.any('select * from fn_viewconversationbysupportid($1)', i).then(function (data) {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatSupport','ITR','INFO','view of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
//                     if(data.length>0){
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:data,
//                             message: "Conversations Retrived  Successfully"
//                         })
//                     }else{
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:"NDF",
//                             message: "No Data Found"
//                         })
//                     }
//                 })
//             })
//             .catch(error => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatSupport','ITR','ERR','view of ChatSupport Table unsuccessfull','ChatSupport',False)").then((log) => {
//                     console.log('ERROR:Unable to View Conversations', error.message || error);
//                     res.status(200).send({
//                         result: false,
//                         error: error.message,
//                         data: "ERROR",
//                         message: "Unable to View Conversations"
//                     })
//                 })
//             });
//     })
//     loginRouter.get('/Chat/view/Messages/:Id', (req, res, next) => {
//         var i = req.params.Id;
//         db.any('select * from fn_viewmessagesbyconversationid($1)', i).then(function (data) {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatSupport','ITR','INFO','view of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
//                     if(data.length>0){
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:data,
//                             message: "Messages Retrived  Successfully"
//                         })
//                     }else{
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:"NDF",
//                             message: "No Data Found"
//                         })
//                     }
//                 })
//             })
//             .catch(error => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatSupport','ITR','ERR','view of ChatSupport Table unsuccessfull','ChatSupport',False)").then((log) => {
//                     console.log('ERROR:Unable to View Messages', error.message || error);
//                     res.status(200).send({
//                         result: false,
//                         error: error.message,
//                         data: "ERROR",
//                         message: "Unable to View Messages"
//                     })
//                 });
//             })
//     })
//     loginRouter.post('/Chat/Messages/Update/', (req, res, next) => {
//         var i = req.body.convoId;
//         db.any("update messagestable set status='R' where conversationid=$1", i).then(function (data) {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','INFO','Update of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
//                     if(data.length>0){
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:data,
//                             message: "Messages Has Been Added Successfully"
//                         })
//                     }else{
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:"NDF",
//                             message: "No Data Found"
//                         })
//                     }
//                 })
//             })
//             .catch(error => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','ERR','Update of ChatSupport Table unsuccessfull','ChatSupport',false)").then((log) => {
//                     console.log('ERROR:Unable to Update Messages', error.message || error);
//                     res.status(200).send({
//                         result: false,
//                         error: error.message,
//                         data: "ERROR",
//                         message: "Unable to Update Messages"
//                     })
//                 })
//             });
//     })
//     loginRouter.get('/Chat/view/Messages/Support/:sId', (req, res, next) => {
//         var i = req.params.sId;
//         db.any('select * from messagestable where fromuser=$1', i).then(function (data) {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:ChatSupport','ITR','INFO','View of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
//                     if(data.length>0){
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:data,
//                             message: "Messages Retrived d Successfully"
//                         })
//                     }else{
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:"NDF",
//                             message: "No Data Found"
//                         })
//                     }
//                 })
//             })
//             .catch(error => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatSupport','ITR','ERR','view of ChatSupport Table unsuccessfull','ChatSupport',False)").then((log) => {
//                     console.log('ERROR:Unable to View Messages', error.message || error);
//                     res.status(200).send({
//                         result: false,
//                         error: error.message,
//                         data: "ERROR",
//                         message: "Unable to View Messages"
//                     })
//                 });
//             })
//     })
//     // adding
//     // loginRouter.post('/Chat/Add/Conversation', (req, res, next) => {
//     //     var supportId = req.body.support;
//     //     var roleName = req.body.roleName;
//     //     var type = req.body.type;
//     //     var supportAttendanceId;
//     //     var date = new Date;
//     //     var todayDate = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
//     //     db.any("select * from supportattendance where supportid=$1 order by id desc", [supportId]).then((data) => {
//     //         supportAttendanceId = data[0].id
//     //         if (data[0].date == todayDate && data[0].status == 'Break Activated') {
//     //             db.any("update supportattendance set status='Returned from Break' where id=$1", data[0].id).then((data) => {})
//     //         }
//     //         if (type == 'Login') {
//     //             db.any("insert into supportattendance(supportid,intime,date,status) values($1,current_time,current_date,'Active')", [supportId]).then(() => {})
//     //         } else if (type == 'Chat') {
//     //             db.any("update supportattendance set outtime=null,status='Active' where id=$1", [supportAttendanceId]).then(() => {})
//     //         } else {}
//     //     })
//     //     for (var i = 0; i < 3; i++) {
//     //         db.any('select * from fn_addconversationtable($1,$2)', [supportId, roleName]).then(() => {})
//     //             .catch(error => {
//     //                 console.log('ERROR:Unable to Add Conversation', error.message || error);
//     //             });
//     //     }
//     //     db.any("select userid from chatqueue where status='Waiting' limit 1").then((data) => {
//     //         if (data.length != 0) {
//     //             db.any('select username from userchat where id=($1)', [data[0].userid]).then((data1) => {
//     //                 db.any('select * from fn_updateconversation($1,$2,$3)', [data[0].userid, data1[0].username, supportId]).then((data2) => {
//     //                     db.any("update chatqueue set status='Assigned',endtime=current_timestamp where userid=$1", data[0].userid).then((update) => {
//     //                         res.status(200).send(data);
//     //                     })
//     //                 })
//     //             })
//     //         } else {
//     //             res.status(200).send({
//     //                 message: 'Added to conversation '
//     //             })
//     //         }
//     //     })
//     // })
//     loginRouter.post('/Chat/Add/New/Convo/', (req, res, next) => {
//         var sessionparam = req.body.userSession;
//         var convoId = req.body.convoId
//         var supportId;
//         db.any('select * from loginview where id=(select fn_viewuserid from fn_viewuserid($1))', sessionparam).then((user) => {
//             supportId = user[0].id;
//             db.any('select * from fn_addconversationtable($1,$2)', [user[0].id, user[0].rolename]).then(() => {})
//                 .catch(error => {
//                     console.log('ERROR:Unable to Add Conversation', error.message || error);
//                 });
//             db.any("select userid from chatqueue where status='Waiting' limit 1").then((data) => {
//                 if (data.length != 0) {
//                     db.any('select username from userchat where id=($1)', [data[0].userid]).then((data1) => {
//                         db.any('select * from fn_updateconversation($1,$2,$3)', [data[0].userid, data1[0].username, supportId]).then((data2) => {
//                             db.any("update chatqueue set status='Assigned',endtime=current_timestamp where userid=$1", data[0].userid).then((update) => {
//                                 res.status(200).send(data);
//                             })
//                         })
//                     })
//                 } else {
//                     res.status(200).send({
//                         message: 'Added to conversation table'
//                     })
//                 }
//             })
//         })
//     })
//     loginRouter.post('/Chat/Add/Messages', (req, res, next) => {
//         var message = req.body.messages;
//         var from = req.body.fromuser;
//         var to = req.body.touser;
//         var Conversationid = req.body.conversationid;
//         db.any('select * from fn_addmessagetable($1,$2,$3,$4)', [message, from, to, Conversationid]).then((data) => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-ADD:ChatSupport','ITR','INFO','ADD of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
//                     if(data.length>0){
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:data,
//                             message: "Message Has Been Added Successfully"
//                         })
//                     }else{
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:"NDF",
//                             message: "No Data Found"
//                         })
//                     }
//                 })
//             })
//             .catch(error => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-ADD:ChatSupport','ITR','ERR','ADD of ChatSupport Table unsuccessfull','ChatSupport',false)").then((log) => {
//                     console.log('ERROR:Unable to Add Message', error.message || error);
//                     res.status(200).send({
//                         result: false,
//                         error: error.message,
//                         data: "ERROR",
//                         message: 'Unable to Add Message'
//                     })
//                 });
//             })
//     })
//     loginRouter.post('/Chat/Feedback/Add', (req, res, next) => {
//         var convoId = req.body.convoId;
//         db.any('insert into chatfeedback(outtime,conversationId) values(current_timestamp,$1) returning id', [convoId]).then((data) => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-ADD:ChatSupport','ITR','INFO','ADD of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
//                     if(data.length>0){
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:data,
//                             message: " Out Time Has Been Added Successfully"
//                         })
//                     }else{
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:"NDF",
//                             message: "No Data Found"
//                         })
//                     }
//                 })
//             })
//             .catch(error => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-ADD:ChatSupport','ITR','ERR','ADD of ChatSupport Table unsuccessfull','ChatSupport',false)").then((log) => {
//                     console.log('ERROR:Unable to Add Out Time', error.message || error);
//                     res.status(200).send({
//                         result: false,
//                         error: error.message,
//                         data: "ERROR",
//                         message: 'Unable to Add Out Time'
//                     })
//                 });
//             })
//     })
//     loginRouter.post('/Chat/Feedback/Update', (req, res, next) => {
//         var convoId = req.body.convoId;
//         var rating = req.body.rating
//         db.any('update chatfeedback set rating=$1 where conversationid=$2', [rating, convoId]).then((data) => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','INFO','Update of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
//                     if(data.length>0){
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:data,
//                             message: " chatfeedback  updated Successfully"
//                         })
//                     }else{
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:"NDF",
//                             message: "No Data Found"
//                         })
//                     }
//                 })
//             })
//             .catch(error => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','ERR','Update of ChatSupport Table unsuccessfull','ChatSupport',false)").then((log) => {
//                     console.log('ERROR:Unable to update chatfeedback', error.message || error);
//                     res.status(200).send({
//                         result: false,
//                         error: error.message,
//                         data: "ERROR",
//                         message: 'Unable to  update chatfeedback'
//                     })
//                 });
//             })
//     })
//     //update
//     loginRouter.post('/Chat/Update/Conversation/', (req, res, next) => {
//         var id = req.body.userChatId;
//         var userName = req.body.userName;
//         var supportId = req.body.supportId
//         db.any('select * from fn_updateconversation($1,$2,$3)', [id, userName, supportId]).then((data) => {
//                 db.any("update chatqueue set status='Assigned',endtime=current_timestamp where userid=$1", id).then((update) => {
//                     db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','INFO','Update of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
//                         if(data.length>0){
//                             res.status(200).send({
//                                 result:true,
//                                 error:"NOERROR",
//                                 data:data,
//                                 message: "Conversation  Has Been Added Successfully"
//                             })
//                         }else{
//                             res.status(200).send({
//                                 result:true,
//                                 error:"NOERROR",
//                                 data:"NDF",
//                                 message: "No Data Found"
//                             })
//                         }
//                     })
//                 })
//             })
//             .catch(error => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','INFO','Update of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
//                     console.log('ERROR:Cannot Update Conversation Details', error.message || error);
//                     res.status(200).send({
//                         result: false,
//                         error: error.message,
//                         data: "ERROR",
//                         message: 'Unable to  update  Conversation Details'
//                     })
//                 });
//             })
//     })
//     loginRouter.post('/Chat/Update/Logout/', (req, res, next) => {
//         sessionparam = req.body.userSession;
//         type = req.body.type;
//         if (type == 'Break') {
//             db.any("select * from supportattendance where supportid=(select fn_viewuserid from fn_viewuserid($1)) order by id desc", [sessionparam]).then((data) => {
//                     db.any("Update supportattendance set status='Break Activated' where id=$1", data[0].id).then((data) => {
//                         db.any("update conversationtable set status='loggedout' where supportid=(select fn_viewuserid from fn_viewuserid($1))", [sessionparam]).then((data) => {
//                             db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','INFO','Update of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
//                                 if(data.length>0){
//                                     res.status(200).send({
//                                         result:true,
//                                         error:"NOERROR",
//                                         data:data,
//                                         message: "   Conversation Details Updated Successfully"
//                                     })
//                                 }else{
//                                     res.status(200).send({
//                                         result:true,
//                                         error:"NOERROR",
//                                         data:"NDF",
//                                         message: "No Data Found"
//                                     })
//                                 }
//                             })
//                         })
//                     })
//                 })
//                 .catch(error => {
//                     db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','INFO','Update of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
//                         console.log('ERROR:Cannot Update Conversation Details', error.message || error);
//                         res.status(200).send({
//                             result: false,
//                             error: error.message,
//                             data: "ERROR",
//                             message: 'Unable to  update  Conversation Details'
//                         })
//                     });
//                 })
//         } else {
//             db.any("select * from supportattendance where supportid=(select fn_viewuserid from fn_viewuserid($1)) order by id desc", [sessionparam]).then((data) => {
//                     db.any("Update supportattendance set status='loggedout' where id=$1", data[0].id).then((data) => {
//                         db.any("update conversationtable set status='loggedout' where supportid=(select fn_viewuserid from fn_viewuserid($1))", [sessionparam]).then((data) => {
//                             db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','INFO','Update of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
//                                 if(data.length>0){
//                                     res.status(200).send({
//                                         result:true,
//                                         error:"NOERROR",
//                                         data:data,
//                                         message: " Conversation Details Has Been Updated Successfully"
//                                     })
//                                 }else{
//                                     res.status(200).send({
//                                         result:true,
//                                         error:"NOERROR",
//                                         data:"NDF",
//                                         message: "No Data Found"
//                                     })
//                                 }
//                             })
//                         })
//                     })
//                 })
//                 .catch(error => {
//                     db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','ERR','Update of ChatSupport Table unsuccessfull','ChatSupport',false)").then((log) => {
//                         console.log('ERROR:Cannot Update Conversation Details', error.message || error);
//                         res.status(200).send({
//                             result: false,
//                             error: error.message,
//                             data: "ERROR",
//                             message: 'Unable to  Update  Conversation Details'
//                         })
//                     });
//                 })
//         }
//     })
//     loginRouter.post('/Chat/Add/UserChat', (req, res, next) => {
//         var userName = req.body.user;
//         db.any('select * from fn_adduserchat($1)', [userName]).then((data) => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Add:ChatSupport','ITR','INFO','Add of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
//                     if(data.length>0){
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:data,
//                             message: "Chat Has Been Added Successfully"
//                         })
//                     }else{
//                         res.status(200).send({
//                             result:true,
//                             error:"NOERROR",
//                             data:"NDF",
//                             message: "No Data Found"
//                         })
//                     }
//                 })
//             })
//             .catch(error => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','ERR','Update of ChatSupport Table unsuccessfull','ChatSupport',false)").then((log) => {
//                     console.log('ERROR:Unable to Add User Chat', error.message || error);
//                     res.status(200).send({
//                         result: false,
//                         error: error.message,
//                         data: "ERROR",
//                         message: 'Unable to  Add User Chat'
//                     })
//                 });
//             })
//     })
//     loginRouter.post('/Chat/Break/Update/', (req, res, next) => {
//         var sessionparam = req.body.userSession;
//         var outTime = req.body.outTime
//         db.any("update conversationtable set status='Break Requested' where supportid=(select fn_viewuserid from fn_viewuserid($1))", sessionparam).then((data) => {
//                 db.any("select * from supportattendance where supportid=(select fn_viewuserid from fn_viewuserid($1)) order by id desc", [sessionparam]).then((data1) => {
//                     db.any("update supportattendance set outtime=$1,status='Break added' where id=$2", [outTime, parseInt(data1[0].id)]).then((data) => {
//                         db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','INFO','Update of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
//                             if(data.length>0){
//                                 res.status(200).send({
//                                     result:true,
//                                     error:"NOERROR",
//                                     data:data,
//                                     message: " Break Details Has Been Updated Successfully"
//                                 })
//                             }else{
//                                 res.status(200).send({
//                                     result:true,
//                                     error:"NOERROR",
//                                     data:"NDF",
//                                     message: "No Data Found"
//                                 })
//                             }
//                         })
//                     })
//                 })
//             })
//             .catch(error => {
//                 db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','ERR','Update of ChatSupport Table unsuccessfull','ChatSupport',false)").then((log) => {
//                     console.log('ERROR:Cannot Update Break Details', error.message || error);
//                     res.status(200).send({
//                         result: false,
//                         error: error.message,
//                         data: "ERROR",
//                         message: 'Unable to  Update  Break Details'
//                     })
//                 });
//             })
//     })
//     // adding
//   loginRouter.post('/Chat/Add/Conversation', (req, res, next) => {
//     var supportId = req.body.support;
//     var roleName = req.body.roleName;
//     var type = req.body.type;
//     var supportAttendanceId;
//     var date = new Date;
//     var todayDate = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
//     db.any("select * from supportattendance where supportid=$1 order by id desc", [supportId]).then((data) => {
//         supportAttendanceId = data[0].id
//         if (data[0].date == todayDate && data[0].status == 'Break Activated') {
//             db.any("update supportattendance set status='Returned from Break' where id=$1", data[0].id).then((data) => {})
//         }
//         if (type == 'Login') {
//             db.any("insert into supportattendance(supportid,intime,date,status) values($1,current_time,current_date,'Active')", [supportId]).then(() => {})
//         } else if (type == 'Chat') {
//             db.any("update supportattendance set outtime=null,status='Active' where id=$1", [supportAttendanceId]).then(() => {})
//         } else {}
//     })
//     for (var i = 0; i < 3; i++) {
//         db.any('select * from fn_addconversationtable($1,$2)', [supportId, roleName]).then(() => {})
//             .catch(error => {
//                 console.log('ERROR:Unable to Add Conversation', error.message || error);
//             });
//     }
//     db.any("select userid from chatqueue where status='Waiting' limit 1").then((data) => {
//         if (data.length != 0) {
//             db.any('select username from userchat where id=($1)', [data[0].userid]).then((data1) => {
//                 db.any('select * from fn_updateconversation($1,$2,$3)', [data[0].userid, data1[0].username, supportId]).then((data2) => {
//                     db.any("update chatqueue set status='Assigned',endtime=current_timestamp where userid=$1", data[0].userid).then((update) => {
//                         res.status(200).send(data);
//                     })
//                 })
//             })
//         } else {
//             res.status(200).send({
//                 message: 'Added to conversation '
//             })
//         }
//     })
// })
// }
{
    loginRouter.post('/Chat/', (req, res, next) => {
        var supportuserId = req.body.support;
        db.any('insert into chat(supportUserId) values($1) ON CONFLICT (supportUserId) DO UPDATE SET userId=null', supportuserId).then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:chat','ITR','INFO','Addition of chat Table successfull','chat',true)").then((log) => {
                    res.status(200).send(data);
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:chat','ITR','ERR','Addition of chat Table unsuccessfull','chat',False)").then((log) => {
                    console.log('ERROR:Unable to View Staff Users', error.message || error);
                    res.status(200).send({
                        message: "Unable to View Staff Users"
                    })
                });
            })
    })
    loginRouter.post('/Chat/Queue/Add/', (req, res, next) => {
        var userId = req.body.userId;
        db.any("insert into chatqueue(userId,status,starttime) values($1,'Waiting',current_timestamp) returning userid", userId).then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:chat','ITR','INFO','Addition of chat Table successfull','chat',true)").then((log) => {
                    res.status(200).send(data);
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:chat','ITR','ERR','Addition of chat Table unsuccessfull','chat',False)").then((log) => {
                    console.log('ERROR:Unable to View Staff Users', error.message || error);
                    res.status(200).send({
                        message: "Unable to View Staff Users"
                    })
                });
            })
    })
    loginRouter.get('/ChatClient/:convoId', (req, res, next) => {
        var convoId = req.params.convoId;
        db.any('select userid from conversationtable where Id=$1', convoId).then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatClient','ITR','INFO','view of chat Table successfull','ChatClient',true)").then((log) => {
                    res.status(200).send(data);
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatClient','ITR','ERR','view of chat Table unsuccessfull','ChatClient',False)").then((log) => {
                    console.log('ERROR:Unable to View Staff Users', error.message || error);
                    res.status(200).send({
                        message: "Unable to View Staff Users"
                    })
                });
            })
    })
    loginRouter.get('/Chat/Check/Support/:supportType', (req, res, next) => {
        console.log(req.params)
        db.any("(with convoCount as(select supportid,count(userid) from conversationtable where supporttype=$1 and status='Inactive' group by 1)select COALESCE(supportid,0) from convoCount where count<3 order by count asc limit 1)", req.params.supportType).then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatClient','ITR','INFO','view of chat Table successfull','ChatClient',true)").then((log) => {
                    res.status(200).send(data);
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatClient','ITR','ERR','view of chat Table unsuccessfull','ChatClient',False)").then((log) => {
                    console.log('ERROR:Unable to View Staff Users', error.message || error);
                    res.status(200).send({
                        message: "Unable to View Staff Users"
                    })
                });
            })
    })
    loginRouter.get('/ChatSupport/:Id', (req, res, next) => {
        console.log(req.params)
        db.any('select supportId from conversationtable where userId=(($1))', req.params.Id).then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatSupport','ITR','INFO','view of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
                    res.status(200).send(data);
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatSupport','ITR','ERR','view of ChatSupport Table unsuccessfull','ChatSupport',False)").then((log) => {
                    console.log('ERROR:Unable to View Staff Users', error.message || error);
                    res.status(200).send({
                        message: "Unable to View Staff Users"
                    })
                });
            })
    })
    loginRouter.post('/Chat/', (req, res, next) => {
        var sessionparam = req.body.user;
        db.any('update chat set userId=(select fn_viewuserid from fn_viewuserid($1)) where supportuserId=(select max(supportuserId) from chat where userId is null) returning userId', sessionparam).then(function (data) {
                db.any("select * from chat where userid=$1", data[0].userid).then((chatdata) => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Chat','ITR','INFO','Update of Chat Table successfull','Chat',true)").then((log) => {
                        res.status(200).send(chatdata);
                    })
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Chat','ITR','ERR','Update of Chat Table unsuccessfull','Chat',False)").then((log) => {
                    console.log('ERROR:Unable to View Staff Users', error.message || error);
                    res.status(200).send({
                        message: "Unable to View Staff Users"
                    })
                });
            })
    })
    loginRouter.post('/Chat/Close/:convoId', (req, res, next) => {
        var userId = req.params.convoId;
        db.any("update conversationtable set status='completed' where Id=$1", userId).then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:userId','ITR','INFO','Update of userId Table successfull','userId',true)").then((log) => {
                    res.status(200).send(data);
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:userId','ITR','ERR','Update of userId Table unsuccessfull','userId',False)").then((log) => {
                    console.log('ERROR:Unable to View Staff Users', error.message || error);
                    res.status(200).send({
                        message: "Unable to View Staff Users"
                    })
                });
            })
    })
    loginRouter.get('/Chat/:chatId', (req, res, next) => {
        db.any('select * from messages where chatid=($1) order by createddate', req.params.chatId).then(function (data) {
                db.any("update messages set status='R' where chatId=$1", req.params.chatId).then((update) => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:chatId','ITR','INFO','View of chatId Table successfull','chatId',true)").then((log) => {
                        res.status(200).send(data);
                    })
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:chatId','ITR','ERR','View of chatId Table unsuccessfull','chatId',False)").then((log) => {
                    console.log('ERROR:Unable to View Staff Users', error.message || error);
                    res.status(200).send({
                        message: "Unable to View Staff Users"
                    })
                });
            })
    })
    loginRouter.get('/Chat/Support/:userSession', (req, res, next) => {
        var sessionparam = req.params.userSession;
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from chatview where pos=1 and cast(conversationid as int) in (select id from conversationtable where supportId=(select fn_viewuserid from fn_viewuserid($1)))', sessionparam).then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:chatview','ITR','INFO','View of chatview Table successfull','chatview',true)").then((log) => {
                    res.status(200).send(data);
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:chatview','ITR','ERR','View of chatview Table unsuccessfull','chatview',False)").then((log) => {
                    console.log('ERROR:Unable to View Staff Users', error.message || error);
                    res.status(200).send({
                        message: "Unable to View Staff Users"
                    })
                });
            })
    })
    loginRouter.get('/Chat/Support/Convo/:userId', (req, res, next) => {
        db.any('select Id from conversationtable where userId=(($1))', req.params.userId).then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatSupport','ITR','INFO','view of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
                    res.status(200).send(data);
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatSupport','ITR','ERR','view of ChatSupport Table unsuccessfull','ChatSupport',False)").then((log) => {
                    console.log('ERROR:Unable to View Staff Users', error.message || error);
                    res.status(200).send({
                        message: "Unable to View Staff Users"
                    })
                });
            })
    })
    loginRouter.get('/Chat/view/UsersChat/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select * from fn_viewuserschat($1)', i).then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatSupport','ITR','INFO','view of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
                    res.status(200).send(data);
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatSupport','ITR','ERR','view of ChatSupport Table unsuccessfull','ChatSupport',False)").then((log) => {
                    console.log('ERROR:Unable to View users Chat', error.message || error);
                    res.status(200).send({
                        message: "Unable to View users Chat"
                    })
                })
            });
    })
    loginRouter.get('/Chat/view/Conversation/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select * from fn_viewconversationbysupportid($1)', i).then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatSupport','ITR','INFO','view of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
                    res.status(200).send(data);
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatSupport','ITR','ERR','view of ChatSupport Table unsuccessfull','ChatSupport',False)").then((log) => {
                    console.log('ERROR:Unable to View Conversations', error.message || error);
                    res.status(200).send({
                        message: "Unable to View Conversations"
                    })
                })
            });
    })
    loginRouter.get('/Chat/view/Messages/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select * from fn_viewmessagesbyconversationid($1)', i).then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatSupport','ITR','INFO','view of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
                    res.status(200).send(data);
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatSupport','ITR','ERR','view of ChatSupport Table unsuccessfull','ChatSupport',False)").then((log) => {
                    console.log('ERROR:Unable to View Messages', error.message || error);
                    res.status(200).send({
                        message: "Unable to View Messages"
                    })
                });
            })
    })
    loginRouter.post('/Chat/Messages/Update/', (req, res, next) => {
        var i = req.body.convoId;
        db.any("update messagestable set status='R' where conversationid=$1", i).then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','INFO','Update of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
                    res.status(200).send(data);
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','ERR','Update of ChatSupport Table unsuccessfull','ChatSupport',false)").then((log) => {
                    console.log('ERROR:Unable to Update Messages', error.message || error);
                    res.status(200).send({
                        message: "Unable to Update Messages"
                    })
                })
            });
    })
    loginRouter.get('/Chat/view/Messages/Support/:sId', (req, res, next) => {
        var i = req.params.sId;
        db.any('select * from messagestable where fromuser=$1', i).then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','INFO','Update of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
                    res.status(200).send(data);
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:ChatSupport','ITR','ERR','view of ChatSupport Table unsuccessfull','ChatSupport',False)").then((log) => {
                    console.log('ERROR:Unable to View Messages', error.message || error);
                    res.status(200).send({
                        message: "Unable to View Messages"
                    })
                });
            })
    })
    // adding
    loginRouter.post('/Chat/Add/Conversation', (req, res, next) => {
        console.log(req.body)
        var supportId = req.body.support;
        var roleName = req.body.roleName;
        var type = req.body.type;
        var supportAttendanceId;
        var date = new Date;
        var todayDate = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
        db.any("select * from supportattendance where supportid=$1 and date=$2 order by intime desc ", [supportId,todayDate]).then((data) => {
            console.log(data)
            if(data.length>0)
            {
            supportAttendanceId = data[0].id
            var dbDate=new Date(data[0].date)
            
            console.log(dbDate,todayDate)
            if (data[0].status == 'Break Activated') {
                db.any("update supportattendance set status='Returned from Break' where id=$1", data[0].id).then((data) => {})
            }
        }
            if (type == 'Login') {
                db.any("insert into supportattendance(supportid,intime,date,status) values($1,current_time,current_date,'Active')", [supportId]).then(() => {})
            } else if (type == 'Chat') {
                db.any("update supportattendance set outtime=null,status='Active' where id=$1", [supportAttendanceId]).then(() => {})
            } else {}
        })
        for (var i = 0; i < 3; i++) {
            db.any('select * from fn_addconversationtable($1,$2)', [supportId, roleName]).then(() => {})
                .catch(error => {
                    console.log('ERROR:Unable to Add Conversation', error.message || error);
                });
        }
        db.any("select userid from chatqueue where status='Waiting' limit 1").then((data) => {
            if (data.length != 0) {
                db.any('select username from userchat where id=($1)', [data[0].userid]).then((data1) => {
                    db.any('select * from fn_updateconversation($1,$2,$3)', [data[0].userid, data1[0].username, supportId]).then((data2) => {
                        db.any("update chatqueue set status='Assigned',endtime=current_timestamp where userid=$1", data[0].userid).then((update) => {
                            res.status(200).send(data);
                        })
                    })
                })
            } else {
                res.status(200).send({
                    'message': 'Added to conversation table'
                })
            }
        })
    })
    loginRouter.post('/Chat/Add/New/Convo/', (req, res, next) => {
        var sessionparam = req.body.userSession;
        var convoId = req.body.convoId
        var supportId;
        db.any('select * from loginview where id=(select fn_viewuserid from fn_viewuserid($1))', sessionparam).then((user) => {
            supportId = user[0].id;
            db.any('select * from fn_addconversationtable($1,$2)', [user[0].id, user[0].rolename]).then(() => {})
                .catch(error => {
                    console.log('ERROR:Unable to Add Conversation', error.message || error);
                });
            db.any("select userid from chatqueue where status='Waiting' limit 1").then((data) => {
                if (data.length != 0) {
                    db.any('select username from userchat where id=($1)', [data[0].userid]).then((data1) => {
                        db.any('select * from fn_updateconversation($1,$2,$3)', [data[0].userid, data1[0].username, supportId]).then((data2) => {
                            db.any("update chatqueue set status='Assigned',endtime=current_timestamp where userid=$1", data[0].userid).then((update) => {
                                console.log(data)
                                res.status(200).send(data);
                            })
                        })
                    })
                } else {
                    res.status(200).send({
                        userid: 'Added to conversation table'
                    })
                }
            })
        })
    })
    loginRouter.post('/Chat/Add/Messages', (req, res, next) => {
        var message = req.body.messages;
        var from = req.body.fromuser;
        var to = req.body.touser;
        var Conversationid = req.body.conversationid;
        db.any('select * from fn_addmessagetable($1,$2,$3,$4)', [message, from, to, Conversationid]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-ADD:ChatSupport','ITR','INFO','ADD of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
                    res.status(200).send(data);
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-ADD:ChatSupport','ITR','ERR','ADD of ChatSupport Table unsuccessfull','ChatSupport',false)").then((log) => {
                    console.log('ERROR:Unable to Add Message', error.message || error);
                    res.status(200).send({
                        message: 'Unable to Add Message'
                    })
                });
            })
    })
    loginRouter.post('/Chat/Feedback/Add', (req, res, next) => {
        var convoId = req.body.convoId;
        db.any('insert into chatfeedback(outtime,conversationId) values(current_timestamp,$1) returning id', [convoId]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-ADD:ChatSupport','ITR','INFO','ADD of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
                    res.status(200).send(data);
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-ADD:ChatSupport','ITR','ERR','ADD of ChatSupport Table unsuccessfull','ChatSupport',false)").then((log) => {
                    console.log('ERROR:Unable to Add Out Time', error.message || error);
                    res.status(200).send({
                        message: 'Unable to Add Out Time'
                    })
                });
            })
    })
    loginRouter.post('/Chat/Feedback/Update', (req, res, next) => {
        var convoId = req.body.convoId;
        var rating = req.body.rating
        db.any('update chatfeedback set rating=$1 where conversationid=$2', [rating, convoId]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','INFO','Update of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
                    res.status(200).send(data);
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','ERR','Update of ChatSupport Table unsuccessfull','ChatSupport',false)").then((log) => {
                    console.log('ERROR:Unable to update chatfeedback', error.message || error);
                    res.status(200).send({
                        message: 'Unable to  update chatfeedback'
                    })
                });
            })
    })
    //update
    loginRouter.post('/Chat/Update/Conversation/', (req, res, next) => {
        var id = req.body.userChatId;
        var userName = req.body.userName;
        var supportId = req.body.supportId
        db.any('select * from fn_updateconversation($1,$2,$3)', [id, userName, supportId]).then((data) => {
                db.any("update chatqueue set status='Assigned',endtime=current_timestamp where userid=$1", id).then((update) => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','INFO','Update of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
                        res.status(200).send(data);
                    })
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','INFO','Update of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
                    console.log('ERROR:Cannot Update Conversation Details', error.message || error);
                    res.status(200).send({
                        message: 'Unable to  update  Conversation Details'
                    })
                });
            })
    })
    loginRouter.post('/Chat/Update/Logout/', (req, res, next) => {
        sessionparam = req.body.userSession;
        type = req.body.type;
        if (type == 'Break') {
            db.any("select * from supportattendance where supportid=(select fn_viewuserid from fn_viewuserid($1)) order by id desc", [sessionparam]).then((data) => {
                    db.any("Update supportattendance set status='Break Activated' where id=$1", data[0].id).then((data) => {
                        db.any("update conversationtable set status='loggedout' where supportid=(select fn_viewuserid from fn_viewuserid($1))", [sessionparam]).then((data) => {
                            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','INFO','Update of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
                                res.status(200).send(data);
                            })
                        })
                    })
                })
                .catch(error => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','INFO','Update of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
                        console.log('ERROR:Cannot Update Conversation Details', error.message || error);
                        res.status(200).send({
                            message: 'Unable to  update  Conversation Details'
                        })
                    });
                })
        } else {
            db.any("select * from supportattendance where supportid=(select fn_viewuserid from fn_viewuserid($1)) order by id desc", [sessionparam]).then((data) => {
                    db.any("Update supportattendance set status='loggedout' where id=$1", data[0].id).then((data) => {
                        db.any("update conversationtable set status='loggedout' where supportid=(select fn_viewuserid from fn_viewuserid($1))", [sessionparam]).then((data) => {
                            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','INFO','Update of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
                                res.status(200).send(data);
                            })
                        })
                    })
                })
                .catch(error => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','ERR','Update of ChatSupport Table unsuccessfull','ChatSupport',false)").then((log) => {
                        console.log('ERROR:Cannot Update Conversation Details', error.message || error);
                        res.status(200).send({
                            message: 'Unable to  update  Conversation Details'
                        })
                    });
                })
        }
    })
    loginRouter.post('/Chat/Add/UserChat', (req, res, next) => {
        var userName = req.body.user;
        db.any('select * from fn_adduserchat($1)', [userName]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Add:ChatSupport','ITR','INFO','Add of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
                    res.status(200).send(data);
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','ERR','Update of ChatSupport Table unsuccessfull','ChatSupport',false)").then((log) => {
                    console.log('ERROR:Unable to Add User Chat', error.message || error);
                    res.status(200).send({
                        message: 'Unable to  Add User Chat'
                    })
                });
            })
    })
    loginRouter.post('/Chat/Break/Update/', (req, res, next) => {
        var sessionparam = req.body.userSession;
        var outTime = req.body.outTime
        db.any("update conversationtable set status='Break Requested' where supportid=(select fn_viewuserid from fn_viewuserid($1))", sessionparam).then((data) => {
                db.any("select * from supportattendance where supportid=(select fn_viewuserid from fn_viewuserid($1)) order by id desc", [sessionparam]).then((data1) => {
                    db.any("update supportattendance set outtime=$1,status='Break added' where id=$2", [outTime, parseInt(data1[0].id)]).then((data) => {
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','INFO','Update of ChatSupport Table successfull','ChatSupport',true)").then((log) => {
                            res.status(200).send(data);
                        })
                    })
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:ChatSupport','ITR','ERR','Update of ChatSupport Table unsuccessfull','ChatSupport',false)").then((log) => {
                    console.log('ERROR:Cannot Update Break Details', error.message || error);
                    res.status(200).send({
                        message: 'Unable to  update  Break Details'
                    })
                });
            })
    })
}
//============================================================================ <Default> ==============================================================================================================
{
    loginRouter.get('/places/:Id', function (req, res, next) {
        var city_id = req.params.Id;
        db.any("select l.locationName||' - '||l.pincode as locationName from locations l join cities c on c.id=l.cityid where l.cityid=$1", city_id).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Signup','ITR','INFO','View of SIGNUP Table successfull','Signup',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Places Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "NO DATA FOUND"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Signup','ITR','ERR','View of SIGNUP Table unsuccessfull','Signup',False)").then((log) => {
                    console.log('ERROR:Unable to View Places', error.message || error);
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: " Unable to View Places"
                    });
                });
            })
    });
      //Get All  Contact Details 
      loginRouter.get('/ContactDetails/Verify/:inp/:inpData', (req, res, next) => {
        inp = req.params.inp;
        inpData = req.params.inpData;
        db.any('select * from ContactdetailsView where ' + inp + '=$1', [inpData]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Allcontactdetails','ITR','INFO','View of Allcontactdetails Table successfull','Allcontactdetails',true)").then((log) => {
                    if (data.length > 0) {
                        var msg = inp + ' already Exists!!';
                        console.log(msg);
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: msg
                        });
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "NO DATA FOUND"
                        });
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Allcontactdetails','ITR','ERR','View of Allcontactdetails Table unsuccessfull','Allcontactdetails',False)").then((log) => {
                    console.log('ERROR:Unable to View Contact Details', error.message || error);
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable To View Contact Details"
                    });
                });
            })
    })
    }

//======================================================================= <Forgot-Password> ==================================================================================================
    {
        loginRouter.get('/Forgotusername/:email', function (req, res, next) {
            var userEmail = req.params.email;
            db.any("select * from fn_forgotusername($1)", [userEmail]).then((data) => {
                    db.any("insert into DomainLogs(LogHeader, LogCode, LogType, Message, LoggedFor, Status) values('Table-View:Forgotpassword', 'ITR', 'INFO', 'View of Forgotpassword Table successfull ','Forgotpassword ',true)").then((log) => {
                        if (data.length > 0) {
                            mail.mailer(data[0].contactid, 'INFOBYT Username  Sending ', 'forgot ');
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "Forget Password Retrived Successfully"
                            });
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "Invalid User Details"
                            });
                        }
                    })
                })
                .catch(error => {
                    db.any("insert into DomainLogs(LogHeader, LogCode, LogType, Message, LoggedFor, Status) values('Table-View:Forgotpassword', 'ITR', 'ERR', 'View of Forgotpassword Table unsuccessfull ',' Forgotpassword ',False)").then((log) => {
                        console.log('ERROR:Unable to View Forget Password',
                            error.message || error);
                        res.status(200).send({
                            result: false,
                            error: "error.message",
                            data: "ERROR",
                            message: "Unable To View Forget Password"
                        });
                    });
                })
        });
        loginRouter.get('/Forgotpassword/:lId/:email', function (req, res, next) {
            var loginId = req.params.lId;
            var userEmail = req.params.email;
            console.log(loginId + " -- " + userEmail)
            db.any("select * from fn_forgotpassword($1,$2)", [loginId, userEmail]).then((data) => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Forgotpassword','ITR','INFO','View of Forgotpassword Table successfull','Forgotpassword',true)").then((log) => {
                        if (data.length > 0) {
                            mail.mailer(data[0].contactid, 'INFOBYT Password Reset', 'forgot');
                            if (data.length > 0) {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: data,
                                    message: "Forgetpassword Has Been Retrieved Successfully"
                                })
                            } else {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: "NDF",
                                    message: "NO DATA FOUND"
                                })
                            }
                        } else {
                            res.status(200).send({
                                message: "Invalid User Details"
                            });
                        }
                    })
                })
                .catch(error => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Forgotpassword','ITR','ERR','View of Forgotpassword Table unsuccessfull','Forgotpassword',False)").then((log) => {
                        console.log('ERROR:Unable to View Forget Password', error.message || error);
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to Retrieve Forget Password"
                        });
                    });
                })
        });
    }
//==============================================>Login <=======================================
{
    loginRouter.post('/Login', function (req, res, next) {
        var username = req.body.loginId;
        var password = req.body.password;
        console.log(req.body)
        var dtype = req.device.type.toUpperCase()
        var ip_info = get_ip(req);
        db.any("select * from authenticationmaster where username=$1", [username]).then((user) => {
                if (user.length > 0) {
                    db.any("select * from authenticationmaster where loginid=$1", user[0].loginid).then((authentication) => {
                        subString = (authentication[0].passcode + '').substring(0, 3)
                        passwordA = subString + password;
                        subString = (authentication[0].passcode + '').substring(3, 7)
                        passwordA = passwordA + subString;
                        ip = ip_info.clientIp.substring(7, 17);
                        db.any('select * from loginview where password=crypt($2,password) and loginid=(select loginid from authenticationmaster where username=$1)',[user[0].username, passwordA]).then((data) => {
                            if (data.length > 0) {
                                console.log('correct password')
                                db.any("select * from loginview where loginid=(select loginid from authenticationmaster where username=$1) and password=crypt($2,password) and loginstatus='INACTIVE'",[user[0].username,passwordA]).then((activeSucess)=>{
                                    console.log('activeSucess')
                                    console.log(activeSucess)
                                    if(activeSucess.length>0)
                                    {
                                global.user = data[0];
                                db.any("update authenticationmaster set online='Active',loginsfailed=0,lastaccessed=current_timestamp,devicetype=$4,accessedip=$5,accessedmid=$6,successfulllogins=((select successfulllogins from authenticationmaster where username=$1)+1),loginfreq=((select loginfreq from authenticationmaster where username=$2)+1) where username=$3 returning successfulllogins", [username, username, username, dtype, ip, username]).then((successfull) => {
                                    console.log(successfull)
                                    if (successfull[0].successfulllogins == 1) {
                                        db.any('update authenticationmaster set initiallogin=current_timestamp where username=$1', user[0].username).then((initial) => {})
                                    }
                                    db.any("select * from fn_addUserLogins($1);", [user[0].userid]).then((loginSession) => {
                                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Login','ITR','INFO','Addition of LOGIN Table successfull','Login',true)").then((d) => {
                                            userSession = loginSession[0].fn_adduserlogins;
                                            global.user.password = userSession;
                                            if (loginSession.length > 0) {
                                                res.status(200).send({
                                                    result: true,
                                                    error: "NOERROR",
                                                    data: global.user,
                                                    message: "authentication Has Been Added Successfully"
                                                })
                                            } else {
                                                res.status(200).send({
                                                    result: true,
                                                    error: "NOERROR",
                                                    data: "NDF",
                                                    message: "NO DATA FOUND"
                                                })
                                            }
                                        })
                                    })
                                })
                                    }
                                else
                                res.status(200).send({
                                    result: false,
                                    error: "NOERROR",
                                    data: "NDF",
                                    message: "Your Login Status is currently Not Activated. Please Contact Your Branch Admin."
                                })
                            })
                            } else {
                                console.log('Incorrect password')
                                db.any("update authenticationmaster set loginfreq=0,lastunsuccessfullaccess=current_timestamp,loginsfailed=((select loginsfailed from authenticationmaster where username=$1)+1) where username=$2 returning loginsfailed", [username, username]).then((unsuccessfull) => {
                                    if (unsuccessfull[0].loginsfailed == 3) {}
                                    res.status(200).send({
                                        result: true,
                                        error: "NOERROR",
                                        data: "NDF",
                                        message: "Incorrect Password"
                                    });
                                })
                            }
                        })
                    })
                } else {
                    db.any("update authenticationmaster set loginfreq=0,lastunsuccessfullaccess=current_timestamp,loginsfailed=((select loginsfailed from authenticationmaster where username=$1)+1) where username=$2 returning loginsfailed", [username, username]).then((unsuccessfull) => {
                        if (unsuccessfull[0].loginsfailed == 3) {}
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "Invalid User"
                        });
                    })
                }
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Login','ITR','ERR','Addition of LOGIN Table unsuccessfull','Login',False)").then((d) => {
                    console.log('ERROR:Cannot  login', error.message || error);
                    res.status(401).send({
                        result: false,
                        error: "error.message",
                        data: "ERROR",
                        message: " Cannot  login"
                    })
                })
            });
    });
}
//==============================================>SignUp <=======================================
{
    loginRouter.get('/verifyloginid/Verify/:inp/:inpData', (req, res, next) => {
        inp = req.params.inp;
        inpData = req.params.inpData;
        db.any('select * from authenticationmaster where username=$1', inpData).then((data) => {
            //console.log(data)
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Allcontactdetails','ITR','INFO','View of Allcontactdetails Table successfull','Allcontactdetails',true)").then((log) => {
                    if (data.length > 0) {
                        var msg = 'Username already Exists!!';
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: msg,
                            message: "Loginid Retrieved succesfully"
                        });
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No data found"
                        });
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Allcontactdetails','ITR','ERR','View of Allcontactdetails Table unsuccessfull','Allcontactdetails',False)").then((log) => {
                    console.log('ERROR:Unable to add', error.message || error);
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View loginid"
                    })
                });
            })
    })
 //PUT For  User
 loginRouter.post('/Update/:Id', (req, res, next) => {
    var i = req.params.Id;
    fnme = req.body.firstName
    mnme = req.body.middleName
    lnme = req.body.lastName
    dateofbrth = req.body.dob
    gndr = req.body.gender
    fathernme = req.body.fatherName
    mothernme = req.body.motherName
    fathroccption = parseInt(req.body.fatherOccupation)
    cste = req.body.caste
    subscste = req.body.subCaste
    rlgen = parseInt(req.body.religion)
    nationlty = parseInt(req.body.nationality)
    stat = req.body.status
    logid = req.body.loginid;
    db.any('select * from fn_updateusers($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14)', [i, fnme, mnme, lnme, dateofbrth, gndr, fathernme, mothernme, fathroccption, cste, subscste, rlgen, nationlty, stat]).then((data) => {
            db.any("select loginid from login where userid=$1", i).then((loginid) => {
                db.any('update authenticationmaster set username=$1 where loginid=$2', [logid, loginid[0].loginid]).then((data) => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:User','ITR','INFO','Update of User Table successfull','User',true)").then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: i,
                                message: "User Has Been Updated Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "NO DATA FOUND"
                            })
                        }
                    })
                })
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:User','ITR','ERR','Update of User Table unsuccessfull','User',False)").then((log) => {
                console.log('ERROR:Cannot Update Users', error.message || error);
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable To Update Users"
                });
            });
        })
})
//Update UserName in Authentication Table
loginRouter.post('/Update/Username/:Id', (req, res, next) => {
    var uid = req.body.Id;
    var uname = req.body.userName;
    var cuname = req.body.cuserName;
    var password = req.body.Password;
    db.any("select * from authenticationmaster where userid=$1", [uid]).then((user) => {
        if (user.length > 0) {
            db.any("select * from authenticationmaster where userid=$1", user[0].userid).then((authentication) => {
                subString = (authentication[0].passcode + '').substring(0, 3)
                passwordA = subString + password;
                subString = (authentication[0].passcode + '').substring(3, 7)
                passwordA = passwordA + subString;
                db.any('select * from loginview where password=crypt($1,password)', passwordA).then((data) => {
                        if (data.length > 0) {
                            console.log('correct password')
                            global.user = data[0];
                            db.any('update authenticationmaster set username=$2,lastmodified=current_timestamp where userid=$1', [uid, cuname]).then(() => {
                                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Login','ITR','INFO','Update of Login Table successfull','Login',true)").then((log) => {
                                    if (data.length > 0) {
                                        res.status(200).send({
                                            result: true,
                                            error: "NOERROR",
                                            data: data,
                                            message: " session Has Been Updated Successfully"
                                        })
                                    } else {
                                        res.status(200).send({
                                            result: true,
                                            error: "NOERROR",
                                            data: "NDF",
                                            message: "No Data Found"
                                        })
                                    }
                                })
                            })
                        }
                    })
                    .catch(error => {
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Login','ITR','ERR','Update of Login Table unsuccessfull','Login',False)").then((log) => {
                            console.log('ERROR: Unable to Update Username', error.message || error);
                            res.status(200).send({
                                result: false,
                                error: error.message,
                                data: ERROR,
                                message: " Unable To Update Username"
                            });
                        })
                    })
            })
        }
    })
})
 //get lastmodified
 loginRouter.get('/lastmodified/:userId', function (req, res, next) {
    var uid = req.params.userId;
    db.any("select loginid,username from authenticationmaster where userid=$1", uid).then((data) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Lastmodified Retrived Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "NO DATA FOUND"
                })
            }
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Login','ITR','ERR','View of LOGIN Table unsuccessfull','Login',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: " Unable to View User Session"
                });
            });
        })
});
//====================Check Permissions=========================
{
   
 loginRouter.get('/Check/Permissions/:Id/:component', function (req, res, next) {
    var uid = req.params.component;
    var sessionparam = req.params.Id;
    var roleId
    var roles;
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }

    db.any('select * from userprofileview where userId=(select fn_viewuserid from fn_viewuserid($1))', sessionparam).then((userRole) => {
        if(userRole.length>0){
        roleId = userRole[0].roleid;
        branchId = userRole[0].branchid;
    }
        db.any('select * from loginview where Id=(select fn_viewuserid from fn_viewuserid($1))', sessionparam).then((role) => {
            roles = role[0].rolename
            if(roles=='OrganizationAdmin')
            {
            branchId = 0
            roleId = role[0].roleid
            }
    db.any("select read,write from fn_viewpermissionsLogin($1,$2) where components=$3", [roleId, branchId,uid]).then((data) => {
    db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
            var userId = Session[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Update:Login','ITR','INFO','Permissions Checked Successfully','Login',true,$1)",userId).then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Permissions Check Retrived Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "NO DATA FOUND"
                })
            }
        })
    })
    }) })
})
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                var userId = Session[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Login','ITR','ERR','View of LOGIN Table unsuccessfull','Login',False,$1)",userId).then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: error,
                    message: " Unable to View Permissions"
                });
            });
        })
    })
});


    loginRouter.get('/Permission/View/Role', function (req, res, next) {
        var module = '';
        var firstList = [];
        var z = 0;
        var childData = [];
       
        db.any('select * from fn_viewpermissionsforrole()').then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:permissiontable','ITR','INFO','View of  permission Table successfull','Subject',true)").then((log) => {
                    firstList = data
                    for (var i = 0; i < firstList.length; i++) {
                        if (module == firstList[i].modules) {
                            childData[z - 1].components.push({
                                "component": firstList[i].components,
                                "componentid": firstList[i].componentid,
                                "read": firstList[i].read,
                                "write": firstList[i].write,
                                "roleid": firstList[i].roleid,
                                "branchid": firstList[i].branchid
                            })
                        }
                        else {
                            module = firstList[i].modules
                            icon=firstList[i].icon
                            childData.push({
                                "module": module,"icon":icon,
                                "components": [{
                                    "component": firstList[i].components,
                                    "componentid": firstList[i].componentid,
                                    "read": firstList[i].read,
                                    "write": firstList[i].write,
                                    "roleid": firstList[i].roleid,
                                    "branchid": firstList[i].branchid
                                }]
                            })
                            z = z + 1;
                        }
                    }
                    console.log(childData)
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: childData,
                            message: " Permission For Roles Retrived Sucessfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Login','ITR','ERR','View of LOGIN Table unsuccessfull','Login',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: " Unable to View Permissions For Roles"
                    });
                });
            })
        
    });
}


loginRouter.post('/Login/Social', function (req, res, next) {
    var socialLogId = req.body.loginId;
    var username;
    var userId;
    console.log(req.body)
    var dtype = req.device.type.toUpperCase()
    var ip_info = get_ip(req);
    ip = ip_info.clientIp.substring(7, 17);
    db.any('select * from sociallogins where socialid =$1', socialLogId).then((sociallogin) => {
        console.log(sociallogin)
        userId = sociallogin[0].userid;
        if (sociallogin.length > 0) {
            db.any("select * from loginview where contactuserid=$1 and loginstatus='INACTIVE'", sociallogin[0].userid).then((user) => {
                if (user.length > 0) {
                    global.user = user[0];
                }
                db.any("select username  from authenticationmaster where userid=$1", sociallogin[0].userid).then((uname) => {
                    console.log(uname)
                    username = uname[0].username
                    console.log(username)
                    db.any("update authenticationmaster set online='Active',loginsfailed=0,lastaccessed=current_timestamp,devicetype=$4,accessedip=$5,accessedmid=$6,successfulllogins=((select successfulllogins from authenticationmaster where username=$1)+1),loginfreq=((select loginfreq from authenticationmaster where username=$2)+1) where username=$3 returning successfulllogins", [username, username, username, dtype, ip, username]).then((successfull) => {
                        console.log(successfull)
                        if (successfull[0].successfulllogins == 1) {
                            db.any('update authenticationmaster set initiallogin=current_timestamp where username=$1', username).then((initial) => { })
                        }
                        console.log(sociallogin[0].userid)
                        console.log(userId)
                        db.any("select * from fn_addUserLogins($1);", [sociallogin[0].userid]).then((loginSession) => {
                            console.log(loginSession)
                            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Login','ITR','INFO','Addition of LOGIN Table successfull','Login',true)").then((d) => {
                                userSession = loginSession[0].fn_adduserlogins;
                                global.user.password = userSession;
                                console.log('global')
                                console.log(global.user)
                                if (loginSession.length > 0) {
                                    res.status(200).send({
                                        result: true,
                                        error: "NOERROR",
                                        data: global.user,
                                        message: "authentication Has Been Added Successfully"
                                    })
                                } else {
                                    res.status(200).send({
                                        result: true,
                                        error: "NOERROR",
                                        data: "NDF",
                                        message: "NO DATA FOUND"
                                    })
                                }
                            })
                        })
                    })
                })
            })
        }

    })


})

loginRouter.post('/Social/add', (req, res, next) => {
    console.log(req.body)
    var provider = req.body.provider;
    var socialId = req.body.socialId;
    var userId = req.body.userId;
    db.any('insert into sociallogins (socialid,provider,userid) values($1,$2,$3) ', [socialId, provider, userId]).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: Fee Payments','ITR','INFO','Addtion of socialLogins Table successfull',' Social logins',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "linked Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: social logins','ITR','ERR','Addtion of social logins Table unsuccessfull',' social logins',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to link."
                })
            })
        });
})


}
module.exports = loginRouter;