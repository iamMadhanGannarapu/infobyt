var express = require('express');
var bodyparser = require('body-parser');
var payslip = require('../payslip')
var fs = require('fs')
var path = require('path')
var mail = require('../mailer');
var batchRouter = express.Router()
batchRouter.use(express.static(path.resolve(__dirname, "public")));
batchRouter.use(bodyparser.json({
    limit: '5mb'
}));
batchRouter.use(bodyparser.urlencoded({
    limit: '5mb',
    extended: true
}));
var config = require('../init_app');
const dba = require('../db.js');
const db = dba.db;
const pgp = db.$config.pgp;
//=================================================Batch====================================================
{
     //POST FOR BATCH
     batchRouter.post('/Batch/Add', (req, res, next) => {
        var sid = req.body.staffId;
        var clsid = req.body.sessionId;
        var sd = req.body.startDate;
        var ed = req.body.endDate;
        var sts = 'Inactive';
        var nam = req.body.name;
       
        db.any('select * from fn_addbatch($1,$2,$3,$4,$5,$6)', [sid, clsid, sd, ed, sts, nam])
            .then((data) => {
               
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:  Batch','ITR','INFO','Addtion  of  Batch  Table  successfull',' Batch  table',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: " Batch Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: Batch','ITR','ERR','Addtion  of  Batch  Table un successfull',' Batch  table',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Batch "
                    })
                })
            });
    })
     // GET BY ID FOR BATCH
     batchRouter.get('/Batch/Details/:Id', (req, res) => {
        var i = req.params.Id
        db.any('select * from fn_viewbatchdetails($1)', i).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view: Batch','ITR','INFO','Retrive  of  Batch  Table  successfull',' Batch  table',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Batch Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view: Batch','ITR','ERR','Retrive  of  Batch  Table  unsuccessfull',' Batch  table',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Batch"
                    })
                })
            });
    })
    //GET FOR BATCH
    batchRouter.get('/Batch/:Id', (req, res) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewbatch($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view: Batch','ITR','INFO','Retrive  of  Batch  Table  successfull',' Batch  table',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Batch Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view: Batch','ITR','ERR','Retrive  of  Batch  Table  unsuccessfull',' Batch  table',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Batch"
                        })
                    })
                })
            });
    })
    //Get Batch by sessionId
    batchRouter.get('/Batch/Session/All/:Id', (req, res) => {
        var i = req.params.Id;
        db.any('select * from batchview where sessionid=$1', i).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view: Batch','ITR','INFO','Retrive  of  Batch  Table  successfull',' Batch  table',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: " Batch Details Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view: Batch','ITR','ERR','Retrive  of  Batch  Table  unsuccessfull',' Batch  table',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Batch Details"
                    })
                })
            });
    })
    //GET FOR BATCH by sessionid
    batchRouter.get('/Batch/Session/Batch/:Id', (req, res) => {
        var clsid = req.params.Id;
        db.any('select * from fn_viewbatchbysession($1)', clsid).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view: Batch','ITR','INFO','Retrive  of  Batch  Table  successfull',' Batch  table',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: " Batch Details Retreived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view: Batch','ITR','ERR','Retrive  of  Batch  Table  unsuccessfull',' Batch  table',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Batch Details"
                    })
                })
            });
    })
    //UPDATE BATCH
batchRouter.post('/Batch/Update/:Id', (req, res, next) => {
    var batchid = req.params.Id;
    var sid = req.body.staffId;
    var clsid = req.body.sessionId;
    var sd = req.body.startDate;
    var ed = req.body.endDate;
    var status = "Active";
    var na = req.body.name;
    db.any('select fn_updatebatch($1,$2,$3,$4,$5,$6,$7)', [batchid, sid, clsid, sd, ed, status, na])
        .then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:  Time Table','ITR','INFO','Update of  Batch  Table  successfull',' Batch  table',true)").then((log) => {
                //res.status(200).send(batchid);
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: batchid,
                        message: "Batch Has Been Updated Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:  Time Table','ITR','ERR','Update of  Batch  Table  unsuccessfull',' Batch  table',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Cannot Update Batch"
                })
            })
        });
});
     //get user transactions
     batchRouter.get('/transactions/:Id', (req, res) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewtransfertransaction($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:  Time Table','ITR','INFO','Retrive  of  Batch  Table  successfull',' Batch  table',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Transactions Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:  Time Table','ITR','ERR','Retrive  of  Batch  Table  unsuccessfull',' Batch  table',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Transactions"
                        })
                    })
                })
            });
    })
}
//==========================================Session-Master================================================
{
     //POST For session Master
     batchRouter.post('/Session/Add', (req, res, next) => {
        classname = req.body.name;
        db.any('select * from fn_addsessionmaster($1)', [classname]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Session','ITR','INFO','Addition of Session Table successfull','Session',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Session Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Session','ITR','ERR','Addition of Session Table unsuccessfull','Session',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                            error: error.message,
                            data: "ERROR",
                        message: " Unable to Add Session"
                    });
                })
            })
    })
      //GET For  Session Master 
      batchRouter.get('/Session/', (req, res, next) => {
        db.any('select * from fn_viewsessiones()').then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Session','ITR','INFO','Viewing of Session Table successfull','Session',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Sessions Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Viewing:Session','ITR','ERR','Viewing of Session Table unsuccessfull','Session',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                            error: error.message,
                            data: "ERROR",
                        message: "Unable to View Session"
                    })
                })
            });
    })
    //Get For Subject Master
    batchRouter.get('/Subject', (req, res, next) => {
        db.any('select * from fn_viewsubject()').then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Subject','ITR','INFO','View of SUBJECT Table successfull','Subject',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Sessions Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
        })
    })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Subject','ITR','ERR','View of SUBJECT Table successfull','Subject',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                     message: " Unable to View Subject" });
            });
        })
    })
    //Get By Id for session Master
    batchRouter.get('/Session/Details/:Id', (req, res, next) => {
        var id = req.params.Id;
        db.any('select * from fn_viewsessiondetails($1)', id).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Session','ITR','INFO','Viewing of Session Table successfull','Session',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Session Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Session','ITR','ERR','Viewing of Session Table unsuccessfull','Session',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                            error: error.message,
                            data: "ERROR",
                        message: " Unable to View Session Details"
                    })
                })
            });
    })
    //Update for  session Master
    batchRouter.post('/Session/Update/:Id', (req, res, next) => {
        cid = req.params.Id;
        var cnm = req.body.name;
        db.any('select * from fn_updatesession($1,$2)', [cid, cnm]).then(() => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Session','ITR','INFO','Viewing of Session Table successfull','Session',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Session Updatd Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Session','ITR','ERR','Viewing of Session Table unsuccessfull','Session',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                            error: error.message,
                            data: "ERROR",
                        message: "Cannot Update Session"
                    })
                })
            })
    })
}
//====================================================Session-Subject=========================================
{
     //POST for Session SUBJECT
     batchRouter.post('/Session/Subject/Add', (req, res, next) => {
        var bcid = req.body.branchSessionId;
        var sid = req.body.subjectId;
        var sbys = 'as on ' + new Date().toLocaleString();
        var bcmid = req.body.batchId;
        var fle = req.body.fileUpload
        var dire = './public/' + 'syllabus'
        if (!fs.existsSync(dire)) {
            fs.mkdirSync(dire);
        }
        db.any('select * from fn_addsessionsubject($1,$2,$3,$4)', [sid, sbys, bcmid, bcid])
            .then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:session Subject ','ITR','INFO','Addtion of session Subject Table  successfull','session Subject',true)").then((log) => {
                    var fn = data[0].fn_addsessionsubject + '.pdf';
                    fs.writeFile('./public/syllabus/' + fn, fle, (err) => { })
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Session Subject Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:session Subject ','ITR','ERR','Addtion of session Subject Table unsuccessfull','session Subject',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Session Subject"
                    })
                })
            });
    })
     //GET FOR Session SUBJECT
     batchRouter.get('/Session/Subject/:Id', (req, res) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewsessionsubject($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
             userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:session Subject ','ITR','INFO','View of session Subject Table  successfull','session Subject',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Session Subject Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    };
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:session Subject ','ITR','ERR','View of session Subject Table unsuccessfull','session Subject',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            message: "Unable to View Session Subject"
                        })
                    })
                })
            });
    })
    // GET BY ID FOR Session SUBJECT
    batchRouter.get('/Session/Subject/Details/:Id', (req, res) => {
        var i = req.params.Id
        db.any('select * from fn_viewsessionsubjectdetails($1)', i).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:session Subject ','ITR','INFO','View of session Subject Table  successfull','session Subject',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Session Subject Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                };
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:session Subject ','ITR','ERR','View of session Subject Table unsuccessfull','session Subject',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Session Subject"
                    })
                })
            })
    })
     //get trainer subject by id
     batchRouter.get('/Branch/Session/Subject/Details/:Id', (req, res) => {
        var i = req.params.Id
        db.any('select * from sessionsubjectview where branchsessionid= $1', i).then((data) => {
         
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: session Subject','ITR','INFO','View of session Subject Table  successfull','  session Subject',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Session Subject Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: session Subject','ITR','ERR','View of session Subject Table  unsuccessfull','  session Subject',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Session Subject "
                    })
                })
            });
    })
      // UPDATE FOR Session SUBJECT
      batchRouter.post('/Session/Subject/Update/:Id', (req, res, next) => {
        clssubid = req.params.Id;
        subid = req.body.subjectId;
        sybs = 'as on ' + new Date().toLocaleString();
        stfid = req.body.batchId;
        brnclsid = req.body.branchSessionId;
        db.any('select * from fn_updatesessionsubject($1,$2,$3,$4,$5)', [clssubid, subid, sybs, stfid, brnclsid]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:session Subject ','ITR','INFO','Update of session Subject Table  successfull','session Subject',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: clssubid,
                        message: "Session  Subject Has Been Updated Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:session Subject ','ITR','ERR','Update of session Subject Table unsuccessfull','session Subject',true)").then((log) => {
                    res.status(200).send({
                        message: "Cannot Update Session Subject"
                    })
                })
            });
    })
}
//========================================================Trainee Subject============================
{
    //Post a Session Subject Trainee
    batchRouter.post('/Session/Subject/Trainer/Add/', (req, res, next) => {
        var subjid = req.body.subjectId;
        var stafid = req.body.staffId;
        db.any('select * from fn_AddTrainerSubject($1,$2)', [subjid, stafid]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: Trainer Subject','ITR','INFO','Addtion of  Trainer Subject Table  successfull','  Trainer Subject',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Trainer Subject Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: Trainer Subject','ITR','ERR','Addtion of  Trainer Subject Table  unsuccessfull','  Trainer Subject',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Trainer Subject"
                    })
                })
            });
    })
  //get trainer subject
  batchRouter.get('/Session/Subject/Trainer/:userSession', (req, res) => {
    var sessionparam = req.params.userSession;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select * from fn_viewTrainersubject($1)', sessionparam).then((data) => {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
             userId = dbsession[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: Trainer Subject','ITR','INFO','View of Trainer Subject Table  successfull','  Trainer Subject',true,$1)",userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Trainer Subject Details Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: Trainer Subject','ITR','ERR','View of Trainer Subject Table  unsuccessfull','  Trainer Subject',False,$1)",userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Trainer Subject Details"
                    })
                })
            })
        });
})
//get trainers by subjectid
batchRouter.get('/Session/Subject/Trainer/Details/:Id', (req, res) => {
    var i = req.params.Id
    db.any('select * from fn_viewtrainersubjectdetails($1)', i).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Trainer Subject','ITR','INFO','View of Trainer Subject Table  successfull','  Trainer Subject',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Trainer Subject Details Retrived Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Trainer Subject','ITR','ERR','View of Trainer Subject Table  unsuccessfull','  Trainer Subject',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Trainer Subject Details"
                })
            })
        });
})
//Get Trainer Subject by SubjectId
batchRouter.get('/Session/Subject/Trainer/Subject/:Id/:session', (req, res) => {
    var id = req.params.Id
    var sessionparam = req.params.session
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select * from fn_viewtrainersubjectbysubject($1,$2)', [id, sessionparam]).then(function (data) {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
             userId = dbsession[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Trainer Subject','ITR','INFO','View of Trainer Subject Table  successfull','  Trainer Subject',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: " Trainer Subject Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Trainer Subject','ITR','ERR','View of Trainer Subject Table  unsuccessfull','  Trainer Subject',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Trainer Subject "
                    })
                })
            })
        });
})
 //update trainer subject
 batchRouter.post('/Session/Subject/Trainer/Update/:Id', (req, res, next) => {
    var id = req.params.Id;
    var subid = req.body.subjectId;
    var stfid = req.body.staffId;
    db.any('select * from fn_updatetrainersubject($1,$2,$3)', [id, subid, stfid]).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Trainer Subject','ITR','INFO','Update of Trainer Subject Table  successfull','  Trainer Subject',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: id,
                    message: "Trainer Subject Has Been Updated Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Trainer Subject','ITR','ERR','Update of Trainer Subject Table  unsuccessfull','  Trainer Subject',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Cannot Update Trainer Subject"
                })
            })
        });
})
}
//======================================================Staff====================================================
{
  // Add Staff
    batchRouter.post('/Staff/Add/:salaryPattern', (req, res, next) => {
        salaryPattern = req.params.salaryPattern
        var ui = req.body.userId;
        var bi = req.body.branchId;
        var doj = req.body.doj;
        var exp = req.body.experience;
        var sal = req.body.salary;
        var uri = req.body.userRoleId;
        var img = req.body.image
        var dpt = parseInt(req.body.departmentId)
        var sftid = parseInt(req.body.shiftId)
        var otpay = req.body.otPay;
        db.any('select * from fn_addstaff($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)', [ui, bi, doj, exp, sal, uri, salaryPattern, dpt, sftid, otpay])
            .then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Staff','ITR','INFO','Addition of STAFF Table successfull','Staff',true)").then((log) => {
                    fn = data[0].fn_addstaff + '.png';
                    ps = './public/staff/' + fn;
                    var dir = './public/' + 'staff';
                    if (!fs.existsSync(dir)) {
                        fs.mkdirSync(dir);
                    }
                    fs.writeFile(ps, img, 'base64', (err) => { })
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "SalaryPattern Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Staff','ITR','ERR','Addition of STAFF Table unsuccessfull','Staff',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: " Unable to Add Staff"
                    })
                });
            })
    })
     // Get For Staff
     batchRouter.get('/Staff/:Id', (req, res) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
        db.any('select * from fn_viewstaff($1)', sessionparam).then((data) => {
             userId = dbsession[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Staff','ITR','INFO','View of STAFF Table successfull','Staff',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Staff  Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Staff','ITR','ERR','View of STAFF Table unsuccessfull','Staff',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Staff"
                    })
                })
                });
            })
    })
     // Get By Id For Staff
     batchRouter.get('/Staff/Details/:Id', (req, res) => {
        var i = req.params.Id;
        db.any('select * from fn_viewstaffdetails($1)', i).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Staff','ITR','INFO','View of STAFF Table successfull','Staff',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Staff  Retrieved Succesfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Staff','ITR','ERR','View of STAFF Table unsuccessfull','Staff',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Staff Details"
                    })
                });
            })
    })
 

// Update  For Staff



// Update  For Staff
batchRouter.post('/Staff/Update/:Id', (req, res, next) => {
        stfid = parseInt(req.params.Id);
        usrid = req.body.userId;
        brnid = req.body.branchId;
        dj = req.body.doj;
        exp = parseFloat((req.body.dy)+'.'+(req.body.dm));
        sal = parseFloat(req.body.salary);
        usrrlid = req.body.userRoleId;
        dpt = parseInt(req.body.departmentId);
        sht = parseInt(req.body.shiftId);
        bank = req.body.bankName;
        ac = req.body.accountNumber;
        pn = req.body.panNumber;
        pf = req.body.pfAccountNumber;
        sp = req.body.salaryPattern;
        ot=parseFloat(req.body.otPay)
        db.any('select * from fn_updatestaff($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15)', [stfid, usrid, brnid, dj, exp, sal, usrrlid, bank, ac, pn, pf, sp, dpt, sht,ot]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Staff','ITR','INFO','Update of STAFF Table successfull','Staff',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: " Update Staff Succesfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Staff','ITR','ERR','Update of STAFF Table unsuccessfull','Staff',False)").then((log) => {
                    res.status(200).send({
    error: "NOERROR",
                        data: "NDF",
                        message: "Unable to Update"
    
    });
                });
            })
    })
    
    




















     //verify bank details
     batchRouter.get('/Staff/Verify/pfnum/:inp/:inpData', (req, res, next) => {
        inp = req.params.inp;
        inpData = req.params.inpData;
        db.any("select * from Accountdetails  where " + inp + '=$1', [inpData]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Account Details','ITR','INFO','View of Account Details Table successfull','Account Details',true)").then((log) => {
                if (data.length > 0) {
                    var msg = inp + ' already Exists!!';
                    console.log(msg)
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: msg
                    });
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: true
                    });
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Account Details','ITR','ERR','View of Account Details Table unsuccessfull','Account Details',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Accountdetails"
                    });
                });
            })
    })
     //verify Role details
     batchRouter.get('/Staff/Verify/Role/:inpData', (req, res, next) => {
        inpData = req.params.inpData;
        db.any("select * from permissions where roleid=$1", [inpData]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Account Details','ITR','INFO','View of Account Details Table successfull','Account Details',true)").then((log) => {
                if (data.length > 0) {
                    var msg = 'Role already Exists!!';
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: msg,
                        message: false
                    });
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: true
                    });
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Account Details','ITR','ERR','View of Account Details Table unsuccessfull','Account Details',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Accountdetails"
                    });
                });
            })
    })
}
//==============================================Trainee-Batch===========================================
{
     //post Trainee Batch
     batchRouter.post('/Trainee/Batch/Add', (req, res, next) => {
        batchId = req.body.batchId;
        studentId = req.body.traineeId;
        status = "Active"
        date = req.body.startDate;
        branchid = req.body.branchId;
        db.any('select * from fn_addtraineebatch($1,$2,$3,$4,$5)', [batchId, studentId, status, date, branchid]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:  Trainee Batch','ITR','INFO','Addtion of trainee Batch  Table  successfull',' traineeBatch  table',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Trainee Batch Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:  Trainee Batch','ITR','ERR','Addtion of trainee Batch  Table  unsuccessfull',' traineeBatch  table',false)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Trainee Batch"
                    })
                })
            })
    })
    //get Trainee Batch
    batchRouter.get('/Trainee/Batch/:Id', (req, res) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any("select * from fn_Viewtraineebatch($1) where stdbatchstatus='Active'", sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:  Trainee Batch','ITR','INFO','Retrive of trainee Batch  Table  successfull',' traineeBatch  table',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Trainee Batch Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:  Trainee Batch','ITR','ERR','Retrive of trainee Batch  Table  unsuccessfull',' traineeBatch  table',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Trainee Batch"
                        })
                    })
                })
            });
    })
    //get Batch By Session
    batchRouter.get('/Batch/sessionname/Details/:sessionId/:Id', (req, res) => {
        var id = req.params.Id;
        var sessionparam = req.params.sessionId;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewbatchsession($1,$2)', [id, sessionparam]).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:  Trainee Batch','ITR','INFO','Retrive of trainee Batch  Table  successfull',' traineeBatch  table',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Trainee Batch Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:  Trainee Batch','ITR','ERR','Retrive of trainee Batch  Table  unsuccessfull',' traineeBatch  table',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Trainee Batch"
                        })
                    })
                })
            });
    })
}
//===============================================Subject=================================================
{
     //Post Subject Master
     batchRouter.post('/Subject/Add', (req, res, next) => {
        name = req.body.name;
        db.any('select * from fn_addsubject($1)', [name]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Subject','ITR','INFO','Addition of SUBJECT Table successfull','Subject',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: " Subject Has Been Added Sucessfully"
                    })
                }
                else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Subject','ITR','ERR','Addition of SUBJECT Table unsuccessfull','Subject',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: " Unable to Add Subject"
                    })
                });
            })
    })
    // Get By Id For Subject 
    batchRouter.get('/Subject/Details/:Id', (req, res, next) => {
        var sessionparam = req.params.Id
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewsubjectdetails($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
             userId = dbsession[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Subject','ITR','INFO','View of SUBJECT Table successfull','Subject',true,$1)",userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Subject Details Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
        })
    })
    })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Subject','ITR','ERR','View of SUBJECT Table successfull','Subject',False,$1)",userId).then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                     message: " Unable to View Subject by Id" });
            });
        })
    })
    })
    // Update For Subject 
    batchRouter.post('/Subject/Update/:Id', (req, res, next) => {
        subid = req.params.Id;
        nme = req.body.name;
        db.any('select * from fn_updatesubject($1,$2)',
            [subid, nme]).then(() => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Subject','ITR','INFO','Update of SUBJECT Table successfull','Subject',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Subject Updated Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Subject','ITR','ERR','Update of SUBJECT Table successfull','Subject',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Cannot Update Subject" 
                });
            });
    })
})
}
//==========================================Time Table===================================================
{
      //Post for session timetable
      batchRouter.post('/Session/Timetable/Add', (req, res, next) => {
        var btcid = req.body.batchId;
        var typ = req.body.type;
        var remark = req.body.remarks;
        var fle = req.body.fileUpload;
        db.any('select fn_AddTimeTable($1,$2,$3)', [btcid, typ, remark])
            .then((Id) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: session Time Table','ITR','INFO','Addtion   of  session Time Table  successfull',' session Time table',true)").then((log) => {
                    var fn = Id[0].fn_addtimetable + '.pdf';
                    var dir = './public/timetable/';
                    if (!fs.existsSync(dir)) {
                        fs.mkdirSync(dir);
                    }
                    fs.writeFile('./public/timetable/' + fn, fle, (err) => { });
                    if (Id.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: Id,
                            message: " Timetable Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: Session Time Table','ITR','ERR','Addtion   of  session Time Table  unsuccessfull',' session Time table',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Time Table"
                    })
                })
            });
    })
     //Get For session Time table
     batchRouter.get('/Session/Timetable/:Id', (req, res) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewtimetable($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:  Time Table','ITR','INFO','Retrive  of Time Table  successfull','Time table',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: " Time Table Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:  Time Table','ITR','ERR','Retrive  of Time Table  unsuccessfull','Time table',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Time Table."
                        })
                    })
                })
            });
    })
     // GET By Id For TimeTable
     batchRouter.get('/Session/Timetable/Details/:Id', (req, res) => {
        var i = parseInt(req.params.Id)
        db.any('select * from fn_viewtimetabledetails($1)', i).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:  Time Table','ITR','INFO','Retrive  of Time Table  successfull','Time table',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Session Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:  Time Table','ITR','ERR','Retrive  of Time Table  unsuccessfull','Time table',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Time Table."
                    })
                })
            });
    })
     //Update For Time Table
     batchRouter.post('/Session/Timetable/Update/:Id', (req, res, next) => {
        i = req.params.Id;
        bi = req.body.batchId;
        tp = req.body.type;
        rm = req.body.remarks;
        db.any('select * from fn_updatetimetable($1,$2,$3,$4)', [i, bi, tp, rm]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:  Time Table','ITR','INFO','Update   of  Time Table  successfull','  Time table',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: " Timetable updated Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:  Time Table','ITR','ERR','Update   of  Time Table  unsuccessfull','  Time table',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Cannot Update Time Table"
                    })
                })
            });
    })
    batchRouter.post('/Staff/Add/Shift/details/', (req, res, next) => {
        userId = req.body.userId;
      shiftId=req.body.shiftId
        db.any('select * from fn_addstaffshifts($1,$2)', [userId, shiftId]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Shift','ITR','INFO','Addition of Shift Table successfull','Shift',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Shift Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Shift','ITR','ERR','Addition of Shift Table unsuccessfull','Shift',False)").then((log) => {
     res.status(200).send({
        result: false,
        error: error.message,
        data: "ERROR",
        message: "Unable to Add Shift Details"
    })
        });
    })
    });
}
//team and allocation
{


 //POST For Team Master
 

 //POST For Team Master
 batchRouter.post('/Team/Add/:Id', (req, res, next) => {

    teamname = req.body.Name;
    teamsize=req.body.Size;
    departmentid = req.body.Department;
    stfid=req.body.staffId
    var sessionparam = req.params.Id
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    var branchid=null
    db.any('select branchid,id from userprofileview where id=(select fn_viewuserid from fn_viewuserid($1))', [sessionparam]).then((bdata) => {
        branchid=bdata[0].branchid;
userId=bdata[0].id

   db.any('select * from fn_addteam($1,$2,$3,$4,$5)', [branchid,departmentid,teamname,teamsize,stfid]).then((data) => {
    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view:  Time Table','ITR','INFO','Retrive  of Time Table  successfull','Time table',true,($1))",userId).then((log) => {

    if (data.length > 0) {
               res.status(200).send({
                   result: true,
                   error: "NOERROR",
                   data: data,
                   message: "Team Added Successfully"
               })
           } else {
               res.status(200).send({
                   result: true,
                   error: "NOERROR",
                   data: "NDF",
                   message: "No Data Found"
               })
           }
       
   })
})
})
       .catch(error => {
               res.status(200).send({
                   result: false,
                       error: error.message,
                       data: "ERROR",
                   message: " Unable to Add Team"
               });
           })
       })



//get team by department
batchRouter.get('/Team/Department/:Id', (req, res) => {
console.log(req.params)
var did=req.params.Id 
db.any('select * from team where departmentid=$1', did).then((data) => {
   console.log(data)
   console.log(data)
       if (data.length > 0) {
           res.status(200).send({
               result: true,
               error: "NOERROR",
               data: data,
               message: "Team Retrieved Successfully"
           })
       } else {
           res.status(200).send({
               result: true,
               error: "NOERROR",
               data: "NDF",
               message: "No Data Found"
           })
       }
   

})
   .catch(error => {
       db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
            userId = dbsession[0].fn_viewuserid;
       db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Staff','ITR','ERR','View of STAFF Table unsuccessfull','Staff',False,$1)", userId).then((log) => {
           res.status(200).send({
               result: false,
               error: error.message,
               data: "ERROR",
               message: "Unable to View team"
           })
       })
       });
   })
})




//POST For TeamAllocation 
batchRouter.post('/Team/Allocation/Add/', (req, res, next) => {
    console.log(req.body)
    var exp = 0;
    var sftid = null
    var otpay = null
    var staffId=''
    bank = ''
    account =null
    pan = ''
    pf = null
    
     var teamList=req.body.teamAllocation
    var selectedList=req.body.selectedList
    
    function addTeamAllocation( selectedList,teamList,callback) {
    
    for (let i = 0; i < selectedList.length; i++) {
    if (selectedList[i].checkboxStatus == true) {
    if(teamList.salary){
        db.any('select * from fn_addstaff($1,$2,$3,$4,$5,$6,$7,$8,$9,$10) ',[selectedList[i].userid,selectedList[i].branchid,teamList.Doj,exp,teamList.salary,teamList.userRoleId,teamList.salaryPattern,teamList.departmentId,sftid,otpay]).then((sdata)=>{
    
            staffId=sdata[0].fn_addstaff
    
      db.any('select * from fn_addaccountdetails($1,$2,$3,$4,$5)', [bank, staffId, account, pan, pf]).then((data) => {
                    })
        })
    
    }
        db.any('select * from fn_addteamallocation($1,$2)', [selectedList[i].userid,teamList.teamId]).then((data) => {
            db.any("update  traineebatch set status='Inactive' where traineeid=(select admissionnum from admisiondetails where userid=$1)", [selectedList[i].userid]).then((data) => {
            })
        })
        .catch(error => {
            res.status(200).send({
                result: false,
                    error: error.message,
                    data: "ERROR",
                message: " Unable to Add Teamallocation"
            });
        })
        if(i==selectedList.length-1)
        {
            callback(true)
        }
    }
    }
    }
        
    
    addTeamAllocation(selectedList,teamList, function (ata) {
    if (ata) {
        res.status(200).send({
            result: true,
            error: "NOERROR",
            data: ata,
            message: "Teamallocation Added Successfully"
        })
    } else {
        res.status(200).send({
            result: true,
            error: "NOERROR",
            data: "NDF",
            message: "No Data Found"
        })
    } 
    })
       
        })

//get studentbatch by branch
batchRouter.get('/TraineeBatch/Branch/:Id', (req, res) => {
  console.log("hiii")
var sessionparam = req.params.Id
var userId='';
for (var i = 0; i < sessionparam.length; i++) {
    sessionparam = sessionparam.replace('-', '/');
}
db.any("select * from traineebatchview where stdbatchstatus='Active' and organizationbranchid=(select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid($1)))", sessionparam).then((data) => {
   console.log(data)

    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:  Time Table','ITR','INFO','Retrive  of Time Table  successfull','Time table',true)").then((log) => {
        if (data.length > 0) {
            res.status(200).send({
                result: true,
                error: "NOERROR",
                data: data,
                message: "Trainee Batch retrived Successfully"
            })
        } else {
            res.status(200).send({
                result: true,
                error: "NOERROR",
                data: "NDF",
                message: "No Data Found"
            })
        }
    })
})
    .catch(error => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:  Time Table','ITR','ERR','Retrive  of Time Table  unsuccessfull','Time table',False)").then((log) => {
            res.status(200).send({
                result: false,
                error: error.message,
                data: "ERROR",
                message: "Unable to View Trainee Batch."
            })
        })
    });
})

//GET FOR Teams
batchRouter.get('/Team/:Id', (req, res) => {
    console.log('hitt')
    var sessionparam = req.params.Id;
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select * from fn_getteams($1)', sessionparam).then((data) => {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
            var userId = dbsession[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view: Batch','ITR','INFO','Retrive  of  Batch  Table  successfull',' Batch  table',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Team Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                var userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view: Batch','ITR','ERR','Retrive  of  Batch  Table  unsuccessfull',' Batch  table',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Team"
                    })
                })
            })
        });
})

//get all teamallocation
batchRouter.get('/TeamAllocation/:Id', (req, res) => {
    var sessionparam = req.params.Id
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
   db.any('select * from fn_viewteamallocation($1)', sessionparam).then((data) => {
       console.log(data)
    
           if (data.length > 0) {
               res.status(200).send({
                   result: true,
                   error: "NOERROR",
                   data: data,
                   message: "Staff  Retrieved Successfully"
               })
           } else {
               res.status(200).send({
                   result: true,
                   error: "NOERROR",
                   data: "NDF",
                   message: "No Data Found"
               })
           }
       
  
})
       .catch(error => {
           db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid;
           db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Staff','ITR','ERR','View of STAFF Table unsuccessfull','Staff',False,$1)", userId).then((log) => {
               res.status(200).send({
                   result: false,
                   error: error.message,
                   data: "ERROR",
                   message: "Unable to View Staff"
               })
           })
           });
       })
})


//get teamallocation members by team
batchRouter.get('/TeamAllocation/ByTeam/:Id', (req, res) => {
  console.log(req.params)
var tid=req.params.Id 
   db.any('select * from teamallocationview where teamid=$1', tid).then((data) => {
     
       console.log(data)
           if (data.length > 0) {
               res.status(200).send({
                   result: true,
                   error: "NOERROR",
                   data: data,
                   message: "TeamAllocation By team Name  Retrieved Successfully"
               })
           } else {
               res.status(200).send({
                   result: true,
                   error: "NOERROR",
                   data: "NDF",
                   message: "No Data Found"
               })
           }
       
  
})
       .catch(error => {
           db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid;
           db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Staff','ITR','ERR','View of STAFF Table unsuccessfull','Staff',False,$1)", userId).then((log) => {
               res.status(200).send({
                   result: false,
                   error: error.message,
                   data: "ERROR",
                   message: "Unable to View TeamAllocation By team Name"
               })
           })
           });
       })
})


}
module.exports = batchRouter;