var express = require('express');
var bodyparser = require('body-parser');
var payslip = require('../payslip')
var fs = require('fs')
var path = require('path')
var mail = require('../mailer');
var transportRouter = express.Router()
transportRouter.use(express.static(path.resolve(__dirname, "public")));
transportRouter.use(bodyparser.json({
    limit: '5mb'
}));
transportRouter.use(bodyparser.urlencoded({
    limit: '5mb',
    extended: true
}));
var config = require('../init_app');
const dba = require('../db.js');
const db = dba.db;
const pgp = db.$config.pgp;
//===============< BUS DRIVER ALLOCATION >===============
{
    // Post For BusDriver//
    transportRouter.post('/Branch/Bus/Driver/Add', (req, res, next) => {
        var bid = req.body.busId;
        var drvrid = req.body.driverId;
        var alldt = req.body.date;
        var stu = "InActive";
        db.any('select * from fn_AddBusDrvAlocation($1, $2, $3,$4)', [bid, drvrid, alldt, stu])
            .then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: Bus Driver','ITR','INFO','Addtion of  Bus Driver Table  successfull','Bus Driver',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Bus Driver Has Been Added Sucessfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: Bus Driver','ITR','ERR','Addtion of  Bus Driver Table  unsuccessfull','Bus Driver',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Bus Driver"
                    })
                })
            });
    })
    // Get For Bus Driver
    transportRouter.get('/Branch/Bus/Driver/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select  * from fn_ViewBusDrvAlocation($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                 userId = dbSession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Bus Driver','ITR','INFO','View of  Bus Driver Table  successfull','Bus Driver',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: " Bus Driver Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                     userId = dbSession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Bus Driver','ITR','ERR','View of  Bus Driver Table  unsuccessfull','Bus Driver',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Bus Driver"
                        })
                    })
                })
            });
    })
    // Get  By Id for Bus Driver
    transportRouter.get('/Branch/Bus/Driver/Details/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select  * from fn_ViewbusDrvAlocationDetails($1)', i).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Bus Driver','ITR','INFO','View of  Bus Driver Table  successfull','Bus Driver',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Bus Driver Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Bus Driver','ITR','ERR','View of  Bus Driver Table  unsuccessfull','Bus Driver',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Bus Driver"
                    })
                })
            });
    })
    // Update Bus Driver
    transportRouter.post('/Branch/Bus/Driver/Update/:Id', (req, res, next) => {
        var id = req.params.Id;
        var bid = req.body.busId;
        var drid = req.body.driverId;
        var aldt = req.body.date;
        var stu = "Active"
        db.any('select * from fn_UpdateDriver($1,$2,$3,$4,$5)', [id, bid, drid, aldt, stu]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Bus Driver','ITR','INFO','View of  Bus Driver Table  successfull','Bus Driver',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: id,
                        message: "Session Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Bus Driver','ITR','ERR','View of  Bus Driver Table  unsuccessfull','Bus Driver',true)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Cannot Update Bus Driver"
                    })
                })
            });
    })
}
//===============< BUS MASTER >===============
{
    //POST FOR BUS
    transportRouter.post('/Bus/Add', (req, res, next) => {
        var reg = req.body.regNum;
        var desc = req.body.description;
        var brid = req.body.branchId;
        db.any('select * from fn_addbus($1,$2,$3)', [reg, desc, brid]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: Bus','ITR','INFO','Addtion of  Bus Table  successfull','  Bus',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Bus Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: Bus','ITR','ERR','Addtion of  Bus Table  unsuccessfull','  Bus',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Bus"
                    })
                })
            });
    })
    /* GET FOR BUS */
    transportRouter.get('/Branches/Bus/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select  * from fn_viewbus($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                 userId = dbSession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Bus','ITR','INFO','View of  Bus Table  successfull','  Bus',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Bus Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                     userId = dbSession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Bus','ITR','ERR','View of  Bus Table  unsuccessfull','  Bus',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Bus'"
                        })
                    })
                })
            });
    })
    /* GET BY ID FOR BUS  */
    transportRouter.get('/Branch/Bus/Details/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select * from fn_viewbusdetails($1)', i).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Bus','ITR','INFO','View of  Bus Table  successfull','  Bus',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Bus Details Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Bus','ITR','ERR','View of  Bus Table  unsuccessfull','  Bus',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Bus Details"
                    })
                })
            });
    })
    //UPDATE FOR BUS BY ID
    transportRouter.post('/Branch/Bus/Update/:Id', (req, res, next) => {
        var drvid = req.params.Id
        var reg = req.body.regNum;
        var desc = req.body.description;
        var brid = req.body.branchId;
        var lat = req.body.Latitude
        var long = req.body.Longitude
        db.any('select * from fn_updatebus($1,$2,$3,$4,$5,$6)', [drvid, reg, desc, brid, lat, long]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Bus','ITR','INFO','Update of  Bus Table  successfull','  Bus',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: drvid,
                        message: "Session Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Bus','ITR','ERR','Update of  Bus Table  unsuccessfull','  Bus',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Cannot Update Bus"
                    })
                })
            });
    });
}
//===============< TRANSPORT FEE MASTER >===============
{
    //Post For  Transportfee
    transportRouter.post('/Branch/Transport/Fee/Add', (req, res, next) => {
        var rid = req.body.routeId;
        var sid = req.body.stopId;
        var amt = req.body.amount;
        db.any('select * from fn_addtransportfee($1,$2,$3)', [rid, sid, amt]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Transport Fee','ITR','INFO','Addtion of Transport Fee Table  successfull','Transport Fee',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Transport Fee  Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Transport Fee','ITR','ERR','Addtion of Transport Fee Table unsuccessfull','Transport Fee',False)").then((log) => {
                    res.status(200).send({
                        message: "Unable to Add Transport Fee"
                    })
                })
            })
    })
    // Get For Transportfee
    transportRouter.get('/Branches/Transport/Fee/:Id', (req, res) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewtransportstopfee($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                 userId = dbSession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Transport Fee','ITR','INFO','View of Transport Fee Table  successfull','Transport Fee',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Transport Fee Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    };
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                     userId = dbSession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Transport Fee','ITR','ERR','View of Transport Fee Table unsuccessfull','Transport Fee',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Transport Fee"
                        })
                    })
                })
            });
    })
    // Get For Transportfee Details by id
    transportRouter.get('/Branch/Transport/Fee/Details/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select * from fn_viewtransportstopfeedetails($1)', i)
            .then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Transport Fee','ITR','INFO','View of Transport Fee Table  successfull','Transport Fee',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Transport Fee Details Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    };
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Transport Fee','ITR','ERR','View of Transport Fee Table unsuccessfull','Transport Fee',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Transport Fee Details"
                    })
                })
            });
    })
    // Put For Transportfee
    transportRouter.post('/Branch/Transport/Fee/Update/:Id', (req, res, next) => {
        i = req.params.Id;
        stid = req.body.stopId;
        rtid = req.body.routeId;
        amt = req.body.amount;
        amt = amt + '';
        db.any('select * from fn_updatetransportstopfee($1,$2,$3,$4)', [i, stid, rtid, amt]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Transport Fee','ITR','INFO','Update of Transport Fee Table  successfull','Transport Fee',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: i,
                        message: "Transport Fee Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Transport Fee','ITR','ERR','Update of Transport Fee Table  unsuccessfull','Transport Fee',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Transport Fee"
                    })
                })
            });
    })
    transportRouter.get('/Branch/Stops/route/:routeId', (req, res, next) => {
        var rid = req.params.routeId;
        db.any('select  * from fn_viewtransportstopmasterByRouteid($1)', rid).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Transport stops','ITR','INFO','View of  Transport stops Table  successfull','  Transport stops',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Transport Stops Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Transport stops','ITR','ERR','View of  Transport stops Table unsuccessfull','  Transport stops',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Transport Stops"
                    })
                })
            });
    })
}
//===============<TRANSPORT ROUTE MASTER >===============
{
    //POST FOR TRANSPORTROUTE
    transportRouter.post('/Transport/Route/Add', (req, res, next) => {
        name = req.body.name;
        from = req.body.startPoint;
        to = req.body.endPoint;
        id = req.body.busId;
        db.any('select * from fn_addtransportroute($1,$2,$3,$4)', [name, from, to, id]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: Transport Route','ITR','INFO','Addtion of  Transport Route Table  successfull','  Transport Route',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Transport Route Has Been Posted"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: Transport Route','ITR','ERR','Addtion of  Transport Route Table  unsuccessfull','  Transport Route',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Transport Route"
                    })
                })
            });
    })
    //GET FOR TRANSPORTROUTE
    transportRouter.get('/Transport/Route/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var UserId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_ViewTransportRoute($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Transport Route','ITR','INFO','View of  Transport Route Table  successfull','  Transport Route',true,$1)",userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Transport Route Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })})
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                    var userId = dbSession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Transport Route','ITR','ERR','View of  Transport Route Table  unsuccessfull','  Transport Route',False,$1)",userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Transport Route"
                        })
                    })
                })
            });
    })
    //GET BY ID FOR TRANSPORTROUTE
    transportRouter.get('/Transport/Route/Details/:Id', (req, res, next) => {
        var rid = req.params.Id;
        db.any('select * from fn_ViewTransportRouteDetails($1)', rid).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Transport Route','ITR','INFO','View of  Transport Route Table  successfull','  Transport Route',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Transport Route Details Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Transport Route','ITR','ERR','View of  Transport Route Table  unsuccessfull','  Transport Route',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Transport Route Details"
                    })
                })
            });
    })
    //UPDATE TRANSPORTROUTE BY ID
    transportRouter.post('/Transport/Route/Update/:Id', (req, res, next) => {
        routeid = req.params.Id;
        nme = req.body.name;
        stpoint = req.body.startPoint;
        edpoint = req.body.endPoint;
        bid = req.body.busId;
        db.any('select * from fn_updatetransportroute($1,$2,$3,$4,$5)', [routeid, nme, stpoint, edpoint, bid]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Transport Route','ITR','INFO','Update of  Transport Route Table  successfull','  Transport Route',true)").then((log) => {
                // res.status(200).send(routeid)
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: routeid,
                        message: "Transport Route Has Been Updated Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Transport Route','ITR','ERR','Update of  Transport Route Table  unsuccessfull','  Transport Route',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Cannot Update Transport Route"
                    })
                })
            });
    })
}
//===============< TRANSPORT STOPS >===============
{
    //Post For Trasport Stops
    transportRouter.post('/Branch/Bus/Stops/Add', (req, res, next) => {
        var ri = req.body.routeId;
        var sn = req.body.name;
        var at = req.body.arrivalTime;
        var dt = req.body.departureTime;
        var lat = req.body.latitude;
        var lng = req.body.longitude
        db.any('select * from fn_AddTransportStop($1,$2,$3,$4,$5,$6)', [ri, sn, at, dt, lat, lng]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: Transport stops','ITR','INFO','Addtion of  Transport stops Table  successfull','  Transport stops',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Transport Stop Has Been Added Sucessfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: Transport stops','ITR','ERR','Addtion of  Transport stops Table  unsuccessfull','  Transport stops',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Transport Stops"
                    })
                })
            });
    })
    // Get For Transport Stops
    transportRouter.get('/Branch/Bus/Stops/route/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select  * from fn_viewtransportstop($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                 userId = dbSession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Transport stops','ITR','INFO','View of  Transport stops Table  successfull','  Transport stops',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: " Transport Stops Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                     userId = dbSession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Transport stops','ITR','ERR','View of  Transport stops Table unsuccessfull','  Transport stops',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Transport Stops"
                        })
                    })
                })
            });
    })
    // Get By Id For Transport Stops
    transportRouter.get('/Branch/Bus/Stops/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        db.any('select  * from fn_viewTRansportStopDetails($1)', sessionparam).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Transport stops','ITR','INFO','View of  Transport stops Table  successfull','  Transport stops',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Transport Stop Details Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Transport stops','ITR','ERR','View of  Transport stops Table unsuccessfull','  Transport stops',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Transport Stop Details"
                    })
                })
            });
    })
    // Update For Transport Stops
    transportRouter.post('/Branch/Bus/Stops/Update/:Id', (req, res, next) => {
        var id = req.params.Id;
        var rid = req.body.routeId;
        var stpm = req.body.name;
        var arvtm = req.body.arrivalTime;
        var dptm = req.body.departureTime;
        var lat = req.body.latitude;
        var lng = req.body.longitude
        db.any('select * from fn_UpdateTransportStop($1,$2,$3,$4,$5,$6,$7)', [id, rid, stpm, arvtm, dptm, lat, lng]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Transport stops','ITR','INFO','Update of  Transport stops Table  successfull','  Transport stops',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: id,
                        message: "Transport Stops Has Been Updated Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Transport stops','ITR','ERR','Update of  Transport stops Table  unsuccessfull','  Transport stops',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Cannot Update Transport Stops"
                    })
                })
            });
    })
}
//===============< USER TRANSPORT>===============
{
    //Post For User Transport
    transportRouter.post('/Branch/Bus/Trainee/Add', (req, res, next) => {
        var uid = req.body.userId;
        var stid = req.body.stopId;
        db.any('select * from fn_AddUserTransport($1,$2)', [uid, stid]).then(() => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:User Transport ','ITR','INFO','Addtion of User Transport  Table  successfull','User  Transport ',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "User Transport Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:User Transport ','ITR','ERR','Addtion of User Transport  Table  unsuccessfull','User Transport ',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add User Trtansport"
                    })
                })
            });
    })
    // Get For User Transport
    transportRouter.get('/Branch/Bus/Trainee/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select  * from fn_ViewUserTransport($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                 userId = dbSession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:User Transport ','ITR','INFO','View of User Transport  Table  successfull','User  Transport ',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "User Transpport Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                     userId = dbSession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:User Transport ','ITR','ERR','View of User Transport Table unsuccessfull','User Transport ',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View User Transpport"
                        })
                    })
                })
            });
    })
    // Get By Id For User Transport
    transportRouter.get('/Branch/Bus/Trainee/Details/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select  * from fn_ViewUserTransportDetails($1)', i).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:User Transport ','ITR','INFO','View of User Transport  Table  successfull','User Transport ',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: " User Transport Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:User Transport ','ITR','ERR','View of User Transport Table unsuccessfull','User Transport ',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View User Transport "
                    })
                })
            });
    })
    //Update For User Transport
    transportRouter.post('/Branch/Bus/Trainee/Update/:Id', (req, res, next) => {
        i = req.params.Id;
        uid = req.body.userId;
        sid = req.body.stopId;
        db.any('select * from fn_UpdateUserTransport($1,$2,$3) ', [i, uid, sid]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:User Transport ','ITR','INFO','Update of User Transport  Table  successfull','User Transport ',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: i,
                        message: "Session Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:User Transport ','ITR','ERR','Update of User Transport Table unsuccessfull','User Transport ',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Cannot Update User Transport"
                    })
                })
            });
    })
    //get user transport fee transactions
    transportRouter.get('/transport/fee/transactions/:Id', (req, res) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewtransportfeetransaction($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                 userId = dbSession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:  Time Table','ITR','INFO','Retrive  of Fee Table  successfull','Fee table',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Transport Fee Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                     userId = dbSession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:  Time Table','ITR','ERR','Retrive  of Fee Table  unsuccessfull','Fee table',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Transport Fee"
                        })
                    })
                })
            });
    })
}
//===============< USER TRANSPORT PAYMENT>===============
{
    //Post For User Transport Payment
    transportRouter.post('/Branch/Bus/fee/Add', (req, res, next) => {
        var ui = req.body.userId;
        var pa = req.body.paidAmount;
        var pd = req.body.paidDate;
        var d = req.body.due;
        var st = req.body.status;
        db.any('select * from fn_addusertransortpayment($1,$2,$3,$4,$5)', [ui, pa, pd, d, st]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Transport Payment ','ITR','INFO','Addtion of  Transport Payment Table  successfull','Transport Payment',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "User Transport Payment Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Transport Payment ','ITR','ERR','Addtion of  Transport Payment Table  unsuccessfull','Transport Payment',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Transport Payment"
                    })
                })
            });
    })
    // Get For User Transport Payment
    transportRouter.get('/Branch/Bus/Fee/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select  * from fn_Viewusertransportpayment($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                 userId = dbSession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Transport Payment ','ITR','INFO','View of  Transport Payment Table  successfull','Transport Payment',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Transport Payment Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                     userId = dbSession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Transport Payment ','ITR','ERR','View of  Transport Payment Table  unsuccessfull','Transport Payment',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Transport Payment"
                        })
                    })
                })
            });
    })
    // Get By Id For User Transport Payment
    transportRouter.get('/Branch/Bus/Fee/Details/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select  * from fn_viewtransportfeepaymentsbyId($1)', i).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Transport Payment ','ITR','INFO','View of  Transport Payment Table  successfull','Transport Payment',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Transport Payment Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Transport Payment ','ITR','ERR','View of  Transport Payment Table  unsuccessfull','Transport Payment',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Transport Payment"
                    })
                })
            });
    })
    // Update For User Transport Payment
    transportRouter.post('/Branch/Bus/Fee/Update/:Id', (req, res, next) => {
        var id = req.params.Id;
        var uid = req.body.userId;
        var pd = req.body.paidAmount;
        var pdt = req.body.paidDate;
        var dptm = req.body.due;
        var stus = 'InActive';
        db.any('select * from fn_updateusertransportpayment($1,$2,$3,$4,$5,$6)', [id, uid, pd, pdt, dptm, stus]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Transport Payment ','ITR','INFO','Update of  Transport Payment Table  successfull','Transport Payment',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: id,
                        message: "Transport Payment Has Been Updated Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Transport Payment ','ITR','ERR','Update of  Transport Payment Table  unsuccessfull','Transport Payment',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Cannot Update Transport Payment"
                    })
                })
            });
    })
    // Get For User Transport Payment By user id
    transportRouter.get('/Branch/Bus/Fee/user/:Id', (req, res, next) => {
        var uid = req.params.Id;
        db.any('select  * from fn_viewusertransportpaymentByUserId($1)', uid).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Transport Payment ','ITR','INFO','View of  Transport Payment Table  successfull','Transport Payment',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Transport Payment Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Transport Payment ','ITR','ERR','View of  Transport Payment Table  unsuccessfull','Transport Payment',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to User Transport Payment"
                    })
                })
            })
    })
    // Get By BatchId For User Transport Payment
    transportRouter.get('/Branch/Bus/Fee/Batch/:Id/:session', (req, res, next) => {
        var id = req.params.Id;
        var sessionparam = req.params.session;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select  * from fn_viewTransportfeepaymentsBybatch($1,$2)', [id, sessionparam]).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                 userId = dbSession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Transport Payment ','ITR','INFO','View of  Transport Payment Table  successfull','Transport Payment',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Transport Fee Payment Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                     userId = dbSession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Transport Payment ','ITR','ERR','View of  Transport Payment Table  unsuccessfull','Transport Payment',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Transport Fee Payment"
                        })
                    })
                })
            });
    })
    // Get For Transportfee Details by user id
    transportRouter.get('/Branch/Transport/Fee/user/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select * from fn_viewtransportstopfeebyuserid($1)', i)
            .then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Transport Fee','ITR','INFO','View of Transport Fee Table  successfull','Transport Fee',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Transport Fee Details Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    };
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Transport Fee','ITR','ERR','View of Transport Fee Table unsuccessfull','Transport Fee',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Transport Fee Details"
                    })
                })
            });
    })
}
module.exports = transportRouter;