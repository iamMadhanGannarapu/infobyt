var express = require('express');
var bodyparser = require('body-parser');
var payslip = require('../payslip')
var fs = require('fs')
var path = require('path')
var mail = require('../mailer');
var dashboardRouter = express.Router()
dashboardRouter.use(express.static(path.resolve(__dirname, "public")));
dashboardRouter.use(bodyparser.json({
    limit: '5mb'
}));
dashboardRouter.use(bodyparser.urlencoded({
    limit: '5mb',
    extended: true
}));
var config = require('../init_app');
const dba = require('../db.js');
const db = dba.db;
const pgp = db.$config.pgp;
//-------------------------------------BulkMessages-------------------------------------
{
    //Bulk Message Add
    dashboardRouter.post('/Branch/BulkMessage/Add', (req, res, next) => {
        deptId = req.body.departmentId;
        classid = req.body.sessionId;
        batchid = req.body.batchId;
        msgType = req.body.messageType;
        msg = req.body.message;
        db.any('select * from fn_addBulkMessage($1,$2,$3,$4,$5)', [deptId, classid, batchid, msgType, msg]).then((Id) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Bulk Message','ITR','INFO','Addition of Bulk Message Table successfull',' Bulk Message',true)").then((log) => {
                if (Id.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: Id,
                        message: "Session Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Bulk Message','ITR','ERR','Addition of Bulk Message Table unsuccessfull',' Bulk Message',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Bulk Message"
                    })
                })
            });
    })
    //get all sessions
    dashboardRouter.get('/Branch/Department/Sessions/:deptId', (req, res, next) => {
        var i = req.params.deptId;
        db.any('select * from fn_getsessionbyDeparment($1)', i).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view: AllSessions','ITR','INFO','Retrive of AllSessions Table successfull',' AllSessions',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Session Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: AllSessions','ITR','ERR','Retrive of AllSessions Table unsuccessfull',' AllSessions',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View All Sessions"
                    })
                })
            })
    })
    //get all batches by classId
    dashboardRouter.get('/Branch/Sessions/Batch/:classId', (req, res, next) => {
        var i = parseInt(req.params.classId);
        db.any('select * from fn_getBtachesbysession($1)', 1).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view: Batches ','ITR','INFO','Retrive of Batches  Table successfull',' Batches ',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Session Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Batches ','ITR','ERR','Retrive of Batches  Table unsuccessfull',' Batches ',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Batches "
                    })
                })
            })
    })
    //get messages
    dashboardRouter.get('/Branch/MessageList/:userSession', (req, res, next) => {
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_getBranchMessages($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
             userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Messages List','ITR','INFO','Retrive  Details of Messages List Table successfull',' Messages List',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: " Messages List Retirved  Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Messages List','ITR','ERR','Retrive  Details of Messages List Table unsuccessfull',' Messages List',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View  Messages List "
                        })
                    })
                });
            });
    })
}
//--------------------------------------Dashboard---------------------------------------
{
    //get enqiry
    dashboardRouter.get('/User/Enquiry/', (req, res, next) => {
        db.any('select * from enquiry').then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:enquiry ','ITR','INFO','view of enquiry  Table successfull','enquiry ',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Enquiry Details Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:enquiry ','ITR','ERR','view of enquiry  Table unsuccessfull','enquiry ',False)").then((log) => {
                    res.status(200).send({
                        message: "Unable to View Enquiry "
                    })
                });
            })
    })
}
//---------------------------------------feedback------------------------------------------
{
    //Post For Feedback
    dashboardRouter.post('/Feedback/Add/:sessionparam', (req, res, next) => {
        var uid = req.body.userId;
        var fid = req.params.sessionparam;
        var fbdesp = req.body.description;
        var rtg = req.body.rating;
        for (var i = 0; i < fid.length; i++) {
            fid = fid.replace('-', '/');
        }
        db.tx(t => {
            return t.one('select fn_viewuserid from fn_viewuserid($1)', fid).then((data) => {
                fid = data.fn_viewuserid;
                db.tx(t => {
                    return t.one('select * from fn_AddFeedback($1,$2,$3,$4)', [uid, fid, fbdesp, rtg]).then((Id) => {
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-Add:Feedback','ITR','INFO','Add of Feedback Table successfull','Feedback',true,$1)",fid).then((log) => {
                            if (Id.length > 0) {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: Id,
                                    message: "Feedback Has Been Added Successfully"
                                })
                            } else {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: "NDF",
                                    message: "No Data Found"
                                })
                            }
                        })
                    })
                })
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-Add:Feedback','ITR','ERR','Add of Feedback Table unsuccessfull','Feedback',False,$1)",fid).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Feedback"
                    })
                })
            })
    })
   //GET FEEDBACK FUNCTION
   dashboardRouter.get('/Feedback/:sessionparam', (req, res, next) => {
    var fid = req.params.sessionparam;
    var userId = 0;
    for (var i = 0; i < fid.length; i++) {
        fid = fid.replace('-', '/');
    }
    db.any("select fn_viewuserid from fn_viewuserid($1)",fid).then((user)=>{
        userId = user[0].fn_viewuserid;
    db.any('select * from fn_Feedback($1)', fid).then(function (data) {
        // userId = dbsession[0].fn_viewuserid;
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Feedback','ITR','INFO','View of Feedback Table successfull','Feedback',true,$1)",userId).then((log) => {
            // res.status(200).send(data);
            if (data.length > 0) {
                //var msg = inp + ' already Exists!!';
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Feedback Details Retrieved succesfully"
                });
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No data found"
                });
            }
        })
    })})
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Feedback','ITR','ERR','View of Feedback Table unsuccessfull','Feedback',False,$1)",userId).then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Feedback"
                })
            });
        })
})
    //GET FEEDBACK BY ID  FUNCTION
    // dashboardRouter.get('/Feedback/Details/:Id', (req, res, next) => {
    //     var i = req.params.Id;
    //     db.any('select * from fn_feedbackdetails($1)', i).then(function (data) {
    //         db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:feedback details','ITR','INFO','View of feedback details Table successfull','feedback details',true)").then((log) => {
    //             if (data.length > 0) {
    //                 var msg = inp + ' already Exists!!';
    //                 res.status(200).send({
    //                     result: true,
    //                     error: "NOERROR",
    //                     data: data,
    //                     message: "Feedback Details Retrieved succesfully"
    //                 });
    //             } else {
    //                 res.status(200).send({
    //                     result: true,
    //                     error: "NOERROR",
    //                     data: "NDF",
    //                     message: "No data found"
    //                 });
    //             }
    //         })
    //     })
    //         .catch(error => {
    //             db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:feedback details','ITR','ERR','View of feedback details Table unsuccessfull','feedback details',False)").then((log) => {
    //                 res.status(200).send({
    //                     result: false,
    //                     error: error.message,
    //                     data: "ERROR",
    //                     message: "Unable to View Feedback Details"
    //                 })
    //             });
    //         })
    // })
    //update Feedback
    dashboardRouter.post('/Feedback/Update/:Id', (req, res, next) => {
        fdid = req.params.Id;
        usrid = req.body.userId;
        feedid = req.body.feederId;
        fbdesc = req.body.description;
        rat = req.body.rating;
        db.any('select * from fn_updatefeedback($1,$2,$3,$4,$5)', [fdid, usrid, feedid, fbdesc, rat]).then(() => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-update:feedback','ITR','INFO','update of feedback Table successfull','feedback',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Update Feedback Retrieved succesfully"
                    });
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No data found"
                    });
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-update:feedback','ITR','ERR','update of feedback Table unsuccessfull','feedback',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Update Feedback"
                    })
                })
            })
    })
}
//===============================================Idcard===============================
{
    //GET For Trainee Idcard
    dashboardRouter.get('/Trainee/Idcard/:userSession', (req, res, next) => {
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_idcard($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view: Idcard','ITR','INFO','view of Idcard Table successfull','Idcard',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Id card Retrived  Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view: Idcard','ITR','ERR','view of Idcard Table unsuccessfull','Idcard',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Id card"
                        })
                    });
                })
            })
    })
}
//=============================================Mail Box=======================================
{
   //get all branch users
   dashboardRouter.get('/Branch/Users/:Id', (req, res, next) => {
    var sessionparam = req.params.Id;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select * from userprofileview where branchid=(select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid($1)))', sessionparam).then(function (data) {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
         userId = dbsession[0].fn_viewuserid;
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Update:Shopping','ITR','INFO','Update of  Shopping Table successfull',' Shopping',true,$1)", userId).then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Branch Users Details Retrieved Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Update:Shopping','ITR','ERR','Update of Shopping Table unsuccessfull','  Shopping',False,$1)", userId).then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "error",
                    message: "Unable to View Branch Users Details."
                })
            });
        })
    })
})
    //add mail    
    dashboardRouter.post('/Mail/Add/:userSession', (req, res, next) => {
        var sessionparam = req.params.userSession
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        var toUser = req.body.toUser;
        var subject = req.body.subject;
        var mail = req.body.mail
        var userId
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
             userId = dbsession[0].fn_viewuserid;
            db.any('select *from fn_addmailbox($1,$2,$3,$4)', [parseInt(data[0].fn_viewuserid), parseInt(toUser), subject, mail]).then((Id) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Mail-Box','ITR','INFO','Addition of Mail-Box Table successfull','Mail-Box',true,$1)", userId).then((log) => {
                    if (Id.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: Id,
                            message: "Message Has Been Sent"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Mail-Box','ITR','ERR','Addition of Mail-Box Table unsuccessfull','Mail-Box',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "error",
                        message: "Unable To Send Message"
                    })
                });
            })
        })
    })
    //get inbox
    dashboardRouter.get('/mailbox/:type/:userSession', (req, res, next) => {
        var type = req.params.type
        var userId
        var sessionparam = req.params.userSession
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
            userId = dbsession[0].fn_viewuserid;
            if (type == 'Inbox') {
                db.any('select * from fn_viewmailbox() m join  users u on u.id=m.fromUser where touser=$1', data[0].fn_viewuserid).then(function (data) {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view:mailbox ','ITR','INFO','view of mailbox  Table successfull','mailbox',true,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Mail Retrieved Successfully"
                        })
                    })
                })
            } else if (type == 'Sent') {
                db.any('select * from fn_viewmailbox() m join  users u on u.id=m.toUser where fromuser=$1', data[0].fn_viewuserid).then(function (data) {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view:mailbox ','ITR','INFO','view of mailbox  Table successfull','mailbox ',true,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Mail Retrieved Successfully"
                        })
                    })
                })
            }
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view:mailbox ','ITR','ERR','view of mailbox  Table unsuccessfull','mailbox ',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "error",
                        message: "Unable to View mailbox "
                    })
                })
            })
        })
    })
}
//-------------------------------------notifications----------------------------------------
{
    //get for notification views
    dashboardRouter.get('/notification/:userSession', (req, res, next) => {
        sessionparam = req.params.userSession
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewNotifications($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Notifications','ITR','INFO','View of Notifications Table  successfull','Notifications',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Notification Retrieved Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Notifications','ITR','ERR','View of Notifications Table unsuccessfull','Notifications',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Notifications"
                        })
                    })
                })
            })
    })
 //notification for lev
 dashboardRouter.post('/notification/leave/add/:Id', (req, res, next) => {
            sessionparam = req.params.Id
            var notification = "A new leave request";
            var rcvr = '';
            var notificationid = 0;
            var userId='';
            for (var i = 0; i < sessionparam.length; i++) {
                sessionparam = sessionparam.replace('-', '/');
            }
            db.any('select * from fn_viewLevreceiverIds($1)', sessionparam).then((data) => {
                rcvr = data;
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    var userId = dbsession[0].fn_viewuserid;
                    db.any(' insert into notifications(notification) values ($1) returning id', notification).then((data) => {
                        notificationid = data[0].id;
                        for (var j = 0; j < rcvr.length; j++) {
                            db.any('select * from fn_addnotificationdetails($1,$2,$3,$4)', [rcvr[j].rolename, rcvr[j].userid, notificationid, sessionparam])
                                .then((Id) => {
                                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','INFO','Addition of Notifications Table  successfull','Notifications',true,$1)", userId).then((log) => {
                                        if (Id.length > 0) {
                                            res.status(200).send({
                                                result: true,
                                                error: "NOERROR",
                                                data: Id,
                                                message: "Leave Detail Notification Added Successfully"
                                            })
                                        } else {
                                            res.status(200).send({
                                                result: true,
                                                error: "NOERROR",
                                                data: "NDF",
                                                message: "No Data Found"
                                            })
                                        }
                                    })
                                })
                        }
                    })
                })
            })
                .catch(error => {
                    db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','ERR','Addition of Notifications Table unsuccessfull','Notifications',False,$1)", userId).then((log) => {
                            res.status(200).send({
                                result: false,
                                error: error.message,
                                data: "ERROR",
                                message: "Unable to Add Notifications"
                            })
                        })
                    })
                })
        })
         //notification for holidays
         dashboardRouter.post('/notification/Holiday/add/:Id', (req, res, next) => {
            sessionparam = req.params.Id
            var notification = "Holiday has been declared";
            var rcvr = '';
            var notificationid = 0;
            var userId='';
            for (var i = 0; i < sessionparam.length; i++) {
                sessionparam = sessionparam.replace('-', '/');
            }
            db.any('select * from fn_viewHolidayreceiverIds($1)', sessionparam).then((data) => {
                rcvr = data;
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any(' insert into notifications(notification) values ($1) returning id', notification).then((data) => {
                        notificationid = data[0].id;
                        for (var j = 0; j < rcvr.length; j++) {
                            db.any('select * from fn_addnotificationdetails($1,$2,$3,$4)', [rcvr[j].rolename, rcvr[j].userid, notificationid, sessionparam])
                                .then((Id) => {
                                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','INFO','Addition of Notifications Table  successfull','Notifications',true,$1)", userId).then((log) => {
                                        if (Id.length > 0) {
                                            res.status(200).send({
                                                result: true,
                                                error: "NOERROR",
                                                data: Id,
                                                message: "Holiday Notification Retrieved Successfully"
                                            })
                                        } else {
                                            res.status(200).send({
                                                result: true,
                                                error: "NOERROR",
                                                data: "NDF",
                                                message: "No Data Found"
                                            })
                                        }
                                    })
                                })
                        }
                    })
                })
                    .catch(error => {
                        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                             userId = dbsession[0].fn_viewuserid;
                            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','ERR','Addition of Notifications Table unsuccessfull','Notifications',False,$1)", userId).then((log) => {
                                res.status(200).send({
                                    result: false,
                                    error: error.message,
                                    data: "ERROR",
                                    message: "Unable to View Holiday Notification"
                                })
                            })
                        })
                    })
            })
        })
         //notification for classteacher when add to a batch
         dashboardRouter.post('/notification/batch/classteacher/add/:Id/:batchId', (req, res, next) => {
            sessionparam = req.params.Id
            var notification = " You have been added as a classteacher  ";
            var rcvr = '';
            var notificationid = 0;
            bid = req.params.batchId
            var userId='';
            for (var i = 0; i < sessionparam.length; i++) {
                sessionparam = sessionparam.replace('-', '/');
            }
            db.any('select * from fn_viewbatchreceiverIds($1,$2)', [sessionparam, bid]).then((data) => {
                rcvr = data;
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any(' insert into notifications(notification) values ($1) returning id', notification).then((data) => {
                        notificationid = data[0].id;
                        for (var j = 0; j < rcvr.length; j++) {
                            db.any('select * from fn_addnotificationdetails($1,$2,$3,$4)', [rcvr[j].rolename, rcvr[j].userid, notificationid, sessionparam])
                                .then((Id) => {
                                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','INFO','Addition of Notifications Table  successfull','Notifications',true,$1)", userId).then((log) => {
                                        if (Id.length > 0) {
                                            res.status(200).send({
                                                result: true,
                                                error: "NOERROR",
                                                data: Id,
                                                message: "Batch Notification Added Successfully"
                                            })
                                        } else {
                                            res.status(200).send({
                                                result: true,
                                                error: "NOERROR",
                                                data: "NDF",
                                                message: "No Data Found"
                                            })
                                        }
                                    })
                                })
                        }
                    })
                })
            })
                .catch(error => {
                    db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                         userId = dbsession[0].fn_viewuserid;
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','ERR','Addition of Notifications Table unsuccessfull','Notifications',False,$1)", userId).then((log) => {
                            res.status(200).send({
                                result: false,
                                error: error.message,
                                data: "ERROR",
                                message: "Unable to Add Batch Notifications"
                            })
                        })
                    })
                })
        })
         //getting notification when student allocated to one batch
         dashboardRouter.post('/notification/traineebatch/add/:Id', (req, res, next) => {
            sessionparam = req.params.Id
            var userId='';
            for (var i = 0; i < sessionparam.length; i++) {
                sessionparam = sessionparam.replace('-', '/');
            }
            var notification = " you have been moved to batch ";
            var receiverId;
            var receiverRolename;
            for (var i = 0; i < req.body.length; i++) {
                if (req.body[i].checkboxStatus == true) {
                    db.any('select userid from admisiondetails where admissionnum=$1', req.body[i].admissionnum).then((data) => {
                        receiverId = data[0].userid
                        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                             userId = dbsession[0].fn_viewuserid;
                            db.any('select rolename from userprofileview where id=($1)', receiverId).then((data) => {
                                receiverRolename = data[0].rolename
                                db.any(' insert into notifications(notification) values ($1) returning id', notification).then((data) => {
                                    notificationid = data[0].id;
                                    db.any('select * from fn_addnotificationdetails($1,$2,$3,$4)', [receiverRolename, receiverId, notificationid, sessionparam])
                                        .then((Id) => {
                                            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','INFO','Addition of Notifications Table  successfull','Notifications',true,$1)", userId).then((log) => {
                                                if (Id.length > 0) {
                                                    res.status(200).send({
                                                        result: true,
                                                        error: "NOERROR",
                                                        data: Id,
                                                        message: "Trainee Batch Notification Added Successfully"
                                                    })
                                                } else {
                                                    res.status(200).send({
                                                        result: true,
                                                        error: "NOERROR",
                                                        data: "NDF",
                                                        message: "No Data Found"
                                                    })
                                                }
                                            })
                                        })
                                })
                            })
                        })
                    })
                        .catch(error => {
                            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                                 userId = dbsession[0].fn_viewuserid;
                                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','ERR','Addition of Notifications Table unsuccessfull','Notifications',False,$1)", userId).then((log) => {
                                    res.status(200).send({
                                        result: false,
                                        error: error.message,
                                        data: "ERROR",
                                        message: "Unable to Add Notifications"
                                    })
                                })
                            })
                        })
                }
            }
        })
          //notification for classteacher and batch trainees when timetable added
          dashboardRouter.post('/notification/TimeTable/add/:Id/:timetableId', (req, res, next) => {
            sessionparam = req.params.Id
            var notification = " Time table has been added ";
            var rcvr = '';
            var notificationid = 0;
            tid = req.params.timetableId
            var batchId
            db.any('select batchid from timetable where id=$1', tid).then((data) => {
                batchId = data[0].batchid
                var userId='';
                for (var i = 0; i < sessionparam.length; i++) {
                    sessionparam = sessionparam.replace('-', '/');
                }
                db.any('select * from fn_viewtimetablereceiverIds($1,$2)', [sessionparam, batchId]).then((data) => {
                    rcvr = data;
                    db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                         userId = dbsession[0].fn_viewuserid;
                        db.any(' insert into notifications(notification) values ($1) returning id', notification).then((data) => {
                            notificationid = data[0].id;
                            for (var j = 0; j < rcvr.length; j++) {
                                db.any('select * from fn_addnotificationdetails($1,$2,$3,$4)', [rcvr[j].rolename, rcvr[j].userid, notificationid, sessionparam])
                                    .then((Id) => {
                                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','INFO','Addition of Notifications Table  successfull','Notifications',true,$1)", userId).then((log) => {
                                            if (Id.length > 0) {
                                                res.status(200).send({
                                                    result: true,
                                                    error: "NOERROR",
                                                    data: Id,
                                                    message: "Timetable Notification Added Successfully"
                                                })
                                            } else {
                                                res.status(200).send({
                                                    result: true,
                                                    error: "NOERROR",
                                                    data: "NDF",
                                                    message: "No Data Found"
                                                })
                                            }
                                        })
                                    })
                            }
                        })
                    })
                })
            })
                .catch(error => {
                    db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                         userId = dbsession[0].fn_viewuserid;
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','ERR','Addition of Notifications Table unsuccessfull','Notifications',False,$1)", userId).then((log) => {
                            res.status(200).send({
                                result: false,
                                error: error.message,
                                data: "ERROR",
                                message: "Unable to Add Notifications"
                            })
                        })
                    })
                })
        })
 //notification for classteacher and batch trainees when Appraisal timetable added
 dashboardRouter.post('/notification/appraisalTimeTable/add/:Id/:examtimetableId', (req, res, next) => {
    sessionparam = req.params.Id
    var notification = "  Exam Timetable has been added ";
    var rcvr = '';
    var notificationid = 0;
    ettid = req.params.examtimetableId
    var classsubjectId;
    db.any('select classsubjectid from examtimetable where id=$1', ettid).then((data) => {
        classsubjectId = data[0].classsubjectid
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewExamtimetablereceiverIds($1,$2)', [sessionparam, classsubjectId]).then((data) => {
            rcvr = data;
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any(' insert into notifications(notification) values ($1) returning id', notification).then((data) => {
                    notificationid = data[0].id;
                    for (var j = 0; j < rcvr.length; j++) {
                        db.any('select * from fn_addnotificationdetails($1,$2,$3,$4)', [rcvr[j].rolename, rcvr[j].userid, notificationid, sessionparam])
                            .then((Id) => {
                                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','INFO','Addition of Notifications Table  successfull','Notifications',true,$1)", userId).then((log) => {
                                    if (Id.length > 0) {
                                        res.status(200).send({
                                            result: true,
                                            error: "NOERROR",
                                            data: Id,
                                            message: "Timetable Notification Added Successfully"
                                        })
                                    } else {
                                        res.status(200).send({
                                            result: true,
                                            error: "NOERROR",
                                            data: "NDF",
                                            message: "No Data Found"
                                        })
                                    }
                                })
                            })
                    }
                })
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','ERR','Addition of Notifications Table unsuccessfull','Notifications',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Notifications"
                    })
                })
            })
        })
})
//count for notifications
dashboardRouter.get('/notification/count/:Id', (req, res, next) => {
    var sessionparam = req.params.Id;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    var stus = 'InActive';
    db.any('select * from fn_notificationcount($1)', sessionparam).then((data) => {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
             userId = dbsession[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Notifications','ITR','INFO','View of Notifications Table  successfull','Notifications',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: " Notifications Count Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Notifications','ITR','ERR','View of Notifications Table  unsuccessfull','Notifications',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Notifications Count"
                    })
                })
            })
        })
})
   //update for notifications
   dashboardRouter.post('/notification/Update/', (req, res, next) => {
    var stus = 'InActive';
    for (var i = 0; i < req.body.length; i++) {
        db.any('update notificationdetails set status=($2) where id=($1)', [req.body[i].id, stus]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Notifications','ITR','INFO','Update of Notifications Table  successfull','Notifications',true)")
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: " Notification Updated Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Notifications','ITR','ERR','Update of Notifications Table  unsuccessfull','Notifications',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Update Notifications"
                    })
                })
            })
    }
})
 //notification for trainee Marks
 dashboardRouter.post('/notification/traineemarks/add/:Id/:studentmarksid', (req, res, next) => {
    sessionparam = req.params.Id
    var notification = " A new marks report   ";
    var rcvr = '';
    var notificationid = 0;
    smid = req.params.studentmarksid
    var studentmarksstudentid;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select studentid from studentmarks where id=$1', smid).then((data) => {
        studentmarksstudentid = data[0].studentid
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
             userId = dbsession[0].fn_viewuserid;
            db.any('select * from fn_viewFeedBackreceiverids($1,$2)', [sessionparam, studentmarksstudentid]).then((data) => {
                rcvr = data;
                db.any(' insert into notifications(notification) values ($1) returning id', notification).then((data) => {
                    notificationid = data[0].id;
                    for (var j = 0; j < rcvr.length; j++) {
                        db.any('select * from fn_addnotificationdetails($1,$2,$3,$4)', [rcvr[j].rolename, rcvr[j].userid, notificationid, sessionparam])
                            .then((Id) => {
                                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','INFO','Addition of Notifications Table  successfull','Notifications',true,$1)", userId).then((log) => {
                                    if (Id.length > 0) {
                                        res.status(200).send({
                                            result: true,
                                            error: "NOERROR",
                                            data: Id,
                                            message: "Trainee Notifications Added Successfully"
                                        })
                                    } else {
                                        res.status(200).send({
                                            result: true,
                                            error: "NOERROR",
                                            data: "NDF",
                                            message: "No Data Found"
                                        })
                                    }
                                })
                            })
                    }
                })
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','ERR','Addition of Notifications Table unsuccessfull','Notifications',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Trainee Notifications"
                    })
                })
            })
        })
})
  //notification for gallery
  dashboardRouter.post('/notification/Gallery/add/:Id', (req, res, next) => {
    sessionparam = req.params.Id
    var notification = "Image has been added in gallery ";
    var rcvr = '';
    var notificationid = 0;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select * from fn_viewGalleryreceiverids($1)', sessionparam).then((data) => {
        rcvr = data;
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
             userId = dbsession[0].fn_viewuserid;
            db.any(' insert into notifications(notification) values ($1) returning id', notification).then((data) => {
                notificationid = data[0].id;
                for (var j = 0; j < rcvr.length; j++) {
                    db.any('select * from fn_addnotificationdetails($1,$2,$3,$4)', [rcvr[j].rolename, rcvr[j].userid, notificationid, sessionparam])
                        .then((Id) => {
                            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','INFO','Addition of Notifications Table  successfull','Notifications',true,$1)", userId).then((log) => {
                                if (Id.length > 0) {
                                    res.status(200).send({
                                        result: true,
                                        error: "NOERROR",
                                        data: Id,
                                        message: "Gallery Notification Added Successfully"
                                    })
                                } else {
                                    res.status(200).send({
                                        result: true,
                                        error: "NOERROR",
                                        data: "NDF",
                                        message: "No Data Found"
                                    })
                                }
                            })
                        })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','ERR','Addition of Notifications Table unsuccessfull','Notifications',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Gallery Notifications"
                    })
                })
            })
        })
})
}
//-------------------------------------------organization feedback-------------------------
{
    //Post For organization Feedback
    dashboardRouter.post('/User/Feedback/Add/:sessionparam', (req, res, next) => {
        var sid = req.params.sessionparam;
        var fid = req.params.sessionparam;
        var fbdesp = req.body.description;
        var rtg = req.body.rating;
        for (var i = 0; i < fid.length; i++) {
            fid = fid.replace('-', '/');
        }
        for (var i = 0; i < sid.length; i++) {
            sid = sid.replace('-', '/');
        }
        db.tx(t => {
            return t.one('select Organizationid from userprofileview where id=(select fn_viewuserid from fn_viewuserid($1))', fid).then((orgId) => {
                sid = orgId.organizationid;
                db.tx(t => {
                    return t.one('select fn_viewuserid from fn_viewuserid($1)', fid).then((usersId) => {
                        fid = usersId.fn_viewuserid;
                        db.tx(t => {
                            return t.one('select * from fn_AddOrganizationFeedback($1,$2,$3,$4)', [sid, fid, fbdesp, rtg]).then((Id) => {
                                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-Addtion:Organization Feedback','ITR','INFO','Addtion of Organization Feedback Table  successfull',' Organization Feedback',true,$1)",fid).then((log) => {
                                    if (Id.length > 0) {
                                        res.status(200).send({
                                            result: true,
                                            error: "NOERROR",
                                            data: Id,
                                            message: "Feedback Has been Posted"
                                        })
                                    } else {
                                        res.status(200).send({
                                            result: true,
                                            error: "NOERROR",
                                            data: "NDF",
                                            message: "No Data Found"
                                        })
                                    }
                                })
                            })
                        })
                    })
                })
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-Addtion:Organization Feedback','ITR','ERR','Addtion of Organization Feedback Table  unsuccessfull',' Organization Feedback',False,$1)",fid).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Feedback"
                    })
                })
            });
    })
    //Get For OrganizationFeedback
    dashboardRouter.get('/User/Feedback/:Id', (req, res) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_OrganizationFeedback($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Organization Feedback','ITR','INFO','View of Organization Feedback Table  successfull',' Organization Feedback',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Feedback Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Organization Feedback','ITR','ERR','View of Organization Feedback Table  unsuccessfull',' Organization Feedback',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Feedback"
                        })
                    })
                })
            });
    })
    // Get By Id For Feedback
    dashboardRouter.get('/User/Feedback/Details/:Id', (req, res, next) => {
        var i = parseInt(req.params.Id);
        db.any('select * from fn_OrganizationFeedbackDetails($1)', i).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Organization Feedback','ITR','INFO','View of Organization Feedback Table  successfull',' Organization Feedback',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Feedback Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Organization Feedback','ITR','ERR','View of Organization Feedback Table  unsuccessfull',' Organization Feedback',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Feedback"
                    })
                })
            });
    })
     //notification for feedback
     dashboardRouter.post('/notification/Feedback/add/:Id/:FeedbackId', (req, res, next) => {
        sessionparam = req.params.Id
        var notification = " A new feedback ";
        var rcvr = '';
        var notificationid = 0;
        fid = req.params.FeedbackId
        var Feedbackuserid;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select userid from feedback where id=$1', fid).then((data) => {
            Feedbackuserid = data[0].userid;
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any('select * from fn_viewFeedBackreceiverids($1,$2)', [sessionparam, Feedbackuserid]).then((data) => {
                    rcvr = data;
                    db.any(' insert into notifications(notification) values ($1) returning id', notification).then((data) => {
                        notificationid = data[0].id;
                        for (var j = 0; j < rcvr.length; j++) {
                            db.any('select * from fn_addnotificationdetails($1,$2,$3,$4)', [rcvr[j].rolename, rcvr[j].userid, notificationid, sessionparam])
                                .then((Id) => {
                                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','INFO','Addition of Notifications Table  successfull','Notifications',true,$1)", userId).then((log) => {
                                        if (Id.length > 0) {
                                            res.status(200).send({
                                                result: true,
                                                error: "NOERROR",
                                                data: Id,
                                                message: " Feedback Notification Added Successfully"
                                            })
                                        } else {
                                            res.status(200).send({
                                                result: true,
                                                error: "NOERROR",
                                                data: "NDF",
                                                message: "No Data Found"
                                            })
                                        }
                                    })
                                })
                        }
                    })
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','ERR','Addition of Notifications Table unsuccessfull','Notifications',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to Add Feedback Notifications"
                        })
                    })
                })
            })
    })
     //notification for Organizationfeedback
     dashboardRouter.post('/notification/organizationfeedback/add/:Id', (req, res, next) => {
        sessionparam = req.params.Id
        var notification = " A new feedback on Organization ";
        var rcvr = '';
        var notificationid = 0;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewOrganizationFeedBackreceiverids($1)', [sessionparam]).then((data) => {
            rcvr = data;
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any(' insert into notifications(notification) values ($1) returning id', notification).then((data) => {
                    notificationid = data[0].id;
                    for (var j = 0; j < rcvr.length; j++) {
                        db.any('select * from fn_addnotificationdetails($1,$2,$3,$4)', [rcvr[j].rolename, rcvr[j].userid, notificationid, sessionparam])
                            .then((Id) => {
                                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','INFO','Addition of Notifications Table  successfull','Notifications',true,$1)", userId).then((log) => {
                                    if (Id.length > 0) {
                                        res.status(200).send({
                                            result: true,
                                            error: "NOERROR",
                                            data: Id,
                                            message: "Organization Feedback Notification Added Successfully"
                                        })
                                    } else {
                                        res.status(200).send({
                                            result: true,
                                            error: "NOERROR",
                                            data: "NDF",
                                            message: "No Data Found"
                                        })
                                    }
                                })
                            })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','ERR','Addition of Notifications Table unsuccessfull','Notifications',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to Add Organization Feedback"
                        })
                    })
                })
            })
    })
    //notification for feepayments
    dashboardRouter.post('/notification/Feepayments/add/:Id/:Feepaymentid', (req, res, next) => {
        sessionparam = req.params.Id
        var notification = " Fee paid";
        var rcvr = '';
        var notificationid = 0;
        fpid = req.params.Feepaymentid;
        var Feepaymentuserid;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select studentid from feedback where id=$1', fpid).then((data) => {
            Feepaymentuserid = data[0].studentid
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any('select * from fn_viewFeepaymentsreceiverids($1,$2)', [sessionparam, Feepaymentuserid]).then((data) => {
                    rcvr = data;
                    db.any(' insert into notifications(notification) values ($1) returning id', notification).then((data) => {
                        notificationid = data[0].id;
                        for (var j = 0; j < rcvr.length; j++) {
                            db.any('select * from fn_addnotificationdetails($1,$2,$3,$4)', [rcvr[j].rolename, rcvr[j].userid, notificationid, sessionparam])
                                .then((Id) => {
                                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','INFO','Addition of Notifications Table  successfull','Notifications',true,$1)", userId).then((log) => {
                                        if (Id.length > 0) {
                                            res.status(200).send({
                                                result: true,
                                                error: "NOERROR",
                                                data: Id,
                                                message: "Fee Payments Notification Added Successfully"
                                            })
                                        } else {
                                            res.status(200).send({
                                                result: true,
                                                error: "NOERROR",
                                                data: "NDF",
                                                message: "No Data Found"
                                            })
                                        }
                                    })
                                })
                        }
                    })
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','ERR','Addition of Notifications Table unsuccessfull','Notifications',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to Add Fee Payments Notifications"
                        })
                    })
                })
            })
    })
    //notification for Achievements
    dashboardRouter.post('/notification/Achievements/add/:Id', (req, res, next) => {
        sessionparam = req.params.Id
        var notification = " A new Achievement has been added ";
        var rcvr = '';
        var notificationid = 0;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewAchievementsreceiverids($1)', [sessionparam]).then((data) => {
            rcvr = data;
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any(' insert into notifications(notification) values ($1) returning id', notification).then((data) => {
                    notificationid = data[0].id;
                    for (var j = 0; j < rcvr.length; j++) {
                        db.any('select * from fn_addnotificationdetails($1,$2,$3,$4)', [rcvr[j].rolename, rcvr[j].userid, notificationid, sessionparam])
                            .then((Id) => {
                                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','INFO','Addition of Notifications Table  successfull','Notifications',true,$1)", userId).then((data) => {
                                    if (Id.length > 0) {
                                        res.status(200).send({
                                            result: true,
                                            error: "NOERROR",
                                            data: Id,
                                            message: "Achievements Notification Added Successfully"
                                        })
                                    } else {
                                        res.status(200).send({
                                            result: true,
                                            error: "NOERROR",
                                            data: "NDF",
                                            message: "No Data Found"
                                        })
                                    }
                                })
                            })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','ERR','Addition of Notifications Table unsuccessfull','Notifications',false,$1)", userId)
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Achievements Notifications"
                    })
                })
            })
    })
}
//-------------------------------shopping---------------------------------------------
{
    //Shpoing
    dashboardRouter.post('/shopping/add', (req, res, next) => {
        var productname = req.body.productName;
        var productprice = req.body.productPrice;
        var desc = req.body.description;
        var category = req.body.category;
        var classid = req.body.sessionId;
        var productdate = req.body.productDate;
        var available = 'false';
        img = req.body.image
        db.any('select * from fn_addseller($1,$2,$3,$4,$5,$6,$7)', [productname, productprice, desc, category, classid, available, productdate])
            .then(function (Id) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:shopping','ITR','INFO','Addition of shopping Table successfull','shopping',true)").then((log) => {
                    fn = Id[0].fn_addseller + '.png';
                    ps = './public/Shopping/' + fn;
                    var dir = './public/' + 'Shopping';
                    if (!fs.existsSync(dir)) {
                        fs.mkdirSync(dir);
                    }
                    fs.writeFile(ps, img, 'base64', (err) => { 
                    })
                    if (Id.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: Id,
                            message: "Product is Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:shopping','ITR','ERR','Addition of shopping Table unsuccessfull','shopping',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add product Details"
                    })
                });
            })
    })
    //add cart
    dashboardRouter.post('/cart/add', (req, res, next) => {
        var sellerid = req.body.sellerid;
        var userid = req.body.cartallocationid;
        var quantity = 1;
        var status = 'Active'
        db.any('select * from cartallocation where admissionnum=$1 and orderstatus=$2', [userid, status])
            .then(function (data) {
                var cartallocationid = data[0].id
                db.any('select * from fn_addcart($1,$2,$3,$4)', [sellerid, quantity, cartallocationid, status])
                    .then(function (Id) {
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:cart','ITR','INFO','Addition of cart Table successfull','cart',true)").then((log) => {
                            if (Id.length > 0) {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: Id,
                                    message: "Product is Added to cart Successfully"
                                })
                            } else {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: "NDF",
                                    message: "No Data Found"
                                })
                            }
                        })
                    })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:cart','ITR','ERR','Addition of cart Table unsuccessfull','cart',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add product to cart"
                    })
                });
            })
    })
    //view shopping products
    dashboardRouter.get('/shopping/view', function (req, res, next) {
        db.any("select * from fn_viewshoppingproducts()").then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:shopping','ITR','INFO','View of shopping Table successfull','shopping',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Shopping Products Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:shopping','ITR','ERR','View of shopping Table unsuccessfull','shopping',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "error",
                        message: " Unable to View Shopping"
                    })
                });
            })
    });
    // View Shoping products
    dashboardRouter.get('/shopping/viewbyid/:id', function (req, res, next) {
        var id = req.params.id;
        db.any("select * from fn_viewshoppingproductsbyid($1)", id).then((data) => {
            var userId = data[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:seller','ITR','INFO','View of Shopping Details Table successfull','Shopping Details',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Shopping Products Details Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:seller','ITR','ERR','View of Shopping Details Table unsuccessfull','Shopping Details',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "error",
                        message: " Unable to View shopping Details "
                    })
                });
            })
    });
 // view products of user
 dashboardRouter.get('/shopping/view/users', function (req, res, next) {
    db.any("select * from fn_viewusersproducts()").then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:shopping','ITR','INFO','View of shopping Table successfull','shopping',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Users Shopping Products Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:shopping','ITR','ERR','View of shopping Table unsuccessfull','shopping',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "error",
                    message: " Unable to View Shopping"
                })
            });
        })
});
//view cart
dashboardRouter.get('/cart/view', function (req, res, next) {
    db.any("select * from fn_viewcartproducts()").then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:cart','ITR','INFO','View of cart Table successfull','cart',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Cart Details Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:cart','ITR','ERR','View of cart Table unsuccessfull','cart',False)").then((log) => {
                res.status(200).send({
                    message: " Unable to View cart"
                });
            });
        })
});
 // update shopping
 dashboardRouter.post('/shopping/update/:Id', (req, res, next) => {
    i = req.params.Id;
    st = req.body.availabile;
    if (st == true) {
        st = "true"
    } else {
        st = "false"
    }
    img = req.body.image
    db.any('select * from fn_updateproductavailability($1,$2)', [i, st]).then((Id) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Shopping','ITR','INFO','Update of  Shopping Table successfull',' Shopping',true)").then((log) => {
                ps = './public/slogo/' + i + '.png';
                fs.writeFile(ps, img, 'base64', (err) => {
                    if (err)
                        console.log(err)
                    else {
                        console.log('Image Saved');
                    }
                    if (Id.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: Id,
                            message: " Shopping Updated Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Shopping','ITR','ERR','Update of Shopping Table unsuccessfull','  Shopping',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "error",
                    message: "Cannot Update Shopping"
                })
            })
        });
})
 //Get By Id For  Contact Details 
 dashboardRouter.get('/ContactDetails/:userId', (req, res, next) => {
    i = parseInt(req.params.userId)
    db.any('select * from fn_viewcontactdetails($1)',i).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:ContactDetails','ITR','INFO','View of ContactDetails Table successfull','ContactDetails',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data[0],
                        message: "ContactDetails Has Been Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "NO DATA FOUND"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:ContactDetails','ITR','ERR','View of ContactDetails Table unsuccessfull','ContactDetails',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable To View Contact Details "
                });
            });
        })
})
 // deleting cart product
 dashboardRouter.delete('/cart/delete/:Id', (req, res, next) => {
    i = req.params.Id;
    db.any('DELETE FROM cart WHERE id=$1', i)
        .then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Deletion:product Details','ITR','INFO','Deletion of product Details Table successfull','product Details',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Product Deleted from Cart Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Deletion:product Details','ITR','ERR','Deletion of product Details Table unsuccessfull','product Details',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "error",
                    message: "Unable to delete product Details"
                })
            });
        })
})
//view cart allocation
dashboardRouter.get('/cartallocation/view/:Id', function (req, res, next) {
    var userid = req.params.Id;
    var status = 'Active'
    db.any("select * from cartallocation where userid=$1 and orderstatus=$2", userid, status).then((data) => {
            var userId = data[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:seller','ITR','INFO','View of seller Table successfull','seller',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "seller Details Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:seller','ITR','ERR','View of seller Table unsuccessfull','seller',False,$1)", userId).then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "error",
                    message: " Unable to View seller"
                });
            });
        })
});
 // checkout
 dashboardRouter.post('/cart/checkout/:userid', (req, res, next) => {
    userid = req.params.userid
    for (let i = 0; i < req.body.length; i++)
        var cartid = req.body[i].cartallocationid;
    var status = 'Order placed'
    db.any('UPDATE cartallocation SET orderstatus = $1 WHERE id=$2', [status, cartid])
        .then(function (data) {
            var userId = data[0].fn_viewuserid;
            var cartstatus = 'Billing conformed'
            db.any('update cart set status=$1 where cartallocationid=$2', [cartstatus, cartid])
                .then(function (data) {
                    var stat = "Active";
                    db.any('insert into cartallocation(admissionnum,orderstatus)values($1,$2)', [userid, stat])
                        .then(function (data) {
                            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:cart','ITR','INFO','Addition of cart Table successfull','cart',true,$1)", userId).then((log) => {
                                if (data.length > 0) {
                                    res.status(200).send({
                                        result: true,
                                        error: "NOERROR",
                                        data: data,
                                        message: "checkout Successfully"
                                    })
                                } else {
                                    res.status(200).send({
                                        result: true,
                                        error: "NOERROR",
                                        data: "NDF",
                                        message: "No Data Found"
                                    })
                                }
                            })
                        })
                })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:cart','ITR','ERR','Addition of cart Table unsuccessfull','cart',False,$1)", userId).then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "error",
                    message: "Unable to Add product Details"
                })
            });
        })
})
}
//----------------------------------------uploads-----------------------------------------
{
//get user uploads
dashboardRouter.get('/Public/Uploads/:Id', (req, res, next) => {
    var userId = req.params.Id
    var filesList1 = [];
    const directoryPath = path.resolve(__dirname, '../public/uploads/' + userId + "/");
    fs.readdir(directoryPath, 'utf8', (err, files) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:Uploads','ITR','INFO','view of Uploads Table successfull','Uploads',true)").then((log) => {
            for (var i = 0; i < files.length; i++) {
                filesList1.push({
                    image: files[i],
                    view: false
                })
            }
            if (files.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: filesList1,
                    message: "Uploads Has Been Retrieved succesfully "
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
   
})
 //get user pdf --
 dashboardRouter.get('/Public/Uploads/:Id/:Name', (req, res) => {
    var nme = req.params.Name;
    var i = req.params.Id;
    var fn = '../public/uploads/' + i + '/' + nme;
    res.status(200).sendFile(path.resolve(__dirname, fn))
})
//get to shared by userid
dashboardRouter.get('/sharing/Details/:Id', (req, res, next) => {
    var i = req.params.Id;
    db.any('select * from fn_gettosharedlist($1)', i).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:Sharing','ITR','INFO','view of Sharing Table successfull','Sharing',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Sharing Details Has Been Deleted succesfully "
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:Sharing','ITR','ERR','view of Sharing Table unsuccessfull','Sharing',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Sharing Details"
                });
            });
        })
})
//Uploads 
dashboardRouter.delete('/uploads/delete/:Id/:fName', (req, res, next) => {
    var userId = req.params.Id
    var nme = req.params.fName
    const directoryPath = path.resolve(__dirname, '../public/uploads/' + userId + "/" + nme);
    try {
        fs.unlink(directoryPath, (err) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Delete uploads','ITR','INFO','Delete of File Table successfull','Deleted File',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Uploads Has Been Deleted succesfully "
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    } catch (err) {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Delete uploads','ITR','ERR',Delete of File Table successfull','Deleted File',false)").then((log) => {
            res.status(200).send({
                result: false,
                error: error.message,
                data: "ERROR",
                message: false
            });
        })
    }
})
// adding sharing
dashboardRouter.post('/sharing/add', (req, res, next) => {
    var fuId = req.body.fUserId;
    var snId = req.body.socialNetworkId;
    var urll = req.body.url;
    for (var i = 0; i < snId.length; i++) {
        db.any('select * from fn_addsharing($1,$2,$3)', [fuId, snId[i].socialnetworkid, urll]).then((Id) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:sharing','ITR','INFO','Addition of sharing Table successfull','sharing',true)").then((log) => {
                    if (Id.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: Id,
                            message: "Sharing Has Been Added succesfully "
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:sharing','ITR','ERR','Addition of sharing Table unsuccessfull','sharing',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        data: "ERROR",
                        error: error.message,
                        message: "Unable to Add Sharing"
                    });
                });
            })
    }
})
//get socialnetworkid by userid
dashboardRouter.get('/Socialnetwork/:Id', (req, res, next) => {
    var i = req.params.Id;
    db.any('select socialnetworkid from authenticationmaster where userid in(select userid from	userprofileview where Organizationid=(select Organizationid from userprofileview where userid=$1))', i).then(function (data) {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "socialnetworkid Has Been Retrieved succesfully "
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
        .catch(error => {
            res.status(200).send({
                result: false,
                data: "ERROR",
                error: error.message,
                message: "Unable to View socialnetwork Details"
            });
        })
})
}
module.exports = dashboardRouter;
