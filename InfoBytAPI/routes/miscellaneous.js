var express = require('express');
var bodyparser = require('body-parser');
var payslip = require('../payslip')
var fs = require('fs')
var path = require('path')
var mail = require('../mailer');
var miscellaneousRouter = express.Router()
miscellaneousRouter.use(express.static(path.resolve(__dirname, "public")));
miscellaneousRouter.use(bodyparser.json({
    limit: '5mb'
}));
miscellaneousRouter.use(bodyparser.urlencoded({
    limit: '5mb',
    extended: true
}));
var config = require('../init_app');
const dba = require('../db.js');
const db = dba.db;
const pgp = db.$config.pgp;
miscellaneousRouter.get('/Images/:folder/:id', (req, res, next) => {
    folder = req.params.folder;
    id = req.params.id;
    res.sendFile(path.resolve('./public/' + folder + '/' + id + '.png'));
})
miscellaneousRouter.get('/Files/:folder/:id', (req, res, next) => {
    folder = req.params.folder;
    id = req.params.id;
    res.sendFile(path.resolve('./public/' + folder + '/' + id + '.pdf'));
})
//upload profile pic
miscellaneousRouter.post('/Upload/ProfilePic/:Id', (req, res, next) => {
    var userId = req.params.Id;
    var img = req.body.image
    var fn = userId + '.png';
    var dir = './public/images/';
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    try {
        fs.writeFile('./public/images/' + fn, img, 'base64', (err) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:profile pic','ITR','INFO','Update of profile pic Table successfull','profile pic',true)").then((log) => {
                res.status(200).send({
                    message: "Pic Uploaded"
                })
            })
        })
    }
    catch (err) {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:profile pic','ITR','ERR','Update of profile pic Table unsuccessfull','profile pic',false)").then((log) => {
            res.status(200).send({
                message: "Upload Failed"
            });
        })
    }
})
//get Profile Pic
miscellaneousRouter.get('/ProfilePic/:Folder/:userSession', (req, res) => {
    var sessionparam = req.params.userSession;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    var flder = req.params.Folder;
    db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((data) => {
     userId = data[0].fn_viewuserid;
    var fn = config.server + '/Misc/Images/' + flder + '/' + userId;
    var path1 = path.resolve(__dirname, '../public/images/' + userId + '.png')
    if (fs.existsSync(path1)) {
        res.status(200).send({
            path: fn, 
            message: true
        })
    }
    else {
        res.status(200).send({
            message: false
        })
    }
}) 
})
module.exports = miscellaneousRouter;