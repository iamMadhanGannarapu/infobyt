var express = require('express');
var bodyparser = require('body-parser');
var path = require('path')
var fs = require('fs')
var attendanceRouter = express.Router()
attendanceRouter.use(express.static(path.resolve(__dirname, "public")));
attendanceRouter.use(bodyparser.json({
    limit: '5mb'
}));
attendanceRouter.use(bodyparser.urlencoded({
    limit: '5mb',
    extended: true
}));
const dba = require('../db.js');
const db = dba.db;
const pgp = db.$config.pgp;
//=================================Attendance=============================================
//=================================Attendance=============================================
{ //(1)(done)
    attendanceRouter.post('/attendance/Trainee/add/:date/:Itime/:Otim', (req, res, next) => {
        dt = req.params.date;
        itm = req.params.Itime;
        otm = req.params.Otim
        usrc = req.body.traineeid;
        var stsc;
        var userId='';
        for (var i = 0; i < req.body.length; i++) {
            if (req.body[i].checkBoxStatus == true)
                stsc = 'P'
            else
                stsc = 'A'
            db.any('select *from fn_addattendanceByBatch($1,$2,$3,$4,$5)', [req.body[i].deviceuserid, dt, itm, otm, stsc]).then((data) => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                     userId = dbSession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Stuedent Attendance','ITR','INFO','Addition of Stuedent Attendance Table successfull','Stuedent Attendance',true,$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "Attendance Has Been Added succesfully "
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
                .catch(error => {
                    db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                         userId = dbSession[0].fn_viewuserid;
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Stuedent Attendance','ITR','ERR','Addition of Stuedent Attendance Table unsuccessfull','Stuedent Attendance',False,$1)", userId).then((log) => {
                            res.status(200).send({
                                result: false,
                                error: error.message,
                                data: "ERROR",
                                message: "Unable to Add Trainee Attendance"
                            });
                        });
                    })
                })
        }
    })
    ///to add attendance manually for staff(2)
    attendanceRouter.post('/attendance/Staff/add/:date/:Itime/:Otim', (req, res, next) => {
        dt = req.params.date;
        itm = req.params.Itime;
        otm = req.params.Otim
        usrc = req.body.traineeid;
        var stsc;
        var userId='';
        for (var i = 0; i < req.body.length; i++) {
            if (req.body[i].checkBoxStatus == true)
                stsc = 'P'
            else
                stsc = 'A'
            db.any('select *from fn_addattendanceByBatch($1,$2,$3,$4,$5)', [req.body[i].deviceuserid, dt, itm, otm, stsc]).then((data) => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                     userId = dbSession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Stuedent Attendance','ITR','INFO','Addition of Stuedent Attendance Table successfull','Stuedent Attendance',true,$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "Attendance Has Been Added succesfully "
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
                .catch(error => {
                    db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                         userId = dbSession[0].fn_viewuserid;
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Stuedent Attendance','ITR','ERR','Addition of Stuedent Attendance Table unsuccessfull','Stuedent Attendance',False,$1)", userId).then((log) => {
                            console.log('ERROR:Unable to Add Staff Attendance', error.message || error);
                            res.status(200).send({
                                result: false,
                                error: error.message,
                                data: "ERROR",
                                message: "Unable to Add Staff Attendance"
                            });
                        });
                    })
                })
        }
    })
    //View Atendance of Branch Admin by organizationadmin(3) 
    attendanceRouter.get('/attendance/staff/trainees/branchAdmins/:branchId/:fDate/:tDate', (req, res, next) => {
        var branchId = req.params.branchId;
        var maxdate = req.params.tDate;
        var mindate = req.params.fDate;
        mindate = "'" + mindate + "'";
        maxdate = "'" + maxdate + "'";
        month = maxdate.substring(1, 3);
        year = maxdate.substring(7, 11);
        db.any('create or replace view userattendanceview as select usrcode,statusCode,count(statusCode) from attendanceLog where cast(attendanceDate as date) between ' + mindate + ' and ' + maxdate + ' group by 1,2 order by 1,2').then((data) => {
            db.any('select * from fn_viewattendancelogbyOrganizationadmin($1,$2,$3)', [branchId, month, year]).then((attendance) => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Stuedent Attendance','ITR','INFO','Addition of Stuedent Attendance Table successfull','Stuedent Attendance',true)").then((log) => {
                        if (attendance.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: attendance,
                                message: "Attendance Data Retrieved succesfully "
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    });
            })
        })
            .catch(error => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Stuedent Attendance','ITR','ERR','Addition of Stuedent Attendance Table unsuccessfull','Stuedent Attendance',False)").then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: " Unable to View Subject"
                        })
                    });
            });
    })
    //get staff by Accontant(4)
    attendanceRouter.get('/attendance/staff/Details/accontant/:userSession/:fDate/:tDate', (req, res, next) => {
        var maxdate = req.params.tDate;
        var mindate = req.params.fDate;
        mindate = "'" + mindate + "'";
        maxdate = "'" + maxdate + "'";
        month = maxdate.substring(1, 3);
        year = maxdate.substring(7, 11);
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((user) => {
            userId = user[0].fn_viewuserid;
            db.any('create or replace view userattendanceview as select usrcode,statusCode,count(statusCode) from attendanceLog where cast(attendanceDate as date) between ' + mindate + ' and ' + maxdate + ' group by 1,2 order by 1,2').then((data) => {
                db.any('select * from fn_viewDepartments($1,$2,$3)', [sessionparam, month, year]).then((attendance) => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view:Departments','ITR','INFO','view of Departments Table successfull','Departments',true,$1)", userId).then((log) => {
                        //  res.status(200).send(data);
                        if (attendance.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: attendance,
                                message: "Attendance Data Retrieved succesfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    });
                })
            })
        })
            .catch(error => {
                db.any('select * from fn_viewDepartments($1,$2,$3)', [sessionparam, month, year]).then((dbSession) => {
                     userId = dbSession[0].userid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view:Departments','ITR','ERR','view of Departments Table unsuccessfull','Departments',False,$1)", userId).then((log) => {
                        console.log('ERROR:Unable to View Attendance Details of Staff', error.message || error);
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: " Unable to View Attendance"
                        })
                    });
                })
            });
    })
    //accordion user details /attendance/userDetails/(5)
    attendanceRouter.get('/attendance/userDetails/:userId', (req, res, next) => {
        var i = req.params.userId;
        db.any('select * from attendancelog a join usermapping um on um.deviceuserid=a.usrcode where cast(um.appuserid as integer)=$1', i)
            .then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:attendancelog','ITR','INFO','view of attendancelog Table successfull','attendancelog',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Attendance Has Been Retrieved succesfully "
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:attendancelog','ITR','ERR','view of attendancelog Table unsuccessfull','attendancelog',False)").then((log) => {
                    console.log('ERROR:Unable to View Attendance ', error.message || error);
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable To View Attendance"
                    });
                })
            })
    })
    //(6)
    attendanceRouter.get('/attendance/trainees/Details/accontant/:userSession/:fDate/:tDate', (req, res, next) => {
        var maxdate = req.params.tDate;
        var mindate = req.params.fDate;
        mindate = "'" + mindate + "'";
        maxdate = "'" + maxdate + "'";
        month = maxdate.substring(1, 3);
        year = maxdate.substring(7, 11);
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((user) => {
            userId = user[0].fn_viewuserid
            db.any('create or replace view userattendanceview as select usrcode,statusCode,count(statusCode) from attendanceLog where cast(attendanceDate as date) between ' + mindate + ' and ' + maxdate + ' group by 1,2 order by 1,2').then((data) => {
                db.any('select * from fn_viewtraineeattendancedetais($1,$2,$3)', [sessionparam, month, year])
                    .then((dbSession) => {
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view:traineeattendancedetais','ITR','INFO','view of traineeattendancedetais Table successfull','traineeattendancedetais',true,$1)", userId).then((log) => {
                            // res.status(200).send(data);
                            if (data.length > 0) {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: data,
                                    message: "Attendance Details of Trainees for Accountant Retrieved succesfully"
                                })
                            } else {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: "NDF",
                                    message: "No Data Found"
                                })
                            }
                        });
                    })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                    userId = Session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view:traineeattendancedetais','ITR','ERR','view of traineeattendancedetais Table unsuccessfull','traineeattendancedetais',False,$1)", userId).then((log) => {
                    console.log('ERROR:Unable to View Attendance Details of Trainees for Accountant', error.message || error);
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: " Unable to View Attendance Details of Trainees for Accountant"
                    })
                })
            })
            });
    })
    //View Atendance of Branch Admin Users(7)
    attendanceRouter.get('/attendance/staff/Details/branchAdmins/:userSession/:fDate/:tDate', (req, res, next) => {
        var maxdate = req.params.tDate;
        var mindate = req.params.fDate;
        mindate = "'" + mindate + "'";
        maxdate = "'" + maxdate + "'";
        month = maxdate.substring(1, 3);
        year = maxdate.substring(7, 11);
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('create or replace view userattendanceview as select usrcode,statusCode,count(statusCode) from attendanceLog where cast(attendanceDate as date) between ' + mindate + ' and ' + maxdate + ' group by 1,2 order by 1,2').then((data) => {
            db.any('select * from fn_viewallBranchAdmins($1,$2,$3)', [sessionparam, month, year]).then((dbSession) => {
                 userId = dbSession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view:BranchAdmins','ITR','INFO','view of BranchAdmins Table successfull','BranchAdmins',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Attendance Data Retrieved succesfully "
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                });
            })
        })
            .catch(error => {
                db.any('select * from fn_viewallBranchAdmins($1,$2,$3)', [sessionparam, month, year]).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view:BranchAdmins','ITR','ERR','view of BranchAdmins Table unsuccessfull','BranchAdmins',False,$1)", userId).then((log) => {
                        console.log('ERROR:Unable to View Attendance Details of BranchAdmin', error.message || error);
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: " Unable to View Attendance Details of BranchAdmin"
                        });
                    })
                });
            });
    })
    //view todays attendance(8)
    attendanceRouter.get('/todaysattendance/staff/trainees/Details/:userSession', (req, res, next) => {
        var maxdate = new Date(Date.now());
        var mindate = new Date(Date.now());
        day = maxdate.getDate()
        month = maxdate.getMonth() + 1;
        year = maxdate.getFullYear()
        mindate = "'" + mindate + "'";
        maxdate = "'" + maxdate + "'";
        var mindate = month + '-' + day + '-' + year
        var maxdate = month + '-' + day + '-' + year
        mindate = "'" + mindate + "'";
        maxdate = "'" + maxdate + "'"
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        var datetime = new Date();
        db.any('create or replace view userattendanceview as select usrcode,statusCode,count(statusCode) from attendanceLog where cast(attendanceDate as date) between ' + mindate + ' and ' + maxdate + ' group by 1,2 order by 1,2').then((data) => {
            db.any('select * from fn_viewtodaysattendancelogbybranchadmin($1,$2)', [sessionparam, datetime]).then((data1) => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                     userId = dbSession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view:traineeattendancedetais','ITR','INFO','view of traineeattendancedetais Table successfull','traineeattendancedetais',true,$1)", userId).then((log) => {
                        if (data1.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data1,
                                message: "TodaysAttendance Has Been Retrieved succesfully "
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    });
                })
            })
        }).catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                 userId = dbSession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view:traineeattendancedetais','ITR','ERR','view of traineeattendancedetais Table unsuccessfull','traineeattendancedetais',False,$1)", userId).then((log) => {
                    console.log('ERROR:Unable to View Current Attendance Details of Trainee', error.message || error);
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: " Unable To Retrieve Current Attendance Details of Trainee"
                    });
                });
            });
        });
    })
    //(9)
    attendanceRouter.get('/attendance/trainees/Details/staff/:userSession/:fDate/:tDate', (req, res, next) => {
        var maxdate = req.params.tDate;
        var mindate = req.params.fDate;
        mindate = "'" + mindate + "'";
        maxdate = "'" + maxdate + "'";
        month = maxdate.substring(1, 3);
        year = maxdate.substring(7, 11);
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('create or replace view userattendanceview as select usrcode,statusCode,count(statusCode) from attendanceLog where cast(attendanceDate as date) between ' + mindate + ' and ' + maxdate + ' group by 1,2 order by 1,2').then((data) => {
            db.any('select * from fn_viewattendancelogtraineebyClassteacher($1,$2,$3)', [sessionparam, month, year])
                .then((dbsession) => {
                     userId = dbsession[0].userid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view:attendancelogtraineebyClassteacher','ITR','INFO','view of attendancelogtraineebyClassteacher Table successfull','attendancelogtraineebyClassteacher',true,$1)", userId).then((log) => {
                        // res.status(200).send(data);
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "Attendance Details of Trainee for Staff Retrieved succesfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    });
                })
        })
            .catch(error => {
                db.any('select * from fn_viewattendancelogtraineebyClassteacher($1,$2,$3)', [sessionparam, month, year])
                    .then((dbsession) => {
                         userId = dbsession[0].userid;
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view:attendancelogtraineebyClassteacher','ITR','ERR','view of attendancelogtraineebyClassteacher Table unsuccessfull','attendancelogtraineebyClassteacher',False,$1)", userId).then((log) => {
                            console.log('ERROR:Unable to View Attendance Details of Trainee for Staff', error.message || error);
                            res.status(200).send({
                                result: false,
                                error: error.message,
                                data: "ERROR",
                                message: " Unable to View Attendance Details of Trainee for Staff"
                            })
                        })
                    });
            });
    })
    //get by id attendence(10)
    attendanceRouter.get('/attendance/Details/:Id', (req, res, next) => {
        var i = req.params.Id
        db.any('select * from fn_viewattendancelogById($1)', i)
            .then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:attendancebyid','ITR','INFO','view of attendancebyid Table successfull','attendancebyid',true)").then((log) => {
                    // res.status(200).send(data);
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: " Attendance Details Retrieved succesfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                });
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:attendancebyid','ITR','ERR','view of attendancebyid Table unsuccessfull','attendancebyid',False)").then((log) => {
                    console.log('ERROR:Unable to View Attendance Details', error.message || error);
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Attendance Details"
                    })
                });
            });
    })
    //update attendance(11)
    attendanceRouter.post('/attendance/update/:Id', function (req, res, next) {
        var i = req.params.Id
        var stats = req.body.statusCode
        db.any("select * from fn_updateattendance($1,$2)", [i, stats]).then((pwd) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-update:attendance','ITR','INFO','update of attendance Table successfull','attendance',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: pwd,
                        message: "Attendance Data Has Been Updated succesfully "
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-update:attendance','ITR','ERR','update of attendance Table unsuccessfull','attendance',False)").then((log) => {
                    console.log('ERROR:Unable to Update Attendance Details', error.message || error);
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable To Update Attendance Details"
                    });
                });
            })
    })
    //get working details(12)
    attendanceRouter.get('/attendance/get/:param_usersession/:mindate/:maxdate', (req, res) => {
        var i = req.params.id
        var userId='';
        sessionparam = req.params.param_usersession;
        mindate = req.params.mindate;
        maxdate = req.params.maxdate
        mindate = "'" + mindate + "'";
        maxdate = "'" + maxdate + "'";
        month = mindate.substring(1, 3);
        year = maxdate.substring(7, 11);
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((user) => {
            userId = user[0].fn_viewuserid;
            db.any('create or replace view userattendanceview as select usrcode,statusCode,count(statusCode) from attendanceLog where cast(attendanceDate as date) between ' + mindate + ' and ' + maxdate + ' group by 1,2 order by 1,2').then((data) => {
                db.any('select * from fn_viewatendance($1,$2,$3)', [sessionparam, month, year]).then((attendance) => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view:attendance','ITR','INFO','view of attendance Table successfull','attendance',true,$1)", userId).then((log) => {
                        //  res.status(200).send(data);
                        if (attendance.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: attendance,
                                message: " UserMapping Details of a trainee Retrieved succesfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    });
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                    userId = dbSession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view:attendance','ITR','ERR','view of attendance Table unsuccessfull','attendance',False,$1)", userId).then((log) => {
                    console.log('ERROR:Unable to View Attendance Details of a trainee', error.message || error);
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View UserMapping Details of a trainee"
                    })
                })
            })
            });
    })
    //13
    attendanceRouter.get('/attendance/traineeList/Batch/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select * from traineebatchview where batchid=$1', i)
            .then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:attendancelog','ITR','INFO','view of attendancelog Table successfull','attendancelog',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: pwd,
                            message: "Trainee Attendance Has Been Retrieved succesfully "
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:attendancelog','ITR','ERR','view of attendancelog Table unsuccessfull','attendancelog',False)").then((log) => {
                    console.log('ERROR:Unable to View Trainee Attendance', error.message || error);
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Trainee Attendance"
                    });
                })
            })
    })
    //14
    attendanceRouter.get('/attendance/traineeList/Batch/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select um.deviceuserid,upv.firstname,upv.lastname from usermapping um join userprofileview upv on um.appuserid=cast(upv.id as varchar) join admisiondetails ad on ad.userid=upv.id join traineebatch sbm on sbm.traineeid=ad.admissionnum where sbm.batchid=$1', i).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:attendancelog','ITR','INFO','view of attendancelog Table successfull','attendancelog',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: " Trainee Attendance Retrieved Sucessfully"
                    })
                }
                else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:attendancelog','ITR','ERR','view of attendancelog Table unsuccessfull','attendancelog',False)").then((log) => {
                    console.log('ERROR:Unable to View Trainee Attendance', error.message || error);
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Trainee Attendance"
                    });
                })
            })
    })
    //Getting staff(15)
    attendanceRouter.get('/attendance/staffList/Branch/:usersession', (req, res, next) => {
        sessionparam = req.params.usersession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        rl = "Trainee"
        db.any('select um.deviceuserid,upv.firstname,upv.lastname from usermapping um join userprofileview upv on um.appuserid=cast(upv.id as varchar) where upv.rolename!=($2) and upv.branchid=(select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid($1)))', [sessionparam, rl])
            .then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view:attendancelog','ITR','INFO','view of attendancelog Table successfull','attendancelog',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "  Attendance Retrieved Sucessfully"
                        })
                    }
                    else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any('select um.deviceuserid,upv.firstname,upv.lastname from usermapping um join userprofileview upv on um.appuserid=cast(upv.id as varchar) where upv.rolename!=($2) and upv.branchid=(select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid($1)))', [sessionparam, rl])
                    .then((dbsession) => {
                         userId = dbsession[0].fn_viewuserid;
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view:attendancelog','ITR','ERR','view of attendancelog Table unsuccessfull','attendancelog',False,$1)", userId).then((log) => {
                            console.log('ERROR:Unable to Attendance', error.message || error);
                            res.status(200).send({
                                result: false,
                                error: error.message,
                                data: "ERROR",
                                message: "Unable to View Attendance"
                            });
                        })
                    })
            })
    })
//amulya
}
//==================================================Holiday=================================
{
    // POST FOR HOLIDAY(**********)
    attendanceRouter.post('/Holiday/Add', (req, res, next) => {
        var hd = req.body.holidayDate;
        var henddate = req.body.holidayEndDate;
        var occ = req.body.occasion;
        var bid = req.body.branchId;
        db.any('select fn_addholiday($1,$2,$3,$4)', [hd, henddate, occ, bid])
            .then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Holiday ','ITR','INFO','Addtion of Holiday  Table  successfull','Holiday ',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Holiday Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Holiday ','ITR','ERR','Addtion of Holiday  Table  unsuccessfull','Holiday ',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Holiday "
                    })
                })
            });
    })
    //notification for holidays(**********)
    attendanceRouter.post('/notification/Holiday/add/:Id', (req, res, next) => {
        sessionparam = req.params.Id
        var notification = "Holiday has been declared";
        var rcvr = '';
        var notificationid = 0;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewHolidayreceiverIds($1)', sessionparam).then((data) => {
            rcvr = data;
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                 userId = dbSession[0].fn_viewuserid;
                db.any(' insert into notifications(notification) values ($1) returning id', notification).then((data) => {
                    notificationid = data[0].id;
                    for (var j = 0; j < rcvr.length; j++) {
                        db.any('select * from fn_addnotificationdetails($1,$2,$3,$4)', [rcvr[j].rolename, rcvr[j].userid, notificationid, sessionparam])
                            .then((data) => {
                                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','INFO','Addition of Notifications Table  successfull','Notifications',true,$1)", userId).then((log) => {
                                    if (data.length > 0) {
                                        res.status(200).send({
                                            result: true,
                                            error: "NOERROR",
                                            data: data,
                                            message: "Holiday Notification Retrieved Successfully"
                                        })
                                    } else {
                                        res.status(200).send({
                                            result: true,
                                            error: "NOERROR",
                                            data: "NDF",
                                            message: "No Data Found"
                                        })
                                    }
                                })
                            })
                    }
                })
            })
                .catch(error => {
                    db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                         userId = dbSession[0].fn_viewuserid;
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addition:Notifications','ITR','ERR','Addition of Notifications Table unsuccessfull','Notifications',False,$1)", userId).then((log) => {
                            res.status(200).send({
                                result: false,
                                error: error.message,
                                data: "ERROR",
                                message: "Unable to View Holiday Notification"
                            })
                        })
                    })
                })
        })
    })
    // GET BY ID HOLIDAY (**********)
    attendanceRouter.get('/Holiday/Details/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select * from fn_viewholidaymasterdetails($1)', i).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Holiday ','ITR','INFO','View of Holiday  Table  successfull','Holiday ',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Holiday Has Been Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Holiday ','ITR','ERR','View of Holiday  Table unsuccessfull','Holiday ',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Holiday Details"
                    })
                })
            });
    })
    //UPDATE HOLIDAY(**********)
    attendanceRouter.post('/Holiday/Update/:Id', (req, res, next) => {
        var id = req.params.Id;
        var holidayDate = req.body.holidayDate;
        var holidayenddate = req.body.holidayEndDate;
        var oct = req.body.occasion;
        db.any('select * from fn_updateholiday($1,$2,$3,$4)', [id, holidayDate, holidayenddate, oct]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-update:Holiday ','ITR','INFO','update of Holiday  Table  successfull','Holiday ',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: id,
                        message: "Holiday Details Has Been Updated Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-update:Holiday ','ITR','ERR','update of Holiday  Table unsuccessfull','Holiday ',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Cannot Update Holiday Details"
                    })
                })
            });
    })
    // GET FOR HOLIDAY(**********)
    attendanceRouter.get('/Holiday/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewholidaymaster($1)', sessionparam).then((Session) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Holiday ','ITR','INFO','View of Holiday  Table  successfull','Holiday ',true,$1)", userId).then((log) => {
                    if (Session.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: Session,
                            message: "Holiday Details Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Holiday ','ITR','ERR','View of Holiday  Table unsuccessfull','Holiday ',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Holiday Details"
                        })
                    })
                })
            });
    })
    //Delete from holiday(**********)
    attendanceRouter.delete('/Holiday/Delete/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select * from fn_deleteHoliaday($1)', i).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Holiday ','ITR','INFO','View of Holiday  Table  successfull','Holiday ',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Holiday Details Has Been Deleted Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Holiday ','ITR','ERR','View of Holiday  Table unsuccessfull','Holiday ',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Delete Holiday Details"
                    })
                })
            });
    })
    // GET FOR HOLIDAY calender(**********)
    attendanceRouter.get('/HolidayCalender/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewholidaycalender($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                 userId = Session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Holiday ','ITR','INFO','View of Holiday  Table  successfull','Holiday ',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Holiday Calender Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                     userId = Session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Holiday ','ITR','ERR','View of Holiday  Table unsuccessfull','Holiday ',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Holiday Calender"
                        })
                    })
                })
            });
    })
}
//======================================Late Logins================================
{   //(1)
    attendanceRouter.get('/latelogins/:userSession', function (req, res, next) {
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
db.any('select fn_viewuserid from fn_viewuserid($1)',sessionparam).then((user)=>{
         userId = user[0].fn_viewuserid;
        db.any("select * from userprofileview where branchid=(select branchid from userprofileview where userid=(select fn_viewuserid from fn_viewuserid($1)));", sessionparam).then((latelogins) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:lateLogins','ITR','INFO','View of lateLogins Table successfull','lateLogins',true,$1)", userId).then((log) => {
                if (latelogins.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR", 
                        data: latelogins,
                        message: "Late LoginsRetrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })})
            .catch(error => {
                db.any("select fn_viewuserid from fn_viewuserid($1)", sessionparam).then((dbSession) => {
                     userId = dbSession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:lateLogins','ITR','ERR','View of lateLogins Table unsuccessfull','lateLogins',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: " Unable to View Late Logins"
                        })
                    })
                })
            });
    })
    //(2)
    attendanceRouter.get('/LateLogin/User/:userId/:month', function (req, res, next) {
        db.any("select * from salarypay where usrcode=(select deviceuserid from usermapping where appuserid=$1) and to_char(attendancedate, 'YYYY-MM')=$2  and (payablestatus!='F') order by attendancedate asc", [req.params.userId + '', req.params.month]).then((data) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Late Logins Retrieved Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:usermapping','ITR','ERR','Addition of usermapping Table unsuccessfull','usermapping',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Logins Details"
                    });
                });
            })
    });
    //(3)
    attendanceRouter.post('/LateLogin/Update', (req, res, next) => {
        date1 = new Date(req.body.date)
        if (req.body.status == 'Accepted') {
            db.any("update salarypay set approvedstatus='F',totalminutes=(select ((extract (hour from (s.endtime-s.gracetime)))*60)+((extract(minute from(s.endtime-s.gracetime)))) from shifts s join allocateshifts dt on s.id=dt.shiftid where dt.userid=(select cast(appuserid as int) from usermapping where deviceuserid=$1 )and cast($2 as date) between dt.startdate and dt.enddate),approvalstatus=$3 where usrcode=$4 and attendancedate=$5", [req.body.usrcode, date1, req.body.status, req.body.usrcode, date1]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Academic Details ','ITR','INFO','Update of Sports management Table  successfull','Sportsmanagement',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "LateLogin Updated Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
                .catch(error => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Academic Details ','ITR','ERR','Update of Sports management Table unsuccessfull','session Subject',true)").then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Cannot Update LateLogin"
                        })
                    })
                });
        }
        if (req.body.status == 'Rejected') {
            db.any("update salarypay set approvalstatus=$1 where usrcode=$2 and attendancedate=$3", [req.body.status, req.body.usrcode, date1]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Academic Details ','ITR','INFO','Update of Sports management Table  successfull','Sportsmanagement',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: " LateLogin Updated Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
                .catch(error => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Academic Details ','ITR','ERR','Update of Sports management Table unsuccessfull','Class Subject',true)").then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Cannot Update LateLogin"
                        })
                    })
                });
        }
    })
}
//===========================================UserMapping=======================
{
    //adding branchmapping
    attendanceRouter.post('/branchmapping/add', (req, res, next) => {
        usrdevid = req.body.userDeviceId;
        branchid = req.body.branchId;
        db.any('select *from fn_addbranchmapping($1,$2)', [usrdevid, branchid]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:branchmapping','ITR','INFO','Addition of branchmapping Table successfull','branchmapping',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Branch Mapping Has Been Added succesfully "
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:branchmapping','ITR','ERR','Addition of branchmapping Table unsuccessfull','branchmapping',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        data: "ERROR",
                        error: error.message,
                        message: "Unable to Add Branch Mapping"
                    });
                });
            })
    })
    //get usersdeviceid by deviceid
    attendanceRouter.get('/usermapping/usercode/details/all/:id', (req, res, next) => {
        sessionparam = req.params.Id
        db.any('select * from fn_branchmapping($1)', sessionparam)
            .then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:branchmapping','ITR','INFO','view of branchmapping Table successfull','branchmapping',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "User Mapping by Device Id Has Been Retrieved succesfully "
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:branchmapping','ITR','ERR','view of branchmapping Table unsuccessfull','branchmapping',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        data: "ERROR",
                        error: error.message,
                        message: "Unable to View User Mapping by Device Id"
                    });
                });
            })
    })
    attendanceRouter.get('/user/usermapping/get/:usersession', (req, res, next) => {
        sessionparam = req.params.usersession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select *from fn_viewuserofbranch($1)', sessionparam).then((data) => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                     userId = dbSession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:userofbranch','ITR','INFO','view of userofbranch Table successfull','userofbranch',true,$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "usersdeviceid by deviceid Has Been Retrieved succesfully "
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbSession) => {
                     userId = dbSession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:userofbranch','ITR','ERR','view of userofbranch Table unsuccessfull','userofbranch',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View User of Branch"
                        })
                    })
                });
            })
    })
    //get usermapping details
    attendanceRouter.get('/usermapping/:usersession', (req, res, next) => {
        var userId;
        var sessionparam = req.params.usersession;
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
       db.any('select fn_viewuserid from fn_viewuserid($1)',sessionparam).then((dbSession)=>{
        userId = dbSession[0].fn_viewuserid;
        db.any('select * from fn_viewusermapping($1)', sessionparam)
            .then((data) => {
                // userId = dbSession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:usermapping','ITR','INFO','view of usermapping Table successfull','usermapping',true,$1)",userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: " User Mapping details Has Been Retrieved succesfully "
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })})
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)',sessionparam).then((dbSession)=>{
                         userId = dbSession[0].fn_viewuserid;
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:usermapping','ITR','ERR','view of usermapping Table unsuccessfull','usermapping',False,$1)",userId).then((log) => {
                            res.status(200).send({
                                result: false,
                                error: error.message,
                                data: "ERROR",
                                message: "Unable to View User Mapping"
                            })
                        });
                    });
            })
    })
    //getting deviceID(************)
    attendanceRouter.get('/usermapping/usercode/getdevice/:usersession', (req, res, next) => {
        sessionparam = req.params.usersession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewuserdeviceid($1)', sessionparam)
            .then((data) => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:userdeviceid','ITR','INFO','view of userdeviceid Table successfull','userdeviceid',true,$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "User device by id Has Been Retrieved succesfully "
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:userdeviceid','ITR','ERR','view of userdeviceid Table unsuccessfull','userdeviceid',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View User device by id"
                        })
                    })
                });
            })
    })
    // adding usermapping
    attendanceRouter.post('/usermapping/add', (req, res, next) => {
        devuserid = req.body.deviceUserId;
        usrid = req.body.appUserId;
        devid = req.body.deviceId;
        db.any('select * from fn_AddUsermapping($1,$2,$3)', [devuserid, usrid, devid]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:usermapping','ITR','INFO','Addition of usermapping Table successfull','usermapping',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Usermapping Has Been Added succesfully "
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:usermapping','ITR','ERR','Addition of usermapping Table unsuccessfull','usermapping',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add User Mapping Details"
                    });
                });
            })
    })
    //get branchmapping details
    attendanceRouter.get('/branchmapping/:usersession', (req, res, next) => {
        sessionparam = req.params.usersession
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewbranchmapping($1)', sessionparam)
            .then((data) => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:branchmapping','ITR','INFO','view of branchmapping Table successfull','branchmapping',true,$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "Branch Mapping Details Has Been Retrieved succesfully "
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:branchmapping','ITR','ERR','view of branchmapping Table unsuccessfull','branchmapping',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: " Unable to  View Branch Mapping Details"
                        })
                    });
                })
            })
    })
}
// ==============================
module.exports = attendanceRouter;