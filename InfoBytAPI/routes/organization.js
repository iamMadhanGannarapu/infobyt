var express = require('express');
var bodyparser = require('body-parser');
var payslip = require('../payslip')
var fs = require('fs')
var path = require('path')
var mail = require('../mailer');
var organizationRouter = express.Router()
organizationRouter.use(express.static(path.resolve(__dirname, "public")));
organizationRouter.use(bodyparser.json({
    limit: '5mb'
}));
organizationRouter.use(bodyparser.urlencoded({
    limit: '5mb',
    extended: true
}));
var config = require('../init_app');
const dba = require('../db.js');
const db = dba.db;
const pgp = db.$config.pgp;
//========================================Achievements=========================================================
{
    //html
    organizationRouter.get('/assets/:folder/:name', (req, res, next) => {
        folder = req.params.folder;
        ssss= req.params.name;
        res.sendFile(path.resolve('./public/assets/' + folder + '/'+ssss));
    })
    organizationRouter.get('/assets/:name', (req, res, next) => {
        ssss= req.params.name;
        res.sendFile(path.resolve('./public/assets/' + '/'+ssss));
    })
    organizationRouter.get('/assets/:folders/:subfolder/:name', (req, res, next) => {
        folder = req.params.folders;
        subfolder = req.params.subfolder;
        ssss= req.params.name;
        res.sendFile(path.resolve('./public/assets/' + folder + '/'+subfolder+'/'+ssss));
    })
    //GET BY ID For Achievement
    organizationRouter.get('/Achievement/Details/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select * from fn_ViewAchievementDetails($1)', i).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:  Achievements','ITR','INFO','Retrive  of Achievements Table  successfull','  Achievements',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Achievements Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:  Achievements','ITR','ERR','Retrive  of Achievements Table  unsuccessfull','  Achievements',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Achievements"
                    })
                })
            });
    })
    //Update for  Achievement
    organizationRouter.post('/Achievement/Update/:Id', (req, res, next) => {
        var achievementsid = req.params.Id;
        var uid = req.body.userId;
        var desp = req.body.description;
        var achd = req.body.achievementDate;
        var img = req.body.image;
        db.any('select * from fn_UpdateAchievement($1,$2,$3,$4)', [achievementsid, uid, desp, achd]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:  Achievements','ITR','INFO','Update  of Achievements Table  successfull','  Achievements',true)").then((log) => {
                fn = achievementsid + '.png';
                ps = './public/achievements/' + fn;
                fs.writeFile(ps, img, 'base64', (err) => { })
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: achievementsid,
                        message: "Achievements Has Been Updated Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:  Achievements','ITR','ERR','Update  of Achievements Table  unsuccessfull','  Achievements',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Cannot Update Achievements"
                    })
                })
            });
    });
    //GET For Achievement
    organizationRouter.get('/Achievement/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId = '';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_ViewAchievements($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                userId = Session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:  Achievements','ITR','INFO','Retrive  of Achievements Table  successfull','  Achievements',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Achievements Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                     userId = Session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:  Achievements','ITR','ERR','Retrive  of Achievements Table  unsuccessfull','  Achievements',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Achievements"
                        })
                    })
                })
            });
    })
    //Post For Achievement
    organizationRouter.post('/Achievement/Add', (req, res, next) => {
        var uid = req.body.userId;
        var desp = req.body.description;
        var img = req.body.image;
        var achd = req.body.achievementDate;
        db.any('select * from fn_addachievements($1,$2,$3)', [uid, desp, achd]).then((Id) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:  Achievements','ITR','INFO','Addtion  of Achievements Table successfull','  Achievements',true)").then((log) => {
                res.status(200).send(Id);
                fn = Id[0].fn_addachievements + '.png';
                ps = './public/achievements/' + fn;
                fs.writeFile(ps, img, 'base64', (err) => { })
                if (Id.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: Id,
                        message: " Gallery posted Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:  Achievements','ITR','ERR','Addtion  of Achievements Table  unsuccessfull','  Achievements',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Achievements"
                    })
                })
            });
    })
}
//============================================Admission Details========================================
{
    // Post For Admision
    organizationRouter.post('/Trainee/Add', (req, res, next) => {
        prntemil = req.body.parentEmail;
        prntmbl = req.body.parentMobile;
        schbrni = req.body.branchId;
        uid = req.body.userId;
        feid = req.body.feeId;
        consid = req.body.concessionId;
        img = req.body.image
        db.any('select * from fn_AddAdmisionDetails($1,$2,$3,$4,$5,$6)', [prntemil, prntmbl, schbrni, uid, feid, consid, img]).then((admissionnum) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:  Admision Table','ITR','INFO','Addtion  of trainee Admision Table  successfull','trainee Admision',true)").then((log) => {
                fn = admissionnum[0].fn_addadmisiondetails + '.png';
                ps = './public/admission/' + fn;
                var dir = './public/' + 'admission';
                if (!fs.existsSync(dir)) {
                    fs.mkdirSync(dir);
                }
                fs.writeFile(ps, img, 'base64', (err) => { })
                if (admissionnum.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: admissionnum,
                        message: " Admission Details Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:  trainee Admision ','ITR','ERR','Addtion  of trainee Admision Table  unsuccessfull','  trainee Admision ',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Admit trainee"
                    })
                })
            });
    })
      // for getuser by there usersession
      organizationRouter.get('/Branch/User/:session', (req, res, next) => {
        var sessionparam = req.params.session;
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_getuserdetails($1)', [sessionparam]).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                var userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,uesrId) values ('Table-view:userdetails','ITR','INFO','view of userdetails table successfull','userdetails',true,$1)", userId).then((log) => {

                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Session Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
                .catch(error => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-Addition:userdetails','ITR','ERR','Addition of userdetails Table unsuccessfull','userdetails',False,$1)", userId).then((log) => {

                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View user Details"
                        })
                    });
                })
        });
    })

    //GET For AdmissionMaster By userid for fee and concession
    organizationRouter.get('/admission/feeconcession/:Id', (req, res, next) => {
        var uid = req.params.Id;
      
        db.any('select * from fn_viewadmisiondetailsByuserId($1)', uid).then(function (data) {
            console.log(data)
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:  Admision Table','ITR','INFO','Retrive  of Student Admision Table  successfull','Student Admision',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: " Fee and Concession Details Retrived successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:  Admision Table','ITR','ERR','Retrive  of Student Admision Table  unsuccessfull','Student Admision',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Fee and Concession Details"
                    })
                })
            });
    })
    //GET For Admission
    organizationRouter.get('/Trainee/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_ViewAdmision($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                 userId = Session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:  Admision Table','ITR','INFO','Retrive  of trainee Admision Table  successfull','trainee Admision',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Admission Details Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                     userId = Session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:  Admision Table','ITR','ERR','Retrive  of trainee Admision Table  unsuccessfull','trainee Admision',False,$1)", userId).then((log) => { 
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Admission Details"
                        })
                    })
                })
            });
    })
    //GET By Id Admission
    organizationRouter.get('/Trainee/Details/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select * from fn_ViewAdmisionDetails($1)', i).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:  Admision Table','ITR','INFO','Retrive  of trainee Admision Table  successfull','trainee Admision',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Admission Details Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:  Admision Table','ITR','ERR','Retrive  of trainee Admision Table  unsuccessfull','trainee Admision',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Admission Details"
                    })
                })
            });
    })
    //Update trainee Admission
    organizationRouter.post('/Trainee/Update/:admissionNum', (req, res, next) => {
        i = req.params.admissionNum;
        pm = req.body.parentEmail;
        pmb = req.body.parentMobile;
        sbid = req.body.branchId;
        uid = req.body.userId;
        fid = req.body.feeId;
        cid = req.body.concessionId;
        db.any('select * from fn_updateadmisiondetails($1,$2,$3,$4,$5,$6,$7)', [i, pm, pmb, sbid, uid, fid, cid]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:  Admision Table','ITR','INFO','Update  of trainee Admision Table  successfull','trainee Admision',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: i,
                        message: "Admission Details Has Been Updated Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:  Admision Table','ITR','ERR','Update  of trainee Admision Table  unsuccessfull','trainee Admision',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Cannot Update Admission Details."
                    })
                })
            });
    })
   //add transfer Traineeid to to transfer
   organizationRouter.post('/Branch/Transfer/Trainee/Add/:sessionparam', (req, res, next) => {
    var uid = req.params.sessionparam;
    for (var i = 0; i < uid.length; i++) {
        uid = uid.replace('-', '/');
    }
    //var userId
    db.any('select fn_viewuserid from fn_viewuserid($1)', uid).then((data) => {
        uid = data[0].fn_viewuserid
        db.any('select * from  fn_addtraineetransfer($1)', [uid]).then((data) => {
            db.any("insert into  domainlogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values  ('Table-Addtion:transfer','ITR','INFO','Addtion of transfer Table   successfull','transfer',true,$1)", uid).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Transfer  Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any("insert into  DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values  ('Table-Addtion:transfer','ITR','ERR','Addtion of transfer Table  unsuccessfull','transfer',False,$1)", uid).then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Add Transfer"
                })
            })
        })
})
organizationRouter.post('/Tc/Approve', (req, res, next) => {
    var sessionparam=req.body.userSession;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
   db.any("select fn_viewuserid from fn_viewuserid($1)",sessionparam).then((user)=>{
    userId = user[0].fn_viewuserid
    db.any('update admisiondetails set status=$1 where admissionnum=$2 and branchid=(select branchid from userprofileview where id=$3)',[req.body.type,req.body.admissionNum,uid]).then((data) => {
        //uid = data[0].fn_viewuserid
            db.any("insert into  domainlogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values  ('Table-Addtion:transfer','ITR','INFO','Addtion of transfer Table   successfull','transfer',true,$1)", uid).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "TC Request Updated"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "TC Request Updated"
                    })
                }
            })
        })
    }) 
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                userId = Session[0].fn_viewuserid;
            db.any("insert into  DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values  ('Table-Addtion:transfer','ITR','ERR','Addtion of transfer Table  unsuccessfull','transfer',False,$1)", uid).then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Update TC Request"
                })
            })
        })
        })
})
    organizationRouter.get('/ToBranch/AcceptTransferList/:userSession', (req, res, next) => {
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_toBranchAcceptStudents($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: To Branch Approve','ITR','INFO','Retrive  Details of To Branch Approve Table successfull',' To Branch Approve',,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "branch approve transfer Retrived  Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                     userId = session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: To Branch Approve','ITR','ERR','Retrive  Details of To Branch Approve Table unsuccessfull',' To Branch Approve',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable To branch approve transfer"
                        })
                    })
                });
            });
    })
    organizationRouter.get('/Branch/TraineeTransferStatus/:userSession', (req, res, next) => {
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewStdntTransferStatus($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                 userId = Session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: trainee Transfer Status','ITR','INFO','Retrive  Details of trainee Transfer Status Table successfull',' trainee Transfer Status',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Trainee Transfer Status Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                     userId = Session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: trainee Transfer Status','ITR','ERR','Retrive  Details of trainee Transfer Status Table unsuccessfull',' trainee Transfer Status',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Trainee Transfer Status"
                        })
                    })
                })
            });
    })
}
//=======================================Allocate Shifts==================================================
{
    organizationRouter.post('/Allocate/User/Shift', (req, res, next) => {
        var deptUsers = req.body.DeptUsers;
        var departmentId = parseInt(req.body.allocateShift.departmentId);
        var startDate = req.body.allocateShift.startDate;
        var endDate = req.body.allocateShift.endDate;
        var shiftId = parseInt(req.body.allocateShift.shiftId);
        var addAllocate = [];
        //var count;
        function addAllocateShifts(deptUsers, departmentId, shiftId, startDate, endDate, callback) {
            var count = 0
            for (var i = 0; i < deptUsers.length; i++) {
                if (deptUsers[i].checkBoxStatus == true) {
                    count = count + 1;
                    db.any("select * from fn_addallocateshifts($1,$2,$3,$4,$5)", [deptUsers[i].userid, departmentId, shiftId, startDate, endDate]).then((data) => {
                    }).catch(error => {
                            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: trainee Transfer Status','ITR','ERR','Add Allocate shifts Table unsuccessfull',' Add Allocate shifts',False)").then((log) => {
                                res.status(200).send({
                                    result: false,
                                    error: error.message,
                                    data: "ERROR",
                                    message: "Unable to Allocate Shifts"
                                })
                            })
                        })
                        if(i==deptUsers.length-1)
                        {
                            callback(count)
                        }
                }
            }
        }
        addAllocateShifts(deptUsers, departmentId, shiftId, startDate, endDate, function (count) {
            if (count <= 0) {
                res.status(200).send({
                    result: false,
                    error: ":Error",
                    data: "ERROR",
                    message: "Please Tick the Staff to Allocate Shifts"
                })
            }
            else {
                addAllocate = []
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: trainee Transfer Status','ITR','INFO','Add Allocate shifts Table successfull','Add Allocate Shifts',true)").then((log) => {
                    if (addAllocate.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: addAllocate,
                            message: "Shifts Allocated Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "Shifts Allocated Successfully"
                        })
                    }
                })
            }
        })
    })
    
    organizationRouter.get('/AllocateShifts/ViewAll/:UserSession', (req, res, next) => {
        var sessionparam = req.params.UserSession;
        var userId;
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select  * from fn_viewallocateshifts($1)',sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                userId = session[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:AllocateShifts','ITR','INFO','View of AllocateShifts Table successfull','AllocateShifts',true,$1)",userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "AlloateShifts Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                    userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,Userid) values ('Table-View:AllocateShifts','ITR','ERR','View of AllocateShifts Table unsuccessfull','AllocateShifts',false,$1)",userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Cannot View AllocateShifts"
                    })
                })
            });
        })
    })	
}
//=======================================Courses==========================================================
{
    organizationRouter.post('/courses/Add', (req, res, next) => {
        cnm = req.body.courseName;
        yr = parseInt(req.body.years)
        sim = req.body.semesters;
        lvl = req.body.level;
        drt=req.body.duration;
        db.any('select * from fn_addcourses($1,$2,$3,$4,$5)', [cnm, yr, sim, lvl,drt]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Courses','ITR','INFO','Addition of Courses Table successfull','Courses',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Courses Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Courses','ITR','ERR','Addition of Events Table unsuccessfull','Courses',False)").then((log) => {
                    console.log('ERROR:Unable to Add courses', error.message || error);
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add courses"
                    })
                });
            });
    })
    organizationRouter.get('/courses/Details', (req, res, next) => {
        db.any('select  * from fn_viewcourses()').then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Courses','ITR','INFO','View of Courses Table successfull','Courses',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Course Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Courses','ITR','ERR','View of Courses Table unsuccessfull','Courses',false)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Cannot View Course Details"
                    })
                })
            });
    })
    // Get By Courses By ID
    organizationRouter.get('/courses/DetailsbyId/:Id', (req, res) => {
        id = req.params.Id;
        db.any('select * from  fn_viewCoursesdetailsbyid($1)', id).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Events','ITR','INFO','View of Events Table successfull','Events',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Course Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Events','ITR','ERR','View of Events Table unsuccessfull','Events',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to view Courses Details"
                    })
                })
            })
    })
    //Update Courses
    organizationRouter.post('/courses/Update/:id', (req, res) => {
        var id = req.params.id;
        var crs = req.body.courseName;
        var yr = req.body.years
        var semtr = req.body.semesters + '';
        var lvl = req.body.level;
        db.any('select * from fn_updatecourse($1,$2,$3,$4,$5)', [id, crs, yr, semtr, lvl]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:courses','ITR','INFO','Update of courses Table  successfull',' courses',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Courses Updated Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:courses','ITR','ERR','Update of courses Table  unsuccessfull',' courses',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Cannot Update Courses"
                    })
                })
            });
    });
}
//=======================================Department=======================================================
{   //Dept Add
    organizationRouter.post('/Departments/Add', (req, res, next) => {
        dptname = req.body.name;
        brId = req.body.branchId;
        db.any('select * from fn_Adddepartment($1,$2)', [dptname, brId]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:courses','ITR','INFO','Update of courses Table  successfull',' courses',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Department Has been added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:courses','ITR','INFO','Update of courses Table  successfull',' courses',false)").then((log) => {  
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Add department"
                })
            });})
    })
    // View Dept
    // View Dept
    organizationRouter.get('/Departments/Details/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewdepartment($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Departments','ITR','INFO','View of Courses Table Departments','Departments',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Department Retrieved Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        }).catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Departments','ITR','INFO','View of Departments Table unsuccessful','Departments',false,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Departments Details"
                    })
                })
            });
        })
    });
     //User of Dept
     organizationRouter.get('/Branch/Dept/Users/:deptId', (req, res, next) => {
        db.any('select * from staff s join users u on u.id=s.userid where departmentid=$1', req.params.deptId).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view: Shiftdetails','ITR','INFO','Retrive of Shiftdetails Table successfull',' Shiftdetails',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Department Retrived  Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
                .catch(error => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Shiftdetails','ITR','ERR','Retrive of Shiftdetails Table unsuccessfull',' Shiftdetails',False)").then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Department"
                        })
                    })
                })
        })
    })
}
//========================================Domain==================================================================
{
    organizationRouter.post('/Domain/Add', (req, res, next) => {
        console.log("yugyug")
        console.log(req.body)
        dnm = req.body.name;
        stf = req.body.staffId;
        cid = req.body.courseId;
        db.any('select  * from courses where id=$1', [cid]).then((data) => {
            var courseData = data
            var courseYear = parseInt(JSON.stringify(courseData[0].years))
            for (var i = 1; i <= courseYear; i++) {
                for (var j = 1; j <= 2; j++) {
                    var cls = dnm + '' + i + '-' + j
                    console.log(cls)
                    if(courseData[0].durationtype=='Year'){
                        console.log(cls+"if")

                    db.any('insert into sessionmaster(sessionname) values($1)', "SCL_"+cls).then((data) => { })
                }
                else{
                    console.log(cls)

                    db.any('insert into sessionmaster(sessionname) values($1)', "SCL_"+dnm).then((data) => { })

                }
            }
        }

            db.any('select * from fn_Adddepartments($1,$2,$3)', ["DO_"+dnm, stf, cid]).then((data) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Domain Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Add departmemts"
                })
            });
    })
    organizationRouter.get('/Domain/Details/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_getDepartments($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Departments','ITR','INFO','View of Courses Table Departments','Departments',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Domain Details Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        }).catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Departments','ITR','INFO','View of Departments Table unsuccessful','Departments',false,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Domain Details"
                    })
                });
            })
        })
    });
    //Get Departments By Id
    organizationRouter.get('/Domain/DetailbyId/:Id', (req, res) => {
        id = req.params.Id;
        db.any('select * from  fn_viewdepartmentdetailsbyid($1)', id).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Events','ITR','INFO','View of Events Table successfull','Events',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Domain Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Events','ITR','ERR','View of Events Table unsuccessfull','Events',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Domian Details"
                    })
                })
            })
    })
    organizationRouter.get('/Domain/Details/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_getDepartments($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Departments','ITR','INFO','View of Courses Table Departments','Departments',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Domain Details Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        }).catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Departments','ITR','INFO','View of Departments Table unsuccessful','Departments',false,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Domain Details"
                    })
                });
            })
        })
    });
    //Udate Departments By ID
    organizationRouter.post('/Domain/Update/:Id', (req, res) => {
        var id = req.params.Id;
        var nm = req.body.name;
        var sfid = req.body.staffId;
        var cid = parseInt(req.body.courseid);
        db.any('select * from fn_updatedepartments($1,$2,$3,$4)', [id, nm, sfid, cid]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:departments','ITR','INFO','Update of departments Table  successfull',' departments',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Domain Updated Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:departments','ITR','ERR','Update of departments Table  unsuccessfull',' departments',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Cannot Update Domain"
                    })
                })
            });
    })
}
//===================================================Events===========================================================
{
    organizationRouter.post('/Events/Add', (req, res, next) => {
        nam = req.body.name;
        depid = req.body.departmentId;
        fd = req.body.fromDate;
        td = req.body.toDate;
        st = req.body.startTime;
        et = req.body.endTime;
        uid = req.body.userId;
        cid = req.body.contactdetailsId;
        des = req.body.description;
        ven = req.body.venue;
        bran = req.body.branchId;
        db.any('select fn_addevents($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)', [nam, depid, fd, td, st, et, uid, cid, des, ven, bran]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtions: Events Mangement','ITR','INFO','Addititon of Events Management Table successfull',' Events Management',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Events Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "Events Added Successfully"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtions: Events Management','ITR','ERR','Addititon of Events Management Table unsuccessfull',' Events Management',False)").then((log
                ) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: 'Unable to Add Events'
                    })
                })
            });
    })
    organizationRouter.get('/Single/Event/:id', (req, res, next) => {
        id = req.params.id;
        db.any('select * from fn_vieweventdetailsbyid($1)', id).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Events','ITR','INFO','View of Events Table successfull',' Events',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Event Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Events','ITR','ERR','User of Events Table unsuccessfull',' Events',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: 'Unable to View Event'
                    })
                })
            });
    });
    organizationRouter.post('/Events/Update/:id', (req, res, next) => {
        id = req.params.id;
        eid = req.body.name;
        did = req.body.departmentId;
        fd = req.body.fromDate;
        td = req.body.toDate;
        uid = req.body.userId;
        st = req.body.startTime;
        et = req.body.endTime;
        cid = req.body.contactdetailsId;
        d = req.body.description;
        v = req.body.venue;
        br = req.body.branchId;
        db.any('select * from fn_updateeventsdetails($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)', [id, eid, did, fd, td, uid, d, v, st, et, cid, br]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Events','ITR','INFO','Update of Events Table successfull',' Events',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Events Updated Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Events','ITR','ERR','Update of Events Table unsuccessfull',' Events',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: 'Unable to Update Events'
                    })
                })
            });
    })
    organizationRouter.get('/All/Events/:userSession', (req, res, next) => {
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewevents($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Events','ITR','INFO','View of Events Table successfull',' Events',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Events Retrieved Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: Events','ITR','ERR','View of Events Table unsuccessfull',' Events',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: 'Unable to View Events'
                        })
                    })
                })
            });
    });
    organizationRouter.get('/All/Calender/:userSession', (req, res, next) => {
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_vieweventscalender($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Events','ITR','INFO','View of Events Table successfull',' Events',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Calendar Retrieved Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: Events','ITR','ERR','View of Events Table unsuccessfull',' Events',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: 'Unable to View Events Calender'
                        })
                    })
                })
            });
    })
    organizationRouter.get('/Users/All/:usersession', (req, res, next) => {
        sessionparam = req.params.usersession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewuserbybranchid($1)', [sessionparam]).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:User','ITR','INFO','View of User Table successfull',' User',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "All Users Retrieved Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: User','ITR','ERR','User of Events Table unsuccessfull',' User',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: 'Unable to View Users'
                        })
                    })
                })
            });
    })
    organizationRouter.post('/EventResponse/:id/:approve', (req, res, next) => {
        var Id = req.params.id;
        var Approved = req.params.approve;
        db.any('update  eventrequest set status=$2 where userid=$1',[Id,Approved] ).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Events','ITR','INFO','View of Events Table successfull',' Events',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Events Updated Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Events','ITR','ERR','User of Events Table unsuccessfull',' Events',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: 'Unable to View Event'
                    })
                })
            });
    });
    organizationRouter.get('/College/Contact/:userId', (req, res, next) => {
        var userId = req.params.userId;
        db.any('select * from fn_viewcontactdetailsbyuserid($1)', userId).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:College','ITR','INFO','View of College Table successfull',' College',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Contact Details of User Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: College','ITR','ERR','User of College Table unsuccessfull',' College',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: 'Unable to View Contacts of User'
                    })
                })
            });
    })
    organizationRouter.get('/Events/Request/Participation/:eventid/:usersession', (req, res, next) => {
        var eid = req.params.eventid;
        sessionparam = req.params.usersession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select fn_viewuserid from fn_viewuserid($1)', [sessionparam]).then((dbsession) => {
            userId = dbsession[0].fn_viewuserid;
            db.any('select * from fn_addeventparticipationrequest($1,$2)', [eid, userId]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Events','ITR','INFO','View of Events Table successfull',' Events',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Event Participation Requested Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Events','ITR','ERR','User of Events Table unsuccessfull',' Events',False,$1)",userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: 'Unable to View Event Request'
                    })
                })
            })
            });
    })
    organizationRouter.get('/Events/Request/List/:usersession', (req, res, next) => {
        sessionparam = req.params.usersession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewalleventrequest($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Events','ITR','INFO','View of Events Table successfull',' Events',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Event Requested List Retrieved Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: Events','ITR','ERR','User of Events Table unsuccessfull',' Events',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: 'Unable to View Events List'
                        })
                    })
                })
            });
    })
}
//===================================================Branch===========================================================
{   //Add Branch
    organizationRouter.post('/Branch/Add', (req, res, next) => {
        sid = req.body.organizationId;
        startdate = req.body.startDate
        st = 'INACTIVE';
        contact = req.body.contactDetailsId;
        lat = req.body.latitude;
        long = req.body.longitude;
        db.any('select * from fn_AddBranch($1,$2,$3,$4,$5,$6)', [sid, startdate, st, contact, lat, long]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition: Organization Branch','ITR','INFO','Addition of Organization Branch Table successfull',' Organization Branch',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: " Branch Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition: Organization Branch','ITR','ERR','Addition of Organization Branch Table unsuccessfull',' Organization Branch',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Branch"
                    })
                })
            });
    })
    organizationRouter.get('/Branch/:userSession', (req, res, next) => {
        var OrganizationId;
        var userSession = req.params.userSession;
        var userId='';
        for (var i = 0; i < userSession.length; i++) {
            userSession = userSession.replace('-', '/');
        }
        db.any("select Organizationid from userprofileview where id=(select fn_viewuserid from fn_viewuserid($1))", userSession).then((data) => {
            OrganizationId = data[0].organizationid;
            db.any('select fn_viewuserid from fn_viewuserid($1)', userSession).then((Session) => {
                 userId = Session[0].fn_viewuserid;
                db.any('select * from  fn_Viewbranches($1,$2)', [OrganizationId, userSession]).then(function (branches) {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: Organization Branch','ITR','INFO','Retrive of Organization Branch Table successfull',' Organization Branch',true,$1)", userId).then((log) => {
                        if (branches.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: branches,
                                message: "Branch Retrived Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', userSession).then((Session) => {
                     userId = Session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Organization Branch','ITR','ERR','Retrive of Organization Branch Table unsuccessfull',' Organization Branch',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Branch"
                        })
                    })
                })
            });
    })
    organizationRouter.get('/Branches/Details/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId=''; 
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_Viewbranch($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
             userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Organization Branch','ITR','INFO','Retrive of Organization Branch Table successfull',' Organization Branch',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: " Branch Details Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Organization Branch','ITR','ERR','Retrive of Organization Branch Table unsuccessfull',' Organization Branch',False,$1)",userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Branch Details"
                    })
                })
                })
            });
    })
    //GET For School Branch
    organizationRouter.get('/Branch/Details/:Id', (req, res, next) => {
        var schbrid = req.params.Id;
        db.any('select * from  fn_viewbranchdetails($1)', [schbrid]).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Organization Branch','ITR','INFO','Retrive of Organization Branch Table successfull',' Organization Branch',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Branch Details Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Organization Branch','ITR','ERR','Retrive of Organization Branch Table unsuccessfull',' Organization Branch',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Branch Details"
                    })
                })
            });
    })
    organizationRouter.post('/Branch/Status/Update/:Id', (req, res) => {
        sbrid = req.params.Id;
        sta = req.body.status;
        if (sta == true) {
            sta = "Active"
        } else {
            sta = "Inactive"
        }
        db.any('select * from fn_updateOrganizationbranchstatus($1,$2)', [sbrid, sta]).then((data) => {
            res.send(data)
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Organization Branch','ITR','INFO','Update of Organization Branch Table successfull',' Organization Branch',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: sbrid,
                        message: "Branch Has Been Updated Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Organization Branch','ITR','ERR','Update of Organization Branch Table unsuccessfull',' Organization Branch',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Cannot Update Branch"
                    })
                })
            });
    });
    organizationRouter.post('/Branch/Update/:Id', (req, res) => {
        sbrid = req.params.Id;
        schid = req.body.organizationId;
        sdate = req.body.startDate;
        sta = req.body.status;
        if (sta == true) {
            sta = "Active"
        } else {
            sta = "Inactive"
        }
        cndtid = req.body.contactDetailsId;
        db.any('select * from fn_updateOrganizationbranch($1,$2,$3,$4,$5)', [sbrid, schid, sdate, sta, cndtid]).then(() => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Organization Branch','ITR','INFO','Update of Organization Branch Table successfull',' Organization Branch',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: sbrid,
                        message: "Branch Has Been Updated Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Organization Branch','ITR','ERR','Update of Organization Branch Table unsuccessfull',' Organization Branch',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Cannot Update Branch"
                    })
                })
            });
    })
}
//==========================================Branch-Session===========================================================
{
    organizationRouter.get('/Session/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_ViewOrganizationSession($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                 userId = Session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view: Branch Session','ITR','INFO','Retrive of Branch Session Table successfull','Branch Session',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Session Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((Session) => {
                     userId = Session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view: Branch Session','ITR','ERR','Retrive of Branch Session Table unsuccessfull','Branch Session',False,$1)",userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: 'Unable to view Session'
                        })
                    })
                })
            });
    })
    organizationRouter.get('/Session/Details/:Id', (req, res, next) => {
        var i = req.params.Id;
        var userId='';
        db.any('select * from fn_viewOrganizationsessiondetails($1)', i).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((data) => {
                 userId = data[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view: Branch Session','ITR','INFO','Retrive of Branch Session Table successfull','Branch Session',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Session Details Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view: Branch Session','ITR','ERR','Retrive of Branch Session Table unsuccessfull','Branch Session',False,$1)",userId).then((log) => {
                    res.status.send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: 'Unable to view Session Details'
                 }) })
                })
            });
    })
    organizationRouter.post('/Branch/Session/Update/:Id', (req, res) => {
        var bid = req.params.Id;
        var cid = req.body.sessionId;
        var brnid = req.body.branchId;
        var med = req.body.medium;
        var nofs = req.body.numOfTrainees;
        var dpt = req.body.departmentid
        db.any('select * from fn_UpdateBranchSession($1,$2,$3,$4,$5,$6)', [bid, cid, brnid, med, nofs, dpt]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Branch Session','ITR','INFO','update of Branch Session Table successfull','Branch Session',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: bid,
                        message: "Session Has Been Updated Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Branch Session','ITR','ERR','Update of Branch Session Table unsuccessfull','Branch Session',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: 'Cannot Update  Session'
                    })
                })
            });
    });
    organizationRouter.post('/Session/Add', (req, res, next) => {
        cid = req.body.sessionId;
        bid = req.body.branchId;
        med = req.body.medium;
        nos = req.body.numOfTrainees;
        dep = req.body.departmentid;
        db.any('select fn_addbranchSession($1,$2,$3,$4,$5)', [cid, bid, med, nos, dep]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtions: Branch Session','ITR','INFO','Retrive of Branch Session Table successfull',' Branch Session',true)").then((d) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: 'Branch Session Has Been Added Successfully'
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtions: Branch Session','ITR','ERR','Retrive of Branch Session Table unsuccessfull',' Branch Session',False)").then((d) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: 'Unable to Add Session'
                    })
                })
            });
    })
}
//===========================================Shifts=========================================================
{
    organizationRouter.get('/Branch/AllShifts/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select branchid from userprofileview where userid=(select fn_viewuserid from fn_viewuserid($1))', sessionparam).then((data) => {
            db.any('select * from fn_getshiftbybranch($1)', data[0].branchid).then(function (shifts) {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                    userId  = session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view: AllShifts','ITR','INFO','Retrive of AllShifts Table successfull',' AllShifts',true,$1)", userId).then((log) => {
                        if (shifts.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: shifts,
                                message: " Shifts Retrived  Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                     userId = session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: AllShifts','ITR','ERR','Retrive of AllShifts Table unsuccessfull',' AllShifts',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View All Shifts"
                        })
                    })
                });
            })
    })
}
//===========================================Organization=========================================================================
{
    organizationRouter.get('/OrganizationDetails/Verify/:inp/:inpData', (req, res, next) => {
        inp = req.params.inp;
        inpData = req.params.inpData;
        db.any('select * from Organization where ' + inp + ' = $1', [inpData]).then((data) => {
            if (data.length > 0) {
                var msg = inp + ' already Exists!!';
                res.send({
                    message: false
                });
            } else {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Organization D','ITR','INFO','Addition of Organization D Table successfull','Organization D',true)").then((log) => {
                    res.status(200).send({
                        message: true
                    })
                })
            }
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Organization D','ITR','ERR','Addition of Organization D Table unsuccessfull','Organization D',False)").then((log) => {
                    res.status(200).send({
                        message: "Cannot View Organization Details "
                    })
                })
            })
    })
    organizationRouter.get('/details/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select * from Organization where id=$1', i).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Organization','ITR','INFO','ViRetriveew  Details of  Organization Table successfull','  Organization',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Organization Details Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
                // res.status(200).send(data);
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Organization','ITR','ERR','Retrive  Details of  Organization Table unsuccessfull','  Organization',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Organization Details"
                    })
                })
            });
    })
    //GET for  organization
    organizationRouter.get('/Organizations/:userSession', (req, res, next) => {
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_ViewOrganizations($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((user) => {
                 userId = user[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Organization','ITR','INFO','Retrive  Details of Organization Table successfull',' Organization',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Organizations Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((data) => {
                     userId = data[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Organization','ITR','ERR','Retrive  Details of Organization Table unsuccessfull',' Organization',False,$1)",userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to view Organizations"
                        })
                    })
                })
            });
    })
    organizationRouter.post('/Add/Organization', (req, res, next) => {
        OrganizationTypeid = parseInt(req.body.organizationTypeId);
        boardid = parseInt(req.body.boardId);
        Organizationname = req.body.name;
        tp = req.body.type
        st = 'INACTIVE'
        est = req.body.establishedYear
        contact = req.body.contactDetailsId
        img = req.body.image
        db.any('select * from fn_addOrganization($1,$2,$3,$4,$5,$6,$7)', [OrganizationTypeid, boardid, Organizationname, tp, st, est, contact]).then((Id) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition: Organization','ITR','INFO','Organization of signup Table successfull',' Organization',true)").then((log) => {
                fn = Id[0].fn_addorganization + '.png';
                ps = './public/slogo/' + fn;
                var dir = './public/' + 'slogo';
                if (!fs.existsSync(dir)) {
                    fs.mkdirSync(dir);
                }
                fs.writeFile(ps, img, 'base64', (err) => { })
                if (Id.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: Id,
                        message: " Organization Has Been Added Successfully"
                    });
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition: Organization','ITR','ERR','Organization of signup Table unsuccessfull',' Organization',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Organization"
                    })
                })
            })
    })
    //GET Organization FUNCTION (view requests by id)
    organizationRouter.get('/request/', (req, res, next) => {
        db.any('select * from Organization').then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Organization','ITR','INFO','Retrive  Details of Organization Table successfull',' Organization',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Organization Details Retrive Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Organization','ITR','ERR','Retrive  Details of Organization Table unsuccessfull',' Organization',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to view Organization Details"
                    })
                })
            });
    })
    organizationRouter.post('/Update/:Id', (req, res, next) => {
        i = req.params.Id;
        n = req.body.name;
        typ = req.body.type;
        st = req.body.status;
        if (st == true) {
            st = "Active"
        } else {
            st = "Inactive"
        }
        ey = req.body.establishedYear;
        rd = req.body.registrationDate;
        cd = req.body.contactDetailsId;
        img = req.body.image
        db.any('select * from fn_updateOrganization($1,$2,$3,$4,$5,$6,$7)', [i, n, typ, st, ey, rd, cd]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: Organization ','ITR','INFO','Update of  Organization  Table successfull','  Organization ',true)").then((log) => {
                ps = './public/slogo/' + i + '.png';
                fs.writeFile(ps, img, 'base64', (err) => {
                    if (err)
                        console.log(err)
                    else {
                        console.log('Image Saved');
                    }
                })
            })
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Organization Has Been Updated Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:  Organization ','ITR','ERR','Update of Organization  Table unsuccessfull','  Organization ',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Cannot Update Organization"
                    })
                })
            });
    })
    //GET for  organization
    organizationRouter.get('/Organizations/:userSession', (req, res, next) => {
        var sessionparam = req.params.userSession;
       var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_ViewOrganizations($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((user) => {
                 userId = user[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Organization','ITR','INFO','Retrive  Details of Organization Table successfull',' Organization',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Organizations Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((data) => {
                     userId = data[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Organization','ITR','ERR','Retrive  Details of Organization Table unsuccessfull',' Organization',False,$1)",userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to view Organizations"
                        })
                    })
                })
            });
    })
}
//==============================================Shifts======================================================
{
    //Post for Shifts
    organizationRouter.post('/Branch/Shifts/Add', (req, res, next) => {
        var bid = req.body.branchId;
        var sname = req.body.shiftName;
        var days = []
        var stime = req.body.startTime;
        var etime = req.body.endTime;
        var grcintime = req.body.graceInTime;
        var grcouttime = req.body.graceOutTime;
        var opt = req.body.optional
        if (req.body.monday == true) {
            req.body.monday = 'Monday';
            days.push(req.body.monday);
        }
        if (req.body.tuesday == true) {
            req.body.tuesday = 'Tuesday';
            days.push(req.body.tuesday);
        }
        if (req.body.wednesday == true) {
            req.body.wednesday = 'Wednesday';
            days.push(req.body.wednesday);
        }
        if (req.body.thursday == true) {
            req.body.thursday = 'Thursday';
            days.push(req.body.thursday);
        }
        if (req.body.friday == true) {
            req.body.friday = 'Friday';
            days.push(req.body.friday);
        }
        if (req.body.saturday == true) {
            req.body.saturday = 'Saturday';
            days.push(req.body.saturday);
        }
        if (req.body.sunday == true) {
            req.body.sunday = 'Sunday';
            days.push(req.body.sunday);
        }
        db.any('select * from fn_addshifts($1,$2,$3,$4,$5,$6,$7,$8)', [bid, sname, days, stime, etime, grcintime, grcouttime, opt]).then((data) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Shifts Has Been Added Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
    organizationRouter.get('/Branch/AllShifts/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select branchid from userprofileview where userid=(select fn_viewuserid from fn_viewuserid($1))', sessionparam).then((data) => {
            db.any('select * from fn_getshiftbybranch($1)', data[0].branchid).then(function (data) {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                     userId = session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view: AllShifts','ITR','INFO','Retrive of AllShifts Table successfull',' AllShifts',true,$1)", userid).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: " Shifts Retrived  Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                     userId = session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: AllShifts','ITR','ERR','Retrive of AllShifts Table unsuccessfull',' AllShifts',False,$1)", userid).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View All Shifts"
                        })
                    })
                });
            })
    })
    organizationRouter.get('/Branch/Shiftby/:Id', (req, res, next) => {
        var i = req.params.Id;
        var days = []
        db.any('select * from fn_getshiftbyid($1)', i).then(function (data) {
            days = data[0].param_days
            for (var i = 0; i < days.length; i++) {
                if (days[i] == 'Monday') {
                    data[0].monday = true;
                }
                if (days[i] == 'Tuesday') {
                    data[0].tuesday = true;
                }
                if (days[i] == 'Wednesday') {
                    data[0].wednesday = true;
                }
                if (days[i] == 'Thursday') {
                    data[0].thursday = true;
                }
                if (days[i] == 'Friday') {
                    data[0].friday = true;
                }
                if (days[i] == 'Saturday') {
                    data[0].saturday = true;
                }
                if (days[i] == 'Sunday') {
                    data[0].sunday = true;
                }
            }
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view: Shiftdetails','ITR','INFO','Retrive of Shiftdetails Table successfull',' Shiftdetails',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Shift Details  Retrived  Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Shiftdetails','ITR','ERR','Retrive of Shiftdetails Table unsuccessfull',' Shiftdetails',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Shift Details"
                    })
                })
            })
    })
    //Update for shifts
  //Update for shifts
  organizationRouter.post('/Branch/Shifts/Update/:Id', (req, res, next) => {
    var id = req.params.Id;
    var brid = req.body.branchId;
    var sn = req.body.shiftName;
    var dys = req.body.days;
    var st = req.body.startTime;
    var et = req.body.endTime;
    var grintime = req.body.graceInTime;
    var grouttime = req.body.graceOutTime;
    var opt = req.body.optional;
    var days = [];
    if (req.body.monday == true) {
        req.body.monday = 'Monday';
        days.push(req.body.monday);
    }
    if (req.body.tuesday == true) {
        req.body.tuesday = 'Tuesday';
        days.push(req.body.tuesday);
    }
    if (req.body.wednesday == true) {
        req.body.wednesday = 'Wednesday';
        days.push(req.body.wednesday);
    }
    if (req.body.thursday == true) {
        req.body.thursday = 'Thursday';
        days.push(req.body.thursday);
    }
    if (req.body.friday == true) {
        req.body.friday = 'Friday';
        days.push(req.body.friday);
    }
    if (req.body.saturday == true) {
        req.body.saturday = 'Saturday';
        days.push(req.body.saturday);
    }
    if (req.body.sunday == true) {
        req.body.sunday = 'Sunday';
        days.push(req.body.sunday);
    }
    db.any('select * from fn_updateshifts($1,$2,$3,$4,$5,$6,$7,$8,$9)', [id, brid, sn, days, st, et, grintime, grouttime, opt]).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Shifts Details','ITR','INFO','Addtion of Shifts Table  successfull','Shifts',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Shifts Details  Has Been Added Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion:Shifts Details','ITR','ERR','Addtion of Shifts Table unsuccessfull','Shifts',False)").then((log) => {
                console.log('ERROR:Unable to Add Shifts Details', error.message || error);
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Add Shifts Details"
                })
            })
        })
})
    organizationRouter.get('/Branch/ShiftsAll/', (req, res, next) => {
        db.any('select * from fn_getallshifts()').then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Shifts','ITR','INFO','Viewing of Shifts Table successfull','Shifts',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Session Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Shifts','ITR','ERR','Viewing of Shifts Table unsuccessfull','Shifts',false)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable To View Shifts"
                    })
                })
            })
    })
}
//===============================================Transfers=================================================
{
    organizationRouter.get('/FromBranch/AprvTransfer/:userSession', (req, res, next) => {
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewFrmBranchApprove($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: From Branch Approve','ITR','INFO','Retrive  Details of From Branch Approve Table successfull',' From Branch Approve',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Session Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                     userId = session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: From Branch Approve','ITR','ERR','Retrive  Details of From Branch Approve Table unsuccessfull',' From Branch Approve',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable from branch approve transfer"
                        })
                    })
                });
            });
    })
    organizationRouter.post('/Branch/Transfer/toBranch/Add/:sessionparam/:branchId/:classId', (req, res, next) => {
        var uid = req.params.sessionparam;
        var branchid = req.params.branchId;
        var classid = req.params.classId;
        var userId='';
        for (var i = 0; i < uid.length; i++) {
            uid = uid.replace('-', '/');
        }
        db.any('select fn_viewuserid from fn_viewuserid($1)', uid).then((data) => {
            userId = data[0].fn_viewuserid
            db.any('select * from  fn_updateStdntTransferbytoBranch($1,$2,$3)', [branchid, classid, uid]).then((data) => {
                db.any("insert into  domainlogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values  ('Table-Addtion:transfer','ITR','INFO','Addtion of transfer Table   successfull','transfer',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "transfer  Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                db.any("insert into  DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values  ('Table-Addtion:transfer','ITR','ERR','Addtion of transfer Table  unsuccessfull','transfer',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "unable to add transfer"
                    })
                })})
            })
    })
    //add transfer frombranchid to to transfer
    organizationRouter.post('/Branch/Transfer/fromBranch/Add/:sessionparam/:id', (req, res, next) => {
        var uid = req.params.sessionparam;
        var stdId = req.params.id;
        var userId='';
        for (var i = 0; i < uid.length; i++) {
            uid = uid.replace('-', '/');
        }
        db.any('select fn_viewuserid from fn_viewuserid($1)', uid).then((data) => {
            userId = data[0].fn_viewuserid
            db.any('select * from  fn_updateStdntTransferbyfrmBranch($1,$2)', [uid, stdId]).then((data) => {
                db.any("insert into  domainlogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values  ('Table-Addtion:transfer','ITR','INFO','Addtion of transfer Table   successfull','transfer',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Transfer Successfully Allcated"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                db.any("insert into  DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values  ('Table-Addtion:transfer','ITR','ERR','Addtion of transfer Table  unsuccessfull','transfer',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Transfer"
                    })
                })
            })
            })
    })
    organizationRouter.get('/Branch/Transfer/:userSession', (req, res, next) => {
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_getApplytransfer($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: transfer','ITR','INFO','Retrive  Details of transfer Table successfull',' transfer',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "transfer Retrived  Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                     userId = session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: transfer','ITR','ERR','Retrive  Details of transfer Table unsuccessfull',' transfer',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to view transfer"
                        })
                    })
                });
            });
    })
    //get All Organizations
    organizationRouter.get('/AllOrganizations/', (req, res, next) => {
        db.any('select * from fn_viewOrganizations()').then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view: Organizations','ITR','INFO','Retrive of Organizations Table successfull',' Organizations',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Session Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Organizations','ITR','ERR','Retrive of Organizations Table unsuccessfull',' Organizations',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Organizations"
                    })
                })
            })
    })
    organizationRouter.get('/TcApplicants/:userSession', (req, res, next) => {
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewTcApplicants($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: TC Applicants','ITR','INFO','Retrive  Details of TC Applicants Table successfull',' TC Applicants',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "TC Applicants  Retrived  Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                     userId = session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: TC Applicants','ITR','ERR','Retrive  Details of TC Applicants Table unsuccessfull',' TC Applicants',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to TC Applicants"
                        })
                    })
                });
            });
    })
    organizationRouter.post('/Branch/Accept/Trainee/:studentid', (req, res, next) => {
        var i = req.params.studentid;
        db.any('select * from fn_updateStdntTcAcceptence($1)', i).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view: TC Acceptence','ITR','INFO','Retrive of TC Acceptence Table successfull',' TC Acceptence',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Session Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: TC Acceptence','ITR','ERR','Retrive of TC Acceptence Table unsuccessfull',' TC Acceptence',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View TC Acceptence"
                    })
                })
            })
    })
    organizationRouter.get('/AllBranches/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select * from fn_getbranches($1)', i).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view: Branches','ITR','INFO','Retrive of Branches Table successfull',' Branches',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Session Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Branches','ITR','ERR','Retrive of Branches Table unsuccessfull',' Branches',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Branches"
                    })
                })
            })
    })
    organizationRouter.get('/Branch/AllSessions/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select * from fn_getClassbyBranch($1)', i).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view: AllSessions','ITR','INFO','Retrive of AllSessions Table successfull',' AllSessions',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Session Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: AllSessions','ITR','ERR','Retrive of AllSessions Table unsuccessfull',' AllSessions',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View All Sessions"
                    })
                })
            })
    })
    organizationRouter.get('/User/Achievement/:Id', (req, res, next) => {
        var userId = req.params.Id;
        db.any('select * from fn_ViewAchievementbyUserId($1)', userId).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userid) values ('Table-view:Achievement','ITR','INFO','view of Achievement Table successfull','Achievement',true,$1)",userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Achievements Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "Achievements Retrived Successfully"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:Achievement','ITR','ERR','view of Achievement Table unsuccessfull','Achievement',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Achievements by Userid"
                    })
                });
            })
    })
}
module.exports = organizationRouter;
