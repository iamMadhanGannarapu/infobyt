var express = require('express');
var bodyparser = require('body-parser');
var payslip = require('../payslip')
var fs = require('fs')
var path = require('path')
var mail = require('../mailer');
var userRouter = express.Router()
userRouter.use(express.static(path.resolve(__dirname, "public")));
userRouter.use(bodyparser.json({
    limit: '5mb'
}));
userRouter.use(bodyparser.urlencoded({
    limit: '5mb',
    extended: true
}));
var config = require('../init_app');
const dba = require('../db.js');
const db = dba.db;
const pgp = db.$config.pgp;
//==========================================> Enquiry <========================================
{
    userRouter.post('/User/Login/Add', (req, res, next) => {
        name = req.body.name
        email = req.body.email
        mobile = req.body.mobile
        message = req.body.message
        db.any('select fn_addEnquiry($1,$2,$3,$4)', [name, email, mobile, message]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Enquiry','ITR','INFO','Addition of Enquiry Table successfull','Enquiry',true)").then((log) => {
                    res.status(200).send(data);
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Enquiry','ITR','ERR','Addition of Enquiry Table unsuccessfull','Enquiry',False)").then((log) => {
                    console.log('ERROR:Unable to Add Enquiry', error.message || error);
                    res.status(200).send({
                        message: "Unable to Add Enquiry"
                    });
                });
            })
    })
    userRouter.get('/StaffUsers/FeedBack/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_ViewAllStaffUsers($1)', sessionparam).then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:StaffUsers','ITR','INFO','view of StaffUsers Table successfull','StaffUsers',true)").then((log) => {
                    res.status(200).send(data);
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:StaffUsers','ITR','ERR','view of StaffUsers Table unsuccessfull','StaffUsers',False)").then((log) => {
                    console.log('ERROR:Unable to View Staff Users', error.message || error);
                    res.status(200).send({
                        message: "Unable to View Staff Users"
                    })
                });
            })
    })
}
//===================================Contact Details======================================
{
    //POST For ContactDetails by mail
    userRouter.post('/ContactDetails/Add', (req, res, next) => {
    uid = req.body.userId
    con = req.body.country
    st = req.body.state
    ci = req.body.city
    pl = req.body.place
    adrs = req.body.address
    adh = req.body.uniqueIdentificationNo
    em = req.body.email
    mbl = req.body.mobile
    db.any('select fn_AddContactDetails($1,$2,$3,$4,$5,$6,$7,$8,$9)', [uid, con, st, ci, pl, adrs, adh, em, mbl]).then((Id) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:ContactDetails','ITR','INFO','Addition of ContactDetails Table successfull','ContactDetails',true)").then((log) => {
            mail.mailer(Id[0].fn_addcontactdetails, 'INFOBYT Email Verification', 'SignUp');
            if (Id.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: Id,
                    message: "ContactDetails by mail Has Been Added Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "NO DATA FOUND"
                })
            }
        })
    }).catch(error => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:ContactDetails','ITR','ERR','Addition of ContactDetails Table unsuccessfull','ContactDetails',False)").then((log) => {
            res.status(200).send({
                result: "false",
                error: error.message,
                data: "ERROR",
                message: "Unable to Add Contact Details",
            });
        });
    })
})
userRouter.get('/Login/Update/:mailId/:password', function (req, res, next) {
    var mid = req.params.mailId;
    var pwd = req.params.password;
    var password;
    var Id;
    db.any('select * from loginview where email=$1', mid).then((login) => {
        db.any("select passcode from authenticationmaster where loginid=$1", login[0].loginid).then((authentication) => {
            var random = authentication[0].passcode + ''
            subString = random.substring(0, 3) 
            password = subString + pwd + '';
            subString = random.substring(3, 7);
            password = password + subString;
            db.any("update login set password=crypt($1,gen_salt('md5')) where userid=$2 returning password", [password, login[0].id]).then((update) => {
                db.any("select LoginId from loginview where email=$1", mid).then((LoginData) => {
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Login','ITR','INFO','View of LOGIN Table successfull','Login',true)").then((log) => {
                            res.status(200).send(LoginData)
                        })
                    })
                    .catch(error => {
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Login','ITR','ERR','View of LOGIN Table unsuccessfull','Login',False)").then((log) => {
                            res.status(200).send({
                                message: " Unable to View Userid by session"
                            });
                        });
                    })
            })
        })
    })
})
    //get all contact details using branchid
    userRouter.get('/Users/ContactDetails/:userSession', (req, res, next) => {
        var sessionparam = req.params.userSession;
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from  fn_viewcontactdetails($1)', sessionparam).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:ContactDetails','ITR','INFO','View of ContactDetails Table successfull','ContactDetails',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "ContactDetails Has Been Retrieved Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "NO DATA FOUND"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:ContactDetails','ITR','ERR','View of ContactDetails Table unsuccessfull','ContactDetails',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable To View Contact Details"
                    });
                });
            })
    });
     //Get By Id For  Contact Details 
     userRouter.get('/ContactDetails/:userId', (req, res, next) => {
        i = req.params.userId
        db.any('select * from fn_viewcontactdetails($1)', i).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:ContactDetails','ITR','INFO','View of ContactDetails Table successfull','ContactDetails',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: Id[0],
                            message: "ContactDetails Has Been Retrieved Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "NO DATA FOUND"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:ContactDetails','ITR','ERR','View of ContactDetails Table unsuccessfull','ContactDetails',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable To View Contact Details "
                    });
                });
            })
    });
    //Get All Religions
    userRouter.get('/religion', function (req, res, next) {
        db.any("select * from fn_getallreligion()").then((religion) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:religion','ITR','INFO','View of religion Table successfull','religion',true)").then((log) => {
                    if (religion.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: religion,
                            message: "Religion Retrieved Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "NO DATA FOUND"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:religion','ITR','ERR','View of religion Table unsuccessfull','religion',False)").then((log) => {     
                    res.status(200).send({
                        result: false,
                        error: error,
                        data: "ERROR",
                        message: " Unable To Retrieve religion"
                    });
                });
            })
    });
    //Get all Countries
    userRouter.get('/countries/', function (req, res, next) {  
    db.any("select * from fn_getallcountries()").then((countries) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Countries','ITR','INFO','View of Countries Table successfull','Countries',true)").then((log) => {
                if (countries.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: countries,
                        message: "Countries Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "NO DATA FOUND"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Countries','ITR','ERR','View of Countries Table unsuccessfull','Countries',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: " Unable To Retrive Countries"
                });
            });
        })
});
//Get all States
userRouter.get('/states/:Id', function (req, res, next) {
    var state_id = req.params.Id;
    db.any("select * from fn_getstates($1)", state_id).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:states','ITR','INFO','View of states Table successfull','states',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "States Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "NO DATA FOUND"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:states','ITR','ERR','View of states Table unsuccessfull','states',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Retrive States"
                });
            });
        })
});
//Get all Cities
userRouter.get('/cities/:Id', function (req, res, next) {
    var id = req.params.Id;
    db.any("select * from fn_getcities($1)", id).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:cities','ITR','INFO','View of cities Table successfull','cities',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Cities Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "NO DATA FOUND"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:cities','ITR','ERR','View of cities Table unsuccessfull','cities',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to Retrive Cities"
                });
            });
        })
});
 // POST FOR state
 userRouter.post('/data/State/enter', (req, res, next) => {
    var ccode = req.body.countryId;
    var name = req.body.name;
    db.any("select id from countries where countrycode=$1 ", [ccode]).then((data) => {
            sid = data[0].id;
            db.any('insert into states (name,countryid) values($1,$2)', [name, sid]).then(() => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:state','ITR','INFO','Addition of state Table successfull','state',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "State Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "NO DATA FOUND"
                        })
                    }
                });
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:state','ITR','ERR','Addition of state Table unsuccessfull','state',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable To Add States"
                });
            });
        })
});
 // POST FOR city
 userRouter.post('/data/Cities/enter', (req, res, next) => {
    var ccode = req.body.stateId;
    var name = req.body.name;
    db.any("select id from states where name=$1 ", [ccode]).then((data) => {
            sid = data[0].id;
            db.any('insert into cities (name,stateid) values($1,$2)', [name, sid]).then(() => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:city','ITR','INFO','Addition of city Table successfull','city',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "City Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "NO DATA FOUND"
                        })
                    }
                })
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:city','ITR','ERR','Addition of city Table unsuccessfull','city',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: "error.message",
                    data: "ERROR",
                    message: "Unable To Add City"
                })
            })
        })
});
}
//=========================================Role====================================
 {//Post For Role 
 userRouter.post('/Role/Add', (req, res, next) => {
    rnm = req.body.name;
    db.any('select * from fn_AddRoleMaster($1)', [rnm]).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Role','ITR','INFO','Addition of ROLE Table successfull','Role',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: " Role Has Been Added Sucessfully"
                })
            }
            else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Role','ITR','ERR','Addition of ROLE Table unsuccessfull','Role',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: " Unable to Add Role"
                })
            });
        })
});
  // Get For  Role 
  userRouter.get('/Roles/', (req, res, next) => {
    db.any('select * from fn_viewrole()').then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Role','ITR','INFO','View of ROLE Table successfull','Role',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: " Data Retrieved Role "
                })
            }
            else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Role','ITR','ERR','View of ROLE Table unsuccessfull','Role',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: " Unable to View Role"
                })
            });
        })
});
 //Get By Id For  Role 
 userRouter.get('/Role/Details/:Id', (req, res, next) => {
    var i = req.params.Id;
    db.any('select * from fn_viewroledetails($1)', i).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Role','ITR','INFO','View of ROLE Table successfull','Role',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Role Details Retrieved Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Role','ITR','ERR','View of ROLE Table unsuccessfull','Role',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: " Unable to View Role By Id"
                })
            });
        })
});
 //Get By UserSession For  Role 
 userRouter.get('/Role/User/:userSession', (req, res, next) => {
    var sessionparam = req.params.userSession;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select * from fn_viewroledetailsofuser($1)', sessionparam).then((data) => {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
             userId = dbsession[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Role','ITR','INFO','View of ROLE Table successfull','Role',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: " Role Retrieved Sucessfully"
                    })
                }
                else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Role','ITR','ERR','View of ROLE Table unsuccessfull','Role',false,$1)",userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: " Unable to View Role"
                    })
                });
            })})
    });
  
 // Update for  Role
 userRouter.post('/Role/Update/:Id', (req, res, next) => {
    rid = req.params.Id;
    rlname = req.body.name;
    sta = req.body.status;
    if (sta == true) {
        sta = true
    } else {
        sta = false
    }
    db.any('select * from fn_updaterole($1,$2,$3)', [rid, rlname, sta]).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Role','ITR','INFO','Update of ROLE Table successfull','Role',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: " Subject Has Been Added Sucessfully"
                })
            }
            else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    }).catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Role','ITR','ERR','Update of ROLE Table successfull','Role',false)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: " Unable to View Role"
                })
            });
        })
});
     //GET All Roles
     userRouter.get('/AllRoles', (req, res, next) => {
        db.any('select id,rolename from rolemaster').then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view: Permissions','ITR','INFO','Retrive of Permissions Table successfull','Permissions',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: " Permissions by roles Retrieved Sucessfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view: Permissions','ITR','ERR','Retrive of Permissions Table unsuccessfull','Permissions',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: 'Unable to view Permissions by role'
                    })
                })
            });
    })
}
//============================================User========================================
{
     //POST for  Users
     userRouter.post('/Add/:Id', (req, res, next) => {
        fnme = req.body.firstName
        mnme = req.body.middleName
        lnme = req.body.lastName
        dateofbrth = req.body.dob
        gndr = req.body.gender
        fathernme = req.body.fatherName
        mothernme = req.body.motherName
        fathroccption = req.body.fatherOccupation
        cste = req.body.caste
        subscste = req.body.subCaste
        rlgen = req.body.religion
        nationlty = req.body.nationality
        rl = req.body.role;
        var sessionparam = req.params.Id;
        var sscpe = null;
        var intper = null;
        var ugper = null;
        var posper = null;
        var otper =null;
        var exper = null;
        var adminId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((data) => {
            adminId = data[0].fn_viewuserid
                db.any('select fn_AddUsers($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14)', [fnme, mnme, lnme, dateofbrth, gndr, fathernme, mothernme, fathroccption, cste, subscste, rlgen, nationlty, rl, adminId]).then((Id) => {
                    uid=Id[0].fn_addusers
                    db.any('select * from fn_addacdemicdetails($1,$2,$3,$4,$5,$6,$7)', [uid, sscpe, intper, ugper, posper, otper, exper]).then(() => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-Addition:Users','ITR','INFO','Addition of Users Table successfull','Users',true,$1)",uid).then((log) => {
                        if (Id.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: Id[0],
                                message: "Users Has Been Added Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "NO DATA FOUND"
                            })
                        }
                    })
                })})
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-Addition:Users','ITR','ERR','Addition of Users Table unsuccessfull','Users',False,$1)",userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Users"
                    })
                })})
            })
    });
    //Add Admin
    userRouter.post('/Add/Admin/:Id', (req, res, next) => {
        fnme = req.body.firstName
        mnme = req.body.middleName
        lnme = req.body.lastName
        dateofbrth = req.body.dob
        gndr = req.body.gender
        fathernme = req.body.fatherName
        mothernme = req.body.motherName
        fathroccption = parseInt(req.body.fatherOccupation)
        cste = req.body.caste
        subscste = req.body.subCaste
        rlgen = parseInt(req.body.religion)
        nationlty = parseInt(req.body.nationality)
        rl = req.body.role;
        adminId = req.params.Id;
        db.any('select fn_addadmin($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14)', [fnme, mnme, lnme, dateofbrth, gndr, fathernme, mothernme, fathroccption, cste, subscste, rlgen, nationlty, rl, adminId]).then((Id) => {
                if (Id.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: Id[0],
                        message: "Admin Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "NO DATA FOUND"
                    })
                }
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Admin','ITR','ERR','Addition of Admin Table unsuccessfull','Admin',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Admin"
                    });
                });
            })
    });
    //Get Different Users
    userRouter.get('/Users/Branch/', (req, res, next) => {
        var i = 'BranchAdmin'
        db.any('select * from fn_ViewAllUsers($1)', i).then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:Users','ITR','INFO','view of Users Table successfull','Users',true)").then((log) => {
                    if (data.length > 0) {
                       // var msg = inp + ' already Exists!!';
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Users Retrieved succesfully"
                        });
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No data found"
                        });
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:Users','ITR','ERR','view of Users Table unsuccessfull','Users',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View  Users"
                    })
                });
            })
    });
    //Get All Users
    userRouter.get('/Users/All/', (req, res, next) => {
        var i = ''
        db.any('select * from fn_ViewAllUsers($1)', i).then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:AllUsers','ITR','INFO','view of AllUsers Table successfull','AllUsers',true)").then((log) => {
                    if (data.length > 0) {
                     //   var msg = inp + ' already Exists!!';
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "All Users Retrieved succesfully"
                        });
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No data found"
                        });
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:AllUsers','ITR','ERR','view of AllUsers Table unsuccessfull','AllUsers',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View All Users"
                    })
                });
            })
    });
    //Get All Staff Users
    userRouter.get('/StaffUsers/All/:userSession', (req, res, next) => {
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_ViewAllStaffUserss($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:StaffUsers','ITR','INFO','view of StaffUsers Table successfull','StaffUsers',true,$1)",userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Staff Users Retrieved succesfully"
                        });
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No data found"
                        });
                    }
                })})
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((data) => {
                    userId = data[0].fn_viewuserid
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:StaffUsers','ITR','ERR','view of StaffUsers Table unsuccessfull','StaffUsers',False,$1)",userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Staff Users"
                    })
                });
            })})
    });
    //Get All Staff Users By Id
    userRouter.get('/StaffUsers/Details/:userSession', (req, res, next) => {
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewallstaffusers($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((data) => {
                userId = data[0].fn_viewuserid
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:StaffUsers','ITR','INFO','view of StaffUsers Table successfull','StaffUsers',true,$1)",userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Staff Users Has Been Retrieved Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "NO DATA FOUND"
                        })
                    }
                })})
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((data) => {
                    userId = data[0].fn_viewuserid
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:StaffUsers','ITR','ERR','view of StaffUsers Table unsuccessfull','StaffUsers',False,$1)",userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable To Retrieve  Staff Users"
                    });
                });
            })})
    });
    //Get All Drivers
    userRouter.get('/Applicants/Driver/:RoleName/:userSession/:dId', (req, res, next) => {
        var roleName = req.params.RoleName
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewallApplicantsbyrolebyid($1,$2) where userid=$3', [roleName, sessionparam, req.params.dId]).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:Applicants','ITR','INFO','view of Applicants Table successfull','Applicants',true,$1)",userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Applicants Has Been Retrieved succesfully "
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })})
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:Applicants','ITR','ERR','view of Applicants Table unsuccessfull','Applicants',False,$1)",userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Applicants"
                    });
                });
            })})
    });
    //Get All Trainers
    userRouter.get('/Users/Trainer/', (req, res, next) => {
        var i = 'Trainer'
        db.any('select * from fn_ViewAllUsers($1)', i).then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:AllUsers','ITR','INFO','view of AllUsers Table successfull','AllUsers',true)").then((log) => {
                    if (data.length > 0) {
                        var msg = inp + ' already Exists!!';
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Trainer Retrieved succesfully"
                        });
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No data found"
                        });
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:AllUsers','ITR','ERR','view of AllUsers Table unsuccessfull','AllUsers',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Class Trainer"
                    })
                });
            })
    });
    //Get All Trainees
    userRouter.get('/Users/Trainee/:userSession', (req, res, next) => {
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_ViewAllUsers($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:Users','ITR','INFO','view of Users Table successfull','Users',true,$1)",userId).then((log) => {
                    if (data.length > 0) {
                        var msg = inp + ' already Exists!!';
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Trainees Retrieved succesfully"
                        });
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No data found"
                        });
                    }
                })})
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:Users','ITR','ERR','view of Users Table unsuccessfull','Users',False,$1)",userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Class Trainee "
                    })
                });
            })})
    });
    //Get All Driver Details
    userRouter.get('/Users/DriverDetails/', (req, res, next) => {
        var sessionparam = 'Driver';
        db.any('select * from fn_ViewAllUsers($1)', sessionparam).then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:DriverDetails','ITR','INFO','view of DriverDetails Table successfull','DriverDetails',true)").then((log) => {
                    if (data.length > 0) {
                        var msg = inp + ' already Exists!!';
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Driver Retrieved succesfully"
                        });
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No data found"
                        });
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:DriverDetails','ITR','ERR','view of DriverDetails Table unsuccessfull','DriverDetails',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Driver details "
                    })
                });
            });
    });
     //get city name by city id
     userRouter.get('/cities/city/:Id', function (req, res, next) {
        var id = req.params.Id;
        db.any("select * from cities where id=($1)", id).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:city','ITR','INFO','View of city Table successfull','city',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Cities BY Id Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "NO DATA FOUND"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:city','ITR','ERR','View of city Table unsuccessfull','city',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Retrive Citites by Id"
                    });
                });
            })
    });
}
//===========================================Middle Names=========================
{  
    //Get All Middle Names
    userRouter.get('/MiddleNames/', (req, res, next) => {
        db.any('select castename as name from  subcaste').then((data) => {
          db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:MiddleNames','ITR','INFO','view of MiddleNames Table successfull','MiddleNames',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Middle Names Has Been Retrieved succesfully "
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
             })
        }).catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:MiddleNames','ITR','ERR','view of MiddleNames Table unsuccessfull','MiddleNames',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    data: "ERROR",
                    error: error.message,
                    message: "Unable to View Middle Names"
                });
            });
        })
    })
}
//========================================Occupations=========================
{
    userRouter.get('/Occupations/', (req, res, next) => {
        db.any('select * from designation  order by name asc').then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Designation','ITR','INFO','View of DESIGNATION Table successfull','Designation',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Occupations Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "NO DATA FOUND"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Designation','ITR','ERR','View of DESIGNATION Table unsuccessfull','Designation',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: " Unable To Retrive Occupations"
                    });
                });
            })
    })
}
//===========================================medium=======================================
{
    userRouter.get('/Medium/', (req, res, next) => {
        db.any('select * from mediums  order by name asc').then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:medium','ITR','INFO','View of MEDIUM Table successfull','medium',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Medium Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "NO DATA FOUND"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:medium','ITR','ERR','View of MEDIUM Table unsuccessfull','medium',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: " Unable To View Medium"
                    });
                });
            })
    });
     //get userid from session
     userRouter.get('/UserSession/:userSession', function (req, res, next) {
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any("select * from LogedView($1)", sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Login','ITR','INFO','View of LOGIN Table successfull','Login',true,$1)",userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "UserSession Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "NO DATA FOUND"
                    })
                }
            })})
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Login','ITR','ERR','View of LOGIN Table unsuccessfull','Login',False,$1)",userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: " Unable To View User Session"
                    });
                });
            })})
    });
    userRouter.get('/Staff/Transport/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewStaffForTransport($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:StaffForTransport','ITR','INFO','view of StaffForTransport Table successfull','StaffForTransport',true,$1)",userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "View Staff For transport Retreived succesfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }})
                });
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:StaffForTransport','ITR','ERR','view of StaffForTransport Table unsuccessfull','StaffForTransport',False,$1)",userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Staff For transport"
                    })
                });})
            });
    });
            // Get User Name By usser session
            userRouter.get('/View/Username/:userSession', (req, res, next) => {
                var sessionparam = req.params.userSession;
                var userId='';
                for (var i = 0; i < sessionparam.length; i++) {
                    sessionparam = sessionparam.replace('-', '/');
                }
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {

                db.any('select * from  fn_getUserName($1)', sessionparam).then((data) => {
                        uuserId = dbsession[0].userid;
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:User Name','ITR','INFO','View of User Name Table successfull','User Name',true,$1)", userId).then((log) => {
                            if (data.length > 0) {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: data,
                                    message: "User Name Alloacted Successfully"
                                })
                            } else {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: "NDF",
                                    message: "No Data Found"
                                })
                            }
                        })
                    })
                })
                    .catch(error => {
                        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                           
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:User Name','ITR','ERR','View of User Name Table unsuccessfull','User Name',False,$1)", userId).then((log) => {
                            res.status(200).send({
                                result: false,
                                error: error.message,
                                data: "error",
                                message: "Unable to View User Name"
                            })
                        })})
                    })
                })
}
//======================================get All nationalities==============================
{
    userRouter.get('/Nationalities/', (req, res, next) => {
        db.any('select * from nationalities  order by name asc').then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Nationalities','ITR','INFO','View of Nationalities Table successfull','Nationalities',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Nationalities Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "NO DATA FOUND"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Nationalities','ITR','ERR','View of Nationalities Table unsuccessfull','Nationalities',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: " Unable to Retrive Nationalities"
                    });
                });
            })
    })
}
//==================================Testimonial============================
{
     //Add Testimonial 
     userRouter.post('/testimonial/Add', (req, res, next) => {
        id = req.body.Id
        name = req.body.name
        description = req.body.description
        var img = req.body.image
        db.any('select *from fn_testimonials($1,$2)', [name, description]).then((id) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Testimonial','ITR','INFO','Addition of Testimonial Table successfull','Testimonial',true)").then((log) => {
                    res.status(200).send();
                    fn = id[0].fn_testimonials + '.png';
                    ps = './public/reviews/' + fn;
                    fs.writeFile(ps, img, 'base64', (err) => {
                        if (err) {
                            console.log('image upload Failure')
                        } else {
                            console.log('Image Save SuccessFully')
                        }
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "Testimonial Has Been Added Sucessfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Testimonial','ITR','ERR','Addition of Testimonial Table unsuccessfull','Testimonial',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Testimonial"
                    })
                });
            })
    });
     //View Testimonial 
     userRouter.get('/testimonial', (req, res, next) => {
        var userId='';
        var sessionparam='';
        db.any('select * from fn_viewtestimonial()').then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view: Permissions','ITR','INFO','Retrive of Permissions Table successfull','Permissions',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Testimonial Retrieved Sucessfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }})
                })
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view: Permissions','ITR','ERR','Retrive of Permissions Table unsuccessfull','Permissions',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Testimonial"
                    })
                });})
            })
    })
}
  //=======================================Trainee and staff logs===============================
  {
    //get logs by staff
    userRouter.get('/StaffLogs/branch/Details/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewstafflogs($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Subject','ITR','INFO','View of SUBJECT Table successfull','Subject',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "StaffLog Details Retrieved Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })})
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Subject','ITR','ERR','View of SUBJECT Table unuccessfull','Subject',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "error",
                        message: "Unable to View StaffLog Details."
                    })
                });})
            });
    })
    userRouter.get('/TraineeLogs/branch/Details/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewstudentlogs($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Subject','ITR','INFO','View of SUBJECT Table successfull','Subject',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Student Details Retrieved Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })})
            })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Subject','ITR','ERR','View of SUBJECT Table unuccessfull','Subject',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "error",
                        message: "Unable to View TraineeLogs Details."
                    })
                });})
            });
    })
}
//==============================Academic Details==========================
{
    userRouter.post('/Add/Applicant/Academicdetails', (req, res, next) => {
        var uid = req.body.userId;
        var sscpe = req.body.sscPercentage;
        var intper = req.body.interPercentage;
        var ugper = req.body.ugPercentage;
        var posper = req.body.pgPercentage;
        var otper = req.body.otherPercentage;
        var exper = req.body.experience;
        db.any('select * from fn_addacdemicdetails($1,$2,$3,$4,$5,$6,$7)', [uid, sscpe, intper, ugper, posper, otper, exper]).then(() => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Add:Academic Details ','ITR','INFO','Add of Academic Details successfull','Academic Details',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Academic Details Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Add:Academic Details ','ITR','ERR','Add of   Academic Details unsuccessfull','Academic Details',true)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "error",
                        message: "Cannot add Academic Details"
                    })
                })
            });
    });
    //  Update Academic details
    userRouter.post('/Update/Applicant/Academicdetails', (req, res, next) => {
        var uid = req.body.userId;
        var sscpe = req.body.sscPercentage;
        var intper = req.body.interPercentage;
        var ugper = req.body.ugPercentage;
        var posper = req.body.pgPercentage;
        var otper = req.body.otherPercentage;
        var exper = req.body.experience;
        db.any('select * from fn_updateacademicdetails($1,$2,$3,$4,$5,$6,$7)', [uid, sscpe, intper, ugper, posper, otper, exper]).then(() => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Academic Details ','ITR','INFO','Update of academic details  successfull','academic details',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Academic details sRetrieved Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Academic Details ','ITR','ERR','Update of academic details unsuccessfull',' academic details',true)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Cannot add academic details"
                    })
                })
            })
    })
  // Get Academic details
  userRouter.get('/Academic/Details/:userSession', (req, res, next) => {
    var sessionparam = req.params.userSession;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select * from fn_getacademicdetails($1)', sessionparam).then((data) => {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
            userId = dbsession[0].fn_viewuserid
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:academicdetails','ITR','INFO','View of academic details Table successfull','academic details',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: " Academic Details Alloacted Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })})
        })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:academicdetails','ITR','ERR','View of academic details Table unsuccessfull','academic details',False,$1)", userId).then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "error",
                    message: "Unable to View Academic Details"
                })
            })})
        })
});
 //GET Component By Module
 userRouter.get('/Module/Component/:moduleName', (req, res, next) => {
    var mname = req.params.moduleName;
    db.any('select id,components from components where modules=$1', mname).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view: Permissions','ITR','INFO','Retrive of Permissions Table successfull','Permissions',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Component By Module Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view: Permissions','ITR','ERR','Retrive of Permissions Table unsuccessfull','Permissions',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: 'Unable to view Component By Module'
                })
            })
        });
})
}
userRouter.get('/StaffUsers/FeedBack/:Id', (req, res, next) => {
    var sessionparam = req.params.Id;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select fn_viewuserid from fn_viewuserid($1)',sessionparam).then((udata)=>{
            db.any('select * from fn_ViewAllStaffUsers($1)', sessionparam).then(function (data) {
                 userId = udata[0].fn_viewuserid;
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:StaffUsers','ITR','INFO','view of StaffUsers Table successfull','StaffUsers',true,$1)", userId).then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Staff Users Retrieved Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })
})
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)',sessionparam).then((udata)=>{
            db.any('select * from fn_ViewAllStaffUsers($1)', sessionparam).then(function (udata) {
                 userId = udata[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:StaffUsers','ITR','ERR','view of StaffUsers Table unsuccessfull','StaffUsers',False,$1)", userId).then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Staff Users"
                })
            });
        });
        })
    })
})
userRouter.get('/Trans/:RoleName/:userSession', (req, res, next) => {
    var roleName = req.params.RoleName
    var sessionparam = req.params.userSession;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select fn_viewuserid from fn_viewuserid($1)',sessionparam).then((user)=>{
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
            userId = dbsession[0].fn_viewuserid
        db.any('select * from fn_viewallapplicantsbyroleFortrainees($1,$2)', [roleName, sessionparam]).then(function (data) {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:Applicants','ITR','INFO','view of Applicants Table successfull','Applicants',true,$1)", userId).then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Students Retrieved Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
    })})})
})
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)',sessionparam).then((udata)=>{
            db.any('select * from fn_viewallapplicantsbyroleForStudents($1,$2)', [roleName, sessionparam]).then(function (data) {
                 userId = udata[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:Applicants','ITR','ERR','view of Applicants Table unsuccessfull','Applicants',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Applicants"
                    })
                });
            });
        })
    })
})
userRouter.get('/Users/ClassTeacher/:userSession', (req, res, next) => {
    sessionparam = req.params.userSession
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any("select fn_viewuserid from fn_viewuserid($1)",sessionparam).then((user)=>{
        userId = user[0].fn_viewuserid;
        db.any('select * from fn_viewsessionteachers($1)', sessionparam).then(function (data) {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:classteachers','ITR','INFO','view of classteachers Table successfull','Users',true,$1)", userId).then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "ClassTeacher Retrieved Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "No Data Found"
                })
            }
        })
    })})
        .catch(error => {
            db.any('select * from fn_viewclassteachers($1)', sessionparam).then(function (data) {
                 userId = data[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:classteachers','ITR','ERR','view of classteachers Table unsuccessfull','Users',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Class Teachers"
                    })
                })
            });
        });
})
//user.js
userRouter.get('/Applicants/:RoleName/:userSession', (req, res, next) => {
    var roleName = req.params.RoleName
    var userId='';
    var sessionparam = req.params.userSession;
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any("select fn_viewuserid from fn_viewuserid($1)",sessionparam).then((user)=>{
        userId=user[0].fn_viewuserid
    db.any('select * from fn_viewallApplicantsbyrole($1,$2)', [roleName, sessionparam]).then(function (data) {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:roles of branch','ITR','INFO','view of roles Table successfull','Users',true,$1)", userId).then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Applicants Retrieved Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "Applicants Retrieved Successfully"
                })
            }
        })
    })})
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                userId = dbsession[0].fn_viewuserid
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:classteachers','ITR','ERR','view of classteachers Table unsuccessfull','Users',False,$1)", userId).then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Applicants"
                })
            });})
        })
})
 //Get By RoleName and SchoolId For  Contact Details 
 userRouter.get('/Users/ContactDetails/role/:roleName/:Id/:cId', (req, res, next) => {
    roleName = req.params.roleName
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewcontactdetailsbyrolebyid($1,$2) where contactid=$3', [roleName, sessionparam, req.params.cId]).then((data) => {
           db.any('select fn_viewuserid from fn_viewuserid($1)',sessionparam).then((dbsession)=>{ 
            userId = dbsession[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:classteachers','ITR','INFO','view of classteachers Table successfull','Users',true,$1)", userId).then((log) => {
               console.log(data)
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: " Contact Details by roleid and Schoolid Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }              })
        })})
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:classteachers','ITR','ERR','view of classteachers Table unsuccessfull','Users',False,$1)", userId).then((log) => {   
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Contact Details by roleid and Schoolid"
                })
                });
            })})
    })




    userRouter.get('/Users/ContactDetails/role/:roleName/:Id', (req, res, next) => {
        roleName = req.params.roleName
        var sessionparam = req.params.Id;
        var userId="";
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewcontactdetailsbyrole($1,$2)', [roleName, sessionparam]).then((data) => {
           db.any('select fn_viewuserid from fn_viewuserid($1)',sessionparam).then((user)=>{ 
            userId = user[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:classteachers','ITR','INFO','view of classteachers Table successfull','Users',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: " Contact Details by roleid and Schoolid Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }              })
        })})
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                                         userId = dbsession[0].fn_viewuserid
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:classteachers','ITR','ERR','view of classteachers Table unsuccessfull','Users',False,$1)", userId).then((log) => {   
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Contact Details by roleid and Schoolid"
                })
                });})
            })
    })
    userRouter.get('/StaffUsers/Staff/:RoleName/:userSession', (req, res, next) => {
        var roleName = req.params.RoleName
        var userId='';
        var sessionparam = req.params.userSession;
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any("select fn_viewuserid from fn_viewuserid($1)", sessionparam).then((user) => {
            db.any('select * from fn_viewallstaffbyrole($1,$2)', [roleName, sessionparam]).then(function (data) {
                userId = user[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:classteachers','ITR','INFO','view of classteachers Table successfull','Users',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "View users by role successful"
                        })
                    }
                    else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "View users by role successful"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select * from fn_viewallstaffbyrole($1,$2)', [roleName, sessionparam]).then(function (data) {
                     userId = data[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:classteachers','ITR','ERR','view of classteachers Table unsuccessfull','Users',False,$1)", userId).then((log) => {
                      res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "NDF",
                            message: "Unable to view staff by role"
                        })
                    });
                });
            })
    })
    //Get By UserSession For  Role Master
    userRouter.get('/Role/User/:userSession', (req, res, next) => {
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewroledetailsofuser($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Role','ITR','INFO','View of ROLE Table successfull','Role',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: " Role Retrieved Sucessfully"
                        })
                    }
                    else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
            })
                .catch(error => {
                    db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                        userId = dbsession[0].fn_viewuserid
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Role','ITR','ERR','View of ROLE Table unsuccessfull','Role',false,$1)",userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: " Unable to View Role"
                        })
                    });
                })})
        })
        userRouter.get('/Branch/User/:session', (req, res, next) => {
            var sessionparam = req.params.session;
            var userId='';
            for (var i = 0; i < sessionparam.length; i++) {
                sessionparam = sessionparam.replace('-', '/');
            }
            db.any('select * from fn_getuserdetails($1)', [sessionparam]).then(function (data) {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                    userId = dbsession[0].fn_viewuserid
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:userdetails','ITR','INFO','view of userdetails table successfull','userdetails',true,$1)",userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: " User Details Retrieved Sucessfully"
                            })
                        }
                        else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })})
                .catch(error => {
                    db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                        userId = dbsession[0].fn_viewuserid
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-Addition:userdetails','ITR','ERR','Addition of userdetails Table unsuccessfull','userdetails',False,$1)",userId).then((log) => {
                        console.log('ERROR:Unable to View user Details', error.message || error);
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: " Unable to View User Details"
                        })
                    });})
                })
        });
module.exports = userRouter;
