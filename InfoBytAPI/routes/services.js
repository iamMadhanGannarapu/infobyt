var express = require('express');
var bodyparser = require('body-parser');
var payslip = require('../payslip')
var fs = require('fs')
var path = require('path')
var mail = require('../mailer');
var servicesRouter = express.Router()
servicesRouter.use(express.static(path.resolve(__dirname, "public")));
servicesRouter.use(bodyparser.json({
    limit: '5mb'
}));
servicesRouter.use(bodyparser.urlencoded({
    limit: '5mb',
    extended: true
}));
var config = require('../init_app');
const dba = require('../db.js');
const db = dba.db;
const pgp = db.$config.pgp;
{
    servicesRouter.post('/GatePass/Add/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        description = req.body.description;
        indate = req.body.inDate;
        intime = req.body.inTime;
        branchid = req.body.branchId;
        db.any('select * from fn_addgatepass($1,$2,$3,$4,$5)', [description, indate, intime, branchid, sessionparam]).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-Addtion:gatepass','ITR','INFO','Addtion of gatepass Table  successfull','gatepass',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Gate Pass Has Been Added Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-Addtion:gatepass','ITR','ERR','Addtion of gatepass Table  unsuccessfull','gatepass',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Gate Pass"
                    });
                })
            })
            });
    })
    servicesRouter.get('/GatePass/Details/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId ='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select  * from fn_viewgatepass($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:gatepass','ITR','INFO','view of gatepass Table  successfull','gatepass',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Gate Pass Retrived  Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:gatepass','ITR','ERR','View of gatepass Table unsuccessfull','gatepass',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Gate Pass"
                    })
                })
            })
            })
    })
    servicesRouter.get('/GatePass/DetailbyId/:id', (req, res, next) => {
        var paramsid = req.params.id
        db.any('select  * from fn_viewgatepassById($1)', [paramsid]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:gatepass','ITR','INFO','View of gatepass Table successfull','gatepass',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Gate Pass Has Been Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:gatepass','ITR','ERR','View of gatepass Table unsuccessfull','gatepass',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Gate Pass Details"
                    })
                })
            })
    })
    servicesRouter.post('/GatePass/Update/:id', (req, res, next) => {
        ids = req.params.id;
        sta = req.body.status
        db.any('update gatepass set status=$2 where id=$1', [ids, sta]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:gatepass','ITR','INFO','view of gatepassTable successfull','gatepass',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: ids,
                        message: "Gate Pass Has Been Updated Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: gatepass','ITR','ERR','Update of gatepass Table unsuccessfull','gatepass',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: 'Cannot Update Gate Pass'
                    })
                })
            })
    })
}
//=======================================================Hostel==============================
{
    //Post For  hostel
    servicesRouter.post('/Hostel/Add', (req, res, next) => {
        usid = req.body.userId
        brni = req.body.branchId;
        typ = req.body.type;
        fac = req.body.facilities;
        shng = req.body.sharing;
        bname = req.body.blockName;
        rnum = req.body.roomNum;
        adate = req.body.allocationDate;
        rpd = req.body.representativeId;
        rid = req.body.rentalPeriod;
        db.any('select * from fn_addhostel($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)', [usid, brni, typ, fac, shng, bname, rnum, adate, rpd, rid]).then(() => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:hostel','ITR','INFO','view of hostel table successfull','hostel',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Hostel Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:hostel','ITR','ERR','Addition of hostel Table unsuccessfull','hostel',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add hostel"
                    })
                });
            });
    })
    // for getallhostel
    servicesRouter.get('/Hostel/:session', (req, res, next) => {
        var sessionparam = req.params.session;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewallhostel($1)', [sessionparam]).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view:hostel','ITR','INFO','view of hostel table successfull','hostel',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Hostels Detail Retrived  Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-Addition:hostel','ITR','ERR','Addition of hostel Table unsuccessfull','hostel',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Hostel Details"
                    })
                })
                });
            })
    });
    //GET By Id For hostel
    servicesRouter.get('/Hostel/Details/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select * from fn_viewhosteldetails($1)', i).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:hostel','ITR','INFO','view of hostel table successfull','hostel',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Hostel Details Retrived  Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view:hostel','ITR','ERR','view of hostel Table unsuccessfull','hostel',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Hostel Details."
                    })
            })
            })
    })
    //update hostel
    servicesRouter.post('/Hostel/Update/:Id', (req, res, next) => {
        hstlid = req.params.Id;
        usid = req.body.userId;
        brni = req.body.branchId;
        typ = req.body.type;
        fac = req.body.facilities;
        shng = req.body.sharing;
        bname = req.body.blockName;
        rnum = req.body.roomNum;
        adate = req.body.allocationDate;
        rid = req.body.representativeId;
        rpd = req.body.rentalPeriod;
        db.any('select * from fn_updatehostel($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)', [hstlid, usid, brni, typ, fac, shng, bname, rnum, adate, rid, rpd]).then(() => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:hostel','ITR','INFO','Update of hostel table successfull','hostel',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Hostel Details Updated Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:hostel','ITR','ERR','Update of hostel Table unsuccessfull','hostel',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Update Hostel Details."
                    })
                })
            })
    })
    /* *******************************Visiting*****************************/
    //Post For  visiting
    //Post For  visiting
    servicesRouter.post('/Visiting/Add/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        var brnid=''
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select fn_viewuserid from fn_viewuserid($1)',sessionparam).then((udata)=>{
            userId=udata[0].fn_viewuserid
            db.any('select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid($1))',sessionparam).then((bdata)=>{
                brnid=bdata[0].branchid
        db.any('select * from fn_addvisiting($1,$2)', [brnid,userId]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addtion: visiting','ITR','INFO','Addtion of visiting Table successfull','visiting',true,$1)",userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Visiting Details Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
})
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)',sessionparam).then((udata)=>{
                    userId=udata[0].fn_viewuserid
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-Addtion: visiting','ITR','ERR','Addtion of visiting Table unsuccessfull','visiting',False,$1)",userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Visiting "
                    })
                })
            });
    })
})



//get visiting details by userid


servicesRouter.get('/Visiting/Details/ByuserId/:Id', (req, res, next) => {
    console.log("outdeatils")
    var sessionparam = req.params.Id;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select v.id,v.outdatetime,v.indatetime,u.firstname,u.lastname from visiting v join users u on u.id=v.userid where v.userid=(select fn_viewuserid from fn_viewuserid($1)) order by id desc', sessionparam).then(function (data) {
        console.log(data)
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
             userId = session[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: visiting','ITR','INFO','View of visiting Table successfull','visiting',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Hostel Visiting Retrived  Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            console.log(error)
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view: visiting','ITR','ERR','view of visiting Table unsuccessfull','visiting',False,$1)", userId).then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Hostel Visiting"
                })
            })
        });
    })
})
servicesRouter.get('/Hostel/Visiting/:Id', (req, res, next) => {
    var sessionparam = req.params.Id;
    var userId='';
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
    db.any('select userid from visiting where indatetime is null', sessionparam).then(function (data) {
        db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
             userId = session[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: visiting','ITR','INFO','View of visiting Table successfull','visiting',true,$1)", userId).then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Hostel Visiting Retrived  Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
    })
        .catch(error => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-view: visiting','ITR','ERR','view of visiting Table unsuccessfull','visiting',False,$1)", userId).then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Unable to View Hostel Visiting"
                })
            })
        });
    })

    })
    //GET By Id For hostelvisiting
    servicesRouter.get('/HostelVisiting/Details/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select * from fn_viewhostelvisitdetails($1)', i).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: hostelvisiting','ITR','INFO','View of hostel Table successfull','hostelvisiting',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Hostel Visiting Retrived  Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: hostelvisiting','ITR','ERR','View of gatepass Table unsuccessfull','hostelvisiting',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Hostel Visiting Details."
                    })
                });
            })
    })
    servicesRouter.post('/Visiting/Update/:Id', (req, res, next) => {
        vid = req.params.Id
        visitingin = req.body.visitingintime;
        visitingout = req.body.visitingouttime;
        outingin = req.body.outingintime;
        outingtout = req.body.outingtoutime;
        hstid = req.body.hostelid;
        db.any('select * from fn_updatehostelvisit($1,$2,$3,$4,$5,$6)', [vid, visitingin, visitingout, outingin, outingtout, hstid]).then(() => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: hostelvisiting','ITR','INFO','Update of hostelvisiting Table successfull','visiting',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Visiting Details Updated  Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: hostelvisiting','ITR','ERR','Update of gatepass Table unsuccessfull','hostelvisiting',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Update Visiting Details."
                    })
                });
            })
    })
    //Post For  hostelfee
    servicesRouter.post('/HostelFee/Add', (req, res, next) => {
        ta = req.body.totalAmount;
        pdate = req.body.payDate;
        pmode = req.body.payMode;
        pa = req.body.paidAmount;
        pd = req.body.dueAmount;
        hstl = req.body.hostelId;
        db.any('select * from fn_addhostelfee($1,$2,$3,$4,$5,$6)', [ta, pdate, pmode, pa, pd, hstl]).then(() => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Adding: hostelfee','ITR','INFO','Adding of hostelfee Table successfull','hostelfee',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Hostel Fee Details Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Adding: hostelfee','ITR','ERR','Adding of hostelfee Table unsuccessfull','hostelfee',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Hostel Fee Details "
                    })
                });
            })
    })
    servicesRouter.get('/Hostel/HostelFee/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_hostelfeesview($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view: hostelfee','ITR','INFO','view of hostelfee Table successfull','hostelfee',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Hostel Fee Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-view: hostelfee','ITR','ERR','view of hostelfee Table unsuccessfull','hostelfee',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Hostel Fee"
                    })
                });
            })
        })
    })
    //GET By Id For hostelfee
    servicesRouter.get('/HostelFee/Details/:Id', (req, res, next) => {
        var i = req.params.Id;
        db.any('select * from fn_viewhostelfeedetails($1)', i).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view: hostelfee','ITR','INFO','view of hostelfee Table successfull','hostelfee',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Hostel Fee Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view: hostelfee','ITR','ERR','view of hostelfee Table unsuccessfull','hostelfee',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Hostel Fee Details."
                    })
                });
            });
    })
    servicesRouter.post('/Hostelfee/Update/:Id', (req, res, next) => {
        hfid = req.params.Id;
        ta = req.body.totalAmount;
        pdate = req.body.payDate;
        pmode = req.body.paymode;
        pa = req.body.paidAmount;
        pd = req.body.dueAmount;
        hstl = req.body.hostelId;
        db.any('select * from fn_updatehostelfee($1,$2,$3,$4,$5,$6,$7)', [hfid, ta, pdate, pmode, pa, pd, hstl]).then(() => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update: hostelfee','ITR','INFO','Update of hostelfee Table successfull','hostelfee',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Hostel Fee Updated Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view: hostelfee','ITR','ERR','view of hostelfee Table unsuccessfull','hostelfee',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to update Hostel Fee Details."
                    })
                });
            });
    }); 
        servicesRouter.post('/Branch/HostelApply/:sessionparam', (req, res, next) => {
            var uid = req.params.sessionparam;
            var userId='';
            for (var i = 0; i < uid.length; i++) {
                uid = uid.replace('-', '/');
            }
            db.any('select fn_viewuserid from fn_viewuserid($1)', uid).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid
                db.any('select * from  fn_addhostelreq($1)', [userId]).then((data) => {
                    db.any("insert into  domainlogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values  ('Table-Addtion:Hostel','ITR','INFO','Addtion of Hostel Table   successfull','Hostel',true,$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "Hostel  Has Been Added Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
                .catch(error => {
                    db.any('select fn_viewuserid from fn_viewuserid($1)', uid).then((dbsession) => {
                         userId = dbsession[0].fn_viewuserid
                    db.any("insert into  DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values  ('Table-Addtion:Hostel','ITR','ERR','Addtion of Hostel Table  unsuccessfull','Hostel',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to Add Hostel"
                        })
                    })
                })
                })
        })
        servicesRouter.get('/Branch/HostelAppliedDetails/:userSession', (req, res, next) => {
            var sessionparam = req.params.userSession;
            var userId='';
            for (var i = 0; i < sessionparam.length; i++) {
                sessionparam = sessionparam.replace('-', '/');
            }
            db.any('select * from fn_gethostelapplylist($1)', sessionparam).then(function (data) {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: To Branch hostelrequest','ITR','INFO','Retrive  Details of To Branch hostelrequest Table successfull',' To Branch hostelrequest',true,$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: " Hostel approvals Retrived  Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
                .catch(error => {
                    db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                         userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: To Branch hostelrequest','ITR','ERR','Retrive  Details of To Branch hostelrequest Table unsuccessfull',' To Branch hostelrequest',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable To View Hostel approvals "
                        })
                    })
                })
                });
        })
        servicesRouter.post('/Branch/Hostel/Approve/:studentid', (req, res, next) => {
            var i = req.params.studentid;
            db.any('select * from fn_updatetraineeHostelApprove($1)', i).then(function (data) {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-view: Hostel Approve','ITR','INFO','Retrive of Hostel Approve Table successfull',' Hostel Approve',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: " Hostel Approvals Updated  Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
                .catch(error => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Hostel Approve','ITR','ERR','Retrive of Hostel Approve Table unsuccessfull',' Hostel Approve',False)").then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to Update Hostel Approvals"
                        })
                    })
                })
        })
        servicesRouter.get('/Branch/Hostel/Applicants/:userSession', (req, res, next) => {
            var sessionparam = req.params.userSession;
            var userId='';
            for (var i = 0; i < sessionparam.length; i++) {
                sessionparam = sessionparam.replace('-', '/');
            }
            db.any('select * from fn_getapprovehostelapplicants($1)', sessionparam).then(function (data) {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: To Branch apprve','ITR','INFO','Retrive  Details of To Branch apprve ',' To Branch apprve',true,$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: " Hostel approvals  Retrived Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
                .catch(error => {
                    db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                         userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: To Branch apprve','ITR','ERR','Retrive  Details of To Branch apprve ',' To Branch apprve',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable To View Hostel approvals"
                        })
                    })
                })
                });
        })
    }
//=======================================================Library============================================
{
    //Adding Books in Library
    servicesRouter.post('/Library/Add/', (req, res, next) => {
        branchids = req.body.branchId;
        departmentids = req.body.departmentId;
        booknames = req.body.bookName;
        authornames = req.body.authorName;
        descriptions = req.body.description;
        segregations = req.body.segregation;
        pnlty = req.body.penalty;
        sta = 'InActive';
        db.any('select * from fn_addlibrary($1,$2,$3,$4,$5,$6,$7,$8)', [branchids, departmentids, booknames, authornames, descriptions, segregations, sta, pnlty]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: library','ITR','INFO','Addtion of library Table successfull',' library',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Books Has Been Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: library','ITR','ERR','Addtion of library Table unsuccessfull',' library',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Books in Library"
                    })
                })
            });
    })
    servicesRouter.get('/LibraryTransaction/Details/:Id', (req, res) => {
        var id = req.params.Id;
        db.any('select * from  fn_viewlibrarytransactionsById($1)', id).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Events','ITR','INFO','View of Events Table successfull','Events',true)").then((d) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Library Transaction Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Events','ITR','ERR','View of Events Table unsuccessfull','Events',False)").then((d) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        "message": "Unable to View Library Transaction "
                    })
                })
            })
    })
    //Get All Books In Library
    servicesRouter.get('/Library/All/:Id', (req, res) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from  fn_viewlibrarydetails($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Events','ITR','INFO','View of Events Table successfull','Events',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Library Details Retrived Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                     userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Events','ITR','ERR','View of Events Table unsuccessfull','Events',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Library Details"
                    })
                })
            })
            })
    })
    //Get All Books with Max of count
    servicesRouter.get('/Library/:Id', (req, res) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from  fn_viewlibrary($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Events','ITR','INFO','View of Events Table successfull','Events',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Library Details  Retrived uccessfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                     userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Events','ITR','ERR','View of Events Table unsuccessfull','Events',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Library Details"
                    })
                })
            })
            })
    })
    //Issuing Books to Trainee
    servicesRouter.post('/Traineelibrary/Add/', (req, res, next) => {
        var sta = 'Active'
        var allocatedid = ''
        var bookid = ''
        userids = req.body.userId;
        bookids = req.body.bookId;
        returndates = req.body.returnDate;
        db.any('select * from fn_Addtraineelibrary($1,$2,$3,$4)', [userids, bookids, returndates, sta]).then((data) => {
            this.allocatedid = data[0].fn_Addtraineelibrary
            db.any('select * from fn_addlibrarytransactions($1,$2,$3,$4)', [userids, bookids, returndates, sta]).then((data) => {
                db.any('select l.id from library l join traineelibrary sl   on l.id=sl.bookid where sl.id=$1', [this.allocatedid]).then((data) => {
                    this.bookid = data[0].id;
                    db.any('update  library set status=$2 where id=$1', [this.bookid, sta]).then((data) => {
                        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: Trainee library','ITR','INFO','Addtion of Fee Payments Table successfull',' Trainee library',true)").then((log) => {
                            if (data.length > 0) {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: data,
                                    message: " Trainee Library added Successfully"
                                })
                            } else {
                                res.status(200).send({
                                    result: true,
                                    error: "NOERROR",
                                    data: "NDF",
                                    message: "No Data Found"
                                })
                            }
                        })
                    })
                })
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtion: Trainee library','ITR','ERR','Addtion of Fee Payments Table unsuccessfull',' Trainee library',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Trainee Library"
                    })
                })
            });
    })
    //Get All Books in Trainee library
    servicesRouter.get('/TraineeLibrary/All/:Id', (req, res) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from  fn_viewtraineelibrary($1)', sessionparam).then((data) => {
            console.log(data)
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Events','ITR','INFO','View of Events Table successfull','Events',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Trainee Library  Retrived  Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                     userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Events','ITR','ERR','View of Events Table unsuccessfull','Events',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Trainee Library"
                    })
                })
            })
    })
    //Get All books by its id
    servicesRouter.get('/TraineeLibrary/details/:Id', (req, res) => {
        var id = req.params.Id;
        db.any('select * from  fn_viewtraineelibraryById($1)', id).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Events','ITR','INFO','View of Events Table successfull','Events',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Trainee Library Retrived Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Events','ITR','ERR','View of Events Table unsuccessfull','Events',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Trainee Library Details"
                    })
                })
            })
    })
//Update for Trainee library when they returning the book
    servicesRouter.post('/User/Library/Trainee/Update/:Id', (req, res, next) => {
        id = req.params.Id;
        bid = req.body.bookId;
        returndates = req.body.returnDate;
        uid = req.body.userId;
        sta = "InActive"
        db.any('select * from fn_updatelibrarytransactions($1,$2,$3,$4,$5)', [id, bid, uid, returndates, sta]).then((ltdata) => {
            db.any('delete from traineelibrary where bookid=$1', bid).then((tldata) => {
                db.any('update library set status=$1 where id=$2', [sta, bid]).then((data) => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Leave  ','ITR','INFO','Update of  Leave  Table  successfull','Leave ',true)").then((log) => {
                        if (ltdata.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "Trainee Library Updated Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                        .catch(error => {
                            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Leave  ','ITR','ERR','Update of  Leave  Table unsuccessfull','Leave ',False)").then((log) => {
                                res.status(200).send({
                                    result: false,
                                    error: error.message,
                                    data: "ERROR",
                                    message: "Cannot Update Trainee Library"
                                })
                            })
                        })
                })
            })
        })
    })
    //For accepting or rejecting request
    servicesRouter.post('/User/Library/Request/Update/:Id', (req, res, next) => {
        id = req.params.Id;
        bid = req.body.bookId;
        returndates = req.body.returnDate;
        uid = req.body.userId;
        st = req.body.status;
        sta = 'Active'
        var sts = ''
        if (st == 'Accepted') {
            sts = 'Active'
            db.any('select * from fn_Addtraineelibrary($1,$2,$3,$4)', [uid, bid, returndates, sta]).then((data) => { })
        } else {
            sts = 'InActive'
        }
        db.any('select * from fn_updatebookrequestdetails($1,$2,$3,$4)', [id, bid, uid, st]).then((data) => {
            db.any('update library set status=$1 where id=$2', [sts, bid]).then((data) => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Leave  ','ITR','INFO','Update of  Leave  Table  successfull','Leave ',true)").then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Library Request Updated Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
                .catch(error => {
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Leave  ','ITR','ERR','Update of  Leave  Table unsuccessfull','Leave ',False)").then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Cannot Update Library Request"
                        })
                    })
                })
        })
    })
    //Library details by bookname
    servicesRouter.get('/Library/Details/:bookName', (req, res, next) => {
        var bnm = req.params.bookName;
        db.any('select * from fn_viewlibrarydetailsByName($1)', [bnm]).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Organization Branch','ITR','INFO','Retrive of Organization Branch Table successfull',' Organization Branch',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Library Book Details Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View: Organization Branch','ITR','ERR','Retrive of Organization Branch Table unsuccessfull',' Organization Branch',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                            error: error.message,
                            data: "ERROR",
                        message: "Unable to View Library by bookname"
                    })
                })
            });
    })
    //To get max(id) of that book and adding into library request
    servicesRouter.get('/Library/MaxID/:bookName/:Id', (req, res, next) => {
        var bnm = req.params.bookName;
        var sta = 'InActive';
        var bookid = '';
        var stats = 'Pending';
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select max(id) from library where bookname=$1 and status=$2 and branchid=(select branchid from userprofileview where userid=(select fn_viewuserid from fn_viewuserid($3))) group by(bookname)', [bnm, sta, sessionparam]).then(function (data) {
            this.bookid = data[0].max
            db.any('select * from fn_addlibraryrequests($1,$2,$3)', [this.bookid, sessionparam, stats]).then(function (data) {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                     userId = session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Organization Branch','ITR','INFO','Retrive of Organization Branch Table successfull',' Organization Branch',true,$1)", userId).then((log) => {
                        if (data.length > 0) {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: data,
                                message: "Library Request Retrived  Successfully"
                            })
                        } else {
                            res.status(200).send({
                                result: true,
                                error: "NOERROR",
                                data: "NDF",
                                message: "No Data Found"
                            })
                        }
                    })
                })
            })
                .catch(error => {
                    db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                         userId = session[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Organization Branch','ITR','ERR','Retrive of Organization Branch Table unsuccessfull',' Organization Branch',False,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to Add Library Request"
                        })
                    })
                })
                });
        })
    })
    //Get All Libraryrequests
    servicesRouter.get('/Library/BookRequests/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewlibraryRequests($1)', sessionparam).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Organization Branch','ITR','INFO','Retrive of Organization Branch Table successfull',' Organization Branch',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Library Requests Retrived  Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                     userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View: Organization Branch','ITR','ERR','Retrive of Organization Branch Table unsuccessfull',' Organization Branch',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Library Requests"
                    })
                })
            });
        })
    })
    //get all requests by requestid
    servicesRouter.get('/Library/bookRequests/Details/:Id/:param', (req, res, next) => {
        var rid = req.params.Id
        var sessionparam = req.params.param;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewlibraryRequestsById($1,$2)', [sessionparam, rid]).then(function (data) {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Organization Branch','ITR','INFO','Retrive of Organization Branch Table successfull',' Organization Branch',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: " Library requests by Id Retrived  Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                     userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View: Organization Branch','ITR','ERR','Retrive of Organization Branch Table unsuccessfull',' Organization Branch',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Library requests by Id"
                    })
                })
            })
            });
    })
    //count for requests
    servicesRouter.get('/Library/Request/Count/:Id', (req, res, next) => {
        var sessionparam = req.params.Id;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        var stus = 'InActive';
        db.any('select * from fn_requestcount($1)', sessionparam).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                 userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,userId) values ('Table-View:Notifications','ITR','INFO','View of Notifications Table  successfull','Notifications',true,$1)", userId).then((log) => {
                    /*    res.status(200).send(data) */
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Library Request Count Retrived  Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((session) => {
                     userId = session[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Notifications','ITR','ERR','View of Notifications Table  unsuccessfull','Notifications',False,$1)", userId).then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Library Request Count"
                    })
                })
                })
            })
    })
    servicesRouter.get('/Library/All/Details/:bookName', (req, res, next) => {
        var bid = req.params.bookName;
        db.any('select * from fn_viewalllibrarydetails($1)', bid).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Notifications','ITR','INFO','View of Notifications Table  successfull','Notifications',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Library Details Retrived  Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Notifications','ITR','ERR','View of Notifications Table  unsuccessfull','Notifications',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View  Library Details"
                    })
                })
            })
            })
    })
    servicesRouter.get('/Library/All/IssuedDetails/:bookName', (req, res, next) => {
        var bid = req.params.bookName;
        sta = 'Active'
        db.any('select slv.firstname,slv.lastname from traineelibraryview slv where slv.issuedstatus=$1 and slv.bookname=$2', [sta, bid]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Notifications','ITR','INFO','View of Notifications Table  successfull','Notifications',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Library Issued Details Retrived  Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Notifications','ITR','ERR','View of Notifications Table  unsuccessfull','Notifications',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View  Library Details"
                    })
                })
            })
    })
    //count for days while returning
    servicesRouter.get('/TraineeLibrary/DaysCount/:rdate/:id', (req, res) => {
        var rdt = req.params.rdate;
        var id = req.params.id;
        db.any('select * from  fn_countDaysForLibrary($1,$2)', [rdt, id]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Events','ITR','INFO','View of Events Table successfull','Events',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Trainee Library Days Count Retrived  Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Events','ITR','ERR','View of Events Table unsuccessfull','Events',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Trainee Library count "
                    })
                })
            })
})
}
//==============================================Sports=====================================
{
    servicesRouter.get('/Sports/getallocatedsports/:userSession', (req, res, next) => {
        var sessionparam = req.params.userSession;
        var userId='';
        for (var i = 0; i < sessionparam.length; i++) {
            sessionparam = sessionparam.replace('-', '/');
        }
        db.any('select * from fn_viewsports($1)', [sessionparam]).then((data) => {
            db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                 userId = dbsession[0].fn_viewuserid;
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Sports','ITR','INFO','Viewing of Departments Table successfull','Sports',true,$1)", userId).then((log) => {
                    if (data.length > 0) {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: data,
                            message: "Sports Alloacted Successfully"
                        })
                    } else {
                        res.status(200).send({
                            result: true,
                            error: "NOERROR",
                            data: "NDF",
                            message: "No Data Found"
                        })
                    }
                })
            })
        })
            .catch(error => {
                db.any('select fn_viewuserid from fn_viewuserid($1)', sessionparam).then((dbsession) => {
                     userId = dbsession[0].fn_viewuserid;
                    db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status,UserId) values ('Table-View:Sports','ITR','ERR','Viewing of Departments Table unsuccessfull','Sports',false,$1)", userId).then((log) => {
                        res.status(200).send({
                            result: false,
                            error: error.message,
                            data: "ERROR",
                            message: "Unable to View Sports Details"
                        })
                    })
                })
            })
    })
    servicesRouter.get('/Sports/GetAllSports/', (req, res, next) => {
        db.any('select * from fn_viewallsports()').then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Sports','ITR','INFO','Viewing of Departments Table successfull','Sports',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Sports Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Sports','ITR','ERR','Viewing of Departments Table unsuccessfull','Sports',false)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Sports"
                    })
                })
            })
    })
    servicesRouter.get('/Sports/Edit/:id', (req, res, next) => {
        id = req.params.id;
        db.any('select * from fn_viewsportdetailsbyid($1)', id).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Sports','ITR','INFO','Viewing of Departments Table successfull','Sports',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Sports Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Sports','ITR','ERR','Viewing of Departments Table unsuccessfull','Sports',false)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Sports Details"
                    })
                })
            })
    })
    servicesRouter.get('/Kits/All/', (req, res, next) => {
        db.any('select * from fn_viewsportskit()').then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Sports','ITR','INFO','Viewing of Departments Table successfull','Sports',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Sport Kits Retrieved Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Sports','ITR','ERR','Viewing of Departments Table unsuccessfull','Sports',false)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to View Sport Kits Details"
                    })
                })
            })
    })
    servicesRouter.post('/Sports/Add', (req, res, next) => {
        sid = req.body.sportsId;
        did = req.body.departmentId;
        cid = req.body.sessionId;
        fd = req.body.fromDate;
        st = req.body.startTime;
        et = req.body.endTime;
        ed = req.body.endDate;
        kid = req.body.kitId;
        db.any('select fn_addsports($1,$2,$3,$4,$5,$6,$7,$8)', [sid, did, cid, fd, st, et, ed, kid]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtions: Sport Management','ITR','INFO','Retrive of Sport Management Table successfull',' Sport Management',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Sports Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtions: Sport Management','ITR','ERR','Retrive of Sport Management Table unsuccessfull',' Sport Management',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Sports"
                    })
                })
            });
    })
    
   servicesRouter.post('/Sports/Update/:Id', (req, res, next) => {
    id = req.params.Id;
    sid = req.body.sportsId;
    did = req.body.departmentId;
    cid = req.body.sessionId;
    fd = req.body.fromDate;
    st = req.body.startTime;
    et = req.body.endTime;
    ed = req.body.endDate;
    kid = req.body.kitId;
    db.any('select * from fn_updatesportsmanagement($1,$2,$3,$4,$5,$6,$7,$8,$9)', [id, sid, did, cid, fd, st, et, ed, kid]).then((data) => {
        db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Sport Management ','ITR','INFO','Update of Sports management Table  successfull','Sports management',true)").then((log) => {
            if (data.length > 0) {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: "Sports Updated Successfully"
                })
            } else {
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: "Sports Updated Successfully"
                })
            }
        })
    })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Update:Sports ','ITR','ERR','Update of Sports management Table unsuccessfull','Sports',true)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: "Sport Not Update"
                })
            })
        });
})
    servicesRouter.post(' ', (req, res, next) => {
        sid = req.body.sportsId;
        kid = req.body.kitId;
        brid = req.body.branchId;
        db.any('select * from fn_addsportkits($1,$2,$3)', [sid, kid, brid]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtions: Sports Kit','ITR','INFO','Add of Sports Kit Table successfull','Sports Kit ',true)").then((log) => {
                if (data.length > 0) {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: data,
                        message: "Sports Kits Added Successfully"
                    })
                } else {
                    res.status(200).send({
                        result: true,
                        error: "NOERROR",
                        data: "NDF",
                        message: "No Data Found"
                    })
                }
            })
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtions: Sport Kit','ITR','ERR','Add of Sports Kit Table unsuccessfull','Sports Kit',False)").then((log) => {
                    res.status(200).send({
                        result: false,
                        error: error.message,
                        data: "ERROR",
                        message: "Unable to Add Sports kit"
                    })
                })
            });
    })
    servicesRouter.post('/Sports/Kits/Add', (req, res, next) => {
        sid = req.body.sportsId;
        kid = req.body.kitId;
        brid=req.body.branchId;
        db.any('select * from fn_addsportskit($1,$2,$3)', [sid,kid,brid]).then((data) => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtions: Sports Kit','ITR','INFO','Add of Sports Kit Table successfull','Sports Kit ',true)").then((log) => {
                if(data.length>0)
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: data,
                    message: 'Sports Kit Added'
                })
                else
                res.status(200).send({
                    result: true,
                    error: "NOERROR",
                    data: "NDF",
                    message: 'Sports Kit Added'
            })
            })
        })
        .catch(error => {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addtions: Sport Kit','ITR','ERR','Add of Sports Kit Table unsuccessfull','Sports Kit',False)").then((log) => {
                res.status(200).send({
                    result: false,
                    error: error.message,
                    data: "ERROR",
                    message: 'Unable to Add Sports Kit'
                })
            })
        });
    })
}
module.exports = servicesRouter;
