var fs = require('fs');
var walk = require('fs-walk');
var path = require('path')
var exports = module.exports = {};
var j = 0,
    fc = 0,
    k = 0;
var dir = 'app';
var fileSizeInBytes = 0;
var dire_path = __dirname;
const dbc = require('./db.js');
const db = dbc.db;
var config = require(__dirname + '/init_app');
exports.readComponents = function readViews(dirPath, cb) {
    if (!fs.existsSync(dirPath + 'InfoBytFE')) {
        console.log("For Server " + dirPath + "../InfoBytFE");
        var components = "insert into components (modules,components,icon) values ('app','app','fa fa-sitemap') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('app','aboutus','fa fa-sitemap') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('app','error-message','fa fa-sitemap') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('app','home','fa fa-sitemap') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('app','services','fa fa-sitemap') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('appraisal','appraisal-timetable','fa fa-file-text') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('appraisal','trainee-marks','fa fa-file-text') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('attendance','attendance','fa fa-clock-o') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('attendance','latelogins','fa fa-clock-o') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('attendance','leave-details','fa fa-clock-o') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('attendance','usermapping','fa fa-clock-o') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('attendance','holiday-master','fa fa-clock-o') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('attendance','survelince','fa fa-clock-o') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('batch','batch','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('batch','session-master','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('batch','staff','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('batch','time-table','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('attendance','attendance','fa fa-clock-o') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('batch','team','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('attendance','latelogins','fa fa-clock-o') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('batch','trainer-subject','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('batch','unicsol','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('batch','trainee-batch','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('batch','teamallocation','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('career','allocate-placement','fa fa-graduation-cap') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('career','allocate-project','fa fa-graduation-cap') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('attendance','leave-details','fa fa-clock-o') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('career','job','fa fa-graduation-cap') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('career','placements','fa fa-graduation-cap') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('career','project','fa fa-graduation-cap') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('career','recruitment','fa fa-graduation-cap') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('dashboard','bulkmessage','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('dashboard','dashboard','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('dashboard','gallery-master','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('dashboard','idcard','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('dashboard','mailbox','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('dashboard','notifications','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('attendance','holiday-master','fa fa-clock-o') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('dashboard','organization-feedback','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('dashboard','shopping','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('dashboard','uploads','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('dashboard','feedback','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('fee','concession','fa fa-money') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('fee','fee-payments','fa fa-money') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('fee','fees','fa fa-money') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('fee','feestructure','fa fa-money') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('attendance','survelince','fa fa-clock-o') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('batch','session-subject','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('login','default','fa fa-sign-in') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('login','forgot-password','fa fa-sign-in') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('login','login','fa fa-sign-in') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('login','signup','fa fa-sign-in') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('batch','subject','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('login','testimonial','fa fa-sign-in') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('login','chat','fa fa-sign-in') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('attendance','usermapping','fa fa-clock-o') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('organization','achievements','fa fa-sitemap') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('organization','admision-details','fa fa-sitemap') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('organization','branch','fa fa-sitemap') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('organization','branch-session','fa fa-sitemap') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('organization','courses','fa fa-sitemap') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('organization','events','fa fa-sitemap') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('organization','organization','fa fa-sitemap') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('organization','allocate-shift','fa fa-sitemap') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('organization','shifts','fa fa-sitemap') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('organization','transfer','fa fa-sitemap') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('organization','department','fa fa-sitemap') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('salary','payroll','fa fa-credit-card') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('salary','payslip','fa fa-credit-card') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('salary','reports','fa fa-credit-card') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('salary','salary','fa fa-credit-card') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('salary','salary-hike','fa fa-credit-card') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('salary','salary-pattern','fa fa-credit-card') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('salary','salary-payment','fa fa-credit-card') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('salary','tds','fa fa-credit-card') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('services','gatepass','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('services','hostel','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('services','hostelfee','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('services','library','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('services','sports','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('services','visiting','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('settings','billing','fa fa-ellipsis-v fa-2x') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('settings','changepassword','fa fa-ellipsis-v fa-2x') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('settings','country','fa fa-ellipsis-v fa-2x') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('settings','institute','fa fa-ellipsis-v fa-2x') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('settings','permission','fa fa-ellipsis-v fa-2x') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('settings','role-master','fa fa-ellipsis-v fa-2x') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('transport','bus-driver-alocation','fa fa-bus') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('transport','bus-master','fa fa-bus') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('transport','transport-fee-master','fa fa-bus') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('transport','transport-route-master','fa fa-bus') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('transport','transport-stops','fa fa-bus') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('transport','user-transport','fa fa-bus') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('transport','user-transport-payment','fa fa-bus') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('user','contact-details','fa fa-group') ON CONFLICT(components) DO NOTHING;" +
            "insert into components (modules,components,icon) values ('user','users','fa fa-group') ON CONFLICT(components) DO NOTHING;";
        db.any(components).then(function (data) {
            cb();
        });
    } else {
        walk.walk(dirPath, function (basedir, filename, stat, next) {
            var perm = stat.isDirectory() ? 0755 : 0644;
            if (!stat.isDirectory() && isSkipDir(basedir) && !isSkipFile(filename)) {
                fc++;
                fileSizeInBytes += stat.size;
                dbc.log(fc + ' => ' + filename + ' : ' + stat.size);
                if (filename.includes('.component.ts')) {
                    var f = filename.substring(filename.lastIndexOf('/') + 1, filename.indexOf('component') - 1);
                    var comp = f + '';
                    dir = basedir.substring(21, basedir.lastIndexOf(f) - 1);
                    if (dir.includes('\\')) {
                        dir = 'app';
                    }
                    var x = JSON.parse(JSON.stringify(config.faicon)).find(function (item, i) {
                        if (item.module === dir) {
                            console.log(item.icon)
                            db.any("insert into components (modules,components,icon) values ('" + dir + "','" + f + "','" + item.icon + "') ON CONFLICT(components) DO NOTHING").then(function (data) {});
                            j++;
                        }
                    })
                }
                if (filename.includes('.module.ts') && !filename.includes('routing.module.ts')) {
                    k++;
                }
            }
            fs.chmod(path.join(basedir, filename), perm, next);
        }, function () {
            cb();
        });
    }
}

function isSkipFile(skipFile) {
    if (skipFile.includes('service_init.json')) return false;
    if (skipFile.includes('main')) return true;
    if (skipFile.includes('polyfills')) return true;
    if (skipFile.includes('runtime')) return true;
    if (skipFile.includes('test.ts')) return true;
    if (skipFile.includes('environment')) return true;
    if (skipFile.includes('.json')) return true;
    if (skipFile.includes('.conf.js')) return true;
    if (skipFile.includes('.js')) return false;
    if (skipFile.includes('.ts')) return false;
    if (skipFile.includes('.css')) return false;
    if (skipFile.includes('.html')) return false;
    if (skipFile.includes('.sql')) return false;
    else return true;
}

function isSkipDir(skipDir) {
    if (skipDir.includes('node_modules')) return false;
    if (skipDir.includes('public')) return false;
    if (skipDir.includes('iisnode')) return false;
    if (skipDir.includes('e2e')) return false;
    if (skipDir.includes('assets')) return false;
    if (skipDir.includes('dist')) return false;
    else return true;
}

function cleanDir(dirPath) {
    try {
        var files = fs.readdirSync(dirPath);
    } catch (e) {
        return;
    }
    if (files.length > 0)
        for (var i = 0; i < files.length; i++) {
            var filePath = dirPath + '/' + files[i];
            if (fs.statSync(filePath).isFile() && filePath.indexOf('.css') > -1) {
                //dbc.log('Deleting => ' + filePath);
                fs.unlinkSync(filePath);
            } else
                cleanDir(filePath);
        }
};

function updateBuild(dire_path) {
    if (fs.existsSync(dire_path)) {
        fs.readdirSync(dire_path).forEach(function (file, index) {
            var curPath = dire_path + "/" + file;
            if (fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(dire_path);
    }
};
exports.flushApp = function checkBuildExpiery() {
    var currenttime = new Date();
    var days = 3;
    fs.stat(dire_path, function (err, stat) {
        var modifiedtime = new Date(stat.mtime + days * 24 * 60 * 60 * 1000);
        if (modifiedtime > currenttime) {
            updateBuild(dire_path);
        }
    });
}
cleanDir('../InfoBytFE/src/app');
//readViews('../');