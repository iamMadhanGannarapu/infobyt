var express = require('express');
var permissionRouter = express.Router();
var walk = require('fs-walk');
var path = require('path')
var config = require('./init_app');
var fs = require('fs');
permissionRouter.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', config.angular);
    res.setHeader('Access-Control-Allow-Methods', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});
const dba=require('./db.js');
const db = dba.db;
permissionRouter.get('/view/:Id', (req, res, next) => {
    var module='';
    var firstList = [];
    var z = 0;
    var childData = [];
    var sessionparam = req.params.Id;
    for (var i = 0; i < sessionparam.length; i++) {
        sessionparam = sessionparam.replace('-', '/');
    }
db.any('select * from fn_viewpermissions($1)', sessionparam).then((data) => {
firstList= data
for (var i = 0; i < firstList.length; i++)
        {
        if(module == firstList[i].modules) {
            childData[z - 1].components.push({
                "component": firstList[i].components,
                "componentid": firstList[i].componentid,
                "read": firstList[i].read,
                "write": firstList[i].write,
                "rolename": firstList[i].rolename, 
                "branchid": firstList[i].branchid, 
            })
        }
        else
        {
        module= firstList[i].modules
        childData.push({
            "module": module,
            "components": [{
                "component": firstList[i].components,
                "componentid": firstList[i].componentid,
                "read": firstList[i].read,
                "write": firstList[i].write,
                "rolename": firstList[i].rolename, 
                "branchid": firstList[i].branchid, 
            }]
        })
        z = z + 1;
    }
    }
    console.log(childData)
     res.send(childData);
})
})
module.exports = permissionRouter