
var nodemailer = require('nodemailer');
var fs = require('fs');
var config = require(__dirname + '/init_app');
const dbc=require('./db.js');
const db = dbc.db;
const pgp = db.$config.pgp;
var eid = Date.now();
const path = require('path');
var pdf = require('html-pdf');
var format = require('format-number')
var dateFormat = require('dateformat');
const ABSPATH = path.dirname(process.mainModule.filename);
var dir = 'public/' + 'hikeLetter';
var dir2 = 'public/' + 'LOI';
var dir3 = 'public/' + 'offerLetter';
var dir4 = 'public/' + 'CallLetter';
// const pgp = db.$config.pgp;
var options = {
  format: 'Letter'
};
var toMail;
var userData;
var HikeData;
var file;
var userData;
var RecData;
var mailOptions;
// from
var transporter = nodemailer.createTransport({
  service: config.mailServer,
  auth: {
    user: config.email,
    pass: config.password
  }
});
// to
function sendMail(mailOptions1) {
  transporter.sendMail(mailOptions1, function (error, info) {
    if (error) {
      dbc.log(error);
    } else {
      dbc.log('Email sent: ' + info.response);
      res.status(200).send([Id[0], {
        message: "success",
        result: "true"
      }]);
    }
  });
}
module.exports = {
  mailer: function (contactId, sub, rtype) {
    dbc.log("mailer ==> " + contactId, sub, rtype)
    var msg = "<html lang='en'><head><meta charset='UTF-8'><link rel='stylesheet' type='text/css' href='https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' />";
    var code = '';
    var password = '', mailPwd = '';
    for (i = 0; i < 3; i++) {
      code += Math.floor((Math.random() * 100) + 1);
    }
    password = code;
    db.none("update login set password=crypt($1,gen_salt('md5')) where userId=(select userId from contactDetails where Id=$2)", [code, contactId])
      .then((passwordUpdate) => {
        db.any('select userid from contactdetails where id=$1', contactId).then((user) => {
          db.any('select * from login where userid=$1', user[0].userid).then((login) => {
            db.any("select * from fn_addauthenticationmaster($1,$2)", [login[0].userid, login[0].loginid]).then((authentication) => {
              var random = authentication[0].fn_addauthenticationmaster + '';
              mailPwd = password;
              subString = random.substring(0, 3)
              password = subString + password + '';
              subString = random.substring(3, 7);
              password = password + subString;
              db.any("update login set password=crypt($1,gen_salt('md5')) where userid=$2 returning password", [password, login[0].userid]).then((update) => {
                db.any("select * from LoginView where contactId=$1", [contactId])
                  .then((data) => {
                    toMail = data[0].email;
                    msg += '<div style="text-align=center;margin-left: 300px">Hello  ' + data[0].firstname + ' ' + data[0].lastname + '</div><b><b>';
                    msg += '<div style="text-align=center;margin-left: 300px">Thank you for registering with infobyt.</div><b>';
                    msg += '<div style="text-align=center;margin-left: 300px"> Please use the login details to access your account and change your settings.</div><b>';
                    code = '';
                    if (rtype != 'forgot') {
                      code += '<div style="text-align=center;margin-left: 300px"><br/>Username : ' + data[0].loginid + ' </div>';
                    }
                    code += '<div style="text-align=center;margin-left: 300px">OTP :  ' + mailPwd + '</div>';
                    msg += '<div style="text-align=center;margin-left: 300px" ">If you received this email by mistake please ignore it.</p></div></b>';
                    msg += code + "</b>";
                    msg += '<div style="text-align=center;margin-left: 300px"><p>Sincerely,</p>';
                    msg += '<div style="text-align=center;margin-left: 300px"><p>Infobyt team</p><b>';
                    msg += '<div style="background-image: url(http://getwallpapers.com/wallpaper/full/b/8/b/1086000-beautiful-simple-backgrounds-1920x1200.jpg) ;text-align: center; height:50px" ><div class="row"><div class="col-md-3"><b class="strong">Download our App in :</b>&nbsp;<a href="#" class="fa fa-android"  ></a> &nbsp;<a href="#" class="fa fa-apple" ></a>&nbsp;</div><div class="col-md-6"><h1 style="color:rgb(34, 31, 82)"></h1></div><div class="col-md-3"><b class="strong">  Follow Us on :</b>&nbsp;<a href="#" class="fa fa-twitter"  ></a>&nbsp;<a href="#" class="fa fa-instagram" ></a>&nbsp;<a href="#" class="fa fa-whatsapp" ></a>&nbsp;<a href="#" class="fa fa-google-plus" ></a></div></div> </div>'
                    var mailOptions = {
                      from: config.email,
                      to: toMail,
                      subject: sub,
                      dsn: {
                        id: eid,
                        return: 'headers',
                        notify: ['failure', 'delay', 'success'],
                        recipient: config.email
                      },
                      html: '<div  style="background-image: url(http://getwallpapers.com/wallpaper/full/b/8/b/1086000-beautiful-simple-backgrounds-1920x1200.jpg) ;text-align: center; height:50px" ><h1 style="color:rgb(34, 31, 82)">Welcome to Infobyt</h1> </div>' + msg
                    };
                    sendMail(mailOptions)
                  });
              })
            })
          })
        })
      })
  },
  letterIntend: function letterOfIntend(userId, Id, callback) {
    console.log("letter Of Intend ==> " + userId, Id)
    file = "<html><head><link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' integrity='sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm' crossorigin='anonymous'></head><body>";
    db.any('select * from users where id=$1', userId).then((data) => {
      userData = data
      db.any('select * from applicantsaggrementsview where userid=$1', userId).then((data) => {
        RecData = data;
        toMail = RecData[0].email;
        file += "<div align='center'><h3>Letter of Intend</h3></div>" +
          "<div> Dear <b>" + userData[0].firstname + userData[0].lastname + "</b></div>" +
          "<div class='col-md-6 col-sm-6' ><p style=' width:80%; word-wrap: break-word;'>We are delighted to offer you the position of <b>" + RecData[0].type + "</b> with <b>" + RecData[0].name + "</b>.</p>" +
          "<p style=' width:80%; word-wrap: break-word;'>This offer shall stand terminated unless extended at the sole discretion of the Company. Please treat the " +
          "details of this offer with utmost confidentiality and please do not discuss or disclose them to any third " +
          "party under any condition at any time.</p>" +
          "<p>    Your annual compensation will be <b>" + RecData[0].package + " </b>per annum. Details are mentioned in " +
          "Annexure I to this letter. Compensation will be paid monthly and tax will be deducted at source, as " +
          "applicable.</p>" +
          "<p>    Please confirm your acceptance and date of joining by signing the duplicate in the appropriate places and " +
          "returning it to us at the earliest. We are excited and confident that you will make an outstanding " +
          "contribution at <b>" + RecData[0].name + "</b>.</p>" +
          "<p>    We also believe you will find the experience of joining us stimulating and rewarding, both professionally and personally. We look forward eagerly to welcoming you on board.</p>" +
          "<h4 align=center>Annexure I</h4>" +
          "<h6>1. Training Period</h6>" +
          "<p>    You will be required to undergo class room and on job training for two months, during which" +
          "period you will be appraised for satisfactory performance during/after which <b>" + RecData[0].name + "</b> would normally" +
          "confirm you." +
          "<p>    This confirmation will be communicated to you in writing. If your performance is found" +
          "unsatisfactory during the training period, the company may afford you opportunities to assist you and" +
          "enable you to improve your performance. If your performance is still found unsatisfactory,<b>" + RecData[0].name + " </b>may" +
          "terminate your traineeship forthwith.</p>" +
          "<p>    However,<b>" + RecData[0].name + "</b> may even otherwise at its sole discretion terminate the traineeship any" +
          "time if your performance is not found satisfactory.</p>" +
          "    <h6>2. Compensation Structure/Salary components</h6>" +
          "<p>    The compensation structure/salary components are subject to change as per <b>" + RecData[0].name +
          "</b>compensation policy from time to time at its sole discretion.</p>" +
          "<p>    Your annual salary package will be decided depending on your assessment scores.</p>" +
          "<h6>3. Service Agreement</h6>" +
          "<p>    You will be required to execute an agreement, to serve <b>" + RecData[0].name + "</b>for a minimum period of 2 years" +
          "after joining.</p></div>"
        fs.writeFile(dir2 + '/' + Id + '.html', file, function (err) {
          if (err) throw err;
          console.log('Html is created successfully.');
          var html = fs.readFileSync('./public/LOI/' + Id + '.html', 'utf8');
          pdf.create(html, options).toFile('./public/LOI/' + Id + '.pdf', function (err, res) {
            if (err) return console.log(err);
            else {
              console.log('pdf is created successfully.');
              const mailOptions = {
                from: config.email,
                to: toMail,
                subject: 'Letter Of Intend ',
                dsn: {
                  id: eid,
                  return: 'headers',
                  notify: ['failure', 'delay', 'success'],
                  recipient: config.email
                },
                text: 'please find your LOI ',
                attachments: [{
                  path: ABSPATH + '/public/LOI/' + Id + '.pdf'
                }]
              };
              sendMail(mailOptions)
            }
          });
        });
      })
    })
  },
  letterHike: function hikeletter(userId, Id, callback) {
    console.log("Hike letter ==>" + userId, Id)
    file = "<html><head><link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' integrity='sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm' crossorigin='anonymous'></head><body>";
    console.log(userId)
    db.any('select max(salary) from salaryview where userid=$1', userId).then((data) => {
      userData = data
      console.log(userData)
      db.any('select * from salaryhikeview where userId=$1', userId).then((data) => {
        HikeData = data;
        toMail = HikeData[0].email;
        file += "<div><p> Mr. / Ms./ Mrs." + HikeData[0].firstname + " " + HikeData[0].lastname + "</p>" +
          "    <p>" + HikeData[0].rolename + "</p>" +
          "<p> Dear" + HikeData[0].firstname + " " + HikeData[0].lastname + "</p>" +
          "<p>We take this opportunity to congratulate you and express our appreciation for your valuable " +
          "contribution in achieving company goals. Management is pleased to announce  that your gross salary has been revised to " + userData[0].max + ".</p>" +
          "<p>Revised Salary package will be effective from" + HikeData[0].hikedate + ".</p>" +
          "<p>The detailed salary structure is mentioned in annexure as a part of this letter.</p>" +
          " <p>All the other terms and conditions of your appointment remain unchanged.</p>" +
          "<p>We appreciate the efforts you put in and expect  that you would continue to do so in future with same zeal and confidence.</p>" +
          "<p>For and On behalf of </p>" +
          "<p>" + HikeData[0].name + "</p>"
        "</div>"
        fs.writeFile(dir + '/' + Id + '.html', file, function (err) {
          if (err) throw err;
          console.log('Html is created successfully.');
          var html = fs.readFileSync('./public/hikeLetter/' + Id + '.html', 'utf8');
          pdf.create(html, options).toFile('./public/hikeLetter/' + Id + '.pdf', function (err, res) {
            if (err) return console.log(err);
            else {
              console.log('pdf is created successfully.');
              const options = {
                from: config.email,
                to: toMail,
                subject: 'Letter Of Intend ',
                dsn: {
                  id: eid,
                  return: 'headers',
                  notify: ['failure', 'delay', 'success'],
                  recipient: config.email
                },
                text: 'Please find your Hike Letter ',
                attachments: [{
                  path: ABSPATH + '/public/hikeLetter/' + Id + '.pdf'
                }]
              };
              sendMail(options)
            }
          });
        });
      })
    })
  },
  
 letterOffer: function offerLetter( Id,userId, callback) {
  console.log("offer letter ==> " + userId, Id, callback)
  file = "<html><head><link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' integrity='sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm' crossorigin='anonymous'></head><body>";
  db.any('select * from users where id=$1', userId).then((data) => {
    userData = data
    db.any('select * from applicantsaggrementsview where userid=$1', userId).then((data) => {
      RecData = data;
      toMail = RecData[0].email;
      file += "<div align='center'><h3>Offer Letter</h3></div>" +
        "<div> Dear <b>" + userData[0].firstname + userData[0].lastname + "</b></div>" +
        "<div class='col-md-6 col-sm-6' ><p style=' width:80%; word-wrap: break-word;'>We are delighted to offer you the position of <b>" + RecData[0].type + "</b> with <b>" + RecData[0].name + "</b>.</p>" +
        "<p style=' width:80%; word-wrap: break-word;'>This offer shall stand terminated unless extended at the sole discretion of the Company. Please treat the " +
        "details of this offer with utmost confidentiality and please do not discuss or disclose them to any third " +
        "party under any condition at any time.</p>" +
        "<p>    Your annual compensation will be <b>" + RecData[0].annualctc + " </b>per annum. Details are mentioned in " +
        "Annexure I to this letter. Compensation will be paid monthly and tax will be deducted at source, as " +
        "applicable.</p>" +
        "<p>    Please confirm your acceptance and date of joining by signing the duplicate in the appropriate places and " +
        "returning it to us at the earliest. We are excited and confident that you will make an outstanding " +
        "contribution at <b>" + RecData[0].name + "</b>.</p>" +
        "<p>    We also believe you will find the experience of joining us stimulating and rewarding, both professionally and personally. We look forward eagerly to welcoming you on board.</p></div>"
      fs.writeFile(dir3 + '/' + Id + '.html', file, function (err) {
        if (err) throw err;
        console.log('Html is created successfully.');
        var html = fs.readFileSync('./public/offerLetter/' + Id + '.html', 'utf8');
        pdf.create(html, options).toFile('./public/offerLetter/' + Id + '.pdf', function (err, res) {
          if (err) return console.log(err);
          else {
            console.log('pdf is created successfully.');
            console.log("fromMail ==> " + config.email)
            console.log("toMail ==> " + toMail)
            const options = {
              from: config.email,
              to: toMail,
              subject: 'Letter Of Intend ',
              dsn: {
                id: eid,
                return: 'headers',
                notify: ['failure', 'delay', 'success'],
                recipient: config.email
              },
              text: 'please find your offerLetter ',
              attachments: [{
                path: ABSPATH + '/public/offerLetter/' + Id + '.pdf'
              }]
            };
            sendMail(options)
          }
        });
      });
    })
  })
},

  letterCallLetter: function callLetter(userId, Id, callback) {
    console.log("call letter sent ==> "+userId,Id)
    file = "<html><head><link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' integrity='sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm' crossorigin='anonymous'></head><body>";
    db.any('select * from interviewschedule where fromid=$1 and toid=$2', [userId,Id]).then((data) => {
      interview=data
      db.any('select * from users where id=$1', Id).then((data) => {
        userData = data;
        db.any('select * from users where id=$1', userId).then((data) => {
          fromData = data;
          db.any('select * from applicantsaggrementsview where userid=$1', Id).then((data) => {
            RecData = data;
            toMail = RecData[0].email;
            file += "<div align='center'><h3>Call letter</h3></div>" +
              "<div> This is from <b>" + fromData[0].firstname + fromData[0].lastname + "</b></div>" +
              "<div> Dear <b>" + userData[0].firstname + userData[0].lastname + "</b></div>" +
              "<div>  Your interview is on date : "+interview[0].schedulerdata+"</div>"+
              "<div>  timings from : "+interview[0].fromtime+" to "+interview[0].totime+"</div>"
            fs.writeFile(dir4 + '/' + Id + '.html', file, function (err) {
              if (err) throw err;
              console.log('Html is created successfully.');
              var html = fs.readFileSync('./public/CallLetter/' + Id + '.html', 'utf8');
              pdf.create(html, options).toFile('./public/CallLetter/' + Id + '.pdf', function (err, res) {
                if (err) return console.log(err);
                else {
                  console.log('pdf is created successfully.');
                  const options = {
                    from: config.email,
                    to: toMail,
                    subject: 'Call letter',
                    dsn: {
                      id: eid,
                      return: 'headers',
                      notify: ['failure', 'delay', 'success'],
                      recipient: config.email
                    },
                    text: 'please find your callletter ',
                    attachments: [{
                      path: ABSPATH + '/public/CallLetter/' + Id + '.pdf'
                    }]
                  };
                  sendMail(options)
                }
              });
            });
          })
        })
      })
    })
  }
}
