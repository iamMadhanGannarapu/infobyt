var express = require('express');
var bb = require('bluebird');
var bodyparser = require('body-parser');
var fs = require('fs')
var path = require('path')
var survilleance = express()
var config = require('./init_app');
var options = {
    promiseLib: bb
}
app.use(express.static(path.resolve(__dirname, "public")));
app.use(bodyparser.json({limit:'50mb'}));
app.use(bodyparser.urlencoded({
    limit:'50mb',
    extended: true
}));
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', '*');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
const db = require(__dirname+'/db');
var pgp = require('pg-promise')(options)
survilleance.use(express.static(path.resolve(__dirname, "public")));
survilleance.use(bodyparser.json({limit:'50mb'}));
survilleance.use(bodyparser.urlencoded({
    limit:'50mb',
    extended: true
}));
var cs = config.dbad;
//-----------------------------survelince-----------------------------------/
//getall
{
    survilleance.get('/abc/All', (req, res, next) => {
        var sessionparam = 3;
        db.any('select * from fn_viewsurvlince($1)', sessionparam).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Survelince','ITR','INFO','View of SURVELINCE Table successfull','Survelince',true)");
            res.status(200).send(data);
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Survelince','ITR','ERR','View of SURVELINCE Table unsuccessfull','Survelince',false)");
                console.log('ERROR:Unable to get Surveliance', error.message || error);
                res.status(404).send({ "message": " Unable to get Surveliance" });
            });
    })
    survilleance.get('/Camera/:id', (req, res, next) => {
        var t = req.params.id;
        db.any('select * from fn_viewsurvlincedetails($1)', [t]).then(function (data) {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Survelince','ITR','INFO','View of SURVELINCE Table successfull','Survelince',true)");
            res.status(200).send(data);
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-View:Survelince','ITR','ERR','View of SURVELINCE Table unsuccessfull','Survelince',false)");
                console.log('ERROR:Unable to View Surveliance by Id', error.message || error);
                res.status(404).send({ "message": " Unable to View Surveliance by Id" });
            });
    })
    survilleance.post('/camera/add', (req, res, next) => {
        var c = req.body.camId;
        var u = req.body.url;
        var b = parseInt(req.body.batchId);
        var s = "true";
        db.none(' select fn_addsurvelince($1,$2,$3,$4)', [c, b, u, s]).then(function () {
            db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Survelince','ITR','INFO','Addition of SURVELINCE Table successfull','Survelince',true)");
            res.status(200).send({
                message: "Added succesfully..."
            });
        })
            .catch(error => {
                db.any("insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Addition:Survelince','ITR','ERR','Addition of SURVELINCE Table unsuccessfull','Survelince',false)");
                console.log('ERROR:Unable to Add Surveliance', error.message || error);
                res.status(404).send({ "message": " Unable to Add Surveliance" });
            });
    })
}
app.listen(3500, function (err) {
    if (err) {
        console.log("Error in connecting server");
    } else {
        console.log("Server Started http://localhost:3500/" + ' @ ' + new Date(Date.now()).toLocaleString());
    }
});