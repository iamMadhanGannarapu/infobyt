select * from fn_addrolemaster('Dummy');
select * from fn_addrolemaster('IBAdmin');
select * from fn_addrolemaster('OrganizationAdmin');
select * from fn_addrolemaster('BranchAdmin');
select * from fn_addrolemaster('SessionTeacher');
select * from fn_addrolemaster('Accountant');
select * from fn_addrolemaster('Driver');
select * from fn_addrolemaster('Default');
select * from fn_addrolemaster('Trainee');
select * from fn_addrolemaster('Trainer');
select * from fn_addrolemaster('Support');
select * from fn_addrolemaster('SalesSupport');
select * from fn_addrolemaster('Hod');
select * from fn_addrolemaster('Warden');
update roleMaster set status=true where roleName in ('IBAdmin','Dummy','OrganizationAdmin');
do $$
 declare password varchar(30);
 declare userId int;
 declare roleId int;
 declare year int;
 begin
    Insert into Users(Id,Firstname,Lastname,DOB,Gender,FatherName,MotherName,FatherOccupation,Religion,Nationality,status) Values(0,'Dummy','Dummy',CURRENT_DATE,'O','FatherName','MotherName',0,0,0,'INACTIVE') returning id into userId;
insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Adding-Data:User','INT','INFO','Insertion of SuperAdmin User is successfull','User',true);
select Id from roleMaster where roleName='Dummy' into roleId;
SELECT (random()::numeric)::text into password;
select date_part('year', CURRENT_DATE) into year;
insert into Login values(CONCAT(UPPER('DM'),'_',(year % 100),lpad(extract(month from now())::text,2,'0'),lpad(extract(day from now())::text, 2, '0'),lpad(concat('0',userId),3,'0')),substr(password,3,6),CURRENT_TIMESTAMP,userId,roleId,'INACTIVE');
insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Adding-Data:Login','INT','INFO','Login for SuperAdmin User is Initiated successfully','Login',true);
end;
$$ language 'plpgsql';
do $$
 declare password varchar(30);
 declare userId int;
 declare roleId int;
 declare year int;
 begin
Insert into Users(Firstname,Lastname,DOB,Gender,FatherName,MotherName,FatherOccupation,Religion,Nationality,status) Values('Prabhas','Uppalapati',CURRENT_DATE,'O','FatherName','MotherName',0,0,0,'INACTIVE') returning id into userId;
    insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Adding-Data:User','INT','INFO','Insertion of SuperAdmin User is successfull','User',true);
select Id from roleMaster where roleName='SuperAdmin' into roleId;
SELECT (random()::numeric)::text into password;
select date_part('year', CURRENT_DATE) into year;
Insert into ContactDetails(UserId,Country,State,City,Place,Address,UniqueIdentificationNo,Email,Mobile) 
Values(userId, 101, 36, 706, 'Palace', 'Agdress', 'p0o9i8u7y6t5r4e3w2q1','sa@infobyt.com', '9887766554');
insert into Login values(CONCAT(UPPER('SA'),'_',(year % 100),lpad(extract(month from now())::text,2,'0'),lpad(extract(day from now())::text, 2, '0'),lpad(concat('0',userId),3,'0')),substr(password,3,6),CURRENT_TIMESTAMP,userId,roleId,'INACTIVE');
insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Adding-Data:Login','INT','INFO','Login for SuperAdmin User is Initiated successfully','Login',true);
end;
$$ language 'plpgsql';
do $$
 declare password varchar(30);
 declare userId int;
 declare roleId int;
 declare year int;
 declare loginId varchar(30);
 declare auth varchar(30);
 begin
Insert into Users(Firstname,Lastname,DOB,Gender,FatherName,MotherName,FatherOccupation,Religion,Nationality,status) Values('Virat','Kohli',CURRENT_DATE,'O','FatherName','MotherName',0,0,0,'INACTIVE') returning id into userId;
insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Adding-Data:User','INT','INFO','Insertion of SuperAdmin User is successfull','User',true);
select Id from roleMaster where roleName='IBAdmin' into roleId;
SELECT (random()::numeric)::text into password;
select date_part('year', CURRENT_DATE) into year;
Insert into ContactDetails(UserId,Country,State,City,Place,Address,UniqueIdentificationNo,Email,Mobile) 
Values(userId, 101, 36, 706, 'Palace', 'Agdress', 'q1w2e3r4t5y6u7i8o9p0','admin@infobyt.com', '9080700102');
select CONCAT(UPPER('IB'),'_',(year % 100),lpad(extract(month from now())::text,2,'0'),lpad(extract(day from now())::text, 2, '0'),lpad(concat('0',userId),3,'0')) into loginId;
insert into Login values(loginId,substr(password,3,6),CURRENT_TIMESTAMP,userId,roleId,'INACTIVE');
select fn_addauthenticationmaster(userId,loginId) into auth;
insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Adding-Data:Login','INT','INFO','Login for SuperAdmin User is Initiated successfully','Login',true);

end;
$$
language plpgsql;

insert into contactdetails values(0,0,101,45,150,'dummy','dummy','dummy','dummy','0');
INSERT INTO organizationType(id,name) VALUES (0,'dummy');
INSERT INTO board(id,boardname) VALUES (0,'dummy');
insert into Organization values(0,'dummy Organization','dummy','inactive',0,current_date,0,0,0);
insert into Branch values(0,0,current_date,'inactive',0);
insert into sessionmaster values(0,'dummy');
insert into branchsession values(0,0,0,'dummy',0);
insert into staff values(0,1,0,current_date,0.0,0.0,2);
insert into batch values(0,0,0,current_date,current_date,'inactive','dummy');
insert into countries values(0,'SCL','select from country');
insert into states values(0,'select state',0);
insert into cities values(0,'select city',0);