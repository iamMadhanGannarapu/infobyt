create or replace function fn_addnotificationdetails(receiverrolename text,receiverid int,notificationid int,param_usersession text)
returns integer
 as  $$
declare senderid int;
declare senderrname text;
 declare n int;
 begin
select userid from userprofileview where userid in(select fn_viewuserid from fn_viewuserid(param_usersession)) into senderid;
select rolename from userprofileview where userid in(select fn_viewuserid from fn_viewuserid(param_usersession)) into senderrname;
insert into notificationdetails(senderrolename,SenderUserId,receiverrolename,ReceiverUserId,
                            notificationId,status) values(senderrname,senderid,receiverrolename,receiverid,notificationid,'Active')returning  Id into  n;
    return n;
 end;
    $$ language plpgsql;
		
	CREATE OR REPLACE FUNCTION fn_addEnquiry(Name character varying, Email character varying,Moblie character varying,Message character varying) 
RETURNS integer
    LANGUAGE plpgsql
    AS '
    declare n int;
    begin
	Insert into Enquiry
		(name,email,mobile,message)
	Values(Name,Email,Moblie,Message)
	returning  Id into  n;
	return n;
	end
    ';
CREATE OR REPLACE FUNCTION fn_addrolemaster(rolename character varying) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin
	Insert into RoleMaster
		(RoleName)
	Values(RoleName) returning Id into n;
	return n;
end
';
	
CREATE OR REPLACE FUNCTION fn_addaccountdetails(
	bankname character varying,
	satfid integer,
	accountnumber character varying,
	pannumber character varying,
	pfnumber character varying)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
 begin Insert into Accountdetails(Bankname,staffid,Accountnumber,pannumber,Pfnumber)
 values(bankname ,
		satfid,
  accountnumber,
   pannumber  ,
    pfnumber);
 end;
$BODY$;
--Adding Admin Function
CREATE OR REPLACE FUNCTION fn_addadmin(
    fname character varying,
    mname character varying,
    lname character varying,
    dob date,
    gender character varying,
    fathername character varying,
    mothername character varying,
    fatheroccupation int,
    caste character varying,
    subcaste character varying,
    religion int,
    nationality int,
    rl_name character varying,
    adminid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
AS $BODY$
declare password varchar(30);
declare userId int;
declare roleId int;
declare year int;
begin
Insert into Users(firstName,middleName,lastname,dob,gender,fatherName,motherName,fatherOccupation,caste,subCaste,religion,nationality,status)
Values(Fname, Mname, Lname, DOB, Gender, FatherName, MotherName, FatherOccupation, Caste, SubCaste, Religion, Nationality, 'INACTIVE')
returning Id into userId;
select Id from roleMaster where roleName=rl_name into roleId;
 SELECT (random()::numeric)::text into password;
 select date_part('year', CURRENT_DATE) into year;
 insert into applicants values(userId,AdminId);
 insert into Login values(CONCAT(
     UPPER(SUBSTR(rl_name,0,3)),
                  '_',
                  (year % 100),
                  lpad(extract(month from now())::text,3,'0'),
                       lpad(extract(day from now())::text, 3, '0'),
                            lpad(concat(nextval('login_id_seq')),3,'0')),
                                        substr(password,3,6),CURRENT_TIMESTAMP,userId,roleId,'INACTIVE');
 return userId;
end ;
$BODY$;
--
-- Name: fn_addachievements(integer, character varying, date); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addachievements(userid integer, description character varying, achievementdate date) RETURNS integer
    LANGUAGE plpgsql
    AS '
    declare n int;
    begin
	Insert into Achievements
		(UserId,Description,AchievementDate)
	Values(UserId, Description, AchievementDate)
	returning  Id into  n;
	return n;
	end
    ';
	
--
-- Name: fn_addadmisiondetails(character varying, character varying, integer, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: -
CREATE OR REPLACE FUNCTION fn_addadmisiondetails(
	param_parentemail character varying,
	param_parentmobile character varying,
	param_branchid integer,
	param_userid integer,
	param_feeid integer,
	param_concessionid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
declare ano int;
begin
    select a.Admissionnum from AdmisionDetails a where a.userid=param_userid into ano;
	update AdmisionDetails set status='Inactive' where Admissionnum=ano;
    Insert into AdmisionDetails
        (ParentEmail,ParentMobile,BranchId,UserId,FeeId,ConcessionId)
    Values(param_ParentEmail, param_ParentMobile,param_BranchId,param_UserId,param_FeeId,param_ConcessionId)returning 
	admissionnum into n;
    insert into traineeBatch values(0,n,'Active',current_date,param_branchid);
	insert into cartallocation(admissionnum,orderstatus) values(param_userid,'Active');
	
    return n;
end
$BODY$;
CREATE OR REPLACE FUNCTION fn_addattendancebybatch(
	params_usercode integer,
	params_attendancedate character varying,
	params_intime character varying,
	params_outtime character varying,
	params_statuscode character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin Insert into attendancelog(usrcode,attendanceDate,intime,outtime,statuscode)
            Values(params_usercode,params_attendancedate,params_intime,params_outtime,params_statuscode);
 return n; end 
$BODY$;
--
-- Name: fn_addattendance(integer, time without time zone, time without time zone, date, time without time zone, character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addattendance(usermapdeviceuserid integer, intime time without time zone, outtime time without time zone, attendancedate date, workinghours time without time zone, status character varying) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin Insert into attendance(usermapDeviceUserId,inTime,outTime,attendanceDate,workingHours,status)
            Values(usermapDeviceUserId,inTime,outTime,attendanceDate,workingHours,status);
 return n; end ';
--
-- Name: fn_addattendance(integer, timestamp without time zone, timestamp without time zone, date, timestamp without time zone, character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addattendance(deviceuserid integer, intime timestamp without time zone, outtime timestamp without time zone, attendancedate date, workinghours timestamp without time zone, status character varying) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin Insert into attendance(DeviceUserId,inTime,outTime,attendanceDate,workingHours,status)
            Values(DeviceUserId,inTime,outTime,attendanceDate,workingHours,status) returning id into n;
 return n;
 end 
 ';
--
-- Name: fn_addbatch(integer, integer, date, date, character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addbatch(staffid integer, sessionid integer, startdate date, enddate date, status character varying, name character varying) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin
	Insert into Batch
		(StaffId,SessionId,StartDate,EndDate,Status,Name)
	Values(StaffId, SessionId, StartDate, EndDate, Status, Name) returning Id into n; 
	return n;
end
';
CREATE OR REPLACE FUNCTION fn_addsurvelince(
	camid character varying,
	batchid integer,
	camurl text,
	status boolean)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin 
 insert into survelince (CamId,BatchId,Camurl,Status) values(CamId,BatchId,Camurl,Status)
 returning Id into n;
return n;
end
$BODY$;
--
-- Name: fn_addbranchsessionmaster(integer, integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: -
--									
CREATE OR REPLACE FUNCTION fn_addbranchsession(
	sessionid integer,
	branchid integer,
	medium character varying,
	numOfTrainees integer,
departmentid int)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin
Insert into BranchSession(SessionId,BranchId,Medium,numOfTrainees,DepartmentId)
Values(SessionId, BranchId, Medium, numOfTrainees,departmentid)
returning Id into n;
return n;
end
$BODY$;		
--
-- Name: fn_addbusdrvalocation(integer, integer, date, character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addbusdrvalocation(busid integer, driverid integer, allocationdate date, status character varying) RETURNS integer
    LANGUAGE plpgsql
    AS '
 declare n int;
begin
	Insert into BusDrvAlocation
		(BusId,DriverId,AllocationDate,Status)
	Values(BusId, DriverId, AllocationDate, Status)returning Id into n;
	return n;
end
';
--
-- Name: fn_addbus(character varying, character varying, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addbus(
	regnum character varying,
	description character varying,
	branchid integer
	)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin
	Insert into Bus
		(RegNum,Description,BranchId)
	Values(RegNum, Description, BranchId) returning Id into n;
	return n;
end
$BODY$;
--
-- Name: fn_addsessionmaster(character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addsessionmaster(sessionname character varying) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin
Insert into SessionMaster(SessionName)
Values(SessionName)
returning Id into n;
return n;
end;
';
--
-- Name: fn_addsessionsubject(integer, character varying, integer, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addsessionsubject(
	subjectid integer,
	syllabus character varying,
	batchid integer,branchsessionid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin
	Insert into SessionSubject
		(SubjectId,Syllabus,BatchId,BranchSessionId)
	Values(SubjectId, Syllabus, BatchId,branchsessionid) returning Id into n;
	return n;
end
$BODY$;
--
-- Name: fn_addconcession(integer, character varying, character varying, double precision, date); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addconcession(
	branchid integer,
	concessionname character varying,
	details character varying,
	concession double precision,
	creationdate date,
amount double precision)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin
Insert into Concession
(BranchID,ConcessionName,Details,Concession,CreationDate,amount)
Values(BranchID, ConcessionName, Details, Concession, CreationDate,amount) returning Id into n;
return n;
end
$BODY$;
--
-- Name: fn_addcontactdetails(integer, integer, integer, integer, character varying, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addcontactdetails(userid integer, country integer, state integer, city integer, place character varying, address character varying, uniqueidentificationno character varying, email character varying, mobile character varying) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin
Insert into ContactDetails(UserId,Country,State,City,Place,Address,UniqueIdentificationNo,Email,Mobile)
Values(UserId, Country, State, City, Place, Address, UniqueIdentificationNo, Email, Mobile)
returning Id into n;
return n;
end
'; 
/*
CREATE OR REPLACE FUNCTION fn_addcontactdetails(
	param_userid integer,
	country integer,
	state integer,
	city integer,
	place character varying,
	address character varying,
	uniqueidentificationno character varying,
	email character varying,
	mobile character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
declare uid int;
declare m int;
begin
Insert into ContactDetails(UserId,Country,State,City,Place,Address,UniqueIdentificationNo,Email,Mobile)
Values(param_userid, Country, State, City, Place, Address, UniqueIdentificationNo, Email, Mobile)
returning Id into m;
return m;
end;
$BODY$;
*/
CREATE OR REPLACE FUNCTION fn_addstopfee(
	stopid integer,
	routeid integer,
	amount varchar(50))
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
    declare n int;
    begin
	Insert into transportstopfee
		(stopid,routeid,amount)
	Values(stopid, routeid, amount)
	returning  Id into  n;
	return n;
	end
$BODY$;
-- Name: fn_addappraisaltimetable(integer, date, time without time zone, time without time zone, character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addappraisaltimetable(
	sessionsubjectid integer,
	AppraisalstartdateTime timestamp without time zone,
	AppraisalenddateTime timestamp without time zone,
	appraisalname character varying,
	status character varying,
	appraisalid integer,
totalmarks int)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin
Insert into AppraisalTimetable
        (SessionSubjectId,AppraisalStartdateTime,AppraisalenddateTime,AppraisalName,Status,appraisalid,totalmarks)
    Values(SessionSubjectId,  AppraisalStartdateTime, AppraisalenddateTime, AppraisalName, Status,appraisalid,totalmarks)
    returning Id into n;
return n;
end
$BODY$;
--
-- Name: fn_addtrainersubject(integer, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addtrainersubject(subjectid integer, staffid integer) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin
	Insert into TrainerSubject
		(SubjectId,StaffId)
	Values(SubjectId, StaffId) returning Id into n;
	return n;
end
';
--
-- Name: fn_addfeedback(integer, integer, character varying, double precision); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addfeedback(userid integer, feederid integer, feedbackdescription character varying, rating double precision) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin
	Insert into Feedback
		(UserId,FeederId,FeedbackDescription,Rating )
	Values(UserId, FeederId, FeedbackDescription, Rating )returning Id into n;
	return n;
end
';
--
-- Name: fn_addfeepayments(integer, date, integer, character varying, character varying, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addfeepayments(paidamount integer, paydate date, due integer, paymode character varying, status character varying, traineeid integer) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin
	Insert into FeePayments
		(PaidAmount,PayDate,Due,PayMode,Status,TraineeId)
	Values(PaidAmount, PayDate, Due, PayMode, Status,TraineeId)returning id into n;
	return n;
end
';
--
-- Name: fn_addfees(character varying, integer, integer, double precision); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addfees(feename character varying, branchid integer, branchsessionid integer, feeamount double precision) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin
Insert into Fees(FeeName,BranchId,BranchSessionId,FeeAmount)
Values(FeeName, BranchId, BranchSessionId, FeeAmount)
returning Id into n;
return n;
end
';
--
-- Name: fn_addgallery(integer, character varying, date); Type: FUNCTION; Schema: public; Owner: -
--
CREATE or replace FUNCTION fn_addgallery(branchid integer, descriptn character varying, gallerydate date) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin
	INSERT INTO Gallery(BranchId,Description,GalleryDate) VALUES (BranchId,Descriptn,GalleryDate) ON CONFLICT (Description)
DO UPDATE SET GalleryDate = current_Date returning Id into n;
return n;
end
';
--
-- Name: fn_addholiday(date, character varying, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addholiday(
	holidaydate date,
	holidayenddate date,
	occation character varying,
	branchid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin
	Insert into Holiday
		(holidayDate,HolidayEndDate,Occation,BranchId)
	Values(holidayDate,HolidayEndDate,Occation, BranchId) returning Id into n;
	return n;
end
$BODY$;
--
-- Name: fn_addleavedetails(integer, date, date, character varying, character varying, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addleavedetails(userid integer, fromdate date, todate date, type varchar, status character varying, description character varying, approvedby integer) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin
	Insert into LeaveDetails
		(Userid,FromDate,ToDate,type,Status,Description,ApprovedBy)
	Values(Userid, FromDate, ToDate,type, Status, Description, ApprovedBy)returning Id into n;
	return n;
end
';
--
-- Name: fn_addrolemaster(character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addstaff(
	param_userid integer,
	branchid integer,
	doj date,
	experience double precision,
	salary double precision,
	param_userroleid integer,
	param_splallowances integer,
	param_departmentid integer,
	param_shiftid integer,
	param_otpay double precision)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int; 
declare m int;
begin Insert into Staff(UserId,BranchId,DOJ,Experience,Salary,UserRoleId,departmentid,shiftId,otPay) 
Values(param_UserId,BranchId,DOJ,Experience,Salary,param_UserRoleId,param_departmentid,param_shiftid,param_otpay) returning Id into n;
Update login set UserRoleId=param_UserRoleId where userId=param_UserId;
insert into salary(staffid,salary,splallowances,date) values(n,salary,param_splallowances,CURRENT_TIMESTAMP) returning id into m;
update staff set salary=(m) where id=(n);
return n; end 
$BODY$;
--
-- Name: fn_addsalaryhike(integer, date, double precision); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addsalaryhike(
	param_staffid integer,
	hikedate date,
	increment double precision,
	param_type character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
declare m int;
declare allowanceid int;
declare salaryId int;
begin
Insert into SalaryHike
(StaffId,HikeDate,Increment,salarytype,hikemailstatus)
Values(param_StaffId, HikeDate, Increment,param_type,'Pending') returning Id into n;
if param_type='HIKE'
then
select max(salary) from salary sm where sm.staffid=param_StaffId into m;
m=m+((m*increment)/100);
select splallowances from salary salm where salm.staffid=param_StaffId into allowanceid;
insert into salary(staffid,salary,splallowances,date) values(param_StaffId,m,allowanceid,current_date) returning id into salaryId;
update staff set salary=salaryId where id=param_StaffId;
return m;
else
return m;
END IF;
end
$BODY$;
--
-- Name: fn_addsalary(integer, double precision, double precision); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addsalary(staffid integer, salary double precision, splallowances double precision) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin
	Insert into Salary
		(StaffId,Salary,SplAllowances)
	Values(StaffId, Salary, SplAllowances) returning Id into n;
	return n;
end
';
--
-- Name: fn_addsalarypayment(integer, integer, date, integer, integer, integer, integer, character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addsalarypayment(
	stafid integer,
	paidamount double precision,
	paydate date,
	payable integer,
	allowances double precision,
	deductions double precision,
	bonus double precision,
	payalwdeddetails character varying,
	remarks character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin
	Insert into SalaryPayment
		(
		StafId,PaidAmount,PayDate,Payable,Allowances,Deductions,Bonus,payAlwDedDetails,Remarks)
	Values(StafId, PaidAmount, PayDate, Payable, Allowances, Deductions, Bonus, payAlwDedDetails, Remarks)returning Id into n;
	return n;
end
$BODY$;
--
-- Name: fn_addBranch(integer, date, character varying, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addBranch(
	Organizationid integer,
	startdate date,
	status character varying,
	contactdetailsid integer,
latitude text,
longitude text)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin
Insert into Branch(OrganizationId,StartDate,Status,ContactDetailsId,Latitude,Longitude)
Values(OrganizationId, StartDate, Status, ContactDetailsId,Latitude,Longitude)
returning Id into n;
return n;
end
$BODY$;
--
-- Name: fn_addOrganizationfeedback(integer, integer, character varying, double precision); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addOrganizationfeedback(Organizationid integer, feederid integer, description character varying, rating double precision) RETURNS integer
    LANGUAGE plpgsql
    AS'
    declare n int;
    begin
    Insert into OrganizationFeedback
        (Organizationid,FeederId,Description,Rating)
    Values(Organizationid, FeederId, Description, Rating) returning Id into n;
    return n;
    end
    ';
--
-- Name: fn_addOrganization(character varying, character varying, character varying, integer, date, integer); Type: FUNCTION; Schema: public; Owner: -
-- --
-- CREATE OR REPLACE FUNCTION fn_addddOrganization(name character varying, type character varying, status character varying, establishedyear integer, contactdetailsid integer) RETURNS integer
--     LANGUAGE plpgsql
--     AS '
-- declare n int;
-- begin
-- Insert into Organization(Name,Type,EstablishedYear,Regdate,ContactDetailsId)
-- Values(Name, Type, EstablishedYear, current_date, ContactDetailsId)
-- returning Id into n;
-- return n;
-- end
-- ';
--
-- Name: fn_addstaff(integer, integer, date, double precision, double precision, integer); Type: FUNCTION; Schema: public; Owner: -
--
--
-- Name: fn_addtraineebatch(integer, integer, character varying, date); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addtraineebatch(
    p_batchid integer,
    p_traineeid integer,
    p_status character varying,
    p_startdate date,
    p_branchid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
AS $BODY$
declare n int;
begin
update traineebatch  set status='Inactive' where traineeid=p_traineeid;
    Insert into traineebatch
        (batchid,Traineeid,status,startdate,branchid)
    Values(p_batchid, p_traineeid,p_status,p_startdate,p_branchid) returning traineebatch.batchid into n;
    return n;
end
$BODY$;
--
-- Name: fn_addtraineemarks(integer, integer, double precision); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addtraineemarks(
	traineeid integer,
	subjectid integer,
	appraisalid integer,
	marks double precision)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
 declare n int;
begin
	Insert into   TraineeMarks
		(TraineeId,subjectId,AppraisalId,Marks)
	Values(TraineeId, subjectId,AppraisalId, Marks)returning Id into n;
	return n;
end
$BODY$;
--
-- Name: fn_addsubject(character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addsubject(name character varying) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin
	Insert into SubjectMaster
		(Name)
	Values(Name) returning Id into n;
	
	return n;
end
';
--
-- Name: fn_addtimetable(integer, character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addtimetable(batchid integer, type character varying, remarks character varying) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin
	Insert into TimeTable
		(BatchId,Type,Remarks)
	Values(BatchId, Type, Remarks)
	returning Id into n;
	return n;
end
';
--
-- Name: fn_addtransportfee(character varying, character varying, double precision, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addtransportfee(
	routeid integer,
	stopid integer,
	amount integer
	)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin
	Insert into TransportStopFee
		(RouteId,stopId,amount)
	Values(RouteId,stopId,amount) returning  Id Into n;
	return n;
end
$BODY$;
--
-- Name: fn_addtransportroute(character varying, character varying, character varying, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addtransportroute(name character varying, startpoint character varying, endpoint character varying, busid integer) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin
	Insert into TransportRoute
		(Name,StartPoint,EndPoint,BusId)
	Values(Name, StartPoint, EndPoint, BusId) returning Id into n; 
	return n;
end
';
--
-- Name: fn_addtransportstop(integer, character varying, time without time zone, time without time zone); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addtransportstop(
	routeid integer,
	stopname character varying,
	arrivaltime time without time zone,
	departuretime time without time zone,
	latitude text,
	longitude text)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
 declare n int;
begin
										Insert into TransportStop
		(RouteId,StopName,ArrivalTime,DepartureTime,Latitude,Longitude)
	Values(RouteId, StopName, ArrivalTime, DepartureTime,Latitude,Longitude)returning Id into n;
	return n;
end
$BODY$;
--
-- Name: fn_adduserlogins(integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_adduserlogins(param_userid integer) RETURNS text
    LANGUAGE plpgsql
    AS '
declare n text;
declare lsession text;
begin
select gen_random_uuid() into n;
INSERT INTO UserLogins(userId,logsession)VALUES (param_userid,crypt(n,gen_salt(''md5''))) ON CONFLICT (userId)
DO UPDATE SET logsession =crypt(n,gen_salt(''md5'')) ,loggedTime = current_timestamp returning logsession into lsession;
return lsession;
end
';
--
-- Name: fn_addusermapping(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addbranchmapping(
	b_userdeviceid character varying,
	b_branchid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
    declare n int;
    begin
	Insert into branchmapping
		(userdeviceid,branchid)
	Values(b_userdeviceid,b_branchid);
	return n;
	end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_addusermapping(
	deviceuserid integer,
	appuserid character varying,
	deviceid character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin Insert into usermapping(DeviceUserId,appuserid,deviceId)
            Values(DeviceUserId,appuserid,deviceId);
 return n; end 
$BODY$;
--
-- Name: fn_addusers(character varying, character varying, character varying, date, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addusers(
	fname character varying,
	mname character varying,
	lname character varying,
	dob date,
	gender character varying,
	fathername character varying,
	mothername character varying,
	fatheroccupation int,
	caste character varying,
	subcaste character varying,
	religion int,
	nationality int,
	rl_name character varying,
	adminid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare password varchar(30);
declare userId int;
declare roleId int;
declare year int;
begin
Insert into Users(firstName,middleName,lastname,dob,gender,fatherName,motherName,fatherOccupation,caste,subCaste,religion,nationality,status)
Values(Fname, Mname, Lname, DOB, Gender, FatherName, MotherName, FatherOccupation, Caste, SubCaste, Religion, Nationality, 'INACTIVE')
returning Id into userId;
select Id from roleMaster where roleName=rl_name into roleId;
 SELECT (random()::numeric)::text into password; 
 select date_part('year', CURRENT_DATE) into year;
 insert into applicants values(userId,AdminId);
 insert into Login values(CONCAT(
     UPPER(SUBSTR(rl_name,0,3)),
                  '_',
                  (year % 100),
                  lpad(extract(month from now())::text,3,'0'),
                       lpad(extract(day from now())::text, 3, '0'),
                            lpad(concat(nextval('login_id_seq')),3,'0')),
                                        substr(password,3,6),CURRENT_TIMESTAMP,userId,roleId,'INACTIVE');
 return userId;
end ;
$BODY$;
--
-- Name: fn_addusertransortpayment(integer, integer, date, integer, character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addusertransortpayment(userid integer, paidamount integer, paiddate date, due integer, status character varying) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin
	Insert into UserTransportPayment
		(UserId,PaidAmount,PaidDate,Due,Status)
	Values(UserId, PaidAmount, PaidDate, Due, Status)returning Id into n;
	return n;
end
';
--
-- Name: fn_addusertransport(integer, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_addusertransport(userid integer, stopid integer) RETURNS integer
    LANGUAGE plpgsql
    AS '
 declare n int;
begin
	Insert into UserTransport
		(UserId,StopId)
	Values(UserId, StopId)returning ID into n;
	return n;
end
';
CREATE OR REPLACE FUNCTION fn_testimonials(
	name character varying,
	description text
	)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin
Insert into testimonial (name,description,posteddate)		
	Values(name,description,current_timestamp)
returning Id into n;
return n;
end
$BODY$;
CREATE OR REPLACE FUNCTION fn_addeventparticipationrequest(
	param_eventsid integer,
	param_userid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n integer;
begin
insert into eventrequest(eventsId,userId)values(param_eventsid,param_userid)
returning id into n;
return n;
end
$BODY$;
CREATE OR REPLACE FUNCTION fn_addevents(
	name character varying,
	departmentid integer,
	fromdate date,
	todate date,
	starttime time without time zone,
	endtime time without time zone,
	userid integer,
	contactdetailsid integer,
	description character varying,
	venue character varying,
	branchid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
											 declare n integer;
											 begin
											 insert into events(name ,departmentId,fromDate,toDate,startTime,endTime,
											 userId,contactdetailsId,description,venue,branchId)
											 values(name,departmentid,fromdate,todate,starttime,endtime,userid,contactdetailsid,
											 description,venue,branchid)
											 returning Id into n;
											 return n;
											 end;
											 
$BODY$;
CREATE OR REPLACE FUNCTION fn_addcourses(
	coursenames character varying,
	year integer,
	semester integer,
	levels integer,duration varchar
)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
    declare n int;
    begin
    Insert into courses
        (CourseName,years,semesters, level,durationtype)values (courseNames,year,semester,levels,duration)
    returning  Id into  n;
    return n;
    end
$BODY$;
CREATE OR REPLACE FUNCTION fn_adddepartments(
	names character varying,
	staffids integer,
	courseids integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
    declare n int;
    begin
    Insert into departments
        (name,staffid,courseId)values (names,staffids,courseids)
    returning  Id into  n;
    return n;
    end
$BODY$;
CREATE OR REPLACE FUNCTION fn_adddepartment(
	name character varying,branchId int)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
    declare n int;
    begin
    Insert into department
        (name,branchid)values (name,branchId)
    returning  Id into  n;
    return n;
    end
$BODY$;
create or replace function fn_addorganizationType(Name text)
returns void as
$$
begin
insert into organizationType(name) values(Name);
end;
$$ language plpgsql;
CREATE OR REPLACE FUNCTION fn_addboard(
	boardname text,organizationTypeid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin
insert into board(boardname,organizationTypeid) 
values(boardName,organizationTypeid) returning id into n;	
return n;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_addOrganization(
	organizationTypeId integer,
	boardId integer,
	name character varying,
	type character varying,
	status character varying,
	establishedyear integer,
	contactdetailsid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin
Insert into Organization(organizationTypeId,	boardId
,Name,Type,EstablishedYear,Regdate,ContactDetailsId)
Values(organizationTypeId,	boardId,Name, Type, EstablishedYear, CURRENT_DATE, ContactDetailsId)
returning Id into n;
return n;
end
$BODY$;
CREATE OR REPLACE FUNCTION fn_addlibrary(
	branchid integer,
	departmentid integer,
	bookname character varying,
	authorname character varying,
	description character varying,
	segregation character varying,
	status character varying,
	penalty integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
 declare n int;
   begin				
   Insert into  library 
(branchid,departmentid,bookname,authorname,description,segregation,status,penalty)
values (branchid,departmentid,bookname,authorname,description,segregation,status,penalty)
	returning  Id into  n;
	return n;
	end	
$BODY$;
--add institute type
create or replace function fn_addInstitutetype(institutetypeName text,organizationTypeid int)
returns void as
$$
begin
insert into institutetype(name,organizationTypeid) values(institutetypeName,organizationTypeid);
end;
$$ language plpgsql;
CREATE OR REPLACE FUNCTION fn_addlibrarytransactions(
	userid integer,
	bookid integer,
	returndate date,
status varchar)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $$ 
declare n int;
begin 							   
insert into librarytransactions(userid,bookid,issuedate,returndate,status)  								   
values(userid,bookid,current_date,returndate,status) 							   
returning  Id into  n;
return n;
end	 
$$;
CREATE OR REPLACE FUNCTION fn_addtraineelibrary(
	userid integer,
	bookid integer,
	returndate date,
status varchar)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $$ 
declare n int; 
begin 							   
insert into traineelibrary(userid,bookid,issuedate,returndate,status)  								   
values(userid,bookid,current_date,returndate,status) 							   
returning  Id into  n;
return n;
end	 
$$;
--AddFunction for libraryrequests
CREATE OR REPLACE FUNCTION fn_addlibraryrequests(
	params_bookid integer,
	params_usersession text,
	status varchar
	)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $$
 declare n int;
	 declare userid int;
            begin	
			select fn_viewuserid from fn_viewuserid(params_usersession) into userid;
			Insert into  libraryrequests 
(bookid,userid,status)values (params_bookid,userid,status)
	returning  Id into  n;
	return n;
	end	
$$;
create or replace function fn_addHostel(userid int,
									   branchid int,
									  type varchar,
					facilities varchar,
					sharing varchar,
					blockname varchar,
					roomnum varchar,
					allocationdate date ,
					representativeid int ,
					rentalperiod varchar ) returns void as
$$
begin
insert into hostel(userid ,branchid,
					type ,facilities ,sharing ,blockname,
					roomnum,allocationdate,representativeid,
					rentalperiod)
					values(userid,branchid,
					type,facilities ,sharing,
					blockname,roomnum,allocationdate,
					representativeid ,rentalperiod);
					end;
					$$ language plpgsql;
 



 create or replace function fn_addvisiting(branchid int,userid int) returns integer as
$$
declare n int;
begin
insert into visiting(branchid,userid,outdatetime )
					values(branchid,
					  userid,
					  current_timestamp) returning Id into n;
					  return n;
					end;
					$$ language plpgsql;
					
			
   create or replace function fn_addhostelfee(  totalamount int,paydate date,
					   paymode varchar,
					   payamount int,
					   dueamount int,
					   hostelid int				   
					) returns void as
$$
begin
insert into hostelfee( totalamount,paydate,
					   paymode,
					   paidamount,
					   dueamount,
					   hostelid)
					values(totalamount,paydate,
					   paymode,
					   payamount,
					   dueamount,
					   hostelid);
					end;
					$$ language plpgsql;               
					
create or replace function fn_addprojects(param_branchid int,param_projectname varchar,param_assigneddate date,param_submissiondate date,param_type varchar,param_details varchar,param_teamsize integer,projectfee integer,param_category varchar) returns void
as
$$
begin
insert into projects(branchid,projectname,assigneddate,submissiondate,type,details,teamsize,fee,category)
values(param_branchid,param_projectname,param_assigneddate,param_submissiondate,param_type,param_details,param_teamsize,projectfee,param_category);
end;
$$
language plpgsql;
CREATE OR REPLACE FUNCTION fn_addplacements(
	param_branchid integer,
	jobid integer,
	start_time timestamp without time zone,
	end_time timestamp without time zone,
	designation character varying,
	param_details character varying,
	param_positions integer,
	param_package integer,
	param_name character varying)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
begin
insert into placements(branchid,jobId,starttime,endtime,type,details,positions,package,name,category)
values(param_branchid,jobid,start_time,end_time,designation,param_details,param_positions,param_package,param_name,'placement');
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_forgotusername(
	param_email character varying)
     RETURNS table (current_username varchar,contactId int)
     LANGUAGE 'plpgsql'
     COST 100
     VOLATILE
AS $BODY$
begin
return query
	select am.username,l.contactid from authenticationmaster am join 
loginview l on l.id=am.userid
	where l.email= param_email;
end;
$BODY$;

																						
	create or replace function fn_allocproj(param_userid int, param_projectid int) returns void as
$$
begin
insert into allocateproject(userid,projectid) values(param_userid,param_projectid);
end
$$
language plpgsql;		
CREATE OR REPLACE FUNCTION fn_addsports(
	sportsid integer,
	departmentid integer,
	sessionid integer,
	fromdate date,
	starttime time without time zone,
	endtime time without time zone,
	todate date,
	kitid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $$
 declare n int;
	begin
	insert into sportsmanagement(sportsId,departmentId,sessionId,fromDate,startTime,endTime,toDate,kitId)
	values(sportsid,departmentid,sessionid,fromdate,starttime,endtime,todate,kitid) 
	returning Id into n;
    return n;
	end
$$;
create or replace function fn_allocplac(param_traineeid int, param_placementid int) returns void as
$$
begin
insert into allocateplacement(traineeid,placementid) values(param_traineeid,param_placementid);
end
$$
language plpgsql;


 CREATE OR REPLACE FUNCTION fn_addjob(
	param_jobtittle character varying,
	param_jobdescription character varying,
	param_designation  integer[],
	param_minexperience integer,
	param_maxexperience integer,
	param_annualctc bigint,
	param_othersalary varchar,
	param_vacancy integer,
	param_location character varying,
	param_jobid integer,
	param_ugqualificationid integer,
	param_pgqualificationid integer,
	param_otherqualificationid integer,
	param_requiteremail character varying,
	param_organizationTypeid integer,
	param_description character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n integer;
begin
insert into job(jobTittle,jobDescription,designation,minimumExperience,maximumExperience,annualCtc,
otherSalary,vacancy,location,jobId,
ugQualificationId,pgQualificationId,otherQualificationId,requiterEmail
,organizationTypeId,Description,postingDate)
values(param_jobtittle,param_jobdescription,param_designation,param_minexperience,param_maxexperience,param_annualctc,
param_othersalary,param_vacancy,param_location,param_jobid,param_ugqualificationid,param_pgqualificationid,
param_otherqualificationid,param_requiteremail,param_organizationTypeid,param_description ,current_timestamp
)
returning Id into n;
return n;
end;								
$BODY$;

CREATE OR REPLACE FUNCTION fn_addjobaplicant(
	param_userid integer,
	param_placementid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin
insert into jobapplicants(userid,placementid) values(param_userid,param_placementid) returning id into n;
return n;
end;
$BODY$;
create or replace function fn_addjobrequest(jobid integer,userid integer)returns integer
as
$$
declare n integer;
begin
insert into jobrequest(jobId,userId)values(jobid,userid)
returning Id into n;
return n;
end
$$
language plpgsql;
CREATE OR REPLACE FUNCTION fn_adduserchat(
	param_username text)
    RETURNS character varying
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $$
 declare n varchar;
declare m int;
 begin
SELECT nextval('userchat_id_seq') into m;
insert into userchat(id,username) values(cast(concat('U_',m) as varchar),param_username) returning id into n;
return n;
 end;
$$;
CREATE OR REPLACE FUNCTION fn_addconversationtable(
	param_supportid integer,
	param_supporttype character varying)
    RETURNS character varying
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $$
declare n int;
 begin
-- select user_sequence_id
insert into conversationtable(supportid,status,currentdate,supporttype) values(param_supportid,'Inactive',current_date,param_supporttype) returning id into n;
return n;
 end;
$$;
CREATE OR REPLACE FUNCTION fn_addmessagetable(
	param_messages text,
	param_fromuser character varying,
	param_touser character varying,
	param_conversationid integer)
    RETURNS character varying
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $$
 declare n int;
 begin
insert into messagestable(messages,fromuser,touser,conversationid) values(param_messages,param_fromuser,param_touser,param_conversationid) returning id into n;
return n;
 end;
$$;
CREATE OR REPLACE FUNCTION fn_addtraineetransfer(
	param_traineeid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare admnum int;
declare n int;
declare m int;
declare o int;
begin
select admissionnum from admisiondetails where userid=param_traineeid and status='Active' into admnum;
update  admisiondetails set status='ApplyTc' where admissionnum=admnum 
returning admissionnum into o;
insert into transfertrainee
(traineeid) values(param_traineeid)returning id into n;
update 	transfertrainee
 set status='Applied' where id=n returning id into m; 
return n;
return m;
return o;
 end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_savebillfree(
    param_userId int,
    param_Organizationid int,
    param_buydate timestamp without time zone,
    param_enddate timestamp without time zone,
param_amount int)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
AS $$
 declare n int;
begin
insert into billing (userid,Organizationid,buydate,enddate,amount,status)values(param_userId,param_Organizationid,param_buydate,
param_enddate,param_amount,'ACTIVE') returning id into n;
return n;
end;
$$;
CREATE OR REPLACE FUNCTION fn_addseller(
	param_productname character varying,
	param_productprice double precision,
	param_description text,
	param_category character varying,
	param_sessionid integer,
	param_available boolean,
	param_productdate date)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $$
 declare n int;
begin
	Insert into seller(productname,productprice,description,category,sessionid,available,productdate)
	Values(param_productname, param_productprice,param_description, param_category,param_sessionid,param_available,param_productdate) returning Id into n;
	return n;
end
$$;
CREATE OR REPLACE FUNCTION fn_addcart(
	param_sellerid integer,
	param_quantity integer,
	param_cartallocationid integer,
	param_status character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin
	Insert into cart(sellerid,quantity,cartallocationid,status)
	Values(param_sellerid,param_quantity,param_cartallocationid,param_status)
	ON CONFLICT (sellerid)
    DO UPDATE SET quantity = cart.quantity+1
	returning Id into n;
	return n;
end
$BODY$;
create or replace function fn_addBulkMessage(param_Departmentid int,
param_sessionid int,param_batchId int,param_MessageType varchar,param_message text)
returns integer as
$$
declare n int;
begin
insert into bulkMessage(Departmentid,sessionid,batchId,MessageType,message)
           values(param_Departmentid,param_sessionid,param_batchId,
				  param_MessageType,param_message)
					returning id into n;
						return n;
end;
$$ language plpgsql;
			
			CREATE OR REPLACE FUNCTION fn_addmailbox(
	param_fromuser int,
	param_touser int,
	param_subject text,
	param_mail text)
    RETURNS int
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $$
 declare n int;
 begin
insert into mailbox(fromuser,touser,subject,mail,status,sentdate) values(param_fromuser,param_touser,param_subject,param_mail,'Unread',current_date) returning id into n;
return n;
 end;
$$;
			
CREATE OR REPLACE FUNCTION fn_addleavemaster(
        type varchar(30))
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $$
 declare n int;
begin
	Insert into leavemaster
		(type
		)
	Values(type)returning Id into n;
	return n;
end
$$;
  CREATE OR REPLACE FUNCTION fn_addleavepattern(
        leavetype varchar,numofdays float)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
AS $$
declare n int;
begin
    Insert into leavepattern
        (
         leavetype, numofdays )
    Values(leavetype ,numofdays)returning Id into n;
    return n;
end
$$;
CREATE or replace FUNCTION fn_addrecruitments
(userid int,
    skills  character varying,
 profilesummary  character varying,
 Address character varying,
  maritailStatus character varying,
 languages character varying,
  hobbies character varying,
  Experience  int,
  domain character varying,
 projects character varying
)
RETURNS integer
    LANGUAGE plpgsql
    AS
     $$
 declare n int;
    begin
        insert into recruitments (userid,skills,profilesummary, address, maritailStatus,languages,hobbies,
                         Experience, domain,projects)
                    values(userid,skills,profilesummary, Address, maritailStatus,languages,hobbies,
                         Experience, domain,projects)
            returning  Id into  n;
    return n;
    end
        $$;
CREATE OR REPLACE FUNCTION fn_addrequestrecruitments(
	param_companyid integer,
	param_userid integer,
	param_position character varying,
	param_package character varying,
	param_details character varying,
	param_date date,
	param_time time without time zone,
	param_address character varying,
	param_status character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
    declare n int;
    begin
     insert into requestrecruitments (companyid,userid,position,package,details,date,time,address,status,loimailstatus,loistatus,docverificationstatus,ofrltrmailstatus,ofrltrstatus)
        values(param_companyid,param_userid,param_position,param_package,param_details,param_date,param_time,param_address,param_status,'Pending','Pending','Pending','Pending','Pending')
 returning  Id into  n;
    return n;
    end
$BODY$;
		
CREATE OR REPLACE FUNCTION fn_addgatepass(
description text,indate date,intime varchar ,branchid int,param_usersession text	
)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
declare userid int;
begin
select fn_viewuserid from fn_viewuserid(param_usersession) into userid;
Insert into   gatepass
(userid,description,indate,intime,branchid)values (userid,description,indate,intime,branchid)
	returning  Id into  n;
	return n;
	end	
$BODY$;
/*******************************Hostel Allocate****************************************/
create or replace function fn_addhostelreq(param_traineeid int)
returns integer as
$$
declare n int;
declare param_branchid int;
begin
select a.branchid from admisiondetails a where a.userid in(param_traineeid)
and a.status='Active' 
union
select s.branchid from staff s where userid in (param_traineeid) into param_branchid;				
insert into hostelAllocate(traineeid,branchid) values (param_traineeid ,
														   param_branchid)
														   returning id into n;
														   return n;
end;
$$ language plpgsql;
CREATE OR REPLACE FUNCTION fn_addfeestructure(
	param_sessionid integer,
	param_feename character varying,
	param_amount double precision,
	param_feepattern character varying,
	param_optional boolean,
	param_emi boolean)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin
Insert into feestructure
        (sessionid,feename,amount,feepattern,optional,emi)
    Values(param_sessionid,param_feename,param_amount,param_feepattern,param_optional,param_emi)
    returning Id into n;
return n;
end
$BODY$;
CREATE OR REPLACE FUNCTION fn_addfeestructurepattern(
	param_feestructureid int,
	param_feepattern character varying,
	param_amount double precision)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin
Insert into feestructurepattern
        (feestructureid,feepattern,amount)
    Values(param_feestructureid,param_feepattern,param_amount)
    returning Id into n;
return n;
end
$BODY$;
CREATE OR REPLACE FUNCTION fn_Adddepartment(
    name character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
AS $BODY$
    declare n int;
    begin
    Insert into department
        (name)values (name)
    returning  Id into  n;
    return n;
    end
$BODY$;	
CREATE OR REPLACE FUNCTION fn_addshifts(
	param_branchid integer,
	param_shiftname character varying,
	param_days text[],
	param_start_time time without time zone,
	param_end_time time without time zone,
param_graceintime time without time zone,param_graceouttime time without time zone,param_optinal boolean)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
begin
insert into shifts(branchid,shiftname,days,starttime,endtime,gracetime,graceouttime,optional)
values(param_branchid,param_shiftname,param_days,param_start_time,param_end_time,param_graceintime,param_graceouttime,param_optinal);
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_addstaffshifts(
	params_userid integer,
	params_shiftid integer
	)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n integer;
begin
insert into staffshifts(userid,shiftid) values (params_userid,params_shiftid) returning Id into n;
return n;
end 
$BODY$;					   
	
	
	create or replace function fn_addhalfloginrequest(
		params_userid int,
	params_requesttime time without time zone,
	params_descriprion varchar)															   
	 RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n integer;
begin															   
																   
Insert into halfloginrequets(userid,requesttime,Description,status)values
(params_userid,params_requesttime,params_descriprion,'Pending') returning Id into n;
return n;
end 
$BODY$;				
	
	CREATE OR REPLACE FUNCTION fn_addallocateshifts(
	param_userid integer,
	param_departmentid integer,
	param_shiftid integer,
	param_startdate date,
	param_enddate date
	)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
begin
insert into allocateshifts(userid,departmentid,shiftid,startdate,enddate)
values(param_userid,param_departmentid,param_shiftid,param_startdate,param_enddate);
end;
$BODY$;
create or replace function fn_addpayroll(
		params_branchid int,
	params_startrange double precision,
	params_endrange double precision,
	params_name varchar,
params_amount double precision)															   
	 RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n integer;
begin															   
																   
Insert into payroll(branchid,startrange,endrange,name,amount)values
(params_branchid,params_startrange,params_endrange,params_name,params_amount)
returning Id into n;
return n;
end 
$BODY$;	
	
	create or replace function fn_adddepartmentpolicies(
	params_departmentid int,
		params_userid int,
	params_allowancename varchar,
	params_amount double precision,
params_type varchar)															   
	 RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n integer;
begin															   
																   
Insert into departmentpolicies(departmentid,userid,allowancename,amount,type)values
(params_departmentid,params_userid,params_allowancename,params_amount,params_type) returning Id into n;
return n;
end 
$BODY$;	
																	 
																		 
create or replace function fn_addtds(
		params_branchid int,
	params_tdsstartrange double precision,
	params_tdsendrange double precision,
	params_tdsname varchar,
params_tdsamount double precision)															   
	 RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n integer;
begin															   
																   
Insert into tds(branchid,tdsstartrange,tdsendrange,tdsname,tdsamount)values
(params_branchid,params_tdsstartrange,params_tdsendrange,params_tdsname,params_tdsamount)
returning Id into n;
return n;
end 
$BODY$;		
create or replace function fn_addapplicantperformence(param_jobapplicantid int,param_performencertng double precision,
							param_ontime double precision,param_behaviour double precision,param_appearence double precision,
													  param_communication double precision,param_status varchar)
returns integer as
$$
declare n int;
begin
insert into requirtement(jobapplicantid,performencertng,ontimertng,behaviourrtng,appearencertng,communicationrtng,interviewstatus) values
(param_jobapplicantid,param_performencertng,param_ontime,param_behaviour,param_appearence,param_communication,
 param_status) returning id into n;
return n;
end;
$$ language plpgsql;
CREATE OR REPLACE FUNCTION fn_addsharing(
	fuserid integer,
	socialnetworkid character varying,
	url character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
				declare n int;
 begin Insert into sharing(fuserid,socialnetworkid,url)
 values(fuserid,socialnetworkid,url) returning id into n;
				return n;
 end;
$BODY$;
create or replace function fn_addacdemicdetails(param_userid integer,param_sscpercentage integer,
											   param_interpercentage integer,param_ugpercentage integer,
											   param_pgpercentage integer,parm_otherpercentage integer,
											   param_experience integer) returns integer
											   as
											   $$
											   declare n integer;
											   begin
											   insert into academicdetails(userID,sscPercentage,interPercentage,ugPercentage,
																		  pgPercentage,otherPercentage,experience)
											  values(param_userid,param_sscpercentage,
											   param_interpercentage,param_ugpercentage,
											   param_pgpercentage,parm_otherpercentage,
											   param_experience) 
											   returning Id into n;
											   return n;
											   end;
											   $$
											   language plpgsql;
create or replace function fn_addapplicantsaggrements(param_applicantsid integer,param_annualctc bigint,
													 param_shiftid integer,param_doj date)
													 returns integer
													 as
													 $$
													 declare n integer;
													 begin
													 insert into applicantsaggrements(applicantsid,annualctc,shiftid,doj)
													 values(param_applicantsid ,param_annualctc,param_shiftid,param_doj)
													 returning Id into n;
									                 return n;
									                 end;
													 $$
													 language plpgsql;
													 
CREATE OR REPLACE FUNCTION fn_addpermissions(
	param_roleid integer,
	param_branchid integer,
	param_componentid integer,
	param_read boolean,
	param_write boolean)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin
insert into permissions(roleid,branchid,componentid,read,write) 
values(param_roleid,param_branchid,param_componentid,param_read,param_write) 
ON CONFLICT (roleid,componentid,branchid) do update set read= param_read,write=param_write returning id into n;
return n;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_addsportskit(
	param_sportsid integer,
	param_kitid integer,
	param_branchid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
 declare n int;
 begin
insert into sportskit(sportsid,kitid,branchid) values(param_sportsid,param_kitid,param_branchid) returning id into n;
return n;
 end;
$BODY$;
																	   


CREATE OR REPLACE FUNCTION public.fn_addteam(
	branchid integer,
	departmentid integer,
	name character varying,
	teamsize integer,
staffid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin
Insert into team (branchid,departmentid,name,teamsize,creationdate,staffid)		
	Values(branchid,departmentid,name,teamsize,current_timestamp,staffid)
returning Id into n;
return n;
end
$BODY$;




																								   
	
CREATE OR REPLACE FUNCTION fn_addteamallocation(
	param_userid int,param_teamid int)
       RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin
Insert into teamallocation (userid,teamid)		
	Values(param_userid,param_teamid)
returning Id into n;
return n;
end
$BODY$;
																							   
	
	