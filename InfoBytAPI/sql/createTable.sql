CREATE EXTENSION PGCRYPTO;
CREATE EXTENSION tablefunc;
CREATE SEQUENCE userchat_id_seq start 1;
CREATE SEQUENCE IF NOT EXISTS login_id_seq START 1;
CREATE TABLE IF NOT EXISTS Religion (Id SERIAL , Name VARCHAR(30));
CREATE TABLE IF NOT EXISTS Nationalities(id SERIAL,Name VARCHAR(40));
CREATE TABLE IF NOT EXISTS Designation(id SERIAL,Name VARCHAR(50));
CREATE TABLE IF NOT EXISTS courses (Id SERIAL NOT NULL PRIMARY KEY,CourseName VARCHAR(50) NOT NULL UNIQUE, years Integer NOT NULL,semesters varchar(50) NOT NULL ,level Integer NOT NULL,durationtype varchar(20) NOT Null);
CREATE TABLE IF NOT EXISTS departments (Id SERIAL NOT NULL PRIMARY KEY, name varchar(50) NOT NULL UNIQUE, staffid Integer NOT NULL,courseId Integer NOT NULL,FOREIGN KEY (courseId) REFERENCES courses (id));
CREATE TABLE IF NOT EXISTS organizationType(id serial primary key ,name text NOT NULL);
CREATE TABLE IF NOT EXISTS InstituteType(id SERIAL PRIMARY KEY,name varchar(40) not null,organizationTypeid int references organizationType(id));
CREATE TABLE IF NOT EXISTS board(id serial PRIMARY KEY,boardname text unique, organizationTypeid integer references organizationType(id));
CREATE TABLE IF NOT EXISTS Notifications(id SERIAL PRIMARY KEY,notification VARCHAR(1000));
CREATE TABLE IF NOT EXISTS NotificationDetails(id SERIAL, senderroleName text REFERENCES rolemaster(roleName),SenderUserId INTEGER NOT NULL REFERENCES Users(Id),receiverroleName text REFERENCES rolemaster(roleName),ReceiverUserId INTEGER NOT NULL REFERENCES Users(Id),notificationId int REFERENCES notifications(id),status VARCHAR(30));
CREATE TABLE IF NOT EXISTS Countries (Id SERIAL NOT NULL PRIMARY KEY,CountryCode VARCHAR(3) NOT NULL UNIQUE,Name VARCHAR(150) NOT NULL UNIQUE);
CREATE TABLE IF NOT EXISTS States (Id SERIAL NOT NULL PRIMARY KEY,Name VARCHAR(30) NOT NULL,countryId INTEGER NOT NULL REFERENCES countries(Id) DEFAULT 1);
CREATE TABLE IF NOT EXISTS Cities (Id SERIAL NOT NULL PRIMARY KEY,Name VARCHAR(30) NOT NULL,stateId INTEGER REFERENCES states(Id) NOT NULL);
CREATE TABLE IF NOT EXISTS ContactDetails(Id SERIAL PRIMARY KEY,UserId INTEGER NOT NULL REFERENCES Users(Id) ON DELETE CASCADE,Country INTEGER REFERENCES countries(Id) ON DELETE CASCADE,State INTEGER REFERENCES States(Id) ON DELETE CASCADE,City INTEGER REFERENCES Cities(Id) ON DELETE CASCADE,Place VARCHAR(50),Address VARCHAR(50),UniqueIdentificationNo VARCHAR(20),Email VARCHAR(50) NOT NULL UNIQUE,Mobile VARCHAR(12),STATUS VARCHAR(20) DEFAULT 'PRIMARY');
CREATE TABLE IF NOT EXISTS Organization(Id SERIAL PRIMARY KEY,Name VARCHAR(100) NOT NULL UNIQUE,Type VARCHAR(20) NOT NULL,Status VARCHAR(100) NOT NULL DEFAULT 'INACTIVE',EstablishedYear Integer NOT NULL,RegDate DATE NOT NULL,ContactDetailsId INTEGER NOT NULL REFERENCES ContactDetails(Id) ON DELETE CASCADE,organizationTypeid integer NOT NULL REFERENCES organizationType(id) ON DELETE CASCADE,boardid integer NOT NULL REFERENCES board(id) ON DELETE CASCADE,	CONSTRAINT Already_Organization_Exits UNIQUE (Name,ContactDetailsId));
CREATE TABLE IF NOT EXISTS Branch(Id SERIAL PRIMARY KEY,OrganizationId INTEGER NOT NULL REFERENCES Organization(Id) ON DELETE CASCADE,StartDate DATE NOT NULL,Status VARCHAR(20) NOT NULL DEFAULT 'INACTIVE',ContactDetailsId INTEGER NOT NULL REFERENCES ContactDetails(Id) ON DELETE CASCADE,Latitude text,Longitude text,CONSTRAINT Already_Branch_Exits UNIQUE (OrganizationId, ContactDetailsId));
CREATE TABLE IF NOT EXISTS SessionMaster(Id SERIAL PRIMARY KEY,SessionName VARCHAR(30) NOT NULL UNIQUE);
CREATE TABLE IF NOT EXISTS BranchSession(Id SERIAL PRIMARY KEY,SessionId INTEGER NOT NULL REFERENCES SessionMaster(Id) ON DELETE CASCADE,BranchId INTEGER NOT NULL REFERENCES Branch(Id) ON DELETE CASCADE,Medium VARCHAR(20) NOT NULL,NumOfTrainees INTEGER,departmentid int references departments(id),CONSTRAINT Already_Session_Exits UNIQUE(SessionId,BranchId,Medium));
CREATE TABLE IF NOT EXISTS Fees (Id SERIAL PRIMARY KEY,FeeName VARCHAR(200) NOT NULL,BranchId INTEGER NOT NULL REFERENCES Branch(Id) ON DELETE CASCADE,BranchSessionId INTEGER NOT NULL REFERENCES BranchSession(Id) ON DELETE CASCADE,FeeAmount FLOAT NOT NULL ,CONSTRAINT FeeNameDuplicate UNIQUE(FeeName,BranchSessionId));
CREATE TABLE IF NOT EXISTS Concession(Id SERIAL PRIMARY KEY,BranchID INTEGER NOT NULL REFERENCES Branch(Id) ON DELETE CASCADE,ConcessionName VARCHAR(30) NOT NULL,Details VARCHAR(100),Concession FLOAT NOT NULL,CreationDate DATE NOT NULL,CONSTRAINT Already_Concession_Exits UNIQUE(BranchID,ConcessionName),amount double precision);
CREATE TABLE IF NOT EXISTS Bus(Id SERIAL PRIMARY KEY,RegNum VARCHAR(20) NOT NULL UNIQUE,Description VARCHAR(50),BranchId INTEGER NOT NULL REFERENCES Branch(Id) ON DELETE CASCADE,CONSTRAINT Already_Bus_Exits UNIQUE(RegNum,BranchId),Latitude text,Longitude text);
CREATE TABLE IF NOT EXISTS TransportRoute(Id SERIAL PRIMARY KEY,Name VARCHAR(30) NOT NULL,StartPoint VARCHAR(30) NOT NULL,EndPoint VARCHAR(30) NOT NULL,BusId INTEGER NOT NULL REFERENCES Bus(Id) ON DELETE CASCADE,CONSTRAINT Route_Already_Exists UNIQUE(StartPoint,EndPoint,BusId));
CREATE TABLE IF NOT EXISTS TransportFee(Id SERIAL PRIMARY KEY,Name VARCHAR(100) NOT NULL,Details VARCHAR(50),TransportCharge FLOAT NOT NULL,RoutId INTEGER NOT NULL REFERENCES TransportRoute(Id) ON DELETE CASCADE,CONSTRAINT Already_TrnsFeeExists UNIQUE(Name,TransportCharge,RoutId));
CREATE TABLE IF NOT EXISTS TransportStopFee(id SERIAL constraint pk_primary_stopfee primary key,routeid integer,stopid integer,amount integer,CONSTRAINT TransStopfee_Exists UNIQUE(RouteId,StopId,amount));
CREATE TABLE IF NOT EXISTS department(Id SERIAL PRIMARY KEY, name varchar(50),branchid int references Branch(id));
CREATE TABLE IF NOT EXISTS shifts(id serial primary key,branchid int references Branch(id), shiftname varchar(50),days text[],starttime time without time zone,endtime time without time zone,gracetime time without time zone,graceouttime time without time zone,optional boolean);
CREATE TABLE IF NOT EXISTS Staff(Id SERIAL PRIMARY KEY,UserId INTEGER NOT NULL REFERENCES Users(Id) ON DELETE CASCADE,BranchId INTEGER NOT NULL REFERENCES Branch(Id) ON DELETE CASCADE,DOJ DATE NOT NULL,Experience FLOAT NOT NULL,Salary FLOAT,UserRoleId INTEGER NOT NULL REFERENCES RoleMaster(Id) ON DELETE CASCADE,departmentid int references department(id),CONSTRAINT Already_Staff_Exists UNIQUE(UserId,BranchId,UserRoleId),shiftid integer references shifts(id),otpay double precision);
CREATE TABLE IF NOT EXISTS BranchMapping (userdeviceid character varying,branchid integer);
CREATE TABLE IF NOT EXISTS UserMapping(deviceuserid integer,appuserid character varying(30),deviceid character varying(30),CONSTRAINT UserMappiung_exits UNIQUE(deviceuserid,appuserid,deviceid));
CREATE TABLE IF NOT EXISTS SalaryPattern(id SERIAL,patternName text unique COLLATE pg_catalog."default",CONSTRAINT salarypattern_pkey PRIMARY KEY (id),branchid int references Branch(id));
CREATE TABLE IF NOT EXISTS SalaryAllowancesTable(id SERIAL,patternid integer,allowancestype text COLLATE pg_catalog."default",percentage double precision,type VARCHAR(20),CONSTRAINT salaryallowancestable_patternid_fkey FOREIGN KEY (patternid)REFERENCES salarypattern (id) MATCH SIMPLE);
CREATE TABLE IF NOT EXISTS Enquiry(Id SERIAL,Name VARCHAR(50),Email VARCHAR(50),Mobile VARCHAR(10),Message VARCHAR(300));
CREATE TABLE IF NOT EXISTS Salary (Id SERIAL PRIMARY KEY,StaffId INTEGER NOT NULL REFERENCES Staff(Id) ON DELETE CASCADE,Salary FLOAT NOT NULL,SplAllowances Integer,Date date,CONSTRAINT Salary_Paid UNIQUE(StaffId,Salary));
CREATE TABLE IF NOT EXISTS SalaryHike(Id SERIAL PRIMARY KEY,StaffId INTEGER NOT NULL REFERENCES Staff(Id) ON DELETE CASCADE,HikeDate DATE NOT NULL,Increment FLOAT NOT NULL,SalaryType VARCHAR(10) ,hikemailstatus varchar(30));
CREATE TABLE IF NOT EXISTS SubjectMaster (Id SERIAL PRIMARY KEY,Name VARCHAR(30) NOT NULL UNIQUE);
CREATE TABLE IF NOT EXISTS Gallery(id SERIAL,branchid integer NOT NULL,description character varying(50) COLLATE pg_catalog."default" unique,gallerydate date NOT NULL,CONSTRAINT gallery_pkey PRIMARY KEY (id),CONSTRAINT gallery_branchid_fkey FOREIGN KEY (branchid) REFERENCES Branch (id));
CREATE TABLE IF NOT EXISTS Batch(Id SERIAL PRIMARY KEY,StaffId INTEGER NOT NULL REFERENCES Staff(Id) ON DELETE CASCADE,SessionId INTEGER NOT NULL REFERENCES BranchSession(Id),StartDate DATE NOT NULL,EndDate DATE NOT NULL,Status VARCHAR(20) NOT NULL,Name VARCHAR(20) NOT NULL,CONSTRAINT Already_Batch_Exits UNIQUE(StaffId,SessionId));
CREATE TABLE IF NOT EXISTS Holiday(Id SERIAL PRIMARY KEY,HolidayDate DATE NOT NULL,HolidayEndDate DATE NOT NULL,Occation VARCHAR(30) NOT NULL,BranchId INTEGER NOT NULL REFERENCES Branch(Id) ON DELETE CASCADE,CONSTRAINT HOLIDAY_ERROR UNIQUE(HolidayDate,Occation,BranchId));
CREATE TABLE IF NOT EXISTS TrainerSubject(Id SERIAL PRIMARY KEY,SubjectId INTEGER NOT NULL REFERENCES SubjectMaster(Id) ON DELETE CASCADE,StaffId INTEGER NOT NULL REFERENCES Staff(Id) ON DELETE CASCADE,CONSTRAINT duplicateTrainer UNIQUE (SubjectId,StaffId) );
CREATE TABLE IF NOT EXISTS AdmisionDetails(AdmissionNum SERIAL PRIMARY KEY,ParentEmail VARCHAR(30) NOT NULL,ParentMobile VARCHAR(10) NOT NULL,BranchId INTEGER NOT NULL REFERENCES Branch(Id) ON DELETE CASCADE,UserId INTEGER NOT NULL REFERENCES Users(Id) ON DELETE CASCADE,FeeId INTEGER NOT NULL REFERENCES Fees(Id) ON DELETE CASCADE,ConcessionId INTEGER NOT NULL REFERENCES Concession(Id) ON DELETE CASCADE,Status varchar(30) DEFAULT 'Active',CONSTRAINT Admission_Already_Exists UNIQUE(AdmissionNum,UserId));
CREATE TABLE IF NOT EXISTS FeePayments(Id SERIAL PRIMARY KEY,PaidAmount FLOAT NOT NULL,PayDate TIMESTAMP,Due FLOAT NOT NULL,PayMode VARCHAR(30) NOT NULL,Status VARCHAR(20) NOT NULL,TraineeId INTEGER NOT NULL REFERENCES Users(Id) ON DELETE CASCADE);
CREATE TABLE IF NOT EXISTS SalaryPayment(Id SERIAL PRIMARY KEY,StafId INTEGER NOT NULL REFERENCES Users(Id) ON DELETE CASCADE,PaidAmount FLOAT NOT NULL,PayDate TIMESTAMP,Payable INTEGER NOT NULL REFERENCES Salary(Id) ON DELETE CASCADE,Allowances FLOAT NOT NULL,Deductions FLOAT NOT NULL,Bonus FLOAT NOT NULL,payAlwDedDetails VARCHAR(40) NOT NULL,Remarks VARCHAR(40) NOT NULL,CONSTRAINT Salary_Payment_Exists UNIQUE(StafId,PaidAmount,PayDate,Payable,Allowances,Deductions));
CREATE TABLE IF NOT EXISTS TimeTable (Id SERIAL PRIMARY KEY,BatchId INTEGER NOT NULL REFERENCES Batch(Id) ON DELETE CASCADE,Type VARCHAR(20) NOT NULL,Remarks VARCHAR(20) NOT NULL,CONSTRAINT TimeTable_Added UNIQUE(BatchId,Type));
CREATE TABLE IF NOT EXISTS LeaveDetails(Id SERIAL PRIMARY KEY,Userid INTEGER NOT NULL REFERENCES Users(Id) ON DELETE CASCADE,FromDate DATE NOT NULL,ToDate DATE NOT NULL,type varchar(30) NOT NULL,Status VARCHAR(30) NOT NULL,Description VARCHAR(100) NOT NULL,ApprovedBy INTEGER NOT NULL REFERENCES Users(Id) ON DELETE CASCADE,CONSTRAINT Leave_Applied UNIQUE(Userid,FromDate,ToDate));
CREATE TABLE IF NOT EXISTS Feedback(Id SERIAL PRIMARY KEY,UserId INTEGER NOT NULL REFERENCES Users(Id) ON DELETE CASCADE,FeederId INTEGER NOT NULL REFERENCES Users(Id),FeedbackDescription VARCHAR(40),Rating FLOAT NOT NULL,Date date default current_timestamp,CONSTRAINT Feedback_Given UNIQUE(userId,FeederId,Date));
CREATE TABLE IF NOT EXISTS Achievements (Id SERIAL PRIMARY KEY,UserId INTEGER NOT NULL REFERENCES Users(Id) ON DELETE CASCADE,Description VARCHAR(40) NOT NULL,AchievementDate DATE NOT NULL);
CREATE TABLE IF NOT EXISTS SessionSubject(Id SERIAL PRIMARY KEY,SubjectId INTEGER NOT NULL REFERENCES TrainerSubject(Id) ON DELETE CASCADE,Syllabus VARCHAR(200) NOT NULL,BatchId INTEGER NOT NULL REFERENCES Batch(Id) ON DELETE CASCADE,BranchSessionId INTEGER NOT NULL REFERENCES BranchSession(Id) ON DELETE CASCADE,CONSTRAINT Subject_Added_to_Session UNIQUE(SubjectId,BatchId,BranchSessionId));
CREATE TABLE IF NOT EXISTS OrganizationFeedback (Id SERIAL PRIMARY KEY,OrganizationId INTEGER NOT NULL REFERENCES Organization(Id) ON DELETE CASCADE,FeederId INTEGER NOT NULL REFERENCES Users(Id) ON DELETE CASCADE,Description VARCHAR(30),Rating FLOAT NOT NULL,Date date default current_timestamp,CONSTRAINT Organization_Feedback_Added UNIQUE(OrganizationId,FeederId,Date));
CREATE TABLE IF NOT EXISTS AppraisalTimeTABLE(Id SERIAL PRIMARY KEY,SessionSubjectId INTEGER NOT NULL REFERENCES SessionSubject(Id) ON DELETE CASCADE,AppraisalStartdateTime timestamp without time zone,AppraisalenddateTime timestamp without time zone,AppraisalName VARCHAR(40) NOT NULL,Status VARCHAR(20) NOT NULL DEFAULT 'ACTIVE',AppraisalId integer Not Null,CONSTRAINT Appraisal_TimeTable_Alraedy_Exists UNIQUE(SessionSubjectId,AppraisalStartdateTime,AppraisalenddateTime,AppraisalName,AppraisalId),totalMarks integer);
CREATE TABLE IF NOT EXISTS TraineeMarks (Id SERIAL PRIMARY KEY,TraineeId INTEGER NOT NULL REFERENCES admisiondetails(admissionnum) ON DELETE CASCADE,subjectid integer NOT NULL,AppraisalId INTEGER NOT NULL ,Marks FLOAT NOT NULL,CONSTRAINT duplicatid UNIQUE (Appraisalid,SubjectId,traineeId));
CREATE TABLE IF NOT EXISTS BusDrvAlocation(Id SERIAL PRIMARY KEY,BusId INTEGER NOT NULL REFERENCES Bus(Id) ON DELETE CASCADE,DriverId INTEGER NOT NULL REFERENCES Users(Id) ON DELETE CASCADE,AllocationDate DATE NOT NULL,Status VARCHAR(20) NOT NULL DEFAULT 'ACTIVE',CONSTRAINT driversame UNIQUE(BusId,DriverId));
CREATE TABLE IF NOT EXISTS UserTransportPayment(Id SERIAL PRIMARY KEY,UserId INTEGER NOT NULL REFERENCES Users(Id) ON DELETE CASCADE,PaidAmount FLOAT NOT NULL,PaidDate TIMESTAMP,Due FLOAT NOT NULL,Status VARCHAR(20) NOT NULL DEFAULT 'ACTIVE',CONSTRAINT Transport_Payment_Done UNIQUE(PaidAmount,PaidDate,Due));
CREATE TABLE IF NOT EXISTS TransportStop(Id SERIAL PRIMARY KEY,RouteId INTEGER NOT NULL REFERENCES TransportRoute(Id) ON DELETE CASCADE,StopName VARCHAR(30) NOT NULL,ArrivalTime TIME,DepartureTime TIME,Latitude text,Longitude text,CONSTRAINT Stop_Already_Exists UNIQUE(RouteId,StopName));
CREATE TABLE IF NOT EXISTS UserTransport(Id SERIAL PRIMARY KEY,UserId INTEGER NOT NULL REFERENCES Users(Id) ON DELETE CASCADE,StopId INTEGER NOT NULL REFERENCES TransportStop(Id) ON DELETE CASCADE,CONSTRAINT Already_Trnsport_Exists UNIQUE(UserId,StopId));
CREATE TABLE IF NOT EXISTS TraineeBatch(BatchId INTEGER NOT NULL REFERENCES Batch(Id) ON DELETE CASCADE,TraineeId INTEGER NOT NULL REFERENCES admisiondetails(admissionnum) ON DELETE CASCADE,Status VARCHAR(20) NOT NULL,StartDate date,branchid integer REFERENCES Branch (id) not null );
CREATE TABLE IF NOT EXISTS Applicants (ApplicantId INTEGER NOT NULL REFERENCES Users(Id) ON DELETE CASCADE,CreatedBy INTEGER NOT NULL REFERENCES Users(Id) ON DELETE CASCADE,Status VARCHAR(20) NOT NULL DEFAULT 'ACTIVE');
CREATE TABLE IF NOT EXISTS UserLogins(Id SERIAL primary key,userId integer not null references users(Id) unique,loggedTime timestamp default current_timestamp,logSession text not null,status VARCHAR(10) not null default 'Active');
CREATE TABLE IF NOT EXISTS AttendanceLog(UsrDeviceId int,UsrCode int, AdhaarNumber VARCHAR(255), AttendanceLogId VARCHAR(255), AttendanceDate VARCHAR(255),InTime VARCHAR(255), InDeviceId VARCHAR(255), OutTime VARCHAR(255), OutDeviceId VARCHAR(255), duration VARCHAR(255), StatusCode VARCHAR(255));
CREATE TABLE IF NOT EXISTS Locations(id int,cityId int,locationName VARCHAR(40),pincode VARCHAR(10),CONSTRAINT location_exists UNIQUE(locationName,pincode));
CREATE TABLE IF NOT EXISTS SubCaste(id SERIAL,casteName VARCHAR(35) UNIQUE);
CREATE TABLE IF NOT EXISTS Survelince(Id SERIAL PRIMARY KEY,CamId VARCHAR(20),BatchId INTEGER REFERENCES Batch(Id) ON DELETE CASCADE,CamUrl text,status boolean);
CREATE TABLE IF NOT EXISTS Payslip(userid integer,grosssalary double precision,month character varying COLLATE pg_catalog."default",status character varying COLLATE pg_catalog."default",extraallowancesstatus varchar(20),CONSTRAINT payslip_Already_generated UNIQUE(userid,grosssalary,month));
CREATE TABLE IF NOT EXISTS Testimonial(id SERIAL primary key,Name VARCHAR(40),description text,posteddate date);
CREATE TABLE IF NOT EXISTS Chat (id SERIAL primary key, supportuserid integer unique not null, userid integer unique,CONSTRAINT userid_exists UNIQUE(supportuserid,userid)); 
CREATE TABLE IF NOT EXISTS Messages(id SERIAL, msg text, fromuser int, touser int, status VARCHAR(10), chatid integer REFERENCES chat(id),createddate timestamp not null);
CREATE TABLE IF NOT EXISTS Accountdetails (id serial,bankname varchar(50),staffid integer,accountnumber varchar,pannumber varchar(50),pfnumber varchar(50));
CREATE TABLE IF NOT EXISTS library (Id SERIAL NOT NULL PRIMARY KEY,branchid Integer NOT NULL,departmentid Integer NOT NULL,bookname varchar(50),authorname varchar(50),description varchar(100),segregation varchar(50), status varchar NOT NULL, penalty int,FOREIGN KEY (departmentid) REFERENCES departments (id),FOREIGN KEY (branchid) REFERENCES Branch(id));
CREATE TABLE IF NOT EXISTS Traineelibrary (Id SERIAL NOT NULL PRIMARY KEY, userid Integer NOT NULL,bookid Integer NOT NULL, issuedate date,returndate date not null ,status varchar(10), FOREIGN KEY (bookid) REFERENCES library (id), FOREIGN KEY (userid) REFERENCES users(id));
CREATE TABLE IF NOT EXISTS libraryrequests(id serial,bookid int,userid int,status varchar,FOREIGN KEY (bookid) REFERENCES library (id), FOREIGN KEY (userid) REFERENCES users(id));
CREATE TABLE IF NOT EXISTS librarytransactions (Id SERIAL NOT NULL PRIMARY KEY, userid Integer NOT NULL,bookid Integer NOT NULL,issuedate date,returndate date ,status varchar(10),FOREIGN KEY (bookid) REFERENCES library (id),FOREIGN KEY (userid) REFERENCES users(id));
CREATE TABLE IF NOT EXISTS hostel(id serial primary key not null,userid int references users(id) not null,branchid int references Branch(id) not null,type varchar(30) not null,facilities varchar(30) not null,sharing varchar(30) not null,blockname varchar(30) not null,roomnum varchar(30) not null,	allocationdate date not null,representativeid int references users(id) not null,rentalperiod varchar(30) not null);







CREATE TABLE IF NOT EXISTS visiting(id serial,branchid int,userid int references users(id),indatetime timestamp without time zone, outdatetime timestamp without time zone);


CREATE TABLE IF NOT EXISTS hostelfee(id serial primary key not null,totalamount int,paydate date not null, paymode varchar(30) not null, paidamount int not null, dueamount int not null,hostelid int references hostel(id) not null );
CREATE TABLE IF NOT EXISTS sports(Id serial primary key,sportsType varchar(50) NOT NULL);
CREATE TABLE IF NOT EXISTS sportskit(id serial primary key,sportsId integer references sports(id) NOT NULL,kitId integer not null,branchId integer not null);
CREATE TABLE IF NOT EXISTS sportsmanagement(Id serial,sportsId integer references sports(id) NOT NULL, departmentId integer references departments(id) NOT NULL,SessionID integer references BranchSession(id) not null,fromDate date NOT NULL, startTime time without time zone NOT NULL,endTime time without time zone NOT NULL,toDate date NOT NULL,kitId integer references sportskit(id) NOT NULL ); 
CREATE TABLE IF NOT EXISTS events(Id serial primary key,name varchar(50) not null,departmentId integer references departments(id) not null,fromDate date not null,toDate date not null,startTime time without time zone not null,endTime time without time zone not null,userId integer references users(id) not null,contactdetailsId integer references contactdetails(id) not null,description varchar(50) not null,venue varchar(50) not null,branchId integer not null );
CREATE TABLE IF NOT EXISTS eventrequest(Id serial,eventsId integer references events(id) not null , userId integer references users(id) not null,status varchar(30) default 'Pending',PRIMARY KEY(eventsId, userId));
CREATE TABLE IF NOT EXISTS Joblist(Id serial primary key,JobType varchar(50) not null);
CREATE TABLE IF NOT EXISTS degree(Id serial primary key,uGraduate varchar(70) not null,pGraduate varchar(70) not null,doctorate varchar(70) not null);
CREATE TABLE IF NOT EXISTS job(Id serial primary key, jobTittle varchar(100) not null, jobDescription varchar(300) not null, designation  integer[] not null, minimumExperience integer NOT NULL, maximumExperience integer not null, annualCtc bigint not null, otherSalary varchar, vacancy integer not null,location varchar(20) not null,jobId integer references Joblist(Id) not null,ugQualificationId integer , pgQualificationId integer ,otherQualificationId integer ,requiterEmail varchar(30) not null,organizationTypeId integer references organizationType(Id) not null, Description varchar(250) not null,postingDate date not null );
CREATE TABLE IF NOT EXISTS projects(id serial primary key,projectname varchar(30),branchid int references Branch(id),AssignedDate date, SubmissionDate date, type varchar(70), details text, teamsize integer, fee integer, category varchar);
CREATE TABLE IF NOT EXISTS placements(id serial primary key,name varchar(30) , branchid int references Branch(id), jobId integer references job(Id),	 starttime timestamp, endtime timestamp, type varchar(70), details text, positions integer, package integer, category varchar);
CREATE TABLE IF NOT EXISTS allocateproject(id serial primary key,   userid int references users(id),   projectid int references projects(id), constraint Already_Project_Allocated UNIQUE (userid,projectid));
CREATE TABLE IF NOT EXISTS allocateplacement(id serial primary key,Traineeid int references admisiondetails(admissionnum),placementid int references placements(id),constraint Already_Placement_Allocated UNIQUE (Traineeid,placementid));
CREATE TABLE IF NOT EXISTS jobapplicants(Id serial primary key,userId integer references users(Id) not null, placementId integer references placements(Id) not null, status varchar(20) not null default 'Pending');
CREATE TABLE IF NOT EXISTS userchat(id character varying(10) primary key,username text );
CREATE TABLE IF NOT EXISTS conversationtable(id serial primary key, supportid integer NOT NULL, userid character varying(10) , status character varying(20) NOT NULL, currentdate date NOT NULL, name character varying(30),supporttype character varying(20));
CREATE TABLE IF NOT EXISTS messagestable(id serial primary key,messages text,fromuser character varying(10) ,touser character varying(10) , conversationid integer NOT NULL,status character varying(10));
CREATE TABLE IF NOT EXISTS chatqueue(id serial primary key,userid character varying(10),status character varying(20),starttime time without time zone,endtime time without time zone);
CREATE TABLE IF NOT EXISTS chatfeedback(id serial primary key, rating integer, outtime time without time zone, conversationid integer);
CREATE TABLE IF NOT EXISTS supportattendance(id serial primary key,intime time without time zone,outtime time without time zone, date date, status character varying(20), supportid integer);
CREATE TABLE IF NOT EXISTS transferTrainee(id serial primary key,TraineeId int references users(id),fromBranchId int references Branch(id),toBranchId int references Branch(id),	SessionId int, Status VARCHAR(20) NOT NULL DEFAULT 'Pending', Date date default current_timestamp);	
CREATE TABLE IF NOT EXISTS mediums(Id SERIAL PRIMARY KEY,name varchar(50)NOT NULL);
CREATE TABLE IF NOT EXISTS billing(id serial primary key,userid int references users(id),Organizationid int references Organization(id),buydate timestamp without time zone, enddate timestamp without time zone,amount int,status varchar default 'INACTIVE',constraint userandOrganization_exists unique(userid,Organizationid));
CREATE TABLE IF NOT EXISTS gatepass(id Serial,userid int,description text,indate date, inTime varchar(30), branchid int ,FOREIGN KEY (branchid) REFERENCES Branch(id), FOREIGN KEY (userid) REFERENCES users(id),status varchar(20) DEFAULT 'Pending' );
CREATE TABLE IF NOT EXISTS feepattern(id serial,	Sessionid integer,	feename varchar(50),	feepattern character varying(50),	amount double precision,optional boolean, emi boolean);
CREATE TABLE IF NOT EXISTS genpatterns(id serial ,pattername varchar(50) , amount double precision,Sessionid int, patternemi boolean,patternemiperiod integer);
CREATE TABLE IF NOT EXISTS patternemi(id serial ,pattername varchar(50) ,emiperiod integer, emiamount double precision);
CREATE TABLE IF NOT EXISTS seller(id serial primary key,productname varchar(50), productprice double precision,description text, category varchar(50),Sessionid integer, available boolean,productdate date );
CREATE TABLE IF NOT EXISTS mailbox(id serial, fromuser int, touser int, subject text, mail text,status varchar, conversationid int,sentdate date);
CREATE TABLE IF NOT EXISTS bulkMessage(id serial primary key,Departmentid int references departments(id),Sessionid int references Sessionmaster(id),batchId int references batch(id),MessageType varchar(30),message text,Date date default current_timestamp); 
CREATE TABLE IF NOT EXISTS leavepattern(id serial,patternid int, leavetype varchar(30),	numofdays float,type varchar(20),CONSTRAINT leave_patternid_fkey FOREIGN KEY (patternid)REFERENCES salarypattern (id),CONSTRAINT leavepattern_pkey PRIMARY KEY (id));
CREATE TABLE IF NOT EXISTS leavemaster(id serial,type varchar(30), CONSTRAINT leavemaster_pkey PRIMARY KEY (id), CONSTRAINT leavemaster_type_key UNIQUE (type));
CREATE TABLE IF NOT EXISTS requirtement(id serial primary key,jobapplicantid int references jobapplicants(id),performencertng double precision,ontimertng double precision not null,behaviourrtng double precision not null,appearencertng double precision not null, communicationrtng double precision not null,interviewstatus varchar(20));
CREATE TABLE IF NOT EXISTS requestrecruitments (Id SERIAL NOT NULL PRIMARY KEY,companyid int NOT NULL, userid int not null, position varchar(50)NOT NULL, package varchar(50) NOT NULL, details varchar(100) NOT NULL, date date NOT NULL,time time,address varchar(100) NOT NULL,status varchar(30),loimailstatus varchar(30),loistatus varchar(30),docverificationstatus varchar(30),ofrltrmailstatus varchar(30),ofrltrstatus varchar(30));
CREATE TABLE IF NOT EXISTS hostelAllocate(id serial primary key,Traineeid int references users(id), branchid int references Branch(id),	status varchar(30) default 'Pending',ReqDate date default current_timestamp );
CREATE TABLE IF NOT EXISTS FeeStructure(id serial primary key,feepattern varchar(50),feename varchar(50),amount double precision,optional boolean,emi boolean,Sessionid int);
CREATE TABLE IF NOT EXISTS feestructurepattern(id serial primary key,feestructureid int references FeeStructure(id), feepattern varchar(50), amount double precision);
CREATE TABLE IF NOT EXISTS allocateshifts(id serial primary key,userid int REFERENCES Users(Id) ,departmentid int REFERENCES department(Id),shiftid int REFERENCES shifts(Id), startdate date,enddate date); 
CREATE TABLE IF NOT EXISTS staffshifts(id serial,userid int,shiftid int,foreign key (userid) references users(id), foreign key (shiftid) references shifts(id) );
CREATE TABLE IF NOT EXISTS halfloginrequets(id serial,userid int,requesttime time without time zone, description varchar(30),status varchar(30),foreign key (userid) references users(id));
CREATE TABLE IF NOT EXISTS extraAllowances(id serial,userid int,allowancename varchar(30),amount double precision, month varchar(30),type varchar(20),foreign key (userid) references users(id));
CREATE TABLE IF NOT EXISTS paydetails(id serial,userid int, present int, absent int, weekoff int,grosssalary double precision,netsalary double precision,month varchar,uniquecode varchar(20) unique);
CREATE TABLE IF NOT EXISTS salarypay(id serial,usrcode int,payablestatus varchar(10),approvedstatus varchar(10),totalminutes int,payrate double precision, otminutes int, otpayrate double precision, attendancedate date, shiftminutes int,intime varchar(30),outtime varchar(30),approvalstatus varchar(30));
CREATE TABLE IF NOT EXISTS payroll(id serial,branchid integer,startrange double precision,endrange double precision,name varchar, amount double precision,foreign key (branchid) references Branch(id));
CREATE TABLE IF NOT EXISTS departmentpolicies(id serial,departmentid integer,userid integer, allowancename varchar,amount double precision,type varchar, foreign key (departmentid) references department(id),foreign key (userid) references users(id));
CREATE TABLE IF NOT EXISTS tds(id serial,branchid integer,tdsstartrange double precision, tdsendrange double precision,tdsname varchar, tdsamount double precision,foreign key (branchid) references Branch(id));
CREATE TABLE IF NOT EXISTS sharing (id serial,fuserid INTEGER NOT NULL REFERENCES Users(Id),socialnetworkid varchar(30) REFERENCES authenticationmaster(socialnetworkid),url varchar(500));
CREATE TABLE IF NOT EXISTS cartallocation (id serial,admissionnum integer,orderstatus varchar(50));
CREATE TABLE IF NOT EXISTS cart(id serial primary key,sellerid integer unique, quantity integer,cartallocationid integer,status varchar(50));
CREATE TABLE IF NOT EXISTS academicdetails(Id serial primary key,userId integer references users(Id) not null unique, sscpercentage integer ,interpercentage integer , ugpercentage integer ,pgpercentage integer ,otherpercentage integer , experience integer );
CREATE TABLE IF NOT EXISTS applicantsaggrements(Id serial primary key,applicantsid integer references jobapplicants(Id) not null unique, annualctc bigint not null,shiftid integer references shifts(id) not null,doj date not null,aggrementstatus varchar default 'default');
CREATE TABLE IF NOT EXISTS components (id serial not null primary key,modules varchar(50),components varchar(50) unique,icon varchar);
CREATE TABLE IF NOT EXISTS permissions(id serial not null primary key,BranchID INTEGER NOT NULL REFERENCES Branch(Id),roleid int not null references rolemaster(id),componentid int not null references components(id),read boolean,write boolean,CONSTRAINT Already_permission_Exits UNIQUE(branchid,roleid,componentid));
CREATE TABLE IF NOT EXISTS interviewschedule(id serial,fromtime timestamp without time zone,totime timestamp without time zone,fromid integer,toid integer);
CREATE TABLE IF NOT EXISTS tdsreports(id serial,BranchID INTEGER NOT NULL REFERENCES Branch(Id),userid int,tdspayableamount double precision,taxamount double precision,remaining double precision, foreign key (userid) references users(id),foreign key (branchid) references Branch(id),month varchar(30),uniquecode varchar(50) unique);


CREATE TABLE IF NOT EXISTS Team(Id SERIAL PRIMARY KEY,departmentid integer references department(ID),	Name VARCHAR(100) NOT NULL ,TeamSize int NOT NULL,		CreationDate DATE NOT NULL,BranchId integer references branch(ID),staffid integer not null references users(ID));															  

CREATE TABLE IF NOT EXISTS Teamallocation(Id SERIAL PRIMARY KEY,userid integer references users(ID),teamid integer references team(ID));
								

-- CREATE USER IB_APP_USER WITH PASSWORD 'password';
-- GRANT SELECT,INSERT,UPDATE,REFERENCES ON ALL TABLES IN SCHEMA PUBLIC TO IB_APP_USER;
-- GRANT USAGE , SELECT ON ALL SEQUENCES IN SCHEMA PUBLIC TO IB_APP_USER;
-- GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA PUBLIC TO IB_APP_USER;