  -- Name: loginview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW salarypatternview AS
 SELECT sm.id AS salarypatternid,
    sm.patternname,
    sa.id AS salaryallowancesid,
    sa.allowancestype,
    sa.percentage
   FROM salarypattern sm
     JOIN salaryallowancestable sa ON sm.id = sa.patternid;     
     CREATE OR REPLACE VIEW loginview AS
 SELECT u.id,
    l.loginid,
    l.password,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    u.status AS userstatus,
    r.id AS roleid,
    r.rolename,
    r.creationtime,
    r.modifiedtime,
    r.status AS rolestatus,
    c.id AS contactid,
    c.userid AS contactuserid,
    c.country,
    c.state,
    c.city,
    c.place,
    c.address,
    c.uniqueidentificationno,
    c.email,
    c.mobile,
    l.status as loginstatus
   FROM login l
     JOIN users u ON l.userid = u.id
     JOIN rolemaster r ON r.id = l.userroleid
     JOIN contactdetails c ON c.userid = u.id;
 CREATE OR REPLACE VIEW logview AS
 SELECT u.id,
    l.loginid,
    l.password,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    u.status AS userstatus,
    r.id AS roleid,
    r.rolename,
    r.creationtime,
    r.modifiedtime,
    r.status AS rolestatus,
    c.id AS contactid,
    c.userid AS contactuserid,
    c.country,
    c.state,
    c.city,
    c.place,
    c.address,
    c.uniqueidentificationno,
    c.email,
    c.mobile,
    l.status as loginstatus,
    am.username
   FROM login l
     JOIN users u ON l.userid = u.id
     JOIN rolemaster r ON r.id = l.userroleid
     JOIN contactdetails c ON c.userid = u.id
     join authenticationmaster am on am.userid=u.id;
CREATE OR REPLACE FUNCTION fn_viewsalarypatterns()
RETURNS SETOF salarypatternview 
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
ROWS 1000
AS $BODY$
begin
return query
select * from salarypatternview;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_getallowances(
	param_id integer)
    RETURNS TABLE(id integer, patternid integer, allowancestype text, percentage double precision,type varchar) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
 return query select * from salaryallowancestable s
 where s.patternId=param_id;
end;
$BODY$;  -- Name: Organizationbranchview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW Organizationbranchview AS
 SELECT sm.name,
    sm.type,
    sm.establishedyear,
    sm.regdate,
    sm.contactdetailsid AS Organizationcontact,
    sbm.id AS branchid,
    sbm.Organizationid,
    sbm.startdate,
    sbm.contactdetailsid AS branchcontact,
	sbm.latitude ,sbm.longitude,
    sbm.status AS branchstatus,
    c.place AS branchname
   FROM Organization sm
     JOIN Branch sbm ON sm.id = sbm.Organizationid
     JOIN contactdetails c ON c.id = sbm.contactdetailsid;
  --
  -- Name: userOrganizationview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW userOrganizationview AS
 SELECT sbv.name,
    sbv.type,
    sbv.establishedyear,
    sbv.regdate,
    sbv.Organizationcontact,
    sbv.branchid,
    sbv.Organizationid,
    sbv.startdate,
    sbv.branchcontact,
    sbv.branchname,
    staffdetails.id,
    staffdetails.brid,
    staffdetails.userid
   FROM (Organizationbranchview sbv
     JOIN ( SELECT staff.id,
            staff.branchid AS brid,
            staff.userid
           FROM staff
          WHERE (staff.userid IN ( SELECT loginview.id
                   FROM loginview
                  WHERE (loginview.rolename <> ALL (ARRAY['SuperAdmin'::text, 'IBAdmin'::text]))))) staffdetails ON ((sbv.branchid = staffdetails.brid)))
UNION
 SELECT sbv.name,
    sbv.type,
    sbv.establishedyear,
    sbv.regdate,
    sbv.Organizationcontact,
    sbv.branchid,
    sbv.Organizationid,
    sbv.startdate,
    sbv.branchcontact,
    sbv.branchname,
    staffdetails.admissionnum AS id,
    staffdetails.brid,
    staffdetails.userid
   FROM (Organizationbranchview sbv
     JOIN ( SELECT admisiondetails.admissionnum,
            admisiondetails.branchid AS brid,
            admisiondetails.userid
           FROM admisiondetails
          WHERE (admisiondetails.userid IN ( SELECT loginview.id
                   FROM loginview
                  WHERE (loginview.rolename <> ALL (ARRAY['SuperAdmin'::text, 'IBAdmin'::text]))))) staffdetails ON ((sbv.branchid = staffdetails.brid)));
                --
-- Name: adminOrganizationview; Type: VIEW; Schema: public; Owner: -
--
CREATE OR REPLACE VIEW adminOrganizationview AS
 SELECT sm.id,
    sm.id AS branchid,
    sm.name,
    sm.type,
    sm.status,
    sm.establishedyear,
    sm.regdate,
    sm.contactdetailsid,
    sm.id AS Organizationid,
    cd.id AS usercontactid,
    cd.userid,
    cd.country,
    cd.state,
    cd.city,
    cd.place,
    cd.address,
    cd.uniqueidentificationno,
    cd.email,
    cd.mobile
   FROM (Organization sm
     JOIN contactdetails cd ON ((cd.id = sm.contactdetailsid)))
UNION
 SELECT sm.id,
    Organizationbranchdetails.branchid,
    sm.name,
    sm.type,
    sm.status,
    sm.establishedyear,
    sm.regdate,
    sm.contactdetailsid,
    Organizationbranchdetails.Organizationid,
    Organizationbranchdetails.usercontactid,
    Organizationbranchdetails.userid,
    Organizationbranchdetails.country,
    Organizationbranchdetails.state,
    Organizationbranchdetails.city,
    Organizationbranchdetails.place,
    Organizationbranchdetails.address,
    Organizationbranchdetails.uniqueidentificationno,
    Organizationbranchdetails.email,
    Organizationbranchdetails.mobile
   FROM (Organization sm
     JOIN ( SELECT sbm.Organizationid,
            sbm.id AS branchid,
            c.id AS usercontactid,
            c.userid,
            c.country,
            c.state,
            c.city,
            c.place,
            c.address,
            c.uniqueidentificationno,
            c.email,
            c.mobile
           FROM (Branch sbm
             JOIN contactdetails c ON ((c.id = sbm.contactdetailsid)))) Organizationbranchdetails ON ((sm.id = Organizationbranchdetails.Organizationid)));
 --
-- Name: userprofileview; Type: VIEW; Schema: public; Owner: -
--
CREATE OR REPLACE VIEW userprofileview AS
 SELECT loginview.id,
    loginview.loginid,
    loginview.password,
    loginview.firstname,
    loginview.middlename,
    loginview.lastname,
    loginview.dob,
    loginview.gender,
    loginview.fathername,
    loginview.mothername,
    loginview.fatheroccupation,
    loginview.caste,
    loginview.subcaste,
    loginview.religion,
    loginview.nationality,
    loginview.userstatus,
    loginview.roleid,
    loginview.rolename,
    loginview.creationtime,
    loginview.modifiedtime,
    loginview.rolestatus,
    loginview.contactid,
    loginview.contactuserid,
    loginview.country,
    loginview.state,
    loginview.city,
    loginview.place,
    loginview.address,
    loginview.uniqueidentificationno,
    loginview.email,
    loginview.mobile,
    userOrganizationview.Organizationid,
    userOrganizationview.name,
    userOrganizationview.type,
    userOrganizationview.establishedyear,
    userOrganizationview.regdate,
    userOrganizationview.Organizationcontact,
    userOrganizationview.branchid,
    userOrganizationview.startdate,
    userOrganizationview.branchcontact,
    userOrganizationview.userid
   FROM (loginview
     JOIN userOrganizationview ON ((loginview.id = userOrganizationview.userid)))
UNION
 SELECT loginview.id,
    loginview.loginid,
    loginview.password,
    loginview.firstname,
    loginview.middlename,
    loginview.lastname,
    loginview.dob,
    loginview.gender,
    loginview.fathername,
    loginview.mothername,
    loginview.fatheroccupation,
    loginview.caste,
    loginview.subcaste,
    loginview.religion,
    loginview.nationality,
    loginview.userstatus,
    loginview.roleid,
    loginview.rolename,
    loginview.creationtime,
    loginview.modifiedtime,
    loginview.rolestatus,
    loginview.contactid,
    loginview.contactuserid,
    loginview.country,
    loginview.state,
    loginview.city,
    loginview.place,
    loginview.address,
    loginview.uniqueidentificationno,
    loginview.email,
    loginview.mobile,
    adminOrganizationview.Organizationid,
    adminOrganizationview.name,
    adminOrganizationview.type,
    adminOrganizationview.establishedyear,
    adminOrganizationview.regdate,
    adminOrganizationview.contactdetailsid AS Organizationcontact,
    adminOrganizationview.branchid,
    adminOrganizationview.regdate AS startdate,
    adminOrganizationview.usercontactid AS branchcontact,
    adminOrganizationview.userid
   FROM (loginview
     JOIN adminOrganizationview ON ((loginview.id = adminOrganizationview.userid)));
  --
  -- Name: feedbackview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW feedbackview AS
 SELECT u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    u.status,
    c.userid AS contacuid,
    c.country,
    c.state,
    c.city,
    c.place,
    c.address,
    c.uniqueidentificationno,
    c.email,
    c.mobile,
    s.id,
    s.type,
    s.name,
    s.status AS schmastatus,
    s.establishedyear,
    s.regdate,
    s.contactdetailsid AS schcontact,
    sbm.id AS branchid,
    sbm.Organizationid,
    sbm.startdate,
    sbm.status AS sbmstatus,
    sbm.contactdetailsid,
    fdm.id AS feedbckid,
    fdm.userid AS feedbckuserid,
    fdm.feederid,
    fdm.feedbackdescription,
    fdm.rating
   FROM ((((((feedback fdm
     JOIN users u ON ((fdm.userid = u.id)))
     JOIN applicants a ON ((a.applicantid = u.id)))
     JOIN userprofileview upv ON ((a.applicantid = upv.id)))
     JOIN Branch sbm ON ((sbm.id = upv.branchid)))
     JOIN Organization s ON ((s.id = sbm.Organizationid)))
     JOIN contactdetails c ON ((c.id = s.contactdetailsid)));
  --
  -- Name: fn_feedback(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_feedback(
	param_usersession text)
    RETURNS table(feederid int,feedbackdescription varchar,rating double precision)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
	 begin
	 return
	 query
select f.feederid,f.feedbackdescription,f.rating from feedback f where f.userid=
(select userid from userProfileview 
where id=(select fn_viewuserid from
		  fn_viewuserid(param_usersession))); 
	 end
	 	 
$BODY$;
  --
  -- Name: fn_forgotpassword(character varying, character varying); Type: FUNCTION; Schema: public; Owner:  --
  --
 CREATE OR REPLACE FUNCTION fn_forgotpassword(
	param_loginid character varying,
	param_email character varying)
    RETURNS table (current_password text,contactId integer)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
begin
return query
	select l.password,l.contactId from loginview l 
	where l.Loginid= param_loginid and l.email= param_email;
end;
$BODY$;
  --
  -- Name: fn_getallcountries(); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_getallcountries() RETURNS SETOF countries
    LANGUAGE plpgsql
    AS $$
begin
 return query select * from countries;
end;
$$;
  --
  -- Name: fn_getbranchidofuser(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_getbranchidofuser(param_userid integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
declare n int;
begin
select branchid from userprofileview where id=param_userid into n;
return n;
end
$$;
  --
  -- Name: fn_getcities(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_getcities(param_id integer) RETURNS SETOF cities
    LANGUAGE plpgsql
    AS $$
begin
 return query select * from cities
 where stateId=param_id;
end;
$$;
  --
  -- Name: fn_getpassword(text, character varying); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_getpassword(param_usersession text, oldpassword character varying) RETURNS TABLE(username integer, passwordd text)
    LANGUAGE plpgsql
    AS $$
begin
return query select userid,password from login where userid=(select fn_viewuserid from fn_viewuserid(param_usersession)) and password=oldpassword;
end;
$$;
  --
  -- Name: fn_getplaces(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_getplaces(param_id integer) RETURNS SETOF locations
    LANGUAGE plpgsql
    AS $$
begin
return query select * from locations where cityid=param_Id;
end;
$$;
  --
  -- Name: fn_getOrganizationidofuser(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_getOrganizationidofuser(param_userid integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
declare n int;
begin
select Organizationid from userprofileview where id=param_userid into n;
return n;
end
$$;
  --
  -- Name: fn_getstates(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_getstates(param_id integer) RETURNS SETOF states
    LANGUAGE plpgsql
    AS $$
begin
 return query select * from states
 where countryId=param_id;
end;
$$;
  --
  -- Name: Organizationfeedbackview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW schfeedbackview AS
 SELECT sfb.id,
 sfb.Organizationid,
    sfb.feederid,
    sfb.description,
    sfb.rating,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    u.status AS userstatus,
    st.branchid,
    sbm.startdate,
    sbm.contactdetailsid
 FROM Organizationfeedback sfb
     JOIN users u ON sfb.feederid = u.id
     JOIN staff st ON st.userid = u.id
     JOIN Branch sbm ON sbm.id = st.branchid
union
 SELECT sfb.id,
 sfb.Organizationid,
    sfb.feederid,
    sfb.description,
    sfb.rating,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    u.status AS userstatus,
    st.branchid,
    sbm.startdate,
    sbm.contactdetailsid
FROM Organizationfeedback sfb
     JOIN users u ON sfb.feederid = u.id
     JOIN admisiondetails st ON st.userid = u.id
     JOIN branch sbm ON sbm.id = st.branchid;
CREATE OR REPLACE FUNCTION fn_Organizationfeedback(param_usersession text) RETURNS SETOF schfeedbackview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select * from schfeedbackview where branchid in(select branchid from userProfileview
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$$;
  --
  -- Name: achievementsview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW achievementsview AS
 SELECT ac.id,
    ac.userid,
    ac.description,
    ac.achievementdate,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    u.status AS userstatus,
    sbm.id AS branchid,
    sbm.Organizationid,
    sbm.startdate,
    sbm.contactdetailsid
   FROM (((achievements ac
     JOIN users u ON ((ac.userid = u.id)))
     JOIN userprofileview upv ON ((upv.id = u.id)))
     JOIN branch sbm ON ((sbm.id = upv.branchid)));
  --
  -- Name: fn_viewachievementdetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewachievementdetails(param_id integer) RETURNS SETOF achievementsview
    LANGUAGE plpgsql
    AS $$
begin
return query
select * from achievementsview
where Id=param_Id;
end;
$$;
  --
  -- Name: fn_viewachievements(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewachievements(param_usersession text) RETURNS SETOF achievementsview
    LANGUAGE plpgsql
    AS $$
begin
return query
select * from achievementsview where branchid in(select branchid from userProfileview 
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$$;
 create or replace function fn_ViewAchievementbyUserId(param_userId int)
returns setof achievements as
$$
begin
 return query
select * from achievements where userid=param_userId;
 end;
$$ language plpgsql;
  --
  -- Name: fn_viewacontactdetailsbyid(character varying, integer, integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewacontactdetailsbyid(param_rolename character varying, param_Organizationid integer, param_branchid integer) RETURNS TABLE(id integer, rolename text, name character varying, type character varying, establishedyear integer, regdate date)
    LANGUAGE plpgsql
    AS $$
begin
 return query
select rl.id ,rl.rolename ,sm.name ,sm.type ,sm.establishedyear ,sm.regdate
from branch,rolemaster as rl
join login as l on l.userroleid=rl.id
join users as u on u.id=l.userid
join contactdetails as c on c.userid=u.id
join Organization as sm on sm.contactdetailsid=c.id
join branch as sbm on sbm.Organizationid=sm.id where rl.rolename=param_RoleName
                                    and sbm.Organizationid=param_OrganizationId and sbm.id=param_branchId ;
end;
$$;
  --
  -- Name: fn_viewacontactdetailsbyrole(text, integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewacontactdetailsbyrole(param_rolename text, param_Organizationid integer) RETURNS TABLE(id integer, firstnamename text, lastname text)
    LANGUAGE plpgsql
    AS $$
begin
 return query
select  c.id,u.firstname,u.lastname from users u join login l on l.userid=u.id join contactdetails as c on c.userid=u.id
where u.id in  (select userId from login where userroleId=(select id from rolemaster as rl where rl.roleName='BranchAdmin')) ;
end;
$$;
  --
  -- Name: admisionview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW admisionview AS
 SELECT a.admissionnum,
    a.parentemail,
    a.parentmobile,
    a.branchid,
    a.userid,
    a.feeid,
    a.concessionid,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    u.status,
    f.feename,
    f.branchsessionid,
    f.feeamount,
    bcm.sessionid,
    bcm.medium,
    bcm.numoftrainees,
    c.sessionname,
    cm.concessionname,
    cm.concession,
    a.status AS admisionstatus,
    cd.place AS branchname
   FROM admisiondetails a
     JOIN users u ON u.id = a.userid
     JOIN fees f ON f.id = a.feeid
     JOIN branchsession bcm ON bcm.id = f.branchsessionid
     JOIN sessionmaster c ON c.id = bcm.sessionid
     JOIN concession cm ON a.concessionid = cm.id
join branch sbm  on sbm.id=a.branchid
join contactdetails cd on sbm.contactdetailsid=cd.id;
  --
  -- Name: fn_viewadmision(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewadmision(
	param_usersession text)
    RETURNS SETOF admisionview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
 return query
select * from admisionview where admisionstatus !='Inactive' and
branchid in(select branchid from userProfileview 
where rolename!='Trainee' and id =(select fn_viewuserid
					from fn_viewuserid(param_usersession)))
union
select * from admisionview where userid in(select userid from userProfileview 
where id in(select fn_viewuserid 
	from fn_viewuserid(param_usersession)))
and BranchId in(select branchid from userProfileview 
where id=(select fn_viewuserid from 
		fn_viewuserid(param_usersession)));
 end;
$BODY$;
  --
  -- Name: fn_viewadmisiondetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewadmisiondetails(param_admissionnum integer) RETURNS SETOF admisionview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select * from admisionview where admissionnum=param_admissionNum;
end;
$$;
  --
  -- Name: fn_viewallapplicantsbyrole(character varying, text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewallapplicantsbyrole(
    params_rolename character varying,
    param_usersession text)
    RETURNS TABLE(firstname character varying, lastname character varying, userid integer)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
 return query
 select u.firstname,u.lastname,u.id from users u where u.id in
(select u.id as userid
    from Applicants a
        join users as u on u.id=a.applicantid
        join login as l on l.userId=u.id
where a.createdby=(select fn_viewuserid from fn_viewuserid(param_usersession))
and l.userroleid in (select id from rolemaster where rolename like params_rolename||'%')
except
(select a.userid as userid from Admisiondetails a
union
select bda.driverid as userid from BusDrvAlocation bda)) order by u.firstname asc;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewallapplicantsbyrolebyid(
	params_rolename character varying,
	param_usersession text)
    RETURNS TABLE(firstname character varying, lastname character varying, userid integer) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
 return query
 select u.firstname,u.lastname,u.id from users u where u.id in
(select u.id as userid
    from Applicants a
        join users as u on u.id=a.applicantid
        join login as l on l.userId=u.id
where a.createdby=(select fn_viewuserid from fn_viewuserid(param_usersession))
and l.userroleid in (select id from rolemaster where rolename like params_rolename||'%')) order by  firstname asc;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewallapplicantsbyroleFortrainees(
	params_rolename character varying,
	param_usersession text)
    RETURNS TABLE(firstname character varying, lastname character varying, userid integer) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
 return query
 select u.firstname,u.lastname,u.id from users u where u.id in
(select u.id as userid
    from Applicants a
        join users as u on u.id=a.applicantid
        join login as l on l.userId=u.id
where a.createdby=(select fn_viewuserid from fn_viewuserid(param_usersession))
and l.userroleid in (select id from rolemaster where rolename like params_rolename||'%'));
end;
$BODY$;
  --
  -- Name: fn_viewallstaffbyrole(character varying, text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewallstaffbyrole(params_rolename character varying, param_usersession text) RETURNS TABLE(firstname character varying, lastname character varying, userid integer, staffid integer, branchid integer)
    LANGUAGE plpgsql
    AS $$
begin
 return query select u.firstname,u.lastname,u.id,s.id,s.branchid
         from users as u
		 join staff as s on u.id=s.userid
		 join branch as sbm on sbm.id=s.branchid
         where s.branchid in(select upv.branchid from userProfileview as upv
where id=(select fn_viewuserid from fn_viewuserid(param_userSession))) and u.id in
         (select s.userid from staff as s where userroleid in
          (select r.id from rolemaster as r where rolename like params_rolename||'%'));
end;
$$;
  --
  -- Name: fn_viewallstaffusers(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewallstaffusers(
	params_usersession text)
    RETURNS TABLE(firstname character varying, lastname character varying, userid integer) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query select s.firstname,s.lastname,s.userid as userid from staffview s where
s.branchid in(select branchid from userProfileview
where id=(select fn_viewuserid from fn_viewuserid(params_usersession))) order by s.firstname asc;
end;
$BODY$;
create or replace function fn_ViewAllStaffUserss(param_usersession text)
returns table(firstname varchar,lastname varchar,userid int) as
$$
begin
return query
select u.firstname,u.lastname,u.id as userid
from users as u
where id in
(select log.userid from login log where log.userroleid!=
 (select id from rolemaster where rolename='Trainee') and log.userid in
(select l.applicantid from applicants as l where createdby in
 (select fn_viewuserid from fn_viewuserid(param_usersession))))
 except
select s.firstname,s.lastname,s.userid as userid from staffview s where
s.branchId in(select branchid from userProfileview
where id in(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$$ language plpgsql;
  --
  -- Name: fn_viewallusers(character varying); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewallusers(params_rolename character varying) RETURNS TABLE(firstname character varying, lastname character varying, userid integer)
    LANGUAGE plpgsql
    AS $$
begin
 return query select u.firstname,u.lastname,u.id
         from users as u
         where id in
         (select l.userid from login as l where userroleid in
          (select r.id from rolemaster as r where rolename like params_rolename||'%'));
end;
$$;
CREATE OR REPLACE FUNCTION fn_branchmapping(
	param_usersession text)
    RETURNS TABLE(usrcode integer) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select a.usrcode from branchmapping a where usrdeviceid in(
	select userdeviceid from branchmapping where branchid in(
		select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewuserofbranch(
    param_usersession text)
    RETURNS TABLE(id integer, firstname character varying, lastname character varying, loginid character varying)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
return query
select up.id,up.firstname,up.lastname,up.loginid from userprofileview up where up.id
in(select up.id as userid from userprofileview up
 where roleid!=(select rm.id from rolemaster rm where rm.rolename='OrganizationAdmin')
and branchid in( select branchid from userprofileview upw where upw.id =
    (select fn_viewuserid from fn_viewuserid(param_usersession)))
except
select cast(usermapping.appuserid as int) as userid from usermapping);
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewuserdeviceid(
	param_userserssion text)
    RETURNS TABLE(usrdeviceid character varying, branchid integer) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
										
select b.userdeviceid,b.branchid from branchmapping b where b.branchid in(
select upv.branchid from userprofileview upv where upv.id=(
select fn_viewuserid from fn_viewuserid(param_userserssion)));	
end;
$BODY$;
CREATE OR REPLACE VIEW attendancelogview AS
 SELECT um.appuserid,
    a.attendancedate,
    a.intime,
    a.outtime,
    a.duration,
    a.statuscode,
    u.firstname,
    u.lastname,
    a.usrdeviceid,
    r.rolename,
    a.attendancelogid
FROM attendancelog a
     JOIN usermapping um ON um.deviceuserid = a.usrcode
     JOIN users u ON u.id = cast(um.appuserid as int)
     JOIN login l ON l.userid = cast(um.appuserid as int)
     JOIN rolemaster r ON r.id = l.userroleid;
  --
  -- Name: fn_viewattendance(); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewattendance() RETURNS SETOF attendancelogview
    LANGUAGE plpgsql
    AS $$
begin
 return query
select * from attendancelogView;
 end;
$$;
  --
 CREATE OR REPLACE FUNCTION fn_viewallbranchadmins(
	param_branchid integer,
	param_month integer,
	param_year integer)
    RETURNS TABLE(id integer, firstname character varying, lastname character varying, usrcode integer, absent integer, present integer, weekoffs integer) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
declare n int;
begin
select count(holidaydate) from holidaymaster where (date_part('month',holidaydate)=param_month) and (date_part('year',holidaydate)=param_year) into n ;
    return query
	 select us.id,us.firstname,us.lastname,cast(newtable.usrcode as int),newtable._a,newtable._p,newtable._wo from crosstab ('select * from userattendanceview','select distinct statusCode from attendanceLog order by 1') as newtable (usrcode varchar,_A integer,_P integer,_WO integer)
	join usermapping um on um.deviceuserid=cast(newtable.usrcode as integer)
    join users us on us.id=cast(um.appuserid as integer) where us.id in
(with bstaff as (select userid from userprofileview where (rolename='Driver' or rolename='Faculty' or rolename='ClassTeacher' or rolename='Accountant') and branchid=param_branchid)
select * from bstaff); 												 
end;
$BODY$;
  -- Name: fn_viewattendancebydate(text, character varying, character varying); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewattendancebydate(
	param_usersession text,
	param_attendancedate1 date,
	param_attendancedate2 date)
    RETURNS SETOF attendancelogview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from attendancelogview
where (cast (attendancedate as date) between param_attendancedate1 and param_attendancedate2)
and cast (appuserid as int)=(select fn_viewuserid from fn_viewuserid(param_usersession));
																												 
end;
$BODY$;
  --
  -- Name: fn_viewattendancedetails(character varying); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewattendancedetails(param_attendancelogid character varying) RETURNS SETOF attendancelogview
    LANGUAGE plpgsql
    AS $$
begin
return query
select * from attendancelogview
where AttendanceLogId=param_AttendanceLogId;
end;
$$;
CREATE OR REPLACE FUNCTION fn_viewattendancelog(
	param_attendancelogid varchar)
    RETURNS SETOF attendancelogview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
select * from attendancelogview where attendancelogid =param_attendancelogid;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewattendancelogbyid(
	param_attendancelogid character varying)
    RETURNS SETOF attendancelogview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
select * from attendancelogview where attendancelogid =param_attendancelogid;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewattendancelogtraineebystaff(
	param_usersession text,
	param_attendancedate1 date,
	param_attendancedate2 date)
    RETURNS SETOF attendancelogview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
select * from attendancelogview where (cast (attendancedate as date) between param_attendancedate1 and param_attendancedate2 )
and cast (appuserid as int) in 
 (select userid from admisiondetails where admissionnum in (select traineeid from traineebatch where batchid=	(																			 
select id from batch where staffid=(select id from staff 
 where userid=(select fn_viewuserid from fn_viewuserid(param_usersession)))))) ;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewattendancelogstraineebyaccountant(
	param_usersession text,
	param_attendancedate1 date,
	param_attendancedate2 date)
    RETURNS SETOF attendancelogview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
	
select * from attendancelogview where (cast (attendancedate as date) between param_attendancedate1 and param_attendancedate2 )
and cast (appuserid as int) in 
(select id from userprofileview where rolename='Trainee' and  branchid in(select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid(param_usersession)) and rolename='Accountant'));												 
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewattendancelogstaffbyaccontant(
	param_usersession text,
	param_attendancedate1 date,
	param_attendancedate2 date)
    RETURNS SETOF attendancelogview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
select * from attendancelogview where (cast (attendancedate as date) between param_attendancedate1 and param_attendancedate2 )
and cast (appuserid as int) in 
(select id from userprofileview where rolename<>'Trainee'
 and  branchid in(select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid(param_usersession)) and rolename='Accountant'));												 
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewattendancelog(
	param_attendancelogid varchar)
    RETURNS SETOF attendancelogview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
select * from attendancelogview where attendancelogid =param_attendancelogid;
end;
$BODY$;
  --
  -- Name: batchview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW batchview AS
 SELECT bm.id,
    bm.staffid,
    bm.sessionid,
    c.sessionname,
    bm.startdate AS batchstartdate,
    bm.enddate,
    bm.status AS batchstatus,
    bm.name AS batchname,
    bcm.id AS branchsessionid,
    bcm.branchid,
    bcm.medium,
    bcm.numoftrainees,
    u.firstname,
    u.lastname,
    ms.name as mediumname
   FROM batch bm
     JOIN branchsession bcm ON bm.sessionid = bcm.id
     JOIN sessionmaster c ON c.id = bcm.sessionid
     JOIN staff stfm ON stfm.id = bm.staffid
     JOIN users u ON stfm.userid = u.id
     JOIN mediums ms ON ms.id::character varying::text = bcm.medium::text
  WHERE (bcm.branchid IN ( SELECT stfm_1.branchid
           FROM staff stfm_1
             JOIN users u_1 ON stfm_1.userid = u_1.id
             JOIN branch schbr ON stfm_1.branchid = schbr.id));
           
  CREATE OR REPLACE VIEW traineebatchview AS
 SELECT sba.batchid,
    sba.traineeid,
    sba.branchid AS Organizationbranchid,
    sba.status AS stdbatchstatus,
    sba.startdate AS stdbatchstrtdate,
    b.staffid,
    b.sessionid,
    b.startdate AS batchstartdate,
    b.enddate AS batchenddate,
    b.status AS batchstatus,
    b.name AS batchname,
    s.userid,
    s.branchid,
    s.admissionnum,
    sb.Organizationid,
    sb.startdate,
    sb.status,
    sb.contactdetailsid,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    cd.userid AS contacuid,
    cd.country,
    cd.state,
    cd.city,
    cd.place,
    cd.address,
    cd.uniqueidentificationno,
    cd.email,
    cd.mobile,
    scm.type,
    scm.name,
    scm.status AS schmastatus,
    scm.establishedyear,
    scm.regdate,
    scm.contactdetailsid AS scmcontact,
		stfm.userid as staffuserid
   FROM traineebatch sba
     JOIN batch b ON sba.batchid = b.id
	 join staff stfm on stfm.id=b.staffid
     JOIN admisiondetails s ON sba.traineeid = s.admissionnum
     JOIN users u ON s.userid = u.id
     JOIN branch sb ON s.branchid = sb.id
     JOIN Organization scm ON scm.id = sb.Organizationid
     JOIN contactdetails cd ON cd.id = sb.contactdetailsid
     JOIN branchsession c ON c.id = b.sessionid;
  --
  -- Name: fn_viewbatch(text); Type: FUNCTION; Schema: public; Owner:  --
 SELECT tsf.id,
    tsf.amount,
    tsf.routeid,
    tsf.stopid,
    trm.name AS routename,
    tsm.id AS stopsid,
    trm.startpoint,
    trm.endpoint,
    tsm.stopname,
    tsm.arrivaltime,
    tsm.departuretime,
    b.branchid,
	utp.id as transportid,
		utp.userid,	utp.paidamount,
		utp.paiddate,
		utp.due,
		utp.status as transportstatus
 FROM transportstopfee tsf
     JOIN transportstop tsm ON tsf.stopid = tsm.id
     JOIN transportroute trm ON tsm.routeid = trm.id
     JOIN bus b ON trm.busid = b.id
     JOIN branch sbm ON b.branchid = sbm.id
     JOIN Organization sch ON sbm.Organizationid = sch.id
     JOIN contactdetails cd ON sch.contactdetailsid = cd.id
     JOIN users u ON cd.userid = u.id
	 join usertransportpayment utp on u.id=utp.userid ;
CREATE OR REPLACE FUNCTION fn_viewtransfertransaction(
	param_usersession varchar)
    RETURNS SETOF traineebatchview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
select * from traineebatchview sbv where sbv.batchid in(select batchid from traineebatch 
	where traineeid=(select admissionnum from admisiondetails where userid in(select id from userProfileview
where id=(select fn_viewuserid from fn_viewuserid (param_usersession)))));
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewbatch(param_usersession text) RETURNS SETOF batchview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select * from batchview
    where branchid in(select branchid from userProfileview 
where id=(select fn_viewuserid from fn_viewuserid(param_userSession))) ;
end;
$$;
  --
  -- Name: fn_viewbatchdetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewbatchdetails(param_id integer) RETURNS SETOF batchview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select * from batchview 
    where Id=param_Id;
end;
$$;
CREATE OR REPLACE FUNCTION fn_viewbatchbysession(
    param_sessionid integer)
    RETURNS SETOF batchview
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
    return query
    select * from batchview
    where sessionId=param_sessionId;
end;
$BODY$;
  --
  -- Name: fn_viewbatchmates(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewbatchmates(params_userid integer) RETURNS TABLE(id integer, firstname character varying, middlename character varying, lastname character varying)
    LANGUAGE plpgsql
    AS $$
begin
return query select u.id, u.firstname, u.middlename, u.lastname
from Users u join contactDetails c on u.id=c.userid
where u.id in (select userId from traineebatch
    where batchId=(select batchId from traineebatch
where traineeid=(select admissionnum from admisiondetails
                                                where userId=params_userid)));											
end;
$$;
  --
  -- Name: fn_viewbranch(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewbranchdetails(
	param_Organizationbranchid integer)
    RETURNS SETOF Organizationbranchview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from Organizationbranchview
where branchid =param_OrganizationBranchId;
end;
$BODY$;
 --
-- Name: fn_viewbranch(text); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_viewbranch(param_usersession text)
    RETURNS SETOF Organizationbranchview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from Organizationbranchview
where branchid in(select branchid 
				from userProfileview where 
				id=(select fn_viewuserid from fn_viewuserid(param_usersession))) order by branchname asc ;
end;
$BODY$;
  --
  -- Name: fn_viewbranches(integer, text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewbranches(param_Organizationid integer, param_usersession text) RETURNS SETOF Organizationbranchview
    LANGUAGE plpgsql
    AS $$
begin
return query
select * from Organizationbranchview where OrganizationId = param_Organizationid and OrganizationId in(select id from Organization 
where id in (select Organizationid from userProfileview where id=(select fn_viewuserid from fn_viewuserid(param_usersession)) ));
end;
$$;
CREATE OR REPLACE VIEW busdrvalocationview AS
 SELECT bda.id,
    bda.busid,
    bda.driverid,
    bda.allocationdate,
    bda.status AS busdriverallocationstatus,
    bm.regnum,
    bm.description,
    bm.branchid,
    sbm.Organizationid,
    sbm.startdate,
    sbm.status AS branchstatus,
    sbm.contactdetailsid,
    u.firstname,
    u.lastname,
	cd.place AS branchname
   FROM busdrvalocation bda
     JOIN bus bm ON bda.busid = bm.id
     JOIN branch sbm ON sbm.id = bm.branchid
	 join contactdetails cd on cd.id=sbm.contactdetailsid
     JOIN users u ON bda.driverid = u.id;
  --
  -- Name: fn_viewbusdrvalocation(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewbusdrvalocation(
	param_usersession text)
    RETURNS SETOF busdrvalocationview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select * from busdrvalocationview where branchid in(select branchid from userProfileview 
where rolename!='OrganizationAdmin' and id=
(select fn_viewuserid from fn_viewuserid(param_usersession)))
union all
select * from busdrvalocationview where Organizationid=(select Organizationid from userProfileview 
where rolename='OrganizationAdmin' and id=
(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$BODY$;
  --
  -- Name: fn_viewbusdrvalocationdetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewbusdrvalocationdetails(param_id integer) RETURNS SETOF busdrvalocationview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select * from busdrvalocationview
    where Id=param_Id;
end;
$$;
  --
  -- Name: busview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW busview AS
 SELECT bs.id,
    bs.regnum,
    bs.description,
    bs.branchid,
    bs.latitude,
    bs.longitude,
    sbr.Organizationid,
    sbr.startdate,
    sbr.status,
    sbr.contactdetailsid,
    sch.name AS Organizationname,
    sch.type,
    sch.status AS Organizationstatus,
    sch.establishedyear,
    sch.regdate,
    cd.country,
    cd.state,
    cd.city,
    cd.place,
    cd.address,
    cd.uniqueidentificationno,
    cd.email,
    cd.mobile,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    u.status AS userstatus,
	c.place AS branchname
   FROM bus bs
     JOIN branch sbr ON bs.branchid = sbr.id
	      JOIN contactdetails c ON sbr.contactdetailsid = c.id
     JOIN Organization sch ON sbr.Organizationid = sch.id
     JOIN contactdetails cd ON sch.contactdetailsid = cd.id
     JOIN users u ON cd.userid = u.id;
  --
  -- Name: fn_viewbus(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_getbranches(
    param_Organizationid integer)
    RETURNS SETOF Organizationbranchview
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
return query select * from Organizationbranchview where Organizationid=param_Organizationid;
end;
$BODY$;
ALTER FUNCTION fn_getbranches(integer)
    OWNER TO postgres;
CREATE OR REPLACE FUNCTION fn_viewbus(
	param_usersession text)
    RETURNS SETOF busview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select * from busview
    where branchid in(select branchid from userProfileview 
where rolename!='OrganizationAdmin' and id=(select fn_viewuserid from fn_viewuserid(param_usersession)))
union all
    select * from busview
																		  
 where Organizationid=(select Organizationid from userProfileview 
where rolename='OrganizationAdmin' and id=(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$BODY$;
  -- Name: fn_viewbusdetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewbusdetails(param_id integer) RETURNS SETOF busview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select * from busview
    where Id=param_Id;
end;
$$;
  --
  -- Name: sessionview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW sessionview AS
 SELECT sessionmaster.id,
    sessionmaster.sessionname
   FROM sessionmaster;
  --
  -- Name: fn_viewsessiondetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewsessiondetails(param_sessionid integer) RETURNS SETOF sessionview
    LANGUAGE plpgsql
    AS $$
begin
    return query
select * from sessionview
where Id=param_sessionId;
end;
$$;
  --
  -- Name: fn_viewsessiones(); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewsessiones() RETURNS SETOF sessionview
    LANGUAGE plpgsql
    AS $$
begin
return query
select * from sessionview where sessionname !='dummy' order by sessionname asc;
end;
$$;
  --
  -- Name: sessionsubjectview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW sessionsubjectview AS
 SELECT cs.id,
    cs.subjectid,
    cs.syllabus,
    cs.batchid,
    fsm.staffid,
    sm.name AS subjectname,
    bcm.id AS branchsessionid,
	bm.name as batchname,
    bcm.sessionid,
    bcm.medium,
    bcm.numoftrainees,
    cm.sessionname,
    bcm.branchid,
    stfm.userid
   FROM sessionsubject cs
     JOIN trainersubject fsm ON cs.subjectid = fsm.id
     JOIN staff stfm ON stfm.id = fsm.staffid
     JOIN subjectmaster sm ON fsm.subjectid = sm.id
     JOIN batch bm ON bm.id = cs.batchid
     JOIN branchsession bcm ON bcm.id = bm.sessionid
     JOIN sessionmaster cm ON cm.id = bcm.sessionid
     JOIN branch scbm ON scbm.id = bcm.branchid
     JOIN users u ON u.id = stfm.userid;
	 
	 
  --
  -- Name: fn_viewsessionsubject(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewsessionsubject(
	param_usersession text)
    RETURNS SETOF sessionsubjectview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select * from sessionsubjectview where  branchid in(select branchid from userProfileview 
where rolename='BranchAdmin' and  id=(select fn_viewuserid from fn_viewuserid(param_usersession)))
union
select * from sessionsubjectview where userid=(select userid from userProfileview 
where  id=(select fn_viewuserid from fn_viewuserid(param_usersession))) and branchid in(select branchid from userProfileview 
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)))
union
select * from sessionsubjectview where batchid in(select batchid from traineebatch 
	where traineeid in(select admissionnum from admisiondetails where userid 
	in (select userid from userProfileview 
where  id in(select fn_viewuserid from fn_viewuserid(param_usersession))))) 
  and branchid in(select branchid from userProfileview 
where id in(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$BODY$;
  --
  -- Name: fn_viewsessionsubjectdetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewsessionsubjectdetails(param_id integer) RETURNS SETOF sessionsubjectview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select * from sessionsubjectview
    where Id=param_Id;
end;
$$;
  --
  -- Name: fn_viewclassteachers(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewsessionteachers(param_usersession text) RETURNS TABLE(id integer, userid integer, firstname character varying, lastname character varying)
    LANGUAGE plpgsql
    AS $$
begin
 return query select s.id,u.id,u.firstName,u.lastName from staff s join Users u on u.id=s.userId 
 where s.userroleId =(select r.id from rolemaster r where rolename='SessionTeacher') and s.branchid in(select branchid from userProfileview as upv
where upv.id=(select fn_viewuserid from fn_viewuserid(param_usersession))) order by u.firstname asc;
end;
$$;
create or replace function fn_ViewAllStaffclassteacheres(param_usersession text)
returns table(id int,firstname varchar,lastname varchar) as
$$
begin
return query
select sm.id,u.firstname,u.lastname from users u join staff as sm on sm.userid=u.id where sm.id in
(select s.id as staffId from
staff s join Users u on u.id=s.userId
where s.userroleId =(select r.id from rolemaster r where rolename='ClassTeacher')
 and s.branchid in(select branchid from userProfileview as upv
where upv.id=(select fn_viewuserid from fn_viewuserid(param_usersession)))
except
select b.staffid as staffid from batchview as b
where b.branchid in(select branchid from userProfileview as upv
where upv.id=(select fn_viewuserid from fn_viewuserid(param_usersession))));
end;
$$ language plpgsql;
  --
  -- Name: concesionview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW concesionview AS
 SELECT cm.id,
    cm.branchid,
    cm.concessionname,
    cm.details,
    cm.concession,
    cm.creationdate,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    scbm.Organizationid,
    scbm.startdate,
    scbm.status,
    scbm.contactdetailsid,
    scm.name,
    scm.type,
    scm.status AS orgstatus,
    scm.establishedyear,
    scm.regdate,
    cd.userid,
    cd.country,
    cd.state,
    cd.city,
    cd.place,
    cd.address,
    cd.uniqueidentificationno,
    cd.email,
    cd.mobile
   FROM ((((concession cm
     JOIN branch scbm ON ((cm.branchid = scbm.id)))
     JOIN Organization scm ON ((scbm.Organizationid = scm.id)))
     JOIN contactdetails cd ON ((scbm.contactdetailsid = cd.id)))
     JOIN users u ON ((cd.userid = u.id)));
  --
  -- Name: fn_viewconcession(text); Type: FUNCTION; Schema: public; Owner:  --
  --
  CREATE OR REPLACE FUNCTION fn_viewconcession(param_usersession text) RETURNS SETOF concesionview
    LANGUAGE plpgsql
    AS $$
begin
return query  select * from concesionview where branchid in(select branchid from userProfileview
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)))  order by concessionname asc;
end;
$$;
  --
  -- Name: fn_viewconcessiondetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewconcessiondetails(param_id integer) RETURNS SETOF concesionview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select * from concesionview
    where Id=param_Id;
end;
$$;
  --
  -- Name: contactdetailsview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW contactdetailsview AS
 SELECT c.id,
    c.userid,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    u.status,
    c.country,
    c.state,
    c.city,
    c.place,
    c.address,
    c.uniqueidentificationno,
    c.email,
    c.mobile
   FROM (contactdetails c
     JOIN users u ON ((c.userid = u.id)));
  --
  -- Name: fn_viewcontactdetails(); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewcontactdetails(
    params_userSession text)
    RETURNS SETOF contactdetailsview
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
return query
select * from contactdetailsview
where userId=(select fn_viewUserId from fn_viewUserId(params_userSession));
end;
$BODY$;
  --
  -- Name: fn_viewcontactdetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewcontactdetails(params_userid integer) RETURNS SETOF contactdetailsview
    LANGUAGE plpgsql
    AS $$
begin
return query
select * from contactdetailsview
where userId=params_userId;
end;
$$;
  --
  -- Name: fn_viewcontactdetailsbyrole(text, integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewcontactdetailsbyrole(
    param_rolename text,
    param_usersession text)
    RETURNS SETOF loginview
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
 return query select * from loginview where loginview.contactid in
(select  loginview.contactid as contact from loginview
where loginview.id in (select applicantid from applicants where createdby
                =(select fn_viewuserid from fn_viewuserid(param_usersession)))
and roleId= (select rl.id from rolemaster as rl where rl.roleName=param_rolename)
except
select contactdetailsid as contact from branch    where contactdetailsid not in(0));
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewcontactdetailsbyrolebyid(
    param_rolename text,
    param_usersession text)
    RETURNS SETOF loginview
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
 return query select * from loginview where loginview.contactid in
(select  loginview.contactid as contact from loginview
where loginview.id in (select applicantid from applicants where createdby
                =(select fn_viewuserid from fn_viewuserid(param_usersession)))
and roleId= (select rl.id from rolemaster as rl where rl.roleName=param_rolename));
end;
$BODY$;
  --
  -- Name: fn_viewcontacts(); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewcontacts() RETURNS TABLE(id integer, userid integer, firstname character varying, middlename character varying, lastname character varying, dob date, gender character varying, fathername character varying, mothername character varying, fatheroccupation character varying, caste character varying, subcaste character varying, religion character varying, nationality character varying, status character varying, country character varying, state character varying, city character varying, place character varying, address character varying, uniqueidentificationno character varying, email character varying, mobile character varying)
    LANGUAGE plpgsql
    AS $$
begin
 return query select    c.id,c.userid,
                        u.Firstname,u.Middlename,u.Lastname,u.DOB,u.Gender,u.FatherName,u.MotherName,
						u.FatherOccupation,u.Caste,u.SubCaste,u.Religion,u.Nationality,u.Status,
						c.Country ,c.State,c.City,
	                    c.Place ,c.Address ,c.UniqueIdentificationNo ,
	                    c.Email ,c.Mobile
						
						from contactdetails as c
						join users as u
						on c.userid=u.id
						join Organization as s
						on s.contactdetailsid=c.id
						join branch as sb
						on sb.Organizationid=s.id
						where s.Id=param_BranchId;
end;
$$;
  --
  -- Name: fn_viewcontacts(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewcontacts(param_branchid integer) RETURNS TABLE(id integer, userid integer, firstname character varying, middlename character varying, lastname character varying, dob date, gender character varying, fathername character varying, mothername character varying, fatheroccupation character varying, caste character varying, subcaste character varying, religion character varying, nationality character varying, status character varying, country character varying, state character varying, city character varying, place character varying, address character varying, uniqueidentificationno character varying, email character varying, mobile character varying)
    LANGUAGE plpgsql
    AS $$
begin
 return query select    c.id,c.userid,
                        u.Firstname,u.Middlename,u.Lastname,u.DOB,u.Gender,u.FatherName,u.MotherName,
						u.FatherOccupation,u.Caste,u.SubCaste,u.Religion,u.Nationality,u.Status,
						c.Country ,c.State,c.City,
	                    c.Place ,c.Address ,c.UniqueIdentificationNo ,
	                    c.Email ,c.Mobile
						
						from contactdetails as c
						join users as u
						on c.userid=u.id
						join Organization as s
						on s.contactdetailsid=c.id
						join branch as sb
						on sb.Organizationid=s.id
						where s.Id=param_BranchId;
end;
$$;
  -- Name: appraisaltimetableview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW appraisaltimetableview AS
 SELECT ett.id,
    ett.appraisalid,
    ett.sessionsubjectid,
    ett.appraisalstartdatetime,
    ett.appraisalenddatetime,
    ett.appraisalname,
    ett.status AS appraisalstatus,
    csm.subjectid,
    csm.syllabus,
    csm.batchid,
    sbm.id AS branchid,
    sbm.Organizationid,
    sbm.startdate,
    sbm.status AS sbmstatus,
    sbm.contactdetailsid,
    s.name AS subjectname,
	ett.totalmarks
   FROM appraisaltimetable ett
     JOIN sessionsubject csm ON ett.sessionsubjectid = csm.id
     JOIN trainersubject fs ON csm.subjectid = fs.id
     JOIN subjectmaster s ON s.id = fs.subjectid
     JOIN batch bm ON csm.batchid = bm.id
     JOIN branchsession bcm ON bm.sessionid = bcm.id
     JOIN branch sbm ON sbm.id = bcm.branchid;
  --
  -- Name: fn_viewappraisaltimetabledetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewappraisaltimetabledetails(param_id integer) RETURNS SETOF appraisaltimetableview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select * from appraisaltimetableview
    where Id=param_Id;
end;
$$;
CREATE OR REPLACE FUNCTION fn_viewappraisaltimetables(param_usersession text) RETURNS SETOF appraisaltimetableview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select * from appraisaltimetableview where branchid in(select branchid from userProfileview 
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$$;
	
CREATE OR REPLACE FUNCTION fn_viewappraisaltimetableByBatchId(
	param_batchid integer)
    RETURNS TABLE(appraisalid int,appraisalname character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
 select etv.appraisalid, etv.appraisalname from appraisaltimetableview etv where etv.batchid=param_batchid  
 group by(etv.appraisalname,etv.appraisalid);
 end;		  
$BODY$;
		
  --
  -- Name: trainersubjectview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW trainersubjectview AS
 SELECT fs.id,
    fs.subjectid,
    fs.staffid,
    stfm.userid,
    stfm.branchid,
    stfm.doj,
    stfm.experience,
    stfm.salary,
    stfm.userroleid,
    subm.name,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    u.status AS userstatus,
    schbr.Organizationid,
    schbr.startdate AS branchstartdate,
    schbr.status AS branchstatus,
    schbr.contactdetailsid,
    schm.name AS Organizationname,
    schm.type,
    schm.status AS Organizationstatus,
    schm.establishedyear,
    schm.regdate,
    cd.country,
    cd.state,
    cd.city,
    cd.place,
    cd.address,
    cd.uniqueidentificationno,
    cd.email,
    cd.mobile,
    rm.rolename,
    rm.creationtime,
    rm.modifiedtime,
    rm.status AS rolestatus
   FROM (((((((trainersubject fs
     JOIN subjectmaster subm ON ((fs.subjectid = subm.id)))
     JOIN staff stfm ON ((fs.staffid = stfm.id)))
     JOIN users u ON ((stfm.userid = u.id)))
     JOIN branch schbr ON ((stfm.branchid = schbr.id)))
     JOIN Organization schm ON ((schm.id = schbr.Organizationid)))
     JOIN contactdetails cd ON ((schm.contactdetailsid = cd.id)))
     JOIN rolemaster rm ON ((stfm.userroleid = rm.id)));
  --
  -- Name: fn_viewtrainersubject(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewtrainersubject(param_usersession text) RETURNS SETOF trainersubjectview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select * from trainersubjectview
    where branchid in(select branchid from userProfileview 
where id=(select fn_viewuserid from fn_viewuserid(param_userSession)));
end;
$$;
  --
  -- Name: fn_viewtrainersubjectdetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewtrainersubjectdetails(
	params_Id integer)
    RETURNS SETOF trainersubjectview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select * from trainersubjectview
    where Id=params_Id;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewtrainersubjectbysubject(
	param_subjectid integer,param_usersession text)
    RETURNS SETOF trainersubjectview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from trainersubjectview
where subjectId=param_subjectid and branchid in(select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid(param_usersession))) order by firstname asc;
end;
$BODY$;
  -- Name: feeview; Type: VIEW; Schema: public; Owner:  --
  --
 CREATE OR REPLACE VIEW feeview AS
 SELECT fm.id,
    fm.feename,
    fm.branchid,
    fm.branchsessionid,
    fm.feeamount,
    bcm.sessionid,
    bcm.medium,
    cm.sessionname,
    sbm.Organizationid,
    sbm.startdate,
    sbm.status,
    sbm.contactdetailsid,
    sm.name,
    sm.type,
    sm.status AS Organizationstatus,
    sm.establishedyear,
    sm.regdate,
    c.userid,
    c.country,
    c.state,
    c.city,
    c.place,
    c.address,
    c.uniqueidentificationno,
    c.email,
    c.mobile,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    u.status AS userstatus
   FROM ((((((fees fm
     JOIN branchsession bcm ON ((fm.branchsessionid = bcm.id)))
     JOIN branch sbm ON ((bcm.branchid = sbm.id)))
     JOIN Organization sm ON ((sbm.Organizationid = sm.id)))
     JOIN contactdetails c ON ((sm.contactdetailsid = c.id)))
     JOIN users u ON ((c.userid = u.id)))
     JOIN sessionmaster cm ON ((bcm.sessionid = cm.id)));
  --
  -- Name: fn_viewfee(text); Type: FUNCTION; Schema: public; Owner:  --
  --
  CREATE OR REPLACE FUNCTION fn_viewfee(param_usersession text) RETURNS SETOF feeview
    LANGUAGE plpgsql
    AS $$
begin
return query
select *from feeview
where branchid in(select branchid from userProfileview
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)))  order by feename asc ;
end;
$$;
  --
  -- Name: fn_viewfeedetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewfeedetails(param_feesid integer) RETURNS SETOF feeview
    LANGUAGE plpgsql
    AS $$
begin
return query
select * from feeview
where Id=param_FeesId;
end;
$$;
CREATE OR REPLACE FUNCTION fn_viewfeedetailsforaccountant(
	
	param_usersession text)
    RETURNS TABLE (id int,firstname varchar,lastname varchar)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query														   
select u.id,u.firstname,u.lastname from users u where u.id in(select userid from admisiondetails where branchid in(select upv.branchid from userprofileview upv where upv.id=(select fn_viewuserid from fn_viewuserid(param_usersession))));
end;
$BODY$;
CREATE OR REPLACE VIEW feepaymentsview AS
 SELECT f.id,
    f.paidamount,
    f.paydate,
    f.due,
    f.paymode,
    f.status,
    f.traineeid,
    a.admissionnum,
    a.parentemail,
    a.parentmobile,
    a.branchid,
    a.concessionid,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality
   FROM ((feepayments f
     JOIN users u ON ((f.traineeid = u.id)))
     JOIN admisiondetails a ON ((a.userid = u.id)));
CREATE OR REPLACE VIEW feepaymentsbybatchview AS
 SELECT f.id,
    f.paidamount,
    f.paydate,
    f.due,
    f.paymode,
    f.status,
    f.traineeid,
    ad.admissionnum,
    ad.parentemail,
    ad.parentmobile,
    ad.branchid,
    ad.concessionid,
    u.firstname,
    u.middlename,
    u.lastname,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    bm.id AS batchid,
    bm.name AS batchname,
	fm.feeamount,
	cm.concession
   FROM feepayments f
     JOIN admisiondetails ad ON f.traineeid = ad.userid
	 join fees fm on fm.id=ad.feeid
	 join concession cm on cm.id=ad.concessionid
     JOIN traineebatch sbm ON sbm.traineeid = ad.admissionnum
     JOIN batch bm ON bm.id = sbm.batchid
     JOIN users u ON u.id = ad.userid;
CREATE OR REPLACE FUNCTION fn_viewfeepaymentsbybatch(
	param_batchid integer,params_usersession text)
    RETURNS SETOF feepaymentsbybatchview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select * from feepaymentsByBatchview where BatchId=param_batchid and
	branchid in (select branchid from userProfileview 
where rolename!='Trainee' and id=(select fn_viewuserid from fn_viewuserid(params_usersession)))
union
select * from feepaymentsByBatchview where BatchId=param_batchid and traineeid in (select userid from userProfileview 
where id=(select fn_viewuserid from fn_viewuserid(params_usersession))) and branchid in(select branchid from userProfileview 
where  id=(select fn_viewuserid from fn_viewuserid(params_usersession)));
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewfeepaymentsbyId(
	param_feepaymentsid int)
    RETURNS SETOF feepaymentsbybatchview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select * from feepaymentsByBatchview where batchname!='dummy' and id=param_feepaymentsid;
end;
$BODY$;
  --
  -- Name: fn_viewfeepayments(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewfeepayments(param_branchid integer) RETURNS SETOF feepaymentsview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select * from feepaymentsview where BranchId=param_BranchId;
end;
$$;
  --
  -- Name: fn_viewfeepayments(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewfeepayments(param_usersession text) RETURNS SETOF feepaymentsview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select * from feepaymentsview where branchid in(select branchid from userProfileview 
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$$;
CREATE OR REPLACE FUNCTION fn_viewadmisiondetailsByuserId(
	param_userId integer)
    RETURNS SETOF admisionview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select * from admisionview where userid=param_userId;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewfeepaymentsbyuserid(
	param_userid integer)
    RETURNS TABLE (paidamnt double precision)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select coalesce(sum(paidamount),0) from feepaymentsview where traineeid=param_userid;
end;
$BODY$;
  -- Name: galleryview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW galleryview AS
 SELECT gm.id,
    gm.branchid,
    gm.description,
    gm.gallerydate,
    schbr.Organizationid,
    schbr.startdate,
    schbr.status AS branchstatus,
    schbr.contactdetailsid,
    sm.name,
    sm.type,
    sm.status AS Organizationstatus,
    sm.establishedyear,
    sm.regdate,
    cd.userid,
    cd.country,
    cd.state,
    cd.city,
    cd.place,
    cd.address,
    cd.uniqueidentificationno,
    cd.email,
    cd.mobile,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    u.status AS userstatus
   FROM ((((gallery gm
     JOIN branch schbr ON ((gm.branchid = schbr.id)))
     JOIN Organization sm ON ((schbr.Organizationid = sm.id)))
     JOIN contactdetails cd ON ((schbr.contactdetailsid = cd.id)))
     JOIN users u ON ((cd.userid = u.id)));
  --
  -- Name: fn_viewgallery(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewgallery(
    param_usersession text)
    RETURNS SETOF galleryview
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
    return query
    select * from galleryview
    where branchid in(select branchid from userProfileview
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$BODY$;
  --
  -- Name: fn_viewgallerydetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewgallerydetails(param_id integer) RETURNS SETOF galleryview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select * from galleryview
    where Id=param_Id;
end;
$$;
  --
  -- Name: holidayview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW holidayview AS
 SELECT holiday.id,
    holiday.branchid,
    holiday.holidaydate,
    holiday.holidayenddate,
    holiday.occation,
    sbr.Organizationid,
    sbr.startdate,
    sbr.status,
    sbr.contactdetailsid,
    sch.name AS Organizationname,
    sch.type,
    sch.status AS Organizationstatus,
    sch.establishedyear,
    sch.regdate,
    cd.country,
    cd.state,
    cd.city,
    cd.place,
    cd.address,
    cd.uniqueidentificationno,
    cd.email,
    cd.mobile,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    u.status AS userstatus,
	c.place AS branchname
   FROM holiday holiday
     JOIN branch sbr ON holiday.branchid = sbr.id
	 join contactdetails c on sbr.contactdetailsid=c.id
     JOIN Organization sch ON sbr.Organizationid = sch.id
     JOIN contactdetails cd ON sch.contactdetailsid = cd.id
     JOIN users u ON cd.userid = u.id;
  --
CREATE OR REPLACE FUNCTION fn_viewholidaymaster(
	param_usersession text)
    RETURNS SETOF holidayview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select * from holidayview
    where BranchId=(select branchid from userProfileview 
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)))
union all												  
 select * from holidayview
    where organizationid=(select organizationid from userProfileview 
where rolename='OrganizationAdmin' and id=(select fn_viewuserid from fn_viewuserid(param_usersession)))												  
												  ;
end;
$BODY$;
	CREATE OR REPLACE FUNCTION fn_viewholidaymasterdetails(param_id integer) RETURNS SETOF holidayview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select * from holidayview
    where Id=param_Id;
end;
$$;
  -- Name: fn_viewholiday(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewholiday(
	param_usersession text)
    RETURNS SETOF holidayview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select * from holidayview where holidaydate > CAST(CURRENT_TIMESTAMP AS DATE)
    and branchid in(select branchid from userProfileview 
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)))
union all												  
 select * from holidayview
    where Organizationid=(select Organizationid from userProfileview 
where rolename='OrganizationAdmin' and id=(select fn_viewuserid from fn_viewuserid(param_usersession)))												  
												  ;
end;
$BODY$;
create or replace function fn_deleteHoliaday(param_id integer)
returns integer
as
$$
declare n integer;
begin
delete from holiday where Id=param_id
returning Id into n;
return n;
end
$$
language plpgsql;
  --
  -- Name: fn_viewholidaydetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewholidaydetails(param_id integer) RETURNS SETOF holidayview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select * from holidayview
    where Id=param_Id;
end;
$$;
  -- Name: leavedetailsview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW leavedetailsview AS
 SELECT l.userid,
    l.fromdate,
    l.todate,
    l.status AS leavestatus,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    s.branchid
   FROM ((leavedetails l
     JOIN users u ON ((l.userid = u.id)))
     JOIN staff s ON ((s.userid = u.id)));
  --
  -- Name: fn_viewleaves(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewleaves(param_usersession text) RETURNS SETOF leavedetailsview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select *
    from leavedetailsview
    where branchid in(select branchid from userProfileview 
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$$;
  --
  -- Name: fn_viewlogin(character varying); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewlogin(param_loginid character varying) RETURNS SETOF loginview
    LANGUAGE plpgsql
    AS $$
begin
 return query 
select * from loginview where loginId=param_loginId;
 end;
$$;
  --
  -- Name: roleview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW roleview AS
 SELECT rolemaster.id,
    rolemaster.rolename,
    rolemaster.creationtime,
    rolemaster.modifiedtime,
    rolemaster.status
   FROM rolemaster;
  --
  -- Name: fn_viewrole(); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewrole(
    )
    RETURNS SETOF roleview
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
    return query
    select * from roleview
     where rolename
not in ('IBAdmin','SuperAdmin','OrganizationAdmin','Dummy','Default','Trainee')  order by rolename asc;
end;
$BODY$;
  --
  -- Name: fn_viewroledetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewroledetails(param_id integer) RETURNS SETOF roleview
    LANGUAGE plpgsql
    AS $$
begin
return query
select * from roleview
where Id=param_Id;
end;
$$;
  --
  -- Name: fn_viewroledetailsofuser(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewroledetailsofuser(param_usersession text) RETURNS SETOF roleview
    LANGUAGE plpgsql
    AS $$
begin
return query
select * from roleview
where Id=(select roleid from userProfileview 
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$$;
  --
  -- Name: salaryview; Type: VIEW; Schema: public; Owner:  --
CREATE OR REPLACE VIEW salaryview AS
 SELECT s.id,
    s.staffid,
    s.salary,
    s.splallowances,
    sm.branchid,
    u.firstname,
    u.middlename,
    u.lastname,
    u.gender,
    sbm.Organizationid,
    sbm.startdate,
    sbm.status AS scbstau,
    sbm.contactdetailsid,
    scm.name,
    scm.type,
    scm.status,
    scm.establishedyear,
    scm.regdate,
    scm.contactdetailsid AS scmcontact,
    spm.patternname,
    cd.place AS branchname,
	sm.userid
   FROM salary s
     JOIN staff sm ON s.staffid = sm.id
     JOIN branch sbm ON sm.branchid = sbm.id
     JOIN contactdetails cd ON cd.id = sbm.contactdetailsid
     JOIN users u ON sm.userid = u.id
     JOIN Organization scm ON sbm.Organizationid = scm.id
     JOIN salarypattern spm ON s.splallowances = spm.id;
  --
  -- Name: fn_viewsalary(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewsalary(
	param_usersession text)
    RETURNS SETOF salaryview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query select  * from salaryview
where branchid in(select branchid from userProfileview 
where rolename!='OrganizationAdmin' and  id=(select fn_viewuserid from fn_viewuserid(param_usersession)))
union all																		  
 select  * from salaryview
where Organizationid=(select Organizationid from userProfileview 
where rolename='OrganizationAdmin' and id=(select fn_viewuserid from fn_viewuserid(param_usersession)));
																		  
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewsalarytransactions(
	param_usersession varchar)
    RETURNS SETOF salaryview  
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
	select * from salaryview where staffid=(select id from staff where userid=(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$BODY$;
  --
  -- Name: fn_viewsalarybystaff(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewsalarybystaff(
    param_staffid integer,
    param_year integer,
    param_month integer)
    RETURNS TABLE(grosssalary double precision, firstname character varying, lastname character varying, salary double precision, allowances double precision, deductions double precision, bonus double precision, patternname text, salaryid integer)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
declare n int;
declare m int;
declare bonus double precision;
declare patternname text;
begin
select sum(sat.percentage) from staff sm join salary sl on sm.salary=sl.id
join salaryallowancestable sat on sl.splallowances=sat.patternid where sat.type='Allowance' and sm.id=param_staffid into n;
select sum(sat.percentage) from staff sm join salary sl on sm.salary=sl.id
join salaryallowancestable sat on sl.splallowances=sat.patternid where sat.type='Deduction' and sm.id=param_staffid into m;
select coalesce(sum(increment),0) from salaryhike where salarytype='BONUS' and staffid=param_staffid and date_part('month',hikedate)=param_month and date_part('year',hikedate)=param_year into bonus;
select spm.patternname from salarypattern spm where id=(select DISTINCT  splallowances from salary where staffid=param_staffid)  into patternname;
return query
select (p.grosssalary+bonus),u.firstname,u.lastname,sl.salary,(p.grosssalary*n)/100 as allowances,(p.grosssalary*m)/100 as deductions,bonus,patternname,sl.id as salaryid from payslip p
join users u on u.id=p.userid
join staff sm on sm.userid=u.id
join salary sl on sl.id=sm.salary where sm.id=param_staffid and date_part('month',cast(p.month as date))=param_month and date_part('year',cast(p.month as date))=param_year;
end;
$BODY$;
  --
  -- Name: fn_viewsalarydetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewsalarydetails(param_id integer) RETURNS SETOF salaryview
    LANGUAGE plpgsql
    AS $$
begin
return query
select  * from  salaryview
where Id=param_Id;
end;
$$;
  -- Name: salaryhikeview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW salaryhikeview AS
 SELECT sh.id,
    sh.staffid,
    sh.hikedate,
    sh.increment,
    u.firstname,
    u.middlename,
    u.lastname,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    rm.rolename,
    sbm.Organizationid,
    sbm.startdate,
    sbm.status,
    sbm.contactdetailsid,
    cd.userid,
    cd.country,
    cd.state,
    cd.city,
    cd.place,
    cd.address,
    cd.uniqueidentificationno,
    cd.mobile,
    cd.email,
    scm.name,
    scm.type,
    scm.status AS schmst,
    scm.establishedyear,
    scm.regdate,
    sm.branchid,
    sh.hikemailstatus
   FROM salaryhike sh
     JOIN staff sm ON sh.staffid = sm.id
     JOIN branch sbm ON sm.branchid = sbm.id
     JOIN users u ON sm.userid = u.id
     JOIN rolemaster rm ON sm.userroleid = rm.id
     JOIN Organization scm ON sbm.Organizationid = scm.id
     JOIN contactdetails cd ON u.id= cd.userid;
  --
  -- Name: fn_viewsalaryhike(text); Type: FUNCTION; Schema: public; Owner:  --
  --
  												   
CREATE OR REPLACE FUNCTION fn_viewsalaryhiketransactions(
	param_usersession varchar)
    RETURNS SETOF salaryhikeview  
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
	select * from salaryhikeview where staffid=(select id from staff where userid=(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewsalaryhike(
    param_usersession text)
    RETURNS SETOF salaryhikeview
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
return query
select * from salaryhikeview
where branchId in(select branchid from userProfileview
where (rolename='BranchAdmin' or rolename='Accountant' or rolename='OrganizationAdmin')
and id in(select fn_viewuserid from fn_viewuserid(param_usersession)))
union
select * from salaryhikeview
where staffid in(select id from staff where userid in((select userid from userProfileview
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)))))
and branchid in(select branchid from userProfileview
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)))    ;
end;
$BODY$;
  --
  -- Name: fn_viewsalaryhikedetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewsalaryhikedetails(param_id integer) RETURNS SETOF salaryhikeview
    LANGUAGE plpgsql
    AS $$
begin
return query  select * from salaryhikeview
where Id=param_Id;
end;
$$;
  --
  -- Name: salarypaymentview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW salarypaymentview AS
 SELECT sp.id,
    sp.stafid,
    sp.paidamount,
    sp.paydate,
    sp.payable,
    sp.allowances,
    sp.deductions,
    sp.bonus,
    sp.payalwdeddetails,
    sp.remarks,
    s.userid,
    s.branchid,
    s.doj,
    s.experience,
    s.userroleid,
    r.rolename,
    sm.salary,
    sm.splallowances,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    sbm.Organizationid,
    sbm.startdate,
    sbm.contactdetailsid,
    cd.country,
    cd.state,
    cd.city,
    cd.place,
    cd.address,
    cd.uniqueidentificationno,
    cd.email,
    cd.mobile
   FROM salarypayment sp
     JOIN staff s ON sp.stafid = s.id
     JOIN salary sm ON sm.id= s.salary
     JOIN users u ON u.id = s.userid
     JOIN rolemaster r ON r.id = s.userroleid
     JOIN branch sbm ON s.branchid = sbm.id
     JOIN contactdetails cd ON cd.id = sbm.contactdetailsid;
  --
  -- Name: fn_viewsalarypayment(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewsalarypayment(
    param_usersession text)
    RETURNS SETOF salarypaymentview
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
    return query
    select *
    from salarypaymentview
    where branchid in(select branchid from userProfileview
where (rolename='BranchAdmin' or rolename='Accountant' or rolename='OrganizationAdmin') and
                    id=(select fn_viewuserid from fn_viewuserid(param_usersession)))
union
 select * from salarypaymentview
     where stafid in(select id from staff where userid in((select userid from userProfileview
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)))))
and branchid in(select branchid from userProfileview
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$BODY$;
  --
  -- Name: fn_viewsalarypaymentdetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewsalarypaymentdetails(param_id integer) RETURNS SETOF salarypaymentview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select *
    from salarypaymentview
    where Id=param_Id;
end;
$$;
  --
  -- Name: Organizationview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW Organizationview AS
 SELECT sm.id,
    sm.name,
    sm.type,
    sm.status AS Organizationstatus,
    sm.establishedyear,
    sm.regdate,
    c.userid,
    c.country,
    c.state,
    c.city,
    c.place,
    c.address,
    c.uniqueidentificationno,
    c.email,
    c.mobile,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    u.status AS userstatus,
    it.name as institutetypename
  FROM Organization sm
   join institutetype it on it.id=cast(sm.type as int)
     JOIN contactdetails c ON sm.contactdetailsid = c.id
     JOIN users u ON c.userid = u.id;
  --
  -- Name: fn_viewOrganization(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewOrganization(param_Organizationid integer) RETURNS SETOF Organizationview
    LANGUAGE plpgsql
    AS $$
begin
return query
select * from Organizationview where id=param_OrganizationId;
end;
$$;
  --
  -- Name: branchsessionview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW branchsessionview AS
 SELECT bcm.id,
    bcm.sessionid,
    bcm.medium,
    bcm.numoftrainees,
    cm.sessionname,
    sbm.Organizationid,
    sbm.startdate,
    sbm.status,
    sbm.id AS Organizationbranch,
    sbm.contactdetailsid,
    sm.name,
    sm.type,
    sm.status AS Organizationstatus,
    sm.establishedyear,
    sm.regdate,
    c.userid,
    c.country,
    c.state,
    c.city,
    c.place,
    c.address,
    c.uniqueidentificationno,
    c.email,
    c.mobile,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    u.status AS userstatus,
    ms.name AS mediumname,
    bcm.departmentid
   FROM branchsession bcm
     JOIN branch sbm ON bcm.branchid = sbm.id
     JOIN Organization sm ON sbm.Organizationid = sm.id
     JOIN contactdetails c ON sm.contactdetailsid = c.id
     JOIN users u ON c.userid = u.id
     JOIN sessionmaster cm ON bcm.sessionid = cm.id
     JOIN mediums ms ON ms.id::character varying::text = bcm.medium::text;
  --
  -- Name: fn_viewOrganizationsession(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewOrganizationsession(param_usersession text) RETURNS SETOF branchsessionview
    LANGUAGE plpgsql
    AS $$
begin
return query
select * from branchsessionview
where Organizationbranch=(select branchid from userProfileview where id=(select fn_viewuserid from fn_viewuserid(param_usersession))) order by sessionname asc;
end;
$$;
  --
  -- Name: fn_viewOrganizationsessiondetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewOrganizationsessiondetails(param_branchsessionid integer) RETURNS SETOF branchsessionview
    LANGUAGE plpgsql
    AS $$
begin
return query
select * from branchsessionview
where id=param_BranchsessionId;
end;
$$;
  --
  -- Name: fn_viewOrganizations(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewOrganizations(param_usersession text) RETURNS SETOF Organizationview
    LANGUAGE plpgsql
    AS $$
begin
return query
select * from Organizationview where id =(select id from Organization where contactdetailsid in (select id from contactdetails where userid=(select fn_viewuserid from fn_viewuserid(param_usersession))));
end;
$$;
  --
  -- Name: staffview; Type: VIEW; Schema: public; Owner:  --
  --
  CREATE OR REPLACE VIEW public.staffview AS
 SELECT s.id,
    s.userid,
    s.branchid,
    s.doj,
    s.experience,
    s.salary,
    s.userroleid,
    salm.salary AS realsalary,
    sb.organizationid,
    sb.startdate,
    sb.status,
    sb.contactdetailsid,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    r.rolename,
    cd.userid AS contacuid,
    cd.country,
    cd.state,
    cd.city,
    cd.place,
    cd.address,
    cd.uniqueidentificationno,
    cd.email,
    cd.mobile,
    scm.type,
    scm.name,
    scm.status AS schmastatus,
    scm.establishedyear,
    scm.regdate,
    scm.contactdetailsid AS scmcontact,
    c.place AS branchname,
    ac.bankname,
    ac.staffid AS bankstaffid,
    ac.accountnumber,
    ac.pannumber,
    ac.pfnumber,
    sp.id AS patternid,
    sp.patternname,
    s.departmentid,
    s.otpay,
    d.name AS departmentname,
    sh.shiftname,sh.id as shiftid
 FROM accountdetails ac
     JOIN staff s ON ac.staffid = s.id
     JOIN department d ON d.id = s.departmentid
     JOIN shifts sh ON s.shiftid = sh.id
     JOIN salary salm ON salm.id::double precision = s.salary
     JOIN salarypattern sp ON sp.id = salm.splallowances
     JOIN branch sb ON s.branchid = sb.id
     JOIN contactdetails c ON sb.contactdetailsid = c.id
     JOIN users u ON s.userid = u.id
     JOIN rolemaster r ON s.userroleid = r.id
     JOIN organization scm ON sb.organizationid = scm.id
     JOIN contactdetails cd ON scm.contactdetailsid = cd.id;

  -- Name: fn_viewstaff(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewstaff(
    param_usersession text)
    RETURNS SETOF staffview
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
 return query
 select * from staffview where branchId =
 (select branchid from userProfileview up where up.rolename='BranchAdmin' and
 up.id=
  (select fn_viewuserid from fn_viewuserid(param_usersession))) 
union
select * from staffview where Organizationid=
 (select branchid from userProfileview up where up.rolename='OrganizationAdmin' and
 up.id=
  (select fn_viewuserid from fn_viewuserid(param_usersession)))
  union
  select * from staffview where branchId =
 (select branchid from userProfileview up where up.rolename='Accountant' and
 up.id=
  (select fn_viewuserid from fn_viewuserid(param_usersession))) order by firstname asc;
 end;
$BODY$;
  --
  -- Name: fn_viewstaffdetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewstaffdetails(param_id integer) RETURNS SETOF staffview
    LANGUAGE plpgsql
    AS $$
begin
 return query select  * from staffview    where Id=param_Id;
end;
$$;
  --
  -- Name: traineebatchview; Type: VIEW; Schema: public; Owner:  --
  --
-- View: traineebatchview
-- DROP VIEW traineebatchview;
  --
  -- Name: fn_viewtraineebatch(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewtraineebatch(
	params_usersession text)
    RETURNS SETOF traineebatchview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select * from traineebatchview
    where branchid in(select branchid from userProfileview
where rolename='BranchAdmin' and id=(select fn_viewuserid from fn_viewuserid(params_usersession)))
union
  select * from traineebatchview sbv where sbv.staffuserid=(select userid from userProfileview
where id=(select fn_viewuserid from fn_viewuserid(params_usersession))) and sbv.branchid in
(select branchid from userProfileview
where id=(select fn_viewuserid from  fn_viewuserid(params_usersession)))
union
select * from traineebatchview sbv where sbv.batchid in(select batchid from traineebatch 
	where traineeid=(select admissionnum from admisiondetails where userid in(select id from userProfileview
where id=(select fn_viewuserid from fn_viewuserid (params_usersession)))))
and sbv.branchid in (select branchid from userProfileview
where id in(select fn_viewuserid from  fn_viewuserid(params_usersession)));	
			 end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewbatchsession(
    param_id integer,
    sessionparam character varying)
    RETURNS SETOF traineebatchview
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
 begin return query select * from traineebatchview
 where Organizationbranchid in(select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid(sessionparam)))
 and batchid=param_id and stdbatchstatus='Active'; end;
$BODY$;

create or replace function fn_traineebybatch(param_batchid int)
RETURNS SETOF traineebatchview
     as
     $$
     begin
     return query select * from traineebatchview
         where stdbatchstatus='Active' and batchid=param_batchid;
     end
     $$
     language plpgsql;
      
create or replace view allocprojview as
select ap.id,
       ap.userid,
       ap.projectid,
       p.projectname,
       p.branchid,
       p.type,
       p.fee,
       p.assigneddate,
       p.submissiondate,
       p.teamsize,
       p.category,
       u.firstname,
       u.middlename,
       u.lastname
 from allocateproject ap join projects p on p.id=ap.projectid
join users u on u.id=ap.userid;

																						
																						
																						
																						
																						
																						
																						
create or replace function fn_viewallocatedproject(sessionparam character varying)
 RETURNS SETOF allocprojview
     as
     $$
     begin
     return query select * from allocprojview
         where branchid in(select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid(sessionparam)));
     end
     $$
     language plpgsql;


create or replace function fn_allocprojectbyid(param_id int)
returns setof allocprojview
as
$$
begin
return query select * from allocprojview where id=param_id;
end
$$
language plpgsql;


														

  --
  -- Name: traineemarksview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW traineemarksview AS
 SELECT sm.id,
    sm.traineeid,
    sm.appraisalid,
    sm.marks,
    ett.sessionsubjectid,
    ett.appraisalstartdatetime,
    ett.appraisalenddatetime,
    ett.appraisalname,
    ett.status AS appraisalstatus,
    sub.name,
    csm.syllabus,
    csm.batchid,
    sbm.Organizationid,
    sbm.startdate,
    sbm.status AS branchstatus,
    sbm.contactdetailsid,
    ad.admissionnum,
    sbm.id AS branchid,
    u.firstname,
    u.lastname,
	ett.totalmarks
  FROM traineemarks sm
	 join appraisaltimetable ett on ett.id=sm.subjectid
     JOIN sessionsubject csm ON ett.sessionsubjectid = csm.id
     JOIN subjectmaster sub ON ett.sessionsubjectid = sub.id
     JOIN branchsession bcm ON bcm.id = csm.branchsessionid
     JOIN branch sbm ON sbm.id = bcm.branchid
     JOIN admisiondetails ad ON ad.admissionnum = sm.traineeid
     JOIN users u ON u.id = ad.userid;
  --
  -- Name: fn_viewtraineemarks(text); Type: FUNCTION; Schema: public; Owner:  --
CREATE OR REPLACE FUNCTION fn_viewtraineemarks(
	param_usersession text)
    RETURNS SETOF traineemarksview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select * from traineemarksview where branchid in(select branchid from userProfileview 
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewtraineemarksdetails(
	param_id integer)
    RETURNS SETOF traineemarksview
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select * from traineemarksview
    where Id=param_Id;
end;
$BODY$;
   --
  -- Name: subjectview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW subjectview AS
 SELECT subjectmaster.id,
    subjectmaster.name
   FROM subjectmaster;
  --
  -- Name: fn_viewsubject(); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewsubject() RETURNS SETOF subjectview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select
    *
    from subjectview;
end;
$$;
  --
  -- Name: fn_viewsubjectdetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewsubjectdetails(param_id integer) RETURNS SETOF subjectview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select *
    from subjectview
    where Id=param_Id;
end;
$$;
  --
  -- Name: survelinceview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW survelinceview AS
 SELECT bm.id,
    bm.staffid,
    bm.sessionid,
    bm.startdate,
    bm.enddate,
    bm.status,
    bm.name,
    su.id AS surveliceid,
    su.camid,
    su.batchid,
    su.camurl
   FROM (batch bm
     JOIN survelince su ON ((bm.id = su.batchid)));
  --
  -- Name: fn_viewsurvlince(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewsurvlince(param_batchid integer) RETURNS SETOF survelinceview
    LANGUAGE plpgsql
    AS $$
begin
return query
select * from survelinceview where id=param_batchid;
end;
$$;
  --
  -- Name: fn_viewsurvlincedetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewsurvlincedetails(param_id integer) RETURNS SETOF survelinceview
    LANGUAGE plpgsql
    AS $$
begin
return query
select * from survelinceview where id=param_id;
end;
$$;
  --
  -- Name: timetableview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW timetableview AS
 SELECT t.id,
    t.batchid,
    t.type,
    t.remarks,
    b.staffid,
    b.sessionid,
    b.startdate AS batchstartdate,
    b.enddate,
    b.status,
    b.name,
    c.sessionname,
    s.userid,
    s.branchid,
    s.doj,
    s.experience,
    s.userroleid,
    sbm.Organizationid,
    sbm.startdate AS branchstarteddate,
    sbm.contactdetailsid,
    cd.country,
    cd.state,
    cd.city,
    cd.place,
    cd.address,
    cd.uniqueidentificationno,
    cd.email,
    cd.mobile
   FROM timetable t
     JOIN batch b ON t.batchid = b.id
     JOIN branchsession bc ON bc.id = b.sessionid
	 join sessionmaster c on c.id=bc.sessionid
     JOIN staff s ON b.staffid = s.id
     JOIN branch sbm ON s.branchid = sbm.id
     JOIN contactdetails cd ON cd.id = sbm.contactdetailsid;
  --
  -- Name: fn_viewtimetable(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewtimetable(
	param_usersession text)
    RETURNS SETOF timetableview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select *
from
timetableview
where branchid in(select branchid from userProfileview 
where rolename!='Trainee' and  id=(select fn_viewuserid
				from fn_viewuserid(param_usersession)))
union
select * from timetableview
where batchid in(select batchid from traineebatch 
	where traineeid in(select admissionnum from admisiondetails where userid 
	in(select id from userProfileview
where id in(select fn_viewuserid from fn_viewuserid (param_usersession))))) and 
branchId in(select branchid from userProfileview 
where id in(select fn_viewuserid from fn_viewuserid(param_usersession)));
 end;
$BODY$;
  --
  -- Name: fn_viewtimetabledetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewtimetabledetails(param_id integer) RETURNS SETOF timetableview
    LANGUAGE plpgsql
    AS $$
begin
 return query select * from timetableview
 where Id=param_Id;
end;
$$;
  -- Name: trasportfeeview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW trasportfeeview AS
 SELECT tf.id,
    tf.name,
    tf.details,
    tf.transportcharge,
    tf.routid,
    trm.name AS routename,
    trm.startpoint,
    trm.endpoint,
    trm.busid,
    b.branchid,
    sbm.Organizationid,
    sbm.startdate,
    sbm.status,
    sbm.contactdetailsid,
    b.regnum,
    b.description,
    sch.name AS Organizationname,
    sch.type,
    sch.status AS Organizationstatus,
    sch.establishedyear,
    sch.regdate,
    cd.country,
    cd.state,
    cd.city,
    cd.place,
    cd.address,
    cd.uniqueidentificationno,
    cd.email,
    cd.mobile,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    u.status AS userstatus
   FROM ((((((transportfee tf
     JOIN transportroute trm ON ((tf.routid = trm.id)))
     JOIN bus b ON ((trm.busid = b.id)))
     JOIN branch sbm ON ((b.branchid = sbm.id)))
     JOIN Organization sch ON ((sbm.Organizationid = sch.id)))
     JOIN contactdetails cd ON ((sch.contactdetailsid = cd.id)))
     JOIN users u ON ((cd.userid = u.id)));
  --
  -- Name: fn_viewtransportfee(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewtransportfee(param_usersession text) RETURNS SETOF trasportfeeview
    LANGUAGE plpgsql
    AS $$
begin
return query
 select  * from  trasportfeeview
where branchid in(select branchid from userProfileview 
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)));
end
$$;
  --
  -- Name: fn_viewtransportfeedetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewtransportfeedetails(param_id integer) RETURNS SETOF trasportfeeview
    LANGUAGE plpgsql
    AS $$
begin
return query
select * from trasportfeeview
where Id=param_Id;
end;
$$;
  --
  -- Name: transportrouteview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW transportrouteview AS
 SELECT t.id,
    t.name,
    t.startpoint,
    t.endpoint,
    t.busid,
    bs.regnum,
    bs.description,
    bs.branchid,
    sbr.Organizationid,
    sbr.startdate,
    sbr.status,
    sbr.contactdetailsid,
    sch.name AS Organizationname,
    sch.type,
    sch.status AS Organizationstatus,
    sch.establishedyear,
    sch.regdate,
    cd.country,
    cd.state,
    cd.city,
    cd.place,
    cd.address,
    cd.uniqueidentificationno,
    cd.email,
    cd.mobile,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    u.status AS userstatus,
	c.place AS branchname
   FROM transportroute t
     JOIN bus bs ON t.busid = bs.id
     JOIN branch sbr ON bs.branchid = sbr.id
	      JOIN contactdetails c ON sbr.contactdetailsid = c.id
     JOIN Organization sch ON sbr.Organizationid = sch.id
     JOIN contactdetails cd ON sch.contactdetailsid = cd.id
     JOIN users u ON cd.userid = u.id;
  --
  -- Name: fn_viewtransportroute(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewtransportroute(
	param_usersession text)
    RETURNS SETOF transportrouteview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select * from transportrouteview
    where branchid in(select branchid from userProfileview 
where rolename!='OrganizationAdmin' and id=
					(select fn_viewuserid from fn_viewuserid(param_usersession)))
union all															 
 select * from transportrouteview
    where Organizationid=(select Organizationid from userProfileview 
where rolename='OrganizationAdmin' and id=
					(select fn_viewuserid from fn_viewuserid(param_usersession))) ;
end;
$BODY$;
  --
  -- Name: fn_viewtransportroutedetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewtransportroutedetails(param_id integer) RETURNS SETOF transportrouteview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select * from transportrouteview
    where Id=param_Id;
end;
$$;
  --
  -- Name: transportstopview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW transportstopview AS
 SELECT ts.id,
    ts.routeid,
    ts.stopname,
    ts.arrivaltime,
    ts.departuretime,
    ts.latitude,
    ts.longitude,
    trm.name AS routename,
    trm.startpoint,
    trm.endpoint,
    trm.busid,
    b.regnum,
    b.description,
    b.branchid,
    sbm.Organizationid,
    sbm.contactdetailsid,
    s.name AS Organizationname,
    s.type,
    s.establishedyear,
    s.regdate,
    c.userid,
    c.country,
    c.state,
    c.city,
    c.place,
    c.address,
    c.uniqueidentificationno,
    c.email,
    c.mobile,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
	cd.place AS branchname
   FROM transportstop ts
     JOIN transportroute trm ON ts.routeid = trm.id
     JOIN bus b ON trm.busid = b.id
     JOIN branch sbm ON b.branchid = sbm.id
       JOIN contactdetails cd ON sbm.contactdetailsid = cd.id
   JOIN Organization s ON sbm.Organizationid = s.id
     JOIN contactdetails c ON s.contactdetailsid = c.id
     JOIN users u ON c.userid = u.id;
  --
  -- Name: fn_viewtransportstop(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewtransportstop(
	param_usersession text)
    RETURNS SETOF transportstopview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select * from transportstopview where branchid in(select branchid from userProfileview 
where rolename!='OrganizationAdmin' and id=(select fn_viewuserid from fn_viewuserid(param_usersession)))
union all																			  
select * from transportstopview where Organizationid=(select Organizationid from userProfileview 
where rolename='OrganizationAdmin' and id=(select fn_viewuserid from fn_viewuserid(param_usersession)))																			  ;
end;
$BODY$;
  --
  -- Name: fn_viewtransportstopdetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewtransportstopdetails(param_id integer) RETURNS SETOF transportstopview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select * from transportstopview
    where Id=param_Id;
end;
$$;
CREATE OR REPLACE VIEW transportstopchargeview AS
 SELECT tsf.id,
    tsf.amount,
    tsf.routeid,
    tsf.stopid,
    trm.name AS routename,
    tsm.id AS stopsid,
    trm.startpoint,
    trm.endpoint,
    tsm.stopname,
    tsm.arrivaltime,
    tsm.departuretime,
    b.branchid,
	sbm.Organizationid,
	c.place AS branchname
   FROM transportstopfee tsf
     JOIN transportstop tsm ON tsf.stopid = tsm.id
     JOIN transportroute trm ON tsm.routeid = trm.id
     JOIN bus b ON trm.busid = b.id
     JOIN branch sbm ON b.branchid = sbm.id
     JOIN contactdetails c ON sbm.contactdetailsid = c.id
     JOIN Organization sch ON sbm.Organizationid = sch.id
     JOIN contactdetails cd ON sch.contactdetailsid = cd.id
     JOIN users u ON cd.userid = u.id;
     CREATE OR REPLACE FUNCTION fn_viewtransportstopbyrouteid(
	param_routeid integer)
    RETURNS SETOF transportstopview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select * from transportstopview
    where routeId=param_routeid;
end;
$BODY$;
CREATE OR REPLACE VIEW stopbyrouteview
 as
 select ut.id as UserTransportid,ut.userid,ut.stopid,  trm.id as transportrouteid,trm.name,trm.startpoint,trm.endpoint,trm.busid,
 tsm.id,tsm.routeid,tsm.stopname,tsm.arrivaltime,tsm.departuretime
 from  transportroute as trm join
transportstop as tsm on trm.id = tsm.routeid
 join UserTransport as ut on ut.stopid=tsm.routeid;

CREATE OR REPLACE FUNCTION fn_viewtransportstopfee(
	param_usersession text)
    RETURNS SETOF transportstopchargeview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
 select  * from  transportstopchargeview
where branchid in(select branchid from userProfileview 
where rolename!='OrganizationAdmin' and id=(select fn_viewuserid 
								  from fn_viewuserid(param_usersession)))
union all
select  * from  transportstopchargeview
where Organizationid=(select Organizationid from userProfileview 
where rolename='OrganizationAdmin' and id=(select fn_viewuserid 
								  from fn_viewuserid(param_usersession)));
end
$BODY$;

CREATE OR REPLACE FUNCTION fn_viewtransportstopfeedetails(
	param_id integer)
    RETURNS SETOF transportstopchargeview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
 select  * from  transportstopchargeview
where id= param_id;
end
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewstopfeebyroute(
    param_routeid integer)
    RETURNS SETOF stopbyrouteview
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
return query
select * from stopbyrouteview
where routeid=param_routeid;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewtransportstopfeebyuserid(
	param_userid int)
	returns table(amnt int)
	LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
begin
return query
select amount from transportstopfee where stopid=(select stopid from usertransport where userid=param_userid);
end;
$BODY$;
  --
  -- Name: userview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW userview AS
 SELECT users.id,
    users.firstname,
    users.middlename,
    users.lastname,
    users.dob,
    users.gender,
    users.fathername,
    users.mothername,
    users.fatheroccupation,
    users.caste,
    users.subcaste,
    users.religion,
    users.nationality,
    users.status
   FROM users;
  --
  -- Name: fn_viewuser(); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewuser() RETURNS SETOF userview
    LANGUAGE plpgsql
    AS $$
begin
return query
select * from userview;
end
$$;
  --
  -- Name: fn_viewuserdetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewuserdetails(param_id integer) RETURNS SETOF userview
    LANGUAGE plpgsql
    AS $$
begin
return query
select * from userview    where Id=param_Id;
end;
$$;
  --
  -- Name: fn_viewuserid(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewuserid(param_logsession text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
declare n int;
begin
select userid from userlogins where logsession = param_logsession into n;
return n;
end
$$;
 --
  -- Name: usertransportview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW usertransportview AS
 SELECT ut.id,
    ut.userid,
    ut.stopid,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    a.applicantid,
    sbm.Organizationid,
    sbm.startdate,
    sbm.contactdetailsid,
    ts.stopname,
    ts.arrivaltime,
    ts.departuretime,
    upv.branchid,
	cd.place AS branchname
   FROM usertransport ut
     JOIN users u ON ut.userid = u.id
     JOIN applicants a ON a.applicantid = u.id
     JOIN userprofileview upv ON upv.id = a.applicantid
     JOIN branch sbm ON sbm.id = upv.branchid
	 join contactdetails cd on sbm.contactdetailsid=cd.id
     JOIN transportstop ts ON ts.id = ut.stopid;
  --
  -- Name: fn_viewusertransport(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewusertransport(
	param_usersession text)
    RETURNS SETOF usertransportview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select * from usertransportview where branchid in(select branchid from userProfileview
where rolename!='Trainee' and rolename!='OrganizationAdmin' and id=(select fn_viewuserid from fn_viewuserid(param_usersession)))
    union all
select * from usertransportview where userid in(select userid from userProfileview
where rolename='!OrganizationAdmin' and id=(select fn_viewuserid from fn_viewuserid(param_usersession))) and branchid in(select branchid from userProfileview
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)))
union all
 select * from usertransportview where Organizationid=(select Organizationid from userProfileview
where (rolename='OrganizationAdmin') and id=(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$BODY$;
  --
  -- Name: fn_viewusertransportdetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewusertransportdetails(param_id integer) RETURNS SETOF usertransportview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select * from usertransportview
    where Id=param_Id;
end;
$$;
  --
  -- Name: usertransportpaymentview; Type: VIEW; Schema: public; Owner:  --
  --
CREATE OR REPLACE VIEW usertransportpaymentview AS
 SELECT utp.id,
    utp.userid,
    utp.paidamount,
    utp.paiddate,
    utp.due,
    utp.status AS transportpaymentstatus,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    sbm.id AS branchid,
    sbm.Organizationid,
    sbm.startdate,
    sbm.contactdetailsid
   FROM ((((usertransportpayment utp
     JOIN users u ON ((utp.userid = u.id)))
     JOIN applicants a ON ((a.applicantid = u.id)))
     JOIN userprofileview upv ON ((upv.id = a.applicantid)))
     JOIN branch sbm ON ((sbm.id = upv.branchid)));
 CREATE OR REPLACE VIEW usertransportpaymentbybatchview AS
 SELECT utp.id,
    utp.userid,
    utp.paidamount,
    utp.paiddate,
    utp.due,
    utp.status AS transportpaymentstatus,
    u.firstname,
    u.middlename,
    u.lastname,
    bm.id AS batchid,
    bm.name AS batchname,
    ad.branchid,
	tf.amount,
	tsm.stopname
  from   usertransportpayment utp
 join usertransport ut on ut.userid=utp.userid
     JOIN admisiondetails ad ON utp.userid = ad.userid 
	 join transportstop tsm on tsm.id =ut.stopid
	 join transportstopfee tf on tf.stopid=tsm.id
     JOIN traineebatch sbm ON sbm.traineeid = ad.admissionnum
     JOIN batch bm ON bm.id = sbm.batchid
     JOIN users u ON u.id = ad.userid;
CREATE OR REPLACE FUNCTION fn_viewtransportfeepaymentsbyId(
	param_feepaymentsid int)
    RETURNS SETOF usertransportpaymentbybatchview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select * from usertransportpaymentbybatchview  where batchname!='dummy' and id=param_feepaymentsid;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewtransportfeepaymentsbybatch(
	param_batchid integer,params_usersession text)
    RETURNS SETOF usertransportpaymentbybatchview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query	
	select * from usertransportpaymentByBatchview where BatchId=param_batchid and
	branchid in (select branchid from userProfileview 
where rolename!='Trainee' and id=(select fn_viewuserid from fn_viewuserid(params_usersession)))
union
select * from usertransportpaymentByBatchview where BatchId=param_batchid and userid in (select userid from userProfileview 
where id=(select fn_viewuserid from fn_viewuserid(params_usersession))) and branchid in(select branchid from userProfileview 
where  id=(select fn_viewuserid from fn_viewuserid(params_usersession)));
end;
	 
	 
$BODY$;
  -- Name: fn_viewusertransportpayment(text); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewusertransportpayment(param_usersession text) RETURNS SETOF usertransportpaymentview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select *
    from usertransportpaymentview
    where branchid in(select branchid from userProfileview 
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$$;
  --
  -- Name: fn_viewusertransportpaymentdetails(integer); Type: FUNCTION; Schema: public; Owner:  --
  --
CREATE OR REPLACE FUNCTION fn_viewusertransportpaymentdetails(param_id integer) RETURNS SETOF usertransportpaymentview
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select *
    from usertransportpaymentview
    where Id=param_Id;
end;
$$;
CREATE OR REPLACE FUNCTION fn_viewusertransportpaymentByUserId(
	param_userid integer)
    RETURNS TABLE(amount double precision) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select coalesce(sum(paidamount),0)
 from usertransportpaymentview
    where userid=param_userid;
end;
$BODY$;
CREATE OR REPLACE VIEW loggedview AS
 SELECT l.id,
    l.loginid,
    l.password,
    l.firstname,
    l.middlename,
    l.lastname,
    l.dob,
    l.gender,
    l.fathername,
    l.mothername,
    l.fatheroccupation,
    l.caste,
    l.subcaste,
    l.religion,
    l.nationality,
    l.userstatus,
    l.roleid,
    l.rolename,
    l.creationtime,
    l.modifiedtime,
    l.rolestatus,
    l.contactid,
    l.contactuserid,
    l.country,
    l.state,
    l.city,
    l.place,
    l.address,
    l.uniqueidentificationno,
    l.email,
    l.mobile,
    cm.sessionname AS standard,
    sm.name AS Organizationname,
    cnm.place AS branchname,
    cty.name AS branchcity
    FROM loginview l
     JOIN ( SELECT admisiondetails.branchid,
            admisiondetails.userid
           FROM admisiondetails) am ON l.id = am.userid
     JOIN ( SELECT branch.id,
            branch.Organizationid,
            branch.contactdetailsid
           FROM branch) sbm ON sbm.id = am.branchid
     JOIN ( SELECT contactdetails.id,
            contactdetails.place,
            contactdetails.city
           FROM contactdetails) cnm ON cnm.id = sbm.contactdetailsid
     JOIN ( SELECT cities.id,
            cities.name
           FROM cities) cty ON cnm.city = cty.id
     JOIN ( SELECT Organization.id,
            Organization.name
           FROM Organization) sm ON sbm.Organizationid = sm.id
     JOIN ( SELECT branchsession.branchid,
            branchsession.sessionid
           FROM branchsession) bcm ON bcm.branchid = sbm.id
     JOIN sessionmaster cm ON cm.id = bcm.sessionid 
UNION
 SELECT l.id,
    l.loginid,
    l.password,
    l.firstname,
    l.middlename,
    l.lastname,
    l.dob,
    l.gender,
    l.fathername,
    l.mothername,
    l.fatheroccupation,
    l.caste,
    l.subcaste,
    l.religion,
    l.nationality,
    l.userstatus,
    l.roleid,
    l.rolename,
    l.creationtime,
    l.modifiedtime,
    l.rolestatus,
    l.contactid,
    l.contactuserid,
    l.country,
    l.state,
    l.city,
    l.place,
    l.address,
    l.uniqueidentificationno,
    l.email,
    l.mobile,
    sm.name AS standard,
    sm.name AS Organizationname,
    l.place AS branchname,
    cty.name AS branchcity
   FROM loginview l
     JOIN ( SELECT Organization.name,
            Organization.contactdetailsid
           FROM Organization) sm ON sm.contactdetailsid = l.contactid
     JOIN ( SELECT cities.id,
            cities.name
           FROM cities) cty ON l.city = cty.id
UNION
 SELECT l.id,
    l.loginid,
    l.password,
    l.firstname,
    l.middlename,
    l.lastname,
    l.dob,
    l.gender,
    l.fathername,
    l.mothername,
    l.fatheroccupation,
    l.caste,
    l.subcaste,
    l.religion,
    l.nationality,
    l.userstatus,
    l.roleid,
    l.rolename,
    l.creationtime,
    l.modifiedtime,
    l.rolestatus,
    l.contactid,
    l.contactuserid,
    l.country,
    l.state,
    l.city,
    l.place,
    l.address,
    l.uniqueidentificationno,
    l.email,
    l.mobile,
    'InfoByt'::character varying AS standard,
    'InfoByt'::character varying AS Organizationname,
    'InfoByt'::character varying AS branchname,
    cty.name AS branchcity
   FROM loginview l
     JOIN cities cty ON cty.id = l.city ;
	
  --
  -- Name: logedview(character varying); Type: FUNCTION; Schema: public; Owner:  --
  --
  CREATE OR REPLACE FUNCTION logedview(
	param_usersession text)
    RETURNS TABLE(userid integer, firstname character varying, middlename character varying, lastname character varying, dob date, gender character varying, fathername character varying, mothername character varying, fatheroccupation integer, caste character varying, subcaste character varying, religion integer, nationality integer, userstatus character varying, contactid integer, contactuserid integer, country integer, state integer, city integer, place character varying, address character varying, uniqueidentificationno character varying, email character varying, mobile character varying, branchname character varying, rolename text, sessionname character varying, loginid character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select l.userid, u.firstname,
    u.middlename, u.lastname, u.dob,u.gender,u.fathername,u.mothername,u.fatheroccupation,u.caste,u.subcaste,
    u.religion, u.nationality,u.status AS userstatus, c.id AS contactid,c.userid AS contactuserid,
    c.country, c.state,c.city,c.place, c.address,c.uniqueidentificationno,c.email,
    c.mobile,c.place branchname,rm.rolename rolename, c.address sessionname,am.loginid
	from login l join users u on l.userid=u.id
join contactdetails c on c.userid=u.id
join authenticationmaster am on u.id=am.userid
join rolemaster rm on l.userroleid=rm.id and rm.rolename='SalesSupport'
where l.userid=(select fn_viewuserid from fn_viewuserid(param_UserSession))
union
 select l.userid, u.firstname,
    u.middlename, u.lastname, u.dob,u.gender,u.fathername,u.mothername,u.fatheroccupation,u.caste,u.subcaste,
    u.religion, u.nationality,u.status AS userstatus, c.id AS contactid,c.userid AS contactuserid,
    c.country, c.state,c.city,c.place, c.address,c.uniqueidentificationno,c.email,
    c.mobile,c.place branchname,rm.rolename rolename, c.address sessionname,am.loginid
	from login l join users u on l.userid=u.id
join contactdetails c on c.userid=u.id
join authenticationmaster am on u.id=am.userid
join rolemaster rm on l.userroleid=rm.id and rm.rolename='Support'
where l.userid=(select fn_viewuserid from fn_viewuserid(param_UserSession))
union
  select l.userid, u.firstname,
    u.middlename, u.lastname, u.dob,u.gender,u.fathername,u.mothername,u.fatheroccupation,u.caste,u.subcaste,
    u.religion, u.nationality,u.status AS userstatus, c.id AS contactid,c.userid AS contactuserid,
    c.country, c.state,c.city,c.place, c.address,c.uniqueidentificationno,c.email,
    c.mobile,c.place branchname,rm.rolename rolename, c.address sessionname,am.loginid
from login l join users u on l.userid=u.id
join contactdetails c on c.userid=u.id
join authenticationmaster am on u.id=am.userid
join rolemaster rm on l.userroleid=rm.id and rm.rolename='OrganizationAdmin'
where l.userid=(select fn_viewuserid from fn_viewuserid(param_UserSession))
union
select l.userid,u.firstname,
    u.middlename, u.lastname, u.dob,u.gender,u.fathername,u.mothername,u.fatheroccupation,u.caste,u.subcaste,
    u.religion, u.nationality,u.status AS userstatus, c.id AS contactid,c.userid AS contactuserid,
    c.country, c.state,c.city,c.place, c.address,c.uniqueidentificationno,c.email,
    c.mobile,sm.name branchname,rm.rolename rolename,sbm.status sessionname,am.loginid
from login l join users u on l.userid=u.id
join contactdetails c on c.userid=u.id
join authenticationmaster am on u.id=am.userid
join branch sbm on sbm.contactdetailsid=c.id
join Organization sm on sm.id=sbm.Organizationid
join rolemaster rm on l.userroleid=rm.id
where l.userid=(select fn_viewuserid from fn_viewuserid(param_UserSession))
union
select l.userid,u.firstname,
    u.middlename, u.lastname, u.dob,u.gender,u.fathername,u.mothername,u.fatheroccupation,u.caste,u.subcaste,
    u.religion, u.nationality,u.status AS userstatus, c.id AS contactid,c.userid AS contactuserid,
    c.country, c.state,c.city,c.place, c.address,c.uniqueidentificationno,c.email,
    c.mobile,sbm.branchplace branchname,rm.rolename rolename,cm.sessionname sessionname,am.loginid
from login l  join users u on l.userid=u.id
join admisiondetails adm on adm.userid=u.id
join contactdetails c on c.userid=u.id
join authenticationmaster am on u.id=am.userid
join (select sbmm.id,sbmm.Organizationid,sbmm.contactdetailsid,cc.place branchplace
      from branch sbmm join contactdetails cc on cc.id=sbmm.contactdetailsid )
      sbm on sbm.id=adm.branchid
join Organization sm on sbm.Organizationid=sm.id
join branchsession bcm on bcm.branchid=sbm.id
join sessionmaster cm on cm.id=bcm.sessionid
join rolemaster rm on l.userroleid=rm.id
where l.userid=(select fn_viewuserid from fn_viewuserid(param_UserSession))
union
select l.userid,u.firstname,
    u.middlename, u.lastname, u.dob,u.gender,u.fathername,u.mothername,u.fatheroccupation,u.caste,u.subcaste,
    u.religion, u.nationality,u.status AS userstatus, c.id AS contactid,c.userid AS contactuserid,
    c.country, c.state,c.city,c.place, c.address,c.uniqueidentificationno,c.email,
    c.mobile,sbm.branchplace branchname,rm.rolename rolename,sbm.status sessionname,am.loginid
from login l join users u on l.userid=u.id
join contactdetails c on c.userid=u.id
join authenticationmaster am on u.id=am.userid
join staff sf on sf.userid=u.id
join (select sbmm.id,sbmm.Organizationid,sbmm.contactdetailsid,sbmm.status,cc.place branchplace
      from branch sbmm join contactdetails cc on cc.id=sbmm.contactdetailsid )
      sbm on sbm.id=sf.branchid
join rolemaster rm on l.userroleid=rm.id
where l.userid=(select fn_viewuserid from fn_viewuserid(param_UserSession))
union
select l.userid, u.firstname,
    u.middlename, u.lastname, u.dob,u.gender,u.fathername,u.mothername,u.fatheroccupation,u.caste,u.subcaste,
    u.religion, u.nationality,u.status AS userstatus, c.id AS contactid,c.userid AS contactuserid,
    c.country, c.state,c.city,c.place, c.address,c.uniqueidentificationno,c.email,
    c.mobile,rm.rolename branchname,rm.rolename rolename,rm.rolename  sessionname,am.loginid
from login l join users u on l.userid=u.id
join contactdetails c on c.userid=u.id
join authenticationmaster am on u.id=am.userid
join rolemaster rm on l.userroleid=rm.id and rm.rolename='IBAdmin'
where l.userid=(select fn_viewuserid from fn_viewuserid(param_UserSession));
end;
$BODY$;
-- Name : fn_viewpaydetails(text,integer,integer); Type: FUNCTION; Schema: public; Owner:  --
--
CREATE OR REPLACE FUNCTION fn_viewpaydetails(
	param_usersession text,
	param_month integer,
	param_year integer)
    RETURNS TABLE(bankname character varying, accountnumber character varying, pannumber character varying, pfnumber character varying, userid integer, firstname character varying, lastname character varying, salary double precision, grosssalary double precision, usrcode character varying, absent integer, present integer, weekoffs integer, grosssalarystatus character varying, extraallowancesstatus character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
declare n int;
begin
select count(holidaydate) from holiday where (date_part('month',holidaydate)=param_month) and (date_part('year',holidaydate)=param_year) into n ; 
return query
 select ad.bankname,ad.accountnumber,ad.pannumber,ad.pfnumber,us.id,us.firstname,us.lastname,s.salary,p.grossSalary,newtable.usrcode,newtable._a-n,newtable._p,newtable._wo+n,p.status as grosssalarystatus,p.extraallowancesstatus
 from crosstab ('select * from paytmview','select distinct statusCode from attendanceLog order by 1') as newtable (usrcode varchar,_A integer,_P integer,_WO integer)
	join usermapping um on um.deviceuserid=cast(newtable.usrcode as integer)
    join users us on us.id=cast(um.appuserid as integer)
	join staff sm on sm.userid=us.id
 	join salary s on sm.salary=s.id
	join accountdetails as ad on ad.staffid=sm.id
	
	left outer join payslip p on p.userId=us.id and date_part('month',cast(p.month as date))=param_month and date_part('year',cast(p.month as date))=param_year where sm.branchid in(select branchid from userProfileview 
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewleavedetails(
	param_id integer)
    RETURNS table(id int,userid int,fromdate date,todate date,status varchar,description varchar,approvedby int)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select ld.id,ld.userid,ld.fromdate,ld.todate,ld.status,ld.description,ld.approvedby from leavedetails ld
    where ld.Id=param_Id;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewleavesofall(
	param_usersession text)
    RETURNS TABLE(id integer, userid integer, fromdate date, todate date, type character varying, status character varying, description character varying, approvedby integer, approvedbyfirstname character varying, approvedbylastname character varying, firstname character varying, lastname character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query select leavedet.id,leavedet.userid,leavedet.fromdate,leavedet.todate,leavedet.type,leavedet.status,leavedet.description,leavedet.approvedby,leavedet.firstname,leavedet.lastname,uz.firstname,uz.lastname from 
(select ld.id,ld.userid,ld.fromdate,ld.todate,ld.type,ld.status,ld.description,ld.approvedby,uzz.firstname,uzz.lastname from leavedetails ld join users uzz on uzz.id=ld.approvedby  where ld.userid in(
select sm.userid from staff sm where sm.userid=(select fn_viewuserid from fn_viewuserid(param_usersession)) and sm.branchid in(
select pv.branchid from userprofileview pv where pv.rolename!='Trainee' and pv.rolename!='BranchAdmin'  and pv.id in(
select fn_viewuserid from fn_viewuserid(param_usersession))))) as leavedet join users uz on leavedet.userid= uz.id
union
select leave.id,leave.userid,leave.fromdate,leave.todate,leave.type,leave.status,leave.description,leave.approvedby,leave.firstname,leave.lastname,us.firstname,us.lastname from										
(select l.id,l.userid,l.fromdate,l.todate,l.type,l.status,l.description,l.approvedby,uzz.firstname,uzz.lastname from leavedetails l join users uzz on uzz.id=l.approvedby where l.userid in(
select ad.userid from admisiondetails ad where ad.userid=(select fn_viewuserid from fn_viewuserid(param_usersession)) and ad.branchid in(
select up.branchid from userprofileview up where up.rolename='Trainee' and up.id in( 
select fn_viewuserid from fn_viewuserid(param_usersession))))) as leave join users us on leave.userid=us.id
union				
select leav.id,leav.userid,leav.fromdate,leav.todate,leav.type,leav.status,leav.description,leav.approvedby,leav.firstname,leav.lastname,usr.firstname,usr.lastname	from																			
(select lds.id,lds.userid,lds.fromdate,lds.todate,lds.type,lds.status,lds.description,lds.approvedby,uzz.firstname,uzz.lastname from leavedetails lds join users uzz on uzz.id=lds.approvedby where lds.userid in(
select upv.userid from userprofileview  upv where upv.branchid in(
select u.branchid from userprofileview u where u.rolename='BranchAdmin' and u.id in( 
select fn_viewuserid from fn_viewuserid(param_usersession))))) as leav join users usr on usr.id=leav.userid
union	
select le.id,le.userid,le.fromdate,le.todate,le.type,le.status,le.description,le.approvedby,le.firstname,le.lastname,ur.firstname,ur.lastname	from
(select lds.id,lds.userid,lds.fromdate,lds.todate,lds.type,lds.status,lds.description,lds.approvedby,uzz.firstname,uzz.lastname from leavedetails lds join users uzz on uzz.id=lds.approvedby where lds.userid in(
select ad.userid from admisiondetails ad where ad.admissionnum in(
select sbm.traineeid from traineebatch sbm where sbm.batchid in(
select bm.id from batch bm where bm.staffid in(
select stf.id from staff stf where stf.userid=(select fn_viewuserid from fn_viewuserid(param_usersession)) and stf.userid in(
select upv.userid from userprofileview  upv where upv.rolename='Trainee' or upv.rolename='ClassTeacher' and upv.branchid in(
select u.branchid from userprofileview u where u.rolename='ClassTeacher' and u.id in( 
select fn_viewuserid from fn_viewuserid(param_usersession))))))))) as le join users ur on ur.id=le.userid;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewpayslipofuser(
	param_userid integer)
    RETURNS TABLE(userid integer, firstname character varying, lastname character varying, email character varying, rolename text, Organizationid integer, Organizationname character varying, salary double precision, grosssalary double precision, month character varying, doj date,bankname varchar,accountnumber varchar,pannumber varchar,pfnumber varchar) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select u.id as userid,u.firstname,u.lastname,upv.email,upv.rolename,upv.Organizationid, upv.name as Organizationname,sal.salary,p.grosssalary,p.month,sm.doj,ad.bankname,ad.accountnumber,ad.pannumber,ad.pfnumber
 from payslip p
join users u on u.id=p.userid
join staff sm on sm.userid=p.userid
join salary sal on sal.id=sm.salary
join accountdetails ad on ad.staffid=sm.id
join userprofileview upv on upv.id=u.id  where p.userid=param_userid ;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewnotifications(
	param_usersession text)
    RETURNS TABLE(id integer, notificationmessage character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select nd.id,n.notification as notificationmessage from notificationdetails as  nd join notifications n on   nd.notificationid=n.id
                                        join users u on nd.senderuserid=u.id where nd.receiveruserid=(select upv.id from userprofileview upv where upv.userid=
        (select fn_viewuserid from fn_viewuserid(param_usersession))) order by n.id desc;
end;
$BODY$;
create or replace function fn_viewLevreceiverIds(param_usersession text)
	returns table(userid int,rolename text)
		LANGUAGE 'plpgsql'
			as	$body$
				begin	
				return query
select s.userid,upv.rolename from staff s join userprofileview upv on upv.id=s.userid  where s.id in(															   
 select staffid from batch where id in (select batchid from traineebatch where
traineeid in(select admissionnum from admisiondetails ad where ad.userid in 
(select fn_viewuserid from fn_viewuserid(param_usersession)))) and sessionid in(select id from branchsession
where branchid in(select branchid from userprofileview upv where upv.rolename='Trainee' and  upv.id in
  (select fn_viewuserid from fn_viewuserid(param_usersession)))))
union																							 
 select upv.id,upv.rolename from userprofileview upv where upv.rolename='BranchAdmin'
and upv.branchid in(select branchid from userprofileview where id in
		(select fn_viewuserid from fn_viewuserid(param_usersession)));
			end;
			 $body$;
CREATE OR REPLACE FUNCTION fn_viewholidayreceiverids(
	param_usersession text)
    RETURNS TABLE(userid integer, rolename text) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
				begin	
				return query
																						 
 select upv.id,upv.rolename from userprofileview upv where upv.rolename!='OrganizationAdmin' and upv.rolename!='BranchAdmin' and upv.branchid in(select branchid from userprofileview where id in
		(select fn_viewuserid from fn_viewuserid(param_usersession)));
			end;
			 
$BODY$;
create or replace function fn_viewbatchreceiverIds(param_usersession text,param_batchId int)
	returns table(userid int,rolename text)
		LANGUAGE 'plpgsql'
			as	$body$
				begin	
				return query
							
			select upv.userid,upv.rolename from batch b join staff s on s.id=b.staffid join userprofileview upv on upv.id=s.userid where  b.id=(param_batchId) 	and					
 upv.branchid in(select branchid from userprofileview where id in
		(select fn_viewuserid from fn_viewuserid(param_usersession)));
			end;
			 $body$;
create or replace function fn_viewtimetablereceiverIds(param_usersession text,param_batchId int)
	returns table(userid int,rolename text)
		LANGUAGE 'plpgsql'
			as	$body$
				begin	
				return query
select upv.userid,upv.rolename 
from traineebatch sbm
join batch bm on bm.id=sbm.batchid			
join admisiondetails ad on ad.admissionnum=sbm.traineeid 
join users u on u.id=ad.userid join userprofileview upv on upv.id=u.id where 
sbm.batchid=(param_batchId)				
and upv.branchid in(select branchid from userprofileview where id in
(select fn_viewuserid from fn_viewuserid(param_usersession)))
	union
select upv.userid,upv.rolename 
from batch bm 			
join staff sm on sm.id=bm.staffid
join users u on u.id=sm.userid join userprofileview upv on upv.id=u.id where 
bm.id=(param_batchId)				
and upv.branchid in(select branchid from userprofileview where id in
(select fn_viewuserid from fn_viewuserid(param_usersession)));							 
			end;
			 $body$;
             create or replace function fn_viewappraisaltimetablereceiverIds(param_usersession text,param_sessionubjectId int)
    returns table(userid int,rolename text)
        LANGUAGE 'plpgsql'
            as    $body$
                begin
                return query
select upv.userid,upv.rolename
from appraisaltimetable ett join
sessionsubject csm on csm.id=ett.sessionsubjectid
join batch bm on bm.id=csm.batchid
join traineebatch sbm on bm.id=sbm.batchid
join admisiondetails ad on ad.admissionnum=sbm.traineeid
join users u on u.id=ad.userid join userprofileview upv on upv.id=u.id where
csm.id=(param_sessionubjectId)
and upv.branchid in(select branchid from userprofileview where id in
(select fn_viewuserid from fn_viewuserid(param_usersession)))
    union
select upv.userid,upv.rolename
from appraisaltimetable ett join
sessionsubject csm on csm.id=ett.sessionsubjectid
join batch bm on bm.id=csm.batchid
join staff sm on sm.id=bm.staffid
join users u on u.id=sm.userid join userprofileview upv on upv.id=u.id where
csm.id=(param_sessionubjectId)
and upv.branchid in(select branchid from userprofileview where id in
(select fn_viewuserid from fn_viewuserid(param_usersession)));
            end;
             $body$;
CREATE OR REPLACE FUNCTION fn_viewdepartments(
	param_usersession text,
	param_month integer,
	param_year integer)
    RETURNS TABLE(userid integer, firstname character varying, lastname character varying, usrcode character varying, absent integer, present integer, weekoffs integer) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
declare n int;
begin
select count(holidaydate) from holiday where (date_part('month',holidaydate)=param_month) and (date_part('year',holidaydate)=param_year) into n ; 
return query
 select us.id,us.firstname,us.lastname,newtable.usrcode,newtable._a-n,newtable._p,newtable._wo+n
 from crosstab ('select * from userattendanceview','select distinct statusCode from attendanceLog order by 1') as newtable (usrcode varchar,_A integer,_P integer,_WO integer)
	join usermapping um on um.deviceuserid=cast(newtable.usrcode as integer)
    join userprofileview us on us.id=cast(um.appuserid as integer)
	where rolename<>'Trainee' and rolename<>'BranchAdmin' and rolename<>'OrganizationAdmin' and branchid in(select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewdepartmentdetailsbyid(
    param_id int)
    RETURNS SETOF departments
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
    return query
select *From departments where id=param_id ;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewatendance(
	param_usersession text,
	param_month integer,
	param_year integer)
    RETURNS TABLE(userid integer, firstname character varying, lastname character varying, usrcode character varying, absent integer, present integer, weekoffs integer) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
declare n int;
begin
select count(holidaydate) from holiday where (date_part('month',holidaydate)=param_month) and (date_part('year',holidaydate)=param_year) into n ; 
return query
 select us.id,us.firstname,us.lastname,newtable.usrcode,newtable._a-n,newtable._p,newtable._wo+n
from crosstab ('select * from userattendanceview','select distinct statusCode from attendanceLog order by 1') as newtable (usrcode varchar,_A integer,_P integer,_WO integer)
	join usermapping um on um.deviceuserid=cast(newtable.usrcode as integer)
    join users us on us.id=cast(um.appuserid as integer)
	where id=(select fn_viewuserid from fn_viewuserid(param_usersession));
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewtraineeattendancedetais(
	param_usersession text,
	param_month integer,
	param_year integer)
    RETURNS TABLE(userid integer, firstname character varying, lastname character varying, usrcode character varying, absent integer, present integer, weekoffs integer) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
declare n int;
begin
select count(holidaydate) from holiday where (date_part('month',holidaydate)=param_month) and (date_part('year',holidaydate)=param_year) into n ; 
return query
 select us.id,us.firstname,us.lastname,newtable.usrcode,newtable._a-n,newtable._p,newtable._wo+n
 from crosstab ('select * from userattendanceview','select distinct statusCode from attendanceLog order by 1') as newtable (usrcode varchar,_A integer,_P integer,_WO integer)
	join usermapping um on um.deviceuserid=cast(newtable.usrcode as integer)
    join userprofileview us on us.id=cast(um.appuserid as integer)
	where rolename='Trainee' and branchid in(select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewattendancelogtraineebyclassteacher(
	param_usersession text,
	param_month integer,
	param_year integer)
    RETURNS TABLE(userid integer, firstname character varying, lastname character varying, usrcode character varying, absent integer, present integer, weekoffs integer) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
declare n int;
begin
select count(holidaydate) from holiday where (date_part('month',holidaydate)=param_month) and (date_part('year',holidaydate)=param_year) into n ; 
return query
 select us.id,us.firstname,us.lastname,newtable.usrcode,newtable._a-n,newtable._p,newtable._wo+n
 from crosstab ('select * from userattendanceview','select distinct statusCode from attendanceLog order by 1') as newtable (usrcode varchar,_A integer,_P integer,_WO integer)
	join usermapping um on um.deviceuserid=cast(newtable.usrcode as integer)
    join (select ad.userid from admisiondetails ad where ad.admissionnum in (select  traineeid from traineebatch where batchid in(select id from batch where staffid in (select sm.id from staff sm where sm.userid=(select fn_viewuserid from fn_viewuserid(param_usersession)))))) as btraineeids  on btraineeids.userid=cast(um.appuserid as integer)
	join users us on us.id=btraineeids.userid;
	end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewGalleryreceiverids(
	param_usersession text)
    RETURNS TABLE(userid integer, rolename text) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
				begin	
				return query
select upv.id,upv.rolename  from userprofileview upv  where upv.rolename!='BranchAdmin' and upv.branchid in(select branchid from userprofileview where id in(select a.applicantid from applicants a join userprofileview upv on a.createdby=upv.id 
where a.createdby=(select fn_viewuserid from fn_viewuserid(param_usersession))
and upv.rolename='BranchAdmin'	))
union																							 
select upv.id,upv.rolename  from userprofileview upv where upv.rolename!='OrganizationAdmin' and upv.branchid in(select branchid from userprofileview where id in(select a.applicantid from applicants a join userprofileview upv on a.createdby=upv.id 
where a.createdby=(select fn_viewuserid from fn_viewuserid(param_usersession))
and upv.rolename='OrganizationAdmin'	));
			end;
			 
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewFeedBackreceiverids(
	param_usersession text,param_userid int)
    RETURNS TABLE(userid integer, rolename text) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
				begin	
				return query
select upv.id,upv.rolename from feedback f join userprofileview upv on f.userid=upv.id
where upv.id=(param_userid)and upv.branchid in
(select branchid from userProfileview 
where id=(select fn_viewuserid from
		  fn_viewuserid(param_usersession)));
			end;
			 
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewusermapping(
	param_usersession text)
    RETURNS TABLE(deviceuserid integer, appuserid character varying, deviceid character varying,firstname  character varying,lastname  character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
 return query
																																   
select um.deviceuserid,um.appuserid,um.deviceid,urs.firstname,urs.lastname from usermapping um join users urs on urs.id=cast(um.appuserid as integer) where cast(um.appuserid as integer) 
in(select id from userprofileview where branchid in(select branchid from userprofileview where
	id=(select fn_viewuserid from fn_viewuserid(param_usersession))));
 end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewFeepaymentsreceiverids(
	param_usersession text,param_userid int)
    RETURNS TABLE(userid integer, rolename text) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
				begin	
				return query
select upv.id,upv.rolename from feepayments f join userprofileview upv on f.traineeid=upv.id
where upv.id=(param_userid)and upv.branchid in
(select branchid from userProfileview 
where id=(select fn_viewuserid from
		  fn_viewuserid(param_usersession)));
			end;
			 
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewAchievementsreceiverids(
	param_usersession text)
    RETURNS TABLE(userid integer, rolename text) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
				begin	
				return query
select upv.id,upv.rolename from userprofileview upv where 
 upv.branchid in
(select branchid from userProfileview 
where id=(select fn_viewuserid from
		  fn_viewuserid(param_usersession)));
			end;
			 
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewOrganizationFeedBackreceiverids(
	param_usersession text)
    RETURNS TABLE(userid integer, rolename text) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
				begin	
				return query
select upv.id,upv.rolename from  userprofileview upv 
where upv.rolename='BranchAdmin' and upv.branchid in
(select branchid from userProfileview 
where id=(select fn_viewuserid from
		  fn_viewuserid(param_usersession)));
			end;
			 
$BODY$;
CREATE OR REPLACE FUNCTION fn_notificationcount(
    param_usersession text)
    RETURNS TABLE(notifications bigint)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
                begin
                return query
select count(notificationid) as notifications from notificationdetails where receiveruserid=
(select fn_viewuserid from fn_viewuserid(param_usersession))
                        and status='Active';
            end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewtestimonial(
    )
    RETURNS TABLE(id integer, name character varying, description text, posteddate date)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
 return query
select * from testimonial order by random() limit 5;
 end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewtraineemarksreceiverids(
    param_usersession text,param_traineeid int)
    RETURNS TABLE(userid integer, rolename text)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
                begin
                return query
select upv.id,upv.rolename from traineemarks sm
join admisiondetails ad on sm.traineeid=ad.admissionnum
join userprofileview upv on upv.id=ad.userid where sm.traineeid=(param_traineeid) and
upv.branchid in(select branchid from userprofileview where id=
        (select fn_viewuserid from fn_viewuserid(param_usersession)));
            end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewStaffForTransport(
		param_usersession text)
    RETURNS Table (userid int,branchid int,firstname varchar,lastname varchar) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select s.userid,s.branchid,u.firstname,u.lastname from staff s
join users u on u.id=s.userid 
where s.userroleid!=(select id from rolemaster where rolename='Driver') and s.branchid in
(select upv.branchid from userprofileview  upv where upv.id=(select fn_viewuserid from fn_viewuserid(param_usersession)))order by u.firstname asc;
end;
$BODY$;
CREATE OR REPLACE VIEW eventsview AS
 SELECT e.id,
    e.departmentid,
    e.name,
    e.fromdate,
    e.todate,
    e.userid,
    e.description,
    e.venue,
    e.starttime,
    e.endtime,
    e.contactdetailsid,
    e.branchId,
    d.name AS departmentname,
    d.staffid,
    d.courseid,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    u.status,
    c.country,
    c.state,
    c.city,
    c.place,
    c.address,
    c.uniqueidentificationno,
    c.email,
    c.mobile
   FROM events e
     JOIN departments d ON d.id = e.departmentid
     JOIN users u ON u.id = e.userid
     JOIN contactdetails c ON c.id = e.contactdetailsid;
CREATE OR REPLACE VIEW eventrequestview AS
 SELECT e.id,
    e.departmentid,
    e.name,
    e.fromdate,
    e.todate,
    e.userid,
    e.description,
    e.venue,
    e.starttime,
    e.endtime,
    e.contactdetailsid,
    e.branchid,
    d.name AS departmentname,
    d.staffid,
    d.courseid,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    u.status AS userstatus,
    c.country,
    c.state,
    c.city,
    c.place,
    c.address,
    c.uniqueidentificationno,
    c.email,
    c.mobile,
	et.status as requeststatus
   FROM events e
     JOIN departments d ON d.id = e.departmentid
     JOIN eventrequest et ON e.id = et.eventsid
     JOIN users u ON u.id = e.userid
     JOIN contactdetails c ON c.id = e.contactdetailsid;
create or replace function	fn_viewevents(param_session text)																									   
RETURNS SETOF eventsview
as																										   
$$
begin
return query
select * from eventsview
where branchid in(select branchId from userprofileview where userid=(select fn_viewuserid from fn_viewuserid(param_session))) ;
end;																										   
$$
language plpgsql;
CREATE OR REPLACE FUNCTION fn_vieweventdetailsbyid(
	param_id integer)
    RETURNS SETOF eventsview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from eventsview
where Id=param_id;
end
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewalleventrequest(
	param_usersession text)
    RETURNS SETOF eventrequestview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from eventrequestview
where branchid in(select branchid from userProfileview
where id in(select fn_viewuserid from
          fn_viewuserid(param_usersession)));
end
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewdepartments()
    RETURNS TABLE(ids integer, names character varying, staffids integer, courseids integer) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
 return query
select  id,name,staffid,courseid  from  departments   order by name asc;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewevents(
	param_branchid integer)
    RETURNS SETOF eventsview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from eventsview where 
branchId=param_branchid;
end
$BODY$;
CREATE OR REPLACE VIEW msgsummary AS
 SELECT count(messagestable.status) AS ucount,
    messagestable.status,
    messagestable.conversationid
   FROM messagestable
  GROUP BY messagestable.conversationid, messagestable.status;
CREATE OR REPLACE VIEW chatview AS
 SELECT newtable.conversationid,
    newtable._r,
    newtable._u,
    u.id,
    u.username,
    m.messages,
    row_number() OVER (PARTITION BY c.id ORDER BY m.id DESC) AS pos
   FROM crosstab('select conversationid,status,max(ucount) from msgsummary group by 1,2 order by 1,2'::text, 'select distinct status from msgsummary order by 1'::text) newtable(conversationid character varying, _r integer, _u integer)
     JOIN conversationtable c ON c.id = newtable.conversationid::integer
     JOIN userchat u ON u.id::text = c.userid::text
     JOIN messagestable m ON c.id = m.conversationid
  WHERE c.status::text = 'Active'::text;
CREATE OR REPLACE FUNCTION fn_viewattendancelogbyorganizationadmin(
	param_branchid integer,
	param_month integer,
	param_year integer)
    RETURNS TABLE(id integer, firstname character varying, lastname character varying, usrcode integer, absent integer, present integer, weekoffs integer) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
declare n int;
begin
select count(holidaydate) from holiday where (date_part('month',holidaydate)=param_month) and (date_part('year',holidaydate)=param_year) into n ;
    return query
	 select us.id,us.firstname,us.lastname,cast(newtable.usrcode as int),newtable._a,newtable._p,newtable._wo from crosstab ('select * from userattendanceview','select distinct statusCode from attendanceLog order by 1') as newtable (usrcode varchar,_A integer,_P integer,_WO integer)
	join usermapping um on um.deviceuserid=cast(newtable.usrcode as integer)
    join users us on us.id=cast(um.appuserid as integer) where us.id in
(with bstaff as (select userid from userprofileview where (rolename='Driver' or rolename='Trainer' or rolename='ClassTeacher' or rolename='Accountant') and branchid=param_branchid)
select * from bstaff); 												 
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewtodaysattendancelogbybranchadmin(
	param_usersession text,
	param_attendancedate character varying)
    RETURNS SETOF attendancelogview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
	select * from attendancelogview where (cast (attendancedate as date)) = cast(param_attendancedate as date)
and cast (appuserid as int) in (select id from userprofileview where branchid=(select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid(param_usersession))));
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewtestimonial(
    )
    RETURNS TABLE(id integer, name character varying, description text, posteddate date)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
 return query
select * from testimonial order by random() limit 3;
 end;
$BODY$;
create or replace view mobilelogedview as 
select l.userid, u.firstname,
    u.middlename, u.lastname, u.dob,u.gender,u.fathername,u.mothername,u.fatheroccupation,u.caste,u.subcaste,
    u.religion, u.nationality,u.status AS userstatus, c.id AS contactid,c.userid AS contactuserid,
   -- c.country,ctr.name as countryname,c.state,st.name as statename,c.city,ct.name as cityname,c.place,c.address,c.uniqueidentificationno,c.email,
   c.country,c.state,c.city,c.place,c.address,c.uniqueidentificationno,c.email,
    c.mobile,c.place branchname,rm.rolename rolename, c.address sessionname
from login l join users u on l.userid=u.id
join contactdetails c on c.userid=u.id 
join rolemaster rm on l.userroleid=rm.id and rm.rolename='OrganizationAdmin'
union
select l.userid,u.firstname,
    u.middlename, u.lastname, u.dob,u.gender,u.fathername,u.mothername,u.fatheroccupation,u.caste,u.subcaste,
    u.religion, u.nationality,u.status AS userstatus, c.id AS contactid,c.userid AS contactuserid,
     --c.country,ctr.name as countryname,c.state,st.name as statename,c.city,ct.name as cityname,c.place, c.address,c.uniqueidentificationno,c.email,
    c.country,c.state,c.city,c.place,c.address,c.uniqueidentificationno,c.email,
    c.mobile,sm.name branchname,rm.rolename rolename,sbm.status sessionname
from login l join users u on l.userid=u.id
join contactdetails c on c.userid=u.id
join branch sbm on sbm.contactdetailsid=c.id
join Organization sm on sm.id=sbm.Organizationid
join rolemaster rm on l.userroleid=rm.id
union
select l.userid,u.firstname,
    u.middlename, u.lastname, u.dob,u.gender,u.fathername,u.mothername,u.fatheroccupation,u.caste,u.subcaste,
    u.religion, u.nationality,u.status AS userstatus, c.id AS contactid,c.userid AS contactuserid,
    --c.country,ctr.name as countryname,c.state,st.name as statename,c.city,ct.name as cityname,c.place, c.address,c.uniqueidentificationno,c.email,
    c.country,c.state,c.city,c.place,c.address,c.uniqueidentificationno,c.email,
    c.mobile,sbm.branchplace branchname,rm.rolename rolename,cm.sessionname sessionname
from login l  join users u on l.userid=u.id
join admisiondetails adm on adm.userid=u.id
join contactdetails c on c.userid=u.id
join (select sbmm.id,sbmm.Organizationid,sbmm.contactdetailsid,cc.place branchplace
      from branch sbmm join contactdetails cc on cc.id=sbmm.contactdetailsid )
      sbm on sbm.id=adm.branchid
join Organization sm on sbm.Organizationid=sm.id
join branchsession bcm on bcm.branchid=sbm.id
join sessionmaster cm on cm.id=bcm.sessionid
join rolemaster rm on l.userroleid=rm.id
union
select l.userid,u.firstname,
    u.middlename, u.lastname, u.dob,u.gender,u.fathername,u.mothername,u.fatheroccupation,u.caste,u.subcaste,
    u.religion, u.nationality,u.status AS userstatus, c.id AS contactid,c.userid AS contactuserid,
     --c.country,ctr.name as countryname,c.state,st.name as statename,c.city,ct.name as cityname,c.place, c.address,c.uniqueidentificationno,c.email,
    c.country,c.state,c.city,c.place,c.address,c.uniqueidentificationno,c.email,
    c.mobile,sbm.branchplace branchname,rm.rolename rolename,sbm.status sessionname
from login l join users u on l.userid=u.id
join contactdetails c on c.userid=u.id
join staff sf on sf.userid=u.id
join (select sbmm.id,sbmm.Organizationid,sbmm.contactdetailsid,sbmm.status,cc.place branchplace
      from branch sbmm join contactdetails cc on cc.id=sbmm.contactdetailsid )
      sbm on sbm.id=sf.branchid
join rolemaster rm on l.userroleid=rm.id
union
select l.userid, u.firstname,
    u.middlename, u.lastname, u.dob,u.gender,u.fathername,u.mothername,u.fatheroccupation,u.caste,u.subcaste,
    u.religion, u.nationality,u.status AS userstatus, c.id AS contactid,c.userid AS contactuserid,
   --c.country,ctr.name as countryname,c.state,st.name as statename,c.city,ct.name as cityname,c.place,c.address,c.uniqueidentificationno,c.email,
	c.country,c.state,c.city,c.place,c.address,c.uniqueidentificationno,c.email,
    c.mobile,rm.rolename branchname,
   rm.rolename rolename,rm.rolename  sessionname
from login l join users u on l.userid=u.id
join contactdetails c on c.userid=u.id
join rolemaster rm on l.userroleid=rm.id and rm.rolename='IBAdmin';
-- FUNCTION: mobilelogedview(text)
-- DROP FUNCTION mobilelogedview(text);
CREATE OR REPLACE FUNCTION mobilelogedview(param_usersession text) 
RETURNS TABLE(usrid integer, firstname character varying, middlename character varying, lastname character varying, dob date, gender character varying, fathername character varying, mothername character varying, fatheroccupation character varying, caste character varying, subcaste character varying, religion character varying, nationality character varying, userstatus character varying, contactid integer, contactuserid integer, countryid integer,  stateid integer,  cityid integer, place character varying, address character varying, uniqueidentificationno character varying, email character varying, mobile character varying, branchname character varying, rolename text, sessionname character varying, countryname character varying,statename character varying,cityname character varying)  
    LANGUAGE plpgsql
    AS $$
declare cid integer := -1; sid integer := -1; tid integer := -1;
begin
select COALESCE(country,-1) from mobilelogedview 
where userid=(select fn_viewuserid from fn_viewuserid(param_usersession)) into cid;
select COALESCE(state,-1) from mobilelogedview 
where userid=(select fn_viewuserid from fn_viewuserid(param_usersession)) into sid;
select COALESCE(city,-1) from mobilelogedview 
where userid=(select fn_viewuserid from fn_viewuserid(param_usersession)) into tid;
return query select * from mobilelogedview,
COALESCE((select name as countryname from countries where id=cid),'nocountry') as countryname,
COALESCE((select name as statename from states where id=sid),'nostate') as statename,
COALESCE((select name as cityname from cities where id=tid),'nocity') as cityname
where userid=(select fn_viewuserid from fn_viewuserid(param_usersession));
end
$$;
CREATE OR REPLACE FUNCTION fn_viewdepartmemts(
    )
    RETURNS TABLE(nm character varying, stfid integer, cid integer)
AS
$$
begin
 return query
 select  name,staffid,courseid  from  departments;
end;
$$
language plpgsql;
CREATE OR REPLACE FUNCTION fn_getdepartments(
	param_usersession text)
    RETURNS TABLE(id integer, departmentname character varying, staffid integer, courseid integer, coursename character varying, staffname character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
   select d.id,d.name as departmentname,d.staffid,d.courseid,crs.coursename,u.firstname
                  from departments d
                             join courses crs on crs.id=d.courseid
                             join staff sm on sm.userid=d.staffid
                             join users u on u.id=sm.userid
                             where sm.branchid in(select upv.branchid from userProfileview upv
where upv.id in(select fn_viewuserid from
          fn_viewuserid(param_usersession))) order by departmentname asc;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewcourses(
	)
    RETURNS TABLE(ids integer, coursenames character varying, year integer, semester character varying, levels integer,duration varchar ) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
 return query
select id,coursename,years,semesters,level,durationtype  from  courses  order by coursename asc;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewCoursesdetailsbyid(
    param_id int)
    RETURNS SETOF courses
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
    return query
select *From courses where id=param_id ;
end;
$BODY$;
create or replace function fn_vieworganizationType()
returns table(id int,organizationTypeName text) as
$$													
begin
return query select * from organizationType where organizationType.id>0;
end;
$$ language plpgsql;
-- create or replace function fn_viewBoard()
-- returns table(id int,boardName text) as
-- $$													
-- begin
-- return query select * from board where board.id>0;
-- end;
-- $$ language plpgsql;
CREATE OR REPLACE FUNCTION fn_viewboard(
	)
    RETURNS TABLE(id integer, boardname text,organization text) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
													
begin
return query select b.id,b.boardname,i.name as organization  from board b join
				organizationType i on i.id=b.organizationTypeid; 
end;
$BODY$;
create or replace view organizationTypeview as
select i.Id,
        i.Name as organizationType,
		bm.boardName,
		bm.id as boardId,
		it.name,
		it.id as typeId
 from organizationType i
 join board bm on i.Id=bm.organizationTypeId
 join institutetype it on i.Id=it.organizationTypeId;
  create or replace function fn_viewBoardOrTypeById(param_id integer)
returns setof organizationTypeview
as
$$
begin
return query
select * from organizationTypeview
where Id=param_id;
end;
$$
language plpgsql;
--view for institute type
CREATE OR REPLACE FUNCTION fn_viewinstitutetype(
	)
    RETURNS TABLE(institutetypename varchar ,organization text) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
													
begin
return query select it.name ,i.name as organization from institutetype it join
				organizationType i on i.id=it.organizationTypeid;
end;
$BODY$;
--view for libraryrequests
					
						
CREATE OR REPLACE VIEW libraryrequestsview AS
 SELECT lr.id,lr.bookid,lr.userid,l.bookname,us.firstname,us.lastname,l.branchid,lr.status
														   
    FROM libraryrequests as  lr
   			join library as l on l.id=lr.bookid 							   
	join users as us on us.id=lr.userid;	
						
--View for library
CREATE OR REPLACE VIEW libraryview AS
 SELECT lby.id,
    lby.branchid,
    lby.departmentid,
    lby.bookname,
    lby.authorname,
    lby.description,
    lby.segregation,
    dept.name AS departmentname,
    dept.staffid,
    cd.place AS branchname,
    us.firstname,
    us.middlename,
    us.lastname,
    scbm.contactdetailsid,
    cus.coursename,
    cus.level,
    cd.place,
	lby.status
   FROM library lby
     JOIN departments dept ON lby.departmentid = dept.id
     JOIN branch scbm ON scbm.id = lby.branchid
     JOIN contactdetails cd ON cd.id = scbm.contactdetailsid
     JOIN users us ON us.id = cd.userid
     JOIN courses cus ON cus.id = dept.courseid;
CREATE OR REPLACE VIEW traineelibraryview AS
 SELECT stlby.bookid,
    stlby.userid,
    stlby.issuedate,
    stlby.returndate,
    lby.branchid,
    lby.departmentid,
    lby.bookname,
    lby.authorname,
    lby.description,
    lby.segregation,
    us.firstname,
    us.middlename,
    us.lastname,
    lby.status,
    stlby.id,
    stlby.status AS issuedstatus,
	lby.penalty
   FROM traineelibrary stlby
     JOIN library lby ON stlby.bookid = lby.id
     JOIN users us ON us.id = stlby.userid;
--function for AllLibraryDetailsForCout
CREATE OR REPLACE FUNCTION fn_viewlibrary(
	params_usersession text)
    RETURNS table(bookname varchar,no_of_books bigint,authorname varchar,
				  departmentname varchar,segregation varchar) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
 return query 
 select  lv.bookname,count(lv.bookname) as no_of_books,lv.authorname,lv.departmentname,lv.segregation from libraryview as lv  where lv.status='InActive' and lv.branchid in(select branchid from userprofileview where id=(
	 select fn_viewuserid from fn_viewuserid(params_usersession))) 
											 group by (lv.bookname,lv.authorname,lv.departmentname,lv.segregation); 
											 end;
$BODY$;
--function for requests count
												  
CREATE OR REPLACE FUNCTION fn_requestcount(
	param_usersession text)
    RETURNS TABLE(requests bigint) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
                begin
                return query
select count(id) as requests from libraryrequestsview where 
	branchid in(select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid(param_usersession)))
								 					and  status='Pending';
            end;
$BODY$;		
--function for AllLibraryDetails
CREATE OR REPLACE FUNCTION fn_viewlibrarydetails(
    params_usersession text)
    RETURNS TABLE(id integer, branchid integer, departmentid integer, bookname character varying, authorname character varying, description character varying)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
 return query
 select  lv.id,lv.branchid,lv.departmentid,lv.bookname,lv.authorname,lv.description from libraryview as lv  where
 lv.branchid in(select upv.branchid from userprofileview upv where upv.id=(
     select fn_viewuserid from fn_viewuserid(params_usersession)))
        except
        select ltv.bookid,ltv.branchid,ltv.departmentid,ltv.bookname,ltv.authorname,ltv.description from librarytransactionsview ltv ;
end;
$BODY$;
--function for AlltraineeLibraryDetails
CREATE OR REPLACE FUNCTION fn_viewtraineelibrary(
	params_usersession text)
    RETURNS SETOF traineelibraryview 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
 return query 
 select  * from traineelibraryview where branchid in(select branchid from userprofileview 
											where rolename='BranchAdmin' and id in
(select fn_viewuserid from fn_viewuserid(params_usersession))) and issuedstatus='Active' 
	union all
 select  * from traineelibraryview where branchid in(select branchid from userprofileview 
						where rolename!='BranchAdmin' and id in
(select fn_viewuserid from fn_viewuserid(params_usersession))) and
	userid in(select fn_viewuserid from fn_viewuserid(params_usersession)) and issuedstatus='Active' ;		 
										 
										end;
$BODY$;
--function for AlltraineeLibraryDetailsbyId
CREATE OR REPLACE FUNCTION fn_viewtraineelibraryById(
	params_id int)
    RETURNS SETOF traineelibraryview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
 return query 
 select  * from traineelibraryview where id=params_id ;
										end;
$BODY$;	
--function for librarydetailsbyname												  
				
CREATE OR REPLACE FUNCTION fn_viewlibrarydetailsByName(param_bookname varchar)
		RETURNS SETOF libraryview
															   
LANGUAGE plpgsql
    AS $$
begin
    return query
select * from libraryview 
 where bookname=param_bookname;													   
															   
end;
$$;		
create or replace view allocplacview as
select ap.id,
       ap.traineeid,
       ap.placementid,
       p.name,
       p.branchid,
	   p.jobid,
       p.type,
       p.package,
       p.starttime,
       p.endtime,
       p.positions,
       p.category,
	   p.details,
       u.firstname,
       u.middlename,
       u.lastname,
       sb.batchid
from allocateplacement ap join placements p on p.id=ap.placementid
join admisiondetails ad on p.branchid=ad.branchid
join users u on u.id=ad.userid
join traineebatch sb on ad.admissionnum=sb.traineeid where sb.batchid !=0;
create or replace function fn_allocplacementbyid(param_id int)
returns setof allocplacview
as
$$
begin
return query select * from allocplacview where id=param_id;
end
$$
language plpgsql;
create or replace function fn_viewallocatedplacement(sessionparam character varying)
 RETURNS SETOF allocplacview
     as
     $$
     begin
     return query select * from allocplacview
         where branchid in(select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid(sessionparam)));
     end
     $$
     language plpgsql;
			--function for libraryrequests
CREATE OR REPLACE FUNCTION fn_viewlibraryRequests(params_usersession text)
		RETURNS SETOF libraryrequestsview
															   
LANGUAGE plpgsql
    AS $$
begin
    return query
select * from libraryrequestsview 
 where  branchid in(select branchid from userprofileview where
				userid=(select fn_viewuserid from fn_viewuserid(params_usersession))) order by id desc;															   
															   
end;
$$;	
--job view
CREATE OR REPLACE VIEW jobview AS
 SELECT jm.id,
    jm.jobtittle,
    jm.jobdescription,
    jm.designation,
    jm.minimumexperience,
    jm.maximumexperience,
    jm.annualctc,
    jm.othersalary,
    jm.vacancy,
    jm.location,
    jm.jobid,
    jm.ugqualificationid,
    jm.pgqualificationid,
    jm.otherqualificationid,
    jm.requiteremail,
    jm.organizationTypeid,
    jm.description,
    jm.postingdate,
    i.name,
    dm.ugraduate,
    dm.pgraduate,
    dm.doctorate
   FROM job jm
     JOIN joblist jl ON jm.jobid = jl.id
     JOIN organizationType i ON jm.organizationTypeid = i.id
     JOIN degree dm ON jm.pgqualificationid = dm.id;
create or replace view applicantsprevperformanceview 
as
select sm.name,
       jm.location,
	   jm.designation,
	   p.starttime,
	   p.endtime,
	   rq.interviewstatus,
	   rq.performencertng,
	   rq.behaviourrtng,
	   rq.appearencertng,
	   rq.communicationrtng,
	   rq.ontimertng,
	   jba.userid
	  from placements p
	   join branch sbm on sbm.id=p.branchid
	   join Organization sm on sm.id=sbm.Organizationid
	   join job jm on jm.organizationTypeid=sm.id
	   join  jobapplicants jba on jba.placementid=p.id
	   join requirtement rq on rq.jobapplicantid=jba.id;
--job applicants view
CREATE OR REPLACE VIEW jobapplicantsview AS
 SELECT jb.id AS jobapplicantid,
    jb.userid,
    jb.status AS jobapplicantsstatus,
    p.id AS placementid,
    p.branchid,
    p.starttime,
    p.endtime,
    p.type,
    jm.id AS jpbid,
    jm.jobtittle,
    jm.designation,
    jm.jobdescription,
    jm.minimumexperience,
    jm.maximumexperience,
    jm.annualctc,
    jm.location,
    jm.requiteremail,
    jm.ugraduate,
    jm.pgraduate,
    jm.doctorate,
    cv.id AS contactid,
    cv.firstname,
    cv.lastname,
    cv.email,
    cv.mobile
   FROM jobapplicants jb
     JOIN placements p ON p.id = jb.placementid
     JOIN jobview jm ON jm.id = p.jobid
     JOIN contactdetailsview cv ON cv.userid = jb.userid;
create or replace function fn_getapplicantsprevinterview(param_userid integer)
RETURNS SETOF  applicantsprevperformanceview
as
$$
begin
return query
select * from applicantsprevperformanceview where userid=param_userid;
end
$$
language plpgsql;
CREATE OR REPLACE FUNCTION fn_getjobapplicants(
	param_usersession text)
    RETURNS SETOF jobapplicantsview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from jobapplicantsview jb
where jb.branchid in(select branchid from userProfileview
where id in(select fn_viewuserid from
          fn_viewuserid(param_usersession)));
end;
$BODY$;

CREATE OR REPLACE FUNCTION fn_getapplicantsaggrements(
	param_usersession text)
    RETURNS TABLE(applicantaggrementid integer, annualctc bigint, joiningdate date, shiftid integer, shiftname character varying, days text[], starttime time without time zone, endtime time without time zone, applicantid integer, userid integer, branchid integer, placementid integer, jobtittle character varying, contactid integer, firstname character varying, lastname character varying, email character varying, mobile character varying, applicantaggrstatus character varying, recruitmentid integer, location character varying, functionalarea character varying, requiteremail character varying, companyname character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin 
return query 
select ag.id as applicantaggrementid,ag.annualctc,ag.joiningdate,s.id as shiftid,s.shiftname,s.days,s.starttime,s.endtime,
jbv.jobapplicantid,jbv.userid,jbv.branchid,jbv.placementid,
jbv.jobtittle,jbv.contactid,jbv.firstname,jbv.lastname,jbv.email,jbv.mobile,ag.status as applicantaggrstatus,
r.id as recruitmentid,jbv.location,jbv.functionalarea,jbv.requiteremail,pc.name as companyname
from applicantsaggrements ag join requirtement r on r.id=ag.reqruitmentId
join jobapplicantsview jbv on jbv.jobapplicantid=r.jobapplicantId
join placementview pc on pc.id=jbv.placementid
join shifts s on s.id=ag.shiftid where jbv.userid in(select fn_viewuserid from fn_viewuserid(param_userSession));
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_getappliedjobs(
	param_usersession text)
    RETURNS SETOF jobapplicantsview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from jobapplicantsview where userid=(select fn_viewuserid from fn_viewuserid(param_userSession));
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewjob(
	)
    RETURNS SETOF jobview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from jobview;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewjobbyid(
	param_id integer)
    RETURNS SETOF jobview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from jobview
where Id=param_id;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_getorganizationname(
	param_usersession text)
    RETURNS SETOF userprofileview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query select * from userprofileview where userid=(select fn_viewuserid from fn_viewuserid(param_userSession));
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewrecentjob(
	)
    RETURNS SETOF jobview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from jobview where postingDate > now() - interval '45 days' ;
end;
$BODY$;

CREATE OR REPLACE FUNCTION fn_viewbyorganizationTypeid(
	param_usersession text)
    RETURNS SETOF jobview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query 
select * from jobview where organizationTypeId=(select Organizationid from userprofileview where userid=(select fn_viewuserid from fn_viewuserid(param_userSession)));
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_getjobapplicantdetails(
	param_jobapplicantid integer)
    RETURNS TABLE(jobapplicantid integer, userid integer, placementid integer, branchid integer, ugraduate character varying, pgraduate character varying, doctorate character varying, contactid integer, firstname character varying, lastname character varying, email character varying, mobile character varying, sscpercentage integer, interpercentage integer, ugpercentage integer, pgpercentage integer, otherpercentage integer, experience integer) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select  
jbv.jobapplicantid,jbv.userid,jbv.placementid,jbv.branchid,
jbv.ugraduate,jbv.pgraduate,jbv.doctorate,jbv.contactid,jbv.firstname,jbv.lastname,jbv.email,jbv.mobile,
ac.sscpercentage,ac.interpercentage,ac.ugpercentage,ac.pgpercentage,ac.otherpercentage,ac.experience
from jobapplicantsview jbv join academicdetails ac on ac.userid=jbv.userid
where jbv.jobapplicantid=param_jobapplicantid;
end;
$BODY$;
create or replace function fn_getjobapplicantsSchedule(param_usersession text)
returns setof jobapplicantsview as
$$
begin
return query
select * from jobapplicantsview jb
where jb.jobapplicantsstatus='Attend' and jb.branchid in(select branchid from userProfileview
where id in(select fn_viewuserid from
          fn_viewuserid(param_usersession)));
end;
$$ language plpgsql;
--function for traineeLibraryById
CREATE OR REPLACE FUNCTION fn_viewlibraryRequestsById(params_usersession text,params_id int)
		RETURNS SETOF libraryrequestsview
															   
LANGUAGE plpgsql
    AS $$
begin
    return query
select * from libraryrequestsview 
 where  branchid in(select branchid from userprofileview where
				userid=(select fn_viewuserid from fn_viewuserid(params_usersession)))
																
									and id=params_id;															   
															   
end;
$$;																	
	
CREATE OR REPLACE VIEW hostelsview as
select  h.id as hostelid,
		h.userid as hosteluserid,
		
		h.branchid,h.type,h.facilities,h.sharing,h.blockname,h.roomnum,h.allocationdate,h.rentalperiod,
 h.representativeid,u.lastname as representativelastname,u.firstname as representativefirstname
  ,u.middlename as representativemiddlename,
  s.userid as representativeuserid,
  us.lastname as traineelastname, us.firstname as traineefirstname,
  us.middlename as traineemiddlename,c.place AS branchname
												  
	
 from hostel h join staff s on h.representativeid=s.userid 
 join users u on u.id= s.userid
  join users us on us.id= h.userid
join branch sbm on sbm.id=h.branchid
	join contactdetails c on c.id=sbm.contactdetailsid ;
    CREATE OR REPLACE FUNCTION fn_viewallhostel(
	param_usersession text)
    RETURNS SETOF hostelsview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select * from hostelsview where branchid in(select branchid from userProfileview
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewhosteldetails(
	param_id integer)
    RETURNS SETOF hostelsview
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
 return query select  * from hostelsview    where hostelid =param_id;
end;
$BODY$;
CREATE OR REPLACE VIEW visitingview as
select v.id ,v.outdatetime,v.indatetime,v.branchid,v.userid
from visiting v join users u on u.id=v.userid ;
CREATE OR REPLACE FUNCTION fn_viewallhostelvisiting(
	param_usersession text)
    RETURNS SETOF visitingview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select * from visitingview where branchid in(select branchid from userProfileview
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewhostelvisitdetails(
	param_id integer)
    RETURNS SETOF visitingview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
 return query select  * from visitingview  where visitingid =param_id;
end;
$BODY$;
CREATE OR REPLACE VIEW hostelfeeview as
select  hf.id as hostelfeeid, hf.totalamount,hf.paydate,hf.paymode,hf.paidamount,
hf.dueamount,hf.hostelid,h.traineelastname, h.traineefirstname,
h.traineemiddlename,h.branchname, h.branchid
from hostelfee hf join hostelsview as h on hf.hostelid=h.hostelid ;
CREATE OR REPLACE FUNCTION fn_hostelfeesview(
	param_usersession text)
    RETURNS SETOF hostelfeeview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select * from hostelfeeview where branchid in(select branchid from userProfileview
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewhostelfeedetails(
	param_id integer)
    RETURNS SETOF hostelfeeview  
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
 return query select  * from hostelfeeview   where hostelfeeid =param_id;
end;
$BODY$;
create or replace function fn_gethostelapplylist(param_usersession text)
returns table (admissionnum int,userid int,status varchar,id int,firstname varchar,
			  lastname varchar,email varchar, mobile varchar,sessionname varchar,rolename text) as
			  $$
			  begin
			  return query
select  a.admissionnum,h.traineeid as userid,h.status,h.id,
c.firstname,c.lastname,c.email,c.mobile,cm.sessionname,uu.rolename 
from hostelAllocate h join contactdetailsview c on c.userid=h.traineeid 
 join admisiondetails as a on h.traineeid= a.userid						
join traineebatch sbm on sbm.traineeid=a.admissionnum
join batch bm on bm.id=sbm.batchid
join userprofileview as uu on uu.userid=h.traineeid
join sessionmaster cm on cm.id=bm.sessionid where cm.sessionname not in('dummy')
and h.branchid in(select branchid from userProfileview up
where up.id in(select fn_viewuserid from
		  fn_viewuserid(param_usersession)))
union all
select s.id as admissionnum,h.traineeid as userid,h.status,h.id,s.firstname,s.lastname,
s.email,s.mobile,s.type as sessionname,uuu.rolename
from hostelAllocate h join staffview as s on s.userid=h.traineeid
join userprofileview as uuu on uuu.userid=h.traineeid
and h.branchid in(select branchid from userProfileview up
where up.id in(select fn_viewuserid from
		  fn_viewuserid(param_usersession)));				
						end;
$$ language plpgsql;
create or replace function fn_getuserdetails(param_usersession text) 
returns table(id int,userid int,brnchid int,
			 firstname varchar,lastname varchar,email varchar, mobile varchar) as					
	$$
	begin
	return query
 select a.admissionnum as id,a.userid,a.branchid,a.firstname,a.lastname,a.parentemail as email,a.parentmobile as mobile   from admisionview a where a.userid in(select u.userid from userProfileview u 
where u.id in(select fn_viewuserid 
	from fn_viewuserid(param_usersession)))
and BranchId in(select uu.branchid from userProfileview uu
where uu.id=(select fn_viewuserid from 
		fn_viewuserid(param_usersession)))
union
select s.id,s.userid,s.branchid,s.firstname,s.lastname,s.email,s.mobile from staffview s where s.userid in(select u.userid from userProfileview u
where u.id in(select fn_viewuserid 
	from fn_viewuserid(param_usersession)))
and BranchId in(select uu.branchid from userProfileview uu
where uu.id=(select fn_viewuserid from 
		fn_viewuserid(param_usersession)));
		end;
		$$ language plpgsql;
create or replace function fn_getapprovehostelapplicants(param_usersession text) 
returns table(hstlapplyid int,userid int,brnchid int,status varchar,reqdate date,
			 firstname varchar,lastname varchar,email varchar, mobile varchar,address varchar,place varchar,gender varchar) as					
	$$
	begin
	return query
 select h.id ,h.traineeid as userid,
						h.branchid,h.status,h.reqdate,
						c.firstname,c.lastname,c.email,c.mobile,c.address,c.place,c.gender
from hostelAllocate h join contactdetailsview c on c.userid=h.traineeid						
			where h.branchid in(select branchid from userProfileview 
where id in(select fn_viewuserid from
		  fn_viewuserid(param_usersession))) order by c.firstname asc;	
		end;
		$$ language plpgsql;
CREATE OR REPLACE VIEW projectview AS
 SELECT p.id,
    p.assigneddate,
    p.submissiondate,
    p.type,
    p.details,
    p.teamsize,
    p.projectname,
    p.fee,
    p.category,
    sbm.id AS branchid,
    cd.place
  FROM projects p
     JOIN branch sbm ON p.branchid = sbm.id
     JOIN Organization sm ON sbm.Organizationid = sm.id
     JOIN contactdetails cd ON cd.id = sbm.contactdetailsid;
create or replace function fn_viewprojects(sessionparam character varying)
	 RETURNS SETOF projectview 
	 as
	 $$
	 begin
	 return query select * from projectview
         where branchid in(select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid(sessionparam)));
	 end
	 $$
	 language plpgsql;
create or replace function fn_projectbyid(param_id int) 
returns setof projectview
as
$$
begin
return query select * from projectview where id=param_id;
end
$$
language plpgsql;
CREATE OR REPLACE VIEW placementview AS
 SELECT p.id,
    p.jobid,
    p.starttime,
    p.endtime,
    p.type,
    p.details,
    p.positions,
    p.package,
    p.category,
    sbm.id AS branchid,
    cd.place,
    jm.jobtittle,
    jm.jobdescription,
    jm.designation,
    jm.minimumexperience,
    jm.maximumexperience,
    jm.annualctc,
    jm.vacancy,
    jm.othersalary,
    jm.location,
    jm.ugqualificationid,
    jm.pgqualificationid,
    jm.otherqualificationid,
    jm.requiteremail,
    jm.postingdate,
    sm.name,
    sm.regdate,
    dm.ugraduate,
    dm.pgraduate,
    dm.doctorate
   FROM placements p
     JOIN branch sbm ON p.branchid = sbm.id
     JOIN Organization sm ON sbm.Organizationid = sm.id
     JOIN contactdetails cd ON cd.id = sbm.contactdetailsid
     JOIN job jm ON p.jobid = jm.id
     JOIN degree dm ON dm.id = jm.ugqualificationid
  WHERE p.category::text = 'placement'::text;
CREATE OR REPLACE FUNCTION fn_viewplacements(
	sessionparam character varying)
    RETURNS SETOF placementview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
	 begin
	 return query select * from placementview
 where branchid in(select branchid from userprofileview where id=(select fn_viewuserid from fn_viewuserid(sessionparam)));
	 end
	 
$BODY$;
create or replace function fn_viewallsports()
returns table(Ids integer , sportsTypes varchar)
as
$$
begin
return query
select Id,sportsType from sports;
end;
$$
language plpgsql;
CREATE OR REPLACE VIEW sportsview AS
 SELECT sm.id,
    s.sportstype,
    sm.sportsid,
    sm.departmentid,
    sm.sessionid,
    sm.fromdate,
    sm.starttime,
    sm.endtime,
    sm.todate,
    sm.kitid,
	sk.branchId,
    d.name,
    d.staffid,
    d.courseid
   FROM sportsmanagement sm
     JOIN sports s ON s.id = sm.sportsid
     JOIN departments d ON d.id = sm.departmentid
     JOIN sportskit sk ON sk.id = sm.kitid;
CREATE OR REPLACE FUNCTION fn_viewsports(
	param_usersession text)
    RETURNS SETOF sportsview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from sportsview
where branchid in(select branchid from userProfileview
where id in(select fn_viewuserid from
          fn_viewuserid(param_usersession)));
end
$BODY$;
create or replace function fn_viewsportdetailsbyid(param_id integer)
RETURNS SETOF sportsview
as
$$
begin
return query
select * from sportsview
where Id=param_id;
end;
$$
language plpgsql;
CREATE OR REPLACE FUNCTION fn_viewsportskit(
	)
    RETURNS TABLE(ids integer, sportsids integer) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select Id, sportsId from sportskit;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewsportdetails(
	param_sportsid integer)
    RETURNS SETOF sportsview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from sportsview
where sportsId=param_sportsid;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewalllibrarydetails(
	params_bookname varchar)
    RETURNS table(totalbooks bigint,maxid int,bookname varchar ,authorname varchar,issuedbooks bigint
				  )
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
declare x bigint;
declare n varchar;
declare o varchar;
declare z int;
begin
select count(l.id)  from library l where l.bookname= params_bookname into x;
select max(l.id)  from library l where l.bookname= params_bookname and l.status='InActive' into z;
select l.bookname from library l where l.bookname=params_bookname  into n;
select l.authorname from library l where l.bookname=params_bookname  into o;
return query
select x,z,n,o,count(sl.id)  from traineelibrary sl join library l on l.id=sl.bookid
where l.bookname=params_bookname and sl.status='Active' ; 
					 
										end;
$BODY$;

CREATE OR REPLACE FUNCTION fn_viewpermissionsbyroles(
	param_rolename varchar)
    RETURNS table(id int,rolename varchar,componentname varchar,read boolean,write boolean,modules varchar,components varchar,routerlink varchar,componentid int)																			  
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from permissions p join components c on c.id= cast(p.componentname as int)
where cast(p.rolename as int)=( select r.id from rolemaster r where r.rolename=param_rolename);
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewpermissiondetails(
	param_id integer)
    RETURNS SETOF permissions 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from permissions
where id=param_Id;
end;
$BODY$;
CREATE OR REPLACE VIEW librarytransactionsview AS
 SELECT stlby.bookid,
    stlby.userid,
    stlby.issuedate,
    stlby.returndate,
    lby.branchid,
    lby.departmentid,
    lby.bookname,
    lby.authorname,
    lby.description,
    lby.segregation,
    us.firstname,
    us.middlename,
    us.lastname,
    lby.status,
    stlby.id,
	stlby.status as issuedstatus
   FROM librarytransactions stlby
     JOIN library lby ON stlby.bookid = lby.id
     JOIN users us ON us.id = stlby.userid;
CREATE OR REPLACE FUNCTION fn_viewlibrarytransactions(
	params_usersession text)
    RETURNS table(bookname varchar,no_of_books bigint,authorname varchar,
				  segregation varchar) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
 return query 
 select  lv.bookname,lv.authorname,lv.segregation 
from librarytransactionsview as lv  where lv.status='Active'
and lv.branchid in(select branchid from userprofileview where id=(
	 select fn_viewuserid from fn_viewuserid(params_usersession))) 
group by (lv.bookname,lv.authorname,lv.segregation); 
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewlibrarytransactionsbyid(
	params_id integer)
    RETURNS SETOF librarytransactionsview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
 return query 
 select  * from librarytransactionsview where userid=params_id ;
										end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_countdaysforlibrary(
	param_mindate date,param_id int)
    RETURNS TABLE(delaydays int,penalty int,Fine integer) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
declare x int;
declare y int;
begin
select current_date-(param_mindate+1)::date into x;
select sbv.penalty from traineelibraryview  sbv where sbv.id=param_id into y;
return query
select x,y,(x*y);
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewtraineemarks(
	param_usersession text)
    RETURNS SETOF traineemarksview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select * from traineemarksview where branchid in(select branchid from userProfileview 
where rolename='BranchAdmin'and id=(select fn_viewuserid from fn_viewuserid(param_usersession)))
union 
select * from traineemarksview where
branchid in(select branchid from userProfileview 
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)))													  ;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_idcard(
	param_usersession text)
    RETURNS TABLE(userid integer, firstname character varying, middlename character varying, lastname character varying, fathername character varying, rolename text, address character varying, mobile character varying, name character varying, admissionnum integer, parentmobile character varying, sessionname character varying, staffid integer, Organizationid integer) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select
   up.id as userid, up.firstname,up.middlename,up.lastname,up.fathername,up.rolename,up.address,
	up.mobile,up.name,0 as admissionnum ,'0' as parentmobile,'0' as sessionname, sm.id as staffid,up.Organizationid
    from userprofileview up join staff sm on up.id=sm.userid
	where up.rolename!='Trainee' and up.id=(select fn_viewuserid from fn_viewuserid(param_usersession))  
union
 select
  up.id as userid, up.firstname,up.middlename,up.lastname,up.fathername,up.rolename,up.address,
	up.mobile,up.name,a.admissionnum,a.parentmobile,cm.sessionname, 0 as staffid,up.Organizationid
    from userprofileview as up join admisiondetails as a on up.userid= a.userid
   join branchsession bcm on bcm.branchid=up.branchid
join sessionmaster cm on cm.id=bcm.sessionid
where up.rolename='Trainee' and up.id=(select fn_viewuserid from fn_viewuserid(param_usersession));															
	
end;
$BODY$;
create or replace function fn_viewfinancialreports(param_usersession text)
    RETURNS  table(feeamount double precision,concession double precision,paidamount double precision ) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
declare x double precision;
declare y double precision;
begin
select sum(adv.feeamount)from admisionview adv where adv.branchid in(select branchid from userprofileview where id=(select 
fn_viewuserid 
from fn_viewuserid(param_usersession) )) into x;
		  select sum(admv.concession)from admisionview admv where admv.branchid in(select branchid from userprofileview where 
id=(select fn_viewuserid 
from fn_viewuserid(param_usersession))) into y;
				 return query									  
	select x,y,sum(fpv.paidamount)from feepaymentsview  fpv where fpv.branchid in(select branchid from userProfileview
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$BODY$;
	
CREATE OR REPLACE FUNCTION public.fn_viewfinancialreportsbybranchsession(
	param_branchsession integer)
    RETURNS TABLE(feeamount double precision, concession double precision, paidamount double precision) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
												  return query
	select sum(ad.feeamount), sum(ad.concession),sum(fpv.paidamount) from traineebatchview sbv 
												  join admisionview ad on ad.admissionnum=sbv.admissionnum
												  join feepaymentsview fpv on sbv.admissionnum=fpv.admissionnum
													  where sbv.sessionid=param_branchsession;
												  end;
												  $BODY$;
 CREATE OR REPLACE FUNCTION fn_viewuserbybranchid(
	param_usersession text)
    RETURNS SETOF userprofileview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from userProfileview
where branchid in(select branchid from userProfileview where id=(select fn_viewuserid from fn_viewuserid(param_usersession)) )  order by firstname asc;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewcontactdetailsbyuserid(
	params_userid integer)
    RETURNS SETOF contactdetailsview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from contactdetailsview
where userId=params_userid;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewbranchmapping(
    param_usersession text)
    RETURNS table(userdeviceid varchar,branchid int,place varchar)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
 return query
select b.userdeviceid,b.branchid,cd.place from branchmapping b
join branch sbm     on b.branchid=sbm.id
join contactdetails cd on sbm.contactdetailsid= cd .id
where b.branchid in(select upv.branchid from userprofileview upv
where upv.id=(select fn_viewuserid from fn_viewuserid(param_usersession)));
 end;
$BODY$;
	create or replace function fn_viewstafflogs(param_usersession text)
    RETURNS  table(firstname varchar,lastname varchar,rolename text,
				   creationtime timestamp,createdby int,loggedtime timestamp) 
    LANGUAGE 'plpgsql'
COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select  lgw.firstname,lgw.lastname,lgw.rolename,lgw.creationtime,
app.createdby,ugl.loggedtime
 from loginview lgw join applicants app
 on lgw.id=app.applicantid  join userlogins ugl on lgw.id=ugl.userid
 where lgw.rolename!='Trainee' and app.createdby in
 (select fn_viewuserid from fn_viewuserid(param_usersession));
end;
$BODY$;
create or replace function fn_viewtraineelogs(param_usersession text)
    RETURNS  table(firstname varchar,lastname varchar,rolename text,
				   creationtime timestamp,createdby int,loggedtime timestamp) 
    LANGUAGE 'plpgsql'
COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select  lgw.firstname,lgw.lastname,lgw.rolename,lgw.creationtime,
										  app.createdby,ugl.loggedtime
 from loginview lgw join applicants app
  on lgw.id=app.applicantid  join userlogins ugl on lgw.id=ugl.userid
 where lgw.rolename='Trainee' and app.createdby in
(select fn_viewuserid from fn_viewuserid(param_usersession));
  end;
 $BODY$;
 create or replace function fn_getallreligion()
		RETURNS SETOF religion
    LANGUAGE plpgsql
    AS $$
begin
 return query select * from Religion  order by name ASC;
end;
$$;		
CREATE OR REPLACE FUNCTION fn_viewmessagesbyconversationid(
	param_conversationid integer)
    RETURNS TABLE(id integer, messages text, fromuser character varying, touser character varying, conversationid integer, status character varying, convoid integer, suppportid integer, userid character varying, convostatus character varying, currentdate date, username character varying, supporttype character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
 begin
 return query
select * from  messagestable m join conversationtable ct on ct.id=m.conversationid where m.conversationid=param_conversationid order by m.id;
 end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewconversationbysupportid(
	param_supportid integer)
    RETURNS TABLE(id integer, supportid integer, userid character varying, status character varying, currentdate date, name character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
 begin
 return query
select * from  conversationtable m where m.supportid=param_supportid;
 end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewOrganizations()
RETURNS SETOF Organizationview as
$$
begin
return query
select * from Organizationview  where name not in('dummy Organization');
end;
$$ language plpgsql;
CREATE OR REPLACE FUNCTION fn_getsessionbyBranch(param_branchid int)
RETURNS table(sessionid int,branchid int,sessionname varchar) as
$$
begin
return query
select bcm.sessionid,bcm.branchid,c.sessionname
from branchsession bcm join sessionmaster c on bcm.sessionid=c.id 
						where bcm.branchid=param_branchid;
end;
$$ language plpgsql;
CREATE OR REPLACE FUNCTION fn_viewtcapplicants(
	param_session text)
    RETURNS TABLE(transferid int,traineeid integer, frombranchid integer, statuss character varying, firstname character varying, sessionname character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select t.id as transferid,t.traineeid,t.frombranchid,t.status as statuss,u.firstname,c.sessionname 
from transfertrainee t join userprofileview u on u.id=t.traineeid 
join sessionmaster c on c.id=t.sessionid
 where (status='Approve' OR status='Accept') and  t.tobranchid in(select branchid from userProfileview 
where id in(select fn_viewuserid from
		  fn_viewuserid(param_session)));	
end;						
$BODY$;
CREATE OR REPLACE FUNCTION fn_getapplytransfer(
	param_usersession text)
    RETURNS TABLE(transferid int,userid integer, frombranchid integer, status character varying, firstname character varying, lastname character varying, email character varying, mobile character varying, sessionname character varying, admisionstatus character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query select  t.id as transferid,t.traineeid as userid,t.frombranchid,t.status,
up.firstname,up.lastname,up.email,up.mobile,cm.sessionname,a.status as admisionstatus 
from transfertrainee t join userprofileview up on up.id=t.traineeid 
 join admisiondetails as a on up.userid= a.userid 						
join traineebatch sbm on sbm.traineeid=a.admissionnum
join batch bm on bm.id=sbm.batchid
join sessionmaster cm on cm.id=bm.sessionid where cm.sessionname not in('dummy')
and (t.status='Accept' or (a.status='ApplyTc' and (t.status='Applied' or t.status='Approve' )))
and up.branchid in(select branchid from userProfileview 
where id in(select fn_viewuserid from
		  fn_viewuserid(param_usersession)));
						
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewFrmBranchApprove(param_session text)
RETURNS table(userid integer, frombranchid integer, status varchar, 
				  firstname varchar, lastname varchar, 
				  email varchar, mobile varchar, sessionname varchar) as
$$
begin
return query
select  t.traineeid as userid,t.frombranchid,t.status,
up.firstname,up.lastname,up.email,up.mobile,cm.sessionname 
from transfertrainee t join userprofileview up on up.id=t.traineeid 
 join admisiondetails as a on up.userid= a.userid						
join traineebatch sbm on sbm.traineeid=a.admissionnum
join batch bm on bm.id=sbm.batchid
join sessionmaster cm on cm.id=bm.sessionid where cm.sessionname not in('dummy')
and t.status='Approve'
and t.traineeid=(select fn_viewuserid from
		  fn_viewuserid(param_session))
and up.branchid in(select branchid from userProfileview 
where id in(select fn_viewuserid from
		  fn_viewuserid(param_session)));
end;						
$$ language plpgsql	;
create or replace view billview as
select
b.id,
b.userid,
b.Organizationid,
b.buydate,
b.enddate,
b.amount,
b.status,
upv.firstname,
upv.lastname,
upv.name
from billing b join userprofileview upv on b.userid=upv.userid;
CREATE OR REPLACE VIEW holidaycalenderview AS
 SELECT holiday.id,
    holiday.branchid,
    holiday.holidaydate as start,
	holiday.holidayenddate + integer '1' as end,
    holiday.occation as title,
    sbr.Organizationid,
    sbr.startdate,
    sbr.status,
    sbr.contactdetailsid,
    sch.name AS Organizationname,
    sch.type,
    sch.status AS Organizationstatus,
    sch.establishedyear,
    sch.regdate,
    cd.country,
    cd.state,
    cd.city,
    cd.place,
    cd.address,
    cd.uniqueidentificationno,
    cd.email,
    cd.mobile,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    u.status AS userstatus,
    c.place AS branchname
   FROM holiday holiday
     JOIN branch sbr ON holiday.branchid = sbr.id
     JOIN contactdetails c ON sbr.contactdetailsid = c.id
     JOIN Organization sch ON sbr.Organizationid = sch.id
     JOIN contactdetails cd ON sch.contactdetailsid = cd.id
     JOIN users u ON cd.userid = u.id;	
     CREATE OR REPLACE FUNCTION fn_viewholidaycalender(
	param_usersession text)
    RETURNS SETOF holidaycalenderview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
    select * from holidaycalenderview
    where branchid in(select branchid from userProfileview 
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)))
union all												  
 select * from holidaycalenderview
    where Organizationid=(select Organizationid from userProfileview 
where rolename='OrganizationAdmin' and id=(select fn_viewuserid from fn_viewuserid(param_usersession)))												  
												  ;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_tobranchaccepttrainees(
	param_usersession text)
    RETURNS TABLE(id integer, userid integer, frombranchid integer, tobranchid integer, sessionid integer, status character varying, firstname character varying, middlename character varying, lastname character varying, gender character varying, fathername character varying, mothername character varying, fatheroccupation integer, place character varying, address character varying, uniqueidentificationno character varying, email character varying, mobile character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
 begin 
 return query 
 select t.id,t.traineeid as userid,t.frombranchid,t.tobranchid,t.sessionid,t.status,
 c.firstname,c.middlename,c.lastname,c.gender,c.fathername,c.mothername,
 c.fatheroccupation,c.place,c.address,c.uniqueidentificationno,c.email,c.mobile
 from transfertrainee t
 join contactdetailsview c on c.id=t.traineeid  where t.status='Accept'
and  t.tobranchid in(select branchid from userProfileview u
  where u.id in(select fn_viewuserid from
		  fn_viewuserid(param_usersession))); 
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_getdepartments(
	param_usersession text)
    RETURNS TABLE(id integer, departmentname character varying, staffid integer, courseid integer, coursename character varying, staffname character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
   select d.id,d.name as departmentname,d.staffid,d.courseid,crs.coursename,u.firstname
                  from departments d
                             join courses crs on crs.id=d.courseid
                             join staff sm on sm.userid=d.staffid
                             join users u on u.id=sm.userid
                             where sm.branchid in(select upv.branchid from userProfileview upv
where upv.id in(select fn_viewuserid from
          fn_viewuserid(param_usersession))) order by departmentname asc;
end;
$BODY$;
	
CREATE OR REPLACE VIEW transporttransferstopchargeview AS
 SELECT tsf.id,
    tsf.amount,
    tsf.routeid,
    tsf.stopid,
    trm.name AS routename,
    tsm.id AS stopsid,
    trm.startpoint,
    trm.endpoint,
    tsm.stopname,
    tsm.arrivaltime,
    tsm.departuretime,
    b.branchid,
	utp.id as transportid,
		utp.userid,	utp.paidamount,
		utp.paiddate,
		utp.due,
		utp.status as transportstatus
 FROM transportstopfee tsf
     JOIN transportstop tsm ON tsf.stopid = tsm.id
     JOIN transportroute trm ON tsm.routeid = trm.id
     JOIN bus b ON trm.busid = b.id
     JOIN branch sbm ON b.branchid = sbm.id
     JOIN Organization sch ON sbm.Organizationid = sch.id
     JOIN contactdetails cd ON sch.contactdetailsid = cd.id
     JOIN users u ON cd.userid = u.id
	 join usertransportpayment utp on u.id=utp.userid ;
    												   
CREATE OR REPLACE FUNCTION fn_viewtransportfeetransaction(
	param_usersession varchar)
    RETURNS SETOF transporttransferstopchargeview  
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
select * from transporttransferstopchargeview tf where tf.id in(select fn_viewuserid from fn_viewuserid (param_usersession));
end;
$BODY$;
CREATE OR REPLACE VIEW cartview AS
 SELECT s.id,
    s.productname,
    s.productprice,
    s.category,
    s.sessionid,
    s.description,
    s.available,
    s.productdate,
    cm.sessionname,
    ct.quantity,
    ct.id AS cartid,
	ct.cartallocationid,
	ct.status
   FROM seller s
     JOIN sessionmaster cm ON cm.id = s.sessionid
     JOIN cart ct ON ct.sellerid = s.id;
	 
CREATE OR REPLACE VIEW shoppingview AS
 SELECT s.id,
    s.productname,
    s.productprice,
    s.category,
    s.sessionid,
    s.description,
	s.available,
	s.productdate,
    cm.sessionname
   FROM seller s
     JOIN sessionmaster cm ON cm.id = s.sessionid;
CREATE OR REPLACE FUNCTION fn_viewshoppingproducts(
	)
    RETURNS SETOF shoppingview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from shoppingview;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewshoppingproductsbyid(
	param_id integer)
    RETURNS SETOF shoppingview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from shoppingview where id=param_id;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewusersproducts(
	)
    RETURNS SETOF shoppingview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from shoppingview where available ='true';
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewcartproducts(
	)
    RETURNS SETOF cartview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from cartview;
end;
$BODY$;
create or replace function fn_getsessionbyDeparment(param_departmentid int)
returns table (departmentid int,departmentname varchar,sessionid int,sessionname varchar)
as
$$
begin
return query 
	select d.id as departmentid,d.name as departmentname,bcm.sessionid,cm.sessionname 
from departments d join branchsession bcm 
on bcm.departmentid=d.id join sessionmaster cm on cm.id=bcm.sessionid
where d.id=param_departmentid;
end;						
$$ language plpgsql;
create or replace function fn_getBtachesbysession(param_sessionId int)
returns table (sessionid int,sessionname varchar,batchid int,batchname varchar)
as
$$
begin
return query 
	select cm.id as sessionid,cm.sessionname,bm.id as batchid,bm.name as batchname
		from sessionmaster cm join batch bm on cm.id=bm.sessionid
		where bm.name !='dummy' and cm.id=param_sessionId;
end;						
$$ language plpgsql;	
	create or replace function fn_getBranchMessages(param_usersession text)
returns table (id int,departmentname varchar,sessionname varchar,batchname varchar,
			   messagetype varchar,message text,senddate date)
as
$$
begin
return query 
	select bm.id,dp.name as departmentname,cm.sessionname,btm.name as batchname,
      bm.messagetype,bm.message,bm.date as senddate
	from bulkmessage bm join departments dp on bm.departmentid=dp.id	
		join sessionmaster cm on cm.id=bm.sessionid	
        join batch btm on btm.id=bm.batchid
		join branchsession bcm on bcm.sessionid=cm.id				
		where bcm.branchid in(select branchid from userProfileview u
  where u.id in(select fn_viewuserid from
		  fn_viewuserid(param_usersession)));
end;						
$$ language plpgsql;
create or replace function fn_getBilling(param_userSession text) returns table
(uid int,lid varchar,password text,firstname varchar,middlename varchar,lastname varchar,dob date,
gender varchar,fathername varchar,mothername varchar,fatheroccupation integer,caste varchar,
subcaste varchar,religion integer,nationality integer,userstatus varchar,roleid int,rolename text,
creationtime timestamp without time zone,modifiedtime timestamp without time zone,rolestatus boolean,
contactid int,contactuserid int,country int,state int,city int,place varchar,address varchar,
uniqueidentificationno varchar,email varchar,mobile varchar,schlid int,name varchar,type varchar,
establishedyear int,regdate date,Organizationcontact int,branchid int,startdate date,branchcontact int,usid int,
id int,usrid int,schid int,buydate timestamp without time zone,enddate timestamp without time zone,
amount int,status varchar)
as
$$
begin
return query select upv.id as uid,upv.loginid as lid,upv.password,upv.firstname,upv.middlename,upv.lastname,
upv.dob,upv.gender,upv.fathername,upv.mothername,upv.fatheroccupation,upv.caste,upv.subcaste,upv.religion,
upv.nationality,upv.userstatus,upv.roleid,upv.rolename,upv.creationtime,upv.modifiedtime,upv.rolestatus,
upv.contactid,upv.contactuserid,upv.country,upv.state,upv.city,upv.place,upv.address,
upv.uniqueidentificationno,upv.email,upv.mobile,upv.Organizationid as schlid,upv.name,upv.type,upv.establishedyear,
upv.regdate,upv.Organizationcontact,upv.branchid,upv.startdate,upv.branchcontact,upv.userid as usid,
b.id,b.userid as usrid,b.Organizationid as schid,b.buydate,b.enddate,b.amount,b.status
from userprofileview as upv join billing as b on upv.Organizationid=b.Organizationid where upv.userid=(select fn_viewuserid from fn_viewuserid(param_userSession));
end;
$$
language plpgsql;
CREATE OR REPLACE FUNCTION fn_viewmailbox()
    RETURNS SETOF mailbox 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
    return query
select * from mailbox ;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewtotalreports(
    param_admissionnum integer,
    param_appraisalid integer)
    RETURNS TABLE(traineeid integer, appraisalname character varying, totalmarks bigint, totalgainedmarks bigint)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
declare x bigint;
declare y varchar;
declare z int;
begin
select ((sum(smv.marks))) from traineemarksview smv where
smv.traineeid =param_admissionnum and smv.appraisalid=param_appraisalid group by(smv.appraisalid) into x;
select smv.appraisalname from traineemarksview smv where
smv.traineeid =param_admissionnum and smv.appraisalid=param_appraisalid into y group by(smv.appraisalname,smv.appraisalid);
select smv.traineeid from traineemarksview smv where
smv.traineeid =param_admissionnum and smv.appraisalid=param_appraisalid group by(smv.appraisalid,smv.traineeid) into z;
             return query
select z,y,((sum(smv.totalmarks))),x from traineemarksview smv where
smv.traineeid =param_admissionnum and smv.appraisalid=param_appraisalid group by(smv.appraisalid)  ;
             end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_getOrganizationadmin(
    param_usersession text)
    RETURNS SETOF userprofileview
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
return query select * from userprofileview where userid=(select fn_viewuserid from fn_viewuserid(param_userSession));
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewtotalreportsBytrainee(
    param_admissionnum integer,
    param_appraisalid integer)
    RETURNS TABLE(traineeid integer,name varchar,marks double precision)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
             return query
select smv.traineeid,smv.name,smv.marks from traineemarksview smv where
smv.traineeid =param_admissionnum and smv.appraisalid=param_appraisalid   ;
             end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_addleavemaster(
        type varchar(30))
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
AS $BODY$
declare n int;
begin
    Insert into leavemaster
        (type
        )
    Values(type)returning Id into n;
    return n;
end
$BODY$;
	
CREATE OR REPLACE FUNCTION fn_viewleavemaster()
    RETURNS TABLE(id int,type varchar(30)) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
 return query
																																   
select l.id,l.type from leavemaster l;
 end;
$BODY$;
				
CREATE OR REPLACE FUNCTION fn_getleavepattern(
	param_id integer)
    RETURNS TABLE(id integer, patternid integer, leavetype varchar, numofdays double precision,type varchar) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
 return query select l.id, l.patternid, l.leavetype, l.numofdays, l.type from leavepattern l
 where l.patternId=param_id;
end;
$BODY$;
CREATE OR REPLACE VIEW gatepassview AS
 select gtp.id,gtp.userid,gtp.description,gtp.indate,gtp.intime,gtp.status,cd.place,
 gtp.branchid,us.firstname,us.lastname
  from gatepass gtp 
  JOIN users us ON us.id = gtp.userid
   JOIN branch scbm ON scbm.id = gtp.branchid
  JOIN contactdetails cd ON cd.id = scbm.contactdetailsid ;
  CREATE OR REPLACE FUNCTION fn_viewgatepass(
	params_usersession text)
    RETURNS setof gatepassview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
 return query 
 select * from gatepassview as gtp  where gtp.branchid in(select branchid from userprofileview where rolename='BranchAdmin' and id=(
select fn_viewuserid from fn_viewuserid(params_usersession))) 
	union all
 select * from gatepassview as gtp  where  gtp.branchid in(select branchid from userprofileview where rolename!='BranchAdmin' and id=(
select fn_viewuserid from fn_viewuserid(params_usersession))) 
				and gtp.userid=(select fn_viewuserid from fn_viewuserid(params_usersession));
										
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewgatepassById(params_id int)
RETURNS setof gatepassview
As
$$
begin
 return query
select   *from gatepassview where id=params_id;
end;
$$
language plpgsql;
create or replace function fn_getDriverAllocateBusDetails(param_usersession text)
returns table (id int,userid int,allocationdate date ,status varchar,firstname varchar,
			  lastname varchar,place varchar,address varchar,mobile varchar,
			   regnum varchar,branchid int,description varchar) as
$$
begin 
return query 
select  bdr.id,bdr.driverid as userid,bdr.allocationdate,bdr.status,
cv.firstname,cv.lastname,cv.place,cv.address,cv.mobile,bm.regnum,bm.branchid,bm.description
from busdrvalocation bdr
join contactdetailsview cv on cv.userid=bdr.driverid
join bus bm on bm.id=bdr.busid
where bdr.driverid=(select fn_viewuserid from fn_viewuserid(param_usersession));
end;
$$ language plpgsql;
CREATE OR REPLACE VIEW eventscalenderview AS
 SELECT ev.id,
 ev.name,
 ev.departmentId,
 ev.startTime,
 ev.endTime,
 ev.userId,
 ev.description,
 ev.venue,
    ev.branchid,
    ev.fromDate AS start,
    ev.toDate + 1 AS "end",
    sbr.Organizationid,
    sbr.startdate,
    sbr.status,
    sbr.contactdetailsid,
    sch.name AS Organizationname,
    sch.type,
    sch.status AS Organizationstatus,
    sch.establishedyear,
    sch.regdate,
    cd.country,
    cd.state,
    cd.city,
    cd.place,
    cd.address,
    cd.uniqueidentificationno,
    cd.email,
    cd.mobile,
    u.firstname,
    u.middlename,
    u.lastname,
    u.dob,
    u.gender,
    u.fathername,
    u.mothername,
    u.fatheroccupation,
    u.caste,
    u.subcaste,
    u.religion,
    u.nationality,
    u.status AS userstatus,
    c.place AS branchname
   FROM events as ev
     JOIN branch sbr ON ev.branchid = sbr.id
     JOIN contactdetails c ON sbr.contactdetailsid = c.id
     JOIN Organization sch ON sbr.Organizationid = sch.id
     JOIN contactdetails cd ON sch.contactdetailsid = cd.id
     JOIN users u ON cd.userid = u.id;
     CREATE OR REPLACE FUNCTION fn_vieweventscalender(
	param_usersession text)
 RETURNS SETOF eventscalenderview   
AS
$$
begin
   return query
    select * from eventscalenderview
    where branchid in(select branchid from userProfileview 
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)))
union all												  
 select * from eventscalenderview
    where Organizationid=(select Organizationid from userProfileview 
where rolename='OrganizationAdmin' and id=(select fn_viewuserid from fn_viewuserid(param_usersession)))												  
												  ;
end;
 $$
 language plpgsql;
CREATE OR REPLACE VIEW public.feestructurepatternview AS
 SELECT fsp.id,
    fs.sessionid,
    fs.feename,
    fs.amount,
    fs.feepattern AS feestructurepattern,
    fs.optional,
    fs.emi,
    sm.sessionname,
    fsp.feepattern,
    fsp.amount AS pattrenamount,
    bs.branchid
   FROM feestructurepattern fsp
     JOIN feestructure fs ON fsp.feestructureid = fs.id
     JOIN branchsession bs ON fs.sessionid = bs.id
     JOIN sessionmaster sm ON sm.id = bs.sessionid;






CREATE OR REPLACE VIEW public.feestructureview AS
 SELECT fs.id,
    fs.sessionid,
    fs.feename,
    fs.amount,
    fs.feepattern,
    fs.optional,
    fs.emi,
    sm.sessionname,
    bs.branchid
   FROM feestructure fs
     JOIN branchsession bs ON fs.sessionid = bs.id
     JOIN sessionmaster sm ON sm.id = bs.sessionid;

     
CREATE OR REPLACE FUNCTION fn_viewdepartment(param_usersession text)
    RETURNS TABLE(dep_id integer,dep_name character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
 return query
 select  id,name  from  department where branchid in
 (select branchid from userprofileview where id=
 (select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$BODY$;			 
		
        CREATE OR REPLACE FUNCTION fn_getshiftbybranch(
	param_branchid integer)
    RETURNS TABLE(shiftid integer, branchid integer, shiftname character varying, days text[], starttime time without time zone, endtime time without time zone) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select s.id as shiftid,s.branchid,s.shiftname,s.days,s.starttime,s.endtime
from shifts s join branch sbm on sbm.id=s.branchid 
						where sbm.id=param_branchid;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_getshiftbyid(
	param_shiftid integer)
    RETURNS TABLE(param_id integer, param_branchid integer, param_shiftname character varying, param_days text[], param_starttime time without time zone, param_endtime time without time zone,
				 param_graceintime time without time zone,param_graceouttime time without time zone,
				 param_optional boolean) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select *
from shifts where id=param_shiftid;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_getallshifts()
    RETURNS TABLE(param_id integer, param_branchid integer, param_shiftname character varying, param_days text[], param_starttime time without time zone, param_endtime time without time zone, param_graceintime time without time zone, param_graceouttime time without time zone, param_optional boolean) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select *
from shifts ;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewtraineeTransferStatus(param_session text)
RETURNS table(traineeid int,frombranchid int,statuss varchar,firstname varchar,sessionname varchar) as
$$
begin
return query
select t.traineeid,t.frombranchid,t.status as statuss,cv.firstname,c.sessionname
from transfertrainee t join contactdetailsview cv on cv.userid=t.traineeid 
join sessionmaster c on c.id=t.sessionid
 where  t.traineeid=(select fn_viewuserid from fn_viewuserid(param_session));	
end;						
$$ language plpgsql;
																   
CREATE OR REPLACE VIEW halfloginrequestview AS
 SELECT 
hlr.id,
hlr.userid,
hlr.requesttime,
hlr.description,
hlr.status,
u.firstname,
u.lastname
   FROM halfloginrequets hlr
     JOIN users u ON u.id = hlr.userid
     JOIN contactdetails cd ON cd.userid = u.id;																   
																   
	
CREATE OR REPLACE FUNCTION fn_viewHalfLoginRequests(
	param_usersession text)
    RETURNS table(hid int,userid int,userfirstname varchar,userlastname varchar,requesttime time without time zone,description varchar,status varchar)
	LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select hlv.id,hlv.userid,upv.firstname,upv.lastname,hlv.requesttime,hlv.description,hlv.status from halfloginrequestview hlv join userprofileview upv
			on upv.id=hlv.userid													   
		where upv.branchid in(select branchid from userprofileview where id=
		(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$BODY$;	
	
    CREATE OR REPLACE VIEW payrollview AS
 SELECT pr.id,
    pr.startrange,
    pr.endrange,
    pr.name,
    pr.amount,
    pr.branchid
   FROM payroll pr
     JOIN branch sb ON pr.branchid = sb.id;
CREATE OR REPLACE FUNCTION fn_viewpayroll(
	param_usersession text)
    RETURNS SETOF payrollview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from  payrollview where branchid in(select branchid from userprofileview where id=
		(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$BODY$;
CREATE OR REPLACE VIEW departmentpoliciesview AS
 SELECT dp.id,dp.departmentid,
    dp.userid,
    dp.allowancename,
    dp.amount,
    dp.type,
	d.branchid,
	u.firstname,u.lastname,d.name
   FROM departmentpolicies dp
     JOIN department d ON d.id = dp.departmentid
join users u on u.id=dp.userid;
	
CREATE OR REPLACE FUNCTION fn_viewdepartmentpolicies(
	param_usersession text)
    RETURNS SETOF departmentpoliciesview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from  departmentpoliciesview where branchid in(select branchid from userprofileview where id=
		(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$BODY$;
create or replace view paydetailsview as 
							select ad.bankname,ad.accountnumber,ad.pannumber,ad.pfnumber,pd.userid,u.firstname,u.lastname,pd.grosssalary as salary,pd.netsalary as grosssalary,pd.absent,pd.present,pd.weekoff,p.status as grosssalarystatus,p.extraallowancesstatus,pd.month
						from paydetails pd 
							join payslip p on p.userid=pd.userid 
							join users u on u.id=pd.userid
							join staff s on s.userid=pd.userid 
							join accountdetails ad on ad.staffid=s.id;
CREATE OR REPLACE VIEW tdsview AS
 SELECT td.id,
    td.tdsstartrange,
    td.tdsendrange,
    td.tdsname,
    td.tdsamount,
    td.branchid
   FROM tds td
     JOIN branch sb ON td.branchid = sb.id;
CREATE OR REPLACE FUNCTION fn_viewtds(
	param_usersession text)
    RETURNS SETOF tdsview 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from  tdsview where branchid in(select branchid from userprofileview where id=
		(select fn_viewuserid from fn_viewuserid(param_usersession)));
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_viewPfReport(
	param_usersession text)
    RETURNS table(pf double precision,branchid int,branchname varchar,userid int,firstname varchar,lastname varchar)
	LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
with pfReports as(select sum(((sm.salary*percentage)/100)) 
as Pfreport,sfm.branchid,cd.place,sfm.userid,u.firstname,u.lastname from staff sfm 
join salary sm on sm.id=sfm.salary
join salarypattern spm on spm.id=sm.splallowances 
join salaryallowancestable sat on sat.patternid=spm.id 
join branch sbm on sbm.id=sfm.branchid
join contactdetails cd on cd.id=sbm.contactdetailsid
join users u on u.id=sfm.userid
where sat.allowancestype='PF' and sfm.branchid in(select upv.branchid from userprofileview upv where upv.id=(select fn_viewuserid from fn_viewuserid(param_usersession)) )  group by
sfm.branchid,cd.place, CUBE(sfm.userid,u.firstname,u.lastname) order by sfm.branchid,sfm.userid)
select * from pfReports pfr where pfr.branchid in(select upv.branchid from userprofileview upv where upv.id=(select fn_viewuserid from fn_viewuserid(param_usersession)) )
 and ((pfr.userid is null and pfr.firstname is not null and pfr.lastname is not null)
or (pfr.userid is null and pfr.firstname is  null and pfr.lastname is  null)) order by pfr.pfreport ;
end;
$BODY$;	
CREATE OR REPLACE FUNCTION fn_viewESIReport(
	param_usersession text)
    RETURNS table(esi double precision,branchid int,branchname varchar,userid int,firstname varchar,lastname varchar)
	LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
with ESIReport as(select sum(((sm.salary*percentage)/100)) 
as EsiReport,sfm.branchid,cd.place,sfm.userid,u.firstname,u.lastname from staff sfm 
join salary sm on sm.id=sfm.salary
join salarypattern spm on spm.id=sm.splallowances 
join salaryallowancestable sat on sat.patternid=spm.id 
join branch sbm on sbm.id=sfm.branchid
join contactdetails cd on cd.id=sbm.contactdetailsid
join users u on u.id=sfm.userid
where sat.allowancestype='ESI' and sfm.branchid in(select upv.branchid from userprofileview upv where upv.id=(select fn_viewuserid from fn_viewuserid(param_usersession)) )  group by
sfm.branchid,cd.place, CUBE(sfm.userid,u.firstname,u.lastname) order by sfm.branchid,sfm.userid)
select * from ESIReport esir where esir.branchid in(select upv.branchid from userprofileview upv where upv.id=(select fn_viewuserid from fn_viewuserid(param_usersession)) )
 and ((esir.userid is null and esir.firstname is not null and esir.lastname is not null)
or (esir.userid is null and esir.firstname is  null and esir.lastname is  null)) order by esir.EsiReport ;
end;
$BODY$;	
CREATE OR REPLACE FUNCTION fn_viewPtReport(
	param_usersession text)
    RETURNS table(pt double precision,branchid int,branchname varchar,userid int,firstname varchar,lastname varchar)
	LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
with PtReport as(select sum(pr.amount) as ptreport,sfm.branchid as brnid,cd.place,sfm.userid,u.firstname,u.lastname from staff sfm
	join salary sm on sm.id=sfm.salary
	join branch sbm on sbm.id=sfm.branchid
join contactdetails cd on cd.id=sbm.contactdetailsid
join users u on u.id=sfm.userid
join payroll pr on pr.branchid in(select upv.branchid from userprofileview upv
where upv.id=(select fn_viewuserid from fn_viewuserid(param_usersession)) )where pr.name='PT' and sm.salary between
	pr.startrange and pr.endrange group by
sfm.branchid,cd.place, CUBE(sfm.userid,u.firstname,u.lastname) order by sfm.branchid,sfm.userid)
select * from PtReport ptre where ptre.brnid=(select upv.branchid from userprofileview upv where upv.id=(select fn_viewuserid from fn_viewuserid(param_usersession)) )
 and ((ptre.userid is null and ptre.firstname is not null and ptre.lastname is not null)
or (ptre.userid is null and ptre.firstname is  null and ptre.lastname is  null)) order by ptre.ptreport ;
end;
$BODY$;	
create or replace function fn_getapplicantsrating(param_usersession text)
returns table (applicantid int,userid int,type varchar,jobtittle varchar,firstname varchar,lastname varchar,
			  contactid int,email varchar,mobile varchar,recqruitmentid int,jobapplicantid int,performencertng double precision
			   ,ontimertng double precision,behaviourrtng double precision,appearencertng double precision,branchid int,
			   communicationrtng double precision,interviewstatus varchar)as
$$
declare n int;
begin
return query
select jbv.jobapplicantid,jbv.userid,jbv.type,jbv.jobtittle,jbv.firstname,jbv.lastname,jbv.contactid,jbv.email,jbv.mobile,
r.id as recqruitmentid,r.jobapplicantid,r.performencertng,r.ontimertng,r.behaviourrtng,r.appearencertng,jbv.branchid,
r.communicationrtng,r.interviewstatus  from jobapplicantsview jbv join requirtement r on jbv.jobapplicantid=r.jobapplicantid
where jbv.branchid in(select u.branchid from userProfileview u
where id in(select fn_viewuserid from
          fn_viewuserid(param_usersession)));
end;
$$ language plpgsql;
create or replace function fn_getapplicantsratingbyid(param_usersession text)
returns table (applicantid int,userid int,type varchar,jobtittle varchar,firstname varchar,lastname varchar,
			  contactid int,email varchar,mobile varchar,recqruitmentid int,jobapplicantid int,performencertng double precision
			   ,ontimertng double precision,behaviourrtng double precision,appearencertng double precision,branchid int,
			   communicationrtng double precision,interviewstatus varchar)as
$$
declare n int;
begin
return query
select jbv.jobapplicantid,jbv.userid,jbv.type,jbv.jobtittle,jbv.firstname,jbv.lastname,jbv.contactid,jbv.email,jbv.mobile,
r.id as recqruitmentid,r.jobapplicantid,r.performencertng,r.ontimertng,r.behaviourrtng,r.appearencertng,jbv.branchid,
r.communicationrtng,r.interviewstatus  from jobapplicantsview jbv join requirtement r on jbv.jobapplicantid=r.jobapplicantid
where jbv.userid in(select fn_viewuserid from fn_viewuserid(param_usersession));
end;
$$ language plpgsql;
create or replace function fn_getUserName(param_usersession text)
returns setof userprofileview as
$$
begin
return query
select * from userprofileview where userid=(select fn_viewuserid from fn_viewuserid(param_userSession));
end;
$$ 
language plpgsql;
create or replace function fn_getacademicdetails(param_userSession text)
returns setof academicdetails
as
$$
begin
return query
select * from academicdetails where userid in(select fn_viewuserid from fn_viewuserid(param_userSession));
end
$$
language plpgsql;
CREATE OR REPLACE FUNCTION fn_gettosharedlist(
	param_userid int)
    RETURNS TABLE(touserid integer, url varchar, fromuserid integer, firstname varchar, lastname varchar)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
return query
select a.userid as touserid,s.url,s.fuserid as fromuserid, u.firstname, u.lastname
from authenticationmaster a join sharing s on
a.socialnetworkid= s.socialnetworkid join users u on s.fuserid= u.id where a.userid= param_userid;
end;
$BODY$;

CREATE OR REPLACE VIEW applicantsaggrementsview AS
 SELECT apg.annualctc,
    apg.doj,
    apg.id AS applicantaggrementsid,
    apg.aggrementstatus,
    jba.userid,
    jba.placementid,
    jba.status AS applicanststats,
    u.firstname,
    u.lastname,
    u.dob,
    u.gender,
    c.*::contactdetails AS c,
    c.email,
    c.mobile,
    s.id AS shiftid,
    s.shiftname,
    s.days,
    s.starttime,
    s.endtime,
    s.gracetime,
    s.graceouttime,
    rq.jobapplicantid,
    rq.performencertng,
    rq.communicationrtng,
    rq.interviewstatus,
    p.name,
    p.branchid,
    p.starttime AS placementstarttime,
    p.endtime AS placementendtime,
    p.package AS familiarpackage,
	p.type,
    jm.jobtittle,
    jm.designation,
    jm.minimumexperience,
    jm.location,
    jm.requiteremail,
    sm.name AS organizationname,
    sm.establishedyear,apg.id
  FROM applicantsaggrements apg
     JOIN jobapplicants jba ON apg.applicantsid = jba.id
     JOIN users u ON u.id = jba.userid
     JOIN contactdetails c ON c.userid = u.id
     JOIN shifts s ON s.id = apg.shiftid
     JOIN requirtement rq ON rq.jobapplicantid = jba.id
     JOIN placements p ON p.id = jba.placementid
     JOIN job jm ON jm.id = p.jobid
     JOIN branch sbm ON sbm.id = p.branchid
     JOIN organization sm ON sm.id = sbm.organizationid;



CREATE OR REPLACE FUNCTION fn_checkpayrolldata(
    param_name varchar,param_startrange double precision,param_endrange double precision,param_branchid int)
    RETURNS TABLE(id int,branchid int,startrange double precision,endrange double precision,name varchar,amount double precision)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
return query
select  * from payroll p where( param_startrange between p.startrange and p.endrange or param_endrange between
                             p.startrange and p.endrange)and p.branchid=param_branchid and p.name=param_name;
end;
$BODY$;
CREATE OR REPLACE VIEW permissionview AS
 SELECT p.roleid,
    p.componentid,
    p.write,
    p.read,
    c.modules,
    c.components,
    r.rolename
   FROM permissions p
     JOIN components c ON p.componentid = c.id
     JOIN rolemaster r ON r.id = p.roleid;


CREATE OR REPLACE FUNCTION public.fn_viewpermissionsforrole()
    RETURNS TABLE(id integer, roleid integer, branchid integer, read boolean, write boolean, componentid integer, modules character varying, components character varying, icon character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select p.id,p.roleid,p.branchid,coalesce(p.read,false) as read,coalesce(p.write,false) as write,c.id as
   componentId,c.modules,c.components,c.icon FROM permissions p
      JOIN components c ON p.componentid = c.id and (p.branchid =0 )
	 where c.modules not in('app','login','user','dashboard') order by c.modules ;
end;
$BODY$;

 CREATE OR REPLACE FUNCTION fn_viewpermissionsLogin(
    param_roleid integer, param_branchId integer)
    RETURNS table (id int, roleid int,branchid int,read boolean, write boolean, componentId integer, modules varchar,
                  components varchar,icon varchar)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
return query
select p.id,p.roleid,p.branchid,coalesce(p.read,false) as read,coalesce(p.write,false) as write,c.id as
   componentId,c.modules,c.components,c.icon FROM permissions p
      JOIN components c ON p.componentid = c.id and (p.roleid = param_roleid)
	 and (p.branchid= param_branchId)
	 where c.modules not in('app','login','user','dashboard') order by c.modules ;
end;
$BODY$;
   CREATE OR REPLACE FUNCTION fn_viewpermissions(
    param_roleid integer, param_branchId integer)
    RETURNS table (id int, roleid int,branchid int,read boolean, write boolean, componentId integer, modules varchar,
                  components varchar,icon varchar)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
return query
select p.id,p.roleid,p.branchid,coalesce(p.read,false) as read,coalesce(p.write,false) as write,c.id as
   componentId,c.modules,c.components,c.icon FROM permissions p
     right outer JOIN components c ON p.componentid = c.id and (p.roleid = param_roleid)
	 and (p.branchid= param_branchId)
	 where c.modules not in('app','login','user','dashboard') order by c.modules ;
end;
$BODY$;


		
CREATE OR REPLACE FUNCTION fn_viewtotaltaxableamount(
    param_userid integer,param_month varchar,param_uniquecode varchar)
    RETURNS double precision
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
AS $BODY$
declare basic double precision;
declare hra double precision;
declare other double precision;
declare total double precision;
declare tdsamount double precision;
declare remaining1 double precision;
declare branch double precision;
declare n int;
begin
select ((sm.salary*sat.percentage)/100) as basic from staff stfm
join salary sm on sm.id=stfm.salary
join salarypattern spm on spm.id=sm.splallowances
join salaryallowancestable sat on sat.patternid=spm.id where sat.allowancestype='Basic' and stfm.userid=param_userid into basic;
select (((sm.salary*sat.percentage)/100)-((((sm.salary*sat.percentage)/100)*10)/100)) as HRA from staff stfm
join salary sm on sm.id=stfm.salary
join salarypattern spm on spm.id=sm.splallowances
join salaryallowancestable sat on sat.patternid=spm.id where sat.allowancestype='HRA' and stfm.userid=param_userid into hra;
select sum(((sm.salary*sat.percentage)/100)) as others from staff stfm
join salary sm on sm.id=stfm.salary
join salarypattern spm on spm.id=sm.splallowances
join salaryallowancestable sat on sat.patternid=spm.id where
sat.allowancestype!='Basic' and sat.allowancestype!='HRA' and stfm.userid=param_userid
and sat.type='Allowance' into other;
select (basic+hra+other) as total,stfm.userid from staff stfm
join salary sm on sm.id=stfm.salary
join salarypattern spm on spm.id=sm.splallowances
join salaryallowancestable sat on sat.patternid=spm.id where stfm.userid=param_userid group by(stfm.userid)
into total;
select (((((total*12)-((t.tdsstartrange)))*t.tdsamount)/100)/12) as  tdsamount from tds t where (total*12) between
t.tdsstartrange and t.tdsendrange into tdsamount; RAISE NOTICE 'tdsamount called on %', tdsamount;
select (total-tdsamount) as remain into remaining1;
select branchid from staff into branch where userid=param_userid;
 insert into tdsreports(branchid,userid,tdspayableamount,taxamount,remaining,month,uniquecode)values(branch,param_userid,total,tdsamount,remaining1,param_month,param_uniquecode) on conflict(uniquecode) do update set tdspayableamount=total,taxamount=tdsamount,remaining=remaining1  returning Id into n;
 return tdsamount;
end
$BODY$;
																	
																			
CREATE OR REPLACE FUNCTION fn_viewtdsreport(
	param_usersession text)
    RETURNS TABLE(tds double precision, branchid integer, branchname character varying, userid integer, firstname character varying, lastname character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query																		
with  tdsreport as (select sum(taxamount) 
as tdsreport,tds.branchid,cd.place,tds.userid,u.firstname,u.lastname from tdsreports tds 
join branch sbm on sbm.id=tds.branchid
join contactdetails cd on cd.id=sbm.contactdetailsid
join users u on u.id=tds.userid
where tds.branchid in(select upv.branchid from userprofileview upv where upv.id=
	(select fn_viewuserid from fn_viewuserid(param_usersession)) )
group by tds.branchid,cd.place, CUBE(tds.userid,u.firstname,u.lastname) order by tds.branchid,tds.userid	)																	
		select * from tdsreport tds where tds.branchid in(select upv.branchid from userprofileview upv where upv.id=(select fn_viewuserid from fn_viewuserid(param_usersession)) )
 and ((tds.userid is null and tds.firstname is not null and tds.lastname is not null)
or (tds.userid is null and tds.firstname is  null and tds.lastname is  null)) order by tds.tdsreport ;
	end;
$BODY$;

CREATE OR REPLACE FUNCTION fn_viewtransportstopmasterbyrouteid(
    param_routeid integer)
    RETURNS SETOF transportstopview
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
begin
    return query
    select * from transportstopview
    where routeId=param_routeid;
end;
$BODY$;

CREATE OR REPLACE VIEW public.teamview AS
 SELECT t.id,t.departmentid,t.name,t.teamsize,t.branchid,t.staffid,d.name as departmentname,u.firstname,u.lastname FROM team t
     JOIN department d ON t.departmentid = d.id
     JOIN branch b on b.id=t.branchid

join users u on u.id=t.staffid;



CREATE OR REPLACE FUNCTION public.fn_getteams(
	param_usersession text)
    RETURNS setof teamview
	LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
   select * from teamview where branchid in (select branchid from userProfileview
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)));

end;
$BODY$;




CREATE OR REPLACE VIEW public.teamallocationview AS
 SELECT ta.id,ta.teamid,ta.userid,t.name,u.firstname,u.lastname,t.branchid,d.name as departmentname
   FROM teamallocation ta
     JOIN team t ON t.id = ta.teamid
     JOIN users u ON u.id = ta.userid
	 join department d on d.id=t.departmentid;

CREATE OR REPLACE FUNCTION public.fn_viewteamallocation(
	param_usersession text)
    RETURNS setof teamallocationview
	LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
   select * from teamallocationview t where t.branchid
						in(select branchid from userProfileview
where id=(select fn_viewuserid from fn_viewuserid(param_usersession)));

end;
$BODY$;
		
create or replace view allocateshiftsview as
select * from allocateshifts;


create or replace function fn_viewallocateshifts() returns SETOF allocateshiftsview
as
$$
begin 
return query
select * from allocateshiftsview; 
end
$$ 
 language plpgsql;
 