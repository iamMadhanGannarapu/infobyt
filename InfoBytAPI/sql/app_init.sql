do $$
begin
    Create table DomainLogs(LogId SERIAL Primary Key,LogHeader text,LogCode text,LogType text,Message text,ReferenceId integer,LoggedFor text,CreationTime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,ModifiedTime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,UserId integer,Status boolean NOT NULL);
exception when others then 
    insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Creation:DomainLogs','CTR','WARN','DOMAINLOGS Table already Exits','DomainLogs',False);
end;
$$ language 'plpgsql';
DROP Table IF EXISTS RoleMaster CASCADE;
insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Deletion:RoleMaster','DTR','WARN','Deletion of ROLEMASTER Table successfull','RoleMaster',true);
CREATE table RoleMaster(Id SERIAL Primary Key,RoleName text UNIQUE,CreationTime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,ModifiedTime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,Status boolean NOT NULL DEFAULT False);
insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Creation:RoleMaster','CTR','INFO','Creation of ROLEMASTER Table successfull','RoleMaster',true);
do $$
begin
insert into RoleMaster(RoleName,status) Values('SuperAdmin',true);
insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Adding-Data:RoleMaster','INT','INFO','Insertion of SuperAdmin Role is successfull','RoleMaster',true);
end;
$$ language 'plpgsql';
DROP Table IF EXISTS Users CASCADE;
insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Deletion:Users','DTR','WARN','Deletion of USERS Table successfull','Users',true);
DROP Table IF EXISTS Login CASCADE;
insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Deletion:Login','DTR','WARN','Deletion of LOGIN Table successfull','Login',true);
DROP Table IF EXISTS authenticationmaster CASCADE;
insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Deletion:Authenticationmaster','DTR','WARN','Deletion of AUTHENTICATIONMASTER Table successfull','Login',true);
CREATE table Users(Id SERIAL PRIMARY KEY,FirstName VARCHAR(30) NOT NULL,MiddleName VARCHAR(30),LastName VARCHAR(30) NOT NULL,DOB DATE ,Gender VARCHAR(20),FatherName VARCHAR(30) NOT NULL,MotherName VARCHAR(30) NOT NULL,FatherOccupation int NOT NULL,Caste VARCHAR(20),SubCaste VARCHAR(20),Religion int,Nationality int,Status VARCHAR(20) NOT NULL);
insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Creation:Users','CTR','INFO','Creation of USERS Table successfull','Users',true);
CREATE table Login(LoginId VARCHAR(15) NOT NULL PRIMARY KEY,Password TEXT NOT NULL,CreatedDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP,UserId INTEGER NOT NULL REFERENCES Users(Id),UserRoleId INTEGER NOT NULL REFERENCES  RoleMaster(Id),Status VARCHAR(20) NOT NULL);
insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Creation:Login','CTR','INFO','Creation of LOGIN Table successfull','Login',true);
CREATE table authenticationmaster(id serial,userid int,Loginid varchar(15) references login(loginid),username varchar(20),passcode varchar(15),lastAccessed timestamp with time zone,lastModified timestamp with time zone,successFullLogins int,LoginsFailed int,loginFreq int,lastUnsuccessfullAccess timestamp with time zone,online varchar(10),initialLogin timestamp with time zone,accessedIp varchar(30),accessedMid varchar(30),devicetype varchar(20),socialnetworkid varchar(30) unique);
--insert into authenticationmaster values (0,2,'IB_181213002','IB_181213002','140839','2018-12-20 15:32:46.673656+05:30','2018-12-19 09:34:17.513469+05:30',1,0,0,'2018-12-18 11:28:49.76932+05:30','Active','2018-12-13 15:20:22.237463+05:30','','IB_181213002','DESKTOP','Ibadmin01I');
insert into DomainLogs(LogHeader,LogCode,LogType,Message,LoggedFor,Status) values ('Table-Creation:authenticationmaster','CTR','INFO','Creation of AUTHENTICATIONMASTER Table successfull','Login',true);
CREATE OR REPLACE FUNCTION get_random_number(INTEGER, INTEGER) RETURNS INTEGER AS $$
DECLARE
    start_int ALIAS FOR $1;
    end_int ALIAS FOR $2;
BEGIN
    RETURN trunc(random() * (end_int-start_int) + start_int);
END;
$$ LANGUAGE 'plpgsql' STRICT;
CREATE OR REPLACE FUNCTION random_between(low INT ,high INT) 
   RETURNS varchar AS 
   $$
   declare m varchar;
   declare n varchar;
   declare chars text[] := '{A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z}';
BEGIN
select cast(floor(random()* (high+low + 1) + low)as varchar) into m;
select lpad(m,2,'0') into n;
   RETURN concat(n,chars[1+random()*(array_length(chars, 1)-1)]);
END;
$$ language 'plpgsql' STRICT;
CREATE OR REPLACE FUNCTION fn_addauthenticationmaster(
    param_userid integer,
    param_loginid character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare m varchar;
declare o varchar;
declare n int;
declare j varchar;
declare k varchar;
declare passcode int;
begin
select firstname from users where id=param_userid into m;
select lastname from users where id=param_userid into o;
select random_between(1,99) into j;
select concat(m,o,j) into k;
    SELECT get_random_number(100001, 999999) into passcode;                                     
    insert into authenticationmaster(userid,loginid,username,passcode,successfulllogins,loginsfailed,loginfreq,accessedip,accessedmid,devicetype,socialnetworkid) values(param_userid,param_loginid,param_loginid,passcode,0,0,0,null,null,null,k);
return passcode;
end
$BODY$;
