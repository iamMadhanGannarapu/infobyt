--
-- Name: fn_updateachievement(integer, integer, character varying, date); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updateachievement(param_achievementsid integer, param_userid integer, param_description character varying, param_achievementdate date) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
 begin
update achievements set userid=param_userid,description=param_description,achievementdate=param_achievementdate where Id= param_achievementsid
returning Id into n;
return n;
end 
';
--
-- Name: fn_updateadmisiondetails(integer, character varying, character varying, integer, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updateadmisiondetails(param_admissionnum integer, param_parentemail character varying, param_parentmobile character varying, param_branchid integer, param_userid integer, param_feeid integer, param_concessionid integer) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin 
update AdmisionDetails set
parentemail=param_parentemail,
parentmobile=param_parentmobile,
BranchId=param_BranchId,
userid=param_userid,
feeid=param_feeid,
concessionid=param_concessionid
where admissionnum=param_admissionnum
returning admissionnum into n;
return n;							  
end
';
CREATE OR REPLACE FUNCTION fn_updateattendance(
	param_attendancelogid character varying,
	param_statuscode character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin 
update  AttendanceLog     set
statuscode=param_statuscode
where AttendanceLogId=param_AttendanceLogId
returning AttendanceLogId into n;
return n;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_updatetransportstopfee(
	param_id integer,
	param_stopid integer,
	param_routeid integer,
	param_amount varchar(50))
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin 
update  transportstopfee set stopid=param_stopid,routeid=param_routeid,
amount=param_amount where id=param_id returning Id into n;
return n;
end;
$BODY$;
-- Name: fn_updatebatch(integer, integer, integer, date, date, character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updatebatch(param_id integer, param_staffid integer, param_sessionid integer, param_startdate date, param_enddate date, param_status character varying, param_name character varying) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin 
update  batch set staffid=param_staffid,sessionid=param_sessionid,
startdate=param_startdate, enddate=param_enddate,status=param_status,
name=param_name where id=param_id returning Id into n;
return n;
end;
';
--
-- Name: fn_updatebranchsession(integer, integer, integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updatebranchsession(
    params_id integer,
    params_sessionid integer,
    params_branchid integer,
    params_medium character varying,
    params_numoftrainees integer,
    params_departmentid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
AS $BODY$
declare n integer;
begin
update branchsession set sessionid=params_sessionid,branchid=params_branchid,
                        medium=params_medium,numoftrainees=params_numoftrainees,
                        departmentid=params_departmentid
where Id= params_id
returning Id into n;
return n;
end
$BODY$;
--
-- Name: fn_updatebus(integer, character varying, character varying, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updatebus(param_id integer, param_regnum character varying, param_description character varying, param_branchid integer,param_Latitude text,param_Longitude text) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin 
update bus set
regnum=param_regnum,
description=param_description,
Latitude=param_Latitude,
Longitude=param_Longitude,
branchid=param_branchid where id=param_id returning Id into n;
return n;
end;
';
--
-- Name: fn_updatesession(integer, character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updatesession(param_sessionid integer, param_sessionname character varying) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n integer;
begin 
update  sessionmaster set sessionname=param_sessionname
where id=param_sessionid
returning Id into n;
return n;
end
';
--
-- Name: fn_updatesessionsubject(integer, integer, character varying, integer, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updatesessionsubject(
	param_id integer,
	param_subjectid integer,
	param_syllabus character varying,
	param_batchid integer,
	param_branchsessionid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin
update sessionsubject set subjectid=param_subjectid,syllabus=param_syllabus,
batchid=param_batchid, branchsessionid=param_branchsessionid
where id=param_id returning Id into n;
return n;
end
$BODY$;
--
-- Name: fn_updateconcession(integer, integer, character varying, character varying, double precision, date); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updateconcession(param_concessionid integer, param_branchid integer, param_concessionname character varying, param_details character varying, param_concession double precision, param_creadtiondate date) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
		begin 
								update concession set
									branchid=param_branchid,
									concessionname=param_concessionname,
									details=param_details,
									concession=param_concession,
									creationdate=param_creadtiondate
									 where id=param_concessionid  returning  Id into  n;
									  return n;
									 					
						 					 
		 end
';
--
-- Name: fn_updatecontactdetails(integer, integer, integer, integer, integer, character varying, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updatecontactdetails(params_contactdetailsid integer, params_userid integer, params_country integer, params_state integer, params_city integer, params_place character varying, params_address character varying, params_uniqueidentificationno character varying, params_email character varying, params_mobile character varying) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin
update contactdetails set  userid=params_userid,
country=params_country,
state=params_state,
city=params_city,
place=params_place, 
address=params_address,
uniqueidentificationno=params_uniqueidentificationno,
email=params_email,
mobile=params_mobile
where  id=params_contactdetailsid
returning Id into n;
return n;
end
';
--
-- Name: fn_updatedriver(integer, integer, integer, date, character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updatedriver(param_id integer, param_busid integer, param_driverid integer, param_allocationdate date, param_status character varying) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin
    update busdrvalocation set busid=param_busid,driverid=param_driverid,allocationdate=param_allocationdate,
	status=param_status where Id= param_id
	returning Id into n;
	return n;
end
';
--
-- Name: fn_updateappraisaltimetable(integer, integer, date, time without time zone, time without time zone, character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updateappraisaltimetable(param_appraisaltimetableid integer, param_sessionsubjectid integer, param_appraisaldate date, param_starttime time without time zone, param_endtime time without time zone, param_appraisalname character varying, param_status character varying) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin
update appraisaltimetable set sessionsubjectid=param_sessionsubjectid,appraisaldate=param_appraisaldate,starttime=param_starttime,
endtime=param_endtime,appraisalname=param_appraisalname,status=param_status
where Id= param_appraisaltimetableid
returning sessionsubjectid into n;
return n;
end 
';
--
-- Name: fn_updatetrainersubject(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updatetrainersubject(param_id integer, param_subjectid integer, param_staffid integer) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin 
update  trainersubject   set
subjectid=param_subjectid,
staffid=param_staffid
where id=param_id returning Id into n;
return n;
end;
';
--
-- Name: fn_updatefees(integer, character varying, integer, integer, double precision); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION public.fn_updatefees(
	params_id integer,
	params_feename character varying,
	params_branchid integer,
	params_branchsessionid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
declare n integer;
begin 
update fees set
feename=params_feename,
branchid=params_branchid,
branchsessionid=params_branchsessionid
where id=params_id
returning Id into n;
return n;
end;
$BODY$;
--
-- Name: fn_updategallery(integer, integer, character varying, date); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updategallery(param_id integer, param_branchid integer, param_description character varying, param_gallerydate date) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin 
update gallery set branchid=param_branchid,description=param_description,
gallerydate=param_gallerydate where id=param_id
returning Id into n;
return n;
end;
';
--
-- Name: fn_updateholiday(integer, character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updateholiday(
	param_id integer, param_holidaydate date, param_holidayenddate date,
	param_occation character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
	begin
	 update holiday set 
	  occation=param_occation, holidaydate=param_holidaydate, holidayenddate=param_holidayenddate  where Id=param_id returning Id into n;
	  return n;
	end
		
$BODY$;
--
-- Name: fn_updateleavedetails(integer, integer, date, date, character varying, character varying, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updateleavedetails(param_id integer, param_userid integer, param_fromdate date, param_todate date, param_status character varying, param_description character varying, param_aprrovedby integer) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n integer;
	begin
	 update LeaveDetails set 
	 UserId=param_UserId, FromDate=param_FromDate,ToDate=param_ToDate, Status=param_Status,
	 Description= param_Description, ApprovedBy= param_AprrovedBy where id=param_id
	 returning id into n;
	 return n;
	end
		
';
--
-- Name: fn_updatelogin(character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updatelogin(param_loginid character varying, param_password character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS '
declare n varchar;
begin
update login set password=param_password where loginid=param_loginid
returning loginid into n;
return n;
end
';
--
-- Name: fn_updatepassword(integer, character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updatepassword(
param_usersession text,
newpassword character varying)
RETURNS void
LANGUAGE 'plpgsql'
COST 100
VOLATILE 
AS $BODY$
begin
update login 
set password=$2 where userid=(select fn_viewuserid from fn_viewuserid(param_usersession));
end
$BODY$;
--
-- Name: fn_updatepassword(character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updatepassword(loginidd character varying, newpassword character varying) RETURNS void
    LANGUAGE plpgsql
    AS '
begin
update login 
set password=$2 where loginid=$1;
end
';
--
-- Name: fn_updaterole(integer, text, boolean); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updaterole(param_roleid integer, param_rolename text, param_status boolean) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin
update rolemaster set rolename=param_rolename,status=param_status 
where id=param_roleid returning Id into n;
return n;
end
';
--
-- Name: fn_updatesalary(integer, integer, double precision, double precision); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updatesalary(param_salaryid integer, param_staffid integer, param_salary double precision, param_splallowances double precision) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin
	 update salary set staffid=param_staffid,
	                           salary=param_salary,
	                          splallowances=param_splallowances
	    where id=param_salaryid returning Id into n;
	 return n;
  end
';
--
-- Name: fn_updateOrganization(integer, character varying, character varying, character varying, integer, date, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updateOrganization(
	params_id integer,
	params_name character varying,
	params_type character varying,
	params_status character varying,
	params_establishedyear integer,
	params_regdate date,
	params_contactdetailsid integer)
    RETURNS varchar
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n varchar;
begin
update Organization set 
name=params_name,
type=params_type,
status=params_status,
establishedyear=params_establishedyear,
regdate=params_regdate,
contactdetailsid=params_contactdetailsid
where id=params_id
returning status into n;
return n;
end;
									
$BODY$;
--
-- Name: fn_updateOrganizationbranch(integer, integer, date, character varying, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updateOrganizationbranch(params_id integer, params_Organizationid integer, params_startdate date, params_status character varying, params_contactdetailsid integer) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n integer;
begin 
update  branch set
Organizationid=params_Organizationid,
startdate=params_startdate,
status=params_status,
contactdetailsid=params_contactdetailsid
where id=params_id
returning Id into n;
return n;
end
';
--
-- Name: fn_updatestaff(integer, integer, integer, date, double precision, double precision, integer); Type: FUNCTION; Schema: public; Owner: -
--




CREATE OR REPLACE FUNCTION public.fn_updatestaff(
        param_staffid integer,
        param_userid integer,
        param_branchid integer,
        param_doj date,
        param_experience double precision,
        param_salary double precision,
        param_userroleid integer,
        param_bankname character varying,
        param_accountnumber character varying,
        param_pannumber character varying,
        param_pfnumber character varying,
        param_splallowances integer,
        param_department integer,
        param_shiftid integer,
        param_otpay double precision)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE
AS $BODY$
declare n int;
declare m int;
begin
update staff set userid=param_userid,
branchid=param_branchid,
doj=param_doj,
experience=param_experience,
salary=param_salary,
userroleid=param_userroleid ,
departmentid=param_department,
shiftid=param_shiftid,otpay=param_otpay
where id=param_staffid returning Id into n;
update accountdetails set bankname=param_bankname,accountnumber=param_accountnumber,pannumber=param_pannumber,pfnumber=param_pfnumber where staffid=(n);
update salary set salary=param_salary,splallowances=param_splallowances,date=CURRENT_TIMESTAMP where staffid=(n)  returning id into m;
update staff set salary=(m) where id=(n);
return n;
end;
$BODY$;
--
-- Name: fn_updatetraineemarks(integer, integer, integer, double precision); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updatetraineemarks(param_traineemarksid integer, param_traineeid integer, param_appraisalid integer, param_marks double precision) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin
    update traineemarks set traineeid=param_traineeid,appraisalid=param_appraisalid,marks=param_marks where Id= param_traineemarksid
	returning Id into n;
	return n;
end
';
--
-- Name: fn_updatetimetable(integer, integer, character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updatetimetable(param_id integer, param_batchid integer, param_type character varying, param_remarks character varying) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin 
update       timetable      set
batchid=param_batchid,
type=param_type,
remarks=param_remarks
where id=param_id
returning id into n;
return n;
end;
';
--
-- Name: fn_updatetransportfee(integer, character varying, character varying, double precision, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updatetransportstopfee(
    param_id integer,
    param_stopid integer,
    param_routeid integer,
    param_amount character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
AS $BODY$
declare n int;
begin
update  transportstopfee set stopid=param_stopid,routeid=param_routeid,
amount=cast(param_amount as int) where id=param_id returning Id into n;
return n;
end;
$BODY$;
--
-- Name: fn_updatetransportroute(integer, character varying, character varying, character varying, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updatetransportroute(params_id integer, params_name character varying, params_startpoint character varying, params_endpoint character varying, params_busid integer) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
begin
update transportroute set name=params_name,startpoint=params_startpoint,
endpoint=params_endpoint,busid=params_busid where id=params_id 
returning Id into n;
return n;
end
';
--
-- Name: fn_updatetransportstop(integer, integer, character varying, time without time zone, time without time zone); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updatetransportstop(
	param_transportstopsid integer,
	param_routeid integer,
	param_stopname character varying,
	param_arrivaltime time without time zone,
	param_departuretime time without time zone,
param_latitude text,
param_longitude text)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
 begin
update transportstop set routeid=param_routeid,stopname=param_stopname,arrivaltime=param_arrivaltime,departuretime=param_departuretime,latitude=param_latitude,longitude=param_longitude where Id= param_transportstopsid
returning Id into n;
return n;
end 
$BODY$;
--
-- Name: fn_updateuserfeedback(integer, integer, integer, character varying, double precision); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updateuserfeedback(param_id integer, param_userid integer, param_feederid integer, param_feedbackdescription character varying, param_rating double precision) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n integer;
	begin
	 update feedback set 
	 UserId=param_UserId, feederid=param_feederid,feedbackdescription=param_feedbackdescription, 
	rating=param_rating where id=param_id
	 returning id into n;
	 return n;
	end
		
';
--
-- Name: fn_updateusers(integer, character varying, character varying, character varying, date, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updateusers(params_id integer, params_firstname character varying, params_middlename character varying, params_lastname character varying, params_dob date, params_gender character varying, params_fathername character varying, params_mothername character varying, params_fatheroccupation int, params_caste character varying, params_subcaste character varying, params_religion int, params_nationality int, params_status character varying) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n integer;
		begin
update users set 
firstname=params_firstname,
middlename=params_middlename,
lastname=params_lastname,
dob=params_dob,
gender=params_gender,
fathername=params_fathername,
mothername=params_mothername,
fatheroccupation=params_fatheroccupation,
caste=params_caste,
subcaste=params_subcaste,
religion=params_religion,
nationality=params_nationality,
status=params_status 
where id=params_id
returning Id into n;
return n;
end
';
--
-- Name: fn_updateusertransport(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updateusertransport(param_usertransportid integer, param_userid integer, param_stopid integer) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
 begin
update  usertransport set userid=param_userid,stopid=param_stopid where  Id= param_usertransportid
returning Id into n;
return n;
end 
';
--
-- Name: fn_updateusertransportpayment(integer, integer, integer, date, integer, character varying); Type: FUNCTION; Schema: public; Owner: -
--
CREATE OR REPLACE FUNCTION fn_updateusertransportpayment(param_id integer, param_userid integer, param_paidamount integer, param_paiddate date, param_due integer, param_status character varying) RETURNS integer
    LANGUAGE plpgsql
    AS '
declare n int;
 begin
update usertransportpayment set userid=param_userid,paidamount=param_paidamount,
paiddate=param_paiddate,due=param_due,status=param_status
where id= param_id returning id into n;
return n;
end 
';
CREATE OR REPLACE FUNCTION fn_updateevents(
	params_id integer,
	params_branchid integer,
	params_evenname character varying,
	params_organizermobile character varying,
	params_sponsors character varying,
	params_fromdate date,
	params_todate date)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
  declare n integer;
  begin
  update events set
  branchId=params_branchid,
  eventName =params_evenname ,
  organizerMobile =params_organizermobile,
  sponsors = params_sponsors,
  fromDate =params_fromdate,
  toDate =    params_todate
  where id=params_id
  returning Id into n;
  return n;
end;
$BODY$;
--function for Updatelibraryrequest
CREATE OR REPLACE FUNCTION fn_updatebookrequestdetails(
	param_id integer,
		param_bookid int,
	param_userid integer,
	param_status character varying
)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n integer;
	begin
	 update libraryrequests set 
	 bookid=param_bookid,UserId=param_UserId, Status=param_Status
	 where id=param_id
	 returning id into n;
	 return n;
	end
		
$BODY$;		
CREATE OR REPLACE FUNCTION fn_updatehostel(
	param_id int,
	param_userid int,param_branchid int,
									  param_type varchar,
					param_facilities varchar,
					param_sharing varchar,
					param_blockname varchar,
					param_roomnum varchar,
					param_allocationdate date ,
					param_representativeid int ,
					param_rentalperiod varchar )
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin 
update  hostel set userid=param_userid,branchid=param_branchid,
type=param_type, facilities=param_facilities,sharing=param_sharing,
blockname=param_blockname,roomnum=param_roomnum,allocationdate=param_allocationdate,
representativeid=param_representativeid,rentalperiod=param_rentalperiod where id=param_id returning id into n;
return n;
end;
$BODY$;





create or replace function fn_updatevisiting(param_id int) returns integer as
$$
declare n int;
begin
update  visiting set indatetime=current_timestamp
					where id=param_id returning Id into n;
					  return n;
					end;
					$$ language plpgsql;
			
CREATE OR REPLACE FUNCTION public.fn_updprojects(
    param_id integer,
    param_branchid integer,
    param_projectname character varying,
    param_assigneddate timestamp without time zone,
    param_submissiondate timestamp without time zone,
    param_type character varying,
    param_details text,
    param_teamsize integer,
    projectfee integer,
    param_category character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE
AS $BODY$
declare n int;
begin
     update projects set branchid=param_branchid,projectname=param_projectname,assigneddate=param_assigneddate,
     submissiondate=param_submissiondate,type=param_type,details=param_details,teamsize=param_teamsize,
     fee=projectfee,category=param_category
        where Id=param_id returning Id into n;
     return n;
  end
$BODY$;

					
create or replace function fn_updatetraineeHostelApprove(param_traineeid int)
RETURNS integer
as
$$
declare n int;
begin
update	hostelAllocate	set status='Approve' where traineeid=param_traineeid;		
return n;
end;
$$
language plpgsql;
CREATE OR REPLACE FUNCTION fn_updplacements(
	param_placementid integer,
	param_branchid integer,
	companyname character varying,
	param_starttime timestamp without time zone,
	param_endtime timestamp without time zone,
	designation character varying,
	param_details text,
	param_positions integer,
	param_package integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
begin
	 update placements set branchid=param_branchid,name=companyname,starttime=param_starttime,
	 endtime=param_endtime,type=designation,details=param_details,positions=param_positions,
	 package=param_package
	    where id=param_placementid and category='placements' returning Id into n;
	 return n;
  end
$BODY$;
																											  
																												  
CREATE OR REPLACE FUNCTION fn_updateallocproj(param_id int,param_traineeid int,param_projectid int)
returns integer as
$$
declare n int;
begin
     update allocateproject set traineeid=param_traineeid,projectid=param_projectid
        where Id=param_id and category='project' returning Id into n;
     return n;
  end
$$
language plpgsql;
CREATE OR REPLACE FUNCTION fn_updatedepartments(
    param_id integer,
    param_name character varying,
    param_staffid integer,
    param_courseid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
AS $BODY$
declare n int;
begin
update departments set
    name=param_name,
    staffid=param_staffid,
    courseid=param_courseid
    where id=param_id
returning id into n;
return n;
end
$BODY$;
create or replace function fn_updatesportsmanagement(
             param_id integer,
	         param_sportsid integer,
	         param_departmentid integer,
	         param_sessionid integer,
	         param_fromdate date,
	         param_starttime time without time zone,
	         param_endtime time without time zone,
	         param_todate date,
	         param_kitid integer
)
returns integer
as
$$
declare n integer;
begin
update sportsmanagement set
sportsId=param_sportsid,
departmentId=param_departmentid,
sessionId=param_sessionid,
fromDate= param_fromdate,
startTime=param_starttime,
endTime=param_endtime,
toDate=param_todate,
kitId=param_kitid
where Id=param_id
returning Id into n;
return n;
end;
$$
language plpgsql;
CREATE OR REPLACE FUNCTION fn_updatetraineelibrary(
	params_id integer,
	params_bookid integer,
	params_userid integer,
	params_returndate date,
param_status varchar)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $$
declare n integer;
begin
update traineelibrary set bookid=params_bookid,
							userid=params_userid,
							returndate=params_returndate,status=param_status
where Id= params_id
returning Id into n;
return n;
end 
$$;
CREATE OR REPLACE FUNCTION fn_updatelibrarytransactions(
	params_id integer,
	params_bookid integer,
	params_userid integer,
	params_returndate date,
param_status varchar)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $$
declare n integer;
begin
update librarytransactions set bookid=params_bookid,
							userid=params_userid,
							returndate=params_returndate,status=param_status
where Id= params_id
returning Id into n;
return n;
end 
$$;
CREATE OR REPLACE FUNCTION fn_updateallocplac(
    param_id integer,
    param_traineeid integer,
    param_placementid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
AS $$
declare n int;
begin
     update allocateproject set traineeid=param_traineeid,projectid=param_placementid
        where Id=param_id and category='placement' returning Id into n;
     return n;
  end
$$;
CREATE OR REPLACE FUNCTION fn_updatepermissions(
	params_id integer,
	params_rolename character varying,
	params_componentname character varying,
	params_read boolean,
	params_write boolean)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n integer;
begin
update permissions set rolename=params_rolename,componentname=params_componentname,read=params_read,write=params_write
where Id= params_id
returning Id into n;
return n;
end 
$BODY$;
CREATE OR REPLACE FUNCTION fn_updatejob(
	param_id integer,
	param_jobtittle character varying,
	param_jobdescription character varying,
	param_designation character varying,
	param_minexperience integer,
	param_maxexperience integer,
	param_annualctc bigint,
	param_othersalary bigint,
	param_vacancy integer,
	param_location character varying,
	param_functionalarea character varying,
	param_jobid integer,
	param_ugqualificationid integer,
	param_pgqualificationid integer,
	param_otherqualificationid integer,
	param_requiteremail character varying,
	param_organizationTypeid integer,
	param_description character varying,
	param_postingdate date)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
									declare n integer;
									begin
									update job set jobTittle=param_jobtittle,
									jobDescription=param_jobdescription,
									designation=param_designation,minimumExperience=param_minexperience,
									maximumExperience=param_maxexperience,annualCtc=param_annualctc,
									
									 otherSalary=param_othersalary,vacancy=param_vacancy,
									 location=param_location,functionalArea=param_functionalarea,
									 jobId=param_jobid,ugQualificationId=param_ugqualificationid,pgQualificationId=param_pgqualificationid,
									 otherQualificationId=param_otherqualificationid,requiterEmail=param_requiteremail,
									 organizationTypeId=param_organizationTypeid,Description=param_description,
									 postingDate=param_postingdate where Id=param_id
									returning Id into n;
									return n;
									end;
									
$BODY$;
CREATE OR REPLACE FUNCTION fn_updatejobapplicantstatus(
	param_userid integer,
	param_placementid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare m int;
declare n int;						
begin
select id from jobapplicants where userid=param_userid and placementid=param_placementid into m;
update jobapplicants set status='Accepted' where id=m  returning id into n;
return n;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_updatejobapplicantschedule(
	param_userid integer,
	param_placementid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare m int;
declare n int;						
begin
select id from jobapplicants where userid=param_userid and placementid=param_placementid into m;
update jobapplicants set status='Attend' where id=m  returning id into n;
return n;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_updateconversation(
	param_userid character varying,
	param_name character varying,
	param_supportid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare m bigint;
declare n int;
begin
update conversationtable c set userid=param_userid,status='Active',name=param_name where id=(select max(id) from 
						conversationtable where userid isnull and supportid=param_supportId) returning id into n;
return n;
end;
$BODY$;
create or replace function fn_updatetraineeTransferbyfrmBranch(param_frombranchUserid int,param_traineeid int)
RETURNS integer
as
$$
declare n int;
declare brnchid int;
begin
select branchid from userProfileview 
where id in(param_frombranchUserid) into brnchid;
update  transfertrainee set frombranchid=brnchid,status='Approve' where id=param_traineeid;
return n;
end;
$$
language plpgsql;
create or replace function fn_updatetraineeTransferbytoBranch(param_tobranchid int,
param_sessionid int,param_traineeid int)
RETURNS integer
as
$$
declare n int;
begin
update  transfertrainee set tobranchid=param_tobranchid,sessionid=param_sessionid,
status='Approve' where traineeid=param_traineeid;
return n;
end;
$$
language plpgsql;
create or replace function fn_updateTraineeTcAcceptence(param_traineeid int)
RETURNS integer
as
$$
declare n int;
begin
update  transfertrainee set status='Accept' where id=param_traineeid;
return n;
end;
$$
language plpgsql;
CREATE OR REPLACE FUNCTION fn_updatecourse(
    param_id integer,
    param_coursename character varying,
    param_years integer,
    param_semesters character varying,
    param_level integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
AS $BODY$
declare n int;
begin
update courses set
coursename=param_coursename,
years=param_years,
semesters=param_semesters,
level=param_level
    where id=param_id
returning id into n;
return n;
end
$BODY$;
CREATE OR REPLACE FUNCTION fn_updateeventsdetails(
	param_id integer,
	param_name character varying,
	param_departmentid integer,
	param_fromdate date,
	param_todate date,
	param_userid integer,
	param_description character varying,
	param_venue character varying,
	param_starttime time without time zone,
	param_endtime time without time zone,
	param_contactdetailsid integer,
	param_branchid integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n integer;
begin
update events set
name=param_name,
departmentId=param_departmentid,
fromDate= param_fromdate,
toDate=param_todate,
userId=param_userid ,
description=param_description,
venue =param_venue,
startTime=param_starttime,
endTime=param_endtime,
contactdetailsId =param_contactdetailsid,
branchId=param_branchid
where Id=param_id
returning Id into n;
return n;
end;
$BODY$;
create or replace function fn_updatebill(param_id int,param_userid int,param_Organizationid int,
param_buydate timestamp without time zone,param_enddate timestamp without time zone,param_amount int) returns integer as
$$
declare n int;
begin
update billing set userid=param_userid,Organizationid=param_Organizationid,buydate=param_buydate,enddate=param_enddate,
amount=param_amount where id=param_id returning id into n;
return n;
end;
$$
language plpgsql;
CREATE OR REPLACE FUNCTION fn_updatemailbox(
	param_id int)
	
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n int;
 begin
update mailbox mb set status='READ' where id=param_id returning id into n;
return n;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_updateproductavailability(
	params_id integer,
	param_available boolean)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n integer;
begin
update seller set 
available=param_available
where id=params_id
returning Id into n;
return n;
end;
$BODY$;
CREATE OR REPLACE FUNCTION fn_updaterequestrecruitments(
	
	param_id int,
param_status varchar)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
    declare n int;
    begin
     update requestrecruitments set status=param_status where id=param_id
 returning  Id into  n;
    return n;
    end
$BODY$;
CREATE OR REPLACE FUNCTION fn_updateOrganizationbranchstatus(
	params_id integer,
	params_status character varying)
    RETURNS varchar
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n varchar;
begin 
update  branch set
status=params_status
where id=params_id
returning params_status into n;
return n;
end
$BODY$;
CREATE OR REPLACE FUNCTION fn_updateshifts(
	param_id integer,
	param_branchid integer,
	param_shiftname character varying,
	param_days text[],
	param_starttime time without time zone,
	param_endtime time without time zone,
param_graceintime time without time zone,
param_graceouttime time without time zone,
param_optional boolean)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare n integer;
begin
										
update shifts set 
branchId=param_branchid,
shiftName=param_shiftname,
days= param_days,
startTime=param_starttime,
endTime=param_endtime,
gracetime=param_graceintime,
graceouttime=param_graceouttime,optional=param_optional
where Id=param_id
returning Id into n;
return n;
end
										
$BODY$;
CREATE or replace FUNCTION fn_updatestatusofattlog() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
p_status varchar;
statuscode varchar;
p_intime varchar;
p_outtime varchar;
shiftinttime varchar;
shiftouttime varchar; 
p_userid varchar;
usrcode int;
attendancedate varchar;
optional boolean;
totalshiftminutes varchar;
totalminutes varchar;
minutes varchar;
abc varchar;
OTminutes varchar=0;
graceouttime varchar;
payrate double precision;
p_payrate double precision;
OT varchar;
s_userid int;
workingdays int=0;
holidaysCount int=0;
p_usrcode int;
p_otpay int;
p_otpayrate double precision;
   BEGIN
   select a.intime from attendancelog a where a.attendancelogid=NEW.attendancelogid into p_intime;RAISE NOTICE 'intime called on %', p_intime;
   select a.outtime from attendancelog a where a.attendancelogid=NEW.attendancelogid into p_outtime;RAISE NOTICE 'outtime called on %', p_outtime;
   select a.usrcode from attendancelog a where a.attendancelogid=NEW.attendancelogid into usrcode;p_usrcode=usrcode;
   select a.statuscode from attendancelog a where a.attendancelogid=NEW.attendancelogid into statuscode;
   select a.attendancedate from attendancelog a where a.attendancelogid=NEW.attendancelogid into attendancedate;
   select u.appuserid from usermapping u where u.deviceuserid=usrcode into p_userid;
   s_userid=p_userid;
   select otpay from staff where userid=s_userid into p_otpay;
   select count(s.payablestatus) from salarypay s where s.payablestatus!='WO' and s.usrcode=p_usrcode and to_char(cast(s.attendancedate as date), 'YYYY-MM')= to_char(cast(NEW.attendancedate as date), 'YYYY-MM') into workingdays ;
   select count(*) from holiday where branchid=(select branchid from userprofileview where id=s_userid) and cast(NEW.attendanceDate as date) between holidaydate and holidayenddate into holidaysCount;RAISE NOTICE 'holidaysCount called on %', holidaysCount;
   select s.gracetime from shifts s join allocateshifts dt on s.id=dt.shiftid where dt.userid=cast(p_userid as int) and cast(attendancedate as date) between dt.startdate and dt.enddate into shiftinttime;
   select s.endtime from shifts s join allocateshifts dt on s.id=dt.shiftid where dt.userid=cast(p_userid as int) and cast(attendancedate as date) between dt.startdate and dt.enddate into shiftouttime;
   select s.optional from shifts s join allocateshifts dt on s.id=dt.shiftid where dt.userid=cast(p_userid as int) and cast(attendancedate as date) between dt.startdate and dt.enddate into optional; RAISE NOTICE 'optional called on %', optional;
   select ((extract (hour from (s.endtime-s.gracetime)))*60)+((extract(minute from(s.endtime-s.gracetime)))) from shifts s join allocateshifts dt on s.id=dt.shiftid where dt.userid=cast(p_userid as int) and cast(attendancedate as date) between dt.startdate and dt.enddate into totalshiftminutes;RAISE NOTICE 'totalshiftminutes called on %', totalshiftminutes;
	select ((extract (hour from (cast(p_outtime as time)-cast(p_intime as time))))*60)+((extract(minute from(cast(p_outtime as time)-cast(p_intime as time))))) into totalminutes;RAISE NOTICE 'totalminutes called on %', totalminutes;	
select(extract (hour from ((cast(p_outtime as time)-cast(p_intime as time)))))*60 into abc;RAISE NOTICE 'abc called on %', abc;	
select 60-(extract(minute from(cast(p_outtime as time)-cast(p_intime as time)))) into minutes;RAISE NOTICE 'minutes called on %', minutes;	
  IF (optional=true and cast(totalminutes as int)>=cast(totalshiftminutes as int) ) THEN
		p_status= 'F'; RAISE NOTICE 'p_status1 called on %', p_status;
		OTminutes=cast(totalminutes as int)-cast(totalshiftminutes as int);
		
	ELSIF (optional=true and cast(totalminutes as int)<cast(totalshiftminutes as int) ) THEN
		p_status= 'H';   RAISE NOTICE 'p_status2 called on %', p_status;																																   
	ELSIF ((cast(p_intime as time)>cast(shiftinttime as time) and statuscode='P') or (cast(p_outtime as time)<cast(shiftouttime as time) and statuscode='P') ) THEN
        p_status= 'H';  	RAISE NOTICE 'p_status3 called on %', p_status;
		select ((extract (hour from (cast(shiftouttime as time)-cast(p_intime as time))))*60)+((extract(minute from(cast(shiftouttime as time)-cast(p_intime as time))))) into totalminutes;
		IF ((cast(totalminutes as int) )<0) then
		totalminutes='0';
		END IF;
		RAISE NOTICE 'nitesh called on %', totalminutes;
		--totalminutes=cast(cast(totalminutes as int)-(cast(totalminutes as int)-cast(totalshiftminutes as int))as varchar);
	ELSIF ((cast(p_intime as time)<=cast(shiftinttime as time) and statuscode='P') or (cast(p_outtime as time)>=cast(shiftouttime as time) and statuscode='P') ) THEN
       select s.graceouttime from shifts s join allocateshifts dt on s.id=dt.shiftid where dt.userid=cast(p_userid as int) and cast(attendancedate as date) between dt.startdate and dt.enddate into graceouttime;
		IF(cast(graceouttime as time)<cast(p_outtime as time)) THEN
		OTminutes=cast(totalminutes as int)-cast(totalshiftminutes as int);RAISE NOTICE 'OTminutes called on %', OTminutes;	
		ELSE
		totalminutes=totalshiftminutes;  RAISE NOTICE 'totalminutes2 called on %', totalminutes;	
		   END IF;
		p_status= 'F';  RAISE NOTICE 'p_status4 called on %', p_status;	
		IF(p_otpay<=0) then
			totalminutes=totalshiftminutes;RAISE NOTICE 'totalminutes1234 called on %', totalminutes;
			Otminutes=0;RAISE NOTICE 'Otminutes1234 called on %', Otminutes;
		END IF;
	ELSIF (statuscode='A') THEN
        p_status= 'A';  RAISE NOTICE 'p_status5 called on %', p_status;	
	ELSIF (statuscode='WO') THEN
        p_status= 'WO'; RAISE NOTICE 'p_status6 called on %', p_status;
    END IF;
	IF(statuscode!='WO') then
		workingdays=workingdays+1;	RAISE NOTICE 'workingdays1 called on %', workingdays;
	END IF;
	IF(holidaysCount>0) then
		totalminutes=totalshiftminutes;	RAISE NOTICE 'workingdays2 called on %', workingdays;							   
	END IF;
	select salary/(cast(totalshiftminutes as int)*workingdays) from salary where id=(select salary from staff where userid=s_userid) into payrate;p_payrate=payrate;RAISE NOTICE 'p_payrate called on %', p_payrate;
	IF (p_status='WO') then
	totalshiftminutes=0;
	END IF;
	p_otpayrate=p_payrate*p_otpay;
	--update attendancelog as a set status=p_status where a.attendancelogid=NEW.attendancelogid;
	insert into salarypay(usrcode,payablestatus,approvedstatus,totalminutes,payrate,OTminutes,OTpayrate,attendanceDate,shiftminutes,intime,outtime,approvalstatus) values(usrcode,p_status,p_status,cast(totalminutes as int),payrate,cast(Otminutes as int),p_otpayrate,cast(NEW.attendanceDate as date),cast(totalshiftminutes as int),p_intime,p_outtime,'Pending');
    update salarypay s set payrate=p_payrate,otpayrate=p_otpayrate where s.usrcode=p_usrcode and to_char(cast(s.attendancedate as date), 'YYYY-MM')= to_char(cast(NEW.attendancedate as date), 'YYYY-MM');
	RETURN NEW;
   END;
$$;
CREATE TRIGGER updateStatusOfAttLog AFTER INSERT ON attendancelog
FOR EACH ROW EXECUTE PROCEDURE fn_updateStatusOfAttLog();
create  or replace function fn_updateacademicdetails(param_userid integer,param_sscpercentage integer,
													param_interpercentage integer,param_ugpercentage integer,
													param_pgpercentage integer,param_otherpercentage integer,
													param_experience integer) returns integer
													as
													$$
													declare n integer;
													begin
													update academicdetails set sscpercentage=param_sscpercentage,
                                                    	interpercentage=param_interpercentage,ugpercentage=param_ugpercentage,
														pgpercentage=param_pgpercentage,otherpercentage=param_otherpercentage,
														experience=param_experience where userId=param_userid
														returning id into n;
														return n;
													end;
													$$
								language plpgsql;
 create or replace function fn_updateApplicantAggrement(param_applicantaggrementsid int)
returns integer as
$$
declare m int;
declare n int;						
begin
update applicantsaggrements set aggrementstatus='agreed' where id=param_applicantaggrementsid  returning id into n;
return n;
end;
$$ language plpgsql;
create or replace function fn_updatepermission(param_id int,param_roleid int,param_componentid int,param_read boolean,param_write boolean)
returns void as
$$
begin
update permissions set roleid=param_roleid,componentid=param_componentid,read=param_read,write=param_write where id=param_id;
end
$$
language plpgsql;