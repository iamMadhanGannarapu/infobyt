import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubjectComponent } from './subject/subject.component';
import { BatchComponent } from './batch/batch.component';
import { StaffComponent } from './staff/staff.component';
import { TimeTableComponent } from './time-table/time-table.component';
import { TrainerSubjectComponent } from './trainer-subject/trainer-subject.component';
import { TraineeBatchComponent } from './trainee-batch/trainee-batch.component';
import { SessionMasterComponent } from './session-master/session-master.component';
import { SessionSubjectMasterComponent } from './session-subject/session-subject.component';
import {TeamComponent} from './team/team.component'
import { RouteGuardGuard, CanDeactivateGuard } from '../service/route-guard.guard';
import { UnicsolComponent } from './unicsol/unicsol.component';
import { TeamAllocation } from '../modal/Batch/TeamAllocation';
const routes: Routes = [
    { path: 'Subject', component: SubjectComponent, canActivate: [RouteGuardGuard],canActivateChild:[SubjectComponent],canDeactivate:[CanDeactivateGuard] },
    { path: 'Batch', component: BatchComponent, canActivate: [RouteGuardGuard],canActivateChild:[BatchComponent],canDeactivate:[CanDeactivateGuard]},
    { path: 'Session-master', component: SessionMasterComponent, canActivate: [RouteGuardGuard],canActivateChild:[SessionMasterComponent],canDeactivate:[CanDeactivateGuard] },
    { path: 'Trainer-subject', component: TrainerSubjectComponent, canActivate: [RouteGuardGuard],canActivateChild:[TrainerSubjectComponent],canDeactivate:[CanDeactivateGuard] },
    { path: 'Staff', component: StaffComponent, canActivate: [RouteGuardGuard],canActivateChild:[StaffComponent],canDeactivate:[CanDeactivateGuard]},
    { path: 'Time-table', component: TimeTableComponent, canActivate: [RouteGuardGuard],canActivateChild:[TimeTableComponent],canDeactivate:[CanDeactivateGuard] },
    { path: 'Session-subject', component: SessionSubjectMasterComponent, canActivate: [RouteGuardGuard] ,canActivateChild:[SessionSubjectMasterComponent],canDeactivate:[CanDeactivateGuard]},
    { path: 'Trainee-batch', component: TraineeBatchComponent, canActivate: [RouteGuardGuard],canActivateChild:[TraineeBatchComponent],canDeactivate:[CanDeactivateGuard] },
    { path: 'Team', component: TeamComponent, canActivate: [RouteGuardGuard],canActivateChild:[TeamComponent],canDeactivate:[CanDeactivateGuard] },
   { path: 'Unicsol', component: UnicsolComponent },
    { path:'Team-Allocation',component:TeamAllocation }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers:[RouteGuardGuard,SubjectComponent,BatchComponent,SessionMasterComponent,TraineeBatchComponent,CanDeactivateGuard,SessionSubjectMasterComponent,TimeTableComponent,StaffComponent,TrainerSubjectComponent,TeamComponent]
})
export class BatchRoutingModule { }
