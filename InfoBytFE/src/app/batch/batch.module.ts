import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubjectComponent } from './subject/subject.component';
import { SessionSubjectMasterComponent } from './session-subject/session-subject.component';
import { TrainerSubjectComponent } from './trainer-subject/trainer-subject.component';
import { SessionMasterComponent } from './session-master/session-master.component';
import { StaffComponent } from './staff/staff.component';
import { BatchComponent } from './batch/batch.component';
import { TimeTableComponent } from './time-table/time-table.component';
import { RouterModule } from "@angular/router";
import { RatingModule, BsDatepickerModule } from 'ngx-bootstrap';
import { FormsModule } from '@angular/forms'
import { DataTablesModule } from 'angular-datatables';
import { TraineeBatchComponent } from './trainee-batch/trainee-batch.component';
import { ToastrModule } from 'ngx-toastr';
import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';
import { TypeaheadModule } from 'ngx-bootstrap';
import { ImageCompressService, ResizeOptions } from 'ng2-image-compress';
import { UnicsolComponent } from './unicsol/unicsol.component';
import { TeamComponent } from './team/team.component';
import { TeamallocationComponent} from './teamallocation/teamallocation.component'
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatStepperModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import { BatchRoutingModule } from './batch-routing.module';
@NgModule({
  imports: [
    BatchRoutingModule,
    MatTableModule, MatTableModule,
    MatSlideToggleModule,
    CdkTableModule,
    CdkTreeModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule, MatFormFieldModule,
    CommonModule,
    ToastrModule.forRoot(),
    TypeaheadModule, FormsModule, DataTablesModule, RatingModule, BsDatepickerModule.forRoot()
  ], exports: [
    RouterModule
  ], providers: [ImageCompressService, ResizeOptions],
  declarations: [SubjectComponent,TeamallocationComponent,TeamComponent, UnicsolComponent, SessionSubjectMasterComponent, TrainerSubjectComponent, SessionMasterComponent, StaffComponent, BatchComponent, TimeTableComponent, TraineeBatchComponent, UnicsolComponent]
})
export class BatchModule { }
