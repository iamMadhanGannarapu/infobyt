
import { Component, OnInit, ViewChild } from '@angular/core';
import { Staff } from '../../modal/Batch/Staff'
import { BatchService } from '../../service/batch.service';
import { OrganizationService } from '../../service/organization.service';
import { UserService } from '../../service/user.service';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Salaryservice } from '../../service/salary.service';
import { AccountDetails } from '../../modal/Fee/AccountDetails'
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { DatePipe } from '@angular/common';
import { ImageCompressService} from  'ng2-image-compress';
import { StaffShifts } from 'src/app/modal/Batch/staffShifts ';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.scss']
})
export class StaffComponent implements OnInit ,CanActivateChild {
  dataSource;
  banklist:any=[{"Bankname":"Allahabad Bank"},{"Bankname":"Andhra Bank"},
                {"Bankname":"Bank of Baroda"},{"Bankname":"Bank of India"},
                {"Bankname":"Bank of Maharashtra"},{"Bankname":"Canara Bank"},
                {"Bankname":"Central Bank of India"},{"Bankname":"Corporation Bank"},
                {"Bankname":"Dena Bank"},{"Bankname":"Indian Bank"},
                {"Bankname":"Indian Overseas Bank"},{"Bankname":"IDBI Bank"},
                {"Bankname":"Oriental Bank of Commerce"},{"Bankname":"Punjab and Sind Bank"},
                {"Bankname":"Punjab National Bank"},{"Bankname":"State Bank of India"},
                {"Bankname":"Syndicate Bank"},{"Bankname":"UCO Bank"},
                {"Bankname":"Union Bank of India"},{"Bankname":"United Bank of India"},
                {"Bankname":"Vijaya Bank"},{"Bankname":"Yes Bank"},
                {"Bankname":"Axis Bank"},{"Bankname":"Bandhan Bank"},
                {"Bankname":"City Union Bank"},{"Bankname":"Federal Bank"},
                {"Bankname":"HDFC Bank"},{"Bankname":"ICICI Bank"},
                {"Bankname":"IndusInd Bank"},{"Bankname":"IDFC Bank"},
                {"Bankname":"Karur Vysya Bank"},{"Bankname":"Kotak Mahindra Bank"}
              ];
  displayedColumns = ['UserName', 'doj', 'experience', 'realsalary', 'rolename', 'branchname', 'Edit','Excel']
  staff: Staff;
  AccountyDetails: AccountDetails;
  shiftDetails: StaffShifts;
  years: any;
  images: File;
  userDetails: any;
  schoolBranchDetails: any;
  roleDetails: any;
  viewstaff: boolean;
  show1:boolean=true;
  userProfile: any;
  image = { 'URL': '', 'valid': false };
  stfExp: string;
  pipe = new DatePipe('en-US');
  maxDate = new Date();
  minDate = new Date();
  colorTheme = 'theme-dark-blue';
  salaryPatternDetails: any;
  bsConfig: Partial<BsDatepickerConfig>;
  rolename: any;
  msg: any;
  pfvalidation: any;
  bankvalidations: any;
  panvalidations: any;
  ShiftDetails: any;
  shiftList: any
  departmentlist: any;
  read: any;
  write: any;
  roleList: any;
  mdlSampleIsOpen: boolean;
  arrayOne(n: number): any[] {
    return Array(n);
  }
  arrayTwo(n: number): any[] {
    return Array(n);
  }
  constructor( private ics: ImageCompressService,private ls: LoginService, private toastr: ToastrService, private router: Router, private bs: BatchService, private sals: Salaryservice, private os: OrganizationService, private userService: UserService) {
    this.staff = new Staff();
    this.ShiftDetails = new StaffShifts();
    this.AccountyDetails = new AccountDetails();
    this.maxDate.setDate(this.maxDate.getDate() + 180);
    this.minDate.setFullYear(this.minDate.getFullYear() - 25);
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    this.staff.dy='y';
    this.staff.dm='m';
  }
  @ViewChild("addstaff") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  addStaff(salaryPattern: any) {
    var pan = this.AccountyDetails.panNumber
    this.AccountyDetails.panNumber= pan.toUpperCase();
    this.AccountyDetails.staffId = this.staff.userId;
      this.getAllStaff();
        this.bs.saveStaff(this.staff, salaryPattern.value).subscribe((staffData) => {
          if(staffData.result)
          {
          this.AccountyDetails.staffId = staffData.data[0].fn_addstaff;
          this.toastr.success(staffData.message);
          this.sals.saveAccountDetails(this.AccountyDetails).subscribe((data) => {
            if(data.result)
            {
            this.toastr.success(data.message)
          this.router.navigate(['attendance/Usermapping'])
          this.form.reset();
          this.staff.salaryPattern = 0;
          this.staff.userRoleId = 0;
          this.staff.branchId = 0;
            }
            else
            {
              this.toastr.error(data.message)
            }
          this.getAllStaff()
        })
        }
        else
        this.toastr.error(staffData.message);
        this.form.reset();
    })
  }
  verifyRole(rid:any,modal:any){
    this.bs.verifyRole(rid).subscribe((data)=>{
      this.roleList=data.data
      if(this.roleList=="Role already Exists!!"){
      }else{
         this.mdlSampleIsOpen = modal;
      }
    })
}
btnOKClick(){
  this.router.navigate(['settings/Permission'])
}
  fileChangeEvent(fileInput: any) {
    var fileName = fileInput.target.value;
    var idxDot = fileName.lastIndexOf(".") + 1;
    var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
    if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
      ImageCompressService.filesToCompressedImageSource(fileInput.target.files).then(observableImages => {
        observableImages.subscribe((image) => {
          this.image.URL = image.compressedImage.imageDataUrl;
            this.staff.image = image.compressedImage.imageDataUrl;
            this.staff.image = this.staff.image.replace("data:image/gif;base64,", "");
            this.staff.image = this.staff.image.replace("data:image/jpeg;base64,", "");
            this.staff.image = this.staff.image.replace("data:image/jpg;base64,", "");
            this.staff.image = this.staff.image.replace("data:image/png;base64,", "");
        }, (error) => {
          this.toastr.error("Error while converting");
        });
      });
    }else{
      alert("Only jpg/jpeg and png files are allowed!");
    }
  }
  Redirect(){
    this.router.navigate(['attendance/Usermapping'])
  }
  verifyPfnum(inp: string) {
    if (inp != '' && inp != null) {
      this.bs.verifybankData(inp, this.AccountyDetails.pfAccountNumber).subscribe((data) => {
        this.msg = data;
        this.pfvalidation = this.msg.message;
      })
    }
  }
  verifyBankaccount(inp: string) {
    if (inp != '' && inp != null) {
      this.bs.verifybankData(inp, this.AccountyDetails.accountNumber).subscribe((data) => {
        this.bankvalidations = data.data;
        if(this.bankvalidations!='NDF'){
          this.toastr.error('Bank Number Already Exits')
        }
      })
    }
  }
  verifyPannum(inp: string) {
    if (inp != '' && inp != null) {
      this.bs.verifybankData(inp, this.AccountyDetails.panNumber).subscribe((data) => {
        this.panvalidations =  data.data;
        if(this.panvalidations!='NDF'){
          this.toastr.error('PAN Number Already Exits')
        }
      })
    }
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  ddlYears(dy: any) {
    this.stfExp = '';
    this.years = (dy.value)
  }
  ddlMonths(dm: any) {
    this.staff.experience = this.years + '.' + dm.value;
  }
  editStaff(id: number) {
    this.getIds();
    this.bs.getStaffById(id).subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.staff.Id = data.data[0].id;
      this.staff.userId = data.data[0].firstname;
      this.staff.branchId = data.data[0].branchid;
      this.staff.doj = this.pipe.transform(data.data[0].doj, 'MM-dd-yyyy');
      this.staff.experience = data.data[0].experience;
      var splitted = data.data[0].experience.toString().split(".");
      var dy = splitted[0]
      var dm = splitted[1]
      this.staff.dy = parseInt(dy);
      this.staff.dm = parseInt(dm);
      this.staff.salary = data.data[0].realsalary;
      this.staff.userRoleId = data.data[0].userroleid;
      this.staff.userId = data.data[0].userid;
      this.staff.salaryPattern = data.data[0].patternid;
      this.staff.departmentId=data.data[0].departmentid;
      this.staff.shiftId=data.data[0].shiftid;
      this.staff.otPay=data.data[0].otpay;
      this.AccountyDetails.bankName = data.data[0].bankname;
      this.AccountyDetails.accountNumber = data.data[0].accountnumber;
      this.AccountyDetails.panNumber = data.data[0].pannumber;
      this.AccountyDetails.pfAccountNumber = data.data[0].pfnumber;
    }}
    )
  }
  updateStaff() {
    this.getAllStaff();
    this.bs.updateStaff(this.staff,this.AccountyDetails).subscribe(data => {
      if(data.result)
      this.toastr.success(data.message);
      else
      this.toastr.error(data.message);
    });
  }
  getAllStaff() {
    this.bs.getAllStaff().subscribe((data) => {
      if(data.result && data.dat!='NDF')
      {
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      }
    })
  }
  getexcel(){
      this.bs.exportAsExcelFile( this.dataSource.data, 'sample');
}
getPermissions() {
  this.ls.checkRightPermissions('staff').subscribe((data)=>{
    this.read=data.data[0].read;
    this.write=data.data[0].write;
    this.canActivateChild(this.read)
  })
}
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }
  getAllStaffUserss() {
    this.userService.getAllStaffUsers().subscribe((data) => {
      this.userDetails = data.data;
    });
  }
  getSchoolBranchesById() {
    this.os.getBranchById().subscribe((data) => {
      this.schoolBranchDetails = data.data;
    });
  }
  getAllRole() {
    this.userService.getAllRoles().subscribe((data) => {
      this.roleDetails = data.data;
    });
  }
  getSalaryPatterns() {
    this.sals.getSalaryPattern().subscribe((data) => {
      this.salaryPatternDetails = data.data;
    })
  }
  getShifts() {
    this.os.getShiftsByBranch().subscribe((res) => {
      this.shiftList = res.data;
    })
  }
  getIds() {
    this.getAllStaffUserss();
    this.getSchoolBranchesById();
    this.getAllRole();
    this.getSalaryPatterns();
    this.getShifts();
  }
  ngOnInit() {
    if (sessionStorage.getItem('user') != null) {
      this.userService.getUserbySession().subscribe((data) => {
        this.userProfile = data.data[0];
      })
    }
    this.getAllStaff();
    this.staff.userId = 0;
    this.staff.branchId = 0;
    this.staff.salaryPattern = 0;
    this.staff.userRoleId = 0;
    this.staff.experience = 0;
    this.staff.departmentId=0;
    this.staff.shiftId=0;
    this.getDepartment();
    this.getPermissions();
  }
  addStaffUser() {
    this.router.navigate(['user/add-user/Default']);
  }
  viewStaff() {
    this.show1=!this.show1;
    this.viewstaff = !this.viewstaff
  }
  getDepartment() {
    this.os.getAllDepartments().subscribe((data) => {
      if(data.result && data.data!='NDF')
      this.departmentlist = data.data
    })
  }
}