import { Component, OnInit, ViewChild } from '@angular/core';
import { TrainerSubject } from '../../modal/Batch/TrainerSubject';
import { BatchService } from '../../service/batch.service';
import { UserService } from '../../service/user.service';
import { ToastrService } from 'ngx-toastr';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-trainer-subject',
  templateUrl: './trainer-subject.component.html',
  styleUrls: ['./trainer-subject.component.scss']
})
export class TrainerSubjectComponent implements OnInit,CanActivateChild  {
  dataSource;
  displayedColumns = ['SubjectName', 'StaffName', 'experience', 'Excel']
  TrainerAddObj: TrainerSubject;
  Subj: any;
  staff: any;
  ClassTeacher: any;
  rolename: any;
  read: any;
  write: any;
  constructor(private router:Router,private ls: LoginService, private toastr: ToastrService, private bs: BatchService, private us: UserService) {
    this.TrainerAddObj = new TrainerSubject()
  }
  @ViewChild("frm") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  addTrainerSubject() {
    for (var i in this.Subj) {
      if (this.Subj[i].status == true) {
        this.TrainerAddObj.subjectId = this.Subj[i].id;
        this.bs.saveTrainerSubject(this.TrainerAddObj).subscribe(
          (data) => {
            if (data.result) {
              this.toastr.success(data.message);
              this.form.reset();
            }
            else
              this.toastr.error(data.message);
            this.getAllTrainerSubject(); 
          })
      }
    }
  }
  getPermissions() {
    this.ls.checkRightPermissions('trainer-subject').subscribe((data) => {
      this.read =  data.data[0].read;
      this.write =  data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  editTrainerSubject(id: number) {
    this.getIds();
    this.bs.getTrainerSubjectById(id)
      .subscribe((data) => {
        if (data.result && data.data != 'NDF') {
          this.TrainerAddObj.Id = data.data[0].id;
          this.TrainerAddObj.subjectId = data.data[0].subjectid;
          this.TrainerAddObj.staffId = data.data[0].staffid;
        }
      })
  }
  updateTrainerSubject() {
    for (var i in this.Subj) {
      if (this.Subj[i].status == true) {
        this.TrainerAddObj.subjectId = this.Subj[i].id;
        this.bs.updateTrainerSubject(this.TrainerAddObj).subscribe(data => {
          this.getAllTrainerSubject();
        });
      }
    }
  }
  getAllFaculties() {
    this.us.getAllTrainerFromStaff().subscribe((data) => { this.staff = data.data });
  }
  getAllSubjects() {
    this.bs.getAllSubject().subscribe((data) => {
      this.Subj = data.data;
      for (var i in this.Subj) {
        this.Subj[i].status = false
      }
    });
  }
  getAllClassTeachers() {
    this.us.getAllClassTeacher().subscribe((data) => {
      this.ClassTeacher = data.data;
    });
  }
  getIds() {
    this.getAllFaculties();
    this.getAllSubjects();
    this.getAllClassTeachers();
  }
  ngOnInit() {
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename = data.data[0].rolename;
      })
    }
    this.getIds()
    this.getAllTrainerSubject();
    this.TrainerAddObj.staffId = 0;
    this.getPermissions();
  }
  getAllTrainerSubject() {
    this.bs.getTrainerSubject().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      }
    });
  }
  getexcel() {
    this.bs.exportAsExcelFile(this.dataSource.data, 'Trainer Subject');
  }
}
