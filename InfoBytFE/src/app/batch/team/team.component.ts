import { Component, OnInit, ViewChild } from '@angular/core';
import { OrganizationService } from 'src/app/service/organization.service';
import {Team } from '../../modal/Batch/Team';
import { BatchService } from 'src/app/service/batch.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/service/user.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {
  departmentlist: any;
  team:Team;
  dataSource;
  show1:boolean=true;
  displayedColumns = ['Name','TeamSize','Departmentname','hr']
  viewsession: boolean;
  allStaffList:any
   constructor(private toastr: ToastrService,private router: Router,private os: OrganizationService,private bs: BatchService,private us:UserService) {
    this.team= new Team()
   }
   @ViewChild("frm") form: any;
   @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
   addTeam() {
    this.bs.saveTeam(this.team).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.form.reset();
      }
      else{
        this.toastr.error(data.message);
      }
      }
    )
  }
  addHod(){
    this.router.navigate(['user/add-user/Default']);
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getAllTeam() {
    this.bs.getAllTeam().subscribe((res) => {
    if(res.result && res.data!='NDF')
    {
      this.dataSource = new MatTableDataSource(res.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
     } });
  }
getAllHr(){
  this.us.getAllHr().subscribe((data)=>{
    this.allStaffList=data.data
  })
}
  ngOnInit() {
    this.getDepartment();
    this.getAllTeam();
    this.getAllHr()
  }
  getDepartment() {
    this.os.getAllDepartments().subscribe((data) => {
      this.departmentlist = data.data
    })
  }
  viewSession()
  {
    this.show1=!this.show1;
    this.viewsession=!this.viewsession
  }
}
