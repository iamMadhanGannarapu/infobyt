import { Component, OnInit, ViewChild } from '@angular/core';
import { SessionSubject } from '../../modal/Batch/SessionSubject';
import { BatchService } from '../../service/batch.service';
import { OrganizationService } from '../../service/organization.service';
import { UserService } from '../../service/user.service';
import { ToastrService } from 'ngx-toastr';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-session-subject',
  templateUrl: './session-subject.component.html',
  styleUrls: ['./session-subject.component.scss']
})
export class SessionSubjectMasterComponent implements OnInit,CanActivateChild  {
  dataSource;
  displayedColumns = ['subjectname', 'syllabus', 'classname', 'download', 'Excel']
  classSubjectAddObj: SessionSubject
  batchList: any;
  trainerList: any;
  subjectList: any;
  data: any;
  classList: any;
  myurl: string;
  subjectId: any
  read: any;
  write: any
  image = { 'URL': '', 'valid': false };
  rolename: any;
  constructor(private router:Router,private toastr: ToastrService, private ls: LoginService, private bs: BatchService, private os: OrganizationService, private us: UserService) {
    this.classSubjectAddObj = new SessionSubject();
    this.myurl = os.url;
  }
  @ViewChild("frm") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  addClassSubject() {
    this.bs.saveSessionSubject(this.classSubjectAddObj).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.form.reset();
        this.getAllClassSubjects();
      }
      else {
        this.toastr.error(data.message)
      }
    });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getPermissions() {
    this.ls.checkRightPermissions('session-subject').subscribe((data) => {
      this.read = data.data[0].read;
      this.write = data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }
  editClassSubject(id: number) {
    this.getIds();
    this.bs.getSessionSubjctById(id).subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.classSubjectAddObj.Id = data.data[0].id;
        this.classSubjectAddObj.subjectId = data.data[0].subjectid;
        this.classSubjectAddObj.batchId = data.data[0].batchid;
        this.classSubjectAddObj.branchSessionId = data.data[0].branchSessionId;
      }
    });
  }
  updateClassSubject() {
    this.bs.updateSessionSubject(this.classSubjectAddObj)
      .subscribe(data => {
        if (data.result) {
          this.toastr.success(data.message);
          this.getAllClassSubjects();
        }
        else
          this.toastr.error(data.message)
      });
  }
  getAllBatches() {
    this.bs.getAllBatch().subscribe((data) => {
      this.classList = data.data;
    });
  }
  getAllSubjects() {
    this.bs.getAllSubject().subscribe((data) => {
      this.subjectList = data.data;
    });
  }
  getIds() {
    this.getAllBatches();
    this.getAllSubjects();
  }
  fileChangeEvent(event) {
    var fileName = event.target.value;
    var idxDot = fileName.lastIndexOf(".") + 1;
    var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
    if (extFile=="pdf"){
      if (event.target.files && event.target.files[0]) {
        var rd = new FileReader();
        rd.readAsText(event.target.files[0]);
        rd.onload = (ev: any) => {
          this.data = ev.target.result;
        this.classSubjectAddObj.fileUpload = rd.result
        this.image.URL = ev.target.result;
      }
      rd.readAsText(this.classSubjectAddObj.fileUpload)
      }
    }else{
      alert("Only pdf files are allowed!");
    }
  }
  getBatchbyClass() {
    this.bs.getBatchDetailsBySession(this.classSubjectAddObj.branchSessionId).subscribe((data) => {
      this.batchList = data.data;
    })
  }
  getFacultyBySubject() {
    this.bs.getTrainerSubjectBySubjectId(this.subjectId).subscribe((data) => {
      this.trainerList = data.data;
    })
  }
  getAllClassSubjects() {
    this.bs.getAllSessionSubject().subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.dataSource = new MatTableDataSource(data.data)
        this.dataSource.sort = this.sort
        this.dataSource.paginator = this.paginator
      }
    });
  }
  getexcel() {
    this.bs.exportAsExcelFile(this.dataSource.data, 'Session Subject');
  }
  ngOnInit() {
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename = data.data[0].rolename;
      })
    }
    this.getAllClassSubjects();
    this.getIds();
    this.classSubjectAddObj.branchSessionId = 0;
    this.classSubjectAddObj.batchId = 0;
    this.classSubjectAddObj.subjectId = 0;
    this.getPermissions();
  }
}
