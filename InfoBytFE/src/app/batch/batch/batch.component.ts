import { Component, OnInit, ViewChild } from '@angular/core';
import { Batch } from '../../modal/Batch/Batch';
import { BatchService } from '../../service/batch.service';
import { Staff } from '../../modal/Batch/Staff';
import { UserService } from '../../service/user.service';
import { OrganizationService } from '../../service/organization.service';
import { LoginService } from '../../service/login.service';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { DatePipe } from '@angular/common';
import { DashboardService } from "src/app/service/dashboard.service";
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-batch',
  templateUrl: './batch.component.html',
  styleUrls: ['./batch.component.scss']
})
export class BatchComponent implements OnInit,CanActivateChild  {
  dataSource;
  displayedColumns = ['ClassTeacherName', 'ClassName', 'StartDate', 'EndDate', 'Name', 'Medium', 'NumOfStudents', 'edit','Excel']
  batchAddObj: Batch;
  staffList: Staff;
  allStaffList: any
  classList: any
  batchName: any;
  viewbatch:boolean;
  batchId: any;
  myurl: string
  maxDate = new Date();
  minDate = new Date();
  minDatee = new Date();
  frmDate: any;
  toDate: any;
  pipe = new DatePipe('en-US');
  colorTheme = 'theme-dark-blue';
  bsConfig: Partial<BsDatepickerConfig>;
  rolename: any;
  transactionList: any;
  excl: any;
  view=false;
  read: any;
  show1:boolean=true;
write: any;
  constructor(private router:Router,private toastr: ToastrService, private ds:DashboardService,private bs: BatchService, private us: UserService, private os: OrganizationService, private ls: LoginService) {
    this.batchAddObj = new Batch();
    this.myurl = os.url
    this.minDate.setDate(this.minDate.getDate() - 365);
    this.minDate.setDate(this.minDate.getDate() + 1);
    this.maxDate.setDate(this.maxDate.getDate() + 365);
    this.minDatee.setDate(this.minDatee.getDate() + 2);
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
  }
  @ViewChild("batch") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  addBatch() {
    this.frmDate = new Date(this.batchAddObj.startDate[0]);
    this.toDate = new Date(this.batchAddObj.startDate[1]);
    this.batchAddObj.startDate = this.pipe.transform(this.frmDate, 'MM-dd-yyyy');
    this.batchAddObj.endDate = this.pipe.transform(this.toDate, 'MM-dd-yyyy');
    this.bs.saveBatch(this.batchAddObj).subscribe(data => {
      if (data.result) {
        this.toastr.success(data.message);
        this.batchId = data.data[0];
        this.form.reset();
      }
      else
      {
        this.toastr.error(data.message)
      }
      this.ds.addBatchNotification(this.batchId).subscribe((data) => {
      })
    }
    )
  }
  applyFilter(filterValue: string) {
  this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  editBatch(id: number) {
    this.getAllClassTeacheres();
    this.getAllBranchClasses();
    this.getAllClasses();
    this.bs.getBatchById(id)
      .subscribe((data) => {
        if(data.result && data.data!='NDF')
        {
        this.batchAddObj.startDate=[];
        this.batchAddObj.Id = data.data[0].id;
        this.batchAddObj.staffId = data.data[0].staffid;
        this.batchAddObj.sessionId = data.data[0].sessionid;
        this.batchAddObj.startDate[0]=new Date(data.data[0].batchstartdate);
        this.batchAddObj.startDate[1]=new Date(data.data[0].enddate);
        this.batchAddObj.name = data.data[0].batchname;
      }}
      );
  }
  batchUpdate() {
    this.frmDate = new Date(this.batchAddObj.startDate[0]);
    this.toDate = new Date(this.batchAddObj.startDate[1]);
    this.batchAddObj.startDate = this.pipe.transform(this.frmDate, 'MM-dd-yyyy');
    this.batchAddObj.endDate = this.pipe.transform(this.toDate, 'MM-dd-yyyy');
    this.bs.updateBatch(this.batchAddObj).subscribe((data) => {
      if(data.result)
     {
      this.toastr.success(data.message);
    }
      else
      {
      this.toastr.error(data.message);
     } });
  }
  getIds() {
    this.getAllClassTeacheres()
    this.getAllClasses()
    this.form.reset();
    this.batchAddObj.staffId=0;
    this.batchAddObj.sessionId=0;
  }
  getAllClassTeacheres() {
     this.us.getAllClassTeacher().subscribe((data) => {
       this.allStaffList = data.data;
     })
  }
  getAllBranchClasses() {
    this.os.getAllBranchSession().subscribe((data) => {
      this.classList = data.data;
    });
  }
  getAllClasses() {
    this.bs.getAllSession().subscribe((data) => {
      this.batchName = data.data
    })
  }
  getPermissions() {
    this.ls.checkRightPermissions('batch').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }
  getClassName() {
    for (let i in this.classList) {
      if (this.classList[i].id == this.batchAddObj.sessionId)
        this.batchAddObj.name = this.classList[i].sessionname + '-' + (this.minDate.getFullYear())
    }
  }
  getBatch(classid: any) {
    this.batchAddObj.sessionId = classid;
    this.bs.getBatchBySessionId(classid).subscribe((data) => {
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
    })
  }
  getexcel(){
      this.bs.exportAsExcelFile( this.dataSource.data, 'Batch Details');
}
getAllBatchesIfClassNameExist(val:any){
  if(val.length!==''){
    this.view=true;
  }
}
  ngOnInit() {
    $(document).ready(function(){
      $("#datahide").hide();
      $("#showdata").click(function(){
         $("#bath").show()
         $("#showdata").hide()
         $("#datahide").show();
      });
      $("#datahide").click(function(){
        $("#bath").hide()
        $("#showdata").show()
        $("#datahide").hide();
     });
  });
    this.getAllTransactions();
    this.bs.getAllStaff().subscribe((data) => {
      this.staffList = data.data;
    });
    this.getAllBranchClasses();
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename =data.data[0].rolename;
      })
    }
    this.batchAddObj.sessionId=0;
    this.getPermissions();
  }
  getAllTransactions() {
    this.bs.getBatchByUserId().subscribe((data) => {
      this.transactionList = data.data
    });
  }
  viewBatch()
  {
    this.show1=!this.show1;
    this.viewbatch=!this.viewbatch
  }
}