
import { Component, OnInit, ViewChild } from '@angular/core';
import { TimeTable } from '../../modal/Batch/TimeTable';
import { BatchService } from '../../service/batch.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../service/user.service';
import { DashboardService } from "src/app/service/dashboard.service";
import {MatSort, MatTableDataSource,MatPaginator} from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-time-table',
  templateUrl: './time-table.component.html',
  styleUrls: ['./time-table.component.scss']
})
export class TimeTableComponent implements OnInit,CanActivateChild  {
  dataSource;
  displayedColumns=['BatchName','ClassName','Type','Remarks','download','edit','Excel']
  timetableAddObj: TimeTable;
  batchList: any;
  timeTableId:any;
  viewTimetable:boolean;
  data: any;
  myurl:string
  rolename: any;
  image = { 'URL': '', 'valid': false };
  read: any;
  show1:boolean=true;
  write: any;
  constructor(private router:Router,private ls:LoginService, private toastr: ToastrService, private us: UserService, private bs: BatchService,private ds:DashboardService) {
  this.timetableAddObj = new TimeTable();
  this.myurl= us.url
  }
  @ViewChild("timtable") form: any;
  @ViewChild(MatSort) sort:MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  fileChangeEvent(event) {
    var fileName = event.target.value;
    var idxDot = fileName.lastIndexOf(".") + 1;
    var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
    if (extFile=="pdf"){
      if (event.target.files && event.target.files[0]) {
        var rd = new FileReader();
        rd.readAsText(event.target.files[0]);
        rd.onload = (ev: any) => {
          this.data = ev.target.result;
          this.timetableAddObj.fileUpload = rd.result
          this.image.URL= ev.target.result;
        }
        rd.readAsText(this.timetableAddObj.fileUpload)
      }
    }else{
      alert("Only pdf files are allowed!");
    }
  }
  addTimeTable() {
    this.bs.saveTimetable(this.timetableAddObj).subscribe((data) => {
      this.timetableAddObj = data.data;
      if (data.result) {
       this.timeTableId=data.data[0].fn_addtimetable;
        this.toastr.success(data.message);
        this.form.reset();
      }
      else
      {
        this.toastr.error(data.message);
      }
      this.ds.addTimeTableNotification(this.timeTableId).subscribe((data)=>{
      })
      this.getTimeTables();
    })
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  editTimeTable(id: number) {
    this.getIds();
    this.bs.getTimetableById(id).subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.timetableAddObj.Id = data.data[0].id;
      this.timetableAddObj.batchId = data.data[0].batchid;
      this.timetableAddObj.type = data.data[0].type;
      this.timetableAddObj.remarks = data.data[0].remarks;
      }
    });
  }
  updateTimeTable() {
    this.bs.updateTimetable(this.timetableAddObj).subscribe(data => {
      if(data.result)
      this.toastr.success(data.message);
      else
      this.toastr.error(data.message);
      this.getTimeTables();
    });
  }
  getIds() {
    this.bs.getAllBatch().subscribe((data) => {
    this.batchList = data.data
    });
  }
  getTimeTables() {
    this.bs.getAllTimetable().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.dataSource=new MatTableDataSource(data.data)
      this.dataSource.sort=this.sort
      this.dataSource.paginator=this.paginator
      }
    });
  }
  getexcel(){
      this.bs.exportAsExcelFile(this.dataSource.data, 'TimeTable');
}
  ngOnInit() {
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename=data.data[0].rolename;
      })
    }
    this.getTimeTables();
    this.timetableAddObj.batchId=0;
    this.getPermissions();
  }
  back() {
    location.reload()
  }
  viewTimeTable()
  {
    this.show1=!this.show1;
    this.viewTimetable=!this.viewTimetable
  }
  getPermissions() {
    this.ls.checkRightPermissions('time-table').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }
}
