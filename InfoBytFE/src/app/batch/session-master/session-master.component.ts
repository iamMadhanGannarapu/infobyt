import { Component, OnInit, ViewChild } from '@angular/core';
import { Session } from '../../modal/Batch/SessionMaster';
import { BatchService } from '../../service/batch.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../service/user.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-session-master',
  templateUrl: './session-master.component.html',
  styleUrls: ['./session-master.component.scss']
})
export class SessionMasterComponent implements OnInit,CanActivateChild  {
  dataSource;
  displayedColumns = ['name','Excel']
  sessionAddObj: Session;
  sessionGetList: any[] = [];
  viewsession:boolean;
  rolename: any;
  read: any;
  write: any;
  show1:boolean=true;
  constructor(private router:Router,private ls:LoginService,private toastr: ToastrService, private bs: BatchService, private us: UserService) {
    this.sessionAddObj = new Session();
  }
  @ViewChild("frm") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  addSession() {
    this.bs.saveSession(this.sessionAddObj).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.form.reset();
      }
      else
      {
        this.toastr.error(data.message)
      }
      this.getAllSessions()
    }
    )
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  sessionEdit(id: number) {
    this.bs.getSessionById(id)
      .subscribe((data) => {
        if(data.result && data.data!='NDF')
        {
        this.sessionGetList = data.data[0];
        this.sessionAddObj.Id = data.data[0].id;
        this.sessionAddObj.name = data.data[0].classname;
        }
      })
  }
  getAllSessions() {
    this.bs.getAllSession().subscribe((res) => {
      if(res.result && res.data!='NDF')
      {
      this.dataSource = new MatTableDataSource( res.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      }
    });
  }
  getexcel(){
      this.bs.exportAsExcelFile(this.dataSource.data , 'Session Master');
}
  sessionUpdate() {
    this.bs.updateSession(this.sessionAddObj).subscribe((data) => {
      if(data.result)
      this.toastr.success(data.message);
      else
      this.toastr.error(data.message)
      this.getAllSessions();
    })
  }
  ngOnInit(): void {
    this.getAllSessions();
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename =data.data[0].rolename;
      })
    }
    this.getPermissions();
  }
  viewSession()
  {
    this.show1=!this.show1;
    this.viewsession=!this.viewsession
  }
  getPermissions() {
    this.ls.checkRightPermissions('session-master').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }
}