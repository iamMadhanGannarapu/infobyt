import { Component, OnInit, ViewChild } from '@angular/core';
import { BatchService } from '../../service/batch.service';
import { OrganizationService } from '../../service/organization.service';
import { TraineeBatch } from '../../modal/Batch/TraineeBatch';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../service/user.service';
import { DashboardService } from "src/app/service/dashboard.service";
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-trainee-batch',
  templateUrl: './trainee-batch.component.html',
  styleUrls: ['./trainee-batch.component.scss']
})
export class TraineeBatchComponent implements OnInit,CanActivateChild  {
  dataSource;
  displayedColumns = ['BatchName', 'StudentName', 'Status', 'StartDate', 'Modify','Excel']
  traineeBatchMaster: TraineeBatch;
  batchList: any
  list: any;
  myurl: string
  admissionList: any
  traineeBatchGetList: any;
  maxDate = new Date();
  minDate = new Date();
  showstudent: Boolean = false;
  viewtrainee:boolean;
  colorTheme = 'theme-dark-blue';
  bsConfig: Partial<BsDatepickerConfig>;
  rolename: any;
  read: any;
  write: any;
  constructor(
    private router:Router,private ls:LoginService,private toastr: ToastrService, private bs: BatchService, private os: OrganizationService, private us: UserService,private ds:DashboardService) {
    this.traineeBatchMaster = new TraineeBatch();
    this.myurl = os.url;
    this.minDate.setDate(this.minDate.getDate() + 1);
    this.maxDate.setFullYear(this.maxDate.getFullYear() + 10);
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
  }
  @ViewChild("studentsBatchList2") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  getIds() {
    this.os.getAllAdmission().subscribe((data) => {
        this.showstudent = true
        for (let i in data.data) {
          data.data[i].status = false;
        }
        this.admissionList = data.data
        for (let i in this.traineeBatchGetList) {
          for (let j = 0; j < this.admissionList.length; j++) {
            if (this.traineeBatchGetList[i].studentid == this.admissionList[j].admissionnum) {
              this.admissionList.splice(j, 1);
              j--
            }
          }
        }
      });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getAllTraineeBatches() {
    this.bs.getTraineeBatch().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.traineeBatchGetList = data.data;
      for (let i  in this.traineeBatchGetList) {
        this.traineeBatchGetList[i].checkboxStatus = false;
      }
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
    }
    });
  }
  getPermissions() {
    this.ls.checkRightPermissions('trainee-batch').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
this.canActivateChild(this.read)
    })
  }
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }
  getexcel(){
      this.bs.exportAsExcelFile(this.traineeBatchGetList, 'Trainee');
}
  getAllAdmission() {
    this.os.getAllAdmission()
      .subscribe((data) => {
        this.list = data.data
      })
  }
  getList() {
    this.bs.getBatchBySession(this.traineeBatchMaster.batchId).subscribe((data) => {
      this.list = data.data;
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
    })
  }
  getlist2() {
    if (this.traineeBatchMaster.batchId == 0) {
      this.getIds()
    }
  }
  ngOnInit() {
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename=data.data[0].rolename;
      })
    }
    this.getAllTraineeBatches();
    this.bs.getAllBatch()
      .subscribe((data) => {
        this.batchList = data.data;
      });
        this.getPermissions();
        this.traineeBatchMaster.batchId=0;
  }
  addBulkTrainees(batchId2: any) {
    let students = [];
    for (let i in this.list) {
      if (this.list[i].checkboxStatus == true) {
        this.traineeBatchMaster.traineeId = this.list[i].admissionnum;
        students.push(this.list[i].admissionnum);
        this.traineeBatchMaster.batchId = batchId2.value;
        this.traineeBatchMaster.branchId = this.list[i].branchid;
        this.bs.saveTraineeBatch(this.traineeBatchMaster).subscribe((data) => {
          if(data.result)
          this.toastr.success(data.message)
          else
          this.toastr.error(data.message)
          this.getAllTraineeBatches();
        })
      }
    }
    this.ds.saveTraineeBatchNotification(this.list).subscribe((data) => {
    })
  }
  viewTrainee()
  {
    this.viewtrainee=!this.viewtrainee
  }
}
