import { Component, OnInit, ViewChild } from '@angular/core';
import { User, Authentication } from '../../modal/User/User';
import { ContactDetails } from '../../modal/User/ContactDetails';
import { Router, ActivatedRoute } from '../../../../node_modules/@angular/router';
import { UserService } from '../../service/user.service';
import { Observable, of } from 'rxjs';
import { LoginService } from '../../service/login.service';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import * as $ from 'jquery';
import { DatePipe } from '@angular/common';
import { MessageService } from 'primeng/components/common/messageservice';
import { MatStepper } from '@angular/material';
import { SelectItem } from 'primeng/api';
import { AcademicDetails } from '../../modal/User/academicdetails'
import { Staff } from '../../modal/Batch/Staff'
import { BatchService } from '../../service/batch.service';
import { OrganizationService } from '../../service/organization.service';
import { Salaryservice } from '../../service/salary.service';
import { AccountDetails } from '../../modal/Fee/AccountDetails'
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { ImageCompressService } from 'ng2-image-compress';
import { StaffShifts } from 'src/app/modal/Batch/staffShifts ';
@Component({
  selector: 'app-unicsol',
  templateUrl: './unicsol.component.html',
  styleUrls: ['./unicsol.component.scss'],
  styles: [`
  :host ::ng-deep button {
      margin-right: .25em;
  }
  :host ::ng-deep .custom-toast .ui-toast-message {
      color: #ffffff;
      background: #FC466B;
      background: -webkit-linear-gradient(to right, #3F5EFB, #FC466B);
      background: linear-gradient(to right, #3F5EFB, #FC466B);
  }
  :host ::ng-deep .custom-toast .ui-toast-close-icon {
      color: #ffffff;
  }
`],
  providers: [MessageService]
})
export class UnicsolComponent implements OnInit {
  usercountryList: SelectItem[];
  usr: User;
  cdi: ContactDetails;
  isLinear = true;
  noResult: boolean;
  placeList: string[];
  Emailvalidation;
  result = false;
  mobilevalidation;
  aadhaarevalidation;
  occupationList: string[];
  nationalitiesList: string[];
  middleNameList: string[];
  userSession: any;
  msg: any;
  sub: any;
  iagree: any;
  occupationList1: SelectItem[];
  Loginvalidation;
  acadedetails: any;
  viewAcademicForm = false;
  selecteduser: any;
  userDetails: any;
  userreligionList: any[] = [];
  userstateList: any;
  usercitiesList: any;
  pipe = new DatePipe('en-US');
  maxDate = new Date();
  minDate = new Date();
  colorTheme = 'theme-dark-blue';
  bsConfig: Partial<BsDatepickerConfig>;
  loginid: any;
  userName: any;
  user: Authentication
  acade: AcademicDetails;
  showSubmit = false;
  dataSource;
  banklist: any = [{ "Bankname": "Allahabad Bank" }, { "Bankname": "Andhra Bank" },
  { "Bankname": "Bank of Baroda" }, { "Bankname": "Bank of India" },
  { "Bankname": "Bank of Maharashtra" }, { "Bankname": "Canara Bank" },
  { "Bankname": "Central Bank of India" }, { "Bankname": "Corporation Bank" },
  { "Bankname": "Dena Bank" }, { "Bankname": "Indian Bank" },
  { "Bankname": "Indian Overseas Bank" }, { "Bankname": "IDBI Bank" },
  { "Bankname": "Oriental Bank of Commerce" }, { "Bankname": "Punjab and Sind Bank" },
  { "Bankname": "Punjab National Bank" }, { "Bankname": "State Bank of India" },
  { "Bankname": "Syndicate Bank" }, { "Bankname": "UCO Bank" },
  { "Bankname": "Union Bank of India" }, { "Bankname": "United Bank of India" },
  { "Bankname": "Vijaya Bank" }, { "Bankname": "Yes Bank" },
  { "Bankname": "Axis Bank" }, { "Bankname": "Bandhan Bank" },
  { "Bankname": "City Union Bank" }, { "Bankname": "Federal Bank" },
  { "Bankname": "HDFC Bank" }, { "Bankname": "ICICI Bank" },
  { "Bankname": "IndusInd Bank" }, { "Bankname": "IDFC Bank" },
  { "Bankname": "Karur Vysya Bank" }, { "Bankname": "Kotak Mahindra Bank" }
  ];
  displayedColumns = ['UserName', 'doj', 'experience', 'realsalary', 'rolename', 'branchname', 'Edit', 'Excel']
  staff: Staff;
  AccountyDetails: AccountDetails;
  shiftDetails: StaffShifts;
  years: any;
  images: File;
  Stafflist: any[] = [];
  schoolBranchDetails: any;
  roleDetails: any;
  viewstaff: boolean;
  image = { 'URL': '', 'valid': false };
  stfExp: string;
  salaryPatternDetails: any;
  rolename: any;
  pfvalidation: any;
  bankvalidations: any;
  panvalidations: any;
  ShiftDetails: any;
  shiftList: any
  departmentlist: any;
  nextStep: boolean = false;
  hideformOne: boolean = true;
  arrayOne(n: number): any[] {
    return Array(n);
  }
  arrayTwo(n: number): any[] {
    return Array(n);
  }
  constructor(private ics: ImageCompressService, private bs: BatchService, private ss: Salaryservice, private os: OrganizationService, private toastr: ToastrService, private router: Router, private route: ActivatedRoute, private us: UserService, private ls: LoginService, private ms: MessageService) {
    this.minDate.setFullYear(this.maxDate.getFullYear() - 80);
    this.maxDate.setFullYear(this.maxDate.getFullYear() - 20);
    this.usr = new User();
    this.user = new Authentication();
    this.acade = new AcademicDetails();
    this.usr.role = 'Default';
    this.cdi = new ContactDetails();
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    this.staff = new Staff();
    this.ShiftDetails = new StaffShifts();
    this.AccountyDetails = new AccountDetails();
    this.maxDate.setDate(this.maxDate.getDate() + 180);
    this.minDate.setFullYear(this.minDate.getFullYear() - 25);
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    this.staff.dy = 'y';
    this.staff.dm = 'm';
    var unicUrl = this.router.url
    var unicses = unicUrl.substring(unicUrl.lastIndexOf('=') + 1, unicUrl.length + 1)
    sessionStorage.setItem("user", unicses);
  }
  @ViewChild("signup") form: any;
  @ViewChild("adduserForm") form1: any;
  @ViewChild("addstaff") form3: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  getAllPlaces() {
    this.ls.getAllLocationNames(this.cdi.city).subscribe((data) => {
      this.placeList = data.data;
    });
  }
  addUser() {
    this.nextStep = true;
    this.hideformOne = false
    this.usr.fatherName = 'NA';
    this.usr.motherName = 'NA';
    localStorage.setItem("Users", JSON.stringify(this.usr));
    this.toastr.success('Redirecting to Contacts Page');
  }
  getreligion() {
    this.us.getAllReligion().subscribe((data) => {
      this.userreligionList = data.data;
      this.userreligionList.push({ id: -1, name: 'Other' })
    })
  }
  backTo() {
    this.hideformOne = true;
    this.nextStep = false;
  }
  getStates(couId: any): Observable<any> {
    this.us.getAllStates(parseInt(couId)).subscribe((data) => {
      this.userstateList = data.data;
      this.cdi.state = 0;
      this.cdi.city = 0;
    })
    return of(JSON.stringify(this.userstateList));
  }
  verifyloginid(inp: string) {
    if (this.user.userName != '' && this.user.userName != null) {
      if (this.user.userName.length >= 6) {
        this.ls.varifyloginid(inp, this.user.userName).subscribe((data) => {
          this.msg = data;
          this.Loginvalidation = this.msg.message;
          if (!this.Loginvalidation) {
            this.toastr.error('User Name already exists!');
            this.user.userName = '';
          }
          else {
            this.toastr.success('It Suits For U...  :)');
          }
        });
      }
    }
  }
  addContactDetails(addcontactdetails: any) {
    let user = JSON.parse(localStorage.getItem("Users"));
    this.us.saveUser(user).subscribe((data) => {
      this.cdi.userId = data.data.fn_addusers;
      this.us.saveContactDetails(this.cdi).subscribe((data) => {
        this.toastr.success('Registered Successfully')
        localStorage.removeItem('Users');
        if (data.result) {
          this.toastr.success('Registered Successfully');
          this.getAllStaffUserss();
          this.getAllRole()
          this.getSalaryPatterns()
          this.getSchoolBranchesById()
        }
        else {
          this.toastr.error('Registeration Failed');
        }
      })
    })
  }
  showWarn() {
    this.clear();
    this.ms.add({ severity: 'info', summary: 'Please read the agreement carefully' });
  }
  showError() {
    this.clear();
    this.ms.add({ severity: 'error', summary: 'Please accept the agreement' });
  }
  clear() {
    this.ms.clear();
  }
  onRegstr() {
    this.router.navigate(['login/Login']);
  }
  getCountries() {
    this.us.getAllCountries().subscribe((data) => {
      this.usercountryList = data.data
    })
  }
  getCities(stateId: number) {
    this.us.getAllCities(stateId).subscribe((data) => {
      this.usercitiesList = data.data
      this.cdi.city = 0;
    })
  }
  verifyEmail(inp: string) {
    if (this.cdi.email != '' && this.cdi.email != null) {
      if (this.cdi.email.length > 9) {
        this.ls.verifyUserData(inp, this.cdi.email).subscribe((data) => {
          this.msg = data;
          this.Emailvalidation = this.msg.message;
          if (!this.Emailvalidation) {
            this.toastr.error('Email already exists!');
            this.cdi.email = '';
          }
        });
      }
    }
  }
  verifyMobile(inp: string) {
    if (this.cdi.mobile != '' && this.cdi.mobile != null) {
      if (this.cdi.mobile.length > 9) {
        this.ls.verifyUserData(inp, this.cdi.mobile).subscribe((data) => {
          this.msg = data;
          this.mobilevalidation = this.msg.message
          if (!this.mobilevalidation) {
            this.toastr.error('Mobile Number already exists!');
            this.cdi.mobile = '';
          }
        });
      }
    }
  }
  verifyAuthencation(inp: string, inpData: any) {
    this.ls.verifyUserData(inp, inpData.value).subscribe((data) => {
      this.msg = data;
      this.aadhaarevalidation = this.msg.message
    });
  }
  ngOnInit() {
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename = data.data[0].rolename;
      })
    }
    this.staff.userId = 0;
    this.staff.branchId = 0;
    this.staff.salaryPattern = 0;
    this.staff.userRoleId = 0;
    this.staff.experience = 0;
    this.staff.departmentId = 0;
    this.staff.shiftId = 0;
    this.getDepartment();
    var self = this;
    if (sessionStorage.getItem('user') != null) {
      this.userSession = sessionStorage.getItem('user')
      if (this.userSession != null) {
      }
    }
    $(document).ready(function () {
      $('#iagree').hide();
      $('#notagree').hide();
      $('#Accept').click(function () {
        self.showError();
      });
      $('#terms').scroll(function () {
        if ($(this).scrollTop() > 2000) {
          $('#Accept').removeAttr('disabled');
          if ($('#iagree').prop("checked") == false) {
            $('#iagree').show();
            $('#notagree').show();
            self.showWarn();
          }
        }
      })
      $('#iagree').click(function () {
        $("#notagree").remove();
      });
    });
    this.getCountries();
    this.getMiddleNames();
    this.getreligion();
    this.getNationalities();
    this.getOccupations();
    $(document).ready(function () {
      $('.signIn').click(function () {
        $('.fold').toggleClass('active')
      })
    })
    this.sub = this.route.params.subscribe(params => {
      this.usr.role = 'Default';
      this.usr.gender = 'm';
      this.usr.religion = '0';
      this.usr.nationality = '0';
      this.usr.fatherOccupation = '0'
    });
    if (sessionStorage.getItem('user') != null) {
      this.getLastMod();
      this.viewAcademicDetails();
      this.getUSerName();
    }
    this.acade.userId = 0;
    this.acade.experience = 0;
  }
  getMiddleNames() {
    this.us.getAllMiddleNames().subscribe((data) => {
      this.middleNameList = data;
    })
  }
  getOccupations() {
    this.us.getAllOccupations().subscribe((data) => {
      this.occupationList = data.data
      this.occupationList1 = data.data
    })
  }
  getNationalities() {
    this.us.getAllNationalities().subscribe((data) => {
      this.nationalitiesList = data.data
    })
  }
  viewProfile() {
    this.us.getUserbySession().subscribe((data) => {
      this.userDetails = data.data;
    })
  }
  updateProfileUser(frm: any) {
    this.result = false;
    this.ls.updateProfile(this.usr).subscribe((data) => {
      if (data) {
        this.toastr.success('Successfully Update profile ');
      }
    })
  }
  onKeydown(event) {
    if (this.cdi.place != '' || this.cdi.place != null) {
      this.cdi.place = this.cdi.place.replace('/[0-9]/g', '');
      if ((event.keyCode > 64 && event.keyCode < 91) || (event.keyCode > 96 && event.keyCode < 123)) { }
      else { this.cdi.place = this.cdi.place.replace(event.key, ''); }
    }
  }
  filter(query, inputData: any[]): any[] {
    let filtered: any[] = [];
    for (let i = 0; i < inputData.length; i++) {
      let search = inputData[i];
      if (search.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(search);
      }
    }
    return filtered;
  }
  typeaheadNoResults(event: boolean): void {
    this.noResult = event;
  }
  iagreed(stepper: MatStepper) {
    stepper.next();
  }
  editUserName() {
    this.us.getUserbySession().subscribe((data) => {
      if (data) {
        this.user.Id = data.data[0].userid;
      }
    })
  }
  getLastMod() {
    this.us.getUserbySession().subscribe((data) => {
      let Id = data.data[0].userid;
      this.ls.getLastModified(Id).subscribe((data) => {
        this.loginid = data.data[0].loginid
        this.userName = data.data[0].username;
      })
    })
  }
  viewAcademicDiv() {
    this.viewAcademicForm = true;
    this.showSubmit = true;
    if (this.viewAcademicForm) {
      this.result = false;;
    }
    else {
      this.result = true;
    }
    this.getUSerName();
  }
  addAcdemicMark() {
    this.result = false;
    this.viewAcademicForm = false;
    this.us.saveAcademic(this.acade).subscribe((data) => {
      if (data) {
        this.toastr.success('Academic Details Has Been saved');
      }
    },
      (err) => {
        this.toastr.error(err.error.message)
      })
  }
  getUSerName() {
    this.us.getUser().subscribe((data) => {
      this.selecteduser = data.data;
    })
  }
  upDateAcademicdetails() {
    this.us.updatedAcademic(this.acade).subscribe((data) => {
      if (data) {
        this.toastr.success('Academic Details Has Been updated');
      }
    }, (err) => {
      this.toastr.error(err.error.message)
    })
  }
  viewAcademicDetails() {
    this.us.getUserAcademicDetailsBysession().subscribe((data) => {
      this.acadedetails = data.data[0];
    })
  }
  addStaff(salaryPattern: any) {
    var pan = this.AccountyDetails.panNumber
    this.AccountyDetails.panNumber = pan.toUpperCase();
    this.AccountyDetails.staffId = this.staff.userId;
    this.getAllStaff();
    this.toastr.success("Account Details Added Successfully")
    this.bs.saveStaff(this.staff, salaryPattern.value).subscribe((staffData) => {
      this.AccountyDetails.staffId = staffData.data[0].fn_addstaff;
      this.toastr.success('Staff Has Been Added Successfully ');
      this.ss.saveAccountDetails(this.AccountyDetails).subscribe((data) => {
        this.router.navigate([{ outlets: { view: ['UserMapping'] } }])
        this.form.reset();
        this.staff.salaryPattern = 0;
        this.staff.userRoleId = 0;
        this.staff.branchId = 0;
        this.getAllStaff()
      },
        err => {
          this.toastr.error(err.error.message);
        });
      this.form.reset();
    },
      err => {
        this.toastr.error(err.error.message);
      })
  }
  fileChangeEvent(fileInput: any) {
    var fileName = fileInput.target.value;
    var idxDot = fileName.lastIndexOf(".") + 1;
    var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
    if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
      ImageCompressService.filesToCompressedImageSource(fileInput.target.files).then(observableImages => {
        observableImages.subscribe((image) => {
          this.image.URL = image.compressedImage.imageDataUrl;
            this.staff.image = image.compressedImage.imageDataUrl;
            this.staff.image = this.staff.image.replace("data:image/gif;base64,", "");
            this.staff.image = this.staff.image.replace("data:image/jpeg;base64,", "");
            this.staff.image = this.staff.image.replace("data:image/jpg;base64,", "");
            this.staff.image = this.staff.image.replace("data:image/png;base64,", "");
        }, (error) => {
          this.toastr.error("Error while converting");
        });
      });
    }else{
      alert("Only jpg/jpeg and png files are allowed!");
    }
  }
  verifyPfnum(inp: string) {
    if (inp != '' && inp != null) {
      this.bs.verifybankData(inp, this.AccountyDetails.pfAccountNumber).subscribe((data) => {
        this.msg = data;
        this.pfvalidation = this.msg.message;
      })
    }
  }
  verifyBankaccount(inp: string) {
    if (inp != '' && inp != null) {
      this.bs.verifybankData(inp, this.AccountyDetails.accountNumber).subscribe((data) => {
        this.msg = data;
        this.bankvalidations = this.msg.message;
      })
    }
  }
  verifyPannum(inp: string) {
    if (inp != '' && inp != null) {
      this.bs.verifybankData(inp, this.AccountyDetails.panNumber).subscribe((data) => {
        this.msg = data;
        this.panvalidations = this.msg.message;
      })
    }
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  ddlYears(dy: any) {
    this.stfExp = '';
    this.years = (dy.value)
  }
  ddlMonths(dm: any) {
    this.staff.experience = this.years + '.' + dm.value;
  }
  updateStaff() {
    this.getAllStaff();
    this.bs.updateStaff(this.staff, this.AccountyDetails).subscribe(data => {
      this.toastr.success('Successfully Updated Details');
    },
      err => {
        this.toastr.error(err.error.message);
      });
  }
  getAllStaff() {
    this.bs.getAllStaff().subscribe((data) => {
      this.Stafflist = data.data;
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
    },
      err => {
        this.toastr.error(err.error.message);
      })
  }
  getexcel() {
    this.bs.exportAsExcelFile(this.Stafflist, 'sample');
  }
  getAllStaffUserss() {
    this.us.getAllStaffUsers().subscribe((data) => {
      this.userDetails = data.data;
    },
      err => {
        this.toastr.error(err.error.message);
      });
  }
  getSchoolBranchesById() {
    this.os.getBranchById().subscribe((data) => {
      this.schoolBranchDetails = data.data;
    },
      err => {
        this.toastr.error(err.error.message);
      });
  }
  getAllRole() {
    this.us.getAllRoles().subscribe((data) => {
      this.roleDetails = data.data;
    },
      err => {
        this.toastr.error(err.error.message);
      });
  }
  getSalaryPatterns() {
    this.ss.getSalaryPattern().subscribe((data) => {
      this.salaryPatternDetails = data.data;
    },
      err => {
        this.toastr.error(err.error.message);
      })
  }
  getShifts() {
    this.os.getShiftsByBranch().subscribe((res) => {
      this.shiftList = res.data;
    })
  }
  getIds() {
    this.getAllStaffUserss();
    this.getSchoolBranchesById();
    this.getAllRole();
    this.getSalaryPatterns();
    this.getShifts();
  }
  addStaffUser() {
    this.router.navigate(['user/add-user/Default']);
  }
  viewStaff() {
    this.viewstaff = !this.viewstaff
  }
  getDepartment() {
    this.os.getAllDepartments().subscribe((data) => {
      if (data.data != 'NDF')
        this.departmentlist = data.data
    })
  }
}
