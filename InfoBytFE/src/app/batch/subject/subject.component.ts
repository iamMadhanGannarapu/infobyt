import { Component, OnInit, ViewChild } from '@angular/core';
import { Subject } from '../../modal/Batch/Subject'
import { BatchService } from '../../service/batch.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../service/user.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.scss']
})
export class SubjectComponent implements OnInit,CanActivateChild  {
  dataSource;
  displayedColumns = ['name', 'Excel']
  subjectAddObj: Subject;
  viewsubject: boolean;
  rolename: any;
  read: any;
  write: any;
  show1:boolean=true;
  constructor(private router:Router,private toastr: ToastrService, private ls: LoginService, private bs: BatchService, private us: UserService) {
    this.subjectAddObj = new Subject();
  }
  @ViewChild("frm") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  addSubject() {
    this.bs.saveSubject(this.subjectAddObj).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.form.reset();
      }
      else {
        this.toastr.error(data.message);
      }
      this.getAllSubjects();
    })
  }
  editSubject(id: number) {
    this.bs.getSubjectById(id).subscribe((res) => {
      if (res.result && res.data != 'NDF') {
        this.subjectAddObj.Id = res[0].id;
        this.subjectAddObj.name = res[0].name
      }
    });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  updateSubject() {
    this.bs.updateSubject(this.subjectAddObj).subscribe(data => {
      if (data.result)
        this.toastr.success(data.message);
      else
        this.toastr.error(data.message);
    });
    this.getAllSubjects();
  }
  getAllSubjects() {
    this.bs.getAllSubject().subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.dataSource = new MatTableDataSource(data.data)
        this.dataSource.sort = this.sort
        this.dataSource.paginator = this.paginator
      }
    });
  }
  getexcel() {
    this.bs.exportAsExcelFile( this.dataSource.data , 'Subject');
  }
  ngOnInit() {
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename = data.data[0].rolename;
      })
    }
    this.getAllSubjects();
    this.getPermissions();
  }
  viewSubject() {
    this.show1=!this.show1;
    this.viewsubject = !this.viewsubject
  }
  getPermissions() {
    this.ls.checkRightPermissions('subject').subscribe((data) => {
      this.read = data.data[0].read;
      this.write = data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }
}
