import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/service/user.service';
import { BatchService } from 'src/app/service/batch.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { TeamAllocation } from 'src/app/modal/Batch/TeamAllocation';
import { OrganizationService } from 'src/app/service/organization.service';
import { Salaryservice } from 'src/app/service/salary.service';
@Component({
  selector: 'app-teamallocation',
  templateUrl: './teamallocation.component.html',
  styleUrls: ['./teamallocation.component.scss']
})
export class TeamallocationComponent implements OnInit {
  showFilter: any;
  selectList: any;
  userTrainerList: any;
  userTraineeList: any;
  teamAllocationObj: TeamAllocation;
  teamList: any;
  roleDetails: any;
  dataSource;
  displayedColumns = ['name', 'modify', 'Excel']
  dataSources;
  displayedColumn = ['TeamName', 'UserName', 'Departmentname']
  departmentlist: any;
  departmentId: any
  teamallocation: boolean
  date: any
  show1:boolean=true;
  salaryPatternDetails: any
  constructor(private os: OrganizationService, private toastr: ToastrService, private us: UserService, private bs: BatchService, private ss: Salaryservice) {
    this.date = new Date();
    this.teamAllocationObj = new TeamAllocation();
  }
  @ViewChild("studentsBatchList2") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getDepartment() {
    this.os.getAllDepartments().subscribe((data) => {
      this.departmentlist = data.data
    })
  }
  getIds() {
    this.bs.getAllTraineesfromTraineeBatch().subscribe((data) => {
      this.userTraineeList = data.data
      for (var i in this.userTraineeList) {
        this.userTraineeList[i].checkboxStatus = false
      }
    })
    this.bs.getAllStaff().subscribe((data) => {
      this.userTrainerList = data.data;
      for (var i in this.userTrainerList) {
        this.userTrainerList[i].checkboxStatus = false
      }
    })
  }
  getAllRole() {
    this.us.getAllRoles().subscribe((data) => {
      this.roleDetails = data.data;
    });
  }
  getTeams() {
    this.bs.getTeamByDepartment(this.teamAllocationObj.departmentId).subscribe((data) => {
      this.teamList = data.data
    })
  }
  showTrainer() {
    if (this.showFilter == 'faculty') {
      this.selectList = this.userTrainerList
      this.dataSource = new MatTableDataSource(this.userTrainerList)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
    }
    else {
      this.selectList = this.userTraineeList
      this.dataSource = new MatTableDataSource(this.userTraineeList)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
    }
  }
  getAllteamAllocation() {
    this.bs.getAllTeamAllocationMembers().subscribe((data) => {
      if(data.result && data.message!='NDF')
      this.dataSources = new MatTableDataSource(data.data)
      this.dataSources.sort = this.sort
      this.dataSources.paginator = this.paginator
    })
  }
  addBulkMembers() {
    this.teamAllocationObj.doj = this.date
    this.bs.saveTeamAllocation(this.teamAllocationObj, this.selectList).subscribe((data) => {
      this.getAllteamAllocation()
      if (data.result) {
        this.toastr.success(data.message)
        this.form.reset()
      }
      else
        this.toastr.error(data.message)
    })
  }
  getSalaryPatterns() {
    this.ss.getSalaryPattern().subscribe((data) => {
      this.salaryPatternDetails = data.data;
    })
  }
  viewAllteamAllocation() {
    this.show1=!this.show1;
    this.teamallocation = !this.teamallocation
  }
  ngOnInit() {
    this.getIds();
    this.getDepartment();
    this.getAllteamAllocation();
    this.getAllRole();
    this.getSalaryPatterns()
  }
}
