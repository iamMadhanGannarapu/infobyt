import { Component, OnInit, ViewChild } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { HostelFee } from '../../modal/Services/hostelfee';
import { ToastrService } from 'ngx-toastr';
import { ServicesService } from '../../service/services.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-hostelfee',
  templateUrl: './hostelfee.component.html',
  styleUrls: ['./hostelfee.component.scss']
})
export class HostelfeeComponent implements OnInit,CanActivateChild  {
  dataSource;
  displayedColumns = ['totalAmount', 'Payamount', 'payDate', 'paymode', 'dueAmount', 'name', 'branchname', 'edit', 'Excel']
  bsConfig: Partial<BsDatepickerConfig>;
  colorTheme = 'theme-dark-blue';
  hostelDetails: any
  hstlfee: HostelFee;
  maxDate = new Date();
  minDate = new Date();
  read: any;
  write: any;
  constructor(private router:Router,private ls:LoginService, private ss: ServicesService, private toastr: ToastrService) {
    this.minDate.setDate(this.minDate.getDate());
    this.maxDate.setFullYear(this.maxDate.getFullYear() + 1);
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    this.hstlfee = new HostelFee();
  }
  @ViewChild("hostelfeeForm") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  calculateDue() {
    this.hstlfee.dueAmount = this.hstlfee.totalAmount - this.hstlfee.paidAmount
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  addhostelFeePayments() {
    this.ss.savehostelfee(this.hstlfee).subscribe((data) => {
      if(data.result)
      {
        this.toastr.success(data.message)
      }
      else
      {
        this.toastr.error(data.message)
      }
    })
  }
  getAllhostels() {
    this.ss.getAllhostel().subscribe((data) => {
      this.hostelDetails = data.data;
    })
  }
  getAllhostelFee() {
    this.ss.getAllHostelFee().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      }
    });
  }
  getexcel() {
    this.ss.exportAsExcelFile(this.dataSource.data, 'Hostel Fee Details');
  }
  updHostelFee() {
    this.ss.updateHostelFee(this.hstlfee).subscribe((data) => {
      if(data.result)
      this.toastr.success(data.message);
      else
      this.toastr.error(data.message)
    })
    this.getAllhostelFee();
  }
  editHostelFee(id: any) {
    this.getAllhostels()
    this.ss.getAllHostelFeeById(id).subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.hstlfee.Id = data.data[0].hostelfeeid
      this.hstlfee.dueAmount = data.data[0].dueamount
      this.hstlfee.hostelId = data.data[0].hostelid
      this.hstlfee.paidAmount = data.data[0].paidamount
      this.hstlfee.payDate = data.data[0].paydate
      this.hstlfee.payMode = data.data[0].paymode
      this.hstlfee.totalAmount = data.data[0].totalamount
      }
    })
  }
  ngOnInit() {
    $(document).ready(function(){
      $("#datahide").hide();
      $("#showdata").click(function(){
         $("#viewhstfee").show()
         $("#showdata").hide()
         $("#datahide").show();
      });
      $("#datahide").click(function(){
        $("#viewhstfee").hide()
        $("#showdata").show()
        $("#datahide").hide();
     });
  });
    this.getPermissions();
    this.getAllhostelFee();
    this.getAllhostels();
    this.hstlfee.hostelId = 0;
  }
  getPermissions() {
    this.ls.checkRightPermissions('hostelfee').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }
}
