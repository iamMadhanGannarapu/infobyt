import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LibraryComponent } from './library/library.component';
import { SportsComponent } from './sports/sports.component';
import { GatepassComponent } from './gatepass/gatepass.component';
import { HostelComponent } from './hostel/hostel.component';
import { VisitingComponent } from './visiting/visiting.component';
import { HostelfeeComponent } from './hostelfee/hostelfee.component';
import { RouteGuardGuard, CanDeactivateGuard } from '../service/route-guard.guard';
const routes: Routes = [
  { path: 'Library', component: LibraryComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard] ,canActivateChild:[LibraryComponent]},
  { path: 'Sports', component: SportsComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard],canActivateChild:[SportsComponent]},
  { path: 'Gatepass', component: GatepassComponent, canActivate: [RouteGuardGuard] ,canActivateChild:[GatepassComponent]},
  { path: 'Hostel', component: HostelComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard],canActivateChild:[HostelComponent] },
  { path: 'Visiting', component: VisitingComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard],canActivateChild:[VisitingComponent]},
  { path: 'Hostelfee', component: HostelfeeComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard] ,canActivateChild:[HostelfeeComponent]},
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [RouteGuardGuard,CanDeactivateGuard,GatepassComponent,HostelComponent,SportsComponent,HostelfeeComponent,VisitingComponent,LibraryComponent]
})
export class ServicesRoutingModule { }
