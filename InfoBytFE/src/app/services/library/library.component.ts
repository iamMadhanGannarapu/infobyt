
import { Component, OnInit, ViewChild } from '@angular/core';
import { ServicesService } from 'src/app/service/services.service';
import { ToastrService } from 'ngx-toastr';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { Library } from 'src/app/modal/Services/Library'
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { TraineeLibrary } from 'src/app/modal/Services/Traineelibrary';
import { UserService } from 'src/app/service/user.service';
import { RequestLibrary } from 'src/app/modal/Services/RequestLibrary';
import { OrganizationService } from 'src/app/service/organization.service';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.scss']
})
export class LibraryComponent implements OnInit, CanActivateChild {
  displayedColumns = ['departmentname', 'bookname', 'noofbooks', 'segregation', 'View', 'request', 'Excel']
  dataSource;
  dataSources;
  dataSourcer
  displayedColumn = ['username', 'bookname', 'returndate', 'history', 'return', 'Excel']
  displayedColumnr = ['bookname', 'username', 'returndate', 'status', 'Appove', 'Excel']
  count: any
  userProfile: any
  rolename: any
  historylist: any
  libraryAddObj: Library
  studentLibraryAddObj: TraineeLibrary
  showBook: boolean=true;
  showBook1: boolean=true;
  showIssue: boolean;
  departmentlist: any
  getLibrary: any;
  branchList: any;
  noofbooks: any;
  userStudentList: any;
  booksList: any
  userFacultyList: any;
  libraryListById: any
  showFilter: string = ''
  selectList: any
  
  showRequest: boolean;
  updateRequestList: RequestLibrary
  colorTheme = 'theme-dark-blue';
  
  libcount: any
  stuliblist: any
  bsConfig: Partial<BsDatepickerConfig>;
  penalty: string;
  delaydays: string;
  fine: string;
  returnDate: any;
  write: any;
  read: any;
  constructor(private router: Router, private ls: LoginService, private ss: ServicesService, private os: OrganizationService, private toastr: ToastrService, private us: UserService) {
    this.libraryAddObj = new Library()
    this.studentLibraryAddObj = new TraineeLibrary()
    this.updateRequestList = new RequestLibrary()
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("stdntlib") form: any;
  @ViewChild("schbrn") form1: any;
  @ViewChild("addschoolbranch") form2: any;
  addLibrary() {
    for (var i = 0; i < this.noofbooks; i++) {
      this.ss.saveLibrary(this.libraryAddObj).subscribe((data) => {
        if (data.result) {
          this.toastr.success(data.message);
          this.getBooks();
          this.getAllBooks();
        }
        else {
          this.toastr.error(data.message);
        }
      });
    }
  }
  addStudentLibrary() {
    this.ss.saveTraineeLibrary(this.studentLibraryAddObj).subscribe((data) => {
      if (data.result) {
        this.getBooks();
        this.getAllBooks();
        this.getIssuedetails()
        this.form.reset();
        this.toastr.success(data.message);
      }
      else {
        this.toastr.error(data.message)
      }
    })
  }
  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getIssuedetails() {
    this.ss.getAllTraineeLibrary().subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        
        this.dataSources = new MatTableDataSource(data.data)
        this.dataSources.sort = this.sort
        this.dataSources.paginator = this.paginator
      }
    })
  }
  getexcel() {
    this.ss.exportAsExcelFile(this.dataSources, 'Trainee Library');
  }
  getSchoolBranchByIds() {
    this.os.getBranchById().subscribe((data) => {
      this.branchList = data.data;
      this.libraryAddObj.branchId = 0;
    })
  }
  getAllDepartments() {
    this.os.getAllDomains().subscribe((data) => {
      this.departmentlist = data.data
      this.libraryAddObj.departmentId = 0;
    })
  }
  getIdAdd() {
    this.showBook=!this.showBook;
    this.getSchoolBranchByIds();
    this.getAllDepartments();
    this.showBook = true
    this.showIssue = false
    this.showRequest = false
  }
  getAllStudentFromApplicant() {
    this.us.getAllFromApplicantsForTrainees().subscribe((data) => {
      this.userStudentList = data.data
    })
  }
  getAllStaff() {
    this.us.getAllStaffUsersForFeedBack().subscribe((data) => {
      this.userFacultyList = data.data
    })
  }
  getBooks() {
    this.ss.getLibrary().subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.dataSource = new MatTableDataSource(data.data)
        this.dataSource.sort = this.sort
        this.dataSource.paginator = this.paginator
      }
    })
  }
  getexcel1() {
    this.ss.exportAsExcelFile(this.dataSource.data, 'Library ');
  }
  getAllBooks() {
    this.ss.getAllLibrary().subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.booksList = data.data;
        this.studentLibraryAddObj.bookId = 0;
      }
    })
  }
  showFaculty() {
    if (this.showFilter == 'faculty') {
      this.selectList = this.userFacultyList
    }
    else {
      this.selectList = this.userStudentList
    }
  }
  getBookdetails(id: any) {
    this.getnameofIssued(id)
    this.getCountforAll(id)
  }
  getnameofIssued(id: any) {
    this.ss.getTraineeLibrarry(id).subscribe((data) => {
      this.stuliblist = data.data
    })
  }
  getCountforAll(id: any) {
    this.ss.getLibrarry(id).subscribe((data) => {
      this.libcount = data.data
    })
  }
  getIdIssue() {
    this.showBook1=!this.showBook1;
    this.getAllStudentFromApplicant();
    this.getAllStaff()
    this.getAllBooks()
    this.showBook = false
    this.showIssue = true
    this.showRequest = false
    this.studentLibraryAddObj.bookId = 0;
    this.studentLibraryAddObj.userId = 0;
    this.showFilter = 'student';
  }
  sendRequest(bnm: any) {
    this.ss.getAllLibraryByname(bnm).subscribe((data) => {
      this.libraryListById = data.data
      this.ss.getMaxBookId(this.libraryListById[0].bookname).subscribe((data) => {
        this.toastr.success('Book Has Been Requested Successfully  ');
      })
    })
  }
  getAllRequests() {
    this.ss.getAllBookRequests().subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.dataSourcer = new MatTableDataSource(data.data)
        this.dataSourcer.sort = this.sort
        this.dataSourcer.paginator = this.paginator
        this.showRequest = true
        this.showBook = false
        this.showIssue = false
      }
    })
  }
  getexcel2() {
    this.ss.exportAsExcelFile(this.dataSourcer.data, 'BookRequest');
  }
  ReturnEdit(id: any) {
    this.ss.updateTraineeLibraryDetails(id).subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.studentLibraryAddObj.Id = data.data[0].id;
        this.studentLibraryAddObj.bookId = data.data[0].bookid;
        this.studentLibraryAddObj.userId = data.data[0].userid;
        this.studentLibraryAddObj.returnDate = data.data[0].returndate;
        this.ss.getdaysCount(this.studentLibraryAddObj.returnDate, this.studentLibraryAddObj.Id).subscribe((data) => {
          this.delaydays = JSON.stringify(data.data[0].delaydays)
          this.penalty = JSON.stringify(data.data[0].penalty)
          this.fine = JSON.stringify(data.data[0].fine)
        })
      }
    })
  }
  updateStudentLibrary() {
    this.ss.updatetraineelibrarytDetails(this.studentLibraryAddObj).subscribe((data) => {
      if (data.result)
        this.toastr.success(data.message);
      else
        this.toastr.error(data.message);
    })
  }
  acceptRequest(requestid: any, status: any) {
    this.ss.getAllBookRequestsById(requestid).subscribe((data) => {
      this.updateRequestList.Id = data.data[0].id;
      this.updateRequestList.bookId = data.data[0].bookid;
      this.updateRequestList.returnDate = this.returnDate;
      this.updateRequestList.userId = data.data[0].userid;
      this.updateRequestList.status = status.value;
      this.ss.updateLibraryRequestDetails(this.updateRequestList).subscribe((data) => {
        this.getAllRequests()
      })
    })
    this.ngOnInit();
  }
  getPermissions() {
    this.ls.checkRightPermissions('library').subscribe((data) => {
      this.read = data.data[0].read;
      this.write = data.data[0].write;
      this.canActivateChild(this.read)
    })
  }


  canActivateChild(x: any): boolean {
    if (x == true) {
      return true;
    } else {
      this.router.navigate(['404']);
      return false;
    }
  }

  ngOnInit() {
    this.getPermissions();
    this.getBooks();
    this.getIssuedetails();
    this.getAllRequests();
    this.getRequests()
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((school) => {
        this.rolename = school[0].rolename;
      })
    }
    this.libraryAddObj.branchId = 0;
    this.libraryAddObj.departmentId = 0;
  }
  getRequests() {
    this.ss.getRequestCount().subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.count = data.data[0].requests
      }
    })
  }
  viewTransactions(id: any) {
    this.ss.getAllLibraryTransactionById(id).subscribe((data) => {
      this.historylist = data.data
    })
  }
}