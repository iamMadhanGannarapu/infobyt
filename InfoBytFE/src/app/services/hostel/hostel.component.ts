import { Component, OnInit, ViewChild } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { UserService } from '../../service/user.service';
import { Hostel } from '../../modal/Services/hostel'
import { ServicesService } from '../../service/services.service';
import { ToastrService } from 'ngx-toastr';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { OrganizationService } from 'src/app/service/organization.service';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-hostel',
  templateUrl: './hostel.component.html',
  styleUrls: ['./hostel.component.scss']
})
export class HostelComponent implements OnInit, CanActivateChild {
  dataSource;
  dataSource1;
  dataSource2;
  displayedColumns = ['UserName', 'Type', 'BlockName', 'RoomNumber', 'RepresentativeName', 'edit', 'Excel']
  displayedColumns1 = ['UserName', 'Email', 'Mobile', 'Rolename', 'Approve']
  displayedColumns2 = ['UserName', 'Email', 'Mobile', 'hostel']
  userDetails: any;
  hst: Hostel;
  schoolBranchDetails: any;
  bsConfig: Partial<BsDatepickerConfig>;
  maxDate = new Date();
  minDate = new Date();
  colorTheme = 'theme-dark-blue';
  rolename: any;
  allStaffList: any;
  read: any;
  write: any;
  constructor(private router: Router, private ls: LoginService, private us: UserService, private ss: ServicesService, private os: OrganizationService, private toastr: ToastrService) {
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    this.hst = new Hostel();
  }
  @ViewChild("addhostel") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  addhostel() {
    this.ss.savehostel(this.hst).subscribe((data) => {
      this.getAllhostels()
      if (data.result) {
        this.toastr.success(data.message)
        this.form.reset();
      }
      else {
        this.toastr.error(data.message)
      }
    })
  }
  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getAllhostels() {
    this.ss.getAllhostel().subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.dataSource = new MatTableDataSource(data.data)
        this.dataSource.sort = this.sort
        this.dataSource.paginator = this.paginator
      }
    })
  }
  getexcel() {
    this.ss.exportAsExcelFile(this.dataSource.data, 'Hostel');
  }
  getAppliedList() {
    this.ss.getAllTraineeappliedlist().subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.dataSource1 = new MatTableDataSource(data.data)
        this.dataSource.sort = this.sort
        this.dataSource.paginator = this.paginator
      }
    })
  }
  approveHostel(userid: number) {
    this.ss.approveTraineeForhostel(userid).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
      }
      else {
        this.toastr.error(data.message)
      }
    });
  }
  editHostel(id: any) {
    this.getid()
    this.ss.getAllHostelsById(id).subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.hst.Id = data.data[0].hostelid
        this.hst.type = data.data[0].type
        this.hst.allocationDate = data.data[0].allocationdate
        this.hst.blockName = data.data[0].blockname
        this.hst.branchId = data.data[0].branchid
        this.hst.facilities = data.data[0].facilities
        this.hst.rentalPeriod = data.data[0].rentalperiod
        this.hst.representativeId = data.data[0].representativeid
        this.hst.roomNum = data.data[0].roomnum
        this.hst.sharing = data.data[0].sharing
        this.hst.userId = data.data[0].hosteluserid
      }
    })
  }
  updateHostel() {
    this.ss.updtHostel(this.hst).subscribe((data) => {
      if (data.result)
        this.toastr.success(data.message);
      else
        this.toastr.error(data.message)
    }, (err) => {
      this.toastr.error(err.error.message)
    })
    this.getAllhostels();
  }
  getSchoolBranchesById() {
    this.os.getBranchById().subscribe((data) => {
      this.schoolBranchDetails = data.data;
    });
  }
  getuserdetailsByid() {
    this.os.getuserById().subscribe((data) => {
      this.dataSource2 = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
    });
  }
  getAllStudentUsers() {
    this.ss.getAllHostelApplicants().subscribe((data) => {
      this.userDetails = data.data;
    });
  }

  applyHostel() {
    this.ss.Applyhostel().subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
      }
      else {
        this.toastr.error(data.message)
      }
    });
  }
  getAllRepresentatives() {
    this.us.getAllWardens().subscribe((data) => {
      this.allStaffList = data.data;
    })
  }
  getid() {
    this.getAllStudentUsers();
    this.getSchoolBranchesById();
    this.getAllhostels();
    this.getAllRepresentatives();
  }
  getPermissions() {
    this.ls.checkRightPermissions('hostel').subscribe((data) => {
      this.read = data.data[0].read;
      this.write = data.data[0].write;
      this.canActivateChild(this.read)
    })
  }

  canActivateChild(x: any): boolean {
    if (x == true) {
      return true;
    } else {
      this.router.navigate(['404']);
      return false;
    }
  }

  ngOnInit() {

    $(document).ready(function () {
      
      $("#datahide1").hide();
      $("#datahide2").hide();
      $("#datahide3").hide();
      $("#showdata1").click(function () {
        $("#viewhst1").show()
        $("#showdata1").hide()
        $("#datahide1").show();
      });
      $("#showdata2").click(function () {
        $("#viewhst2").show()
        $("#showdata2").hide()
        $("#datahide2").show();
      });
      $("#showdata3").click(function () {
        $("#viewhst3").show()
        $("#showdata3").hide()
        $("#datahide3").show();
      });


      $("#datahide1").click(function () {
        $("#viewhst1").hide()
        $("#showdata1").show()
        $("#datahide1").hide();
      });
      $("#datahide2").click(function () {
        $("#viewhst2").hide()
        $("#showdata2").show()
        $("#datahide2").hide();
      });

      $("#datahide3").click(function () {
        $("#viewhst3").hide()
        $("#showdata3").show()
        $("#datahide3").hide();
      });
    });


    this.getPermissions();
    this.getuserdetailsByid();
    this.getAppliedList();
    this.getAllhostels();
    this.hst.branchId = 0;
    this.hst.userId = 0;
    this.hst.type = '0'
    this.hst.representativeId = 0;
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename = data.data[0].rolename;
      });
    }
  }
}
