
import { Component, OnInit, ViewChild } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { DatePipe } from '@angular/common';
import { Sports } from '../../modal/Services/sports';
import { SportsDetails } from '../../modal/Services/sportsdetails';
import { Sportskit } from '../../modal/Services/sportskit';
import { ServicesService } from '../../service/services.service';
import { OrganizationService } from '../../service/organization.service';
import { ToastrService } from 'ngx-toastr';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-sports',
  templateUrl: './sports.component.html',
  styleUrls: ['./sports.component.scss']
})
export class SportsComponent implements OnInit,CanActivateChild {
  schoolBranchDetails: any;
  dataSource: MatTableDataSource<{}>;
  displayedColumns = ['sportsid', 'departmentid', 'fromdate', 'starttime', 'endtime', 'edit', 'Excel']
  res: any;
  pipe = new DatePipe('en-US');
  sportsName: Sports
  sportsInfo: SportsDetails
  kitInfo: Sportskit
  sport: any;
  show1:boolean=true;
  minDate = new Date();
  colorTheme = 'theme-dark-blue';
  bsConfig: Partial<BsDatepickerConfig>;
  sporttype: any[];
  department: any;
  kits: any;
  public selectedMoments1 = new Date();
  public selectedMoments2 = new Date();
  maxDate = new Date();
  managedSports: any = [];
  sportsById: any = [];
  branchclassList: any[] = [];
  result = false;
  write: any;
  read: any;
  constructor(private router:Router,private ls: LoginService, private os: OrganizationService, private toastr: ToastrService, private ss: ServicesService) {
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    this.sportsName = new Sports();
    this.sportsInfo = new SportsDetails();
    this.kitInfo = new Sportskit;
  }
  @ViewChild("addsports") form: any;
  @ViewChild("addsportskit") form1: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  addSportsKit() {

    this.ss.addKit(this.kitInfo).subscribe((data) => {
      if (data.result)
        this.toastr.success(data.message)
      else
        this.toastr.error(data.message)
    })
  }
  getAllSportss() {
    this.ss.getAllSports().subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.sport = data.data;
      }
    })
  }
  getAllDepartmentss() {
    this.os.getAllDepartments().subscribe((data) => {
      this.department = data.data;
    })
  }
  getAllkitss() {
    this.ss.getAllkits().subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.kits = data.data;
      }
    })
  }
  getAllBranchClasss() {
    this.os.getAllBranchSession().subscribe((data) => {
      this.branchclassList = data.data;
    })
  }
  allocatedSports() {
    this.show1=!this.show1;
    this.ss.getAllocatedSports().subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.dataSource = new MatTableDataSource(data.data)
        this.dataSource.sort = this.sort
        this.dataSource.paginator = this.paginator
      }
    })
  }
  getexcel() {
    this.ss.exportAsExcelFile(this.dataSource.data, 'Sports');
  }
  applyFilter(filterValue: string) { 
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  editAllocatedSports(id: any) {
    this.ss.editSports(id).subscribe((data) => {
      this.sportsInfo.Id = data.data[0].id;
      this.sportsInfo.sportsId = data.data[0].sportsid;
      this.sportsInfo.departmentId = data.data[0].departmentid;
      this.sportsInfo.sessionId = data.data[0].sessionid;
      this.sportsInfo.fromDate = new Date(data.data[0].fromdate);
      this.sportsInfo.startTime = data.data[0].starttime;
      let startTimeHours = '' + this.sportsInfo.startTime.substring(0, this.sportsInfo.startTime.indexOf(':'))
      let startTimeMinutes = '' + this.sportsInfo.startTime.substring(this.sportsInfo.startTime.indexOf(':') + 1, this.sportsInfo.startTime.lastIndexOf(':'))
      let startTimeSeconds = '' + this.sportsInfo.startTime.substring(this.sportsInfo.startTime.lastIndexOf(':') + 1, this.sportsInfo.startTime.length + 1)
      var startTimeT = new Date().setHours(parseInt(startTimeHours), parseInt(startTimeMinutes), parseInt(startTimeSeconds))
      this.selectedMoments2 = new Date(startTimeT)
      this.sportsInfo.endTime = data.data[0].endtime;
      let endTimeHours = '' + this.sportsInfo.endTime.substring(0, this.sportsInfo.endTime.indexOf(':'))
      let endTimeMinutes = '' + this.sportsInfo.endTime.substring(this.sportsInfo.endTime.indexOf(':') + 1, this.sportsInfo.endTime.lastIndexOf(':'))
      let endTimeSeconds = '' + this.sportsInfo.endTime.substring(this.sportsInfo.endTime.lastIndexOf(':') + 1, this.sportsInfo.endTime.length + 1)
      var endTimeT = new Date().setHours(parseInt(endTimeHours), parseInt(endTimeMinutes), parseInt(endTimeSeconds))
      this.selectedMoments1 = new Date(endTimeT)
      this.sportsInfo.endDate = new Date(data.data[0].todate);
      this.sportsInfo.kitId = data.data[0].kitid;
    })
  }
  updateSportsmanagement(startTime: any, endTime: any) {
    this.sportsInfo.endTime = endTime;
    this.sportsInfo.startTime = startTime;
    this.ss.updateSports(this.sportsInfo)
      .subscribe(data => {
        this.allocatedSports() 
        if (data.result)
          this.toastr.success(data.message);
        else
          this.toastr.error(data.message)
      });
  }
  getSchoolBranchesById() {
    this.os.getBranchById().subscribe((data) => {
      this.schoolBranchDetails = data.data;
    });
  }
  addSportsDetails(startTime: any, endTime: any) {
    this.sportsInfo.endTime = endTime;
    this.sportsInfo.startTime = startTime;
    this.ss.saveSports(this.sportsInfo).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.form.reset();
      }
      else {
        this.toastr.error(data.message)
      }
    });
  }
  getPermissions() {
    this.ls.checkRightPermissions('sports').subscribe((data) => {
  
      this.read = data.data[0].read;
      this.write = data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }
  ngOnInit() {
    this.getPermissions();
    this.allocatedSports();
    this.getAllDepartmentss();
    this.getAllkitss();
    this.getAllBranchClasss();
    this.getAllSportss();
    this.getSchoolBranchesById();
    this.kitInfo.sportsId = 0;
    this.kitInfo.branchId = 0;
    this.sportsInfo.sportsId = 0;
    this.sportsInfo.sessionId = 0;
    this.sportsInfo.departmentId = 0;
    this.sportsInfo.kitId = 0;
  }
}
