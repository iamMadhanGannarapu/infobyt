import { Component, OnInit, ViewChild } from '@angular/core';
import { ServicesService } from 'src/app/service/services.service';
import { GatePass } from 'src/app/modal/Services/Gatepass';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { UserService } from 'src/app/service/user.service';
import { ToastrService } from 'ngx-toastr';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { OrganizationService } from 'src/app/service/organization.service';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-gatepass',
  templateUrl: './gatepass.component.html',
  styleUrls: ['./gatepass.component.scss']
})
export class GatepassComponent implements OnInit, CanActivateChild {
  gateobject: GatePass;
  dataSource;
  branchList: any;
  rolename: any;
  show: boolean = true;
  bsConfig: Partial<BsDatepickerConfig>;
  maxDate = new Date();
  minDate = new Date();
  displayedColumns = ['firstname', 'description', 'intime', 'indate', 'status', 'Approve', 'Excel']
  colorTheme = 'theme-dark-blue';
  read: any;
  write: any;
  constructor(private router: Router, private ls: LoginService, private toastr: ToastrService, private os: OrganizationService, private ss: ServicesService, private us: UserService) {
    this.gateobject = new GatePass()
    this.minDate.setDate(this.minDate.getDate() + 1);
    this.maxDate.setDate(this.maxDate.getDate() + 270);
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  addGatePass() {
    this.ss.saveGatepass(this.gateobject).subscribe((data) => {
      this.getAllGatepass()
      if (data.result)
        this.toastr.success(data.message)
      else
        this.toastr.success(data.message)
    })
  }
  getAllGatepass() {
    this.ss.getAllGatePass().subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.dataSource = new MatTableDataSource(data.data)
        this.dataSource.sort = this.sort
        this.dataSource.paginator = this.paginator
      }
    })
  }
  getexcel() {
    this.ss.exportAsExcelFile(this.dataSource.data, 'sample');
  }
  getSchoolBranchByIds() {
    this.os.getBranchById().subscribe((data) => {
      this.branchList = data.data;
    })
  }
  getId() {
    this.getSchoolBranchByIds();
  }
  getPermissions() {
    this.ls.checkRightPermissions('gatepass').subscribe((data) => {
      this.read = data.data[0].read;
      this.write = data.data[0].write;
      this.canActivateChild(this.read)
    })
  }

  canActivateChild(x: any): boolean {
    if (x == true) {
      return true;
    } else {
      this.router.navigate(['404']);
      return false;
    }
  }

  ngOnInit() {
    $(document).ready(function () {
      $("#datahide").hide();
      $("#showdata").click(function () {
        $("#viewgate").show()
        $("#showdata").hide()
        $("#datahide").show();
      });
      $("#datahide").click(function () {
        $("#viewgate").hide()
        $("#showdata").show()
        $("#datahide").hide();
      });
    });
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename = data.data[0].rolename;
      })
    }
    this.getPermissions();
    this.getId();
    this.getAllGatepass()
    this.gateobject.branchId = 0;
  }
  acceptLeave(Id: any, status: any) {
    this.ss.getGatePassById(Id).subscribe((data) => {
      this.gateobject.Id = data.data[0].id;
      this.gateobject.status = status.value;
      this.ss.updateGatePass(this.gateobject).subscribe((data) => {
        this.ngOnInit();
      })
    })
  }
}
