import { Component, OnInit } from '@angular/core';
import { OrganizationService } from '../service/organization.service'
@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {
  myStyle: object = {};
  myParams: object = {};
  width: number = 100;
  height: number = 100;
  myurl: any;
  pattern1: any;
  pattern2: any;
  constructor(private os: OrganizationService) {
    this.myurl = this.os.url;
    this.pattern1 = this.myurl + '/school/assets/images/pattern/p9.png';
    this.pattern2 = this.myurl + '/school/assets/images/pattern/p5.jpg';
  }
  ngOnInit() {
    this.myStyle = {
      'position': 'absolute',
      'width': '100%',
      'height': '300px',
      'left': 0,
      'background-color': 'black',
    };
    this.myParams = {
      particles: {
        number: {
          value: 500,
        },
        color: {
          value: '#fff'
        },
        shape: {
          type: 'star',
        },
        opacity: {
          value: 0.5,
          random: true,
          anim: {
            enable: true,
            speed: 5
          }
        },
        size: {
          value: 5,
          random: true,
          anim: {
            enable: true,
            speed: 30
          }
        },
        line_linked: {
          enable: true,
          distance: 120,
          color: '#fff',
          width: 1
        },
        move: {
          enable: true,
          speed: 5,
          direction: 'none'
        }
      }, interactivity: {
        events: {
          onhover: {
            enable: true,
            mode: 'repulse'
          },
          onclick: {
            enable: true,
            mode: 'remove'
          }
        },
        modes: {
          repulse: {
            distance: 50,
            duration: 0.4
          },
        }
      }
    };
  }
}
