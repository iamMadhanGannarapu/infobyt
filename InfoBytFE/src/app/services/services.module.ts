import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GatepassComponent } from "./gatepass/gatepass.component";
import { HostelComponent } from "./hostel/hostel.component";
import { HostelfeeComponent } from "./hostelfee/hostelfee.component";
import { LibraryComponent } from "./library/library.component";
import { SportsComponent } from "./sports/sports.component";
import { VisitingComponent } from "./visiting/visiting.component";
import { ServicesRoutingModule } from './services-routing.module';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatStepperModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatSliderModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { MatSlideToggleModule } from '@angular/material';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { TypeaheadModule } from 'ngx-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { AgmCoreModule } from '@agm/core'
import { AgmDirectionModule } from 'agm-direction';
import { TreeModule } from 'primeng/tree';
import { FileUploadModule } from 'ng2-file-upload';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ImageCompressService, ResizeOptions } from 'ng2-image-compress';
import { GalleriaModule } from 'primeng/galleria';
import { AutoCompleteModule } from 'primeng/autocomplete';
@NgModule({
  declarations: [GatepassComponent,HostelComponent,HostelfeeComponent,LibraryComponent,SportsComponent,VisitingComponent],
  imports: [
    CommonModule,
    ServicesRoutingModule,
    MatTableModule, MatTableModule,
    MatSlideToggleModule,
    CdkTableModule,
    CdkTreeModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule, MatFormFieldModule,
    CommonModule,
    //BrowserAnimationsModule, 
    CommonModule, ToastrModule.forRoot(),
    //RouterModule.forRoot(routes), 
    FormsModule, DataTablesModule, TypeaheadModule.forRoot(),
    //BrowserAnimationsModule, 
    MatStepperModule, MatInputModule, MatIconModule, MatButtonModule, MatSlideToggleModule, BsDatepickerModule.forRoot(),
    AgmCoreModule.forRoot({ apiKey: '' }),
    AgmDirectionModule, FileUploadModule, TreeModule, GalleriaModule,AutoCompleteModule
  ],
  providers: [ImageCompressService, ResizeOptions],
  exports: [
   RouterModule
  ]
})
export class ServicesModule { }
