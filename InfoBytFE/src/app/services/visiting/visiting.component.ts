
import { Component, OnInit, ViewChild } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ServicesService } from 'src/app/service/services.service';
import { Visiting } from 'src/app/modal/Services/visiting';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
import { OrganizationService } from 'src/app/service/organization.service';
@Component({
  selector: 'app-visiting',
  templateUrl: './visiting.component.html',
  styleUrls: ['./visiting.component.scss']
})
export class VisitingComponent implements OnInit, CanActivateChild {
   dataSource;
  displayedColumns = ['name', 'out','in']
 visit: Visiting;
  dataSource2
  displayedColumns2 = ['UserName', 'Email']
  userDetailsList:any
  userId:any;
  bsConfig: Partial<BsDatepickerConfig>;
  maxDate = new Date();
  minDate = new Date();
  historyList:any
  colorTheme = 'theme-dark-blue';
  hostelDetails: any
  read: any;
  write: any;
  show1: boolean = true;
  constructor(private router: Router, private ls: LoginService, private ss: ServicesService,private os:OrganizationService, private toastr: ToastrService) {
    this.minDate.setDate(this.minDate.getDate());
    this.maxDate.setFullYear(this.maxDate.getFullYear() + 1);
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    this.visit = new Visiting();
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  addVisiting(){
    this.ss.saveVisiting().subscribe((data)=>{
      

    })
  }
  getOutDetails(){
    this.ss.getVisitingDetailsByUserId().subscribe((data)=>{
      //alert(JSON.stringify(data.data))
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
    })
  }
  getPrevious()
{
  this.ss.getAllHostelVisiting().subscribe((data)=>{
    this.historyList=data.data
    console.log(this.historyList)
  })
}
  getuserdetailsByid() {
    this.os.getuserById().subscribe((data) => {
     this.userDetailsList=data.data
      //alert(JSON.stringify(data.data))
   //  this.userId=this.userDetailsList.userid
      this.dataSource2 = new MatTableDataSource(data.data)
      this.dataSource2.sort = this.sort
      this.dataSource2.paginator = this.paginator
    });
  }
  canActivateChild(x: any): boolean {
    if (x == true) {
      return true;
    } else {
      this.router.navigate(['404']);
      return false;
    }
  }
  UpdateVisiting(id:any){
    this.ss.updtVisit(id).subscribe((data)=>{

    })
  }
  ngOnInit() {
   
    $(document).ready(function(){
      $("#datahide").hide();
      $("#showdata").click(function(){
         $("#visiting").show()
         $("#showdata").hide()
         $("#datahide").show();
      });
      $("#datahide").click(function(){
        $("#visiting").hide()
        $("#showdata").show()
        $("#datahide").hide();
     });
  });

  this.getuserdetailsByid();
  this.getOutDetails();
  this.getPrevious();
  
  }
  
}
