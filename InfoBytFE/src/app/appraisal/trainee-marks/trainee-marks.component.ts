import { Component, OnInit, ViewChild } from '@angular/core';
import { TraineeMarks } from '../../modal/Exam/TraineesMarks';
import { AppraisalService } from '../../service/appraisal.service';
import { UserService } from '../../service/user.service';
import { OrganizationService } from '../../service/organization.service';
import { ToastrService } from 'ngx-toastr';
import { BatchService } from '../../service/batch.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { MessageService } from 'primeng/api';
import { DashboardService } from "src/app/service/dashboard.service";
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-trainee-marks',
  templateUrl: './trainee-marks.component.html',
  styleUrls: ['./trainee-marks.component.scss']
})
export class TraineetMarksComponent implements OnInit, CanActivateChild {
  dataSource;
  data: any;
  displayedColumns = ['StudentName', 'examname', 'name', 'marks', 'edit', 'Excel']
  dataSources;
  displayedColumn = ['examname', 'totalmarks', 'totalgainedmarks', 'Percentage', 'card', 'graph']
  percentage: any
  stdmarks: any;
  userStudentList: any;
  StudentBatch: any;
  batchesList: any;
  show1:boolean=true;
  userProfile: any;
  examsubjectsList: any;
  examsubjectsList1: any;
  examList: any = [];
  sm: TraineeMarks
  show: boolean = false;
  batchId: any
  studentsId: any
  rolename: any;
  totalmarks: any
  showTotal: boolean = false;
  showStudentsMarks: boolean = true
  admissionnum: number
  examName: any;
  exammarks: any;
  graphExamname: any[] = []
  graphexammarks: any[] = []
  exam: any
  subjectList: any
  studentReportCardList: any
  showGraph: boolean = false;
  TotalMarksClassList: any;
  marks: any
  da: string = "";
  read: any;
  write: any;
  constructor(private router: Router, private ls: LoginService, private toastr: ToastrService, private ds: DashboardService, private bs: BatchService, private sr: AppraisalService, private us: UserService, private os: OrganizationService, private messageService: MessageService) {
    this.sm = new TraineeMarks()
  } @ViewChild("schbrn") form1: any;
  @ViewChild("stdmark") form: any;
  @ViewChild("addschoolbranch") form3: any;
  @ViewChild("frm") form2: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  saveMarks(studentid: any, marks1: any) {
    this.sm.traineeId = studentid
    this.sm.marks = marks1.value;
    this.sr.saveMarks(this.sm).subscribe((data) => {
      this.studentsList();
      if (data.result) {
        this.studentsId = data[0].fn_addstudentmarks;
        this.toastr.success(data.message);
      }
      else {
        this.toastr.error(data.message);
      }
      this.ds.addMarksNotification(this.studentsId).subscribe((data) => {
      }, (err) => {
        this.toastr.error(err.error.message)
      });
    }, (err) => {
      this.toastr.error(err.error.message)
    });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  GetAllStudentMarks() {
    this.sr.getAllMarks().subscribe((data) => {
      this.showTotal = false
      this.showStudentsMarks = false
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
    }, (err) => {
      this.toastr.error(err.error.message)
    });
  }
  getPermissions() {
    this.ls.checkRightPermissions('trainee-marks').subscribe((data) => {
      this.read = data.data[0].read;
      this.write = data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  canActivateChild(x: any): boolean {
    if (x == true) {
      return true;
    } else {
      this.router.navigate(['404']);
      return false;
    }
  }
  getexcel() {
    this.sr.exportAsExcelFile(this.dataSource, 'sample');
  }
  getGraph() {
    for (let i in this.totalmarks) {
      this.exammarks = this.totalmarks[i].totalgainedmarks
      this.graphexammarks.push(this.exammarks);
    }
    for (let i in this.totalmarks) {
      this.examName = this.totalmarks[i].appraisalname
      this.graphExamname.push(this.examName);
    }
    this.data = {
      labels: this.graphExamname,
      datasets: [
        {
          label: 'TotalMarks:',
          data: this.graphexammarks,
          fill: false,
          borderColor: '#1E88E5'
        }
      ]
    }
    this.showGraph = true
  }
  selectData(event) {
    this.messageService.add({ severity: 'info', summary: 'Data Selected', 'detail': this.data.datasets[event.element._datasetIndex].data[event.element._index] });
  }
  ngOnInit() {
    $(document).ready(function(){
      $("#datahide").hide();
      $("#showdata").click(function(){
         $("#responsive").show()
         $("#showdata").hide()
         $("#datahide").show();
      });
      $("#datahide").click(function(){
        $("#responsive").hide()
        $("#showdata").show()
        $("#datahide").hide();
     });
  });
    this.GetAllStudentMarks();
    this.getIds();
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.userProfile = data[0];
        this.rolename = this.userProfile.rolename;
      }, (err) => {
        this.toastr.error(err.error.message)
      });
    }
  }
  marksDetails(n: number) {
    this.show = false
    this.sr.getMarksById(n).subscribe((data) => {
      this.stdmarks = data
    }, (err) => {
      this.toastr.error(err.error.message)
    });
  }
  editMarks(id: number) {
    this.getIds()
    this.show = false;
    this.sr.getMarksById(id).subscribe((data) => {
      this.stdmarks = data;
      this.sm.Id = data[0].id
      this.sm.traineeId = data[0].studentid
      this.sm.examId = data[0].examid
      this.examsubjects(this.sm.examId);
      this.sm.marks = data[0].marks;
      this.sm.subjectId = data[0].subjectid;
    }, (err) => {
      this.toastr.error(err.error.message)
    });
  }
  updateStudentMarks() {
    this.sr.updateMarks(this.sm).subscribe((data) => {
      if (data.result)
        this.toastr.success(data.message);
      else
        this.toastr.error(data.message);
      this.GetAllStudentMarks();
    }, (err) => {
      this.toastr.error(err.error.message)
    });
  }
  studentsList() {
    this.sr.getTrainees(this.batchId).subscribe((data) => {
      this.userStudentList = data.data;
    }, (err) => {
      this.toastr.error(err.error.message)
    });
    this.getExamTimeTable()
  }
  examsubjects(n: number) {
    this.show = true;
    this.sr.getAppraisalSubjects(n).subscribe((data) => {
      this.examsubjectsList = data.data;
    }, (err) => {
      this.toastr.error(err.error.message)
    });
  }
  subjectNames(n: number) {
    this.sr.getAppraisalSubjectsById(n).subscribe((data) => {
      this.examsubjectsList1 = data.data;
    }, (err) => {
      this.toastr.error(err.error.message)
    });
  }
  getId() {
    this.getAllAdmissions()
    this.getExamTimeTable()
    this.showTotal = true
    this.showStudentsMarks = false
  }
  getTotal() {
    this.sr.getMarksByAdmissionnum(this.admissionnum, this.exam).subscribe((data) => {
      this.totalmarks = data.data;
      this.percentage = ((data.data[0].totalgainedmarks / data.data[0].totalmarks) * 100)
      this.showTotal = true
      this.dataSources = new MatTableDataSource(data.data)
      this.dataSources.sort = this.sort
      this.dataSources.paginator = this.paginator
    })
    this.showGraph = false
  }
  getIdForList() {
  
    this.showTotal = false
    this.showStudentsMarks = true
  }
  getClassMarksList() {
    this.sr.getSessionMarks(this.batchId, this.exam).subscribe((data) => {
      this.TotalMarksClassList = data.data
      let props: any = (Object.keys(data.data[0]))
      this.da += "<table class='table table-bordered'><tr>"
      for (let z in props) {
        this.da += "<th>" + props[z] + "</th>"
      }
      this.da += "</tr>";
      this.showStudentsMarks = true
      for (let y in this.TotalMarksClassList) {
        this.da += "<tr>";
        for (let x in props) {
          this.da += "<td>" + this.TotalMarksClassList[y][props[x]] + "</td>"
        }
        this.da += "</tr>"
      }
      this.da += "</table>"
    })
  }
  getReportCard(id: number) {
    this.sr.getReportByTrainee(id, this.exam).subscribe((data) => {
      this.studentReportCardList = data.data
    })
    {
    }
  }
  getAllAdmissions() {
    this.os.getAllAdmission().subscribe((data) => {
      this.userStudentList = data.data;
    }, (err) => {
      this.toastr.error(err.error.message)
    });
  }
  getStudentBatches() {
    this.bs.getTraineeBatch().subscribe((data) => {
      this.StudentBatch = data;
    }, (err) => {
      this.toastr.error(err.error.message)
    });
  }
  getAllBatches() {
    this.bs.getAllBatch().subscribe((data) => {
      this.batchesList = data.data;
    }, (err) => {
      this.toastr.error(err.error.message)
    });
  }
  getExamTimeTable() {
    this.sr.getAppraisalTimetableByBatchId(this.batchId).subscribe((data) => {
      this.examList = data.data;
    });
    this.getSubjects();
  }
  getSubjects() {
    this.sr.getSubjectnamesByBatch(this.batchId, this.exam).subscribe((data) => {
      this.subjectList = data.data
    });
  }
  getIds() {
    this.getPermissions();
    this.getAllAdmissions();
    this.getStudentBatches();
    this.getAllBatches();
    this.getExamTimeTable();
    this.getSubjects();
  }
}
