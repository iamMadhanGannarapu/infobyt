import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppraisalTimetableComponent } from './appraisal-timetable/appraisal-timetable.component';
import { TraineetMarksComponent } from './trainee-marks/trainee-marks.component';
import { RouteGuardGuard, CanDeactivateGuard } from '../service/route-guard.guard';
const routes: Routes = [
    { path: 'Appraisal-timetable', component: AppraisalTimetableComponent ,canActivate:[RouteGuardGuard],canActivateChild:[AppraisalTimetableComponent],canDeactivate:[CanDeactivateGuard]},
    { path: 'Trainee-marks', component: TraineetMarksComponent ,canActivate:[RouteGuardGuard],canActivateChild:[TraineetMarksComponent],canDeactivate:[CanDeactivateGuard]},
  ]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers:[RouteGuardGuard,AppraisalTimetableComponent,TraineetMarksComponent,CanDeactivateGuard]
})
export class ExamRoutingModule { }
