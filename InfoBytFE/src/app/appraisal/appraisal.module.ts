import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppraisalTimetableComponent } from './appraisal-timetable/appraisal-timetable.component';
import { TraineetMarksComponent } from './trainee-marks/trainee-marks.component';
import { RouterModule, Routes } from "@angular/router";
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { BsDatepickerModule } from 'ngx-bootstrap';
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
//import{BrowserModule} from '@angular/platform-browser'
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
//import { HttpClientModule } from '@angular/common/http';
import {ChartModule} from 'primeng/chart';
import { ToastModule } from 'primeng/toast';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';   
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatStepperModule,
  MatProgressBarModule ,
  MatProgressSpinnerModule,
  MatSliderModule ,
  MatSlideToggleModule ,
  MatTooltipModule ,
  MatTreeModule,
} from '@angular/material';
import { ExamRoutingModule } from './appraisal-routing.module';
//import { RouteGuardGuard } from '../service/route-guard.guard';
@NgModule({
  imports: [
    ExamRoutingModule,
    MatTableModule, MatTableModule,
    //BrowserModule,HttpClientModule,
    MatSlideToggleModule,
    CdkTableModule,
    CdkTreeModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    OwlNativeDateTimeModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    OwlDateTimeModule,
    MatTooltipModule,
    ChartModule,
    MatTreeModule,MatFormFieldModule,ToastModule,
    CommonModule,
    //BrowserAnimationsModule, 
    ToastrModule.forRoot(),
    FormsModule, DataTablesModule, BsDatepickerModule.forRoot()
  ], exports: [
    RouterModule
  ],providers:[],
  declarations: [AppraisalTimetableComponent, TraineetMarksComponent]
})
export class AppraisalModule { }
