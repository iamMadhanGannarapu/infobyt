import { Component, OnInit, ViewChild } from '@angular/core';
import { AppraisalService } from '../../service/appraisal.service';
import { AppraisalTimetable } from '../../modal/Exam/AppraisalTimetable';
import { BatchService } from '../../service/batch.service';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../service/user.service';
import { LoginService } from '../../service/login.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { DatePipe } from '@angular/common';
import { DashboardService } from "src/app/service/dashboard.service";
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-appraisal-timetable',
  templateUrl: './appraisal-timetable.component.html',
  styleUrls: ['./appraisal-timetable.component.scss']
})
export class AppraisalTimetableComponent implements OnInit, CanActivateChild {
  dataSource;
  displayedColumns = ['SubjectName', 'startTime', 'EndTime', 'ExamName', 'Download', 'Excel']
  Show: boolean = true;
  exam: AppraisalTimetable;
  getexam: any;
  examId: number
  uniqId: any;
  sub: any;
  examTimeTableId: any;
  subjectList: any
  data: any;
  subbyid: any
  millisec: any;
  maxDate = new Date();
  minDate = new Date();
  i: number;
  classList: any
  colorTheme = 'theme-dark-blue';
  branchClassId: any;
  pipe = new DatePipe('en-US');
  myurl: string
  bsConfig: Partial<BsDatepickerConfig>;
  rolename: any;
  image = { 'URL': '', 'valid': false };
  read: any;
  write: any;
  constructor(private router: Router, private ls: LoginService, private ds: DashboardService, private toastr: ToastrService, private es: AppraisalService, private us: UserService, private bs: BatchService) {
    this.minDate.setDate(this.minDate.getDate() + 1);
    this.exam = new AppraisalTimetable();
    this.myurl = ls.url
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
  }
  @ViewChild("ett") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  ngOnInit() {
    $(document).ready(function(){
      $("#datahide").hide();
      $("#showdata").click(function(){
         $("#viewallExamTimeTable").show()
         $("#showdata").hide()
         $("#datahide").show();
      });
      $("#datahide").click(function(){
        $("#viewallExamTimeTable").hide()
        $("#showdata").show()
        $("#datahide").hide();
     });
  });


    this.getPermissions();
    this.getExamDetails();
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename = data.data[0].rolename;
      });
    }
  }
  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getExamDetails() {
    this.es.getAppraisalTimeTables().subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.dataSource = new MatTableDataSource(data.data)
        this.dataSource.sort = this.sort
        this.dataSource.paginator = this.paginator
      }
    });
  }
  getexcel() {
    this.es.exportAsExcelFile(this.dataSource, 'Appraisal');
  }
  fileChangeEvent(event) {
    var fileName = event.target.value;
    var idxDot = fileName.lastIndexOf(".") + 1;
    var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
    if (extFile == "pdf") {
      if (event.target.files && event.target.files[0]) {
        var rd = new FileReader();
        rd.readAsText(event.target.files[0]);
        rd.onload = (ev: any) => {
          this.data = ev.target.result;
          this.exam.fileUpload = rd.result
          this.image.URL = ev.target.result;
        }
        rd.readAsText(this.exam.fileUpload)
      }
    } else {
      alert("Only pdf files are allowed!");

    }
  }
  addExamTimeTable(subjectId: any, etime: any, tmarks: any) {
    this.exam.examStartDateTime = new Date(etime._selecteds[0]);
    this.exam.examEndDateTime = new Date(etime._selecteds[1]);
    this.exam.totalMarks = tmarks.value;
    this.exam.sessionSubjectId = subjectId;
    this.exam.examId = this.uniqId;
    this.es.saveTimeTable(this.exam).subscribe((data) => {
      if (data.message) {
        this.examTimeTableId = data.data[0].fn_appraisalexamtimetable;
        this.toastr.success(data.message);
        this.ds.addAppraisalTimeTableNotification(this.examTimeTableId).subscribe((data) => {
        });
      }
      else {
        this.toastr.error(data.message);
      }
      this.getExamDetails();
    })
  }
  getIds() {
    this.bs.getAllBatch().subscribe((res) => {
      this.classList = res.data;
    });
  }
  examDetails(n: number) {
    this.Show = false;
    this.es.getAppraisalTimetableById(n).subscribe((res) => {
      if (res.result && res.data != 'NDF')
        this.subbyid = res;
    });
  }
  editExamTimeTable(id: number) {
    this.getIds();
    this.es.getAppraisalTimetableById(id).subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.exam.Id = data.data[0].id;
        this.exam.sessionSubjectId = data.data[0].sessionSubjectid;
        this.exam.examStartDateTime = data.data[0].starttime;
        this.exam.examEndDateTime = data.data[0].endtime;
        this.exam.name = data.data[0].examname;
        this.exam.status = data.data[0].status;
      }
    }
    );
  }
  updateExamTimeTable(etime: any, stime: any) {
    this.exam.examEndDateTime = etime.value;
    this.exam.examStartDateTime = stime.value;
    this.es.updateTimetable(this.exam).subscribe((data) => {
      if (data.result)
        this.toastr.success(data.message);
      else
        this.toastr.error(data.message);
      this.getExamDetails();
    });
  }
  getSubjectsByClass() {

    this.bs.getSessionSubjectByBranch(this.branchClassId).subscribe((data) => {
      this.subjectList = data.data;
    });
  }
  getUserSession() {
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.millisec = this.maxDate.getMilliseconds();
        this.uniqId = this.millisec + "" + data.data[0].userid;
      })
    }
  }
  getPermissions() {
    this.ls.checkRightPermissions('appraisal-timetable').subscribe((data) => {
      this.read = data.data[0].read;
      this.write = data.data[0].write;
      this.canActivateChild(this.read)
    })
  }

  canActivateChild(x: any): boolean {
    if (x == true) {
      return true;
    } else {
      this.router.navigate(['404']);
      return false;
    }
  }


}
