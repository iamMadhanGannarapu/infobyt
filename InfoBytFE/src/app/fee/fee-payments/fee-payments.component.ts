import { Component, OnInit, ViewChild } from '@angular/core';
import { FeePayments } from '../../modal/Fee/feepayments';
import { FeeService } from '../../service/fee.service';
import { UserService } from '../../service/user.service';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { OrganizationService } from '../../service/organization.service';
import { BatchService } from '../../service/batch.service';
import {MatSort, MatTableDataSource,MatPaginator} from '@angular/material';
import { DashboardService } from 'src/app/service/dashboard.service';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-fee-payments',
  templateUrl: './fee-payments.component.html',
  styleUrls: ['./fee-payments.component.scss']
})
export class FeePaymentsComponent implements OnInit,CanActivateChild {

  dataSource;
  displayedColumns=['StudentName','feeamount','PaidAmount','Due','receipt','Excel']
feereciptListByID:any
  reportSource;
  reportColumns=['feeamount','concession','PaidAmount','due']
  reportSources;
  reportColumn=['feeamount','concession','PaidAmount','due']
  showReports: boolean = true;
  showReportsByClass: boolean = true;
  feepayments: FeePayments
  studentDetails: any;
  userProfile: any;
  maxDate = new Date();
  minDate = new Date();
  FeeconcessionList: any;
  DueList: any
  payable: any
  batchList: any
  batchId: any
  classList: any
  branchclass:any;
  colorTheme = 'theme-dark-blue';
  feepaymentId:any
  bsConfig: Partial<BsDatepickerConfig>;
  read: any;
  write: any;
  constructor( private router:Router,  private ls:LoginService,private toastr: ToastrService,private ds:DashboardService , private bs: BatchService, private fs: FeeService, private us: UserService, private os: OrganizationService) {
    this.feepayments = new FeePayments()
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
  }
  @ViewChild("feepaymentsForm") form: any;
  @ViewChild(MatSort) sort:MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  ngOnInit() {
    this.getPermissions();
    this.getAllBranchClasses();
    this.getAllFeePayments();
    this.getAllFinancialReports();
    this.bs.getAllBatch().subscribe((res) => {
      this.batchList = res;
    });
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.userProfile = data.data[0];
      });
    }
    this.feepayments.traineeId=0;
  }
  getPermissions() {
    this.ls.checkRightPermissions('fee-payments').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  canActivateChild(x:any):boolean{
    if(x==true){
      return true;
    }else{
      this.router.navigate(['404']);
        return false;
    }
      }
  applyFilter(filterValue: string) {
 
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getAllFeePayments() {
    this.fs.getAllFeepayments().subscribe((data) => {
    });
    this.showReports=false
  }
  getFeeConcessionDues() {
    this.os.getAdmissionByUserId(this.feepayments.traineeId).subscribe((data) => {
      this.FeeconcessionList = data.data;
      this.calulatePayable();
    });
    this.fs.getFeepaymentByUserId(this.feepayments.traineeId).subscribe((data) => {
      this.DueList = data.data;
      this.calulatePayable();
    });
  }
  getBatch() {
    this.fs.getFeepaymentByBatchId(this.batchId).subscribe((data) => {
    
      this.dataSource=new MatTableDataSource(data.data)
      this.dataSource.sort=this.sort
      this.dataSource.paginator=this.paginator
    });
  }
  getexcel(){
    this.fs.exportAsExcelFile(this.dataSource, 'sample');
}
  calulatePayable() {
    this.payable = this.FeeconcessionList[0].feeamount - this.FeeconcessionList[0].concession - this.DueList[0].paidamnt
  }
  calculateDue() {
    this.feepayments.due = this.payable - this.feepayments.paidAmount
  }
  getIds() {
    this.fs.getAllTraineenameForAccountant().subscribe((data) => {
      this.studentDetails = data.data;
    });
  }
  addFeePayments() {
    this.fs.saveFeepayments(this.feepayments).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
         this.feepaymentId=data[0].fn_AddFeePayments
      this.form.reset();
      }
      else
      {
        this.toastr.error(data.message);
      }
      this.ds.addFeepaymentsNotification(this.feepaymentId).subscribe((data)=>{
      })
      this.getAllFeePayments();
    });
    this.getAllFinancialReports();
  }
  getAllFinancialReports(){
    this.fs.getAllFinancials().subscribe((data)=>{
      this.reportSource=new MatTableDataSource(data.data)
      this.reportSource.sort=this.sort
      this.reportSource.paginator=this.paginator
    })
    this.showReports=true
  }
  getAllFinancialReportsbybrnclss(){
    this.fs.getAllFinancialreportbybranchSession(this.branchclass).subscribe((data)=>{
      this.reportSources=new MatTableDataSource(data.data)
      this.reportSources.sort=this.sort
      this.reportSources.paginator=this.paginator
    })
  }
getAllBranchClasses(){
  this.os.getAllBranchSession().subscribe((data) => {
    this.classList = data.data;
  });
}
getAllReceiptById(id:number){
  this.fs.getFeepaymentById(id).subscribe((data)=>{
    this.feereciptListByID=data.data
  })
  this.showReports=true
}
}