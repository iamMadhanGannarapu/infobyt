
import { Component, OnInit, ViewChild } from '@angular/core';
import { LoginService } from 'src/app/service/login.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/service/user.service';
import { FeeService } from 'src/app/service/fee.service';
import { FeeStructure } from 'src/app/modal/Fee/feestructure';
import { MatSort,  MatPaginator } from '@angular/material';
import { BatchService } from 'src/app/service/batch.service';
import { OrganizationService } from 'src/app/service/organization.service';
import { GenPattern } from '../../modal/Fee/genpattern'
import { PatternEmi } from '../../modal/Fee/emi'
import { Router, CanActivateChild } from '@angular/router';
class Fee {
  public FeePattern: string;
  public FeeName: string;
  public Amount: string;
  public Optional: string;
  public EMI: string;
  public classId: string;
  constructor() {
  }
}
@Component({
  selector: 'app-feestructure',
  templateUrl: './feestructure.component.html',
  styleUrls: ['./feestructure.component.scss']
})
export class FeestructureComponent implements OnInit,CanActivateChild {
  millisec: any;
  maxDate = new Date();
  getlocaldata: any;
  feestructureList: any;
  feestructurepatternList: any;
  
  classid: any;
  classList: any;
  rolename: any;
  
  feepatternObj: any;
  
  genPatternmodel: any
  dataSource;
  emi: any;
  FeeStructureObj: Fee
  FeeLocalList: Fee[] = []
  
  read: any;
  write: any;
  constructor(private router:Router,  private ls: LoginService, private toastr: ToastrService, private os: OrganizationService, private us: UserService, private fs: FeeService) {
    this.feepatternObj = new FeeStructure();
    this.emi = new PatternEmi();
    this.genPatternmodel = new GenPattern();
  }
  @ViewChild("addfeepattern") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  ngOnInit() {
    this.getPermissions();
    this.getAllBranchClasses();
    this.getFeeStructures();
    this.getFeeStructuresPattern();
    this.feepatternObj.optional = false
    this.feepatternObj.emi = false
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename =data.data[0].rolename;
      }, (err) => {
        this.toastr.error(err.error.message)
      });
    };
  }
  getPermissions() {
    this.ls.checkRightPermissions('feestructure').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  canActivateChild(x:any):boolean{
    if(x==true){
      return true;
    }else{
      this.router.navigate(['404']);
        return false;
    }
      }
  addLocalstorage() {
    this.FeeStructureObj = new Fee();
    this.FeeStructureObj.FeePattern = this.feepatternObj.feePattern;
    this.FeeStructureObj.FeeName = this.feepatternObj.feeName;
    this.FeeStructureObj.Amount = this.feepatternObj.amount;
    this.FeeStructureObj.Optional = this.feepatternObj.optional;
    this.FeeStructureObj.EMI = this.feepatternObj.emi;
    this.FeeStructureObj.classId = this.feepatternObj.sessionId;
    this.FeeLocalList.push(this.FeeStructureObj)
    localStorage.setItem('data', JSON.stringify(this.FeeLocalList));
  }
  addfee(classId) {
    this.getlocaldata = localStorage.getItem('data')
    this.fs.saveFeeStructure(this.getlocaldata, classId.value).subscribe((data) => {
      this.getFeeStructures();
      this.getFeeStructuresPattern();

      if (data.result && data.data!="NDF") {
        this.toastr.success(data.message)
      }
      else
      this.toastr.error(data.message);
    })
  }
  getFeeStructures() {
    this.fs.getFeeStructure().subscribe((data) => {
      if(data.result && data.data!='NDF')
      this.feestructureList = data.data;
    });
  }
  getFeeStructuresPattern() {
    this.fs.getFeeStructurePattern().subscribe((data) => {
      if(data.result && data.data!='NDF')
      this.feestructurepatternList = data.data;
    });
  }
  getAllBranchClasses() {
    this.os.getAllBranchSession().subscribe((data) => {
      this.classList = data.data;
    });
  }
  getAllBranchClassById(i) {
    this.os.getAllBranchSessionById(i.value).subscribe((data) => {
      this.classList = data.data;
    });
  }
  applyFilter(filterValue: string) {
    
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getUserSession() {
    let session = sessionStorage.getItem('user')
    this.ls.getUserBySessionId().subscribe((data) => {
      if (data.data) {
        this.millisec = this.maxDate.getMilliseconds();
        this.feepatternObj.feePattern = this.maxDate.getMinutes() + '' + this.maxDate.getMonth() + this.millisec + data[0].fn_viewuserid + "_FP";
      }
    })
  }
}