import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FeePaymentsComponent } from './fee-payments/fee-payments.component';
import { ConcessionComponent } from './concession/concession.component';
import { FeesComponent } from './fees/fees.component';
import { FeestructureComponent } from './feestructure/feestructure.component';
import { RouteGuardGuard, CanDeactivateGuard } from '../service/route-guard.guard';
const routes:Routes=[
    {path:'Concession',component:ConcessionComponent, canDeactivate:[CanDeactivateGuard],canActivate:[RouteGuardGuard],canActivateChild:[ConcessionComponent]},
    {path:'Fee-payments',component:FeePaymentsComponent, canDeactivate:[CanDeactivateGuard],canActivate:[RouteGuardGuard],canActivateChild:[FeePaymentsComponent]},
    {path:'Fees',component:FeesComponent, canDeactivate:[CanDeactivateGuard],canActivate:[RouteGuardGuard],canActivateChild:[FeesComponent]},
    {path:'Feestructure',component:FeestructureComponent, canDeactivate:[CanDeactivateGuard],canActivate:[RouteGuardGuard],canActivateChild:[FeestructureComponent]}
    ]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers:[RouteGuardGuard,CanDeactivateGuard]
})
export class FeeRoutingModule { }
