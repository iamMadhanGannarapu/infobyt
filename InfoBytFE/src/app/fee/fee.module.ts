import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConcessionComponent } from './concession/concession.component';
import { FeesComponent } from './fees/fees.component';
import { FeePaymentsComponent } from './fee-payments/fee-payments.component';
import { RouterModule,Routes, Router } from "@angular/router";
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { BsDatepickerModule } from 'ngx-bootstrap';
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccordionModule } from 'ngx-bootstrap';
import { ToastrModule } from 'ngx-toastr';
//import{BrowserModule} from '@angular/platform-browser'
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
//import { HttpClientModule } from '@angular/common/http';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatStepperModule,
  MatProgressBarModule ,
  MatProgressSpinnerModule,
  MatSliderModule ,
  MatSlideToggleModule ,
  MatTooltipModule ,
  MatTreeModule,
} from '@angular/material';
import { FeestructureComponent } from './feestructure/feestructure.component';
import { FeeRoutingModule } from './fee-routing.module';
//import { RouteGuardGuard } from '../service/route-guard.guard';
@NgModule({
  imports: [ 
    FeeRoutingModule,
    MatTableModule, MatTableModule,
    //BrowserModule,HttpClientModule,
    MatSlideToggleModule,
    CdkTableModule,
    CdkTreeModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,MatFormFieldModule,
    CommonModule,
    //BrowserAnimationsModule,
    ToastrModule.forRoot(),
    FormsModule,DataTablesModule,BsDatepickerModule.forRoot(),AccordionModule.forRoot(),
  ],
  exports:[
    RouterModule
  ],providers:[],
  declarations: [ConcessionComponent, FeesComponent, FeePaymentsComponent, FeestructureComponent]
})
export class FeeModule { }
