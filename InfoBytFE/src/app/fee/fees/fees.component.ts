import { Component, OnInit, ViewChild } from '@angular/core';
import { Fee } from '../../modal/Fee/fee';
import { FeeService } from '../../service/fee.service';
import { OrganizationService } from '../../service/organization.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../service/user.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-fees',
  templateUrl: './fees.component.html',
  styleUrls: ['./fees.component.scss']
})
export class FeesComponent implements OnInit,CanActivateChild {
  dataSource;
  displayedColumns = ['FeeName', 'BranchName', 'Classname', 'FeeAmount', 'edit','Excel']
  feemaster: Fee;
  branchList: any
  classList: any;
  feestructurepatternList: any;
  feestructurepatternList1: any;
  rolename: any;
  read: any;
  write: any;
  constructor(private router:Router, private ls:LoginService,private toastr: ToastrService, private us: UserService, private fs: FeeService, private os: OrganizationService) {
    this.feemaster = new Fee()
  }
  @ViewChild("feemasterform") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  getAllFees() {
    this.fs.getAllFee().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
    
      this.dataSource = new MatTableDataSource( data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      }
    });
  }
  getexcel(){
      this.fs.exportAsExcelFile(this.dataSource, 'Fee Details');
}
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  ngOnInit() {
    this.getFeeStructuresPattern()
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename = data.data[0].rolename;
      });
    }
    this.getAllFees();
    this.getPermissions();
    this.feemaster.branchSessionId = 0;
    this.feemaster.branchId = 0;
    this.feemaster.feeName='0';
  }
  getPermissions() {
    this.ls.checkRightPermissions('fees').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
    })
  }
  canActivateChild(x:any):boolean{
    if(x==true){
      return true;
    }else{
      this.router.navigate(['404']);
        return false;
    }
      }
  editFees(id: number) {
    this.getIds();
    this.fs.getFeeById(id).subscribe((data) => {
      if(data.result && data.data!='NDF')
      {      
      this.feemaster.Id = data.data[0].id;
      this.feemaster.feeName = data.data[0].feename;
      this.feemaster.branchId = data.data[0].branchid;
      this.feemaster.branchSessionId = data.data[0].branchclassid;
      this.feemaster.feeAmount = data.data[0].feeamount;
      }
    });
  }
  viewSchoolBranchDetails() {
    this.os.getBranchById().subscribe((data) => {
      this.branchList = data.data;
    });
  }
  viewAllBranchClass() {
    this.os.getAllBranchSession().subscribe((data) => {
      this.classList = data.data;
    });
  }
  getIds() {
    this.viewSchoolBranchDetails();
    this.viewAllBranchClass();
  }
  updateFees(amount: any) {
    this.fs.updateFee(this.feemaster).subscribe((data) => {
      if (data.result) 
        this.toastr.success(data.message);
      else 
        this.toastr.error(data.message);
        this.getAllFees();
    });
  }
  getFeeStructuresPattern() {
    this.fs.getFeeStructurePattern().subscribe((data) => {
      this.feestructurepatternList = data.data;
    });
  }
  getFeePatternById(id: any) {
    this.fs.getFeeStructurePatternById(id.value).subscribe((data) => {
      this.feestructurepatternList1 = data.data;
    });
  }
  addFees() {
    this.fs.saveFee(this.feemaster).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.form.reset();
      }
      else
      {
        this.toastr.error(data.message);
      }
      this.getAllFees();
    });
  }
}
