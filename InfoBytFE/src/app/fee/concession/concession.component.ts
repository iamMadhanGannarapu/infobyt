import { Component, OnInit, ViewChild } from '@angular/core';
import { Concession } from '../../modal/Fee/concession';
import { FeeService } from '../../service/fee.service';
import { OrganizationService } from '../../service/organization.service';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../service/user.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { DatePipe } from '@angular/common';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-concession',
  templateUrl: './concession.component.html',
  styleUrls: ['./concession.component.scss']
})
export class ConcessionComponent implements OnInit,CanActivateChild {
  dataSource;
  displayedColumns = ['BranchName', 'concessionname', 'details', 'concessionfee', 'creationdate', 'edit', 'Excel']
  concessionnn: Concession;
  schoolBranchList: any
  feestructurepatternList1: any
  feestructurepatternList: any;
  bsConfig: Partial<BsDatepickerConfig>;
  maxDate = new Date();
  minDate = new Date();
  pipe = new DatePipe('en-US');
  color = "success";
  colorTheme = 'theme-dark-blue';
  rolename: any;
  read: any;
  write: any;
  constructor(private router:Router,  private ls: LoginService, private toastr: ToastrService, private fs: FeeService, private os: OrganizationService, private us: UserService) {
    this.minDate.setDate(this.minDate.getDate());
    this.maxDate.setFullYear(this.maxDate.getFullYear() + 1);
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    this.concessionnn = new Concession()
  }
  @ViewChild("conc") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  getAllConcession() {
    this.fs.getAllConcession().subscribe((data) => {
   
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
    })
  }
  getexcel() {
    this.fs.exportAsExcelFile(this.dataSource, 'Concession');
  }
  getPermissions() {
    this.ls.checkRightPermissions('concession').subscribe((data) => {
      this.read =  data.data[0].read;
      this.write =  data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  canActivateChild(x:any):boolean{
    if(x==true){
      return true;
    }else{
      this.router.navigate(['404']);
        return false;
    }
      }
  ngOnInit() {
    this.getPermissions();
    this.getFeeStructuresPattern();
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename = data.data[0].rolename;
      }, (err) => {
        this.toastr.error(err.error.message)
      });
    }
    this.getAllConcession();
    this.concessionnn.branchId = 0;
  }
  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getBranchIds() {
    this.os.getBranchById().subscribe((data) => {
      this.schoolBranchList = data.data;
    }, (err) => {
      this.toastr.error(err.error.message)
    });
  }
  editConcession(id: number) {
    this.getBranchIds();
    this.fs.getConcessionById(id).subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.getFeeStructuresPattern();
        this.concessionnn.Id = data.data[0].id;
        this.concessionnn.branchId = data.data[0].branchid;
        this.concessionnn.name = data.data[0].concessionname;
        this.concessionnn.details = data.data[0].details;
        this.concessionnn.concession = data.data[0].concession;
        this.concessionnn.date = this.pipe.transform(data.data[0].creationdate, 'MM-dd-yyyy');
      }
    });
  }
  updateConcession() {
    this.fs.updateConcession(this.concessionnn).subscribe((data) => {
      if (data.result)
        this.toastr.success(data.message);
      else
        this.toastr.error(data.message);
      this.getAllConcession();
    });
  }
  addConcession(concessionname: any) {
    this.fs.getFeeStructurePatternById(concessionname.value).subscribe((data) => {
      this.feestructurepatternList1 = data.data;
      this.concessionnn.amount = data.data[0].amount
      this.fs.saveConcession(this.concessionnn).subscribe((data) => {
        if (data.result) {
          this.toastr.success(data.message);
          this.form.reset();
        }
        else
          this.toastr.error(data.message);
        this.getAllConcession();
      });
    });
  }
  getFeePatternById(id: any) {
    this.fs.getFeeStructurePatternById(id.value).subscribe((data) => {
      this.feestructurepatternList1 = data.data;
    });
  }
  getFeeStructuresPattern() {
    this.fs.getFeeStructurePattern().subscribe((data) => {
      this.feestructurepatternList = data.data;
    });
  }
}