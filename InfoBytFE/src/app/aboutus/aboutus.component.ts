import { Component, OnInit } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { OrganizationService } from '../service/organization.service';
@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.scss'],
  providers: [MessageService],
  styles: [`
      .company.ui-organizationchart .ui-organizationchart-node-content.ui-person {
          padding: 0;
          border: 0 none;
      }
      .node-header,.node-content {
          padding: .5em .7em;
      }
      .node-header {
          background-color: #495ebb;
          color: #ffffff;
      }
      .node-content {
          text-align: center;
          border: 1px solid #495ebb;
          font-size:12px;
      }
      .node-content img {
          border-radius: 50%;
          width:60px;
          height:60px;
      }
      .node-content img:hover {
          width:80px;
          height:80px;
      }
      .department-cfo {
          background-color: #7247bc;
          color: #ffffff;
      }
      .department-coo {
          background-color: #a534b6;
          color: #ffffff;
      }
      .department-cto {
          background-color: #e9286f;
          color: #ffffff;
      }
      .ui-person .ui-node-toggler {
          color: #495ebb !important;
      }
      .department-cto .ui-node-toggler {
          color: #8a0a39 !important;
      }
      .grey-image{
          -webkit-filter: grayscale(90%);
          filter: grayscale(90%);
          width: 100%;
          margin: auto;
      }
      .Clients{
          padding: 5%;
      }
      .parallax2 {
        height: 50px;
        background-attachment: fixed;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
      }
      .btn-outline-info {
        color: #0a0a0a;
        background-color: transparent;
        background-image: none;
        border-color: #131313;
        text-transform: uppercase;
      }
      .btn-outline-info:hover {
        color: #fff;
        background-color: #0a0a0a;
        border-color: #0c0c0c
      }
      .btn-outline-info.focus, .btn-outline-info:focus {
        box-shadow: 0 0 0 .2rem rgba(15, 20, 20, 0.5)
      }
      .btn-outline-info.disabled, .btn-outline-info:disabled {
        color: #3f3535;
        background-color: transparent
      }
      .btn-outline-info:not(:disabled):not(.disabled).active, .btn-outline-info:not(:disabled):not(.disabled):active, .show>.btn-outline-info.dropdown-toggle {
        color: #fff;
        background-color: #252727;
        border-color: #1a1a1a
      }
      .btn-outline-info:not(:disabled):not(.disabled).active:focus, .btn-outline-info:not(:disabled):not(.disabled):active:focus, .show>.btn-outline-info.dropdown-toggle:focus {
        box-shadow: 0 0 0 .2rem rgba(10, 16, 17, 0.5)
      }
  `],
})
export class AboutusComponent implements OnInit {
  data: TreeNode[];
  images: any[];
  cars: any[];
  selectedNode: TreeNode;
  noPause = false;
  myurl: any;
  pattern1: any;
  constructor(private messageService: MessageService, private os: OrganizationService) {
    this.myurl = this.os.url;
    this.pattern1 = this.myurl + '/organization/assets/images/pattern/p9.png';
  }
  ngOnInit() {
    $(document).ready(function () {
      $('#collapseExample1').on('click', function () {
        var text = $('#collapseExample1').text();
        if (text === "Read More") {
          $(this).html('Read Less');
        } else {
          $(this).text('Read More');
        }
      });
    });
    this.data = [{
      label: 'PROJECT LEAD',
      type: 'person',
      styleClass: 'ui-person',
      expanded: true,
      data: { name: 'RAMA K', 'avatar': 'a6.jpg' },
      children: [
        {
          label: 'TEAM LEAD',
          type: 'person',
          styleClass: 'ui-person',
          expanded: true,
          data: { name: 'SUKESH KAPPURAM', 'avatar': 'a7.jpg' },
          children: [
            {
              label: 'INTEGRATION TEAM',
              styleClass: 'department-TEAM',
            },
            {
              label: 'DEVELOPMENT TEAM',
              styleClass: 'department-TEAM',
            },
            {
              label: 'UI / UX TEAM',
              styleClass: 'department-TEAM',
            },
            {
              label: 'TESTING TEAM',
              styleClass: 'department-TEAM',
            }
          ]
        }, {
          label: 'TEAM LEAD',
          type: 'person',
          styleClass: 'ui-person',
          expanded: true,
          data: { name: 'POULAV', 'avatar': 'a5.JPG' },
          children: [
            {
              label: 'ANDRIOD TEAM',
              styleClass: 'department-TEAM',
            }
          ]
        }, {
          label: 'TEAM LEAD',
          type: 'person',
          styleClass: 'ui-person',
          expanded: true,
          data: { name: 'SAI NATH', 'avatar': 'a13.JPG' },
          children: [
            {
              label: 'IOS TEAM',
              styleClass: 'department-TEAM',
            }
          ]
        }
      ]
    }]
    this.images = [];
    this.images.push({ source: this.myurl + '/organization/assets/images/team/ibTeam(1).JPG', alt: 'Infobyt Team', title: 'Infobyt Team' });
    this.images.push({ source: this.myurl + '/organization/assets/images/team/ibTeam(6).JPG', alt: 'Database & Integration Team', title: 'Database & Integration Team' });
    this.images.push({ source: this.myurl + '/organization/assets/images/team/ibTeam(8).JPG', alt: 'UI / UX Designs Team', title: 'UI / UX Designs Team' });
    this.images.push({ source: this.myurl + '/organization/assets/images/team/ibTeam(7).JPG', alt: 'Development Team', title: 'Development Team' });
    this.images.push({ source: this.myurl + '/organization/assets/images/team/ibTeam(2).JPG', alt: 'Quality Analyst Team', title: 'Quality Analyst Team' });
    this.cars = [];
    this.cars.push({ name: 'organization', image: 'a12.jpg' });
    this.cars.push({ name: 'organization', image: 'a11.png' });
    this.cars.push({ name: 'organization', image: 'a12.jpg' });
    this.cars.push({ name: 'College', image: 'a9.jpg' });
    this.cars.push({ name: 'College', image: 'a10.jpg' });
    this.cars.push({ name: 'College', image: 'a9.jpg' });
  }
  onNodeSelect(event) {
    this.messageService.add({ severity: 'success', summary: 'Node Selected', detail: event.node.label });
  }
}
