import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CookiesComponent } from './cookies/cookies.component';
import{CopyrightComponent} from './copyright/copyright.component';
import{HelpcenterComponent} from './helpcenter/helpcenter.component';
import{PrivacypolicyComponent} from './privacypolicy/privacypolicy.component';
import{SupportComponent} from './support/support.component';
import{TermsComponent} from './terms/terms.component';
import { RouteGuardGuard } from '../service/route-guard.guard';

var routes: Routes = [
    { path: 'Cookies', component: CookiesComponent },
    { path: 'Copyright', component: CopyrightComponent },
    { path: 'Help', component: HelpcenterComponent },
    { path: 'Privacy', component: PrivacypolicyComponent },
    { path: 'Support', component: SupportComponent },
    { path: 'Terms', component: TermsComponent },
]
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [RouteGuardGuard]
})
export class LinksRoutingModule { }
