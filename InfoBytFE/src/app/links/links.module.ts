import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CopyrightComponent } from './copyright/copyright.component';
import { SupportComponent } from './support/support.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { CookiesComponent } from './cookies/cookies.component';
import { HelpcenterComponent } from './helpcenter/helpcenter.component';
import { LinksRoutingModule } from './links-routing.module';

@NgModule({
  declarations: [CopyrightComponent, SupportComponent, TermsComponent, PrivacypolicyComponent, CookiesComponent, HelpcenterComponent],
  imports: [
    LinksRoutingModule,
    CommonModule
  ]
})
export class LinksModule { }
