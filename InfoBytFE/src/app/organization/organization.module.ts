import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrganizationComponent } from './organization/organization.component';
import { AdmisionDetailsComponent } from './admision-details/admision-details.component';
import { AchievementsComponent } from './achievements/achievements.component';
import { BranchComponent } from './branch/branch.component';
import { BranchSessionrComponent } from './branch-session/branch-session.component';
import { Routes, RouterModule } from "@angular/router";
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { BsDatepickerModule, TypeaheadModule } from 'ngx-bootstrap';
import { MatSlideToggleModule } from "@angular/material";
import { AgmCoreModule } from '@agm/core'
import { AgmDirectionModule } from 'agm-direction';
import { TreeModule } from 'primeng/tree';
import {CalendarModule} from 'primeng/calendar';
import { ToastrModule } from 'ngx-toastr';
import { FileUploadModule } from 'ng2-file-upload';
import { FullCalendarModule } from 'primeng/fullcalendar';
import { CdkTreeModule } from '@angular/cdk/tree';
import { ToastModule } from 'primeng/toast';
import { SpinnerModule } from 'primeng/spinner';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ImageCompressService, ResizeOptions } from 'ng2-image-compress';
import { GalleriaModule } from 'primeng/galleria';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatStepperModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatSliderModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { EventsComponent } from './events/events.component';
import { CoursesComponent } from "./courses/courses.component";
import { TransferComponent } from './transfer/transfer.component';
import { DepartmentComponent } from './department/department.component';
import { ShiftsComponent } from './shifts/shifts.component';
import { AllocateShiftComponent } from './allocate-shift/allocate-shift.component';
import { OrganizationRoutingModule } from './organization-routing.module';
@NgModule({
  imports: [
    OrganizationRoutingModule,
    MatTableModule, MatTableModule,
    MatSlideToggleModule,
    CdkTableModule,
    CdkTreeModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    FullCalendarModule,
    MatSortModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatTableModule,
    ToastModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    SpinnerModule,
    MatTreeModule, MatFormFieldModule,
    CommonModule,
    ToastrModule.forRoot(), TypeaheadModule.forRoot(), FormsModule, DataTablesModule, BsDatepickerModule.forRoot(), MatSlideToggleModule, AgmCoreModule.forRoot({ apiKey: '' }),
    AgmDirectionModule, FileUploadModule, TreeModule, GalleriaModule,CalendarModule
  ], exports: [
    RouterModule
  ], providers: [ImageCompressService, ResizeOptions],
  declarations: [OrganizationComponent, AdmisionDetailsComponent, AchievementsComponent,
    BranchComponent, BranchSessionrComponent,
    EventsComponent, CoursesComponent,  TransferComponent, DepartmentComponent, ShiftsComponent, AllocateShiftComponent]
})
export class OrganizationModule { }
