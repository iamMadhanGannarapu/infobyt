import { Component, OnInit, ViewChild } from '@angular/core';
import { OrganizationService } from '../../service/organization.service';
import { ToastrService } from 'ngx-toastr';
import { Shifts } from '../../modal/Organization/shifts';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-shifts',
  templateUrl: './shifts.component.html',
  styleUrls: ['./shifts.component.scss']
})
export class ShiftsComponent implements OnInit,CanActivateChild  {
  displayedColumns = ['shiftName', 'days', 'startTime', 'endTime', 'edit','Excel']
  dataSource;
  public selectedMoments = [new Date(), new Date()];
  public selectedMoments1 = new Date();
  public selectedMoments2 = new Date();

  show1:any;
  shift: Shifts
  shiftList: any;
  status: any
  branchList: any;
  shiftList1: any;
  write: any;
  read: any;
  constructor(private router:Router,private ls:LoginService, private os: OrganizationService, private toastr: ToastrService) {
    this.shift = new Shifts();
  }
  @ViewChild('frm') form: any
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  addShift(timings: any, graceInTime: any, graceOutTime: any) {
    this.shift.startTime = timings.substring(0, timings.indexOf('~') - 1)
    this.shift.endTime = timings.substring(timings.indexOf('~') + 1, timings.length)
    this.shift.graceInTime = graceInTime;
    this.shift.graceOutTime = graceOutTime
    this.os.saveShift(this.shift).subscribe((data) => {
      this.getShiftBranchById()
      this.form.reset();
      if (data.result) {
        this.toastr.success(data.message)
        this.form.reset();
      }
      else
      {        this.toastr.error(data.message)
      }
    });
  }
  applyFilter(filterValue: string) {
    
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getShiftBranchById() {
    this.os.getShiftsByBranch().subscribe((res) => {
      if(res.result && res.data!='NDF')
      {
      this.shiftList = res.data;
      this.dataSource = new MatTableDataSource(res.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      }
    })
    this.shift.branchId = 0;
  }
  getexcel() {
    this.os.exportAsExcelFile(this.shiftList, 'Shifts');
  }
  
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }
    editShifts(id: any) {
      this.getSchoolBranchByIds()
      this.os.getShiftById(id).subscribe((data) => {
        if (data.result && data.data != 'NDF') {
          this.shift.Id = data.data[0].param_id;
          this.shift.branchId = data.data[0].param_branchid;
          this.shift.shiftName = data.data[0].param_shiftname;
          this.shift.monday = data.data[0].monday
          this.shift.tuesday = data.data[0].tuesday
          this.shift.wednesday = data.data[0].wednesday
          this.shift.thursday = data.data[0].thursday
          this.shift.friday = data.data[0].friday
          this.shift.saturday = data.data[0].saturday
          this.shift.sunday = data.data[0].sunday
          this.shift.startTime = data.data[0].param_starttime;
          let startHours = '' + this.shift.startTime.substring(0, this.shift.startTime.indexOf(':'))
          let startMinutes = '' + this.shift.startTime.substring(this.shift.startTime.indexOf(':') + 1, this.shift.startTime.lastIndexOf(':'))
          let startSeconds = '' + this.shift.startTime.substring(this.shift.startTime.lastIndexOf(':') + 1, this.shift.startTime.length + 1)
          var startT = new Date().setHours(parseInt(startHours), parseInt(startMinutes), parseInt(startSeconds))
          this.shift.endTime = data.data[0].param_endtime;
          let endHours = '' + this.shift.endTime.substring(0, this.shift.endTime.indexOf(':'))
          let endMinutes = '' + this.shift.endTime.substring(this.shift.endTime.indexOf(':') + 1, this.shift.startTime.lastIndexOf(':'))
          let endSeconds = '' + this.shift.endTime.substring(this.shift.endTime.lastIndexOf(':') + 1, this.shift.startTime.length + 1)
          var endT = new Date().setHours(parseInt(endHours), parseInt(endMinutes), parseInt(endSeconds))
         
          this.selectedMoments = [new Date(startT), new Date(endT)];
          this.shift.graceInTime = data.data[0].param_graceintime;
          let graceInTimeHours = '' + this.shift.graceInTime.substring(0, this.shift.graceInTime.indexOf(':'))
          let graceInTimeMinutes = '' + this.shift.graceInTime.substring(this.shift.graceInTime.indexOf(':') + 1, this.shift.graceInTime.lastIndexOf(':'))
          let graceInTimeSeconds = '' + this.shift.graceInTime.substring(this.shift.graceInTime.lastIndexOf(':') + 1, this.shift.graceInTime.length + 1)
          var graceInTimeT = new Date().setHours(parseInt(graceInTimeHours), parseInt(graceInTimeMinutes), parseInt(graceInTimeSeconds))
          this.selectedMoments1 = new Date(graceInTimeT)
          this.shift.graceOutTime = data.data[0].param_graceouttime;
          let graceOutTimeHours = '' + this.shift.graceOutTime.substring(0, this.shift.graceOutTime.indexOf(':'))
          let graceOutTimeMinutes = '' + this.shift.graceOutTime.substring(this.shift.graceOutTime.indexOf(':') + 1, this.shift.graceOutTime.lastIndexOf(':'))
          let graceOutTimeSeconds = '' + this.shift.graceOutTime.substring(this.shift.graceOutTime.lastIndexOf(':') + 1, this.shift.graceOutTime.length + 1)
          var graceOutTimeT = new Date().setHours(parseInt(graceOutTimeHours), parseInt(graceOutTimeMinutes), parseInt(graceOutTimeSeconds))
          this.selectedMoments2 = new Date(graceOutTimeT)
  
          this.shift.optional = data.data[0].param_optional;
        }
      })
    }
  updateShift(timings: any, graceInTime: any, graceOutTime: any) {
    this.shift.startTime = timings.substring(0, timings.indexOf('~') - 1)
    this.shift.endTime = timings.substring(timings.indexOf('~') + 1, timings.length)
    this.shift.graceInTime = graceInTime;
    this.shift.graceOutTime = graceOutTime
    this.os.updateShifts(this.shift).subscribe((data) => {
      if(data.result)
      this.toastr.success(data.message);
      else
      this.toastr.error(data.message)
      this.getShiftBranchById()
    });
  }
  getPermissions() {
    this.ls.checkRightPermissions('shifts').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  ngOnInit() {
    $(document).ready(function(){
      $("#datahide").hide();
      $("#showdata").click(function(){
         $("#viewplcm").show()
         $("#showdata").hide()
         $("#datahide").show();
      });
      $("#datahide").click(function(){
        $("#viewplcm").hide()
        $("#showdata").show()
        $("#datahide").hide();
     });
  });
    this.getShiftBranchById()
    this.getAllShift();
this.getPermissions
  }
  getSchoolBranchByIds() {
    this.os.getBranchById().subscribe((data) => {
      this.branchList = data.data;
    }, (err) => {
      this.toastr.error(err.error.message)
    })
  }
  getAllShift() {
    this.os.getAllShifts().subscribe((res) => {
      if(res.result && res.data!='NDF')   
      this.shiftList1 = res.data;
    })
  }
}