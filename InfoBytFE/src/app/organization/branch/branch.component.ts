import { Component, OnInit, ViewChild } from '@angular/core';
import { Branch } from '../../modal/Organization/Branch';
import { OrganizationService } from '../../service/organization.service';
import { UserService } from '../../service/user.service';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { DatePipe } from '@angular/common';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-branch',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.scss']
})
export class BranchComponent implements OnInit,CanActivateChild  {
  dataSource;
  displayedColumns = ['name', 'startdate', 'status', 'map', 'edit','Excel']
  branchmaster: Branch
  getbranchmaster: any
  show: boolean = true
  
  MasterList: any
  branchId: number
  contactdetailsList: any
  maxDate = new Date();
  minDate = new Date();
  pipe = new DatePipe('en-US');
  colorTheme = 'theme-dark-blue';
  showMap: boolean = false;
  lat: any = 17.437462
  lng: any = 78.448288
  latitude: any
  longitude: any
  lati: any = 10;
  public origin: any
  public destination: any;
  lngi: any = 77.5941009882813;
  bsConfig: Partial<BsDatepickerConfig>;
  rolename:any
  read: any;
  write: any;
  constructor(private ls:LoginService,private toastr: ToastrService, private os: OrganizationService, private us: UserService,private router: Router) {
    this.branchmaster = new Branch()
  }
  markers: marker[] = [
    {
      lat: 0,
      lng: 0,
      label: 'O',
      draggable: true
    },
  ]
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("schbrn") form: any;
  getAllBranchs() {
 
    this.os.getAllBranch().subscribe((data) => {
     this.getbranchmaster = data.data;

     if(data.data!="NDF")
     {
      for (let i in this.getbranchmaster) {
        if (this.getbranchmaster[i].branchstatus == 'Inactive') {
          this.getbranchmaster[i].branchstatus = false
        } else {
          this.getbranchmaster[i].branchstatus = true
        }
      }
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      for(let j in this.getbranchmaster){
     this.origin = { lat:  parseFloat(data.data[0].latitude), lng:  parseFloat(data.data[0].longitude) }
      }}
    });
  }
  btnGetMapClick() {
    this.showMap = true;
  }
  onMapClick(ev) {
    this.branchmaster.latitude = ev.coords.lat + ''
    this.branchmaster.longitude = ev.coords.lng + ''
    this.showMap = false;
  }
  getPermissions() {
    this.ls.checkRightPermissions('branch').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  

canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }

  ngOnInit() {
    
    this.getPermissions();
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((school) => {
        this.rolename=school.data[0].rolename;
          if (this.rolename == "OrganizationAdmin") {
            this.getAllBranchs();
          }
          else {
            this.os.getBranchById().subscribe((data) => {
              this.getbranchmaster = data.data;
              if (this.getbranchmaster[0].branchstatus == 'Inactive') {
                this.getbranchmaster[0].branchstatus = false
              } else {
                this.getbranchmaster[0].branchstatus = true
              }
              this.dataSource = new MatTableDataSource(data.data)
              this.dataSource.sort = this.sort
              this.dataSource.paginator = this.paginator
              this.origin = { lat:  parseFloat(data.data[0].latitude), lng:  parseFloat(data.data[0].longitude) }
            });
          }
      })
    }
    this.os.getAllOrganization().subscribe((data) => {
      this.MasterList = data.data;
      var v=this.MasterList[0].establishedyear
    this.maxDate.setDate(this.maxDate.getDate() - 1);
    this.minDate.setDate(1);
    this.minDate.setMonth(0);
    this.minDate.setFullYear(v);
      this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    })
this.branchmaster.organizationId=0;
this.branchmaster.contactDetailsId=0;
$(document).ready(function () {
  $("#datahide").hide();
  $("#showdata").click(function () {
    $("#viewallBranch").show()
    $("#showdata").hide()
    $("#datahide").show();
  });
  $("#datahide").click(function () {
    $("#viewallBranch").hide()
    $("#showdata").show()
    $("#datahide").hide();
  });
});
  }
     getexcel(){
        this.os.exportAsExcelFile(this.getbranchmaster, 'Branch');
  }
  applyFilter(filterValue: string) {
  
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  BranchDetails(id: number) {
    this.show = false
    this.os.getBranchByOrgBrnId(id).subscribe((data) => {
      this.getbranchmaster = data.data;
    });
  }
  addBranch() {
    this.os.saveBranch(this.branchmaster).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.form.reset();
      }
      else
      this.toastr.error(data.message);
      this.getAllBranchs();
    }, (err) => {
      this.toastr.error(err.error.message)
    });
  }
  editBranch(branchid: number) {
    this.getIds();
    this.os.getBranchByOrgBrnId(branchid).subscribe((data) => {
      this.getbranchmaster = data.data;
      this.branchmaster.Id = data.data[0].branchid;
      this.branchmaster.organizationId = data.data[0].organizationid;
      this.branchmaster.startDate = this.pipe.transform(data.data[0].startdate, 'MM-dd-yyyy');
      this.branchmaster.contactDetailsId =data. data[0].branchcontact;
      this.us.getAllContactDetailsByRoleById(this.branchmaster.contactDetailsId).subscribe((data)=>{
        this.contactdetailsList=data.data
        this.branchmaster.contactDetailsId=data.data[0].contactid
      })
      this.branchmaster.latitude =data. data[0].latitude;
      this.branchmaster.longitude =data. data[0].longitude;
    });
  }
  getIds() {
     this.us.getAllContactDetailsByRole().subscribe((data) => {
       this.contactdetailsList = data.data;
     })
  }

  onChange(branchstatus, id) {
    this.os.getBranchByOrgBrnId(id).subscribe((data) => {
      this.branchmaster.status = branchstatus.checked;
      this.os.updateBranchStatus(id,this.branchmaster).subscribe((data) => {
        this.toastr.success('Toggled Successfully');
        this.ngOnInit()
      })
    })
  }
  updateBranch() {
    this.os.updateBranch(this.branchmaster).subscribe((data) => {
      this.getAllBranchs();
      if(data.result)
      this.toastr.success(data.message);
      else
      this.toastr.error(data.message);
    }, (err) => {
      this.toastr.error(err.error.message)
    })
  }
  addBranchAdmin(){
    this.router.navigate(["user/add-user/BranchAdmin"]);
  }
}
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}