import { Component, OnInit, ViewChild } from '@angular/core';
import { Achievement } from '../../modal/Organization/achievements';
import { OrganizationService } from '../../service/organization.service';
import { UserService } from '../../service/user.service';
import { LoginService } from '../../service/login.service';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ImageCompressService } from 'ng2-image-compress';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { DatePipe } from '@angular/common';
import { DashboardService } from "src/app/service/dashboard.service";
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-achievements',
  templateUrl: './achievements.component.html',
  styleUrls: ['./achievements.component.scss']
})
export class AchievementsComponent implements OnInit,CanActivateChild  {
  dataSource;
  displayedColumns = ['Description', 'AchievementDate', 'Image', 'edit', 'Excel']
  achievements: Achievement;
  show: boolean = true
  show1: boolean = true
  AchievementId: number;
  userProfile: any;
  userTrainerList: any
  userTraineeList: any
  showFilter: string = ''
  image = { 'URL': '', 'valid': false };
  images: File;
  selectList: any
  data: any;
  myurl: string;
  dataSources;
  ClassTeacher: any
  pipe = new DatePipe('en-US');
  maxDate = new Date();
  minDate = new Date();
  bsConfig: Partial<BsDatepickerConfig>;
  colorTheme = 'theme-dark-blue';
  color = "success";
  rolename: any;
  read: any;
  write: any;
  constructor(private router:Router,private ls: LoginService, private imgCompressService: ImageCompressService, private ds: DashboardService, private toastr: ToastrService, private os: OrganizationService, private us: UserService) {
    this.achievements = new Achievement();
    this.myurl = os.url
    this.maxDate.setDate(this.maxDate.getDate() - 1);
    this.minDate.setFullYear(this.maxDate.getFullYear() - 20);
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
  }
  @ViewChild("achievementsForm") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  ngOnInit() {
    $(document).ready(function(){
      $("#datahide").hide();
      $("#showdata").click(function(){
         $("#viewachiv").show()
         $("#showdata").hide()
         $("#datahide").show();
      });
      $("#datahide").click(function(){
        $("#viewachiv").hide()
        $("#showdata").show()
        $("#datahide").hide();
     });
  });
    this.getPermissions();
    this.getAllAchievements();
    this.viewAchievements();
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename = data.data[0].rolename;
      }, (err) => {
        this.toastr.error(err.error.message)
      })
    }
    this.achievements.userId = 0;
  }
  getPermissions() {
    this.ls.checkRightPermissions('achievements').subscribe((data) => {
      this.read = data.data[0].read;
      this.write = data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getSingleAchievement(i: number) {
    this.show = false
    this.os.getAchievementById(i).subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
    
      this.dataSources = new MatTableDataSource( data.data)
      this.dataSources.sort = this.sort
      this.dataSources.paginator = this.paginator
      }
    });
  }
  getexcel() {
    this.os.exportAsExcelFile(this.dataSources.data, 'Achievements');
  }
  getAllAchievements() {
    this.os.getAllAchievements().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.dataSources = new MatTableDataSource(data.data)
      this.dataSources.sort = this.sort
      this.dataSources.paginator = this.paginator
      }
    });
  }
  getIds() {
    this.us.getAllStaffUsersForFeedBack().subscribe((data) => {
      this.userTrainerList = data.data
    })
    this.us.getAllFromApplicantsForTrainees().subscribe((data) => {
      this.userTraineeList = data.data
    })
    this.us.getAllClassTeacher().subscribe((res) => { 
      this.ClassTeacher = res.data; 
    });
  }
 
  fileChangeEvent(fileInput: any) {
    var fileName = fileInput.target.value;
    var idxDot = fileName.lastIndexOf(".") + 1;
    var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
    if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
      ImageCompressService.filesToCompressedImageSource(fileInput.target.files).then(observableImages => {
        observableImages.subscribe((image) => {
          this.image.URL = image.compressedImage.imageDataUrl;
          this.achievements.image = image.compressedImage.imageDataUrl;
          this.achievements.image = this.achievements.image.replace("data:image/gif;base64,", "");
          this.achievements.image = this.achievements.image.replace("data:image/jpeg;base64,", "");
          this.achievements.image = this.achievements.image.replace("data:image/jpg;base64,", "");
          this.achievements.image = this.achievements.image.replace("data:image/png;base64,", "");
        }, (error) => {
          this.toastr.error("Error while converting");
        });
      });
    }else{
      alert("Only jpg/jpeg and png files are allowed!");
     
    }
  }

  editAchievement(i: number) {
    this.getIds();
    this.AchievementId = i
    this.show = false
    this.os.getAchievementById(i).subscribe((data) => {
      if(data.result && data.data!='NDF'){
      this.achievements.Id = data.data[0].id;
      this.achievements.userId = data.data[0].userid
      this.achievements.description = data.data[0].description
      this.achievements.achievementDate = this.pipe.transform(data.data[0].achievementdate, 'MM-dd-yyyy')
      }
    })
  }
  updateAchievementDetails() {
    this.getIds()
    this.os.updateAcheievement(this.achievements).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
      }
      else
        this.toastr.error(data.message);
      this.getAllAchievements();
    })
  }
  showTrainer() {
    if (this.showFilter == 'faculty') {
      this.selectList = this.userTrainerList
    }
    else {
      this.selectList = this.userTraineeList
    }
  }
  addAchievement() {
    this.os.saveAchievements(this.achievements).subscribe((data) => {
      this.getAllAchievements();
      if (data.data!="NDF") {
        this.toastr.success(data.message);
        this.form.reset();
      }
      else {
        this.toastr.error(data.message);
      }
      this.ds.addAchievementsNotification().subscribe((data) => { })
    });
  }
  viewAchievements() {
    this.show1=!this.show1;
    this.us.getUserbySession().subscribe((data) => {
      if (data) {
        this.userProfile = data.data[0];
      }
      if (this.userProfile.userid) {
        this.ls.getAchievementByUserId(this.userProfile.userid).subscribe((data) => {
          if (data.data != "NDF") {
            this.dataSource = new MatTableDataSource(data.data)
            this.dataSource.sort = this.sort
            this.dataSource.paginator = this.paginator
          }
        })
      }
    })
  }
}