import { Component, OnInit, ViewChild } from '@angular/core';
import { AdmisionDetails } from '../../modal/Organization/AdmisionDetails';
import { OrganizationService } from '../../service/organization.service';
import { FeeService } from '../../service/fee.service';
import { UserService } from '../../service/user.service';
import { ToastrService } from 'ngx-toastr';
import { MatSort, MatTableDataSource, MatPaginator, DateAdapter } from '@angular/material';
import { ImageCompressService, ResizeOptions, ImageUtilityService, IImage, SourceImage } from  'ng2-image-compress';
import { MessageService } from 'primeng/api';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-admision-details',
  templateUrl: './admision-details.component.html',
  styleUrls: ['./admision-details.component.scss']
})
export class AdmisionDetailsComponent implements OnInit ,CanActivateChild {
  dataSource;
  displayedColumns = ['ParentEmail', 'MobileNumber', 'UserName', 'FeeName', 'Concessionname','Approve','Excel']
  admitionn: AdmisionDetails;
  admissionNum: any
  branchList: any
  userList: any
show1:boolean=true;
  feeList: any;
  images: File;
  image = { 'URL': '', 'valid': false };
  editButton = false;
  all = true;
  Show: boolean = false;
  concessionList: any
  rolename: any;
  acceptTraineeList: Object;
  
  read: any;
  write: any;
  constructor(private ls:LoginService,private toastr: ToastrService,private ms: MessageService, private router: Router, private os: OrganizationService, private fs: FeeService, private us: UserService) {
    this.admitionn = new AdmisionDetails()
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("admition") form: any;
  getAllAdmissions() {
    this.show1=!this.show1;
    this.os.getAllAdmission().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      }
    });
  }
     getexcel(){
        this.os.exportAsExcelFile(this.dataSource.data, 'sample');
  }
  open() {
    this.Show = !this.Show;
    this.getIds();
  }
  
  fileChangeEvent(fileInput: any) {
    var fileName = fileInput.target.value;
    var idxDot = fileName.lastIndexOf(".") + 1;
    var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
    if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
      ImageCompressService.filesToCompressedImageSource(fileInput.target.files).then(observableImages => {
        observableImages.subscribe((image) => {
          this.image.URL = image.compressedImage.imageDataUrl;
            this.admitionn.image = image.compressedImage.imageDataUrl;
            this.admitionn.image = this.admitionn.image.replace("data:image/gif;base64,", "");
            this.admitionn.image = this.admitionn.image.replace("data:image/jpeg;base64,", "");
            this.admitionn.image = this.admitionn.image.replace("data:image/jpg;base64,", "");
            this.admitionn.image = this.admitionn.image.replace("data:image/png;base64,", "");
        }, (error) => {
          this.toastr.error("Error while converting");
        });
      });
    }else{
      alert("Only jpg/jpeg and png files are allowed!");
     
    }
  }



  getPermissions() {
    this.ls.checkRightPermissions('admision-details').subscribe((data)=>{
      
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }
  ngOnInit() {
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        
        this.rolename=data.data[0].rolename;
      }, (err) => {
        this.toastr.error(err.error.message)
      })
    }
    this.getPermissions();
    this.getAllAdmissions();
    this.getBranchByIds();
    this.acceptTransfer();
    this.admitionn.userId=0;
    this.admitionn.branchId=0;
    this.admitionn.feeId=0;
    this.admitionn.concessionId=0;
  }
  editAdmition(i: number) {
    this.getIds()
    this.editButton = true;
    this.all = false;
    this.os.getAdmissionById(i).subscribe((data) => {
      this.admitionn.admissionNum = data.data[0].admissionnum;
      this.admitionn.parentEmail = data.data[0].parentemail;
      this.admitionn.parentMobile = data.data[0].parentmobile;
      this.admitionn.branchId = data.data[0].branchid;
      this.admitionn.userId = data.data[0].userid;
      this.admitionn.feeId = data.data[0].feeid;
      this.admitionn.concessionId = data.data[0].concessionid;
    }, (err) => {
      this.toastr.error(err.error.message)
    })
  }
  getBranchByIds() {
    this.os.getBranchById().subscribe((data) => {
      this.branchList = data.data;
    }, (err) => {
      this.toastr.error(err.error.message)
    })
  }
  getAllFees() {
    this.fs.getAllFee().subscribe((data) => {
      this.feeList = data.data;
    }, (err) => {
      this.toastr.error(err.error.message)
    })
  }
  getAllConcessions() {
    this.fs.getAllConcession().subscribe((data) => {
      this.concessionList = data.data;
    }, (err) => {
      this.toastr.error(err.error.message)
    })
  }
  getAllTraineeFromApplicant() {
    this.us.getAllTraineeFromApplicants().subscribe((data) => {
      this.userList= data.data
      if(this.acceptTraineeList!= 'undefined'){
      this.userList=this.userList.concat(this.acceptTraineeList)
    }
    }, (err) => {
      this.toastr.error(err.error.message)
    })
  }
  getIds() {
    this.getBranchByIds();
    this.getAllFees();
    this.getAllConcessions();
    this.getAllTraineeFromApplicant()
  }
  updateAdmissionDetails() {
    this.getIds()
    this.os.updateAdmission(this.admitionn).subscribe((data) => {
      if(data.result)
      this.toastr.success(data.message);
      else
      this.toastr.error(data.message);
      this.getAllAdmissions();
    })
  }
  addAdmition() {
    this.os.saveAdmitionDetails(this.admitionn).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.form.reset();
      }
      else
      this.toastr.error(data.message);
      this.getAllAdmissions();
    }, (err) => {
      this.toastr.error(err.error.message)
    });
  }
  applyFilter(filterValue: string) {
  
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  addTraineeUser(){
    this.router.navigate(['add-user','Trainee']);
  }
  applyTransfer()
  {
    this.os.applyTc().subscribe((data)=>
    { 
      this.ms.clear();
       if (data.result)
      {
        this.onReject();
        this.getAllAdmissions();
      this.toastr.success('Successfully TC Applied');
    }   
  }, (err) => {
    this.toastr.error(err.error.message)
  });
  }
  acceptTransfer()
  {
    this.os.getAcceptTrainees().subscribe((data)=>
    {
      this.acceptTraineeList=data.data;
    })
  }
  getStdntTrnasferStatus()
  {
    this.os.getTransferStatus().subscribe((data)=>
    {   
    this.studentStatus=data.data[0];
    })
  }
  studentStatus(studentStatus: any): any {
    throw new Error("Method not implemented.");
  }
  showConfirm() {
    this.ms.clear();
    this.ms.add({key: 'c', sticky: true, severity:'warn', summary:'Are you sure?', detail:'Confirm to proceed '});
}
onReject() {
  this.ms.clear('c');
}
acceptTc(admissionNum:any,type:any){
 this.os.acceptTc(admissionNum,type).subscribe((data)=>{
if(data.result)
{
  this.toastr.success(data.message);
  this.getAllAdmissions();
}
else
{
  this.toastr.error(data.message+' Errror:'+data.error)
}
 })
}
}