
 import { Component, OnInit,ViewChild } from '@angular/core';
 import { BsDatepickerConfig } from 'ngx-bootstrap';
 import { DatePipe } from '@angular/common';
 import { TypeaheadModule } from 'ngx-bootstrap';
 import { OrganizationService} from '../../service/organization.service';
 import { BatchService } from '../../service/batch.service';
 import { EventsDetails }  from '../../modal/Organization/eventsdetails';
 import { UserService } from '../../service/user.service';
 import { ToastrService } from 'ngx-toastr';
 import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
 @Component({
   selector: 'app-events',
   templateUrl: './events.component.html',
   styleUrls: ['./events.component.scss']
 })
 export class EventsComponent implements OnInit ,CanActivateChild {
   dataSource: MatTableDataSource<{}>;
   displayedColumns = ['id', 'departmentid','fromdate','starttime','endtime','edit','option','Excel']
   dataSource1:MatTableDataSource<{}>;
   displayedColumns1=['id','name', 'departmentid','fromdate','starttime','endtime','option'];
   department: any;
   res: any;
   eventdetailsInfo:EventsDetails;
   pipe = new DatePipe('en-US');
   colorTheme = 'theme-dark-blue';
   bsConfig: Partial<BsDatepickerConfig>;
   events: any = [{ eventName: 'sport' }]
   result:boolean;
   show1:boolean=true;
   images: File;
   collegeBranchDetails:any=[{'branchid':'','branchname':''}]
   userConctact:any={'id':'','mobile':'','email':''};
   applicantConctact:any={'id':'','mobile':'','email':''};
   image={'URL':'','valid':false};
   userList: any;
   defaultList: any;
   rolename: any;
   branch:any;
   requests:any=[];
 maxDate = new Date();
   options: { displayEventTime: boolean; header: { left: string; center: string; right: string; }; };
   callist: any;
   read: any;
   write: any;
   constructor(private router:Router,private ls:LoginService,private os:OrganizationService,private us:UserService,private toastr: ToastrService,private bs: BatchService) {
     this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
     this.eventdetailsInfo=new EventsDetails();
     this.maxDate.setDate( this.maxDate.getDate()+365);
   }
   @ViewChild(MatSort) sort: MatSort;
   @ViewChild(MatPaginator) paginator: MatPaginator;
   @ViewChild("addevents") form: any;
   contact(){
     this.os.getContact(this.eventdetailsInfo.userId).subscribe((data)=>{
       this.userConctact=data.data[0];
     })
   }
  getBranch(){
    this.os.getBranchById().subscribe((data)=>{
 this.collegeBranchDetails=data.data;
 this.branch=data.data[0].branchid;
    })
  }
  

canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }

   sendparticipationRequest(id:any){
     this.os.sendparticipation(id).subscribe((data)=>{
     })
   }
   getAllParticipationRequests(){
     this.os.getParticipentsList().subscribe((data)=>{
       if(data.result==true&&data.data!="NDF")
       {
       this.requests=data.data;
       this.dataSource1 = new MatTableDataSource(data.data)
       this.dataSource1.sort = this.sort
       this.dataSource1.paginator = this.paginator
       this.result=!this.result;
       }
     })
   }
   addEvent(dateTime:any){
     this.eventdetailsInfo.fromDate=new Date(dateTime.substring(0,dateTime.indexOf('~')-1))
     this.eventdetailsInfo.toDate=new Date(dateTime.substring(dateTime.indexOf('~')+1,dateTime.length))
     this.eventdetailsInfo.startTime=dateTime.substring(0,dateTime.indexOf('~')-1)
     this.eventdetailsInfo.endTime=dateTime.substring(dateTime.indexOf('~')+1,dateTime.length)
     this.eventdetailsInfo.contactdetailsId=this.userConctact.id;
     this.os.eventPosted(this.eventdetailsInfo).subscribe((data)=>{
     if (data.result) {
      this.form.reset();
       this.toastr.success(data.message);
     }
     else
     {
      this.toastr.error(data.message);
     }
   });
 }
 editEvents(id:number){
  this.os.getEventId(id).subscribe((data)=>{
    if(data.result && data.data!='NDF')
      {
this.eventdetailsInfo.Id=data.data[0].id;
this.eventdetailsInfo.name=data.data[0].name;
this.eventdetailsInfo.departmentId=data.data[0].departmentid;
this.eventdetailsInfo.contactdetailsId=data.data[0].contactdetailsid;
this.eventdetailsInfo.fromDate=this.pipe.transform(data.data[0].fromdate,'MM-dd-yyyy');
this.eventdetailsInfo.toDate=this.pipe.transform(data.data[0].todate,'MM-dd-yyyy');
this.eventdetailsInfo.startTime=data.data[0].starttime;
this.eventdetailsInfo.endTime=data.data[0].endtime;
this.eventdetailsInfo.branchId=data.data[0].branchid;
this.eventdetailsInfo.description=data.data[0].description;
this.eventdetailsInfo.venue=data.data[0].venue;
this.eventdetailsInfo.userId=data.data[0].userid;
      }
  });
}
   getAllDepartmentss(){
     this.os.getAllDepartments().subscribe((data)=>{
      if(data.result==true&&data.data!="NDF")
       this.department=data.data;
     })
    }
    updateEventsDetails(){
     this.eventdetailsInfo.contactdetailsId=this.userConctact.id;
     this.os.updateEvent(this.eventdetailsInfo).subscribe((data)=>{
       if(data.result)
       {
        this.toastr.success(data.message);
       }
       else
       this.toastr.error(data.message);
     })
   }
   getAllEventsByBranchId() {
     this.show1=!this.show1;
    this.os.getAllEvents().subscribe((data) => {
      if(data.result && data.data!="NDF")
{
      this.dataSource = new MatTableDataSource(data.data);
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
}
    })
  }
  getexcel(){
      this.os.exportAsExcelFile(  this.dataSource.data , 'Events');
}
  getAllEventsCal(){
    this.os.getAllEventscalender().subscribe((data)=>{
      this.callist=data;
    })
  }
   allDefaultUserss(){
     this.os.getAllDefaultUser().subscribe((data)=>{
      if(data.result==true&&data.data!="NDF")
       this.defaultList=data.data;
     })
   }
   getAllUsersByBranchs(){
     this.os.getAllUsersByBranch().subscribe((data)=>{
      if(data.result==true&&data.data!="NDF")
       this.userList=data.data;
     })
   }
   getPermissions() {
    this.ls.checkRightPermissions('events').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
ngOnInit() {
  this.getPermissions();
  this.getAllEventsByBranchId();
    this.getAllDepartmentss();
    this.getAllUsersByBranchs();
    this.allDefaultUserss();
    this.getAllEventsCal();
    this.getBranch();
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((school) => {
        this.rolename=school.data[0].rolename;
      })
    }
    this.options = {
      displayEventTime: false,
     header: {
         left: 'prev,next',
         center: 'title',
         right: 'month,agendaWeek,agendaDay'
     }
    }
    this.eventdetailsInfo.userId=0;
    this.eventdetailsInfo.departmentId=0;
    this.eventdetailsInfo.contactdetailsId=0;
    this.eventdetailsInfo.branchId=0;
   }
   applyFilter(filterValue: string) {
    
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  eventResponse(id:any,type:any)
  {
    this.os.evntResponse(id,type).subscribe((data)=>{
      this.getAllParticipationRequests()
    })
  }
}