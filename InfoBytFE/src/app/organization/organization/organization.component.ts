
import { Component, OnInit, ViewChild } from '@angular/core';
import { Organization } from '../../modal/Organization/Organization'
import { OrganizationService } from '../../service/organization.service'
import { UserService } from "../../service/user.service";
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ImageCompressService} from  'ng2-image-compress';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { DatePipe } from '@angular/common';
import { Institute } from '../../modal/Settings/Institute'
import { SettingsService } from "src/app/service/settings.service";
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.scss']
})
export class OrganizationComponent implements OnInit,CanActivateChild  {
  dataSource;
  displayedColumns = ['name', 'type', 'status', 'establishedyear', 'image', 'Manage','Excel']
  school: Organization;
  message: any;
  schoolvalidation: any;
  currentDate: any;
  start;
  schoolmasterList: any[] = [];
  show: boolean = true
  schoolId: number;
  color = "success"
  contactDetails: any;
  instituteTypeList: any;
  image = { 'URL': '', 'valid': false };
  maxDate = new Date();
  minDate = new Date();
  pipe = new DatePipe('en-US');
  schoolAddButton: boolean = true;
  colorTheme = 'theme-dark-blue';
  schoolStatus: boolean;
  userProfile: any;
  showTable: boolean = true;
  bsConfig: Partial<BsDatepickerConfig>;
  data: any
  myurl: string;
  images: File;
  rolename: any;
  instituteList: any;
  boardList: any;
  institute: Institute;
  max: any;
  min: any;
  BoardandType:any=[{'boardid':'','name':'','typeid':'','institutename':'','id':'','boardname':''}];
  read: any;
  write: any;
  getbranchmaster: any;
  ShowLogo: boolean;
  constructor(private ls:LoginService,private sts:SettingsService,private toastr: ToastrService, private os: OrganizationService, private us: UserService, private router: Router) {
    this.school = new Organization();
    this.institute = new Institute();
    this.myurl = this.os.url;
    this.maxDate.setDate(this.maxDate.getDate() - 1);
    this.minDate.setDate(this.minDate.getDate() - 40000);
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    this.start = this.currentDate - 50;
    this.min = this.minDate.getFullYear();
    this.max = this.maxDate.getFullYear();
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("schoolFrm") form: any;
  verifySchool(inp: string, inpData: any) {
    this.os.verifyUserData(inp, inpData.value).subscribe((data) => {
      this.schoolvalidation = data.message;
    });
  }
  editSchool(Id: number) {
    this.schoolId = Id
    this.show = false
    this.os.getOrganizationById(Id).subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.schoolmasterList = data.data;
      this.school.Id = data.data[0].id;
      this.school.name = data.data[0].name;
      this.school.type = data.data[0].type;
      this.school.establishedYear = data.data[0].establishedyear;
      this.school.registrationDate = this.pipe.transform(data.data[0].regdate, 'MM-dd-yyyy');
      this.school.contactDetailsId = data.data[0].contactdetailsid;
      }
    })
  }
  getBoardAndTypeById(id:any){
this.sts.getBoardorType(id).subscribe((data)=>{
  this.BoardandType=data.data;
})
  }
  getInstitute() {
    this.sts.getInstitutes().subscribe((res) => {
      this.instituteList = res.data
    })
  }
  getBoards() {
    this.sts.getBoardType().subscribe((res) => {
      this.boardList = res.data
    })
  }
  
  fileChangeEvent(fileInput: any) {
    var fileName = fileInput.target.value;
    var idxDot = fileName.lastIndexOf(".") + 1;
    var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
    if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
      ImageCompressService.filesToCompressedImageSource(fileInput.target.files).then(observableImages => {
        observableImages.subscribe((image) => {
          this.image.URL = image.compressedImage.imageDataUrl;
            this.school.image = image.compressedImage.imageDataUrl;
            this.school.image = this.school.image.replace("data:image/gif;base64,", "");
            this.school.image = this.school.image.replace("data:image/jpeg;base64,", "");
            this.school.image = this.school.image.replace("data:image/jpg;base64,", "");
            this.school.image = this.school.image.replace("data:image/png;base64,", "");
                 }, (error) => {
          this.toastr.error("Error while converting");
        });
      });
    }else{
      alert("Only jpg/jpeg and png files are allowed!");
     
    }
  }


  updateSchool() {
    this.os.updateOrganization(this.school).subscribe(data => {
      if(data.result )
      {
      this.getAllSchool();
      this.toastr.success(data.message);
      }
      else
      {
        this.toastr.error(data.message)
      }
    });
  }
  onChange(status, id) {
    this.os.getOrganizationById(id).subscribe((data) => {
      this.school.Id = data.data[0].id
      this.school.name = data.data[0].name;
      this.school.type = data.data[0].type;
      this.school.establishedYear = data.data[0].establishedyear;
      this.school.registrationDate = data.data[0].regdate;
      this.school.contactDetailsId = data.data[0].contactdetailsid;
      this.school.status = status.checked;
      this.os.updateOrganization(this.school).subscribe((data) => {
 if(data[0].fn_updateschool=='Active')
        {
          this.toastr.success('Successfully Activated')
        }else{
          this.toastr.success('Successfully InActivated')
        }
        this.ngOnInit()
      }, (err) => {
        this.toastr.error(err.error.message)
      });
    }, (err) => {
      this.toastr.error(err.error.message)
    });
  }
  getAllSchool() {
    this.os.getAllOrganization().subscribe((data) => {
      if (data.result && data.data!="NDF") {
        this.showTable = false
        this.schoolAddButton = false
        if (data.data[0].schoolstatus == "Active")
          data.data[0].status = true
        else
          data.data[0].status = false
        this.schoolmasterList = data.data;
        this.dataSource = new MatTableDataSource(data.data)
        this.dataSource.sort = this.sort
        this.dataSource.paginator = this.paginator
      }
    });
  }
  getexcel(){
      this.os.exportAsExcelFile( this.schoolmasterList, 'sample');
}
getPermissions() {
  this.ls.checkRightPermissions('organization').subscribe((data)=>{
    this.read=data.data[0].read;
    this.write=data.data[0].write;
    

this.canActivateChild(this.read)
  })
}

canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }

  ngOnInit(): void {
    $(document).ready(function () {
      $("#datahide").hide();
      $("#showdata").click(function () {
        $("#vieworg").show()
        $("#showdata").hide()
        $("#datahide").show();
      });
      $("#datahide").click(function () {
        $("#vieworg").hide()
        $("#showdata").show()
        $("#datahide").hide();
      });
    });
     
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.userProfile = data.data[0];
        this.rolename = this.userProfile.rolename;
      }, (err) => {
        this.toastr.error(err.error.message)
      });
    }
    this.us.getRolesByUser().subscribe((data) => {
      if (data[0] == "IBAdmin") {
        this.schoolStatus = true;
        this.getAllSchoolsForIB();
      }
      else {
        this.getAllSchool();
      }
    }, (err) => {
      this.toastr.error(err.error.message)
    });
    this.os.getAllBranch().subscribe((data) => {
      this.getbranchmaster = data.data;
      if(this.getbranchmaster.length>1){
        this.ShowLogo=true
      }else
      {
        this.ShowLogo=false
      }
    })
    this.getPermissions();
    this.school.establishedYear = this.currentDate;
    this.school.type = '0';
    this.school.boardId=0;;
    this.school.contactDetailsId=0;
  }
  getSingleschool(i: number) {
    this.show = false
    this.os.getOrganizationById(i).subscribe((data) => {
      this.schoolmasterList = data.data;
    });
  }
  schoolById() {
    if(this.getbranchmaster.length>1)
    {
      this.router.navigate(["organization/Branch"])
    }
    else{
    this.router.navigate(["settings/Permission"])
    }
  }
  addSchool() {
    this.os.saveOrganization(this.school).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.form.reset();
      }
      else
      {
        this.toastr.error(data.message);
      }
      this.getAllSchool();
    });
    this.ngOnInit();
  }
  getInstituteType() {
    this.sts.getAllInstituteType().subscribe((data) => {
      this.instituteTypeList = data.data;
    });
  }
  getAllSchoolsForIB() {
    this.os.getAllOrganizationsIB().subscribe((data) => {
      for (let i in data) {
        if (data[i].status == "Active")
          data[i].status = true
        else
          data[i].status = false
      }
      this.schoolmasterList = data.data
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
    });
  }
  applyFilter(filterValue: string) {
 
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getIds() {
    this.getInstitute();
    this.getBoards();
    this.us.getAllContactDetails().subscribe((data) => {
      this.contactDetails = data.data;
    });
    this.getInstituteType();
    this.school.boardId = 0;
    this.school.contactDetailsId = 0;
    this.school.organizationTypeId = 0;
  }
  onTextChange() {
    this.school.establishedYear = new Date(this.school.establishedYear )
    this.school.establishedYear =this.school.establishedYear.getFullYear();
    $("#esYear").prop("readonly", false);
  }
}
