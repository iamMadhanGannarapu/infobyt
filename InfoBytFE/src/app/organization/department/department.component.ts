import { Component, OnInit, ViewChild } from '@angular/core';
import { Departments } from '../../modal/Organization/Department'

import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import{ OrganizationService } from 'src/app/service/organization.service'
import { LoginService } from 'src/app/service/login.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.scss']
})
export class DepartmentComponent implements OnInit {
  departmentobject: Departments;
  viewdepartment:boolean;
  read: any;
  write: any;
  dataSource;
  dataSoursesDept;
  branchList: any
  show1:boolean=true;
  displayedColumndept = ['DepartmentsName','Excel']
  deptList: any;
    constructor(private ls:LoginService,private router: Router,private toastr: ToastrService, private os: OrganizationService) {
      this.departmentobject = new Departments();
    }
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild("adddepartmentForm") form: any;
    applyFilter(filterValue: string) {

      this.dataSoursesDept.filter = filterValue.trim().toLowerCase();
    }
    adddepartments() {
      this.os.saveDepartments(this.departmentobject).subscribe((data) => {
        if(data.result)
      {
        this.deptList=data
        this.toastr.success(data.message)
        this.getDepartments()
        this.form.reset();
      }
      else
      {
        this.toastr.error(data.message)
      }
      })
    }
    ngOnInit() {
      this.getDepartments();
      this.getPermissions();
      this.os.getBranchById().subscribe((data) => {
        this.branchList = data.data;
      },
        err => {
          this.toastr.error(err.error.message);
        })
    }
    getPermissions() {
      this.ls.checkRightPermissions('department').subscribe((data)=>{
        this.read=data.data[0].read;
        this.write=data.data[0].write;
        this.canActivateChild(this.read)
      })
    }
    

canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }

    getDepartments() {
      this.os.getAllDepartments().subscribe((data) => {
        if(data.result && data.data!='NDF')
      {
        this.dataSoursesDept = new MatTableDataSource(data.data)
        this.dataSoursesDept.sort = this.sort
        this.dataSoursesDept.paginator = this.paginator
      }
      })
    }
    viewDepartment() {
      this.show1=!this.show1;
      this.viewdepartment = !this.viewdepartment
    }
    getexcel(){
      this.os.getAllCourses().subscribe((data) => {
        this.os.exportAsExcelFile(data, 'sample');
    })
  }
  }