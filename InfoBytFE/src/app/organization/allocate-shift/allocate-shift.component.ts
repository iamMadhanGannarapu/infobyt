import { Component, OnInit, ViewChild } from '@angular/core';
import { AllocateShifts } from 'src/app/modal/Organization/allocateshifts';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/service/user.service';
import { DatePipe } from '@angular/common';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { OrganizationService } from 'src/app/service/organization.service';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';

@Component({
  selector: 'app-allocate-shift',
  templateUrl: './allocate-shift.component.html',
  styleUrls: ['./allocate-shift.component.scss']
})
export class AllocateShiftComponent implements OnInit,CanActivateChild  {
  departmentlist: any;
  dataSource;
  displayedColumns = ['name', 'departmentName', 'startDate', 'endDate']
  allocateshift: AllocateShifts;
  userDetails: any;
  shiftsList: any;
  frmDate: any;
  toDate: any;
  Staff: any;
  Staff1: any;
  show = false;
  ClassTeacher: any;
  pipe = new DatePipe('en-US');
  maxDate = new Date();
  minDate = new Date();
  shiftList: any;
  deptUsers: any;
  Date: any;
  bsConfig: Partial<BsDatepickerConfig>;
  colorTheme = 'theme-dark-blue';
  read: any;
  write: any;
  viewAllShits: any;
  constructor(private router:Router,private ls:LoginService,private os: OrganizationService, private toastr: ToastrService, private us: UserService) {
    this.allocateshift = new AllocateShifts()
    this.minDate.setDate(this.minDate.getDate() + 1);
    this.maxDate.setDate(this.maxDate.getDate() + 3650);
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
  }
  @ViewChild("alctshifts") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  addallocateshifts() {
    this.allocateshift.startDate = new Date(this.Date[0]);
    this.allocateshift.endDate = new Date(this.Date[1]);
    this.allocateshift.startDate = this.pipe.transform(this.allocateshift.startDate, 'yyyy-MM-dd');
    this.allocateshift.endDate = this.pipe.transform(this.allocateshift.endDate, 'yyyy-MM-dd');
    this.os.addallocateshifts(this.allocateshift, this.deptUsers).subscribe((data) => {
      if(data.result==true)
      {
        this.toastr.success(data.message)
      }
      else
      {
        this.toastr.error(data.message+' '+data.error)
      }
    })
  }
  getPermissions() {
    this.ls.checkRightPermissions('allocate-shift').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }

  ngOnInit() {
    $(document).ready(function(){
      $("#datahide").hide();
      $("#showdata").click(function(){
         $("#viewbrn").show()
         $("#showdata").hide()
         $("#datahide").show();
      });
      $("#datahide").click(function(){
        $("#viewbrn").hide()
        $("#showdata").show()
        $("#datahide").hide();
     });
  });
    this.getPermissions();
  }
  getIds() {
    this.show = true;
    this.getAllDepartment();
    this.getShifts();
  }
  getAllDepartment() {
    this.os.getAllDepartments().subscribe((data) => {
      this.departmentlist = data.data
    })
  }
  getUsersOfDept() {
    this.os.getUserofDept(this.allocateshift.departmentId).subscribe((data) => {
      this.deptUsers = data.data
    })
  }
  getShifts() {
    this.os.getShiftsByBranch().subscribe((data) => {
      this.shiftList = data.data
    })
  }
  getAllStaffUserss() {
    this.us.getAllStaffUsers().subscribe((data) => {
      this.Staff = data.data;
      for (var i in this.Staff) {
        this.Staff[i].status = false
      }
    });
  }
  getDepartment() {
    this.os.getAllDomains().subscribe((data) => {
      this.departmentlist = data.data
    })
  }
  viewAllocateshifts(){
    this.os.getAllAllcateShifts().subscribe((data)=>{
      this.viewAllShits=data.data;
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
    })
  }
}