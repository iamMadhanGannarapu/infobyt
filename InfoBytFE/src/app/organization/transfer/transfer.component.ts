import { Component, OnInit,ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Transfer } from 'src/app/modal/Organization/transfer';
import { UserService } from '../../service/user.service';
import { OrganizationService } from '../../service/organization.service';
import {MatSort, MatTableDataSource,MatPaginator} from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.scss']
})
export class TransferComponent implements OnInit ,CanActivateChild {
  transfer:Transfer;
  schoolList:any;
  branchList:any;
  allClassList:any;
 showData:boolean;
  allBranchList:any;
  approveStudentList:any;
  displayedColumns=["firstname","classname","mobile","Approve",'Excel']
  displayedColumns1=["firstname","classname","Approve"]
  dataSource;
  dataSource1;
  rolename:any;
  result=false;
  
  write: any;
  read: any;
  constructor(private router:Router,private ls:LoginService,private os:OrganizationService,private toastr:ToastrService,private us:UserService)
   { 
     this.transfer=new Transfer();
   }
   @ViewChild("addtransfer") form: any;
   @ViewChild(MatSort) sort:MatSort;
   @ViewChild(MatPaginator) paginator: MatPaginator;
   addTransfer()
   {
     this.os.saveTransfer(this.transfer.branchId,this.transfer.sessionId).subscribe((data)=>
     {
       if(data)
       {
        this.toastr.success(' Transfer Request Has Been Added Successfully ')
       }
       this.getTrnasfer();
       }, (err) => {
        this.toastr.error(err.error.message)
      });  
   }
   approveTransfer(id:any)
   {
    this.os.saveTransferBranchid(id).subscribe((data)=>
    { 
      if (data)
     {
     this.toastr.success('Successfully TC Approved');
   }   
 }, (err) => {
   this.toastr.error(err.error.message)
 });
   }
   applyFilter(filterValue: string) {
 
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
 getTrnasfer()
  {
    this.os.getAllTransfer().subscribe((res)=>
    {
      this.showData=true
    this.dataSource=new MatTableDataSource(res.data)
    this.dataSource.sort=this.sort
    this.dataSource.paginator=this.paginator
    })
  }
  getexcel(){
    this.os.exportAsExcelFile(this.dataSource.data,'Transfer')
  }
  getAllSchools()
  {
    this.os.getOrganizations().subscribe((data)=>
    {
      this.schoolList=data.data;
    })
  }
getBranches(schoolId:any)
{
  this.os.getAllBranches(schoolId).subscribe((data)=>
  {
    this.allBranchList=data.data;
  })
}
getClasses(branchId:any)
{
  this.os.getAllSessions(branchId).subscribe((data)=>
  {
    this.allClassList=data.data;
  })
}
applyFilter1(filterValue: string) {
 
  this.dataSource1.filter = filterValue.trim().toLowerCase()
}
getApplyStudents()
{
  this.os.getTcTrainees().subscribe((data)=>
  {
    this.dataSource1=new MatTableDataSource(data.data)
    this.dataSource1.sort=this.sort
    this.dataSource1.paginator=this.paginator
  })
}
acceptApply(studentid:any)
{ 
  this.os.acceptTrainee(studentid).subscribe((data)=>
  { 
    if (data)
   {
   this.toastr.success('Request has been Accepted');
 }   
}, (err) => {
 this.toastr.error(err.error.message)
});
}
getApproveStudents()
{
  this.os.approveTrainees().subscribe((data)=>
  {
    this.approveStudentList=data.data[0];
  })
}
getIds()
{
this.getAllSchools()
}
getPermissions() {
  this.ls.checkRightPermissions('transfer').subscribe((data)=>{
    
    this.read=data.data[0].read;
    this.write=data.data[0].write;
    this.canActivateChild(this.read)

  })
}
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }

  ngOnInit():void{
    $(document).ready(function(){
      $("#datahide").hide();
      $("#showdata").click(function(){
         $("#responsive3").show()
         $("#showdata").hide()
         $("#datahide").show();
      });
      $("#datahide").click(function(){
        $("#responsive3").hide()
        $("#showdata").show()
        $("#datahide").hide();
     });

     $("#datahide").hide();
     $("#showdata").click(function(){
        $("#responsive4").show()
        $("#showdata").hide()
        $("#datahide").show();
     });
     $("#datahide").click(function(){
       $("#responsive4").hide()
       $("#showdata").show()
       $("#datahide").hide();
    });

    $("#datahide").hide();
    $("#showdata").click(function(){
       $("#viewreq").show()
       $("#showdata").hide()
       $("#datahide").show();
    });
    $("#datahide").click(function(){
      $("#viewreq").hide()
      $("#showdata").show()
      $("#datahide").hide();
   });

   $("#datahide").hide();
   $("#showdata").click(function(){
      $("#viewaccp").show()
      $("#showdata").hide()
      $("#datahide").show();
   });
   $("#datahide").click(function(){
     $("#viewaccp").hide()
     $("#showdata").show()
     $("#datahide").hide();
  });
  });
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename=data.data[0].rolename;
      }, (err) => {
        this.toastr.error(err.error.message)
      });
    }
    this.getPermissions();
    this.getTrnasfer();
    this.getApplyStudents()
    this.getApproveStudents();
    this.transfer.organizationId=0;
    this.transfer.branchId=0;
    this.transfer.sessionId=0;
  }
}
