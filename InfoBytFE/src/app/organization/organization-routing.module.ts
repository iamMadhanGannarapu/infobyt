import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrganizationComponent } from './organization/organization.component';
import { BranchComponent } from './branch/branch.component';
import { AchievementsComponent } from './achievements/achievements.component';
import { AdmisionDetailsComponent } from './admision-details/admision-details.component';
import { EventsComponent } from './events/events.component';
import { CoursesComponent } from './courses/courses.component';
import { TransferComponent } from './transfer/transfer.component';
import { BranchSessionrComponent } from './branch-session/branch-session.component';
import { RouteGuardGuard, CanDeactivateGuard } from '../service/route-guard.guard';
import { DepartmentComponent } from './department/department.component';
import { ShiftsComponent } from './shifts/shifts.component';
import { AllocateShiftComponent } from './allocate-shift/allocate-shift.component';
import { getCanActivateChild } from '@angular/router/src/utils/preactivation';
const routes: Routes = [
    { path: 'Organization', component: OrganizationComponent,canDeactivate:[CanDeactivateGuard] ,canActivate: [RouteGuardGuard] ,canActivateChild:[OrganizationComponent]},
    { path: 'Branch-session', component: BranchSessionrComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard] ,canActivateChild:[BranchSessionrComponent]},
    { path: 'Branch', component: BranchComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard],canActivateChild:[BranchComponent] },
    { path: 'Achievements', component: AchievementsComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard] ,canActivateChild:[AchievementsComponent]},
    { path: 'Admision-details', component: AdmisionDetailsComponent, canActivate: [RouteGuardGuard] ,canActivateChild:[AdmisionDetailsComponent]},
    { path: 'Events', component: EventsComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard] ,canActivateChild:[EventsComponent]},
    { path: 'Courses', component: CoursesComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard] ,canActivateChild:[CoursesComponent]},
    { path: 'Transfer', component: TransferComponent, canActivate: [RouteGuardGuard] ,canActivateChild:[TransferComponent]},
    { path: 'Department', component: DepartmentComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard] ,canActivateChild:[DepartmentComponent]},
    { path: 'Shifts', component: ShiftsComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard] ,canActivateChild:[ShiftsComponent]},
    { path: 'Allocate-shift', component: AllocateShiftComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard] ,canActivateChild:[AllocateShiftComponent]}
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers:[RouteGuardGuard,CanDeactivateGuard, OrganizationComponent,BranchSessionrComponent,BranchComponent,AchievementsComponent
    ,AdmisionDetailsComponent,EventsComponent,CoursesComponent,TransferComponent,DepartmentComponent,ShiftsComponent,AllocateShiftComponent]
})
export class OrganizationRoutingModule { }
