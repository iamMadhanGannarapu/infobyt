import { Component, OnInit, ViewChild } from '@angular/core';
import { OrganizationService } from '../../service/organization.service';
import { BranchSession } from '../../modal/Organization/BranchSession';
import { BatchService } from '../../service/batch.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../service/user.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-branch-session',
  templateUrl: './branch-session.component.html',
  styleUrls: ['./branch-session.component.scss']
})
export class BranchSessionrComponent implements OnInit ,CanActivateChild {
  dataSource;
  displayedColumns = ['classname', 'medium', 'numofstudents', 'edit', 'Excel']
  branchSession: BranchSession
  classList: any
  branchList: any
  updateBranchclassObj: any
  rolename: any;
  mediumlist: any;
  departmentsList: any;
  read: any;
  write: any;
  show:any;
  constructor(private router:Router,private ls:LoginService,private toastr: ToastrService, private os: OrganizationService, private us: UserService, private bs: BatchService) {
    this.branchSession = new BranchSession()
    this.updateBranchclassObj = new BranchSession()
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("addschoolbranch") form: any;
  ngOnInit() {
    $(document).ready(function(){
      $("#datahide").hide();
      $("#showdata").click(function(){
         $("#viewbrn").show()
         $("#showdata").hide()
         $("#datahide").show();
      });
      $("#datahide").click(function(){
        $("#viewbrn").hide()
        $("#showdata").show()
        $("#datahide").hide();
     });
  });
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename = data.data[0].rolename;
        if (this.rolename == "BranchAdmin") {
        }
      })
    }
    this.getPermissions();
    this.getAllMediums()
    this.getAllBranchClass();
    this.branchSession.sessionId = 0;
    this.branchSession.branchId = 0;
    this.branchSession.medium = '0';
    this.branchSession.departmentid = 0;
  }
  getPermissions() {
    this.ls.checkRightPermissions('branch-session').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }

  getAllBranchClass() {
    this.os.getAllDomains().subscribe((data) => {
      this.departmentsList = data.data
    })
    this.os.getAllBranchSession().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      }
    })
  }
  getexcel() {
    this.os.exportAsExcelFile(this.dataSource.data, 'Branch Session');
  }
  editBranchClass(id: any) {
    this.getIds();
    this.os.getAllBranchSessionById(id).subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.branchSession.Id = data.data[0].id;
      this.branchSession.sessionId = data.data[0].classid;
      this.branchSession.branchId = data.data[0].schoolbranch;
      this.branchSession.medium = data.data[0].medium;
      this.branchSession.numOfTrainees = data.data[0].numoftrainees;
      this.branchSession.departmentid = data.data[0].departmentid;
      }
    });
  }
  getAllClasses() {
    this.bs.getAllSession().subscribe((data) => {
      this.classList = data.data;
    }, (err) => {
      this.toastr.error(err.error.message)
    })
  }
  getSchoolBranchByIds() {
    this.os.getBranchById().subscribe((data) => {
      this.branchList = data.data;
    }, (err) => {
      this.toastr.error(err.error.message)
    })
  }
  getIds() {
    this.getSchoolBranchByIds();
    this.getAllClasses();
  }
  updateBranchClass() {
    this.os.updateBranchSession(this.branchSession).subscribe((data) => {
      this.getAllBranchClass();
      if(data.result)
      this.toastr.success(data.message);
      else
      this.toastr.error(data.message);
    }, (err) => {
      this.toastr.error(err.error.message)
    })
  }
  addBranchClass() {
    this.os.saveBranchSession(this.branchSession).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.form.reset();
      }
      else
      {
        this.toastr.error(data.message);
      }
      this.getAllBranchClass();
    });
  }
  applyFilter(filterValue: string) {
    
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getAllMediums() {
    this.us.getALlMedium().subscribe((data) => {
      this.mediumlist = data.data
    })
  }
}
