import { Component, OnInit, ViewChild } from '@angular/core';
import { Course } from '../../modal/Organization/Courses';
import { Departments } from '../../modal/Organization/Department'
import { OrganizationService } from 'src/app/service/organization.service';
import { BatchService } from 'src/app/service/batch.service';
import { UserService } from 'src/app/service/user.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit,CanActivateChild  {
  courseobject: Course;
  departmentobject: Departments;
  batchlist: any;
  showCourse: boolean = false
  showdepartment: boolean = false
  courselist: any;
  userDetails: any;courseList: any;
  read: any;
  write: any;

  dataSource;
  dataSoursesDept;
  displayedColumns = ['CourseName', 'Duration', 'Semester', 'Level', 'Edit','Excel']
  displayedColumndept = ['CourseName','DepartmentsName', 'Staff']
  deptList: any;
  allStaffList: any;
  constructor(private ls:LoginService,private router: Router,private toastr: ToastrService, private os: OrganizationService, private batch: BatchService, private userService: UserService) {
    this.departmentobject = new Departments();
    this.courseobject = new Course();
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("addCourseForm") form: any;
  @ViewChild("adddepartmentForm") form1: any;
  applyFilter(filterValue: string) {
   
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }

  addcourses() {
    this.os.saveCourses(this.courseobject).subscribe((data) => {
      if(data.result)
      {
        this.toastr.success(data.message)
        this.getall()
        this.form.reset();
      }
      else
      {
        this.toastr.error(data.message+" "+data.error)
      }
    })
  }
  getIds() {
    this.os.getAllCourses().subscribe((data) => {
      this.courselist = data.data;
    })
    this.batch.getAllBatch().subscribe((data) => {
      this.batchlist = data.data;
    })
  }
  addHod(){
    this.router.navigate(['user/add-user/Default']);
  }
  getallhods(){
    this.userService.getAllHod().subscribe((res) => {
      this.allStaffList= res.data;
    })
  }
  getAllStaffUserss() {
    this.userService.getAllStaffUsers().subscribe((data) => {
      this.userDetails = data.data;
    });
  }
  adddepartments() {
    this.os.saveDomain(this.departmentobject).subscribe((data) => {
      this.deptList=data.data
      this.getAllDomains()
      this.form1.reset();
    })
  }
  getall() {
    this.os.getAllCourses().subscribe((data) => {
      this.courseList=data.data
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      this.allStaff()
      this.departmentobject.courseId = 0;
      this.departmentobject.staffId = 0;
    })
  }
  getexcel(){
      this.os.exportAsExcelFile(this.courseList, 'Cources');
}
  getById(id: number) {
    this.os.getCourseById(id).subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.courseobject.Id = data.data[0].id
      this.courseobject.courseName = data.data[0].coursename
      this.courseobject.years = data.data[0].years
      this.courseobject.semesters = data.data[0].semesters
      this.courseobject.level = data.data[0].level
      }
    })
  }
  ngOnInit() {
    this.getall()
    this.getAllDomains();
    this.getallhods();
    this.getPermissions();
  }
  getPermissions() {
    this.ls.checkRightPermissions('courses').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  allStaff() {
    this.userService.getAllStaffUsers().subscribe((data) => {
      this.userDetails = data;
    });
  }
  viewDomainById(id: number) {
    this.getIds()
    this.os.getDomainById(id).subscribe((data) => {
      this.departmentobject.Id = data[0].id
      this.departmentobject.staffId = data[0].staffid
      this.departmentobject.courseId = data[0].courseid
      this.departmentobject.name = data[0].name
    })
  }
  updateDepartments() {
    this.os.updateDomain(this.departmentobject).subscribe((data) => { })
  }
  updateCourse() {
    this.os.updateCourses(this.courseobject).subscribe((data) => {
      if(data.result)
      {
        this.toastr.success(data.message)
        this.getall();
      }
      else
      {
        this.toastr.error(data.message)
      }
    })
  }
  getAllDomains() {
    this.os.getAllDomains().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.dataSoursesDept = new MatTableDataSource(data.data)
      this.dataSoursesDept.sort = this.sort
      this.dataSoursesDept.paginator = this.paginator
      }
    })
  }
  showCourses() {
    this.showCourse = true
    this.showdepartment = false
  }
  showdepartments() {
    this.showCourse = false
    this.showdepartment = true
  }
}