import { Component, OnInit } from '@angular/core';
import { OrganizationService } from '../service/organization.service';
@Component({
  selector: 'app-error-message',
  templateUrl: './error-message.component.html',
  styleUrls: ['./error-message.component.scss']
})
export class ErrorMessageComponent implements OnInit {
  myurl:any
  constructor(private os:OrganizationService) { 
    this.myurl = this.os.url;
    this.myurl= this.myurl+ '/organization/assets/images/error/error.jpg'
  }
  ngOnInit() {
  }
}
