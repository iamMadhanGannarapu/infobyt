import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { ContactDetailsComponent } from './contact-details/contact-details.component';
import { RouteGuardGuard, CanDeactivateGuard } from '../service/route-guard.guard';
var routes: Routes = [
    { path: 'add-user/:roleName', component: UsersComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard] },
    { path: 'LoggedTime', component: UsersComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard] },
    { path: 'ContactDetails', component: ContactDetailsComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard] },
]
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [RouteGuardGuard,CanDeactivateGuard]
})
export class UserRoutingModule { }
