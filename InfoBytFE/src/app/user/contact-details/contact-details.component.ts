import { Component, OnInit, ViewChild } from '@angular/core';
import { ContactDetails } from '../../modal/User/ContactDetails';
import { Router } from '@angular/router';
import { UserService } from '../../service/user.service';
import { Observable, of } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from '../../service/login.service'
@Component({
    selector: 'app-contact-details',
    templateUrl: './contact-details.component.html',
    styleUrls: ['./contact-details.component.scss']
})
export class ContactDetailsComponent implements OnInit {
    contactDetail: ContactDetails
    contactDetails: any;
    usercountryList: any[] = []
    userSession: any
    userProfile: any
    userstateList: any
    usercitiesList: any;
    result = false;
    res = false;
    Show:boolean=false;
    show: boolean=true;
    constructor(private toastr: ToastrService, private router: Router, private us: UserService, private ls: LoginService) {
        this.contactDetail = new ContactDetails()
    }
    @ViewChild("editContactDetails") form: any;
 
    addContactDetails() {
        let user = JSON.parse(sessionStorage.getItem("Users"));
        this.us.saveUser(user).subscribe((data) => {
            this.contactDetail.userId = data.data[0].fn_addusers;
            this.us.saveContactDetails(this.contactDetail).subscribe(() => {
                localStorage.removeItem('Users');
                this.toastr.success('Registred Successfully');
            })
        })
    }
    ngOnInit() {
        if (sessionStorage.getItem('user') != null) {
            this.userSession = sessionStorage.getItem('user')
            if (this.userSession != null) {
                this.viewContactDetails();
            }
        }
        this.getAllContactDetails();
        this.getCountries();
        this.contactDetail.country = 0;
        this.contactDetail.state = 0;
        this.contactDetail.city = 0;
    }
    getAllContactDetails() {
        this.us.getAllContactDetails().subscribe((data) => {
            this.contactDetails = data.data
        });
    }
    getAllContactnum() {
        this.us.getAllContactDetails().subscribe((data) => {
            this.contactDetails = data.data
        });
    }
    getCountries() {
        this.us.getAllCountries().subscribe((data) => {
            this.usercountryList = data.data
        })
    }
    getStates(couId: any): Observable<any> {
        this.us.getAllStates(parseInt(couId)).subscribe((data) => {
            this.userstateList = data.data
        })
        return of(JSON.stringify(this.contactDetails));
    }
    getCities(stateId: number) {
        this.us.getAllCities(stateId).subscribe((data) => {
            this.usercitiesList = data.data
        })
    }
    viewContactDetails() {
        this.us.getUserbySession().subscribe((data) => {
            if (data) {
                this.userProfile = data.data[0];
            }
            do {
                this.result = true;
            } while (data.data[0].contactid.length > 0)
            this.getCountries();
            this.getStates(this.userProfile.country);
            this.getCities(this.userProfile.state);
            this.contactDetail.Id = this.userProfile.contactid;
            this.contactDetail.userId = this.userProfile.contactuserid
            this.contactDetail.country = this.userProfile.country
            this.contactDetail.state = this.userProfile.state
            this.contactDetail.city = this.userProfile.city
            this.contactDetail.place = this.userProfile.place
            this.contactDetail.address = this.userProfile.address
            this.contactDetail.uniqueIdentificationNo = this.userProfile.uniqueidentificationno
            this.contactDetail.email = this.userProfile.email
            this.contactDetail.mobile = this.userProfile.mobile
        })
    }
    moreConatcts() {
        this.contactDetail.userId = this.userProfile.contactuserid;
        this.us.saveContactDetails(this.contactDetail).subscribe(() => {
            this.toastr.success('More Contact added Successfully');
        })
    }
    newcontact() {
        this.res = true;
        this.router.navigate(['ContactDetails']);
        this.userProfile.contactid = null;
        this.form.reset();
    }
    updateContactDetails() {
        this.ls.updateContact(this.contactDetail).subscribe((data) => {
            if (data) {
                this.toastr.success('Successfully Updated Contact Details');
            }
        })
    }
    open(){
        this.Show=true;
        this.show=false;
    }
  openthis(){
      this.show=true
      this.Show=false
  }
}
