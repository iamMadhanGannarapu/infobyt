import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users/users.component';
import { ContactDetailsComponent } from './contact-details/contact-details.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { MatSlideToggleModule } from '@angular/material';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { TypeaheadModule } from 'ngx-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { SalaryModule } from '../salary/salary.module';
import { BatchModule } from '../batch/batch.module';
import { OrganizationModule } from '../organization/organization.module';
import { AttendanceModule } from '../attendance/attendance.module';
import { AppraisalModule } from '../appraisal/appraisal.module';
import { TransportModule } from '../transport/transport.module';
import { FeeModule } from '../fee/fee.module';
import { FileUploadModule } from 'ng2-file-upload';
import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';
import { AutoCompleteModule } from 'primeng/autocomplete';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatStepperModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatSliderModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import { UserRoutingModule } from './user-routing.module';
@NgModule({
  imports: [
    UserRoutingModule,
    MatTableModule, MatTableModule,
    MatSlideToggleModule,
    CdkTableModule,
    CdkTreeModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule, MatFormFieldModule,
    CommonModule,
    CommonModule, ToastrModule.forRoot(),
    FormsModule, DataTablesModule, TypeaheadModule.forRoot(),
    MatStepperModule, MatInputModule, MatIconModule, MatButtonModule, MatSlideToggleModule, BsDatepickerModule.forRoot(),
    SalaryModule, BatchModule, OrganizationModule, AttendanceModule, AppraisalModule, TransportModule, FeeModule, FileUploadModule, AutoCompleteModule
  ],
  declarations: [UsersComponent, ContactDetailsComponent ], providers: [],
  exports: [
    ContactDetailsComponent, RouterModule
  ]
})
export class UserModule { }
