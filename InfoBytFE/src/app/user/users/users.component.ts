import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from "../../modal/User/User";
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../service/user.service';
import { ContactDetails } from '../../modal/User/ContactDetails';
import { LoginService } from '../../service/login.service';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { Observable, of } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  dataSource;
  displayedColumns = ['Name', 'Email', 'Mobile', 'Role', 'Status']
  Emailvalidation
  mobilevalidation
  aadhaarevalidation
  middleNameList: string[];
  placeList: any;
  religionList: string[] = [
    'Hindu',
    'Christian',
    'Sikh',
    'Islam',
    'Jain',
    'Buddhism',
    'Zoroastrianism',
    'Minoritiess'
  ];
  
  isOptional = false;
  userstateList: any
  usercitiesList: any
  usercountryList: any[]
  usr: User
 
  stafflogSource: any;
  noResult = false;
  cdi: ContactDetails
  sub: any;
  religion: string[]
  userProfile: any
  maxDate = new Date();
  minDate = new Date();
  colorTheme = 'theme-dark-blue';
  isLinear = true;
  nationalitiesList: string[];
  occupationList: string[];
  studentlogSource: any;
  stafflogColumn = ['userName', 'rolename', 'loggedtime']
  studentlogColumn = ['userName', 'rolename', 'loggedtime', 'Excel']
  reportColumns = ['feeamount', 'concession', 'PaidAmount', 'due']
  bsConfig: Partial<BsDatepickerConfig>;
  userreligionList: any[] = [];
  constructor(private toastr: ToastrService, private router: Router, private route: ActivatedRoute, private us: UserService, private ls: LoginService) {
    this.usr = new User();
    this.cdi = new ContactDetails();
    this.minDate.setFullYear(this.maxDate.getFullYear() - 80);
    this.maxDate.setFullYear(this.maxDate.getFullYear() - 20);
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("adduserForm") form: any;
  @ViewChild("addcontactdetails") form1: any;
  addUser() {
    this.usr.fatherName = 'NA';
    this.usr.motherName = 'NA';
    this.usr.fatherOccupation = '0';
    localStorage.setItem("Users", JSON.stringify(this.usr));
    this.toastr.success('Redirecting to Contact Page');
  }
  getAllPlaces() {
    this.ls.getAllLocationNames(this.cdi.city).subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.placeList = data.data;
      }
    });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getSchoolAdmins() {
    this.us.getSchoolAdmins().subscribe((data) => {
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
    }
    )
  }
  getexcel() {
    this.us.exportAsExcelFile(this.dataSource, 'Users')
  }
  onChange(status, id) {
    this.us.getLoginbyUserid(id, status.checked).subscribe((data) => {
      this.getSchoolAdmins();
    })
  }
  getCountries() {
    this.us.getAllCountries().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.usercountryList = data.data
      }
    })
  }
  getStates(couId: any): Observable<any> {
    this.us.getAllStates(parseInt(couId)).subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.userstateList = data.data
      }
    })
    return of(JSON.stringify(this.userstateList));
  }
  getCities(stateId: number) {
    this.us.getAllCities(stateId).subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.usercitiesList = data.data
      }
    })
  }
  onRegstr() {
    this.router.navigate(['Login']);
  }
  addContactDetails(addcontactdetails: any) {
    let user = JSON.parse(localStorage.getItem("Users"));
    this.us.saveUser(user).subscribe((data) => {
      this.cdi.userId = data.data.fn_addusers;
      this.us.saveContactDetails(this.cdi).subscribe((data) => {
        this.toastr.success('Registered Successfully')
        localStorage.removeItem('Users');
        if (user.role == 'Default') {
          this.router.navigate(['batch/Staff']);
        }
        if (user.role == 'BranchAdmin') {
          this.router.navigate(['organization/Branch'])
        }
        if (user.role == 'Trainee') {
          this.router.navigate(['organization/Admision-details'])
        }
      })
    })
  }
  getreligion() {
    this.us.getAllReligion().subscribe((data) => {
      if(data.result && data.data!='NDF')
      this.userreligionList = data.data
    })
  }
  getOccupations() {
    this.us.getAllOccupations().subscribe((data) => {
      if(data.result && data.data!='NDF')
      this.occupationList = data.data
    })
  }
  getNationalities() {
    this.us.getAllNationalities().subscribe((data) => {
      if(data.result && data.data!='NDF')
      this.nationalitiesList = data.data
    })
  }
  verifyEmail(inp: string) {
    this.ls.verifyUserData(inp, this.cdi.email).subscribe((data) => {
      this.Emailvalidation = data.data;
      if (this.Emailvalidation != 'NDF') {
        this.toastr.error('Email Already Exits')
        this.cdi.email = '';
      }
    });
  }
  verifyMobile(inp: string, inpData: any) {
    this.ls.verifyUserData(inp, inpData.value).subscribe((data) => {
      this.mobilevalidation = data.data
      if (this.mobilevalidation != 'NDF') {
        this.toastr.error('Mobile Number Already Exits..');
        this.cdi.mobile = '';
      }
    });
  }
  verifyAuthencation(inp: string, inpData: any) {
    this.ls.verifyUserData(inp, inpData.value).subscribe((data) => {
      this.aadhaarevalidation=data.message
      if(this.aadhaarevalidation=='UniqueIdentificationNo already Exists!!'){
        this.toastr.error('Unique IdentificationNo Already Exits')
this.cdi.uniqueIdentificationNo=''
      }
    });
  }
  viewrole() {
    this.us.getUserbySession().subscribe((data) => {
      if (data) {
        this.userProfile = data.data[0];
      }
    })
  }
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.usr.role = params['roleName'];
      this.usr.gender = 'm';
      this.cdi.country = 0;
      this.cdi.state = 0;
      this.cdi.city = 0;
      this.usr.religion = '0';
      this.usr.nationality = '0';
      this.usr.fatherOccupation = '0'
    });
    this.viewrole()
    this.getCountries();
    this.getMiddleNames();
    this.getNationalities();
    this.getOccupations();
    this.getSchoolAdmins();
    this.getreligion();
  }
  getMiddleNames() {
    this.us.getAllMiddleNames().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.middleNameList = data.data
      }
    })
  }
  onKeydown(event) {
    if (this.cdi.place != '' || this.cdi.place != null) {
      this.cdi.place = this.cdi.place.replace('/[0-9]/g', '');
      if ((event.keyCode > 64 && event.keyCode < 91) || (event.keyCode > 96 && event.keyCode < 123)) { }
      else { this.cdi.place = this.cdi.place.replace(event.key, ''); }
    }
  }
}