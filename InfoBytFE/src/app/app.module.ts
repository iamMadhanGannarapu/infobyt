import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'
import { UserModule } from './user/user.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatStepperModule, MatIconModule, MatInputModule, MatButtonModule } from '@angular/material';
import { RatingModule, BsDatepickerModule, CarouselModule } from 'ngx-bootstrap';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AboutusComponent } from './aboutus/aboutus.component';
import { AgmCoreModule } from '@agm/core'
import { AgmDirectionModule } from 'agm-direction';
import { ErrorMessageComponent } from './error-message/error-message.component'
import { ImageCompressService, ResizeOptions } from 'ng2-image-compress';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { OrganizationChartModule } from 'primeng/organizationchart';
import { MatSlideToggleModule } from '@angular/material';
import { ParticlesModule } from 'angular-particle';
import { GalleriaModule } from 'primeng/galleria';
import { CarouselModule as pCarouselModule } from 'primeng/carousel';
import { DropdownModule } from 'primeng/dropdown';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { DragDropModule } from '@angular/cdk/drag-drop';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatSliderModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import { ServicesComponent } from './services/services.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { SocialLoginModule } from 'angularx-social-login';
import { AuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider, LinkedInLoginProvider } from 'angularx-social-login';
const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('69801110571-5knbudpa5jaihg2jk95s3qmpg3m8p75m.apps.googleusercontent.com')
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('2023787534355464')
  },
  {
    id: LinkedInLoginProvider.PROVIDER_ID,
    provider: new LinkedInLoginProvider('786po9a2snfhpj')
  }
]);
export function provideConfig() {
  return config;
}
var appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'About', component: AboutusComponent },
  { path: 'Services', component: ServicesComponent },
  { path: '404', component: ErrorMessageComponent },
]
@NgModule({
  declarations: [
    AppComponent, HomeComponent, AboutusComponent, ErrorMessageComponent, ServicesComponent
  ],
  imports: [
    SocialLoginModule,
    AppRoutingModule,
    ScrollDispatchModule,
    DragDropModule,
    MatTableModule, MatTableModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatSlideToggleModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    OrganizationChartModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    DropdownModule,
    MatTreeModule, MatFormFieldModule,
    BrowserModule, UserModule, CommonModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule, FormsModule, CarouselModule.forRoot(),
    BrowserAnimationsModule, MatStepperModule, MatInputModule, MatIconModule, MatButtonModule, RatingModule, BsDatepickerModule.forRoot(), ToastModule,
    AgmCoreModule.forRoot({ apiKey: '' }),
    AgmDirectionModule,
    ParticlesModule, GalleriaModule, pCarouselModule
  ],
  exports: [
    RouterModule
  ],
  providers: [{
    provide: AuthServiceConfig,
    useFactory: provideConfig
  }, MessageService, ImageCompressService, ResizeOptions],
  bootstrap: [AppComponent]
})
export class AppModule { }