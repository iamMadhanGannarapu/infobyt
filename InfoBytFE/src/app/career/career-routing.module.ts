import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllocateProjectComponent } from './allocate-project/allocate-project.component';
import { AllocatePlacementComponent } from './allocate-placement/allocate-placement.component';
import { RecruitmentComponent } from './recruitment/recruitment.component';
import { RouteGuardGuard, CanDeactivateGuard } from '../service/route-guard.guard';
import { PlacementsComponent } from './placements/placements.component';
import { ProjectComponent } from './project/project.component';
import { JobComponent } from './job/job.component';
const routes: Routes = [
  { path: 'Allocate-project', component: AllocateProjectComponent, canActivate: [RouteGuardGuard],canActivateChild:[AllocateProjectComponent],canDeactivate:[CanDeactivateGuard] },
    { path: 'Allocate-placement', component: AllocatePlacementComponent, canActivate: [RouteGuardGuard],canActivateChild:[AllocatePlacementComponent] },
    { path: 'Recruitment', component: RecruitmentComponent,canDeactivate:[CanDeactivateGuard] ,canActivate: [RouteGuardGuard],canActivateChild:[RecruitmentComponent] },
    { path: 'Job', component: JobComponent,canDeactivate:[CanDeactivateGuard] ,canActivate: [RouteGuardGuard],canActivateChild:[JobComponent] },
    { path: 'Placements', component: PlacementsComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard],canActivateChild:[PlacementsComponent] },
    { path: 'Project', component: ProjectComponent,canDeactivate:[CanDeactivateGuard] ,canActivate: [RouteGuardGuard],canActivateChild:[ProjectComponent] }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [RouteGuardGuard,AllocateProjectComponent,PlacementsComponent,AllocatePlacementComponent,CanDeactivateGuard,RecruitmentComponent,ProjectComponent,JobComponent]
})
export class CareerRoutingModule { }
