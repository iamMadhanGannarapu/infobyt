

import { Component, OnInit, ViewChild } from '@angular/core';

import { Recruitment, SelectedApplicants } from 'src/app/modal/Career/recruitment';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/service/user.service';
import { OrganizationService } from '../../service/organization.service';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { requestRecruitment } from 'src/app/modal/Career/requestRecruitment';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { CareerService } from "src/app/service/career.service";
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-recruitment',
  templateUrl: './recruitment.component.html',
  styleUrls: ['./recruitment.component.scss']
})
export class RecruitmentComponent implements OnInit,CanActivateChild{
  offerLetterSource: any;
  
  shiftList: any;
  rolename: any;
  userProfile: any;
  specificRatingSource;
  AllratingSource;
  
  allApplicantsRatings;
  agreedSource: any;
  dataSource1;
  showApplicantsRating = false;
  interestedApplicantsList;
  recruitmentAddObj: any
  data: any;
  
  ApplicantsById: any[] = [];
  
  myurl: string
  requestAddObj: any
 
  
  dataSource;
  maxDate = new Date();
  minDate = new Date();
  filterdApplicants: any;
  colorTheme = 'theme-dark-blue';
  bsConfig: Partial<BsDatepickerConfig>;
  displayedColumns = ['companyid', 'position', 'package', 'details', 'interviewdate', 'interviewtime', 'address', 'Status', 'sts']
  intrestSource;
  displayedColumns1 = ['firstname', 'jobtittle', 'position', 'jobapplicantsstatus', 'location', 'email','option'];
  displayedColumns2 = ['jobtittle', 'ApplicantsName', 'email', 'interviewstatus', 'Send'];
  displayedColumns3 = ['jobapplicantsstatus', 'jobtittle', 'designation', 'location', 'email', 'firstname', 'lasttname'];
  displayedColumns4 = ['jobtittle', 'annualctc', 'location', 'joiningdate', 'email', 'starttime', 'endtime', 'acknowledgement'];
  IntrestList: any
  
  showinterestedcandidate = false;
  show1:boolean=true;
  show2:boolean=true;
  read: any;
  write: any;
  permissionList: any;
  
  viewAggredApplicants: boolean;
  dataSource2: MatTableDataSource<{}>;
  viewSelectedApplicant: boolean;
  constructor(private router:Router,  private ls:LoginService,private os: OrganizationService, private toastr: ToastrService, private us: UserService,private cs:CareerService) {
    this.recruitmentAddObj = new Recruitment();
    this.filterdApplicants = new SelectedApplicants();
    this.requestAddObj = new requestRecruitment()
    this.myurl = us.url;
    this.maxDate.setDate(this.maxDate.getDate() + 365);
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("frm") form: any;
  @ViewChild("addApplicantsAggrement") form1: any;
  addRecruitment() {
    this.cs.saveRecrtmntApplication(this.recruitmentAddObj).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
      }
      else
      this.toastr.error(data.message)
    })
  }

  getAggredApplicants() {
    this.show2=!this.show2;
    
    this.cs.getCandidateForSendingOffer().subscribe((data) => {
      
      
      
      this.showinterestedcandidate = true;
      this.showApplicantsRating = false;
      this.viewAggredApplicants=!this.viewAggredApplicants;
      this.dataSource2 = new MatTableDataSource(data.data);
      this.dataSource2.sort = this.sort
      this.dataSource2.paginator = this.paginator
    })
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
  fileChangeEvent(event) {
    if (event.target.files && event.target.files[0]) {
      var rd = new FileReader();
      rd.readAsText(event.target.files[0]);
      rd.onload = (ev: any) => {
        this.data = ev.target.result;
        this.recruitmentAddObj.fileUpload = rd.result
      }
      rd.readAsText(this.recruitmentAddObj.fileUpload)
    }
  }
  getAllApplications() {
    this.cs.getAllRecrtmntApplication().subscribe((data) => {
      
    })
  }
  getApplicationById(id: number) {
    this.cs.getRecrtmntApplicationById(id).subscribe((data) => {
      this.ApplicantsById = data.data
    })
  }
  sendAcceptance() {
    this.requestAddObj.userId = this.ApplicantsById[0].userid
    this.cs.sendRecruitmentRequest(this.requestAddObj).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
      }
      else
  this.toastr.error(data.message)
    })
  }
  RecruitmentStatus(requestid: any, status: any) {
    this.cs.getRequestRecruitmentById(requestid).subscribe((data) => {
      this.requestAddObj.Id = data[0].id;
      this.requestAddObj.Status = status.value;
      this.cs.updateRecruitmentRequest(this.requestAddObj).subscribe((data) => {
       if(data.result)
       this.toastr.success(data.message)
       else
       this.toastr.error(data.message)
      })
    })
    this.ngOnInit();
  }
  OfferLetter(id: any,uid:any) {
    this.cs.sendOfferLetter(id,uid).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
      }
      else
      this.toastr.error(data.message);
    })
  }
  allApplicantsRating() {
    this.show1=!this.show1;
    this.cs.allRatings().subscribe((data) => {
      this.allApplicantsRatings = data.data;
      this.showApplicantsRating = true;
      this.showinterestedcandidate = false;
      this.viewSelectedApplicant=!this.viewSelectedApplicant;
      this.AllratingSource = new MatTableDataSource(data.data)
      this.AllratingSource.sort = this.sort
      this.AllratingSource.paginator = this.paginator
    })
  }
  applicantRating() {
    this.cs.ratings().subscribe((data) => {
      
      this.specificRatingSource = new MatTableDataSource(data.data)
      this.specificRatingSource.sort = this.sort
      this.specificRatingSource.paginator = this.paginator
    })
  }
  
  getShiftBranchById() {
    this.os.getShiftsByBranch().subscribe((data) => {
      this.shiftList = data.data;
    }, (err) => {
      this.toastr.error(err.error.message)
    })
  }
  getSelectedApplicant(id, rId) {
    this.filterdApplicants.applicantId = id;
    this.filterdApplicants.recruitId = rId;
  }
  addAggrementsDetails() {
    this.cs.saveAggrements(this.filterdApplicants).subscribe((res) => {
      if (res.result) {
        this.form.reset();
        this.toastr.success(res.message);
      }
      else
      this.toastr.error(res.message);
    },
      (err) => {
        this.toastr.error(err.error.message)
      })
  }
  getPermissions() {
    this.ls.checkRightPermissions('recruitment').subscribe((data)=>{
      this.permissionList=data.data;
      this.read=this.permissionList[0].read;
      this.write=this.permissionList[0].write;
      this.canActivateChild(this.read)
    })
  }
  canActivateChild(x:any):boolean{
    if(x==true){
      return true;
    }else{
      this.router.navigate(['404']);
        return false;
    }
      }
  ngOnInit() {
    this.getPermissions();
    this.getAgreedList();
    this.getAllApplications();
    this.getInterestedApplicants();
    this.getAllIntrsted();
    this.applicantRating();
    this.getShiftBranchById();
    this.getOfferLetter();
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.userProfile = data.data[0];
        this.rolename = this.userProfile.rolename;
      }, (err) => {
        this.toastr.error(err.error.message)
      });
    }
  }
  sendLois(userId: any, id: any) {
    this.cs.sendLoi(userId, id).subscribe((data) => {
    })
  }
  getAllIntrsted() {
    this.cs.getAllintrst().subscribe((data) => {
    })
  }
  getAgreedList() {
    this.cs.getAllAgreedList().subscribe((data) => {
      
      this.agreedSource = new MatTableDataSource(data.data)
      this.agreedSource.sort = this.sort
      this.agreedSource.paginator = this.paginator
    })
  }
  getInterestedApplicants() {
    this.cs.getApplicantsforinterview().subscribe((data) => {
      this.interestedApplicantsList = data.data
      this.showinterestedcandidate = true;
      this.showApplicantsRating = false;
      this.dataSource1 = new MatTableDataSource(data.data);
      this.dataSource1.sort = this.sort
      this.dataSource1.paginator = this.paginator
    })
  }
  Acceptloi(id: any, status: any) {
    this.cs.acceptLoi(id, status.value).subscribe((data) => {
    })
  }
  DocVerification() {
    this.cs.addDocVerification(this.IntrestList).subscribe((data) => {
    })
  }
  
  getOfferLetter() {
    this.cs.gettingOfferLetter().subscribe((data) => {
      
      this.offerLetterSource = new MatTableDataSource(data.data);
      this.offerLetterSource.sort = this.sort
      this.offerLetterSource.paginator = this.paginator
    })
  }
  showInteresetInJoining(id) {
    this.cs.respondingForTheOffer(id).subscribe((data) => {
    })
  }
  sendOfferLetterToAll(event: Event, rid: any) {
    var id = parseInt(rid)
    if (event.returnValue) {
      this.cs.respondingForTheOffer(id).subscribe((data) => {
      })
    }
  }
}
