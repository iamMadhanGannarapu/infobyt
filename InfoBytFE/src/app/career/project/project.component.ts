
import { Component, OnInit, ViewChild } from '@angular/core';
import { project } from '../../modal/Career/project';
import { OrganizationService } from '../../service/organization.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { UserService } from '../../service/user.service'
import { CareerService } from "src/app/service/career.service";
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit,CanActivateChild {
  displayedColumns = ['projectName', 'assignedDate', 'submissionDate', 'type', 'details', 'teamSize', 'fee', 'category', 'edit', 'Excel']
  dataSource;
  proj: project
  maxDate = new Date();
  minDate1: any = new Date();
  minDate = new Date();
  colorTheme = 'theme-dark-blue';
  bsConfig: Partial<BsDatepickerConfig>;
  pipe = new DatePipe('en-US');
  branchList: any;
  
  
  
  read: any;
  write: any;
  constructor(private router:Router,  private ls: LoginService, private us: UserService, private os: OrganizationService, private toastr: ToastrService, private cs: CareerService) {
    this.proj = new project();
    this.minDate1.setDate(this.minDate1.getDate() + 1);
    this.minDate.setDate(this.minDate.getDate() + 2);
    this.maxDate.setMonth(this.maxDate.getMonth() + 6);
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
  }
  daterange() {
    this.minDate.setDate(this.proj.assignedDate.getDate() + 1);
  }
  @ViewChild("addproject") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  addProject() {
    this.cs.saveProject(this.proj).subscribe((data) => {
      this.getProjectByBranch()
      if (data.message) {
        this.form.reset();
        this.toastr.success(data.message)
      }
      else
        this.toastr.error(data.message)
    })
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
  getProjectByBranch() {
    this.cs.getprjbybranch().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      }
    })
  }
  getexcel() {
    this.cs.exportAsExcelFile(this.dataSource.data, 'Projects');
  }
  getIds() {
    this.os.getBranchById().subscribe((data) => {
      this.branchList = data.data;
    }, (err) => {
      this.toastr.error(err.error.message)
    })
    this.proj.branchId = 0;
  }
  datesubmission() {
    this.minDate.setDate(this.proj.assignedDate.getDate() + 1);
    this.minDate.setMonth(this.proj.assignedDate.getMonth());
    this.minDate.setFullYear(this.proj.assignedDate.getFullYear());
  }
  dateassigned() {
    this.minDate1.setDate(this.minDate1.getDate() + 1);
  }
  getSchoolBranchByIds() {
    this.os.getBranchById().subscribe((data) => {
      this.branchList = data.data;
    }, (err) => {
      this.toastr.error(err.error.message)
    })
  }
  editProjects(id: any) {
    this.getIds();
    this.cs.getProjectsById(id).subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
    
      this.proj.Id = data.data[0].id;
      this.proj.branchId = data.data[0].branchid;
      this.proj.projectName = data.data[0].projectname;
      this.proj.assignedDate = this.pipe.transform(data.data[0].assigneddate, 'MM-dd-yyyy');
      this.proj.submissionDate = this.pipe.transform(data.data[0].submissiondate, 'MM-dd-yyyy');
      this.proj.type = data.data[0].type;
      this.proj.details = data.data[0].details;
      this.proj.teamSize = data.data[0].teamsize;
      this.proj.fee = data.data[0].fee;
      this.proj.category = data.data[0].category;
      }
    })
  }
  updateProject() {
    this.cs.updProject(this.proj).subscribe((data) => {
      if(data.result)
      this.toastr.success(data.message);
      else
      this.toastr.error(data.message)
      this.getProjectByBranch()
    });
  }

  getPermissions() {
    this.ls.checkRightPermissions('project').subscribe((data) => {
      
      this.read = data.data[0].read;
      this.write = data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  
  canActivateChild(x:any):boolean{
    if(x==true){
      return true;
    }else{
      this.router.navigate(['404']);
        return false;
    }
      }
  ngOnInit() {
    $(document).ready(function(){
      $("#datahide").hide();
      $("#showdata").click(function(){
         $("#viewprj").show()
         $("#showdata").hide()
         $("#datahide").show();
      });
      $("#datahide").click(function(){
        $("#viewprj").hide()
        $("#showdata").show()
        $("#datahide").hide();
     });
  });
    this.getPermissions();
    this.getProjectByBranch();
    this.proj.branchId = 0;
  }
}


