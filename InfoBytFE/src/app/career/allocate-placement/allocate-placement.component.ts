
import { Component, OnInit, ViewChild } from '@angular/core';
import { BatchService } from 'src/app/service/batch.service';
import { AllocatePlacement } from 'src/app/modal/career/AllocatePlacement';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { UserService } from 'src/app/service/user.service';
import { CareerService } from "src/app/service/career.service";
import { LoginService } from 'src/app/service/login.service';
import { ToastrService } from 'ngx-toastr';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-allocate-placement',
  templateUrl: './allocate-placement.component.html',
  styleUrls: ['./allocate-placement.component.scss']
})
export class AllocatePlacementComponent implements OnInit,CanActivateChild {
  dataSource;
  displayedColumns = ['StudentName', 'CompanyName', 'Designation', 'StartTime', 'Package', 'edit', 'Excel']
  batchesList: any[] = [];
  plcList: any;
  alplc: AllocatePlacement
 
  traineeList: any;
  id: any;
  
  PlacementList: any;
  placList: any;
  permissionList: any
  read: any;
  write: any;
  constructor(private router:Router,private ls: LoginService, private toastr: ToastrService, private bs: BatchService, private us: UserService, private cs: CareerService) {
    this.alplc = new AllocatePlacement();
  }
  @ViewChild("frm") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  allocPlac(i: any) {
    this.alplc.traineeId = i;
    this.cs.allocatePlac(this.alplc).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.placList = data.data;
      }
      
      else
        this.toastr.error(data.message);
      this.getAllTraineesAllocated();
    })
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
  getAllBatches() {
    this.bs.getAllBatch()
      .subscribe((data) => {
        this.batchesList = data.data;
      })
  }
  getTraineeByBatch(ddlBatchId) {
    this.cs.traineebybatch(ddlBatchId.value).subscribe((data) => {
      this.traineeList = data.data
    })
  }
  getPlacementtByBranch() {
    this.cs.getPlacements().subscribe((data) => {
      this.plcList = data.data;
    })
  }
  editAllocPlac(id: any) {
    this.cs.getAllocPlcId(id).subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.alplc.Id = data.data[0].id;
      this.alplc.placementId = data.data[0].projectid;
      this.alplc.traineeId = data.data[0].studentid;
      }
    })
  }
  updAllocPlac() {
    this.cs.updAllocPlc(this.alplc).subscribe((data) => {
      if (data.result) {
        this.PlacementList = data.data;
        this.toastr.error(data.message);
      }
      else {
        this.toastr.error(data.message);
      }
      this.getAllTraineesAllocated();
    })
  }
  getAllTraineesAllocated() {
    this.cs.traineesAllocatedPlc().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      }
    })
  }
  getexcel() {
    this.bs.exportAsExcelFile(this.dataSource.data, 'Allocate placements');
  }
  getPermissions() {
    this.ls.checkRightPermissions('allocate-placement').subscribe((data) => {
      this.permissionList = data.data;
      this.read = this.permissionList[0].read;
      this.write = this.permissionList[0].write;
    })
    this.canActivateChild(this.read)
  }
  

  canActivateChild(x:any):boolean{
    if(x==true){
      return true;
    }else{
      this.router.navigate(['404']);
        return false;
    }
      }
  ngOnInit() {
    this.getPermissions();
    this.getAllBatches();
    this.getAllTraineesAllocated()
    this.getPlacementtByBranch();
    this.alplc.batchId = 0;
    this.alplc.placementId = 0;
  }
}

