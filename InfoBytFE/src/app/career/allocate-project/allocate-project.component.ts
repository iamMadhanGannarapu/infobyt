
import { Component, OnInit, ViewChild } from '@angular/core';
import { BatchService } from 'src/app/service/batch.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';

import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/service/user.service';
import { CareerService } from "src/app/service/career.service";
import { AllocateProject } from 'src/app/modal/Career/AllocateProject';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-allocate-project',
  templateUrl: './allocate-project.component.html',
  styleUrls: ['./allocate-project.component.scss']
})
export class AllocateProjectComponent implements OnInit, CanActivateChild{
  dataSource;
  displayedColumns = ['StudentName', 'ProjectName','ProjectType','AssignedDate','SubmissionDate','TeamSize','ProjectFee']
  listSource;
  listColumns = ['name','modify','Excel']
  batchesList: any[]=[];
  prjList: any;
  alproj:AllocateProject
  
  viewprojectallocation:boolean;

  
  traineeList: any;
  
  
  permissionList:any
  read: any;
  write: any;
  teamGetList:any;
  show1:boolean=true;
  selectList:any;
  showFilter:any='';
  
  constructor( 
    private router:Router, private ls:LoginService,private cs:CareerService,private toastr: ToastrService,private bs:BatchService,private us:UserService) {
    this.alproj=new AllocateProject();
   }
   @ViewChild("frm") form: any;
   @ViewChild(MatSort) sort: MatSort;
   @ViewChild(MatPaginator) paginator: MatPaginator;
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
  getAllBatches() {
    this.bs.getAllBatch()
    .subscribe((data) => {
      this.batchesList=data.data;
    })
  }
getStudentByBatch(ddlBatchId){
   this.cs.traineebybatch(ddlBatchId.value).subscribe((data)=>{
    this.traineeList=data.data
    for(var i=0;i<this.traineeList.length;i++)
    {
      this.traineeList[i].checkboxStatus=false
    }
    this.listSource = new MatTableDataSource(this.traineeList)
    this.listSource.sort = this.sort
    this.listSource.paginator = this.paginator
  }) 
}
getTeamMembersByteam(ddlBatchId){
  this.bs.traineemembersByTeam(ddlBatchId.value).subscribe((data)=>{
    this.traineeList=data.data
    for(var i=0;i<this.traineeList.length;i++)
    {
      this.traineeList[i].checkboxStatus=false
    }
    this.listSource = new MatTableDataSource( this.traineeList)
    this.listSource.sort = this.sort
    this.listSource.paginator = this.paginator
  }) 
}
  getProjectByBranch() {
    this.cs.getprjbybranch().subscribe((data) => {
    this.prjList=data.data; 
      })
  }
  
  updAllocProj(){
    this.cs.updAllocProj(this.alproj).subscribe((data)=>{
      
      this.getAllStudentsAllocated();
      if(data.result)
      this.toastr.success(data.message)
      else
      this.toastr.error(data.message)
    })
  }
  getAllStudentsAllocated(){
    this.cs.traineesAllocatedPrj().subscribe((data)=>{
      if(data.result && data.data!='NDF')
      {
      
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      }
    })
  }
  getexcel(){
    this.cs.traineesAllocatedPrj().subscribe((data)=>{
      this.bs.exportAsExcelFile(data.data, 'sample');
    })
}
getPermissions() {
  this.ls.checkRightPermissions('allocate-project').subscribe((data)=>{
    this.permissionList=data.data;
    this.read=this.permissionList[0].read;
    this.write=this.permissionList[0].write;
    this.canActivateChild(this.read)
  })
}
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }
getAllTeam() {
  this.bs.getAllTeam().subscribe((res) => {
    this.teamGetList = res.data;
  });
}
showBatch() {
  if (this.showFilter == 'Batch') {
    this.selectList = this.batchesList
  }
  else {
    this.showFilter=='Team'
    this.selectList = this.teamGetList
  }
}
addBulkMembers(){
  this.cs.allocateProj(this.alproj,this.traineeList).subscribe((data)=>{
    this.getAllStudentsAllocated();
    if(data.result){
      this.toastr.success(data.message)
this.form.reset()
    }
    else
    this.toastr.error(data.message)
  })
}
  ngOnInit() {
    this.getPermissions();
    this.getAllBatches();
    this.getAllStudentsAllocated()
    this.getProjectByBranch();
    this.getAllTeam()
    this.alproj.projectId=0;
    this.alproj.batchId=0;
  }
  viewProjectAllocation() {
    this.show1=!this.show1;
    this.viewprojectallocation = !this.viewprojectallocation
  }
}


