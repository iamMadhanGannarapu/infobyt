
import { Component, OnInit,ViewChild } from '@angular/core';
import { OrganizationService } from '../../service/organization.service';
import { placement } from '../../modal/Career/placement';
import { ToastrService } from 'ngx-toastr';
import {MatSort, MatTableDataSource,MatPaginator} from '@angular/material';
import { DatePipe } from '@angular/common';
import { CareerService } from "src/app/service/career.service";
import{UserService} from '../../service/user.service'
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-placements',
  templateUrl: './placements.component.html',
  styleUrls: ['./placements.component.scss']
})
export class PlacementsComponent implements OnInit,CanActivateChild {
  jobSpecOrg: any;
  placemnt:placement;
  branchList: any;
  
  dataSource;
  pipe = new DatePipe('en-US');
  
  date:any;
  AllPostedJobs:any;
  displayedColumns=['companyname','designation','package','positions','startTime','endTime','edit']
  
  permissionList: Object;
  read: any;
  write: any;
  constructor(private router:Router,private ls:LoginService,private us: UserService, private os:OrganizationService,private toastr: ToastrService ,private cs:CareerService) {
    this.placemnt=new placement();
   }
   @ViewChild('placement')form:any
   @ViewChild(MatSort) sort:MatSort;
   @ViewChild(MatPaginator) paginator: MatPaginator;
   addPlacements()
   {
    this.cs.savePlacement(this.placemnt).subscribe((data)=>
      {    
        if (data.result) 
        {       
        this.toastr.success(data.message)   
        this.form.reset();
        }
        else
        {
          this.toastr.error(data.message)
        }
        this.getAllPlacements();
       });
    }
    applyFilter(filterValue: string) {
      filterValue = filterValue.trim();
      filterValue = filterValue.toLowerCase();
      this.dataSource.filter = filterValue;
    }
   getIds(){
   this.getSchoolBranchByIds();
   this.getJobsByInstitution();
 }
 getJobsByInstitution() {
   this.cs.getJobByInstitutionId().subscribe((data) => {
  this.jobSpecOrg=data.data;
   })
   }
 editPlacement(Id:number)
 {
   this.getIds();
   this.cs.getbyPlacementId(Id).subscribe((data)=>
   {
    if(data.result && data.data!='NDF')
    {
     this.placemnt.Id=data.data[0].id 
      this.placemnt.branchId=data.data[0].branchid;
      this.placemnt.jobId=data.data[0].jobid;
      this.placemnt.designation=data.data[0].designation;
      this.placemnt.details=data.data[0].details;
      this.placemnt.endTime=this.pipe.transform(data.data[0].endtime,'MM-dd-yyyy HH:mm:ss');
      this.placemnt.package=data.data[0].package;
      this.placemnt.positions=data.data[0].positions;
    this.placemnt.startTime=this.pipe.transform(data.data[0].starttime,'MM-dd-yyyy HH:mm:ss');
    }
    })
 }
updatePlacements()
 {
   this.cs.updatePlacement(this.placemnt).subscribe((data)=>
   {
     if(data.result)
    this.toastr.success(data.message);
    else
    this.toastr.error(data.message)
    this.getAllPlacements();
  });
} 
 getAllPlacements(){
  this.cs.getPlacements().subscribe((data)=>{
    if(data.result && data.data!='NDF')
      {
    
    this.dataSource=new MatTableDataSource(data.data)
    this.dataSource.sort=this.sort
    this.dataSource.paginator=this.paginator
      }
  })
}
availJob(id:number){
  this.cs.postedJobsOfAllCompanies(id).subscribe((data)=>{
this.AllPostedJobs=data.data[0];
this.placemnt.designation=data.data[0].designation;
this.placemnt.package=data.data[0].annualctc;
this.placemnt.positions=data.data[0].vacancy;
  })
}
 getSchoolBranchByIds() {
  this.os.getBranchById().subscribe((data) => {
    this.branchList = data.data;
    this.placemnt.branchId=data.data[0].branchid;
  })
  this.placemnt.branchId=0;
}
getPermissions() {
  this.ls.checkRightPermissions('placements').subscribe((data)=>{
    this.permissionList=data.data;
    this.read=this.permissionList[0].read;
    this.write=this.permissionList[0].write;
    this.canActivateChild(this.read)
  })
}
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }
  ngOnInit() {
    $(document).ready(function(){
      $("#datahide").hide();
      $("#showdata").click(function(){
         $("#viewplcm").show()
         $("#showdata").hide()
         $("#datahide").show();
      });
      $("#datahide").click(function(){
        $("#viewplcm").hide()
        $("#showdata").show()
        $("#datahide").hide();
     });
  });
    this.getPermissions();
    this.getAllPlacements();
    this.placemnt.branchId=0;
    this.placemnt.jobId=0;
  }
}
