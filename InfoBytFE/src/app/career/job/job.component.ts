
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Job, InterviewSchedule } from '../../modal/Career/job';
import { CareerService } from "src/app/service/career.service";
import { ToastrService } from 'ngx-toastr';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { DatePipe } from '@angular/common';
import { UserService } from '../../service/user.service';
import { ImageCompressService } from 'ng2-image-compress';
import { OrganizationService } from '../../service/organization.service';
import { Router, CanActivateChild } from '@angular/router';

import { LoginService } from 'src/app/service/login.service';

@Component({
    selector: 'app-job',
    templateUrl: './job.component.html',
    styleUrls: ['./job.component.scss']
})
export class JobComponent implements OnInit, CanActivateChild {
    applicantDeatilsByPlacementId: Object;
    showAggrement: boolean;
    newdata1;
    
    
    
    showplace: boolean;
    
    
    
    recentjoblist: any;
    jobsOfSpecificInstitution: any;
    check: boolean;
    jobtype: any;
    organizations: any;
    qualificationList: any;
    images: File;
    pipe = new DatePipe('en-US');
    
    
    
    rolename: any;
    userProfile: any;
   
    dataSource;
    
    dataSource1;
    dataSource4;
    
    showAll = false;
    
    rangeValues: number[] = [];
    
    showAcademics: boolean;
    
    newDataSource;
    displayedColumns = ['companyname', 'designation', 'package', 'positions', 'startTime', 'endTime', 'option']
    displayedColumns1 = ['username', 'email', 'experience', 'qualification', 'disignation', 'details', 'approve'];
    displayedColumns2 = ['firstname', 'email', 'jobtittle', 'location', 'functionalarea', 'applicationstatus', 'option'];
    displayedColumns4 = ['companyname', 'location', 'position', 'interviewsatus', 'performance', 'communication', 'behaviour', 'ontime'];
    displayedColumns5 = ['jobtitle', 'name', 'location', 'annualctc', 'doj', 'shifts', 'aggree'];
    joblists: any;
    
    years: any;
    
    stfExp: string;
    
    result: boolean;
    
    view: boolean;
    
    post: boolean;
    minDate = new Date();
    
    showprevInterview = false;
    read: any;
    write: any;
    permissionList: any;
    keywoardList: any;
    
    countries: any;
    
    list: any[] = [];
    locations: any;
    Hrs: any;
    arrayOne(n: number): any[] {
        return Array(n);
    }
    jobmaster: Job;
    InterviewSchedule: any;
    constructor(private router: Router, private ls: LoginService, private os: OrganizationService, private el: ElementRef, private cs: CareerService, private toastr: ToastrService, private userService: UserService) {
        this.jobmaster = new Job();
        this.InterviewSchedule = new InterviewSchedule();
    }
    @ViewChild("addjob") form: any;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    ddlYears(dy: any) {
        this.stfExp = '';
        this.years = (dy.value)
    }
  
    postJobs() {
        this.showAll = true;
        this.post = false;
        var x = this.rangeValues.toString();
        var pos = x.indexOf(",")
        var res = x.slice(0, pos);
        var res1 = x.slice(pos + 1, x.length);

        this.jobmaster.minimumExperience = parseInt(res);
        this.jobmaster.maximumExperience = parseInt(res1);
        this.jobmaster.designation = this.countries
        this.cs.saveJobs(this.jobmaster).subscribe((data) => {
            this.getAlljobss();
            this.getnewJob();
            if (data.result) {
                this.toastr.success(data.message);
            }
            else
                this.toastr.error(data.message)
        },
            (err) => {
                this.toastr.error(err.error.message)
            })
    }

    viewJob() {
        this.view = true;
        this.post = false;
        this.result = false;
    }
    postJob() {
        this.post = true;
        this.view = false;
    }
    applyFilter(filterValue: string) {
        filterValue = filterValue.trim();
        filterValue = filterValue.toLowerCase();
        this.dataSource.filter = filterValue;
    }
    updateJob() {
        this.view = true;
        this.cs.updateJob(this.jobmaster).subscribe((data) => {
        })
        this.getAlljobss();
    }
    
    getAllPlacements() {
        this.cs.getPlacements().subscribe((data) => {
            
            this.showplace = !this.showplace;
            this.showprevInterview = false;
            this.dataSource = new MatTableDataSource(data.data)
            this.dataSource.sort = this.sort
            this.dataSource.paginator = this.paginator
        })
    }
    sendApplicationRequest(id: any) {
        this.cs.sendApplication(id).subscribe((data) => {
        }, (err) => {
            this.toastr.error(err.error.message);
        })
    }
    getApplicantsAllDetails(id: any) {
        this.cs.getApplicantDeatails(id).subscribe((data) => {
            this.applicantDeatilsByPlacementId = data.data;
        }, (err) => {
            this.toastr.error(err.error.message);
        })
    }
  
    getAppliacnts() {
        this.cs.getParticularBranchApplicants().subscribe((data => {
            
            this.result = true;
            this.view = false;
            this.showprevInterview = false;
            this.dataSource1 = new MatTableDataSource(data.data)
            this.dataSource1.sort = this.sort
            this.dataSource1.paginator = this.paginator
        }))
    }
    accepetAggrements( id: number) {
        let res = confirm('Are you sure to Accept the Aggrement ?');
        if (res) {
            this.cs.updateApplicantsAggrements(id).subscribe((data) => {
            })
        }
        else {
            this.toastr.error('canclled');
            this.el.nativeElement.querySelector('#oops').checked = false;
        }
    }
    getPreviousInterview(id: any) {
        this.cs.getApplicantsPrevInterviewDetails(id).subscribe((data) => {
            
            this.showprevInterview = true;
            this.dataSource4 = new MatTableDataSource(data.data)
            this.dataSource4.sort = this.sort
            this.dataSource4.paginator = this.paginator
        })
    }
    getAggrements() {
        this.cs.getAggrementsStatus().subscribe((data) => {
            

            
            this.showAggrement = !this.showAggrement;
            this.showprevInterview = false;
            this.newdata1 = new MatTableDataSource(data.data)
            this.newdata1.sort = this.sort
            this.newdata1.paginator = this.paginator
        })
    }
    acceptApplicantsReq( userid: number, placementsid: number) {
        this.userService.getUserbySession().subscribe((data) => {
            this.userProfile = data.data[0];
            var sessionid = data.data[0].userid
            this.InterviewSchedule.fromid = sessionid;
            let res = confirm('Are You Sure To Perform The Action?');
            if (res) {
                this.cs.acceptJobApplicants(userid, placementsid, this.InterviewSchedule).subscribe((data) => {
                })
            }
            else {
                this.toastr.error('Cancelled');
                this.el.nativeElement.querySelector('#ch').checked = false;
            }
        })
    }
    getAppliedJobStatus() {
        this.showAcademics = !this.showAcademics;
        this.cs.getJobStatus().subscribe((data) => {
            
            
            this.newDataSource = new MatTableDataSource(data.data);
            this.newDataSource.sort = this.sort
            this.newDataSource.paginator = this.paginator
        })
    }
    scheduleAccptence(userid: number, placementid: number) {
        this.cs.jobApllicantsSchedule(userid, placementid).subscribe((data) => {
            if (data.result)
                this.toastr.success(data.message)
            else
                this.toastr.error(data.message)
        })
    }
    fileChangeEvent(fileInput: any) {
        ImageCompressService.filesToCompressedImageSource(fileInput.target.files).then(observableImages => {
            observableImages.subscribe((image) => {
                this.jobmaster.image = image.compressedImage.imageDataUrl;
                this.jobmaster.image = this.jobmaster.image.replace("data:image/gif;base64,", "");
                this.jobmaster.image = this.jobmaster.image.replace("data:image/jpeg;base64,", "");
                this.jobmaster.image = this.jobmaster.image.replace("data:image/jpg;base64,", "");
                this.jobmaster.image = this.jobmaster.image.replace("data:image/png;base64,", "");
            }, (error) => {
                this.toastr.error("Error while converting");
            });
        });
    }
    
    getAlljobss() {
        this.cs.getAlljob().subscribe((data) => {
            this.joblists = data.data;
        })
    }
  
    getnewJob() {
        this.cs.getRecentJob().subscribe((data) => {
            this.recentjoblist = data.data;
        })
    }

  
    getHr() {
        this.cs.getBranchHr().subscribe((data) => {
            this.Hrs = data.data;

        })
    }
    getLocation() {
        this.os.getBranchById().subscribe((data) => {
            this.locations = data.data;

        },
            err => {
                this.toastr.error(err.error.message);
            });
    }

    keywoards() {

        for (var i = 0; i < this.countries.length; i++) {
            this.list.push(this.countries[i].jobtype)
        }


    }
    Keywords(event) {
        let query = event.query;
        this.cs.getJobType().subscribe((data) => {
            this.keywoardList = this.filterKeywords(query, data.data);

        })
    }
    getLocations() {
        this.os.getBranchById().subscribe((data) => {
            

        })
    }
    filterKeywords(query, countries: any[]): any[] {

        let filtered: any[] = [];
        for (let i = 0; i < countries.length; i++) {
            let country = countries[i]

            if (country.jobtype.toLowerCase().indexOf(query.toLowerCase()) == 0) {
                filtered.push(country);
            }
        }
        return filtered;
    }
    getAlljoblist() {
        this.cs.getJobType().subscribe((data) => {
            this.jobtype = data.data;
        })
    }
    getAllQualification() {
        this.cs.getAllDegree().subscribe((data) => {
            this.qualificationList = data.data;
        })
    }
    getOrganization() {
        this.cs.getOrganizationName().subscribe((data) => {
            this.organizations = data.data;
        })
    }
    editJob(id: number) {
        this.cs.editJob(id).subscribe((data) => {
            
            this.view = false;
            this.post = true;
            this.jobmaster.Id = data.data[0].id;
            this.jobmaster.jobTitles = data.data[0].jobtittle;
            this.jobmaster.jobDescription = data.data[0].jobdescription;
            this.jobmaster.designation = data.data[0].designation;
            this.jobmaster.minimumExperience = data.data[0].minimumexperience;
            this.jobmaster.maximumExperience = data.data[0].maximumexperience;
            this.jobmaster.anualCtc = data.data[0].annualctc;
            this.jobmaster.otherSalary = data.data[0].othersalary;
            this.jobmaster.vacancy = data.data[0].vacancy;
            this.jobmaster.location = data.data[0].location;
            this.jobmaster.functionalArea = data.data[0].functionalarea;
            this.jobmaster.jobId = data.data[0].jobid;
            this.jobmaster.ugQualificationId = data.data[0].ugqualificationid;
            this.jobmaster.pgQualificationId = data.data[0].pgqualificationid;
            this.jobmaster.otherQualificationId = data.data[0].otherqualificationid;
            this.jobmaster.requiterEmail = data.data[0].requiteremail;
            this.jobmaster.organizationId = data.data[0].instituteid;
            this.jobmaster.aboutOrganization = data.data[0].institutedescription;
            this.jobmaster.postingDate = this.pipe.transform(data.data[0].postingdate, 'MM-dd-yyyy');
        })
    }
    getJobsByInstitution(id: any) {
        this.check = true;
        
        this.cs.getJobByInstitutionId().subscribe((data) => {
            
            this.jobsOfSpecificInstitution = data.data;
        }, (err) => {
            this.toastr.error(err.error.message);
        })
    }
    getPermissions() {
        this.ls.checkRightPermissions('job').subscribe((data) => {
            this.permissionList = data.data;
            this.read = this.permissionList[0].read;
            this.write = this.permissionList[0].write;
            this.canActivateChild(this.read)
        })
    }
    canActivateChild(x: any): boolean {
        if (x == true) {
            return true;
        } else {
            this.router.navigate(['404']);
            return false;
        }
    }
    ngOnInit() {
        this.getHr();
        this.getLocation();
        this.getLocations();
        this.getPermissions();
        this.getAlljoblist();
        this.getAlljobss();
        this.getAllQualification();
        this.getOrganization();
        this.getnewJob();
        if (sessionStorage.getItem('user') != null) {
            this.userService.getUserbySession().subscribe((data) => {
                this.userProfile = data.data[0];
                this.rolename = this.userProfile.rolename;
            }, (err) => {
                this.toastr.error(err.error.message)
            });
        }
        this.jobmaster.jobId = 0;
        this.jobmaster.maximumExperience = 0;
        this.jobmaster.minimumExperience = 0;
        this.jobmaster.organizationId = 0;
        this.jobmaster.pgQualificationId = 0;
        this.jobmaster.ugQualificationId = 0;
        this.jobmaster.otherQualificationId = 0;
        this.jobmaster.requiterEmail = 0;
    }
}
