import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllocateProjectComponent } from './allocate-project/allocate-project.component';
import { AllocatePlacementComponent } from './allocate-placement/allocate-placement.component';
import { RecruitmentComponent } from './recruitment/recruitment.component';
import { JobComponent } from './job/job.component';
import { PlacementsComponent } from "./placements/placements.component";
import { ProjectComponent } from "./project/project.component";
import { CareerRoutingModule } from './career-routing.module';
import { SliderModule } from 'primeng/slider';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatStepperModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatSliderModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { MatSlideToggleModule } from '@angular/material';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { TypeaheadModule } from 'ngx-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { AgmCoreModule } from '@agm/core'
import { AgmDirectionModule } from 'agm-direction';
import { TreeModule } from 'primeng/tree';
import { FileUploadModule } from 'ng2-file-upload';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ImageCompressService, ResizeOptions } from 'ng2-image-compress';
import { GalleriaModule } from 'primeng/galleria';
import { AutoCompleteModule } from 'primeng/autocomplete';
@NgModule({
  declarations: [JobComponent,AllocatePlacementComponent,AllocateProjectComponent,RecruitmentComponent,PlacementsComponent,ProjectComponent],
  imports: [SliderModule,
    CommonModule,
    CareerRoutingModule,
    MatTableModule, MatTableModule,
    MatSlideToggleModule,
    CdkTableModule,
    CdkTreeModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule, MatFormFieldModule,
    CommonModule,
    //BrowserAnimationsModule, 
    CommonModule, ToastrModule.forRoot(),
    //RouterModule.forRoot(routes), 
    FormsModule, DataTablesModule, TypeaheadModule.forRoot(),
    //BrowserAnimationsModule, 
    MatStepperModule, MatInputModule, MatIconModule, MatButtonModule, MatSlideToggleModule, BsDatepickerModule.forRoot(),
    AgmCoreModule.forRoot({ apiKey: '' }),
    AgmDirectionModule, FileUploadModule, TreeModule, GalleriaModule,AutoCompleteModule
  ],
  providers: [ImageCompressService, ResizeOptions],
  exports: [
    RouterModule
 ]
})
export class CareerModule { }
