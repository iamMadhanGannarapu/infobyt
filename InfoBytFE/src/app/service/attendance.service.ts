import { Injectable } from '@angular/core';
import {Holiday } from '../modal/Attendance/Holiday';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as config from './service_init.json';
import { Attendance } from '../modal/Attendance/Attendance';
import { UserMapping, BranchMapping } from '../modal/User/branchmapping';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Injectable({
  providedIn: 'root'
})
export class AttendanceService {
  userSession:any;
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }
  url: string = (<any>config).api;
  constructor(private http: HttpClient, ) { }
  initSession(){
    this.userSession = sessionStorage.getItem('user')
  }
  /*============================ Excel=============================== */
  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }
  /*============================ Holiday Master=============================== */
  //Done
  saveHoliday(hds:Holiday): Observable<any> {
    return this.http.post(this.url+'/Attendance/Holiday/Add', JSON.stringify(hds), this.httpOptions)
  }
  //Done
  getAllHolidayDetails() :Observable<any> {
    this.initSession();
    for(var i=0;i<this.userSession.length;i++){
      this.userSession=this.userSession.replace('/','-');
      }
    return this.http.get(this.url+'/Attendance/Holiday/'+this.userSession, { responseType: 'json' })
  }
  //Done
  getAllHolidayCal():Observable<any> {
    this.initSession();
    for(var i=0;i<this.userSession.length;i++){
      this.userSession=this.userSession.replace('/','-');
      }
    return this.http.get(this.url+'/Attendance/HolidayCalender/'+this.userSession, { responseType: 'json' })
  }
  //Done
  deleteHoliday(id:number): Observable<any>{
    return this.http.delete(this.url+'/Attendance/Holiday/Delete/'+ id,this.httpOptions)
  }
  //Done
  getHolidaysById(Id:number) :Observable<any> {    
    return this.http.get(this.url+'/Attendance/Holiday/Details/'+ Id,{ responseType: 'json' })
  }
  //Done
  updateHoliday(holiday: Holiday): Observable<any>{
    return this.http.post(this.url+'/Attendance/Holiday/Update/'+holiday.Id, JSON.stringify(holiday), this.httpOptions)
  }
   /* ============================Attendance================================= */
   //Done
saveAttendance(attendancelist: any,dt:any,itm:any,otm:any): Observable<any> {
  return this.http.post(this.url + '/Attendance/attendance/Trainee/add/'+dt+'/'+itm+'/'+otm, attendancelist, this.httpOptions);
}
//Done
saveAttendanceForStaff(attendanceliststaff: any,dt:any,itm:any,otm:any): Observable<any> {
  return this.http.post(this.url + '/Attendance/attendance/Staff/add/'+dt+'/'+itm+'/'+otm, attendanceliststaff, this.httpOptions);
}
//Done
getAllTraineeListByBatch(id):Observable<any>
{
return this.http.get(this.url+'/Attendance/attendance/traineeList/Batch/'+id ,{responseType:'json'})
}
//Done
getAllAttendance(attendance: Attendance):Observable<any>
{
  this.initSession();
  for(var i=0;i<this.userSession.length;i++){
    this.userSession=this.userSession.replace('/','-');
    }
  return this.http.get(this.url + '/Attendance/attendance/get/'+ this.userSession +'/'+ attendance.fromDate+ '/'+ attendance.toDate , {responseType:'json'})
}
//Done
getAttendanceById(Id):Observable<any>
{
return this.http.get(this.url+'/Attendance/attendance/Details/'+Id ,{responseType:'json'})
}
//Done
getAttendanceOfTraineesByStaff(attendance: Attendance):Observable<any>
{
this.initSession();
for(var i=0;i<this.userSession.length;i++){
  this.userSession=this.userSession.replace('/','-');
  }
return this.http.get(this.url+'/Attendance/attendance/trainees/Details/staff/'+this.userSession +'/'+ attendance.fromDate+ '/'+ attendance.toDate  ,{responseType:'json'})
}
//Done
getAttendanceOfTraineesByAccountant(attendance: Attendance):Observable<any>
{
this.initSession();
for(var i=0;i<this.userSession.length;i++){
  this.userSession=this.userSession.replace('/','-');
  }
return this.http.get(this.url+'/Attendance/attendance/trainees/Details/accontant/'+this.userSession +'/'+ attendance.fromDate+ '/'+ attendance.toDate,{responseType:'json'})
}
//Done
getAttendanceOfStaffByAccountant(attendance: Attendance):Observable<any>
{
this.initSession();
for(var i=0;i<this.userSession.length;i++){
  this.userSession=this.userSession.replace('/','-');
  }
return this.http.get(this.url+'/Attendance/attendance/staff/Details/accontant/'+this.userSession +'/'+ attendance.fromDate+ '/'+ attendance.toDate,{responseType:'json'})
}
//Done
getAttendanceOfBranchadminBySchoolAdmin(attendance: Attendance):Observable<any>
{
this.initSession();
for(var i=0;i<this.userSession.length;i++){
  this.userSession=this.userSession.replace('/','-');
  }
return this.http.get(this.url+'/Attendance/attendance/staff/Details/branchAdmins/'+this.userSession +'/'+ attendance.fromDate+ '/'+ attendance.toDate,{responseType:'json'})
}
//Done
getTodaysAttendance(attendance:Attendance):Observable<any>
{
  this.initSession();
  for(var i=0;i<this.userSession.length;i++){
    this.userSession=this.userSession.replace('/','-');
    }
  return this.http.get(this.url+'/Attendance/todaysattendance/staff/trainees/Details/'+this.userSession ,{responseType:'json'})
  }
//Done
  getattendancebybranchadmin(branchId:any,attendance:Attendance):Observable<any>
  {
    this.initSession();
    for(var i=0;i<this.userSession.length;i++){
      this.userSession=this.userSession.replace('/','-');
      }
    return this.http.get(this.url+'/Attendance/attendance/staff/trainees/branchAdmins/'+branchId+'/'+attendance.fromDate+'/'+attendance.toDate,{responseType:'json'})
    }
    //Done
updateAttendance(attendance: Attendance):Observable<any>{
  return this.http.post(this.url+'/Attendance/attendance/update/'+attendance.attendanceLogId, JSON.stringify(attendance), this.httpOptions)
}
//Done
attendancebyuserId(userId:any):Observable<any>
{
return this.http.get(this.url+'/Attendance/attendance/userDetails/'+userId,{responseType:'json'})
}
//Done
getAttendanceOfUser(attendance: Attendance):Observable<any>
{
this.initSession();
for(var i=0;i<this.userSession.length;i++){
  this.userSession=this.userSession.replace('/','-');
  }
return this.http.get(this.url+'/Attendance/attendance/get/'+this.userSession +'/'+ attendance.fromDate + '/'+ attendance.toDate ,{responseType:'json'})
}
//Done(not found)
getAttendanceOfbranchId(attendance: Attendance):Observable<any>
{
this.initSession();
for(var i=0;i<this.userSession.length;i++){
  this.userSession=this.userSession.replace('/','-');
  }
return this.http.get(this.url+'/Attendance/attendance/Details/branchadminid/'+this.userSession +'/'+ attendance.fromDate + '/'+ attendance.toDate ,{responseType:'json'})
}
//Done
getAllStaffListByBranch():Observable<any>
{
  this.initSession();
  for(var i=0;i<this.userSession.length;i++){
    this.userSession=this.userSession.replace('/','-');
    }
return this.http.get(this.url+'/Attendance/attendance/staffList/Branch/'+this.userSession ,{responseType:'json'})
}
/* ==============================UserMapping============================== */
//Done
addBranchmapping(bm:BranchMapping):Observable<any>{
  return this.http.post(this.url + '/Attendance/branchmapping/add/', JSON.stringify(bm), this.httpOptions);
}
//Done
   addUserMappingmapping(um:UserMapping):Observable<any>{
    return this.http.post(this.url + '/Attendance/usermapping/add/', JSON.stringify(um), this.httpOptions);
  }
  //Done
  getUserMapping():Observable<any>
    {
      this.initSession();
      for(var i=0;i<this.userSession.length;i++){
        this.userSession=this.userSession.replace('/','-');
        }
      return this.http.get(this.url+'/Attendance/user/usermapping/get/'+this.userSession,{responseType:'json'})
    }
    // Done
    getDeviceId():Observable<any>
    {
      this.initSession();
      for(var i=0;i<this.userSession.length;i++){
        this.userSession=this.userSession.replace('/','-');
        }
      return this.http.get(this.url+'/Attendance/usermapping/usercode/getdevice/'+this.userSession,{responseType:'json'})
    }
    //Done
    getUserdeviceIdBydeviceid(id:any):Observable<any>
  {
    return this.http.get(this.url+'/Attendance/usermapping/usercode/details/all/'+id,{responseType:'json'})
  }
  //Done 
  getusermappingdetails():Observable<any>
{
  this.initSession();
  for(var i=0;i<this.userSession.length;i++){
    this.userSession=this.userSession.replace('/','-');
    }
  return this.http.get(this.url+'/Attendance/usermapping/'+this.userSession,{responseType:'json'})
}
//Done
getbranchmappingdetails():Observable<any>
{
  this.initSession();
  for(var i=0;i<this.userSession.length;i++){
    this.userSession=this.userSession.replace('/','-');
    }
  return this.http.get(this.url+'/Attendance/branchmapping/'+this.userSession,{responseType:'json'})
}
//============================================latelogins
//Done
getLateOfTraineesByStaff():Observable<any>
{
this.initSession();
for(var i=0;i<this.userSession.length;i++){
  this.userSession=this.userSession.replace('/','-');
  }
return this.http.get(this.url+'/Attendance/latelogins/'+this.userSession,{responseType:'json'})
}
//Done
getLateLoginsOfUser(userId:any,month:any):Observable<any>
{
  return this.http.get(this.url+'/Attendance/LateLogin/User/'+userId+'/'+month,{responseType:'json'})
}
//Done
updateLateLoginStatus(usrcode:any,date:any,status:any):Observable<any>{
  return this.http.post(this.url+'/Attendance/latelogin/update/', {usrcode:usrcode,date:date,status:status}, this.httpOptions)
}
}
