import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Fee } from '../modal/Fee/fee';
import { FeePayments } from '../modal/Fee/FeePayments';
import { Concession } from '../modal/Fee/concession';
import * as config from './service_init.json';
import { FeeStructure } from '../modal/Fee/feestructure';
import { GenPattern } from '../modal/Fee/genpattern';
import { PatternEmi } from '../modal/Fee/emi';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Injectable({
    providedIn: 'root'
})
export class FeeService {
    myUrl: string = (<any>config).api;
    userSession: any;
    httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) }
    constructor(private http: HttpClient) {
    }
    initSession() {
        this.userSession = sessionStorage.getItem('user')
    }
        //==========================Excel=============================================================
    public exportAsExcelFile(json: any[], excelFileName: string): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
      }
      private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
          type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
      }
    //==========================Concession Master=============================================================
    saveConcession(concession: Concession): Observable<any> {
        return this.http.post(this.myUrl + "/Fee/Concession/Add", JSON.stringify(concession), this.httpOptions)
    }
    getAllConcession(): Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.myUrl + "/Fee/Concession/" + this.userSession, { responseType: 'json' })
    }
    getConcessionById(i: number): Observable<any> {
        return this.http.get(this.myUrl + "/Fee/Concession/Details/" + i, { responseType: 'json' })
    }
    updateConcession(concessionObj: Concession): Observable<any> {
        return this.http.post(this.myUrl + '/Fee/Concession/Update/' + concessionObj.Id, JSON.stringify(concessionObj), this.httpOptions)
    }
    //================================Fee Payments=======================================
    saveFeepayments(feepayments: FeePayments): Observable<any> {
        return this.http.post(this.myUrl + "/Fee/Trainee/Fee/Add/", JSON.stringify(feepayments), this.httpOptions)
    }
   getAllFeepayments(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.myUrl + "/Fee/Trainee/Fee/" + this.userSession, { responseType: 'json' })
  }
    getFeepaymentById(i: number): Observable<any> {
        return this.http.get(this.myUrl + '/Fee/Trainee/Fee/Details/' + i, { responseType: 'json' });
    }
     getAllTraineenameForAccountant(): Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.myUrl + "/Fee/Fee/Accountant/Details/" + this.userSession, { responseType: 'json' })
      }
 getFeepaymentByUserId(i: number): Observable<any> {
        return this.http.get(this.myUrl + '/Fee/Trainee/Fee/user/' + i, { responseType: 'json' });
    }
 getFeepaymentByBatchId(id: number): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.myUrl + '/Fee/Trainee/Fee/Batch/' + id+'/'+ this.userSession, { responseType: 'json' });
}
    //==================================Fee Master===================================================
    saveFee(fee: Fee): Observable<any> {
        return this.http.post(this.myUrl + "/Fee/Fee/Add", JSON.stringify(fee), this.httpOptions)
    }
    getAllFee(): Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.myUrl + "/Fee/Fee/" + this.userSession, { responseType: 'json' })
    }
    getFeeById(i: number): Observable<any> {
        return this.http.get(this.myUrl + '/Fee/Fee/details/' + i, { responseType: 'json' });
    }
    updateFee(updateFeeMaster: Fee): Observable<any> {
        return this.http.post(this.myUrl + '/Fee/Fee/Update/' + updateFeeMaster.Id, JSON.stringify(updateFeeMaster), this.httpOptions)
    }
    //======================================financialreports========================================================
    getAllFinancials(): Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.myUrl + "/Fee/Financial/Details/" + this.userSession, { responseType: 'json' })
    }
    //======================================financialreports by  branchclass========================================================
    getAllFinancialreportbybranchSession(n:number): Observable<any> {
        return this.http.get(this.myUrl + "/Fee/Financialreport/branchsession/Details/" + n, { responseType: 'json' })
    }
    //===================================Fee Structure================================================
    saveFeeStructure(feepattern: FeeStructure, id: number): Observable<any> {
        var b = JSON.stringify(feepattern);
        var str = b.replace(/\\/g, '');
        str = str.substring(1, str.length - 1)
        return this.http.post(this.myUrl + "/Fee/Fee/Structure/Add/" + id, JSON.parse(str), this.httpOptions)
    }
    getFeeStructure(): Observable<any> {
        return this.http.get(this.myUrl + "/Fee/Trainee/Feestructure/view/", { responseType: 'json' })
    }
    getFeeStructurePattern(): Observable<any> {
        return this.http.get(this.myUrl + "/Fee/Trainee/Feestructurepattern/view", { responseType: 'json' })
    }
    getFeeStructurePatternById(id: number): Observable<any> {
        return this.http.get(this.myUrl + "/Fee/Trainee/Feestructurepattern/viewbyId/" + id, { responseType: 'json' })
    }
}
