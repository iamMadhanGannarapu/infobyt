import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Survelince } from '../modal/Attendance/Survelince';
import * as config from './service_init.json';
import { BatchService } from './batch.service';
@Injectable({
  providedIn: 'root'
})
export class SurvelinceService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }
  url: string = (<any>config).api;
  userSession: any;
constructor(private http: HttpClient,private batch: BatchService) { 
  }
  getAllSurvelinceDetails(): Observable<any> {
    return this.http.get(this.url + "/Surveillance/abc/All", { responseType: 'json' })
}
addCameradetails(cm:Survelince)
{
  return this.http.post(this.url + '/Surveillance/camera/add', JSON.stringify(cm), this.httpOptions)
}
}