import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Organization } from '../modal/Organization/Organization';
import { BranchSession } from '../modal/Organization/BranchSession';
import { Branch } from '../modal/Organization/Branch';
import { Achievement } from '../modal/Organization/achievements'
import { AdmisionDetails } from '../modal/Organization/AdmisionDetails'
import * as config from './service_init.json';
import { Course } from "../modal/Organization/Courses";
import { Departments } from "../modal/Organization/Department";
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { EventsDetails } from '../modal/Organization/eventsdetails';
import { Shifts } from '../modal/Organization/shifts';
import { AllocateShifts } from '../modal/Organization/allocateshifts';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Injectable({
    providedIn: 'root'
})
export class OrganizationService {
    getallshifts(): any {
        throw new Error("Method not implemented.");
    }
    userSession: any
    url: string = (<any>config).api;
    httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) }
    allocateUsertoShifts: any;
    constructor(private http: HttpClient) {
    }
    initSession() {
        this.userSession = sessionStorage.getItem('user')
    }
    //=========================================Excel====================================
    public exportAsExcelFile(json: any[], excelFileName: string): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }
    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    }
    //=========================================Achievements====================================
    saveAchievements(achievements: Achievement): Observable<any> {
        return this.http.post(this.url + "/Organization/Achievement/Add", JSON.stringify(achievements), this.httpOptions)
    }
    getAllAchievements(): Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.url + "/Organization/Achievement/" + this.userSession, { responseType: 'json' })
    }
    getAchievementById(i: number): Observable<any> {
        return this.http.get(this.url + "/Organization/Achievement/details/" + i, { responseType: 'json' })
    }
    updateAcheievement(updateAchievementobj: Achievement): Observable<any> {
        return this.http.post(this.url + '/Organization/Achievement/Update/' + updateAchievementobj.Id, JSON.stringify(updateAchievementobj), this.httpOptions)
    }
    //===================================Admision details==================================
    saveAdmitionDetails(admitiondetails: AdmisionDetails): Observable<any> {
        return this.http.post(this.url + "/Organization/Trainee/Add", JSON.stringify(admitiondetails), this.httpOptions)
    }
    getAllAdmission(): Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.url + "/Organization/Trainee/" + this.userSession, { responseType: 'json' })
    }
    acceptTc(admissionNum: any, type: any): Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.post(this.url + "/Organization/Tc/Approve", { admissionNum: admissionNum, type: type,userSession:this.userSession }, this.httpOptions)
    }
getAdmissionById(i: number): Observable<any> {
        return this.http.get(this.url + "/Organization/Trainee/details/" + i, { responseType: 'json' })
    }
    getAdmissionByUserId(i: number): Observable<any> {
        return this.http.get(this.url + "/Organization/admission/feeconcession/" + i, { responseType: 'json' })
    }
    updateAdmission(updateAdmissionObj: AdmisionDetails): Observable<any> {
        return this.http.post(this.url + '/Organization/Trainee/Update/' + updateAdmissionObj.admissionNum, updateAdmissionObj, this.httpOptions)
    }
    //==============================Branch Class Master=======================================
    saveBranchSession(branchclass: BranchSession): Observable<any> {
        return this.http.post(this.url + "/Organization/Session/Add", JSON.stringify(branchclass), this.httpOptions)
    }
    getAllBranchSession(): Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.url + "/Organization/Session/" + this.userSession, { responseType: 'json' })
    }
    getAllBranchSessionById(i: number): Observable<any> {
        return this.http.get(this.url + "/Organization/Session/Details/" + i, { responseType: 'json' })
    }
    updateBranchSession(updateBranchClass: BranchSession):Observable<any> {
        return this.http.post(this.url + '/Organization/branch/session/Update/' + updateBranchClass.Id, JSON.stringify(updateBranchClass), this.httpOptions)
    }
    getallshiftss(): Observable<any>  {
        return this.http.get(this.url + "/Organization/Branch/Shifts/All/", { responseType: 'json' })
    }
    //===================================School Branch Master=================================
    saveBranch(schoolBranch: Branch): Observable<any> {
        return this.http.post(this.url + "/Organization/Branch/Add", JSON.stringify(schoolBranch), this.httpOptions)
    }
    getAllBranch(): Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-')
        }
        return this.http.get(this.url + "/Organization/branch/" + this.userSession, { responseType: 'json' })
    }
    getuserById():Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.url + "/Organization/Branch/User/" + this.userSession, { responseType: 'json' })
    }
    getBranchById(): Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.url + "/Organization/branches/details/" + this.userSession, { responseType: 'json' })
    }
    getBranchByOrgBrnId(id: number): Observable<any> {
        return this.http.get(this.url + "/Organization/Branch/Details/" + id, { responseType: 'json' })
    }
    updateBranch(updateSchoolBranch: Branch): Observable<any>  {
        return this.http.post(this.url + '/Organization/branch/Update/' + updateSchoolBranch.Id, JSON.stringify(updateSchoolBranch), this.httpOptions)
    }
    //==========================================School Master==================================
    saveOrganization(school: Organization): Observable<any> {
        return this.http.post(this.url + "/Organization/Add/Organization", JSON.stringify(school), this.httpOptions)
    }
    getAllOrganization(): Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.url + "/Organization/Organizations/" + this.userSession, { responseType: 'json' })
    }
    getOrganizationById(i: number): Observable<any> {
        return this.http.get(this.url + "/Organization/Details/" + i, { responseType: 'json' })
    }
    updateOrganization(updateSchool: Organization): Observable<any> {
        return this.http.post(this.url + '/Organization/update/' + updateSchool.Id, JSON.stringify(updateSchool), this.httpOptions)
    }
    getAllOrganizationsIB(): Observable<any> {
        return this.http.get(this.url + "/Organization/request/", { responseType: 'json' })
    }
    updateBranchStatus(id: number, sBranches: Branch) : Observable<any> {
        return this.http.post(this.url + '/Organization/Branch/Status/Update/' + id, JSON.stringify(sBranches), this.httpOptions);
    }
    //==================signup==============================================================
    verifyUserData(inp: string, inpData: any) : Observable<any> {
        return this.http.get(this.url + '/Organization/OrganizationDetails/Verify/' + inp + '/' + inpData, this.httpOptions)
    }
    /* ************************Events master***************** */
    getEventId(id: any): Observable<any> {
        return this.http.get(this.url + '/Organization/Single/Event/' + id, { responseType: 'json' });
      }
      evntResponse(id:any,res:any):Observable<any>
      {
        return this.http.post(this.url + '/Organization/EventResponse/' + id+'/'+res, { responseType: 'json' });
      }
      updateEvent(eventdetailsInfo: EventsDetails) : Observable<any> {
        let id = JSON.stringify(eventdetailsInfo.Id)
        return this.http.post(this.url + "/Organization/Events/Update/" + id, JSON.stringify(eventdetailsInfo), this.httpOptions)
      }
      getContact(userId: number): Observable<any> {
        return this.http.get(this.url + '/Organization/College/Contact/' + userId, { responseType: 'json' });
      }
      eventPosted(eventdetailsInfo: EventsDetails):Observable<any> {
        return this.http.post(this.url + '/Organization/Events/Add', JSON.stringify(eventdetailsInfo), this.httpOptions)
      }
      getAllEvents(): Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
          this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.url + '/Organization/All/Events/' + this.userSession, { responseType: 'json' });
      }
      getAllEventscalender(): Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
          this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.url + '/Organization/All/Calender/' + this.userSession, { responseType: 'json' })
      }
      sendparticipation(n: any): Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
          this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.url + '/Organization/Events/Request/Participation/' + n + '/' + this.userSession, { responseType: 'json' });
      }
      getParticipentsList(): Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
          this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.url + "/Organization/Events/Request/List/" + this.userSession, { responseType: 'json' })
      }
      getAllDefaultUser(): Observable<any> {
        return this.http.get(this.url + "/Session/Events/Default/Applicants", { responseType: 'json' })
      }
      getAllUsersByBranch(): Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
          this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.url + '/Organization/Users/All/' + this.userSession, { responseType: 'json' })
      }
    //*********************************Courses***************************** */
    getAllCourses(): Observable<any> {
        return this.http.get(this.url + "/Organization/courses/Details", { responseType: 'json' })
    }
    getCourseById(id: number): Observable<any> {
        return this.http.get(this.url + "/Organization/courses/DetailsbyId/" + id, { responseType: 'json' })
    }
    saveCourses(course: Course): Observable<any> {
        return this.http.post(this.url + "/Organization/courses/Add", JSON.stringify(course), this.httpOptions)
    }
    updateCourses(course: Course): Observable<any> {
        return this.http.post(this.url + '/Organization/courses/Update/' + course.Id, JSON.stringify(course), this.httpOptions)
    }
    saveDomain(department: Departments): Observable<any> {
        return this.http.post(this.url + "/Organization/Domain/Add", JSON.stringify(department), this.httpOptions)
    }
    getAllDomains(): Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.url + "/Organization/Domain/Details/" + this.userSession, { responseType: 'json' })
    }
    getDomainById(id: number): Observable<any> {
        return this.http.get(this.url + "/Organization/Domain/DetailbyId/" + id, { responseType: 'json' })
    }
    updateDomain(department: Departments): Observable<any> {
        return this.http.post(this.url + '/Organization/Domain/Update/' + department.Id, JSON.stringify(department), this.httpOptions)
    }
    //departments
    saveDepartments(department: Departments): Observable<any> {
        return this.http.post(this.url + "/Organization/Departments/Add", JSON.stringify(department), this.httpOptions)
    }
    getAllDepartments(): Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.url + "/Organization/Departments/Details/" + this.userSession, { responseType: 'json' })
    }
    //=========================================Transfer====================================
    applyTc() : Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.post(this.url + "/Organization/Branch/Transfer/Trainee/Add/" + this.userSession, this.httpOptions)
    }
    saveTransferBranchid(id: any): Observable<any>  {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.post(this.url + '/Organization/Branch/Transfer/fromBranch/Add/' + this.userSession + '/' + id, this.httpOptions)
    }
    getOrganizations(): Observable<any>  {
        return this.http.get(this.url + "/Organization/AllOrganizations/", { responseType: 'json' })
    }
    getAllBranches(schoolId: any): Observable<any>  {
        return this.http.get(this.url + "/Organization/AllBranches/" + schoolId, { responseType: 'json' })
    }
    getAllSessions(branchId: any): Observable<any>  {
        return this.http.get(this.url + "/Organization/Branch/AllSessions/" + branchId, { responseType: 'json' })
    }
    saveTransfer(branchId: any, classId: any): Observable<any>  {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.post(this.url + '/Organization/Branch/Transfer/toBranch/Add/' + this.userSession + '/' + branchId + '/' + classId, this.httpOptions, this.httpOptions)
    }
    getTransferStatus(): Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.url + "/Organization/Branch/TraineeTransferStatus/" + this.userSession, { responseType: 'json' })
    }
    getAllTransfer() : Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.url + "/Organization/Branch/Transfer/" + this.userSession, { responseType: 'json' })
    }
    getTcTrainees(): Observable<any>  {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.url + "/Organization/TcApplicants/" + this.userSession, { responseType: 'json' })
    }
    acceptTrainee(studentid: any) {
        return this.http.post(this.url + '/Organization/Branch/Accept/Trainee/' + studentid, this.httpOptions)
    }
    approveTrainees(): Observable<any>  {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.url + "/Organization/FromBranch/AprvTransfer/" + this.userSession, { responseType: 'json' })
    }
    getAcceptTrainees(): Observable<any>  {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.url + "/Organization/ToBranch/AcceptTransferList/" + this.userSession, { responseType: 'json' })
    }
    /////////shifts////////////////
    saveShift(shift: Shifts): Observable<any> {
        return this.http.post(this.url + "/Organization/Branch/Shifts/Add", JSON.stringify(shift), this.httpOptions)
    }
    getShiftById(id: number): Observable<any> {
        return this.http.get(this.url + "/Organization/Branch/Shiftby/" + id, { responseType: 'json' })
    }
    updateShifts(updateShifts: Shifts): Observable<any> {
        return this.http.post(this.url + '/Organization/Branch/Shifts/Update/' + updateShifts.Id, JSON.stringify(updateShifts), this.httpOptions)
    }
    getAllShifts(): Observable<any> {
        return this.http.get(this.url + "/Organization/Branch/ShiftsAll/", { responseType: 'json' })
    }
    getShiftsByBranch(): Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.url + "/Organization/Branch/AllShifts/" + this.userSession, { responseType: 'json' })
    }
    getUserofDept(deptId: any): Observable<any> {
        return this.http.get(this.url + "/Organization/Branch/Dept/Users/" + deptId, { responseType: 'json' })
    }
    addallocateshifts(allocateShifts: AllocateShifts, deptUsers: any): Observable<any> {
        this.allocateUsertoShifts = new AllocateUsertoShifts();
        this.allocateUsertoShifts.allocateShift = allocateShifts;
        this.allocateUsertoShifts.DeptUsers = deptUsers;
        return this.http.post(this.url + "/Organization/Allocate/User/Shift", this.allocateUsertoShifts, this.httpOptions)
    } 
    getAllAllcateShifts():Observable<any>{
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.url+"/Organization/AllocateShifts/ViewAll/" + this.userSession,this.httpOptions)
    }


}
class AllocateUsertoShifts {
    allocateShift: any;
    DeptUsers: any;
}
