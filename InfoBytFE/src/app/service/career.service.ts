
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpHandler, HttpInterceptor, HttpRequest, HttpEvent } from '@angular/common/http'
import { Observable } from 'rxjs';
import { AllocateProject } from '../modal/career/AllocateProject';
import { AllocatePlacement } from '../modal/career/AllocatePlacement';
import { placement } from '../modal/Career/placement'
import { project } from '../modal/Career/project'
import { Recruitment, SelectedApplicants } from '../modal/career/recruitment';
import { Job, InterviewSchedule } from '../modal/career/job';
import { requestRecruitment } from 'src/app/modal/career/requestRecruitment';
import * as config from './service_init.json';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Injectable({
  providedIn: 'root'
})
export class CareerService {
    httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    url: string = (<any>config).api;
    userSession: any;
    constructor(private hc: HttpClient) {
    }
    initSession() {
      this.userSession = sessionStorage.getItem('user')
    }
     //*********************************Excel***************************** */
  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }
  //===========================Allocate Project===========================
  
allocateProj(alocproj: AllocateProject,traineeList:any): Observable<any> {
  return this.hc.post(this.url + '/Career/Project/Trainee/Add/',{alocproj:alocproj,traineeList:traineeList} , this.httpOptions)
}
  traineebybatch(i: number): Observable<any> {
    return this.hc.get(this.url + "/Career/Trainee/Batch/Project/" + i, { responseType: 'json' })
  }
  traineesAllocatedPrj(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + "/Career/Branch/AllocProject/" + this.userSession, { responseType: 'json' })
  }
  getAllocProjId(i: number): Observable<any> {
    return this.hc.get(this.url + "/Career/Branch/AllocProject/Details/" + i, { responseType: 'json' })
  }
  updAllocProj(alproj: AllocateProject): Observable<any> {
    return this.hc.post(this.url + "/Career/Branch/AllocProject/Update/" + alproj.Id, JSON.stringify(alproj), this.httpOptions)
  }

  //===========================Allocate Placement ======================================
  allocatePlac(alplc: AllocatePlacement): Observable<any> {
    return this.hc.post(this.url + '/Career/Placement/Trainee/Add', JSON.stringify(alplc), this.httpOptions)
  }
  traineesAllocatedPlc(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + "/Career/Branch/AllocPlacement/" + this.userSession, { responseType: 'json' })
  }
  getAllocPlcId(i: number): Observable<any> {
    return this.hc.get(this.url + "/Career/Branch/AllocPlacement/Details/" + i, { responseType: 'json' })
  }
  updAllocPlc(alplc: AllocatePlacement): Observable<any> {
    return this.hc.post(this.url + "/Career/Branch/AllocPlacement/Update/" + alplc.Id, JSON.stringify(alplc), this.httpOptions)
  }
  /* **********************************Recruitment********************** */
  saveRecrtmntApplication(rt: Recruitment): Observable<any> {
    return this.hc.post(this.url + '/Career/Selected/Candidate/Add', JSON.stringify(rt), this.httpOptions)
  }
  getAllRecrtmntApplication(): Observable<any> {
    return this.hc.get(this.url + "/Career/Recruitments/All/", { responseType: 'json' })
  }
  getRecrtmntApplicationById(id): Observable<any> {
    return this.hc.get(this.url + "/Career/Recruitments/Details/" + id, { responseType: 'json' })
  }
  sendRecruitmentRequest(req: requestRecruitment): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.post(this.url + '/Career/Recruitment/Request/Add/' + this.userSession, JSON.stringify(req), this.httpOptions)
  }
  // Getting Aggrement accept candiadte
  getCandidateForSendingOffer(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Career/Job/Applicants/OfferLetter/'+  this.userSession, { responseType: 'json' })
  }
  getRecruitmentNotifications(): Observable<any>  {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Career/Request/Recruitment/Notification/' + this.userSession, { responseType: 'json' })
  }
  getRequestRecruitmentById(id: any): Observable<any>  {
    return this.hc.get(this.url + '/Career/Request/Recruitment/Details/' + id, { responseType: 'json' })
  }
  allRatings() : Observable<any>{
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Career/Applicants/Ratings/' + this.userSession, { responseType: 'json' })
  }
  ratings() : Observable<any>{
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Career/Specific/Applicant/Ratings/' + this.userSession, { responseType: 'json' })
  }
  updateRecruitmentRequest(req: requestRecruitment): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.post(this.url + '/Career/Request/Recruitment/Update/' + req.Id, JSON.stringify(req), this.httpOptions)
  }
  sendLoi(uid: any, id: any): Observable<any> {
    return this.hc.post(this.url + '/Career/Recruitment/Loi/Send/' + uid + '/' + id, this.httpOptions)
  }
  getApplicantsforinterview(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Career/Applicants/Attend/Interview/' + this.userSession, { responseType: 'json' })
  }
  acceptLoi(id: any, st: any): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.post(this.url + '/Career/Recruitment/Loi/Accept/' + this.userSession + '/' + id + '/' + st, this.httpOptions)
  }
  getAllintrst(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Career/Recruitment/Intrest/Details/' + this.userSession, { responseType: 'json' })
  }
  addDocVerification(intrstlist: any): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.post(this.url + '/Career/Recruitment/Doc/Verification/', JSON.stringify(intrstlist), this.httpOptions)
  }
  
  saveAggrements(s: SelectedApplicants): Observable<any> {
    return this.hc.post(this.url + '/Career/Branch/Applicant/Send/Aggrements/', JSON.stringify(s), this.httpOptions)
  }
  gettingOfferLetter(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Career/Recruitment/Job/Package/Details/' + this.userSession, { responseType: 'json' })
  }
  respondingForTheOffer(id: number) : Observable<any> {
    return this.hc.post(this.url + '/Career/Recruitment/Job/Acceptance/Send/' + id, this.httpOptions)
  }
  getAllAgreedList(): Observable<any> {
    return this.hc.get(this.url + "/Career/Applicants/Job/agreed/", { responseType: 'json' })
  }
  sendOfferLetter(uid: any, id: any): Observable<any> {
    return this.hc.post(this.url + '/Career/Recruitment/OfferLetter/Send/' + uid + '/' + id, this.httpOptions)
  }
   //=======================================Placement================================================
   savePlacement(placemnt: placement): Observable<any> {
    return this.hc.post(this.url + '/Career/Branch/Placement/Add', JSON.stringify(placemnt), this.httpOptions)
}
getbyPlacementId(i: number) : Observable<any> {
    return this.hc.get(this.url + '/Career/Branch/Placement/Details/' + i, { responseType: 'json' });
}
updatePlacement(updatePlacemnt: placement):Observable<any> {
    return this.hc.post(this.url + '/Career/Branch/Placement/Update/' + updatePlacemnt.Id, JSON.stringify(updatePlacemnt), this.httpOptions)
}
getPlacements(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Career/Branch/Placements/' + this.userSession, { responseType: 'json' })
}
postedJobsOfAllCompanies(id: any): Observable<any> {
    return this.hc.get(this.url + '/Career/All/Jobs/Placement/' + id, { responseType: 'json' });
}
//===================================Projects==================================
saveProject(proje: project): Observable<any> {
    return this.hc.post(this.url + '/Career/Branch/Project/Add', JSON.stringify(proje), this.httpOptions)
}
getprjbybranch(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Career/Branch/Project/' + this.userSession, { responseType: 'json' })
}
getProjectsById(i: number): Observable<any> {
    return this.hc.get(this.url + '/Career/Branch/Project/Details/' + i, { responseType: 'json' })
}
updProject(proje: project): Observable<any> {
    return this.hc.post(this.url + "/Career/Branch/Project/Update/" + proje.Id, JSON.stringify(proje), this.httpOptions)
}
 /**********************JOB*******************************************/
 saveJobs(jobmaster: Job): Observable<any> {
    return this.hc.post(this.url + "/Career/New/Job/Addtion", JSON.stringify(jobmaster), this.httpOptions)
  }
  updateJob(jobmaster: Job): Observable<any> {
    return this.hc.post(this.url + "/Career/Job/Update/" + jobmaster.Id, JSON.stringify(jobmaster), this.httpOptions)
  }
  sendApplication(n: any): Observable<any>  {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Career/Job/Request/' + n + '/' + this.userSession, { responseType: 'json' });
  }
  getApplicantDeatails(id: any): Observable<any> {
    return this.hc.get(this.url + '/Career/Job/Applicants/Deatails/' + id, { responseType: 'json' });
  }
  getParticularBranchApplicants(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + "/Career/view/Branch/Applicants/" + this.userSession, { responseType: 'json' })
  }
  updateApplicantsAggrements(id: number): Observable<any> {
    return this.hc.post(this.url + '/Career/Update/Applicants/Aggrement/' + id, { responseType: 'json' })
  }
  getAggrementsStatus(): Observable<any> {
    
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + "/Career/view/Aggrements/Details/" + this.userSession, { responseType: 'json' })
  }
  acceptJobApplicants(userid: any, placementsid: any, Interview: InterviewSchedule) {
    return this.hc.post(this.url + '/Career/Branch/Accept/JobApplicant/' + userid + '/' + placementsid, JSON.stringify(Interview), this.httpOptions)
  }
  getBranchHr(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + "/Career/Branch/Hr/" + this.userSession, { responseType: 'json' })
  }
  getJobStatus(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Career/Applicants/Job/Status/' + this.userSession, { responseType: 'json' })
  }
  jobApllicantsSchedule(userid: any, placementsid: any): Observable<any>  {
    return this.hc.post(this.url + '/Career/User/Accept/InterviewSchedule/' + userid + '/' + placementsid, { responseType: 'json' })
  }
  getAlljob(): Observable<any> {
    return this.hc.get(this.url + '/Career/Jobs/All/', { responseType: 'json' })
  }
  getRecentJob(): Observable<any> {
    return this.hc.get(this.url + '/Career/View/Recent/Job/', { responseType: 'json' })
  }
  getAllDegree(): Observable<any> {
    return this.hc.get(this.url + '/Career/Qualifiation/All/', { responseType: 'json' })
  }
  getOrganizationName(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Career/Organization/' + this.userSession, { responseType: 'json' })
  }
  editJob(id: number): Observable<any> {
    return this.hc.get(this.url + '/Career/Job/Details/' + id, { responseType: 'json' })
  }
  getJobType(): Observable<any> {
    return this.hc.get(this.url + '/Career/Job/type', { responseType: 'json' })
  }
  getJobByInstitutionId():Observable<any>{
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Career/Job/View/' + this.userSession, { responseType: 'json' });
  }
  getApplicantsPrevInterviewDetails(userid: any): Observable<any> {
    return this.hc.get(this.url + '/Career/User/Previous/InterviewDetails/' + userid, { responseType: 'json' })
  }
}
