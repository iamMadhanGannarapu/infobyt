
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { User } from '../modal/User/User'
import { ContactDetails } from '../modal/User/ContactDetails'
import { Role } from '../modal/User/Role';
import * as config from './service_init.json';
import { Testimonial } from '../modal/User/testimonial';
import * as io from 'socket.io-client'
import { States } from '../modal/Settings/states';
import { Cities } from '../modal/Settings/cities';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { AcademicDetails } from '../modal/User/academicdetails';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  userSession: any;
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  url: string = (<any>config).api;
  user: any;
  chatUrl = "http://localhost:5000"
  socket;
  notificationList: any;
  constructor(private http: HttpClient) { }
  initSession() {
    this.userSession = sessionStorage.getItem('user')
  }
    //===================================Excel======================================
  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }
  //===================================Contact Details======================================
  saveContactDetails(cdobj: ContactDetails): Observable<any> {
    return this.http.post(this.url + '/User/ContactDetails/Add', JSON.stringify(cdobj), this.httpOptions)
  }
  getAllContactDetails(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/User/Users/ContactDetails/' + this.userSession, this.httpOptions)
  }
  getAllReligion():Observable<any>{
    return this.http.get(this.url+'/User/religion',{ responseType: 'json' })
  }
  getAllCountries(): Observable<any> {
    return this.http.get(this.url + '/User/countries', { responseType: 'json' })
  }
  getAllStates(counId: number): Observable<any> {
    return this.http.get(this.url + '/User/states/' + counId, { responseType: 'json' })
  }
  getAllCities(stateId: number): Observable<any> {
    return this.http.get(this.url + '/User/cities/' + stateId, { responseType: 'json' })
  }
  saveStates(state: States): Observable<any> {
    return this.http.post(this.url + '/User/data/State/enter', JSON.stringify(state), this.httpOptions)
  }
  saveCities(city: Cities): Observable<any> {
    return this.http.post(this.url + '/User/data/Cities/enter', JSON.stringify(city), this.httpOptions)
  }
  //=====================================Role Master=====================================
  saveRole(rm: Role):Observable<any> {
    return this.http.post(this.url + '/User/Role/Add', JSON.stringify(rm), this.httpOptions);;
  }
  getAllRoles(): Observable<any> {
    return this.http.get(this.url + '/User/Roles/', { responseType: 'json' })
  }
  getRolesById(Id: number): Observable<any> {
    return this.http.get(this.url + '/User/Role/Details/' + Id, this.httpOptions)
  }
  getRolesByUser(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/User/Role/User/' + this.userSession, this.httpOptions)
  }
  updateRole(rm: Role):Observable<any> {
    return this.http.post(this.url + '/User/Role/Update/' + rm.Id, JSON.stringify(rm), this.httpOptions)
  }
  //=========================================User=================================
  saveUser(usr: User): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.post(this.url + '/User/Add/' + this.userSession, JSON.stringify(usr), this.httpOptions);
  }
  saveAdmin(usr: User, AdminId: number): Observable<any> {
    return this.http.post(this.url + '/User/Add/Admin/' + AdminId, JSON.stringify(usr), this.httpOptions);
  }
  getAllContactDetailsByRole(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/User/Users/ContactDetails/role/BranchAdmin/' + this.userSession, { responseType: 'json' })
  }
  getAllContactDetailsByRoleById(cId:any): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/User/Users/ContactDetails/role/BranchAdmin/' + this.userSession+'/'+cId, { responseType: 'json' })
  }
  getBranchAdmin(): Observable<any>  {
    return this.http.get(this.url + '/User/Users/Branch/', { responseType: 'json' })
  }
  getAllUsers(): Observable<any>  {
    return this.http.get(this.url + '/User/Users/All/', { responseType: 'json' })
  }
  getAllHod():Observable<any>{
   this.initSession();
   for (var i = 0; i < this.userSession.length; i++) {
     this.userSession = this.userSession.replace('/', '-');
   }
   return this.http.get(this.url + '/User/StaffUsers/Staff/Hod/' + this.userSession, { responseType: 'json' })
 } 
  getAllStaffUsers():Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/User/StaffUsers/All/' + this.userSession, { responseType: 'json' })
  }
 /*  getStaffUsers() {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/User/StaffUsers/Details/' + this.userSession, { responseType: 'json' })
  } */
  getAllTrainerFromStaff():Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/User/StaffUsers/Staff/Trainer/' + this.userSession, { responseType: 'json' })
  }
//UserService
getAllTraineeFromApplicants():Observable<any> {
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
    this.userSession = this.userSession.replace('/', '-');
  }
  return this.http.get(this.url + '/User/Applicants/Trainee/' + this.userSession, { responseType: 'json' })
}
  getAllDriversFromApplicants():Observable<any>  {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/User/Applicants/Driver/' + this.userSession, { responseType: 'json' })
  }
  getAllDriversFromApplicantsById(dId:any): Observable<any>  {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/User/Applicants/Driver/Driver/' + this.userSession+'/'+dId, { responseType: 'json' })
  }
  getAllTrainer(): Observable<any>  {
    return this.http.get(this.url + '/User/Users/Trainer/', { responseType: 'json' })
  }
  getAllTrainee(): Observable<any>  {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/User/Users/Trainee/' + this.userSession, { responseType: 'json' })
  }
  getAllDriversDetails(): Observable<any> {
    return this.http.get(this.url + '/User/Users/DriverDetails/', { responseType: 'json' })
  }
  getCityById(cityId: number): Observable<any>  {
    return this.http.get(this.url + '/User/cities/city/' + cityId, { responseType: 'json' })
  }
  getContactByUserId(id: number): Observable<any> {
    return this.http.get(this.url + '/User/ContactDetails/' + id, { responseType: 'json' });
  }
  getAllMiddleNames(): Observable<any> {
    return this.http.get(this.url + '/User/MiddleNames/', { responseType: 'json' })
  }
  getAllOccupations(): Observable<any> {
    return this.http.get(this.url + '/User/Occupations/', { responseType: 'json' })
  }
  getALlMedium():Observable<any> {
    return this.http.get(this.url +'/user/Medium/',{ responseType: 'json' })
  } 
  getAllNationalities(): Observable<any> {
    return this.http.get(this.url + '/User/Nationalities/', { responseType: 'json' })
  }
  getUserbySession(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/User/UserSession/' + this.userSession, { responseType: 'json' })
  }
  getAllStaff(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/User/Staff/Transport/' + this.userSession, { responseType: 'json' })
  }
  getAllStaffUsersForFeedBack(): Observable<any>  {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/User/StaffUsers/FeedBack/' + this.userSession, { responseType: 'json' })
  }
  //=====================================Testimonial=====================================
  saveTestimonial(ts: Testimonial): Observable<any> {
    return this.http.post(this.url + '/User/testimonial/Add', JSON.stringify(ts), this.httpOptions);;
  }
  getAllTestimonial(): Observable<any> {
    return this.http.get(this.url + '/User/testimonial', { responseType: 'json' })
  }
  getImage(id:any): Observable<any> {
    this.http.get(this.url + '/Organization/Images/reviews/'+id, { responseType: 'json' }).pipe(map(res => {
    }));
    return this.http.get(this.url + '/User/testimonial', { responseType: 'json' })
  }
  //======================================================================================
  ChatConnect(uname: string) {
    this.socket = io(this.chatUrl, { query: "userName=" + uname });
  }
  sendMessage(msg: string, fUser: string, toUser: string) {
    let nMsg = msg + "|" + fUser + "|" + toUser;
    this.socket.emit('addmsg', nMsg);
  }
  getOnlineUsesr(): Observable<any> {
    return this.http.get(this.chatUrl, { responseType: 'json' });
  }
  getChatMessage(): Observable<any>  {
    let observable = new Observable<{ user: string, msg: string }>(
      observer => {
        this.socket.on('sendClient', (data) => {
          observer.next(data)
        });
        return () => { this.socket.disconnect(); }
      });
    return observable;
  }
  getSchoolAdmins(): Observable<any> {
    return this.http.get(this.url + '/User/OrganizationAdmins', { responseType: 'json' })
  }
  getLoginbyUserid(userId: any, status: any): Observable<any> {
    return this.http.post(this.url + '/User/login/' + userId + '/' + status, { responseType: 'json' })
  }
//======================================StaffLogs========================================================
getAllStafflogs(): Observable<any> {
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
  }
  return this.http.get(this.url + "/User/StaffLogs/branch/Details/" + this.userSession, { responseType: 'json' })
}
getAllTraineelogs(): Observable<any> {
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
  }
  return this.http.get(this.url + "/User/TraineeLogs/branch/Details/" + this.userSession, { responseType: 'json' })
}
getAllWardens(): Observable<any>{
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
    this.userSession = this.userSession.replace('/', '-');
  }
  return this.http.get(this.url + '/User/StaffUsers/Staff/Warden/' + this.userSession, { responseType: 'json' })
} 
getUser(): Observable<any>{
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
    this.userSession = this.userSession.replace('/', '-');
  }
return this.http.get(this.url+'/User/View/Username/'+this.userSession,this.httpOptions)
}
saveAcademic(academic:AcademicDetails): Observable<any>{
  return this.http.post(this.url+'/User/Add/Applicant/Academicdetails', JSON.stringify(academic), this.httpOptions)
}
updatedAcademic(academic:AcademicDetails){
  return this.http.post(this.url+'/User/Update/Applicant/Academicdetails', JSON.stringify(academic), this.httpOptions)
}
getUserAcademicDetailsBysession() : Observable<any>{
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
    this.userSession = this.userSession.replace('/', '-');
  }
  return this.http.get(this.url + '/User/Academic/Details/' + this.userSession, { responseType: 'json' });
}
  getCompByMod(inp:string):Observable<any>{
    return this.http.get(this.url + '/User/Module/Component/' + inp, { responseType: 'json' })
  }
getAllRole(): Observable<any> {
    return this.http.get(this.url + "/User/AllRoles", { responseType: 'json' })
}
//user service
getAllFromApplicantsForTrainees():Observable<any> {
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
    this.userSession = this.userSession.replace('/', '-');
  }
  return this.http.get(this.url + '/User/Trans/Trainee/' + this.userSession, { responseType: 'json' })
}
getAllClassTeacher():Observable<any> {
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
    this.userSession = this.userSession.replace('/', '-');
  }
  return this.http.get(this.url + '/User/Users/ClassTeacher/' + this.userSession, { responseType: 'json' })
}

getAllHr(): Observable<any>{
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
    this.userSession = this.userSession.replace('/', '-');
  }
  return this.http.get(this.url + '/User/StaffUsers/Staff/HR/' + this.userSession, { responseType: 'json' })
} 

}