import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Salary } from '../modal/Salary/Salary';
import { SalaryHike } from '../modal/Salary/SalaryHike';
import { SalaryPaymentsInfo } from '../modal/Salary/SalaryPayment';
import { LeaveDetails } from '../modal/Salary/LeaveDetails';
import * as config from './service_init.json';
import { Identifiers } from '@angular/compiler';
import { AccountDetails } from '../modal/Fee/AccountDetails'
import { PaySlip } from '../modal/Salary/PaySlip';
import { LeaveMaster } from '../modal/Batch/leavemaster';
import { HalfLoginRequest } from '../modal/Salary/HalfLoginrequest';
import { PayRoll } from '../modal/Salary/payroll';
import { DepartmentPolicies } from '../modal/Salary/DepartmentPolicies';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { Tds } from '../modal/Salary/Tds';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Injectable({
  providedIn: 'root'
})
export class Salaryservice {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }
  userSession: any;
  sallaryPattern: SallaryPattern;
  url: string = (<any>config).api;
  constructor(private http: HttpClient) {
    this.sallaryPattern = new SallaryPattern()
  }
  initSession() {
    this.userSession = sessionStorage.getItem('user')
  }
    //=====================================Excel====================================
  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }
  //=====================================Leave Details====================================
  //Done
  saveLeave(ld: LeaveDetails): Observable<any>  {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.post(this.url + '/Salary/User/Leave/Add/' + this.userSession, JSON.stringify(ld), this.httpOptions)
  }
//Done
  getAllLeaves(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/Salary/User/Leave/' + this.userSession, { responseType: 'json' })
  }
  getLeaveById(Id: number): Observable<any> {
    return this.http.get(this.url + '/Salary/User/Leave/Details/' + Id, { responseType: 'json' })
  }
  updateLeaveDetails(leaveDetails: LeaveDetails): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {    
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.post(this.url + '/Salary/User/Leave/Update/' + leaveDetails.Id + '/' + this.userSession, JSON.stringify(leaveDetails), this.httpOptions)
  }
  saveLeavePattern(leaveList: any): Observable<any> {
    this.sallaryPattern.lL = leaveList;
  return this.http.post(this.url + '/Organization/leavepattern/Add', JSON.stringify(this.sallaryPattern.lL), this.httpOptions)
}
 saveTypes(lm: LeaveMaster): Observable<any> {
  let x = this.http.post(this.url + '/Salary/leaveType/Add', JSON.stringify(lm), this.httpOptions)
  return x;
}
  //================================Salary Hike Master===============================================================
  //Done
  saveSalaryHike(salary: SalaryHike): Observable<any>  {
    return this.http.post(this.url + '/Salary/Salary/Upgrade', JSON.stringify(salary), this.httpOptions)
  }
  //Done
  getsalaryhiketransactionsbyUserId(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/Salary/salary/hike/transactions/' + this.userSession, { responseType: 'json' })
  }
  //Done
  getAllsalaryhike(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/Salary/Salary/Upgrade/All/' + this.userSession, { responseType: 'json' })
  }
  //Done
  getSalaryHikeById(Id: number): Observable<any> {
    return this.http.get(this.url + '/Salary/Salary/Upgrade/details/' + Id, { responseType: 'json' })
  }
  //Done
 sendHike(uid:any,id:any): Observable<any>{
  return this.http.post(this.url + '/Salary/Salary/Hike/Mail/Send/'+uid+'/'+id, this.httpOptions)
}
  //==================================Salary Master================================================================
 //Done
  saveSalary(sl: Salary): Observable<any> {
    return this.http.post(this.url + '/Salary/Salary/Add', JSON.stringify(sl), this.httpOptions)
  }
//Done
  getsalarytransactionsbyUserId(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/Salary/salary/transactions/' + this.userSession, { responseType: 'json' })
  }
  //Done
  getSalaryById(id: any): Observable<any> {
    return this.http.get(this.url + '/Salary/Salary/details/' + id, { responseType: 'json' });
  }
  //Done
  getAllSalary(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/Salary/Salary/' + this.userSession, { responseType: 'json' })
  }
   //Done
saveHalfLeaveRequest(hlr:HalfLoginRequest): Observable<any> {
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
    this.userSession = this.userSession.replace('/', '-');
  }
  return this.http.post(this.url + '/Salary/User/Half/Login/Add/' + this.userSession, JSON.stringify(hlr), this.httpOptions)
}
 //Done
getAllHalfLoginRequests(): Observable<any> {
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
    this.userSession = this.userSession.replace('/', '-');
  }
  return this.http.get(this.url + '/Salary/Half/Login/Request/' + this.userSession, { responseType: 'json' })
}
 //Done
gethalfLoginRequestById(Id: number): Observable<any> {
  return this.http.get(this.url + '/Salary/Half/Request/Details/' + Id, { responseType: 'json' })
}
 //Done
updateHalfLoginRequestDetails(hlf: HalfLoginRequest): Observable<any> {
  return this.http.post(this.url + '/Salary/Half/Login/Request/Update/' + hlf.Id , JSON.stringify(hlf), this.httpOptions)
}
  //Left
  updateSalary(sl: Salary): Observable<any> {
    return this.http.post(this.url + '/Staff/Salary/Update/' + sl.Id, sl, this.httpOptions)
  }
//Done
//=================== payroll details=============================
//Done
savePayroll(pay:PayRoll): Observable<any> {
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
    this.userSession = this.userSession.replace('/', '-');
  }
  return this.http.post(this.url + '/Salary/salary/payroll/Add/'+ this.userSession, JSON.stringify(pay), this.httpOptions)
}
//Done
getAllPayrolls(): Observable<any> {
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
    this.userSession = this.userSession.replace('/', '-');
  }
  return this.http.get(this.url + '/Salary/Branch/salary/payroll/' + this.userSession, { responseType: 'json' })
}
//Done
saveDepartmentAllowences(dept:DepartmentPolicies,stfList:any): Observable<any> {
  alert
  return this.http.post(this.url + '/Salary/salary/Department/Policies/Add/', {dept:dept,stfList:stfList}, this.httpOptions)
}
//Done
getAllStaffByDepartment(did:any): Observable<any> {
  return this.http.get(this.url + '/Salary/Branch/polici/staff/department/' + did, { responseType: 'json' })
}
//Done
getAllDepartmentPolicies(): Observable<any> {
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
    this.userSession = this.userSession.replace('/', '-');
  }
  return this.http.get(this.url + '/Salary/Branch/staff/department/policy/' + this.userSession , { responseType: 'json' })
}
  //=================== account details======================
  saveAccountDetails(sl: AccountDetails): Observable<any> {
    return this.http.post(this.url + '/Salary/Add/account/details', JSON.stringify(sl), this.httpOptions)
  }
  //====================================Salary Payment====================================
  //Done
  saveSalaryPayment(spm: SalaryPaymentsInfo): Observable<any>  {
    return this.http.post(this.url + '/Salary/Staff/Salary/Add', JSON.stringify(spm), this.httpOptions)
  }
  //Done
  getSalaryPaymentById(Id: number): Observable<any> {
    return this.http.get(this.url + '/Salary/Staff/Salary/Details/' + Id, { responseType: 'json' })
  }
  //Done
  getSalaryPaymentByStaffId(Id: number, month: any): Observable<any> {
    return this.http.get(this.url + '/Salary/Staff/Salary/Staff/' + Id + '/' + month, { responseType: 'json' })
  }
//Done
getAllSalaryPayment(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/Salary/Staff/Salary/' + this.userSession, { responseType: 'json' })
  }
  //================================================Pay Slip===================================
  //Done
  generateGrossSalary(payDetails: any, month: any): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.post(this.url + '/Salary/Staff/Salary/GrossSalary/' + month + '/' + this.userSession, JSON.stringify(payDetails), this.httpOptions)
  }
//Done
  addPayDetails(payDetails:any,month:any):Observable<any>{
    return this.http.post(this.url+'/Salary/Paydetails/Add/'+month,payDetails,this.httpOptions)
  }
  //Done
  getattendanceDetailsOfUser(month: any): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/Salary/Staff/AttendanceDetails/PaySlip/' + month + '/' + this.userSession, { responseType: 'json' })
  }
//Done
  getPaySlipOfUser(userid: any): Observable<any> {
    return this.http.get(this.url + '/Salary/Staff/Salary/PaySlip/' + userid, { responseType: 'json' })
  }
//Done
  getSalaryAllowancesOfUser(userid: number): Observable<any> {
    return this.http.get(this.url + '/Salary/Staff/Salary/SalaryAllowances/' + userid, { responseType: 'json' })
  }
//Done
  generatePaySlip(list1: any, list2: any, list3: any): Observable<any> {
    return
  }
  //Done
  sendPaySlip(paySlip: PaySlip): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.post(this.url + '/Salary/Staff/Salary/PaySlip/Send/'+ this.userSession, JSON.stringify(paySlip), this.httpOptions)
  }
//Done
  addExtraAllowances(addExtraAllowances:any): Observable<any>
  {
    return this.http.post(this.url+'/Salary/Add/ExtraAllowances',addExtraAllowances,this.httpOptions)
  }
//Done
  updateExtraAllowances(userId:any,month:any): Observable<any>
  {
    return this.http.post(this.url+'/Salary/Update/ExtraAllowances/'+userId+'/'+month,this.httpOptions)
  }
//Done
  isPayslipGenerated(): Observable<any> {
    return this.http.get(this.url+'/Salary/Staff/Ispayslip/',{ responseType: 'json' })
}
  /*========================================Salary Pattern and Salary Allowances==============================================*/
  //Done
  getexitsname(key:any):Observable<any>{
    return this.http.post(this.url+ '/Salary/SalaryPattern/patternnamename/',{key:key}, this.httpOptions)
  }
  //Done
  getSalaryPattern(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/Salary/Staff/SalaryPattern/'+this.userSession, { responseType: 'json' })
  }
  //Done
  getLeavePatternDetailsById(id: any): Observable<any> {
    return this.http.get(this.url + '/Salary/Staff/LeavePattern/Details/All/' + id, { responseType: 'json' })
  }
//Done
  //Leave Master
  getLeavemaster(): Observable<any> {
    return this.http.get(this.url + '/Salary/leave/get', { responseType: 'json' })
  }
  //Done
  getSalaryPatternDetailsById(id: any): Observable<any> {
    return this.http.get(this.url + '/Salary/Staff/SalaryPattern/Details/All/' + id, { responseType: 'json' })
  }
  //Done
  addAllowancesSalaryPattern(structureName: any, AllowancesList: any, DeductionsList: any, leaveList: any): Observable<any> {
    this.sallaryPattern.aL = AllowancesList;
    this.sallaryPattern.dL = DeductionsList;
    this.sallaryPattern.lL = leaveList;
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.post(this.url + '/Salary/Staff/Branch/SalaryPattern/Add/' + structureName +'/'+this.userSession, JSON.stringify(this.sallaryPattern), this.httpOptions)
  }
  getSalaryPatternDetails(): Observable<any> {
    return this.http.get(this.url + '/Organization/Staff/SalaryPattern/Details', { responseType: 'json' })
  }
 //=================== TDS details=============================
 //Done
 saveTds(td:Tds): Observable<any> {
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
    this.userSession = this.userSession.replace('/', '-');
  }
  return this.http.post(this.url + '/Salary/salary/tds/Add/'+ this.userSession, JSON.stringify(td), this.httpOptions)
}
getAllTds(): Observable<any> {
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
    this.userSession = this.userSession.replace('/', '-');
  }
  return this.http.get(this.url + '/Salary/Branch/tax/tds/' + this.userSession, { responseType: 'json' })
}
/* ******************************************Reports************** */
getAllPfReports(): Observable<any> {
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
    this.userSession = this.userSession.replace('/', '-');
  }
  return this.http.get(this.url + '/Salary/Branch/Deduction/Pf/' + this.userSession, { responseType: 'json' })
}
getAllEsiReports(): Observable<any> {
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
    this.userSession = this.userSession.replace('/', '-');
  }
  return this.http.get(this.url + '/Salary/Branch/Deduction/esi/' + this.userSession, { responseType: 'json' })
}
getAllPtReports(): Observable<any> {
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
    this.userSession = this.userSession.replace('/', '-');
  }
  return this.http.get(this.url + '/Salary/Branch/Deduction/Pt/' + this.userSession, { responseType: 'json' })
}
getAllTdsReports(): Observable<any> {
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
    this.userSession = this.userSession.replace('/', '-');
  }
  return this.http.get(this.url + '/Salary/Branch/Deduction/tds/' + this.userSession, { responseType: 'json' })
}

getPaySlips(Id:number): Observable<any> {
  return this.http.get(this.url + '/salary/Public/Payslip/'+ Id, { responseType: 'json' })
}
}
export class SallaryPattern {
  aL: any;
  dL: any;
  lL:any;
}
