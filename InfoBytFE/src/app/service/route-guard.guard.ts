import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { SignupComponent } from '../login/signup/signup.component';
import { BatchComponent } from '../batch/batch/batch.component';
import { HolidayMasterComponent } from '../attendance/holiday-master/holiday-master.component';
import { LeaveDetailsComponent } from '../attendance/leave-details/leave-details.component';
import { AppraisalTimetableComponent } from '../appraisal/appraisal-timetable/appraisal-timetable.component';
import { SessionSubjectMasterComponent } from '../batch/session-subject/session-subject.component';
import { StaffComponent } from '../batch/staff/staff.component';
import { SessionMasterComponent } from '../batch/session-master/session-master.component';
import { TraineetMarksComponent } from '../appraisal/trainee-marks/trainee-marks.component';
import { SubjectComponent } from '../batch/subject/subject.component';
import { TeamComponent } from '../batch/team/team.component';
import { TeamallocationComponent } from '../batch/teamallocation/teamallocation.component';
import { TimeTableComponent } from '../batch/time-table/time-table.component';
import { TrainerSubjectComponent } from '../batch/trainer-subject/trainer-subject.component';
import { AllocatePlacementComponent } from '../career/allocate-placement/allocate-placement.component';
import { AllocateProjectComponent } from '../career/allocate-project/allocate-project.component';
import { JobComponent } from '../career/job/job.component';
import { PlacementsComponent } from '../career/placements/placements.component';
import { ProjectComponent } from '../career/project/project.component';
import { RecruitmentComponent } from '../career/recruitment/recruitment.component';
import { DashboardComponent } from '../dashboard/dashboard/dashboard.component';
import { FeedbackComponent } from '../dashboard/feedback/feedback.component';
import { OrganizationFeedbackComponent } from '../dashboard/organization-feedback/organization-feedback.component';
import { ShoppingComponent } from '../dashboard/shopping/shopping.component';
import { ConcessionComponent } from '../fee/concession/concession.component';
import { FeePaymentsComponent } from '../fee/fee-payments/fee-payments.component';
import { FeesComponent } from '../fee/fees/fees.component';
import { FeestructureComponent } from '../fee/feestructure/feestructure.component';
import { ForgotPasswordComponent } from '../login/forgot-password/forgot-password.component';
import { AchievementsComponent } from '../organization/achievements/achievements.component';
import { AdmisionDetailsComponent } from '../organization/admision-details/admision-details.component';
import { BranchComponent } from '../organization/branch/branch.component';
import { BranchSessionrComponent } from '../organization/branch-session/branch-session.component';
import { CoursesComponent } from '../organization/courses/courses.component';
import { DepartmentComponent } from '../organization/department/department.component';
import { EventsComponent } from '../organization/events/events.component';
import { OrganizationComponent } from '../organization/organization/organization.component';
import { ShiftsComponent } from '../organization/shifts/shifts.component';
import { PayrollComponent } from '../salary/payroll/payroll.component';
import { SalaryComponent } from '../salary/salary/salary.component';
import { SalaryHikeComponent } from '../salary/salary-hike/salary-hike.component';
import { SalaryPatternComponent } from '../salary/salary-pattern/salary-pattern.component';
import { SalaryPaymentComponent } from '../salary/salary-payment/salary-payment.component';
import { HostelComponent } from '../services/hostel/hostel.component';
import { HostelfeeComponent } from '../services/hostelfee/hostelfee.component';
import { Library } from '../modal/Services/Library';
import { SportsComponent } from '../services/sports/sports.component';
import { BillingComponent } from '../settings/billing/billing.component';
import { ChangepasswordComponent } from '../settings/changepassword/changepassword.component';
import { InstituteComponent } from '../settings/institute/institute.component';
import { PermissionComponent } from '../settings/permission/permission.component';
import { RoleMasterComponent } from '../settings/role-master/role-master.component';
import { BusDriverAllocation } from '../modal/Transport/BusDriverAllocation';
import { BusMasterComponent } from '../transport/bus-master/bus-master.component';
import { TransportFeeMasterComponent } from '../transport/transport-fee-master/transport-fee-master.component';
import { TransportRouteMasterComponent } from '../transport/transport-route-master/transport-route-master.component';
import { TransportStopsComponent } from '../transport/transport-stops/transport-stops.component';
import { UserTransportComponent } from '../transport/user-transport/user-transport.component';
import { UsertransportPaymentComponent } from '../transport/user-transport-payment/user-transport-payment.component';
import { ContactDetailsComponent } from '../user/contact-details/contact-details.component';
import { UsersComponent } from '../user/users/users.component';
import { AttendanceComponent } from '../attendance/attendance/attendance.component';
import { SurvelinceComponent } from '../attendance/survelince/survelince.component';
import { UsermappingComponent } from '../attendance/usermapping/usermapping.component';
import { BulkmessageComponent } from '../dashboard/bulkmessage/bulkmessage.component';
import { IdcardComponent } from '../dashboard/idcard/idcard.component';
import { HomeComponent } from '../home/home.component';
import { TestimonialComponent } from '../login/testimonial/testimonial.component';
import { AllocateShiftComponent } from '../organization/allocate-shift/allocate-shift.component';
import { TransferComponent } from '../organization/transfer/transfer.component';
import { TdsComponent } from '../salary/tds/tds.component';
import { LibraryComponent } from '../services/library/library.component';
import { BusDriverAlocationComponent } from '../transport/bus-driver-alocation/bus-driver-alocation.component';
@Injectable({
  providedIn: 'root'
})
export class RouteGuardGuard implements CanActivate {
  constructor(private router: Router)
  {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if (sessionStorage.getItem('user') != null) {
return true;
      }
      this.router.navigate(['404']);
    return false;
  }
}
export class CanDeactivateGuard implements CanDeactivate<SignupComponent | BatchComponent | HolidayMasterComponent  | LeaveDetailsComponent | AppraisalTimetableComponent | TraineetMarksComponent | SessionMasterComponent | SessionSubjectMasterComponent | StaffComponent | SubjectComponent | TeamComponent | TeamallocationComponent | TimeTableComponent  | TrainerSubjectComponent  | AllocatePlacementComponent | AllocateProjectComponent | JobComponent | PlacementsComponent | ProjectComponent | RecruitmentComponent  | DashboardComponent | FeedbackComponent  | OrganizationFeedbackComponent | ShoppingComponent  | ConcessionComponent | FeePaymentsComponent | FeesComponent | FeestructureComponent | ForgotPasswordComponent  | AchievementsComponent | AdmisionDetailsComponent | BranchComponent | BranchSessionrComponent | CoursesComponent | DepartmentComponent | EventsComponent | OrganizationComponent | ShiftsComponent | PayrollComponent  | SalaryComponent | SalaryHikeComponent | SalaryPatternComponent | SalaryPaymentComponent  | HostelComponent | HostelfeeComponent | LibraryComponent | SportsComponent | BillingComponent | ChangepasswordComponent  | InstituteComponent | PermissionComponent | RoleMasterComponent | BusDriverAlocationComponent | BusMasterComponent | TransportFeeMasterComponent | TransportRouteMasterComponent | TransportStopsComponent | UserTransportComponent | UsertransportPaymentComponent | ContactDetailsComponent | UsersComponent|SurvelinceComponent|UsermappingComponent|BulkmessageComponent|IdcardComponent|HomeComponent|ForgotPasswordComponent|TestimonialComponent|AllocateShiftComponent|CoursesComponent|TransferComponent|TdsComponent> {
  canDeactivate(component: SignupComponent | BatchComponent | HolidayMasterComponent  | LeaveDetailsComponent | AppraisalTimetableComponent | TraineetMarksComponent | SessionMasterComponent | SessionSubjectMasterComponent | StaffComponent | SubjectComponent | TeamComponent | TeamallocationComponent | TimeTableComponent  | TrainerSubjectComponent  | AllocatePlacementComponent | AllocateProjectComponent | JobComponent | PlacementsComponent | ProjectComponent | RecruitmentComponent  | DashboardComponent | FeedbackComponent  | OrganizationFeedbackComponent | ShoppingComponent  | ConcessionComponent | FeePaymentsComponent | FeesComponent | FeestructureComponent | ForgotPasswordComponent  | AchievementsComponent | AdmisionDetailsComponent | BranchComponent | BranchSessionrComponent | CoursesComponent | DepartmentComponent | EventsComponent | OrganizationComponent | ShiftsComponent | PayrollComponent  | SalaryComponent | SalaryHikeComponent | SalaryPatternComponent | SalaryPaymentComponent  | HostelComponent | HostelfeeComponent | LibraryComponent | SportsComponent  | BillingComponent | ChangepasswordComponent  | InstituteComponent | PermissionComponent | RoleMasterComponent | BusDriverAlocationComponent | BusMasterComponent | TransportFeeMasterComponent | TransportRouteMasterComponent | TransportStopsComponent | UserTransportComponent | UsertransportPaymentComponent | ContactDetailsComponent | UsersComponent|SurvelinceComponent|UsermappingComponent|BulkmessageComponent|IdcardComponent|HomeComponent|ForgotPasswordComponent|TestimonialComponent|AllocateShiftComponent|CoursesComponent|TransferComponent|TdsComponent) {

    if (component.form.dirty) {
      return confirm('Are you sure want to exit from this form')
    }
    return true;
  }
}
export class CanDeactivateGuard1 implements CanDeactivate<UsersComponent|SignupComponent|InstituteComponent|SportsComponent|LibraryComponent|CoursesComponent|TraineetMarksComponent|AttendanceComponent|RecruitmentComponent> {
  canDeactivate(component: UsersComponent|SignupComponent|TraineetMarksComponent|InstituteComponent|SportsComponent|LibraryComponent|CoursesComponent|AttendanceComponent|RecruitmentComponent) {

    if (component.form1.dirty) {
      return confirm('Are you sure want to exit from this form')
    }
    return true;
  }
}     
export class CanDeactivateGuard2 implements CanDeactivate<TraineetMarksComponent|InstituteComponent|LibraryComponent> {
  canDeactivate(component: TraineetMarksComponent|LibraryComponent|InstituteComponent) {

    if (component.form2.dirty) {
      return confirm('Are you sure want to exit from this form')
    }
    return true;
  }
}    
export class CanDeactivateGuard3 implements CanDeactivate<TraineetMarksComponent> {
  canDeactivate(component:TraineetMarksComponent) {

    if (component.form3.dirty) {
      return confirm('Are you sure want to exit from this form')
    }
    return true;
  }
}            