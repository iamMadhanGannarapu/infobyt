import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { TraineeMarks } from '../modal/Exam/TraineesMarks';
import { Observable } from 'rxjs';
import { AppraisalTimetable } from '../modal/Exam/AppraisalTimetable';
import * as config from './service_init.json';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Injectable({
  providedIn: 'root'
})
export class ExamService {
  userSession: any;
  BaseUrl: string = (<any>config).api;
  httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) }
  constructor(private http: HttpClient) {
  }
  initSession() {
    this.userSession = sessionStorage.getItem('user')
  }
    //=======================================Excel=================================================
  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }
  //=======================================Exam TimeTable=================================================
 //done ++
  saveTimeTable(exam: AppraisalTimetable) : Observable<any> {
  return this.http.post(this.BaseUrl + '/Exam/Session/Appraisal/TimeTable/Add', JSON.stringify(exam), this.httpOptions);
}
//done ++
getAppraisalTimeTables() : Observable<any>  {
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
    this.userSession = this.userSession.replace('/', '-');
  }
  return this.http.get(this.BaseUrl + '/Exam/Session/Appraisal/TimeTable/' + this.userSession, { responseType: 'json' });
}
//done ++
getAppraisalTimetableById(id: number): Observable<any>  {
  return this.http.get(this.BaseUrl + '/Exam/Session/Appraisal/TimeTable/Details/' + id, { responseType: 'json' });
}
// done ++
updateTimetable(timetable: AppraisalTimetable): Observable<any> {
  return this.http.post(this.BaseUrl + '/Exam/Session/Appraisal/TimeTable/Update/' + timetable.Id, JSON.stringify(timetable), this.httpOptions);
}
  //===================================Student Marks======================================================
 //done ++
  saveMarks(stu: TraineeMarks): Observable<any> {
  return this.http.post(this.BaseUrl + '/Exam/Trainee/Marks/Add', JSON.stringify(stu), this.httpOptions)
}
//done
getAllMarks(): Observable<any> {
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
    this.userSession = this.userSession.replace('/', '-');
  }
  return this.http.get(this.BaseUrl + '/Exam/Trainee/Marks/' + this.userSession, { responseType: 'json' });
}
//not found
getReportByTrainee(aid:number,eid:number):Observable<any>{
  return this.http.get(this.BaseUrl + '/Exam/trainee/marks/Details/Report/' + aid +'/'+ eid, { responseType: 'json' });
}
//done ++
getMarksById(n: number): Observable<any> {
  return this.http.get(this.BaseUrl + '/Exam/trainee/marks/Details/' + n, { responseType: 'json' });
}
//done++
getTrainees(n: number): Observable<any> {
  return this.http.get(this.BaseUrl + '/Exam/Batch/traineesdetails/Details/' + n, { responseType: 'json' });
}
//done++
getAppraisalSubjects(n: number): Observable<any> {
  return this.http.get(this.BaseUrl + '/Exam/Batch/traineeappraisals/Details/' + n, { responseType: 'json' });
}
//done++
getAppraisalSubjectsById(n: number): Observable<any> {
  return this.http.get(this.BaseUrl + '/Exam/Batch/traineeappraisals/subject/Details/' + n, { responseType: 'json' });
}
//done++
updateMarks(sm: TraineeMarks): Observable<any> {
  return this.http.post(this.BaseUrl + '/Exam/Trainee/Marks/Update/' + sm.Id, sm, this.httpOptions)
}
//done++
getSubjectnamesByBatch(id:number,eid:number): Observable<any>{
  return this.http.get(this.BaseUrl+'/Exam/Session/Appraisal/TimeTable/Batch/subject/Details/'+ id +'/'+eid, { responseType: 'json' });
}
//done++
getSessionMarks(bid: number,eid:number):Observable<any> {
  return this.http.get(this.BaseUrl + '/Exam/trainee/marks/batch/exam/' + bid +'/'+ eid, { responseType: 'json' });
}
//done ++
getMarksByAdmissionnum(n: number,m:number): Observable<any> {
  return this.http.get(this.BaseUrl + '/Exam/Trainee/Total/MarksDetails/' + n +'/'+ m, { responseType: 'json' });
}
//done++
getAppraisalTimetableByBatchId(id: number):Observable<any>  {
  return this.http.get(this.BaseUrl + '/Exam/Session/Appraisal/TimeTable/Batch/Details/' + id, { responseType: 'json' });
}
}