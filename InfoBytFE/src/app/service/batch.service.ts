import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpHandler, HttpInterceptor, HttpRequest, HttpEvent } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Batch } from "../modal/Batch/Batch";
import { Subject } from '../modal/Batch/Subject'
import { SessionSubject } from '../modal/Batch/SessionSubject';
import { OrganizationFeedback } from '../modal/Dashboard/OrganizationFeedback';
import { Staff } from '../modal/Batch/Staff';
import { Feedback } from '../modal/Dashboard/Feedback';
import { TrainerSubject } from '../modal/Batch/TrainerSubject';
import { Session } from '../modal/Batch/SessionMaster';
import { TimeTable } from '../modal/Batch/TimeTable';
import * as config from './service_init.json';
import { TraineeBatch } from '../modal/Batch/TraineeBatch';
import { StaffShifts } from '../modal/Batch/staffShifts ';
import { AccountDetails } from '../modal/Fee/AccountDetails';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { TeamAllocation } from '../modal/Batch/TeamAllocation';
import { Team } from '../modal/Batch/Team';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Injectable({
  providedIn: 'root'
})
export class BatchService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  url: string = (<any>config).api;
  userSession: any;
  constructor(private hc: HttpClient) {
  }
  initSession() {
    this.userSession = sessionStorage.getItem('user')
  }
  //*********************************Excel***************************** */
  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }
  //*********************************Batch Master***************************** */
  saveBatch(b: Batch): Observable<any> {
    return this.hc.post(this.url + "/Batch/Batch/Add", JSON.stringify(b), this.httpOptions)
  }
  getAllBatch(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Batch/Batch/' + this.userSession, { responseType: 'json' });
  }
  getBatchById(n: number): Observable<any> {
    return this.hc.get(this.url + '/Batch/Batch/Details/' + n, { responseType: 'json' })
  }
  getBatchBySessionId(id: number):Observable<any> {
    return this.hc.get(this.url + '/Batch/Batch/Session/All/' + id, { responseType: 'json' });
  }
  getBatchDetailsBySession(n: number): Observable<any> {
    return this.hc.get(this.url + '/Batch/Batch/Session/Batch/' + n, { responseType: 'json' })
  }
  updateBatch(b: Batch): Observable<any> {
    return this.hc.post(this.url + '/Batch/Batch/Update/' + b.Id, JSON.stringify(b), this.httpOptions)
  }
  getBatchByUserId(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Batch/transactions/' + this.userSession, { responseType: 'json' })
  }
  //===================================Class Master===================================
  saveSession(c: Session): Observable<any> {
    let x = this.hc.post(this.url + '/Batch/Session/Add', JSON.stringify(c), this.httpOptions)
    return x;
  }
  getAllSession(): Observable<any> {
    return this.hc.get(this.url + '/Batch/Session/', { responseType: 'json' });
  }
  getSessionById(n: number): Observable<any> {
    return this.hc.get(this.url + '/Batch/Session/Details/' + n, { responseType: 'json' })
  }
  updateSession(cm: Session): Observable<any> {
    return this.hc.post(this.url + '/Batch/Session/Update/' + cm.Id, JSON.stringify(cm), this.httpOptions)
  }
  //===============================Class Subject==================================
  saveSessionSubject(c: SessionSubject): Observable<any> {
    return this.hc.post(this.url + "/Batch/Session/Subject/Add", JSON.stringify(c), this.httpOptions)
  }
  getAllSessionSubject(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Batch/Session/Subject/' + this.userSession, { responseType: 'json' });
  }
  getSessionSubjctById(n: number): Observable<any> {
    return this.hc.get(this.url + '/Batch/Session/Subject/Details/' + n, { responseType: 'json' })
  }
  updateSessionSubject(cs: SessionSubject): Observable<any> {
    return this.hc.post(this.url + '/Batch/Session/Subject/Update/' + cs.Id, JSON.stringify(cs), this.httpOptions)
  }
  getSessionSubjectByBranch(Id: number): Observable<any> {
    return this.hc.get(this.url + '/Batch/Branch/Session/Subject/Details/' + Id, { responseType: 'json' });
  }
  //=================================Faculty Subject================================
  saveTrainerSubject(facObj: TrainerSubject): Observable<any> {
    return this.hc.post(this.url + "/Batch/Session/Subject/Trainer/Add", JSON.stringify(facObj), this.httpOptions)
  }
  getTrainerSubject(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Batch/Session/Subject/Trainer/' + this.userSession, { responseType: 'json' })
  }
  getTrainerSubjectById(id: number): Observable<any> {
    return this.hc.get(this.url + '/Batch/Session/Subject/Trainer/Details/' + id, { responseType: 'json' });
  }
  getTrainerSubjectBySubjectId(id: number): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Batch/Session/Subject/Trainer/Subject/' + id + '/' + this.userSession, { responseType: 'json' });
  }
  updateTrainerSubject(fs: TrainerSubject): Observable<any> {
    return this.hc.post(this.url + '/Batch/Session/Subject/Trainer/Update/' + fs.Id, JSON.stringify(fs), this.httpOptions)
  }
  //===================================Staff Master===============================
  saveStaff(staff1: Staff, salaryPattern: any): Observable<any> {
  
    return this.hc.post(this.url + "/Batch/Staff/Add/" + salaryPattern, staff1, this.httpOptions);
  }
  getAllStaff(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Batch/Staff/' + this.userSession, { responseType: 'json' })
  }
  getStaffById(id: number): Observable<any> {
    return this.hc.get(this.url + '/Batch/Staff/Details/' + id, { responseType: 'json' });
  }
  updateStaff(t: Staff, c: AccountDetails): Observable<any> {
    let data = Object.assign(t, c);
    return this.hc.post(this.url + '/Batch/Staff/Update/' + t.Id, JSON.stringify(data), this.httpOptions);
  }
  verifybankData(inp: string, inpData: string): Observable<any>  {
    return this.hc.get(this.url + '/Batch/Staff/Verify/pfnum/' + inp + '/' + inpData, this.httpOptions)
  }
  verifyRole(rid:any):Observable<any>{
    return this.hc.get(this.url + '/Batch/Staff/Verify/Role/' + rid, this.httpOptions)   
   }
  //==============================Student Batch===========================================================================
  saveTraineeBatch(studentBatch: TraineeBatch): Observable<any> {
    return this.hc.post(this.url + '/Batch/Trainee/Batch/Add', JSON.stringify(studentBatch), this.httpOptions)
  }
  getTraineeBatch(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Batch/Trainee/Batch/' + this.userSession, { responseType: 'json' })
  }
  getBatchBySession(n: number): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Batch/Batch/sessionname/Details/' + this.userSession + '/' + n, { responseType: 'json' })
  }
  //==============================Subject Master========================================
  saveSubject(m: Subject): Observable<any> {
    return this.hc.post(this.url + "/Batch/Subject/Add", JSON.stringify(m), this.httpOptions)
  }
  getAllSubject(): Observable<any> {
    return this.hc.get(this.url + '/Batch/Subject/', { responseType: 'json' });
  }
  getSubjectById(id: number): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Batch/Subject/Details/' + id + this.userSession, { responseType: 'json' });
  }
  updateSubject(sb: Subject): Observable<any> {
    return this.hc.post(this.url + '/Batch/Subject/Update/' + sb.Id, JSON.stringify(sb), this.httpOptions)
  }
  //==================================Time Table=================================
  saveTimetable(rt: TimeTable): Observable<any> {
    return this.hc.post(this.url + '/Batch/Session/Timetable/Add',
      JSON.stringify(rt), this.httpOptions)
  }
  getAllTimetable(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Batch/Session/Timetable/' + this.userSession, { responseType: 'json' });
  }
  getTimetableById(n: number): Observable<any> {
    return this.hc.get(this.url + '/Batch/Session/Timetable/Details/' + n, { responseType: 'json' })
  }
  updateTimetable(tt: TimeTable): Observable<any> {
    return this.hc.post(this.url + '/Batch/Session/Timetable/Update/' + tt.Id, JSON.stringify(tt), this.httpOptions)
  }
  //===========================Shifts===============================
  saveShift(shift: StaffShifts): Observable<any> {
    return this.hc.post(this.url + "Batch/Staff/Add/Shift/details/", shift, this.httpOptions);
  }

  //===team and allocations==



  saveTeam(c:Team): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    console.log(c);
    return this.hc.post(this.url + '/Batch/Team/Add/'+this.userSession, JSON.stringify(c), this.httpOptions)
  
  }
  
  
  
  
getAllTeam(): Observable<any> {
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
    this.userSession = this.userSession.replace('/', '-');
  }
  return this.hc.get(this.url + '/Batch/Team/' + this.userSession, { responseType: 'json' });
}		




getTeamByDepartment(deptId:any):Observable<any> {

return this.hc.get(this.url + '/Batch/Team/Department/'+deptId, this.httpOptions)

}



saveTeamAllocation(teamAllocation:TeamAllocation,selectedList:any): Observable<any> {

return this.hc.post(this.url + '/Batch/Team/Allocation/Add/', {teamAllocation:teamAllocation,selectedList:selectedList}, this.httpOptions)

}
getAllTraineesfromTraineeBatch():Observable<any> {
this.initSession();
for (var i = 0; i < this.userSession.length; i++) {
  this.userSession = this.userSession.replace('/', '-');
}
return this.hc.get(this.url + '/Batch/TraineeBatch/Branch/'+this.userSession,  this.httpOptions)

}


getAllTeamAllocationMembers():Observable<any> {
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
    this.userSession = this.userSession.replace('/', '-');
  }
  return this.hc.get(this.url + '/Batch/TeamAllocation/'+this.userSession,  this.httpOptions)

}
traineemembersByTeam(id:any):Observable<any> {
 
  return this.hc.get(this.url + '/Batch/TeamAllocation/ByTeam/'+id,  this.httpOptions)
}

}