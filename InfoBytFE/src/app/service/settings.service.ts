import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpHandler, HttpInterceptor, HttpRequest, HttpEvent } from '@angular/common/http'
import { Observable } from 'rxjs';
import * as config from './service_init.json';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { Billing } from '../modal/Settings/billing';
import { Country } from '../modal/Settings/country';
import { Institute } from '../modal/Settings/Institute';
import { InstituteType } from '../modal/Settings/institutetype';
import { Permission } from '../modal/Settings/permission';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  url: string = (<any>config).api;
  userSession: any;
  constructor(private hc: HttpClient) {
  }
  initSession() {
    this.userSession = sessionStorage.getItem('user')
  }
  //=========================================Excel====================================
  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }
  //====================================Billing=============================
  saveBill(bil: Billing): Observable<any> {
    return this.hc.post(this.url + '/Settings/Billing/Add', JSON.stringify(bil), this.httpOptions)
  }
  getSAdminBySession():Observable<any> {
    this.initSession()
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Settings/Billing/SAdmin/' + this.userSession, this.httpOptions)
  }
  getBillDetails():Observable<any> {
    this.initSession()
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Settings/Billing/Details/' + this.userSession, this.httpOptions)
  }
  updBillDetails(bil: Billing): Observable<any> {
    return this.hc.post(this.url + '/Settings/Billing/Update/' + bil.Id, JSON.stringify(bil), this.httpOptions)
  }
  //===================================Change Password=================================
  getLoginPassword(oldpassword: any): Observable<any> {
    this.initSession()
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Settings/changepassword/' + oldpassword + '/' + this.userSession, { responseType: 'json' })
  }
  updatePassword(pwd: any): Observable<any>  {
    this.initSession()
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.post(this.url + '/Settings/updatepassword/' + pwd + '/' + this.userSession, this.httpOptions)
  }
  /* *******************************Countries */
  saveCountry(ctry: Country): Observable<any> {
    return this.hc.post(this.url + '/Settings/data/country/enter', JSON.stringify(ctry), this.httpOptions)
  }
  //======================================Institute============================================
  saveInstitute(institute: Institute): Observable<any> {
    return this.hc.post(this.url + "/Settings/OrganizationType/Add", JSON.stringify(institute),
      this.httpOptions)
  }
  saveBoard(institute: Institute): Observable<any> {
    return this.hc.post(this.url + "/Settings/Board/Add", JSON.stringify(institute), this.httpOptions)
  }
  getInstitutes(): Observable<any> {
    return this.hc.get(this.url + '/Settings/OrganizationType/', { responseType: 'json' });
  }
  getBoardorType(id: any): Observable<any> {
    return this.hc.get(this.url + "/Settings/Board/Type/" + id, { responseType: 'json' })
  }
  getBoardType(): Observable<any> {
    return this.hc.get(this.url + '/Settings/Board/', { responseType: 'json' });
  }
  verifyInstituteData(inp: string, inpData: string): Observable<any>  {
    return this.hc.get(this.url + '/Settings/OrganizationType/Verify/' + inp + '/' + inpData, this.httpOptions)
  }
  verifyBoardData(inp: string, inpData: string): Observable<any>  {
    return this.hc.get(this.url + '/Settings/Board/Verify/' + inp + '/' + inpData, this.httpOptions)
  }
  getInstitutestype(): Observable<any> {
    return this.hc.get(this.url + '/Settings/Institutetype/', { responseType: 'json' });
  }
  saveInstitutetype(institutetype: InstituteType): Observable<any> {
    return this.hc.post(this.url + "/Settings/InstituteType/Add", JSON.stringify(institutetype),
      this.httpOptions)
  }
  savePermissions(permission: Permission) : Observable<any> {
    return this.hc.post(this.url + '/Settings/Permission/Add', JSON.stringify(permission), this.httpOptions);;
  }
  getmodules(rId: any, bId:number):Observable<any> {
    return this.hc.get(this.url + '/Settings/permission/view/'+rId+'/'+ bId, { responseType: 'json' })
  }
  getUserModules():Observable<any> {
    this.initSession()
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Settings/permission/view/'+this.userSession, { responseType: 'json' })
  }
  getAllInstituteType(): Observable<any> {
    return this.hc.get(this.url + "/Organization/InstituteType/", { responseType: 'json' })
  }
}
