import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpHandler, HttpInterceptor, HttpRequest, HttpEvent } from '@angular/common/http'
import { Observable } from 'rxjs';
import * as config from './service_init.json';
import { SportsDetails } from '../modal/Services/sportsdetails';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { Visiting } from '../modal/Services/visiting';
import { HostelFee } from '../modal/Services/hostelfee';
import { RequestLibrary } from '../modal/Services/RequestLibrary';
import { TraineeLibrary } from '../modal/Services/Traineelibrary';
import { Library } from '../modal/Services/Library'
import { Hostel } from '../modal/Services/hostel';
import { GatePass } from '../modal/Services/Gatepass';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Injectable({
  providedIn: 'root'
})
export class ServicesService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  url: string = (<any>config).api;
  userSession: any;
  constructor(private hc: HttpClient) {
  }
  initSession() {
    this.userSession = sessionStorage.getItem('user')
  }
    //=========================================Excel====================================
    public exportAsExcelFile(json: any[], excelFileName: string): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }
    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    }
  //*********************************sports Master***************************** 
  getAllocatedSports(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Services/Sports/GetAllocatedSports/' + this.userSession, { responseType: 'json' });
  }
  getAllkits(): Observable<any> {
    return this.hc.get(this.url + '/Services/kits/All/', { responseType: 'json' })
  }
  getAllSports(): Observable<any> {
    return this.hc.get(this.url + '/Services/Sports/GetAllSports/', { responseType: 'json' })
  }
  saveSports(sportsInfo: SportsDetails): Observable<any> {
    return this.hc.post(this.url + "/Services/Sports/Add", JSON.stringify(sportsInfo), this.httpOptions)
  }
  editSports(id: any): Observable<any>{
    return this.hc.get(this.url + "/Services/Sports/Edit/" + id, { responseType: 'json' });
  }
  updateSports(sportsInfo: SportsDetails): Observable<any> {
    return this.hc.post(this.url + '/Services/Sports/Update/' + sportsInfo.Id, JSON.stringify(sportsInfo), this.httpOptions)
  }
  addKit(kitInfo):Observable<any> {
    return this.hc.post(this.url + "/Services/Sports/Kits/Add", JSON.stringify(kitInfo), this.httpOptions)
  } 
  //=====================visiting ================================================
 
  saveVisiting(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.post(this.url + "/Services/Visiting/Add/"+this.userSession , this.httpOptions)
}
getVisitingDetailsByUserId(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + "/Services/Visiting/Details/ByuserId/"+this.userSession , this.httpOptions)
}
getAllHostelVisiting(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + "/Services/Hostel/Visiting/" + this.userSession, { responseType: 'json' })
}
/* getAllHostelVistingById(i: number): Observable<any> {
    return this.hc.get(this.url + "/Services//HostelVisiting/Details/" + i, { responseType: 'json' })
} */
updtVisit(updtVisitObj: Visiting): Observable<any> {
    return this.hc.post(this.url + '/Services/Visiting/Update/' + updtVisitObj.Id, JSON.stringify(updtVisitObj), this.httpOptions)
}
//=====================================hostelfee=============================================
savehostelfee(hstlfee: HostelFee): Observable<any> {
    return this.hc.post(this.url + "/Services/HostelFee/Add", JSON.stringify(hstlfee), this.httpOptions)
}
getAllHostelFee(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + "/Services/Hostel/HostelFee/" + this.userSession, { responseType: 'json' })
}
getAllHostelFeeById(i: number): Observable<any> {
    return this.hc.get(this.url + "/Services/HostelFee/Details/" + i, { responseType: 'json' })
}
updateHostelFee(updateHostelFeeObj: HostelFee): Observable<any> {
    return this.hc.post(this.url + '/Services/HostelFee/Update/' + updateHostelFeeObj.Id, JSON.stringify(updateHostelFeeObj), this.httpOptions)
}
  //--------------------------------library*--------------
  saveLibrary(library: Library): Observable<any> {
    return this.hc.post(this.url + "/Services/Library/Add/", JSON.stringify(library), this.httpOptions)
}
getLibrary(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + "/Services/Library/" + this.userSession, { responseType: 'json' })
}
getAllLibrary(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + "/Services/Library/All/" + this.userSession, { responseType: 'json' })
}
getAllLibraryByname(bnm: any): Observable<any> {
    return this.hc.get(this.url + "/Services/Library/Details/" + bnm, { responseType: 'json' })
}
getMaxBookId(bookname: any): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + "/Services/Library/MaxID/" + bookname + "/" + this.userSession, { responseType: 'json' })
}
getAllBookRequests(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + "/Services/Library/BookRequests/" + this.userSession, { responseType: 'json' })
}
getAllBookRequestsById(id: number): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + "/Services/Library/bookRequests/Details/" + id + "/" + this.userSession, { responseType: 'json' })
}
updateLibraryRequestDetails(request: RequestLibrary): Observable<any> {
    return this.hc.post(this.url + '/Services/User/Library/Request/Update/' + request.Id, JSON.stringify(request), this.httpOptions)
}
/****************** *studentlibrary================*/
saveTraineeLibrary(studentlibrary:TraineeLibrary): Observable<any> {
    return this.hc.post(this.url + "/Services/Traineelibrary/Add/", JSON.stringify(studentlibrary), this.httpOptions)
}
getAllLibraryTransactionById(id: any): Observable<any> {
    return this.hc.get(this.url + "/Services/LibraryTransaction/Details/" + id, { responseType: 'json' })
}
getAllTraineeLibrary(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + "/Services/TraineeLibrary/All/" + this.userSession, { responseType: 'json' })
}
updateTraineeLibraryDetails(id: any): Observable<any> {
    return this.hc.get(this.url + "/Services/TraineeLibrary/Details/" + id, { responseType: 'json' })
}
updatetraineelibrarytDetails(stdnt: TraineeLibrary): Observable<any> {
    return this.hc.post(this.url + '/Services/User/Library/Trainee/Update/' + stdnt.Id, JSON.stringify(stdnt), this.httpOptions)
}
getRequestCount(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + '/Services/Library/Request/Count/' + this.userSession, { responseType: 'json' })
}
getLibrarry(Id: any): Observable<any> {
    return this.hc.get(this.url + "/Services/Library/All/Details/" + Id, { responseType: 'json' })
}
getTraineeLibrarry(Id: any): Observable<any> {
    return this.hc.get(this.url + "/Services/Library/All/IssuedDetails/" + Id, { responseType: 'json' })
}
getdaysCount(rdate: any, id: any): Observable<any> {
    return this.hc.get(this.url + "/Services/TraineeLibrary/DaysCount/" + rdate + '/' + id, { responseType: 'json' })
}
 //==========================================Hostel==============================================
 Applyhostel():Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.post(this.url + "/Services/Branch/HostelApply/" + this.userSession, { responseType: 'json' })
}
getAllTraineeappliedlist(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + "/Services/Branch/HostelAppliedDetails/" + this.userSession, { responseType: 'json' })
}
approveTraineeForhostel(userid: any): Observable<any> {
    return this.hc.post(this.url + '/Services/Branch/Hostel/Approve/' + userid, this.httpOptions)
}
savehostel(hostel1: Hostel): Observable<any> {
    return this.hc.post(this.url + "/Services/Hostel/Add", JSON.stringify(hostel1), this.httpOptions)
}
getAllhostel(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + "/Services/Hostel/" + this.userSession, { responseType: 'json' })
}
getAllHostelsById(i: number): Observable<any> {
    return this.hc.get(this.url + "/Services/Hostel/Details/" + i, { responseType: 'json' })
}
updtHostel(updateHostelObj: Hostel): Observable<any> {
    return this.hc.post(this.url + '/Services/Hostel/Update/' + updateHostelObj.Id, JSON.stringify(updateHostelObj), this.httpOptions)
}
getAllHostelApplicants(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + "/Services/Branch/Hostel/Applicants/" + this.userSession, { responseType: 'json' })
}
 //*********************************gatepass***************************** */
 saveGatepass(gatepass: GatePass): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.post(this.url + "/Services/GatePass/Add/" + this.userSession, JSON.stringify(gatepass), this.httpOptions)
}
getAllGatePass(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.hc.get(this.url + "/Services/GatePass/Details/" + this.userSession, { responseType: 'json' })
}
getGatePassById(id: number): Observable<any> {
    return this.hc.get(this.url + "/Services/GatePass/DetailbyId/" + id, { responseType: 'json' })
}
updateGatePass(gatepass: GatePass): Observable<any> {
    return this.hc.post(this.url + '/Services/GatePass/Update/' + gatepass.Id, JSON.stringify(gatepass), this.httpOptions)
}
}
