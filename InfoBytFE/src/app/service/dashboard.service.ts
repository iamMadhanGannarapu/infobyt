import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpHandler, HttpInterceptor, HttpRequest, HttpEvent } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Feedback } from '../modal/Dashboard/Feedback';
import * as config from './service_init.json';
import { Mailbox } from '../modal/Dashboard/Mailbox';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { BulkMessage } from '../modal/Dashboard/BulkMessage';
import { OrganizationFeedback } from '../modal/Dashboard/OrganizationFeedback';
import { Shopping } from '../modal/Dashboard/shopping';
import { UserUploads } from '../modal/Dashboard/Uploads';
import { Cart } from '../modal/Dashboard/cart';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Injectable({
    providedIn: 'root'
})
export class DashboardService {
    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    url: string = (<any>config).api;
    userSession: any;
    constructor(private http: HttpClient) {
    }
    initSession() {
        this.userSession = sessionStorage.getItem('user')
    }
    //*********************************Excel***************************** */
    public exportAsExcelFile(json: any[], excelFileName: string): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }
    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    }
    //==================================================Bulk Message==============================
    saveBulkMessage(bulkMessage: BulkMessage): Observable<any> {
        return this.http.post(this.url + "/Dashboard/Branch/BulkMessage/Add", JSON.stringify(bulkMessage), this.httpOptions)
    }
    allSessions(deptId: number): Observable<any> {
        return this.http.get(this.url + '/Dashboard/Branch/Department/Sessions/' + deptId, this.httpOptions)
    }
    allBatches(classId: number): Observable<any> {
        return this.http.get(this.url + '/Dashboard/Branch/Sessions/Batch/' + classId, this.httpOptions)
    }
    getMessages(): Observable<any> {
        this.initSession();
        for (var i = 0; i < this.userSession.length; i++) {
            this.userSession = this.userSession.replace('/', '-');
        }
        return this.http.get(this.url + "/Dashboard/Branch/MessageList/" + this.userSession, { responseType: 'json' })
    }
    //---------------------------------------dashboard comp--------------------
    getEnquiry():Observable<any> {
        return this.http.get(this.url + '/Dashboard/User/Enquiry/', { responseType: 'json' })
      }
//===============================Feedback====================================
saveFeedback(feed: Feedback): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.post(this.url + '/Dashboard/Feedback/Add/' + this.userSession, JSON.stringify(feed), this.httpOptions);
  }
  getFeedBack(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/Dashboard/Feedback/' + this.userSession, { responseType: 'json' })
  }
  getFeedBackById(id: number): Observable<any> {
    return this.http.get(this.url + '/Dashboard/Feedback/' + id, { responseType: 'json' })
  }
  updateFeedback(t: Feedback): Observable<any> {
    return this.http.post(this.url + '/Dashboard/Feedback/Update/' + t.Id, JSON.stringify(t), this.httpOptions);
  }  
   //=====================================Gallery======================================
   getGalleryImages(id: number): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + "/file/Gallery/album/images/" + this.userSession+'/'+id, { responseType: 'json' })
}
GetallGallery(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + "/file/Gallery/images/" + this.userSession, { responseType: 'json' })
}
deleteGall(Id:any):Observable<any> {
  this.initSession();
  for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
  }
  return this.http.delete(this.url +'/file/Gallery/delete/'+this.userSession+ '/'+Id, { responseType: 'json' })

}

 //=========================id card=====================================
 getIdCard(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/Dashboard/Trainee/Idcard/' + this.userSession, { responseType: 'json' })
}
//==========================================Mail box=======================================================
getUsersOfBranch(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url+"/Dashboard/Branch/Users/"+this.userSession,{responseType:'json'})
  }
  addMail(mailbox:Mailbox): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.post(this.url+'/Dashboard/Mail/Add/'+this.userSession,JSON.stringify(mailbox),this.httpOptions)
  }
  getInbox(type:any): Observable<any>{
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
        this.userSession = this.userSession.replace('/', '-');
    }
    return  this.http.get(this.url+'/Dashboard/Mailbox/'+type+'/'+this.userSession,{responseType:'json'})
  }
  //=================================Notifications====================
  saveLevNotifications(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.post(this.url + '/Dashboard/notification/leave/add/' + this.userSession, this.httpOptions);
  }
  getNotifications(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/Dashboard/notification/' + this.userSession, { responseType: 'json' });
  }
  saveHolidayNotifications(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.post(this.url + '/Dashboard/notification/Holiday/add/' + this.userSession, this.httpOptions);
  }
  addBatchNotification(Id: number): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.post(this.url + '/Dashboard/notification/batch/classteacher/add/' + this.userSession + '/' + Id, this.httpOptions)
  }
  saveTraineeBatchNotification(list: any): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.post(this.url + '/Dashboard/notification/traineebatch/add/' + this.userSession + '/', JSON.stringify(list), this.httpOptions);
  }
  addTimeTableNotification(Id: number): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.post(this.url + '/Dashboard/notification/TimeTable/add/' + this.userSession + '/' + Id, this.httpOptions)
  }
  addAppraisalTimeTableNotification(Id: number): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.post(this.url + '/Dashboard/notification/appraisalTimeTable/add/' + this.userSession + '/' + Id, this.httpOptions)
  }
  getNotificationCount(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/Dashboard/notification/count/' + this.userSession, { responseType: 'json' })
  }
  updateNotification(Ids: any): Observable<any> {
    return this.http.post(this.url + '/Dashboard/notification/Update/', JSON.stringify(Ids), this.httpOptions);
  }
  addMarksNotification(Id: number): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.post(this.url + '/Dashboard/notification/traineemarks/add/' + this.userSession + '/' + Id, this.httpOptions)
  }
  addGalleryNotification(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.post(this.url + '/Dashboard/notification/Gallery/add/' + this.userSession, this.httpOptions)
  }
  addFeedbackNotification(Id: number): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.post(this.url + '/Dashboard/notification/FeedBack/add/' + this.userSession + '/' + Id, this.httpOptions)
  }
  addOrganizationFeedbackNotification(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.post(this.url + '/Dashboard/notification/organizationfeedback/add/' + this.userSession, this.httpOptions)
  }
  addFeepaymentsNotification(Id: number): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.post(this.url + '/Dashboard/notification/Feepayments/add/' + this.userSession + '/' + Id, this.httpOptions)
  }
  addAchievementsNotification(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.post(this.url + '/Dashboard/notification/achievements/add/' + this.userSession, this.httpOptions)
  }
  downloadFile(file: String): Observable<any>  {
    var body = { filename: file };
    return this.http.post('http://localhost:3400/file/download', body, {
      responseType: 'blob',
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    });
  }
  //====================================Organization Feedback==========================
  saveOrganizationFeedback(scf: OrganizationFeedback): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.post(this.url + "/Dashboard/User/Feedback/Add/" + this.userSession, JSON.stringify(scf), this.httpOptions)
  }
  getOrganizationFeedBack(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/Dashboard/User/Feedback/' + this.userSession, { responseType: 'json' })
  }
  getOrganizationFeedBackById(id: number): Observable<any> {
    return this.http.get(this.url + '/Dashboard/User/Feedback/Details/' + id, { responseType: 'json' })
  }
  //=====================Shopping===============================
  saveSellerProduct(shopping: Shopping): Observable<any> {
    return this.http.post(this.url + '/Dashboard/shopping/add', JSON.stringify(shopping), this.httpOptions)
  }
  saveCart(cart: Cart): Observable<any> {
    return this.http.post(this.url + '/Dashboard/cart/add', JSON.stringify(cart), this.httpOptions)
  }
  getAllShoppingList(): Observable<any> {
    return this.http.get(this.url + '/Dashboard/shopping/view', { responseType: 'json' })
  }
  getShoppingById(i: number): Observable<any> {
    return this.http.get(this.url + "/Dashboard/shopping/viewbyid/" + i, { responseType: 'json' })
  }
  getShoppingForUsers(): Observable<any> {
    return this.http.get(this.url + "/Dashboard/shopping/view/users", { responseType: 'json' })
  }
  getAllCartList(): Observable<any> {
    return this.http.get(this.url + '/Dashboard/cart/view', { responseType: 'json' })
  }
  updateProduct(shopping: Shopping): Observable<any> {
    return this.http.post(this.url + '/Dashboard/shopping/update/' + shopping.Id, JSON.stringify(shopping), this.httpOptions)
  }
  getContactDetails(id: any): Observable<any> {
    return this.http.get(this.url + "/Dashboard/ContactDetails/" + id, { responseType: 'json' })
  }
  deleteCartProduct(id: any): Observable<any> {
    return this.http.delete(this.url + '/Dashboard/cart/delete/' + id, this.httpOptions)
  }
  getAllCartAllocation(): Observable<any> {
    return this.http.get(this.url + '/Dashboard/cart/view', { responseType: 'json' })
  }
  checkoutOrders(cartlist: any,userid:any): Observable<any> {
    return this.http.post(this.url + '/Dashboard/cart/checkout/'+userid, JSON.stringify(cartlist), this.httpOptions)
  }
//========================================Uploads=============================
saveFilesUploads(up: UserUploads, Id: number): Observable<any> {
    return this.http.post(this.url + '/Dashboard/uploads/add/' + Id, JSON.stringify(up), this.httpOptions)
  }
  getFileUploads(Id: number): Observable<any> {
    return this.http.get(this.url + '/Dashboard/Public/Uploads/' + Id, {
      responseType: 'json'
    });
  }
  getSharedUploads(Id:number): Observable<any> {
    return this.http.get(this.url + '/Dashboard/sharing/Details/' + Id, {
      responseType: 'json'
    });
  }
  deleteFileList(f: string, Id: number): Observable<any>  {
    return this.http.delete(this.url +'/Dashboard/uploads/delete/' + Id +'/'+ f, { responseType: 'json' })
  }
  saveSharing(up: UserUploads): Observable<any>  {
    return this.http.post(this.url + '/Dashboard/sharing/add', JSON.stringify(up), this.httpOptions);;
  }
  getSocialNetworkId(Id: number): Observable<any> {
    return this.http.get(this.url + '/Dashboard/Socialnetwork/' + Id, {
      responseType: 'json'
    });
  }

}