import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Login } from "../modal/Login/Login";
import { SocialLogin } from 'src/app/modal/Login/SocialLogin';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { User, Authentication } from '../modal/User/User';
import { ContactDetails } from '../modal/User/ContactDetails';
import * as config from './service_init.json';
import { Enquiry } from '../modal/Login/Enquiry';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }
  userSession: any;
  constructor(private http: HttpClient) {
  }
  url: string = (<any>config).api;
  chatUrl: string = (<any>config).chat;
  initSession() {
    this.userSession = sessionStorage.getItem('user')
  }
  //===================================Excel=================================
  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }
  //===================================Chat=================================
  getChatClientId(convoId: any): Observable<any> {
    return this.http.get(this.url + '/Login/ChatClient/' + convoId, { responseType: 'json' })
  }
  getChatSupportId(frmUser: any): Observable<any>  {
    return this.http.get(this.url + '/Login/ChatSupport/' + frmUser, { responseType: 'json' })
  }
  addSupportUser(userId: number, rolename: any, type: any): Observable<any> {
    return this.http.post(this.url + '/Login/Chat/Add/Conversation', { support: userId, roleName: rolename, type: type }, this.httpOptions);
  }
  addUserToChat(userName: any): Observable<any>  {
    return this.http.post(this.url + '/Login/Chat/Add/UserChat', { user: userName }, this.httpOptions)
  }
  logoutSupport(type: any): Observable<any>  {
    this.initSession()
    return this.http.post(this.url + '/Login/Chat/Update/Logout/', { userSession: this.userSession, type: type }, this.httpOptions)
  }
  addNewConvo(convoId): Observable<any> {
    this.initSession()
    return this.http.post(this.url + '/Login/Chat/Add/New/Convo/', { userSession: this.userSession, convoId: convoId }, this.httpOptions)
  }
  assignSupporttoUser(userChatId: any, userName: any, supportId: any): Observable<any>  {
    return this.http.post(this.url + '/Login/Chat/Update/Conversation/', { userChatId: userChatId, userName: userName, supportId: supportId }, this.httpOptions)
  }
  getChatOfUser(convoId: any): Observable<any> {
    return this.http.get(this.url + '/Login/Chat/view/Messages/' + convoId, { responseType: 'json' })
  }
  updateReadMessages(optValue): Observable<any> {
    return this.http.post(this.url + '/Login/Chat/Messages/Update/', { convoId: optValue }, this.httpOptions)
  }
  getChatOfSupport(sId: any): Observable<any> {
    return this.http.get(this.url + '/Login/Chat/view/Messages/Support/' + sId, { responseType: 'json' })
  }
  varifyloginid(inp: string, inpdata: string): Observable<any>  {
    return this.http.get(this.url + '/Login/verifyloginid/Verify/' + inp + '/' + inpdata, this.httpOptions)
  }
  getAssignedUsersForSupport(): Observable<any> {
    this.initSession()
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/Login/Chat/Support/' + this.userSession, { responseType: 'json' })
  }
  getLastModified(id: number):Observable<any> {
    return this.http.get(this.url + '/Login/lastmodified/' + id, { responseType: 'json' })
  }
  updateUName(user: Authentication):Observable<any> {
    return this.http.post(this.url + '/Login/Update/Username/' + user.Id, JSON.stringify(user), this.httpOptions)
  }
  removeChat(id: number): Observable<any>  {
    return this.http.post(this.url + '/Login/Chat/Close/' + id, { responseType: 'json' })
  }
  getSelectedUserMessages(frmUser: any, toUser: any): Observable<any>  {
    return this.http.get(this.chatUrl + '/' + frmUser + '/' + toUser, { responseType: 'json' });
  }
  addUsertoQueue(userId: any): Observable<any>  {
    return this.http.post(this.url + '/Login/Chat/Queue/Add/', { userId: userId }, this.httpOptions)
  }
  checkForSupport(supportType: any, userId: any, fromUser: any): Observable<any> {
    return this.http.get(this.url + '/Login/Chat/Check/Support/' + supportType, { responseType: 'json' })
  }
  getConversationIdOfUser(userId: any): Observable<any> {
    return this.http.get(this.url + '/Login/Chat/Support/Convo/' + userId, { responseType: 'json' })
  }
  addChatFeedback(conversationId): Observable<any> {
    return this.http.post(this.url + '/Login/Chat/Feedback/Add', { convoId: conversationId }, this.httpOptions)
  }
  updateChatFeedback(rating: any, convoId: any): Observable<any> {
    return this.http.post(this.url + '/Login/Chat/Feedback/Update', { convoId: convoId, rating: rating }, this.httpOptions)
  }
  updateBreak(outTime: any): Observable<any>  {
    this.initSession()
    return this.http.post(this.url + '/Login/Chat/Break/Update', { outTime: outTime, userSession: this.userSession }, this.httpOptions)
  }
  //====================================Default===========================================
  getBatchByUserId(Id: number): Observable<any>  { 
    return this.http.get(this.url + '/User/Batch/' + Id, { responseType: 'json' })
  }
  getProfileImage(): Observable<any> {
    this.initSession()
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/Misc/ProfilePic/images/' + this.userSession, { responseType: 'json' })
  }
  displayProfile(userId: number): Observable<any>  {
    return this.http.get(this.url + '/User/Organization/' + userId, this.httpOptions)
  }
  updateContact(cd: ContactDetails): Observable<any>  {
    return this.http.post(this.url + '/User/ContactDetails/Update/' + cd.Id, JSON.stringify(cd), this.httpOptions)
  }
  updateProfile(user: User): Observable<any>  {
    return this.http.post(this.url + '/User/Update/' + user.Id, JSON.stringify(user), this.httpOptions)
  }
  updateProfileImage(usermodel: User): Observable<any>  { 
    return this.http.post(this.url + '/Misc/Upload/ProfilePic/' + usermodel.Id, JSON.stringify(usermodel), this.httpOptions)
  }
  //===================================Forgot Password=================================
  getPassword(loginId: any, email: any): Observable<any>  {
    return this.http.get(this.url + '/Login/Forgotpassword/' + loginId + '/' + email, { responseType: 'json' })
  }
  getPassword1(email: any) {
    return this.http.get(this.url + '/Login/Forgotusername/' + email,{ responseType: 'json' })
  }
  //=================================Login=======================================
  checkLogin(lgn: Login): Observable<any> {
    return this.http.post(this.url + '/Login/Login/', JSON.stringify(lgn), this.httpOptions);
  }
  //===========================================Sign Up===================================
  verifyUserData(inp: string, inpData: string):Observable<any> {
    return this.http.get(this.url + '/Login/ContactDetails/Verify/' + inp + '/' + inpData, this.httpOptions)
  }
  /* ====================================signup================================= */
  getAllLocationNames(CityId): Observable<any> {
    return this.http.get(this.url + '/Login/places/' + CityId, { responseType: 'json' })
  }
  saveEnquiry(eq: Enquiry):Observable<any> {
    return this.http.post(this.url + '/User/User/Login/Add', JSON.stringify(eq), this.httpOptions)
  }
  //========================================================================================
  getUserBySessionId(): Observable<any> {
    this.initSession()
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.url + '/Exam/Userid/' + this.userSession, { responseType: 'json' })
  }
  //=============================== Logged Out =================================================
  logout(): Observable<any>  {
    this.initSession()
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.post(this.url + '/User/Logout/' + this.userSession, this.httpOptions)
  }
  getAchievementByUserId(Id: number):Observable<any> {
    this.initSession();
    return this.http.get(this.url + '/Organization/User/Achievement/' + Id, { responseType: 'json' })
  }
  checkRightPermissions(str:any):Observable<any>{
    this.initSession()
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
   return this.http.get(this.url + '/Login/Check/Permissions/' + this.userSession+'/'+str, {responseType:'json'})
  }
  getPermissionsForRole():Observable<any>{
    
    return this.http.get(this.url + '/Login/Permission/View/Role', { responseType: 'json' })
  }
  addSocialLogins(social:SocialLogin): Observable<any> {
    alert(JSON.stringify(social))
    return this.http.post(this.url + '/Login/Social/add', JSON.stringify(social), this.httpOptions);
  }
checkSocialLogin(lgn: Login): Observable<any> {
    return this.http.post(this.url + '/Login/Login/Social', JSON.stringify(lgn), this.httpOptions);
  }
}
