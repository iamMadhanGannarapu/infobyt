import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs'
import { UserTransportPayment } from '../modal/Transport/UserTransportPayment';
import { TransportStops } from '../modal/Transport/TransportStops';
import { BusDriverAllocation } from '../modal/Transport/BusDriverAllocation';
import { TransportRoute } from '../modal/Transport/TransportRoute';
import { Bus } from '../modal/Transport/Bus';
import { TransportCharge } from '../modal/Transport/TransportCharge';
import { TransportStopFee } from '../modal/Transport/transportstopfee'
import { UserTransport } from '../modal/Transport/UserTransport';
import * as config from './service_init.json';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Injectable({
  providedIn: 'root'
})
export class TransportService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }
  userSession: any;
  Url: string = (<any>config).api;
  constructor(private http: HttpClient) {
  }
  initSession() {
    this.userSession = sessionStorage.getItem('user')
  }
  //===========================================Excel========================================
  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }
  //===========================================Bus Driver Alocation========================================
  saveBusDriver(m: BusDriverAllocation): Observable<any> {
    return this.http.post(this.Url + '/Transport/branch/bus/driver/add', JSON.stringify(m), this.httpOptions)
  }
  getAllDriver(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.Url + '/Transport/Branch/Bus/Driver/' + this.userSession, { responseType: 'json' });
  }
  getDriverById(id: number): Observable<any> {
    return this.http.get(this.Url + '/Transport/Branch/Bus/Driver/Details/' + id, { responseType: 'json' });
  };
  updateBusDriver(Bda: BusDriverAllocation): Observable<any> {
    return this.http.post(this.Url + '/Transport/Branch/Bus/Driver/Update/' + Bda.Id, JSON.stringify(Bda), this.httpOptions)
  }
  busDetails(): Observable<any>  {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.Url + "/Transport/Branch/Driver/Busdetails/" + this.userSession, { responseType: 'json' })
  }
  //========================================Bus Master===================================================
  saveBus(b: Bus): Observable<any> {
    return this.http.post(this.Url + '/Transport/Bus/Add/', JSON.stringify(b), this.httpOptions)
  }
  getAllBus(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.Url + '/Transport/Branches/Bus/' + this.userSession, { responseType: 'json' })
  }
  getBusById(id: number): Observable<any> {
    return this.http.get(this.Url + '/Transport/Branch/Bus/Details/' + id, { responseType: 'json' })
  }
  UpdateBus(bus: Bus): Observable<any> {
    return this.http.post(this.Url + '/Transport/Branch/Bus/Update/' + bus.Id, JSON.stringify(bus), this.httpOptions)
  }
  //====================================Transport Fee Master==============================================
  saveTransportFee(tf: TransportStopFee):Observable<any> {
    return this.http.post(this.Url + '/Transport/Branch/Transport/Fee/Add', JSON.stringify(tf), this.httpOptions)
  }
  getAllTransportFee(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.Url + '/Transport/Branches/Transport/Fee/' + this.userSession, { responseType: 'json' })
  }
  getTransportFeeById(Id: number): Observable<any> {
    return this.http.get(this.Url + '/Transport/Branch/Transport/Fee/Details/' + Id, { responseType: 'json' })
  }
  updateTransportFee(tfm: TransportStopFee): Observable<any> {
    return this.http.post(this.Url + '/Transport/Branch/Transport/Fee/Update/' + tfm.Id, JSON.stringify(tfm), this.httpOptions)
  }
  getTransportStopsByrouteId(id: number): Observable<any> { 
    return this.http.get(this.Url + '/Transport/Branch/Stops/route/' + id, { responseType: 'json' });
  }
  getStopsByRoute(Id: number): Observable<any> { //NOT FOUND
    return this.http.get(this.Url + '/Transport/Transport/Stop/' + Id, { responseType: 'json' })
  }
  //================================ Transport Route Master ================================================
  saveTransportRoute(trm: TransportRoute):Observable<any> {
    return this.http.post(this.Url + '/Transport/Transport/Route/Add', JSON.stringify(trm), this.httpOptions)
  }
  getAllTransportRoute(): Observable<any> {
    this.initSession()
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.Url + '/Transport/Transport/Route/' + this.userSession, { responseType: 'json' })
  }
  getTransportRouteById(id: number): Observable<any> {
    return this.http.get(this.Url + '/Transport/Transport/Route/Details/' + id, { responseType: 'json' })
  }
  updateTransportRoute(tfm: TransportRoute): Observable<any> {
    return this.http.post(this.Url + '/Transport/Transport/Route/Update/' + tfm.Id, JSON.stringify(tfm), this.httpOptions)
  }
  //=======================================Transport Stops=======================================
  saveTransportStops(ts: TransportStops): Observable<any> {
    return this.http.post(this.Url + '/Transport/Branch/Bus/Stops/Add', JSON.stringify(ts), this.httpOptions);
  }
  getAllTransportStops(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.Url + '/Transport/Branch/Bus/Stops/route/' + this.userSession, { responseType: 'json' });
  }
  getTransportStopsById(id: number): Observable<any> {
    return this.http.get(this.Url + '/Transport/Branch/Bus/Stops/' + id, { responseType: 'json' });
  }
  updateTransportStops(uts: TransportStops): Observable<any> {
    return this.http.post(this.Url + '/Transport/Branch/Bus/Stops/Update/' + uts.Id, uts, this.httpOptions)
  }
  //========================================User Transport====================================
  saveUserTransport(m: UserTransport): Observable<any> {
    return this.http.post(this.Url + '/Transport/Branch/Bus/Trainee/Add', JSON.stringify(m), this.httpOptions)
  }
  getAllUserTransport(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.Url + '/Transport/Branch/Bus/Trainee/' + this.userSession, { responseType: 'json' })
  }
  getUserTransportById(n: number): Observable<any> {
    return this.http.get(this.Url + '/Transport/Branch/Bus/Trainee/Details/' + n);
  };
  updateUserTransport(utrsn: UserTransport): Observable<any> {
    return this.http.post(this.Url + '/Transport/Branch/Bus/Trainee/Update/' + utrsn.Id, utrsn, this.httpOptions)
  }
  gettransportfeeByUserId(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.Url + '/Transport/transport/fee/transactions/' + this.userSession, { responseType: 'json' })
  }
  //=====================================User Transport Payment===========================
  saveUserTransportPayment(utpi: UserTransportPayment): Observable<any> {
    return this.http.post(this.Url + '/Transport/Branch/Bus/Fee/Add', JSON.stringify(utpi), this.httpOptions)
  }
  getAllUserTransportPayment(): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.Url + '/Transport/Branch/Bus/Fee/' + this.userSession, { responseType: 'json' })
  }
  getUserTransportPaymentById(id: number): Observable<any> {
    return this.http.get(this.Url + '/Transport/Branch/Bus/Fee/details/' + id, { responseType: 'json' })
  }
  updateUserTransportPayment(utp: UserTransportPayment): Observable<any> {
    return this.http.post(this.Url + '/Transport/Branch/Bus/Fee/Update/' + utp.Id, utp, this.httpOptions)
  }
  getUserTransportPaymentByUserId(id: number): Observable<any> {
    return this.http.get(this.Url + '/Transport/Branch/Bus/Fee/user/' + id, { responseType: 'json' })
  }
  getUserTransportPaymentByBatchId(id: number): Observable<any> {
    this.initSession();
    for (var i = 0; i < this.userSession.length; i++) {
      this.userSession = this.userSession.replace('/', '-');
    }
    return this.http.get(this.Url + '/Transport/Branch/Bus/Fee/Batch/' + id + '/' + this.userSession, { responseType: 'json' })
  }
  getTransportFeeByUserId(Id: number): Observable<any> { 
    return this.http.get(this.Url + '/Transport/Branch/Transport/Fee/user/' + Id, { responseType: 'json' })
  }
}
