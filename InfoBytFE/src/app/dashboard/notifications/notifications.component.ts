import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/service/user.service';
import { DashboardService } from "src/app/service/dashboard.service";
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationComponent implements OnInit,CanActivateChild {
  notificationList:any
  read: any;
  write: any;
  constructor(private router:Router,  private us:UserService,private ds:DashboardService, private ls: LoginService) { }
  ngOnInit() {
    this.getPermissions();
    this.ds.getNotifications().subscribe((data)=>{
      this.notificationList=data.data;
      this.us.notificationList=this.notificationList;
      this.canActivateChild(this.read)
    })
  }
  getPermissions() {
    this.ls.checkRightPermissions('notifications').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
    })
  }
  canActivateChild(x:any):boolean{
    if(x==true){
      return true;
    }else{
      this.router.navigate(['404']);
        return false;
    }
      }
}