import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FeedbackComponent } from './feedback/feedback.component';
import { NotificationComponent } from './notifications/notifications.component';
import { OrganizationFeedbackComponent } from './organization-feedback/organization-feedback.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MailboxComponent } from './mailbox/mailbox.component';
import { RouteGuardGuard, CanDeactivateGuard } from '../service/route-guard.guard';
import { IdcardComponent } from './idcard/idcard.component';
import { BulkmessageComponent } from './bulkmessage/bulkmessage.component';
import { GalleryMasterComponent } from './gallery-master/gallery-master.component';
import { UploadsComponent } from './uploads/uploads.component';
import { ShoppingComponent } from './shopping/shopping.component';
const routes: Routes = [
  { path: 'Feedback', component: FeedbackComponent, canActivate: [RouteGuardGuard],canActivateChild:[FeedbackComponent],canDeactivate:[CanDeactivateGuard] },
  { path: 'Notifications', component: NotificationComponent, canActivate: [RouteGuardGuard],canActivateChild:[NotificationComponent] },
  { path: 'Organization-feedback', component: OrganizationFeedbackComponent,canDeactivate:[CanDeactivateGuard] ,canActivate: [RouteGuardGuard],canActivateChild:[OrganizationFeedbackComponent] },
  { path: 'Dashboard', component: DashboardComponent },
    { path: 'Mailbox', component: MailboxComponent, canActivate: [RouteGuardGuard],canActivateChild:[MailboxComponent] },
    { path: 'Idcard', component: IdcardComponent, canDeactivate:[CanDeactivateGuard] ,canActivate: [RouteGuardGuard],canActivateChild:[IdcardComponent] },
    { path: 'Gallery-master', component: GalleryMasterComponent, canActivate: [RouteGuardGuard],canActivateChild:[GalleryMasterComponent] },
    { path: 'BulkMessage', component: BulkmessageComponent, canActivate: [RouteGuardGuard],canActivateChild:[BulkmessageComponent] },
    { path: 'Uploads', component: UploadsComponent, canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard],canActivateChild:[UploadsComponent] },
    { path: 'Shopping', component: ShoppingComponent, canDeactivate:[CanDeactivateGuard],canActivate: [RouteGuardGuard],canActivateChild:[ShoppingComponent]}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [RouteGuardGuard,FeedbackComponent,NotificationComponent,OrganizationFeedbackComponent,
    MailboxComponent, IdcardComponent,BulkmessageComponent,GalleryMasterComponent,UploadsComponent,ShoppingComponent]
})
export class DashboardRoutingModule { }
