import { Component, OnInit, ViewChild } from '@angular/core';
import { OrganizationFeedback } from '../../modal/Dashboard/OrganizationFeedback'
import { BatchService } from '../../service/batch.service';
import { UserService } from '../../service/user.service';
import { ToastrService } from 'ngx-toastr';
import { DashboardService } from "src/app/service/dashboard.service";
import {MatSort, MatTableDataSource,MatPaginator} from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-organization-feedback',
  templateUrl: './organization-feedback.component.html',
  styleUrls: ['./organization-feedback.component.scss']
})
export class OrganizationFeedbackComponent implements OnInit ,CanActivateChild
{
  dataSource;
  displayedColumns=['UserName','FeedbackDescription','Rating','Excel']
  schFeedBackAddObj: OrganizationFeedback;
  vieworganization:boolean;
  rolename: any;
  read: any;
  write: any;
  constructor(private router:Router, private toastr: ToastrService, 
    private ls: LoginService,private bs: BatchService, private us: UserService,private ds:DashboardService) {
    this.schFeedBackAddObj = new OrganizationFeedback();
  }
  @ViewChild("feedback") form: any;
  @ViewChild(MatSort) sort:MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  addSchoolFeedback() {
    this.ds.saveOrganizationFeedback(this.schFeedBackAddObj).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.form.reset();
      }
      else
      this.toastr.error(data.message);
      this.ds.addOrganizationFeedbackNotification().subscribe((data)=>{
      }
      )
      this.getAllSchoolFeedback();
    })
  }
  applyFilter(filterValue: string) {
    
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getIds() {
    this.us.getAllUsers().subscribe((data) => {
    })
  }
  getAllSchoolFeedback() {
    this.ds.getOrganizationFeedBack().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.dataSource=new MatTableDataSource(data.data)
      this.dataSource.sort=this.sort
      this.dataSource.paginator=this.paginator
      }
    })
  }
  getexcel(){
      this.bs.exportAsExcelFile(this.dataSource, 'Organization Feedback');
}
  ngOnInit() {
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
          this.rolename=data.data[0].rolename;
      })
    }
    this.getPermissions();
    this.getAllSchoolFeedback()
  }
  viewOrganization()
  {
    this.vieworganization=!this.vieworganization
  }
  getPermissions() {
    this.ls.checkRightPermissions('organization-feedback').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
  this.canActivateChild(this.read)
    })
  }
  canActivateChild(x:any):boolean{
    if(x==true){
      return true;
    }else{
      this.router.navigate(['404']);
        return false;
    }
      }
}
