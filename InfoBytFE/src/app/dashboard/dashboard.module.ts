import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeedbackComponent } from './feedback/feedback.component';
import { OrganizationFeedbackComponent } from './organization-feedback/organization-feedback.component';
import { NotificationComponent } from "./notifications/notifications.component";
import { MailboxComponent } from "./mailbox/mailbox.component";
import { IdcardComponent } from "./idcard/idcard.component";
import { BulkmessageComponent } from "./bulkmessage/bulkmessage.component";
import { GalleryMasterComponent } from "./gallery-master/gallery-master.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { UploadsComponent } from './uploads/uploads.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { ShoppingComponent } from "./shopping/shopping.component";
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatStepperModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatSliderModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { MatSlideToggleModule } from '@angular/material';
import { BsDatepickerModule, RatingModule } from 'ngx-bootstrap';
import { TypeaheadModule } from 'ngx-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { AgmCoreModule } from '@agm/core'
import { AgmDirectionModule } from 'agm-direction';
import { TreeModule } from 'primeng/tree';
import { FileUploadModule } from 'ng2-file-upload';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ImageCompressService, ResizeOptions } from 'ng2-image-compress';
import { GalleriaModule } from 'primeng/galleria';
import { AutoCompleteModule } from 'primeng/autocomplete';
import {ScrollDispatchModule} from '@angular/cdk/scrolling';
import {DragDropModule} from '@angular/cdk/drag-drop';
@NgModule({
  declarations: [UploadsComponent,NotificationComponent,OrganizationFeedbackComponent,FeedbackComponent,MailboxComponent,IdcardComponent,BulkmessageComponent,
    GalleryMasterComponent,UploadsComponent,DashboardComponent,ShoppingComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MatTableModule, MatTableModule,
    MatSlideToggleModule,
    CdkTableModule,
    CdkTreeModule,
    ScrollDispatchModule,
    DragDropModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule, MatFormFieldModule,
    CommonModule,
    CommonModule, ToastrModule.forRoot(),
    FormsModule, DataTablesModule, TypeaheadModule.forRoot(),
    MatStepperModule, MatInputModule, MatIconModule, MatButtonModule, MatSlideToggleModule, BsDatepickerModule.forRoot(),
    AgmCoreModule.forRoot({ apiKey: '' }),
    AgmDirectionModule, FileUploadModule, TreeModule, GalleriaModule,AutoCompleteModule,RatingModule
  ],
  providers: [ImageCompressService, ResizeOptions],
  exports: [
     UploadsComponent,RouterModule
  ]
})
export class DashboardModule { }
