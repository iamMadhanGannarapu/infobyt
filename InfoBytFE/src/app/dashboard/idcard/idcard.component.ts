
import { Component, OnInit, ViewChild } from '@angular/core';
import { Idcard } from "../../modal/dashboard/idcard";
import { UserService } from "../../service/user.service";
import { User } from '../../modal/User/User';
import { DashboardService } from "src/app/service/dashboard.service";
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
    selector: 'app-idcard',
    templateUrl: './idcard.component.html',
    styleUrls: ['./idcard.component.scss']
  })
  export class IdcardComponent implements OnInit ,CanActivateChild{
  idcard:Idcard
  myurl: string
    idList: any;
    user: User;
    image:any;
    rolename:any
    images: File;
    read: any;
    write: any;
    constructor(private router:Router,  private ls:LoginService,private userService: UserService,private ds:DashboardService){
        this.idcard=new Idcard()
        this.user = new User();
        this.myurl= ds.url
        this.image = { 'URL': '', 'valid': false };
    }
    @ViewChild("idcard") form: any;
    generateIdCard(){
        this.ds.getIdCard().subscribe((data)=>{
          if(data.result && data.data!='NDF')
            this.idList = data.data;
        })
    }
    getPermissions() {
        this.ls.checkRightPermissions('idcard').subscribe((data)=>{
          this.read=data.data[0].read;
          this.write=data.data[0].write;
          this.canActivateChild(this.read)

        })
      }
      
  canActivateChild(x:any):boolean{
    if(x==true){
      return true;
    }else{
      this.router.navigate(['404']);
        return false;
    }
      }
    ngOnInit() {
        if (sessionStorage.getItem('user') != null) {
            this.userService.getUserbySession().subscribe((data) => {
              this.rolename=data.data[0].rolename;
            });
          }
          this.getPermissions();
    }
  }