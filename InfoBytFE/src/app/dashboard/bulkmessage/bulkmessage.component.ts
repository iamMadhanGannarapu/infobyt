import { Component, OnInit ,ViewChild} from '@angular/core';
import {BulkMessage} from '../../modal/Dashboard/BulkMessage'
import {OrganizationService} from '../../service/organization.service'
import { ToastrService } from 'ngx-toastr';
import {MatSort, MatTableDataSource,MatPaginator} from '@angular/material';
import { DashboardService } from "src/app/service/dashboard.service";
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-bulkmessage',
  templateUrl: './bulkmessage.component.html',
  styleUrls: ['./bulkmessage.component.scss']
})
export class BulkmessageComponent implements OnInit,CanActivateChild {
bulkMessage:BulkMessage;
departmentList:any;
classList:any;
  batchList: any;
  displayedColumns=['departmentName','className','batchName','messageType','message','Excel']
  dataSource;
  read: any;
  write: any;
  constructor(private router:Router,  private ls:LoginService,private os:OrganizationService,private toastr: ToastrService ,private ds:DashboardService) { 
    this.bulkMessage=new BulkMessage()
  }
  @ViewChild("message") form: any;
 @ViewChild(MatSort) sort:MatSort;
 @ViewChild(MatPaginator) paginator: MatPaginator;
  addBulkMessage()
  {
    this.ds.saveBulkMessage(this.bulkMessage).subscribe((data)=>
    {
      if(data.result)
        this.toastr.success(data.message)
      else
      this.toastr.error(data.message);
    })
    this.getMessagesList();;
  }
 getDepartments()
 {
this.os.getAllDomains().subscribe((data)=>
{
  this.departmentList=data.data;
})
 }
 getAllClasses(deptId:any)
 {
    this.ds.allSessions(deptId).subscribe((data)=>
    {
      this.classList=data.data;
    })
 }
 getBatches(classId:number)
 {this.ds.allBatches(classId).subscribe((data)=>this.batchList=data.data )}  
 
applyFilter(filterValue: string) {
  this.dataSource.filter = filterValue.trim().toLowerCase();
}
 getMessagesList()
 {
   this.ds.getMessages().subscribe((data)=>
   {
     if(data.result && data.data!='NDF')
     {
    
     this.dataSource=new MatTableDataSource(data.data)
     this.dataSource.sort=this.sort
     this.dataSource.paginator=this.paginator
     }
   })
 }
 getexcel(){
  this.os.exportAsExcelFile( this.dataSource.data, 'Bulk Messages');
}
getIds()
{
  this.getDepartments();
}
getPermissions() {
  this.ls.checkRightPermissions('bulkmessage').subscribe((data)=>{
    this.read=data.data[0].read;
    this.write=data.data[0].write;
    this.canActivateChild(this.read)
  })
}
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }
  ngOnInit() {
    this.getPermissions();
    this.getMessagesList();
    this.bulkMessage.departmentId=0;
    this.bulkMessage.batchId=0;
    this.bulkMessage.sessionId=0;
    this.bulkMessage.messageType=0;
  }
}
