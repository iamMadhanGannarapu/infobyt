import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { DashboardService } from '../../service/dashboard.service'
import { Enquiry } from '../../modal/Login/Enquiry'
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/service/user.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  dataSource;
  displayedColumns = ['Name', 'Email', 'Mobile', 'Message', 'Excel']
  myUrl: string;
  enquiry: Enquiry;
  viewAcademicForm1: boolean;
  stafflogSource: MatTableDataSource<{}>;
  studentlogSource: any;
  constructor(private us: UserService, private toastr: ToastrService,  private ds: DashboardService) {
    this.enquiry = new Enquiry()
    this.myUrl = ds.url;
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("addEnquiryForm") form: any;
  ngOnInit() {
    this.getallEnquirydetails();
    this.getAllStudentLogins();
    this.getAllStaffLogins();
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getallEnquirydetails() {
    this.ds.getEnquiry().subscribe((data) => {
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
    },
      err => {
        this.toastr.error(err.error.message);
      });
  }
  getexcel() {
    this.ds.exportAsExcelFile(this.dataSource, 'login');
  }
  getAllStaffLogins() {
    this.viewAcademicForm1 = true;
    this.us.getAllStafflogs().subscribe((data) => {
      this.stafflogSource = new MatTableDataSource(data.data)
      this.stafflogSource.sort = this.sort
      this.stafflogSource.paginator = this.paginator
    })
  }
  getAllStudentLogins() {
    this.viewAcademicForm1 = true;
    this.us.getAllTraineelogs().subscribe((data) => {
      this.studentlogSource = new MatTableDataSource(data.data)
      this.studentlogSource.sort = this.sort
      this.studentlogSource.paginator = this.paginator
    })
  }
}