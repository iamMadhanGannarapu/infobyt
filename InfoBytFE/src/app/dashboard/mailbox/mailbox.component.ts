import { Component, OnInit } from '@angular/core';
import { Mailbox } from 'src/app/modal/dashboard/Mailbox';
import { DashboardService } from "src/app/service/dashboard.service";
import { LoginService } from 'src/app/service/login.service';
import { ToastrService } from 'ngx-toastr';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-mailbox',
  templateUrl: './mailbox.component.html',
  styleUrls: ['./mailbox.component.scss']
})
export class MailboxComponent implements OnInit,CanActivateChild {
  toList: any
  mailbox: Mailbox
  mailList: any
  write: any;
  read: any;
  constructor(private router:Router,  private ds: DashboardService, private ls: LoginService,private toastr:ToastrService) {
    this.mailbox = new Mailbox();
  }
  ngOnInit() {
    this.getPermissions();
    this.getMail('Inbox');
    this.ds.getUsersOfBranch().subscribe((data) => {
      this.toList = data.data
    })
  }
  getMail(type: any) {
    this.mailList = null;
    this.ds.getInbox(type).subscribe((data) => {
      if (data.result == true && data.data != "NDF")
        this.mailList = data.data
    })
  }
  addMail() {
    this.ds.addMail(this.mailbox).subscribe((data) => {
      if(data.result)
      this.toastr.success(data.message)
      else
      this.toastr.error(data.message)
    })
  }
  getPermissions() {
    this.ls.checkRightPermissions('mailbox').subscribe((data) => {
      this.read = data.data[0].read;
      this.write = data.data[0].write;
      this.canActivateChild(this.read) 
     })
  }
  canActivateChild(x:any):boolean{
    if(x==true){
      return true;
    }else{
      this.router.navigate(['404']);
        return false;
    }
      }
}
