
import { Component, OnInit, ViewChild } from '@angular/core';
import { Gallery } from '../../modal/dashboard/gallery';
import { OrganizationService } from '../../service/organization.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../service/user.service';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { DashboardService } from "src/app/service/dashboard.service";
import {  FileUploader } from 'ng2-file-upload';
import { LoginService } from '../../service/login.service';
import { TreeNode } from 'primeng/api';
import { Router,CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-gallery-master',
  templateUrl: './gallery-master.component.html',
  styleUrls: ['./gallery-master.component.scss']
})
export class GalleryMasterComponent implements OnInit,CanActivateChild {
  schoolBranchList: any
  usr: number
  gallery: Gallery
  Id: number;
  data: any;
  myurl: string;
  colorTheme = 'theme-dark-blue';
  ipath: string
  public uploader: FileUploader
  bsConfig: Partial<BsDatepickerConfig>;
  albumName: any;
  list: any;
  filesTree0: TreeNode[];
  myList: any;
  images: any[];
  selection: number
  selectedFile: TreeNode;
  deleteSelected:any
  show:boolean=false
  write: any;
  read: any;
  ext: any;
  video: any;
  source: string;
  constructor( private router:Router,private toastr: ToastrService, private ds:DashboardService, private os: OrganizationService, private us: UserService, private ls: LoginService) {
    this.gallery = new Gallery()
    this.myurl = os.url
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
  }
  public hasBaseDropZoneOver:boolean = false;
  public hasAnotherDropZoneOver:boolean = false;
  public fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }
  public fileOverAnother(e:any):void {
    this.hasAnotherDropZoneOver = e;
  }
  getUserId() {
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.albumName = 'albm_' + Date.now();
       
      this.usr = data.data[0].userid;
      this.ipath = this.myurl + '/file/upload/' + this.usr + '/' + this.albumName;
      this.uploader = new FileUploader({url: this.ipath, 
        allowedMimeType: ['image/png', 'image/gif', 'video/mp4', 'image/jpeg', 'image/jpg'] });
      this.uploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
        item.remove();
        this.toastr.success("Uploaded Sucessfully")
        this.getAllGallery();
    };
    this.uploader.onAfterAddingFile = f => {
      if (this.uploader.queue.length > 10) {
      this.uploader.removeFromQueue(this.uploader.queue[0]);
      this.toastr.error("Only 10 Files are allowed")
      }
      };
      
    })
  }
  }
  
  
  
getPermissions() {
  this.ls.checkRightPermissions('project').subscribe((data) => {
    this.read =  data.data[0].read;
    this.write =  data.data[0].write;
    this.canActivateChild(this.read)
  })
}

canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }
  setAlbum(album: any) {
  }
  ngOnInit() {
    this.getAllGallery();
    this.getUserId()
    this.ipath = this.myurl + '/file/upload/' + this.usr + '/' + this.albumName;
    this.uploader = new FileUploader({ url: this.ipath });
    this.list = [
      {
        source:this.myurl+'/Organization/assets/angular2-logo-red.png',
        alt:'Gallery',
        title:'Gallery'
      }]
  }
  getIds() {
    this.os.getBranchById().subscribe((data) => {
      this.schoolBranchList = data.data;
    })
  }
  getAllGallery() {
    this.ds.GetallGallery().subscribe((folders) => {
      this.myList = JSON.stringify(folders)
      for (var i = 0; i < folders.length; i++) {
        this.myList = this.myList.replace("expandedicon", "expandedIcon");
        this.myList = this.myList.replace("collapsedicon", "collapsedIcon");
      }
      this.filesTree0 = JSON.parse(this.myList);
    })
  }
  nodeSelect(event) {
    this.selection = event.node.data
   
   this.ext= (event.node.label.split('.').pop())

   if(this.ext== 'mp4'){
     this.video= event.node.label
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        
      this.usr = data.data[0].userid;
      })
    }
    this.source= this.myurl+'/file/Public/Video/'+this.usr+'/'+this.selection+'/'+this.video
    this.selection=null
   // alert(this.source)
  }
    this.list=''
    
    
    this.ds.getGalleryImages(this.selection).subscribe((data) => {
      this.list = data;
     
    })
  }


  holdDeleteNodeSelect(event){
  this.deleteSelected= event.node.data
  }
  delete(){
    this.ds.deleteGall(this.deleteSelected).subscribe((data)=>{
    })
  }
  showList(){
    this.show=true
  }
  refreshGallery(){
    this.getAllGallery();
  }
}
