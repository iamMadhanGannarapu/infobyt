import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../service/user.service';
import { UserUploads } from '../../modal/Dashboard/Uploads';
import { LoginService } from "../../service/login.service";
import { ToastrService } from 'ngx-toastr';
import { FileUploader } from 'ng2-file-upload';
import { DashboardService } from "src/app/service/dashboard.service";
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-uploads',
  templateUrl: './uploads.component.html',
  styleUrls: ['./uploads.component.scss']
})
export class UploadsComponent implements OnInit,CanActivateChild {
  file: UserUploads
  uploadlist: any[] = []
  userSession: any
  usr: any;
  public uploader: FileUploader;
  myurl: any;
  socialList: any;
  toList: any;
  url: string;
  holdFile: string
  sharedList: any;
  ipath = '';
  read: any;
  write: any;
  constructor(private router:Router, private ls:LoginService,private toastr: ToastrService, private fs: UserService,private ds:DashboardService) {
    this.userSession = sessionStorage.getItem('user')
    this.file = new UserUploads()
    this.myurl = ls.url
  }
  @ViewChild("shareForm") form: any;
getUserId(){
  if (sessionStorage.getItem('user') != null) {
    this.fs.getUserbySession().subscribe((data) => {
      this.usr = data.data[0].userid;
      this.ipath= this.myurl + '/file/Add/' + this.usr;
      this.uploader = new FileUploader({ url: this.ipath });
     })
  }
}
  viewUploads() {
    this.ds.getFileUploads(this.usr).subscribe((data) => {
      this.uploadlist = data.data
     })
  }
  getPermissions() {
    this.ls.checkRightPermissions('uploads').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  canActivateChild(x:any):boolean{
    if(x==true){
      return true;
    }else{
      this.router.navigate(['404']);
        return false;
    }
      }
  ngOnInit() {
    this.getPermissions();
   this.getUserId()
   this.ipath= this.myurl + '/file/Add/' + this.usr;
   this.uploader = new FileUploader({ url: this.ipath });
  }
  single() {
    this.uploader.onAfterAddingFile = f => {
      if (this.uploader.queue.length > 1) {
        this.uploader.removeFromQueue(this.uploader.queue[0]);
      }
    };
  }
  refreshUploads() {
    this.viewUploads();
  }
  sharedUploads() {
    this.ds.getSharedUploads(this.usr).subscribe(data => {
      this.sharedList = data.data;
    });
  }
  deleteFile() {
    this.ds.deleteFileList(this.holdFile, this.usr).subscribe(() => {
      this.refreshUploads()
    })
  }
  getSocialId(event) {
    let query = event.query;
    this.ds.getSocialNetworkId(this.usr).subscribe(data => {
      this.socialList = this.filterSSI(query, data.data);
    });
  }
  filterSSI(query, data: any[]): any[] {
    let filtered: any[] = [];
    for (let i in  data) {
      let users = data[i];
      let namess = users.socialnetworkid
      if (namess.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(users);
      }
    }
    return filtered;
  }
  addSharing(id: any) {
    this.file.fUserId = this.usr;
    this.file.socialNetworkId = id;
    this.file.url = this.url
    this.ds.saveSharing(this.file).subscribe((data) => {
      if (data) {
        this.toastr.success('Shared Sucessfully');
        this.form.reset();
      }
    },
      err => {
        this.toastr.error(err.error.message);
      })
  }
  addShareFile(m: string) {
    this.url = m
  }
  holdDeleteFile(n: string) {
    this.holdFile = n;
  }
  getdetails(i: any) {
    this.uploadlist[i].view = true
  }
  mouseLeave(i: any) {
    this.uploadlist[i].view = false
  }
}