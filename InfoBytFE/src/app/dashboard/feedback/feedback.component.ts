import { Component, OnInit, ViewChild } from '@angular/core';
import { BatchService } from '../../service/batch.service';
import { Feedback } from '../../modal/Dashboard/Feedback'
import { UserService } from '../../service/user.service';
import { ToastrService } from 'ngx-toastr';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { DashboardService } from 'src/app/service/dashboard.service';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit ,CanActivateChild{
  dataSource;
  displayedColumns = ['FeedbackDescription', 'Rating', 'Excel']
  feed: Feedback;
  viewfeedback: boolean;
  userList: any
  rolename: any;
  read: any;
  write: any;
  constructor(private router:Router,private ls: LoginService, private toastr: ToastrService, private bs: BatchService, private us: UserService, private ds: DashboardService) {
    this.feed = new Feedback();
  }
  @ViewChild("feedback") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  addFeedBack() {
    this.ds.saveFeedback(this.feed).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.ds.addFeedbackNotification(data.data[0].fn_addfeedback).subscribe((data) => {

        })
        this.getAllFeedbacks();
        this.form.reset();
      }
      else {
        this.toastr.error(data.message);
      }
    });
  }
  applyFilter(filterValue: string) {
    
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getIds() {
    this.us.getAllStaffUsersForFeedBack().subscribe((data) => {
      this.userList = data.data
    })
  }
  getAllFeedbacks() {
    this.ds.getFeedBack().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      }
    })
  }
  getexcel() {
    this.bs.exportAsExcelFile( this.dataSource.data, 'feedback');
  }
  getPermissions() {
    this.ls.checkRightPermissions('feedback').subscribe((data) => {
      this.read = data.data[0].read;
      this.write = data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  canActivateChild(x:any):boolean{
    if(x==true){
      return true;
    }else{
      this.router.navigate(['404']);
        return false;
    }
      }
  ngOnInit() {
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename = data.data[0].rolename;
        if (this.rolename == "BranchAdmin" || this.rolename == "SchoolAdmin" || this.rolename == 'Trainee') {
        }
      })
    }
    this.getPermissions();
    this.getAllFeedbacks();
    this.getIds()
    this.feed.userId = 0;
  }
  viewFeedback() {
    this.viewfeedback = !this.viewfeedback
  }
}