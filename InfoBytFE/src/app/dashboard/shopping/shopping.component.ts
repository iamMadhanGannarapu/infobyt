
import { Component,ChangeDetectionStrategy, OnInit, ViewChild } from '@angular/core';
import { Shopping } from 'src/app/modal/Dashboard/shopping';
import { UserService } from 'src/app/service/user.service';
import { ContactDetails } from '../../modal/User/ContactDetails';
import { Observable, of } from 'rxjs';
import { BatchService } from 'src/app/service/batch.service';
import { Cart } from 'src/app/modal/Dashboard/cart';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { ImageCompressService } from  'ng2-image-compress';

import { DashboardService } from "src/app/service/dashboard.service";
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-shopping',
  templateUrl: './shopping.component.html',
  styleUrls: ['./shopping.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShoppingComponent implements OnInit,CanActivateChild {
  shoppingObj: Shopping;
  contactDetail: ContactDetails
  dataSource;
  image = { 'URL': '', 'valid': false };
  bsConfig: Partial<BsDatepickerConfig>;
  colorTheme = 'theme-dark-blue';
  color = "success"
  images: File;
  displayedColumns = ['productname', 'productprice','Image', 'productdate', 'description', 'classname', 'available']
  classList: any;
  cartlist: any;
  quantuty1: any;
  ContactDetails: any;
  cartObj: Cart;
  shoppinglistForUsers: any;
  viewsession: boolean;
  rolename: any;
  contactDetails: any;
  usercountryList: any[] = [];
  userstateList: any;
  usercitiesList: any;
  myurl: string;
  cartTotalAmount: any = 0;
  read: any;
  write: any;
  constructor(private router:Router,  private ls:LoginService,private imgCompressService: ImageCompressService,private ds:DashboardService,private us: UserService, private bs: BatchService, private toastr: ToastrService) {
    this.shoppingObj = new Shopping();
    this.contactDetail = new ContactDetails()
    this.cartObj = new Cart();
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    this.myurl = us.url;
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("frm") form: any;
  ngOnInit() {
    this.getCountries();
    this.getAllClasses();
    this.getAllShopping();
    this.getPermissions();
    this.getAllProductsForUsers();
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename = data.data[0].rolename;
      }, (err) => {
        this.toastr.error(err.error.message)
      });
    }
    this.shoppingObj.category='0';
    this.shoppingObj.sessionId=0;
  }
  getPermissions() {
    this.ls.checkRightPermissions('shopping').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  
  canActivateChild(x:any):boolean{
    if(x==true){
      return true;
    }else{
      this.router.navigate(['404']);
        return false;
    }
      }
  checkout() {
    this.us.getUserbySession().subscribe((data) => {
      var userid = data.data[0].userid;
      this.ds.checkoutOrders(this.cartlist,userid).subscribe((data) => {
        if (data) {
          this.toastr.success("Your order has placed..")
          this.getAllCart()
        }
      })
    })
  }
  deleteProduct(i: any) {
    this.ds.deleteCartProduct(this.cartlist[i].cartid).subscribe((data) => {
      if (data) {
        this.toastr.success("deleted successfully...")
        this.getAllCart()
      }
    })
  }
  getCountries() {
    this.us.getAllCountries().subscribe((data) => {
      this.usercountryList = data.data
    })
  }
  getStates(couId: any): Observable<any> {
    this.us.getAllStates(parseInt(couId)).subscribe((data) => {
      this.userstateList = data.data
    })
    return of(JSON.stringify(this.contactDetails));
  }
  getCities(stateId: number) {
    this.us.getAllCities(stateId).subscribe((data) => {
      this.usercitiesList = data.data
    })
  }
  applyFilter(filterValue: string) {
  
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  addShoppingProduct() {
    this.ds.saveSellerProduct(this.shoppingObj).subscribe((data) => {
      if (data) {
        this.getAllShopping();
      }
    })
  }
  addBillingAddress() {
    let user = JSON.parse(sessionStorage.getItem("Users"));
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        var userid = data.data[0].userid
        this.contactDetail.userId = data.data[0].userid;
        this.us.saveContactDetails(this.contactDetail).subscribe((data) => {
          this.toastr.success('Registred Successfully');
        })
      }, (err) => {
        this.toastr.error(err.error.message)
      });
    }
  }
  
  fileChangeEvent(fileInput: any) {
    var fileName = fileInput.target.value;
    var idxDot = fileName.lastIndexOf(".") + 1;
    var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
    if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
      ImageCompressService.filesToCompressedImageSource(fileInput.target.files).then(observableImages => {
        observableImages.subscribe((image) => {
            this.shoppingObj.image = image.compressedImage.imageDataUrl;
            this.image.URL=  image.compressedImage.imageDataUrl;
            this.shoppingObj.image = this.shoppingObj.image.replace("data:image/gif;base64,", "");
            this.shoppingObj.image = this.shoppingObj.image.replace("data:image/jpeg;base64,", "");
            this.shoppingObj.image = this.shoppingObj.image.replace("data:image/jpg;base64,", "");
            this.shoppingObj.image = this.shoppingObj.image.replace("data:image/png;base64,", "");
        }, (error) => {
          this.toastr.error("Error while converting");
        });
      });
    }else{
      alert("Only jpg/jpeg and png files are allowed!");
     
    }
  }

  checkout2(){
    this.toastr.success("Your Order has been placed")
  }
  addCart(id: any) {
    let user = JSON.parse(sessionStorage.getItem("Users"));
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.cartObj.cartAllocationId = data.data[0].userid;
        this.cartObj.sellerId = id;
        this.ds.saveCart(this.cartObj).subscribe((data) => {
          if (data) {
            this.toastr.success("Added to Cart Successfully")
            this.getAllCart()
          }
        })
      }, (err) => {
        this.toastr.error(err.error.message)
      });
    }
  }
  getAllClasses() {
    this.bs.getAllSession().subscribe((data) => {
      this.classList = data.data;
    })
  }
  getAllShopping() {
    this.ds.getAllShoppingList().subscribe((data) => {
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
    }, (err) => {
      this.toastr.error(err.error.message)
    })
  }
  getAllProductsForUsers() {
    this.ds.getShoppingForUsers().subscribe((data) => {
      this.shoppinglistForUsers = data.data
    })
  }
  getAddressDetails() {
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        var userid = data.data[0].userid
        this.ds.getContactDetails(userid).subscribe((data) => {
          this.ContactDetails = data.data
        })
        this.rolename = data.data[0].rolename;
      }, (err) => {
        this.toastr.error(err.error.message)
      });
    }
  }
  getAllCart() {
    this.ds.getAllCartList().subscribe((data) => {
      this.cartlist = data.data
      if (data.length != 0) {
        this.calculateCartAmount();
      }
    })
  }
  increment(value1: any, i: any) {
    var value = parseInt(value1.value) + 1
    if (value1 != 0) {
      this.cartlist[i].quantity = value
    }
    this.calculateCartAmount();
  }
  decerement(value1: any, i: any) {
    var value = parseInt(value1.value) - 1
    if (value1 != 0) {
      this.cartlist[i].quantity = value;
    }
    this.calculateCartAmount();
  }
  trackByIndex(index: number, obj: any): any {
    return index;
  }
  viewBilling() {
    this.viewsession = !this.viewsession
  }
  onChange(status, id) {
    this.ds.getShoppingById(id).subscribe((data) => {
      this.shoppingObj.Id = data.data[0].id;
      this.shoppingObj.availabile = status.checked;
      this.ds.updateProduct(this.shoppingObj).subscribe((data) => {
        this.getAllProductsForUsers();
        if (data) {
          this.getAllProductsForUsers();
        }
      })
    })
  }
  calculateCartAmount() {
    this.cartTotalAmount = 0;
    for (var i in this.cartlist) {
      this.cartTotalAmount = this.cartTotalAmount + ((parseFloat(this.cartlist[i].productprice) * parseInt(this.cartlist[i].quantity)))
    }
  }
}