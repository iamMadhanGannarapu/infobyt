export class Shifts {
    Id: number;
    branchId: number;
    shiftName: any;
    days: any;
    startTime: any;
    endTime: any;
    monday: any=true;
    tuesday: any=true;
    wednesday:any=true;
    thursday:any=true;
    friday:any=true;
    saturday:any=true;
    sunday:any=true;
    graceInTime:any;
    graceOutTime:any;
    optional:any;
}