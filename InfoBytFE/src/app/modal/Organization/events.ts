export class Events {
    Id: number;
    branchId: number;
    eventName: String;
    organizerMobile: number;
    sponsors: String;
    fromDate: any;
    toDate: any;
    image: any
}