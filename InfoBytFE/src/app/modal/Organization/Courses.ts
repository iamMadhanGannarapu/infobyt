export class Course{
    Id:number;
    courseName:string;
    years:number;
    semesters:string;
    level:string;
    duration:string;
}
