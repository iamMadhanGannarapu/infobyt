export class Organization {
    Id: number
    name: string
    type: string
    status: string
    establishedYear: any
    registrationDate: any
    contactDetailsId: number
    image: any
    boardId:any;
    organizationTypeId:any;
}
