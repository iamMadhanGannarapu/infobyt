export class BranchSession {
    Id: number;
    sessionId: number;
    departmentid :number;
    branchId: number;
    medium: string;
    numOfTrainees: number;
}