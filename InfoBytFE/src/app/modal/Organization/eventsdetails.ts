import { Time } from "ngx-bootstrap/timepicker/timepicker.models";
export class EventsDetails {
    Id: number;
    name: string;
    departmentId: number;
    fromDate: any;
    toDate: any;
    startTime: Time;
    endTime: Time;
    userId: number;
    contactdetailsId: number;
    description: string;
    venue: string;
    branchId: number;
}