export class Branch {
    Id: number;
    organizationId: number;
    startDate: any;
    status: string;
    contactDetailsId: number;
    latitude:any;
    longitude:any;
}