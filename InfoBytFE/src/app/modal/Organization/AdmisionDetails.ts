export class AdmisionDetails {
    admissionNum: number
    parentEmail: string
    parentMobile: string
    branchId: number
    userId: number
    feeId: number
    concessionId: number;
    image:any;
}