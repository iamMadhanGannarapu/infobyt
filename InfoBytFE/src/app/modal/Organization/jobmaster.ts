
export class jobMaster{
    Id:number;
    jobTitles:string;
    companyName:string;
    requirements:string;
    experience: any;
    location:string;
    vacancy:number;
    jobDescription:string;
    jobId:number;
    salary: any;
    role:string;
    postedBy:string;
    mobile:number;
}
