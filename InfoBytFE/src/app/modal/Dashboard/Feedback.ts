export class Feedback {
    Id: number;
    userId: number;
    feederId: number;
    description: string;
    rating: any;
}