export class Mailbox{
    Id:number;
    fromUser:number;
    toUser:number;
    subject:string;
    mail:string;
    status:string;
    conversationId:number;
}