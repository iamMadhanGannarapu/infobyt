export class OrganizationFeedback {
    Id: number;
    organizationId: number;
    feederId: number;
    description: string;
    rating: any;
}