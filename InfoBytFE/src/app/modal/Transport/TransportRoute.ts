export class TransportRoute {
    Id: number;
    name: string;
    startPoint: string;
    endPoint: string;
    busId: number;
}
