export class TransportCharge {
    Id: number;
    name: string;
    details: string;
    transportCharge: any;
    routeId: number;
}