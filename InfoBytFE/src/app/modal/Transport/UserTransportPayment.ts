export class UserTransportPayment {
    Id: number;
    userId: number;
    paidAmount: number;
    paidDate: any;
    due: number;
    status: string='INACTIVE';
}
