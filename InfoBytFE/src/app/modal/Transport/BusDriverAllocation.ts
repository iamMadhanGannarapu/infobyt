export class BusDriverAllocation {
    Id: number;
    busId: number;
    driverId: number;
    date: any;
    status: string;
}