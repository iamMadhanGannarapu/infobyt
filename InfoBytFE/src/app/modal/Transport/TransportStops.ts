export class TransportStops {
    Id: number;
    routeId: string;
    name: string;
    arrivalTime: any;
    departureTime: any;
    latitude:any;
    longitude:any;
}
