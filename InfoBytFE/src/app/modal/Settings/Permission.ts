export class Permission{
    Id:Number;
    roleName:string;
    componentName:string;
    read:Boolean;
    write:Boolean
    componentId:any
    roleId:any
    branchId:number
}
