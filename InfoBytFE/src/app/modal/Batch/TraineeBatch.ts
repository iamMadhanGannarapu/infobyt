export class TraineeBatch{
    batchId:number
    traineeId:number
    status:string='INACTIVE';
    startDate:Date
    branchId:number;
}