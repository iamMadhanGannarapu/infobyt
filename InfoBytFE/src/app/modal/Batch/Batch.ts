export class Batch {
    Id: number;
    staffId: number;
    sessionId: number;
    startDate: any;
    endDate: any;
    status: string;
    name:string;
}