export class Staff {
    Id: number;
    userId: number;
    branchId: number;
    doj: any;
    experience: any;
    salary: any;
    userRoleId: number;
    salaryPattern:any;
    image:any;
    dy?:any;
    dm?:any;
    departmentId:number;
    shiftId:number;
    otPay:any;
}
