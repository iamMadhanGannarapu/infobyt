export class FeePayments{
    Id:number;
    paidAmount:any;
    payDate:any;
    due:any;
    payMode:string;
    status:string='INACTIVE';
    traineeId:number;
}