export class FeeStructure {
    Id: number;
    feePattern: any
    feeName: string;
    amount: any;
    optional: boolean;
    emi:boolean;
    sessionId:number;
    sessionName:string;
}