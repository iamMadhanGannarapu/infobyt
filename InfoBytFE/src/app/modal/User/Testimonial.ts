export class Testimonial {
    id:	number;
    name:string;
    description:string;
    postedDate:Date
    image:any
}