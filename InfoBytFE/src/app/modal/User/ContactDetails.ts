export class ContactDetails {
    Id: number;
    userId: number;
    country: number;
    state: number;
    city: number;
    place:string;
    address:string
    uniqueIdentificationNo: string;
    email: string;
    mobile: string;
    loginId:string;
}
