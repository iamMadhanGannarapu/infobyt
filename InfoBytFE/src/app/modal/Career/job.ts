
export class Job {
    Id: number;
    jobTitles: string;
    jobDescription: string;
    designation: string;
    minimumExperience: number;
    maximumExperience: number;
    anualCtc: any;
    otherSalary: any;
    vacancy: number;
    location: string;
    functionalArea: string;
    jobId: number;
    ugQualificationId: number;
    pgQualificationId: number;
    otherQualificationId: number;
    requiterEmail: any;
    organizationId: number;
    aboutOrganization: string;
    postingDate: any;
    image: any;
}
export class InterviewSchedule {
    Id: number;
    fromtime: any;
    totime: any;
    fromid: number;
    toid: number
    schedule:any
}
