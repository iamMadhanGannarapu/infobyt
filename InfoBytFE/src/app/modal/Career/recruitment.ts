
export class Recruitment{
    Id:number;
    jobApplicantId:number;
    performanceRating:any;
    onTimeRating:any;
    behaviourRating:any;
    appearenceRating:any;
    communicationRating:any;
    interviewStatus:string;

}
export class SelectedApplicants{
    Id:number;
    recruitId:number;
    applicantId:number;
    shiftId:number;
  doj:Date;
  annualCTC:any;
}
