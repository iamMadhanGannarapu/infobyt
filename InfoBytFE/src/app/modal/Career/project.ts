export class project{
    Id:number;
    branchId:number;
    projectName:string;
    assignedDate:any;
    submissionDate:any;
    type:string;
    details:string;
    teamSize:number;
    fee:number;
    category:string;
}
