export class LeaveDetails {
    Id: number;
    userId: number;
    leaveDate: any;
    fromDate: any;
    toDate: any;
    type: string;
    description: string;
    status: string;
    approvedBy: number;
}