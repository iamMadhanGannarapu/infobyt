
export class Tds {
    Id: number;
    branchId: number;
    tdsStartRange: any;
    tdsEndRange: any;
    tdsName: string;
    tdsAmount: any;
}
