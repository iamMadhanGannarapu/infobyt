export class SalaryPaymentsInfo {
    Id: number
    staffId: number
    paidAmount: any
    payDate: any
    payable: number
    allowances: any
    deductions: any
    bonus: any
    payAllowanceDeductionDetails: string
    remarks: string
}