
export class Library{
    Id:number;
    branchId:number;
    departmentId:number;
    bookName:string;
    authorName:string;
    description:string;
    segregation:string
    penalty:number
}
