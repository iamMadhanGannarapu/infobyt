
import { Component, OnInit, ViewChild } from '@angular/core';
import { Institute } from '../../modal/Settings/Institute'
import { InstituteType } from '../../modal/Settings/institutetype'
import { OrganizationService } from 'src/app/service/organization.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from "../../service/user.service";
import { SettingsService } from "src/app/service/settings.service";
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';

import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-institute',
  templateUrl: './institute.component.html',
  styleUrls: ['./institute.component.scss']
})
export class InstituteComponent implements OnInit, CanActivateChild {
  displayedColumns = ['instituteName', 'Excel']
  dataSource;
  displayedColumns1 = ['Organizations', 'boardName', 'Excel']
  displayedColumns2 = ['Organization', 'InstitueName', 'Excel'
  ]
  dataSources;
  dataSourcess;
  institution: Institute;
  institutiontype: InstituteType;
  instituteList: any;
  res: boolean = false;
  show1: boolean = true;
  Institutevalidation;
  Boardvalidation;
  read: any;
  write: any;
  constructor(private router: Router, private ls: LoginService, private os: OrganizationService, private toastr: ToastrService, private sts: SettingsService) {
    this.institution = new Institute();
    this.institutiontype = new InstituteType();
  }
  @ViewChild('institute') form: any;
  @ViewChild('institutetype') form1: any;
  @ViewChild('board') form2: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort2: MatSort;
  @ViewChild(MatPaginator) paginator2: MatPaginator;
  addInstitute() {
    this.sts.saveInstitute(this.institution).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message)
        this.form.reset();
      }
      else
        this.toastr.error(data.message)
      this.getInstitute();
    }, (err) => {
      this.toastr.error(err.error.message)
    });
  }
  addInstitutetype() {
    this.sts.saveInstitutetype(this.institutiontype).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.form1.reset();
      }
      else
        this.toastr.error(data.message)
      this.getInstitutetype();
    });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase()
  }
  applyFilter2(filterValue: string) {
    this.dataSourcess.filter = filterValue.trim().toLowerCase()
  }
  @ViewChild('board') formB: any;
  @ViewChild(MatSort) sort1: MatSort;
  @ViewChild(MatPaginator) paginator1: MatPaginator;
  addBoard() {
    this.sts.saveBoard(this.institution).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message)
        this.formB.reset();
      }
      else { this.toastr.error(data.message) }
      this.getBoards()
    })
  }
  applyFilter1(filterValue: string) {

    this.dataSources.filter = filterValue.trim().toLowerCase()
  }
  getInstitute() {
    this.sts.getInstitutes().subscribe((res) => {
      if (res.result && res.data != 'NDF') {
        this.instituteList = res.data
        this.dataSource = new MatTableDataSource(res.data)
        this.dataSource.sort = this.sort
        this.dataSource.paginator = this.paginator
      }
    })
  }
  getexcel() {
    this.os.exportAsExcelFile(this.dataSource.data, 'Institute Details');
  }
  getOrgCategoryexcel() {
    this.os.exportAsExcelFile(this.dataSources.data, 'Institute Details');
  }
  getInstituteTypeexcel() {
    this.os.exportAsExcelFile(this.dataSourcess.data, 'Organization Categories');
  }
  getInstitutetype() {
    this.sts.getInstitutestype().subscribe((res) => {
      if (res.result && res.data != 'NDF') {
        this.dataSourcess = new MatTableDataSource(res.data)
        this.dataSourcess.sort = this.sort
        this.dataSourcess.paginator = this.paginator
      }
    })
  }
  getBoards() {
    this.sts.getBoardType().subscribe((res) => {
      if (res.result && res.data != 'NDF') {
        this.dataSources = new MatTableDataSource(res.data)
        this.dataSources.sort = this.sort
        this.dataSources.paginator = this.paginator
      }
    })
  }
  verifyInstitute(inp: any) {
    this.sts.verifyInstituteData(inp, this.institution.instituteName).subscribe((data) => {

      this.Institutevalidation = data.data.result;
      if (this.Institutevalidation == false) {
        this.res = true;
      }
    });
  }
  verifyBoard(inp: string) {
    this.sts.verifyBoardData(inp, this.institution.boardName).subscribe((data) => {
      this.Boardvalidation = data.data.message;
    })
  }
  getPermissions() {
    this.ls.checkRightPermissions('institute').subscribe((data) => {
      this.read = data.data[0].read;
      this.write = data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  canActivateChild(x: any): boolean {
    if (x == true) {
      return true;
    } else {
      this.router.navigate(['404']);
      return false;
    }
  }

  ngOnInit() {
    $(document).ready(function () {
      $("#datahide1").hide();
      $("#datahide2").hide();
      $("#datahide3").hide();


      $("#showdata1").click(function () {
        $("#vieworg").show()
        $("#showdata1").hide()
        $("#datahide1").show();
      });
      $("#showdata2").click(function () {
        $("#viewaffl").show()
        $("#showdata2").hide()
        $("#datahide2").show();
      });

      $("#showdata3").click(function () {
        $("#viewinstyp").show()
        $("#showdata3").hide()
        $("#datahide3").show();
      });
      $("#datahide1").click(function () {
        $("#vieworg").hide()
        $("#showdata1").show()
        $("#datahide1").hide();
      });

      $("#datahide2").click(function () {
        $("#viewaffl").hide()
        $("#showdata2").show()
        $("#datahide2").hide();
      });
      $("#datahide3").click(function () {
        $("#viewinstyp").hide()
        $("#showdata3").show()
        $("#datahide3").hide();
      });
    });


    this.institution.organizationTypeId = 0;
    this.institutiontype.organizationTypeId = 0;
    this.getInstitute()
    this.getBoards()
    this.getInstitutetype()
    this.getPermissions();
  }
}
