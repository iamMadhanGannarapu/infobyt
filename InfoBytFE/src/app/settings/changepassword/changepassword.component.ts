import { Component, OnInit,ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { SettingsService } from "src/app/service/settings.service";
import { LoginService } from 'src/app/service/login.service';
@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.scss']
})
export class ChangepasswordComponent implements OnInit {
list:any
  read: any;
  write: any;
  constructor(private ls:LoginService,private toastr:ToastrService,private sts:SettingsService) { }
@ViewChild("changePasswordForm") form:any;
changePassword(oldpassword:any,newpassword:any)
{
 this.sts.getLoginPassword(oldpassword.value).subscribe((data)=>{
   this.list=data.data
if(this.list.length==1)
{
this.sts.updatePassword(newpassword.value).subscribe((data)=>{
  this.toastr.success('Password Has Been Updated Successfully');
this.form.reset();
})
}
else{
  this.toastr.error('Please Enter Current Password')
}
})
}
getPermissions() {
  this.ls.checkRightPermissions('changepassword').subscribe((data)=>{
    this.read=data.data[0].read;
    this.write=data.data[0].write;
  })
}
  ngOnInit() {
    this.getPermissions();
  }
}
