import { Component, OnInit, ViewChild } from '@angular/core';
import { Permission } from 'src/app/modal/Settings/permission';
import { SettingsService } from "src/app/service/settings.service";
import { UserService } from 'src/app/service/user.service';
import { OrganizationService } from 'src/app/service/organization.service';
import { Router, CanActivateChild } from '@angular/router';
import { LoginService } from 'src/app/service/login.service';
@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.scss']
})
export class PermissionComponent implements OnInit,CanActivateChild  {
  
  permission: Permission
  rolesList: any;

  modules: any;
  roleId: number
  childrenList: any = []
  branchList: any;
  selectedRoleId: any
  branchesList: any;
  rolename: any;
  branchId: any;
  moduleId: any;
  
  read: any;
  write: any;
  constructor(private ls:LoginService,private router:Router,private us: UserService, private sts: SettingsService, private os: OrganizationService) {
    this.permission = new Permission();
  }
  @ViewChild('frm') form: any;

  getAllRoles() {
    this.us.getAllRole().subscribe((data) => {
      this.rolesList = data.data
    })
  }
  getChildren(i: any) {
    this.moduleId = i;
    this.sts.getmodules(this.selectedRoleId, this.branchId.value).subscribe((data) => {
      this.modules = data.data;
      this.childrenList = i.components
    })
  }
  getRole(pRoleId) {
    this.selectedRoleId = pRoleId.value
  }
  getPermissions(pBranchId) {
    this.branchId = pBranchId;
    this.sts.getmodules(this.selectedRoleId, this.branchId.value).subscribe((data) => {
      this.modules = data.data
    })
  }
  getPermission() {
    this.ls.checkRightPermissions('permisssion').subscribe((data) => {

      this.read = data.data[0].read;
      this.write = data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }

  getAllBranches() {
    this.os.getAllBranch().subscribe((data) => {
      this.branchesList = data.data;
    })
  }
  getBranchForBranchAdmin() {
    this.os.getBranchById().subscribe((data) => {
      this.branchList = data.data;
    })
  }
  ngOnInit() {
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
      
        this.rolename =data.data[0].rolename;
      })
    }
    this.getAllRoles();
    this.getAllBranches();
    this.getBranchForBranchAdmin();
  }
  update(i: any, value1: any, value2: any) {
  
    this.permission.componentId = i;
    this.permission.write = value2;
    this.permission.read = value1;
    this.sts.savePermissions(this.permission).subscribe((data) => {
      this.sts.getmodules(this.selectedRoleId, this.branchId.value).subscribe((data) => {
        this.modules = data.data;
        this.moduleId.components.forEach(component => {
          if(i==component.componentid){
         
            component.read=value1;
            component.write=value2;
          }
        });
        this.childrenList = this.moduleId.components
      })
    })
  }
}