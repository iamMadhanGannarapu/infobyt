import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { BillingComponent } from './billing/billing.component';
import { InstituteComponent } from './institute/institute.component';
import { PermissionComponent } from './permission/permission.component';
import { RouteGuardGuard, CanDeactivateGuard } from '../service/route-guard.guard';
import { CountryComponent } from "./country/country.component";
import { RoleMasterComponent } from './role-master/role-master.component';
const routes: Routes = [
  { path: 'Billing', component: BillingComponent, canActivate: [RouteGuardGuard],canDeactivate:[CanDeactivateGuard] },
    { path: 'Changepassword', component: ChangepasswordComponent,canDeactivate:[CanDeactivateGuard] },
    { path: 'Permission', component: PermissionComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard] ,canActivateChild:[PermissionComponent]},
    { path: "Institute", component: InstituteComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard],canActivateChild:[InstituteComponent] },
  { path: 'Country', component: CountryComponent,canDeactivate:[CanDeactivateGuard],canActivateChild:[CountryComponent]},
  { path: 'Roles', component: RoleMasterComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard] }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [RouteGuardGuard,CanDeactivateGuard,InstituteComponent,CountryComponent,PermissionComponent,RoleMasterComponent,BillingComponent]
})
export class SettingsRoutingModule { }
