import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../service/user.service';
import { Country } from '../../modal/Settings/country';
import { Cities } from '../../modal/Settings/cities';
import { States } from '../../modal/Settings/states';
import { ToastrService } from 'ngx-toastr';
import { SettingsService } from "src/app/service/settings.service";
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss']
})
export class CountryComponent implements OnInit,CanActivateChild  {
  countryObj: Country;
  statesObj: States;
  citiesObj: Cities;
  
  read: any;
  write: any;
  constructor(private router:Router,private ls:LoginService,private toastr: ToastrService, private us: UserService,private sts:SettingsService) {
    this.countryObj = new Country();
    this.statesObj = new States();
    this.citiesObj = new Cities();
  }
  @ViewChild("frm") form: any;
  addCountry() {
    this.sts.saveCountry(this.countryObj).subscribe((data) => {
      if(data.result)
      this.toastr.success(data.message)
      else
      this.toastr.error(data.message)
    })
  }
  addStates(cc: any) {
    this.statesObj.countryId = cc.value;
    this.us.saveStates(this.statesObj).subscribe((data) => {
      if(data.result)
      this.toastr.success(data.message)
      else
      this.toastr.error(data.message)
    })
  }
  addCities(n: any) {
    this.citiesObj.stateId = n.value
    this.us.saveCities(this.citiesObj).subscribe((data) => {
      if(data.result)
      this.toastr.success(data.message)
      else
      this.toastr.error(data.message)
    })
  }
  getPermissions() {
    this.ls.checkRightPermissions('country').subscribe((data) => {

      this.read =  data.data[0].read;
      this.write =  data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }

  ngOnInit() {
  }
}
