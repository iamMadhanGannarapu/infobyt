import { Component, OnInit, ViewChild } from '@angular/core';
import { Billing } from 'src/app/modal/Settings/billing';

import { SettingsService } from "src/app/service/settings.service";
@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.scss']
})
export class BillingComponent implements OnInit {
  actualpay: any = 1000;
  discount: any;
  GST: any = 9;
  CGST: any = 9;
  x: any;
  y: any;
  z: any;
  data: any;
  today: any = new Date();
  dd: any = this.today.getDate();
  mm: any = this.today.getMonth() + 1;
  mmf: any = this.today.getMonth() + 3
  yy: any = this.today.getFullYear();
  yy1: any = this.today.getFullYear() + 1;
  yy2: any = this.today.getFullYear() + 2
  cday: any = this.mm + '/' + this.dd + '/' + this.yy;
  trail: any = this.mmf + '/' + this.dd + '/' + this.yy;
  premium: any = this.mm + '/' + this.dd + '/' + this.yy1;
  gold: any = this.mm + '/' + this.dd + '/' + this.yy2;
  bill: Billing;
  billList: any;

  scAdminDetails: Object;
  updbill: any;
  constructor(private sts: SettingsService) {
    this.bill = new Billing();
    this.data = {
      labels: ['A', 'B', 'C'],
      datasets: [
        {
          data: [300, 50, 100],
          backgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56"
          ],
          hoverBackgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56"
          ]
        }]
    };
  }
  @ViewChild("frm") form: any;
  free(fr: any) {
    this.actualpay = fr
    this.bill.amount = fr
    this.bill.startDate = this.cday;
    this.bill.endDate = this.trail;
    this.bill.status = 'Active'
  }
  saveBil() {
    this.sts.saveBill(this.bill).subscribe((data) => {
      this.billList = data.data
    })
  }
  getScAdmin() {
    this.sts.getSAdminBySession().subscribe((data) => {
      this.scAdminDetails = data.data;
    })
  }
  editBill() {
    this.sts.getBillDetails().subscribe((data) => {
      this.bill.Id = data.data[0].id;
      this.bill.userId = data.data[0].usid;
      this.bill.organizationId = data.data[0].schlid;
      this.bill.startDate = data.data[0].buydate;
      this.bill.endDate = data.data[0].enddate;
      this.bill.amount = data.data[0].amount;
      this.bill.status = data.data[0].status;
    })
  }
  updateBill() {
    this.sts.updBillDetails(this.bill).subscribe((data) => {
      this.updbill = data.data;
    })
  }
  buy1(fr: any) {
    this.actualpay = fr
    this.bill.amount = fr
    this.bill.startDate = this.cday;
    this.bill.endDate = this.premium;
    this.bill.status = 'Active'
  }
  buy2(fr: any) {
    this.actualpay = fr
    this.bill.amount = fr
    this.bill.startDate = this.cday;
    this.bill.endDate = this.gold;
    this.bill.status = 'Active'
  }
  offer() {
    this.discount = this.bill.offer;
    this.percentage();
  }
  percentage() {
    this.y = this.actualpay - ((this.discount * this.actualpay) / 100)
    this.z = this.y + (((this.GST + this.CGST) * this.actualpay) / 100)
    this.x = this.z;
  }
  ngOnInit() {
    this.getScAdmin()
  }
}
