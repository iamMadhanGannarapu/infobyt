
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Role } from "../../modal/User/Role";
import { UserService } from "../../service/user.service";
import { Subject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-role-master',
  templateUrl: './role-master.component.html',
  styleUrls: ['./role-master.component.scss']
})
export class RoleMasterComponent implements OnInit, OnDestroy {
  displayedColumns = ['Id', 'name', 'status', 'edit', 'Excel']
  dataSource;
  dtOptions: DataTables.Settings = {};
  rm: Role
  dtTrigger: Subject<any> = new Subject();
  roleList: any
  public origin: any
  constructor(private toastr: ToastrService, private us: UserService) {
    this.rm = new Role()
  }
  @ViewChild("addRoleForm") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  addRole() {
    this.us.saveRole(this.rm).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.form.reset();
      }
      else {
        this.toastr.error(data.message)
      }
      this.getAllRoleDetails();
    });
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
  editRole(id: number) {
    this.us.getRolesById(id).subscribe((data) => {
      this.rm.Id = data.data[0].id;
      this.rm.name = data.data[0].rolename;
      this.rm.status = data.data[0].status;
    })
  }
  updateRole() {
    this.us.updateRole(this.rm).subscribe(data => {
      if (data.result)
        this.toastr.success(data.message)
      else
        this.toastr.error(data.message)
      this.ngOnInit();
    });
  }
  onChange(status, id) {
    this.us.getRolesById(id).subscribe((data) => {
      this.rm.Id = data.data[0].id;
      this.rm.name = data.data[0].rolename;
      this.rm.status = status.checked;


      this.us.updateRole(this.rm).subscribe((data) => {
        if (data.result && data.data != 'NDF')
          this.toastr.success(data.message);
        else
          this.toastr.error(data.message)
        this.ngOnInit()
      })
    })
  }
  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.getAllRoleDetails();
    this.getAllRoles();
  }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  getAllRoleDetails() {
    this.us.getAllRoles().subscribe((data) => {
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
    }, (err) => {
      this.toastr.error(err.error.message)
    });
  }
  getexcel() {
    this.us.exportAsExcelFile(this.dataSource.data, 'sample');
  }
  getAllRoles() {
    this.us.getAllRoles().subscribe((data) => {
      this.roleList = data.data;

      if (data.data != "NDF") {
        for (let i = 0; i < this.roleList.length; i++) {
          if (this.roleList[i].status == false) {
            this.roleList[i].status = false
          } else {
            this.roleList[i].status = true
          }
        }
        this.dataSource = new MatTableDataSource(data.data)

        this.dataSource.sort = this.sort
        this.dataSource.paginator = this.paginator
        for (let j = 0; j < this.roleList.length; j++) {
          this.origin = { lat: parseFloat(data.data[0].latitude), lng: parseFloat(data.data[0].longitude) }
        }
      }
    });
  }
}


