import { Component, OnInit, ViewChild } from '@angular/core';
import { Salaryservice } from 'src/app/service/salary.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
  displayedColumns = ['branch', 'StaffName', 'pf']
  dataSource;
  ESISource;
  TdsSource;
  TdsColumns = ['branch', 'StaffName', 'TDS']
  EsiColumns = ['branch', 'StaffName', 'esi', 'Excel']
  PTSource;
  displayedPtColumns = ['branch', 'StaffName', 'PT']
  constructor(private ss: Salaryservice) { }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  getAllPfReport() {
    this.ss.getAllPfReports().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      }
    })
  }
  getexcel() {
    this.ss.exportAsExcelFile(this.dataSource.data, 'Reports');
  }
  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getAllESIReport() {
    this.ss.getAllEsiReports().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.ESISource = new MatTableDataSource(data.data)
      this.ESISource.sort = this.sort
      this.ESISource.paginator = this.paginator
      }
    })
  }
  getAllPTReport() {
    this.ss.getAllPtReports().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.PTSource = new MatTableDataSource(data.data)
      this.PTSource.sort = this.sort
      this.PTSource.paginator = this.paginator
      }
    })
  }
  ngOnInit() {
    $(document).ready(function(){
      $("#datahide1").hide();
      $("#showdata1").click(function(){
         $("#ViewPfReports").show()
         $("#showdata1").hide()
         $("#datahide1").show();
      });
      $("#datahide1").click(function(){
        $("#ViewPfReports").hide()
        $("#showdata1").show()
        $("#datahide1").hide();
     });

     
  });
  $(document).ready(function(){
    $("#datahide2").hide();
    $("#showdata2").click(function(){
       $("#ViewEsiReports").show()
       $("#showdata2").hide()
       $("#datahide2").show();
    });
    $("#datahide2").click(function(){
      $("#ViewEsiReports").hide()
      $("#showdata2").show()
      $("#datahide2").hide();
   });

   
});
$(document).ready(function(){
  $("#datahide3").hide();
  $("#showdata3").click(function(){
     $("#ViewPTReports").show()
     $("#showdata3").hide()
     $("#datahide3").show();
  });
  $("#datahide3").click(function(){
    $("#ViewPTReports").hide()
    $("#showdata3").show()
    $("#datahide3").hide();
 });

 
});
$(document).ready(function(){
  $("#datahide4").hide();
  $("#showdata3").click(function(){
     $("#ViewTdsReports").show()
     $("#showdata3").hide()
     $("#datahide4").show();
  });
  $("#datahide4").click(function(){
    $("#ViewTdsReports").hide()
    $("#showdata3").show()
    $("#datahide4").hide();
 });

 
});

    this.getAllPfReport()
    this.getAllESIReport()
    this.getAllPTReport()
  }
  getAllTdsReport() {
    this.ss.getAllTdsReports().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.TdsSource = new MatTableDataSource(data.data)
      this.TdsSource.sort = this.sort
      this.TdsSource.paginator = this.paginator
      }
    })
  }
}
