import { Component, OnInit, ViewChild } from '@angular/core';
import { Salaryservice } from '../../service/salary.service';
import { PayRoll } from 'src/app/modal/Salary/PayRoll';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../service/user.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { OrganizationService } from 'src/app/service/organization.service';
import { DepartmentPolicies } from '../../modal/Salary/DepartmentPolicies';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-payroll',
  templateUrl: './payroll.component.html',
  styleUrls: ['./payroll.component.scss']
})
export class PayrollComponent implements OnInit,CanActivateChild  {
  payRollAddObj: PayRoll
  deptPolicyAddObj: DepartmentPolicies
  displayedColumns = ['startrange', 'endrange', 'name', 'amount']
  dataSource;
  displayedColumn = ['stfname', 'status', 'Excel']
  dataSources;
  departmentPolicies;
  departmentPoliciesColumn = ['department', 'staff', 'type', 'allowance', 'amount']
  departmentList: any;
  staffList: any
  extraAllowancesList: string[] = [
    "Special Allowance", "Medical Allowance", "Meal Coupons Allowance",
    "Incentives", "Child Education Allowance", "Travel Allowance",
    "Telephone Rent Allowance", "School Transport Allowance",
    "Over Time", "Shift Allowance", "Statutory Bonus",
    "Arrears", "Sickness/Maternity"]
  extraDeductionsList: string[] = [
    'Tax Deduction At Sources/Income Tax',
    'Employee State Insurance',
    'Canteen Deduction/Meal Coupons Issued',
    'Salary Advance',
    'Retirement Insurance', 'Other Deductions']
  allList: any
  checkboxStatus: boolean = true;
  write: any;
  read: any;
  constructor(private router:Router,private ls: LoginService, private ss: Salaryservice, private toastr: ToastrService, private us: UserService,
    private ar: ActivatedRoute, private route: Router, private sc: OrganizationService) {
    this.payRollAddObj = new PayRoll()
    this.deptPolicyAddObj = new DepartmentPolicies()
  }
  @ViewChild("salaryhike") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatPaginator) paginator1: MatPaginator;
  addPayroll() {
    this.ss.savePayroll(this.payRollAddObj).subscribe((data) => {
      if (data.message == 'duplicate') {
        this.toastr.error('PayRoll Values Already Existed');
      }
      if (data.message == 'payroll added') {
        this.toastr.success('Successfully Added payroll  ');
        this.ngOnInit()
      }
    })
  }
  applyFilter(filterValue: string) {
    
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getAllPayRoll() {
    this.ss.getAllPayrolls().subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.dataSource = new MatTableDataSource(data.data)
        this.dataSource.sort = this.sort
        this.dataSource.paginator = this.paginator
      }
    });
  }
  getexcel() {
    this.ss.exportAsExcelFile(this.dataSource, 'Payroll');
  }
  getDepartments() {
    this.sc.getAllDepartments().subscribe((data) => {
      this.departmentList = data.data
    })
  }
  getStaff() {
    this.ss.getAllStaffByDepartment(this.deptPolicyAddObj.departmentId).subscribe((data) => {
      this.dataSources = new MatTableDataSource(data.data)
      this.dataSources.sort = this.sort
      this.dataSources.paginator = this.paginator
    })
  }
  ListOfPolicies(type: any) {
    if (type == "Allowance") {
      this.deptPolicyAddObj.type = "Allowance"
      this.allList = this.extraAllowancesList
    }
    else {
      this.deptPolicyAddObj.type = "Deduction"
      this.allList = this.extraDeductionsList
    }
  }
  addBulkStaffPolicies() {
    this.ss.saveDepartmentAllowences(this.deptPolicyAddObj, this.dataSources.data).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message)
      }
      else
        this.toastr.error(data.message)
    })
  }
  getAllDepartmentPolicies() {
    this.ss.getAllDepartmentPolicies().subscribe((data) => {
      this.departmentPolicies = new MatTableDataSource(data.data)
      this.departmentPolicies.sort = this.sort
      this.departmentPolicies.paginator1 = this.paginator1
    });
  }
  getPermissions() {
    this.ls.checkRightPermissions('payroll').subscribe((data) => {
      this.read =  data.data[0].read;
      this.write =  data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }

  ngOnInit() {
    $(document).ready(function(){
      $("#datahide1").hide();
      $("#showdata1").click(function(){
         $("#viewpayroll").show()
         $("#showdata1").hide()
         $("#datahide1").show();
      });
      $("#datahide1").click(function(){
        $("#viewpayroll").hide()
        $("#showdata1").show()
        $("#datahide1").hide();
     });
  });

  $(document).ready(function(){
    $("#datahide2").hide();
    $("#showdata2").click(function(){
       $("#viewdepartmentpolicies").show()
       $("#showdata2").hide()
       $("#datahide2").show();
    });
    $("#datahide2").click(function(){
      $("#viewdepartmentpolicies").hide()
      $("#showdata2").show()
      $("#datahide2").hide();
   });
});
    this.getPermissions();
    this.getAllPayRoll();
    this.getDepartments();
    this.getAllDepartmentPolicies()
  }
}
