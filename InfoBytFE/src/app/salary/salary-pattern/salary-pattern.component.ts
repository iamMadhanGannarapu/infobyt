import { Component, OnInit, ViewChild, DoCheck } from '@angular/core';
import { Salaryservice } from '../../service/salary.service';
import { UserService } from '../../service/user.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
class Allowance {
  public AlloanceName: string;
  public AllowancePercentage: number;
  constructor() {
  }
}
class Deductions {
  public DeductionName: string;
  public DeductionPercentage: number;
  constructor() {
  }
}
class Leave {
  public type: string;
  public numOfDays: any;
  constructor(){
  }
}
@Component({
  selector: 'app-salary-pattern',
  templateUrl: './salary-pattern.component.html',
  styleUrls: ['./salary-pattern.component.scss']
})
export class SalaryPatternComponent implements OnInit,DoCheck {
  displayedColumns = ['AllowanceType', 'Percentage','Excel'];
  typeColumns = ['LeaveType', 'No.of.Days'];
typeSource;
  dataSource;
  allowancesList: string[] = ["Dearest Allowances", "Conveyance Allowance",
    "Medical Allowance", "Variable Pay"]
  deductionsList: string[] = [
    'Tax Deduction At Sources/Income Tax',
    'Education Cess',
    'Loans',
    'Transport Deduction',
    'Group Insurance/LIC',
    'Other Deductions']
    allowancesLocalList: Allowance[] = [];
    deductionsLocalList: Deductions[] = [];
    count: number = 0;
    counts: number = 0;
    Basic:any='Basic'
    HRA:any='HRA'
    HraValue:any
    PF:any='PF'
    PT:any='PT'
    showAdd:boolean=true
    show1:boolean=true;
    Others:any='Other Allowance'
    PfValue:any
    remain:any=100;
    OValue1:any=0;
    ESI:any='ESI'
    EsiValue:any=1.75
  AllowanceObj: Allowance
  DeductionObj: Deductions
  leaveMasterLocalList: Leave[]=[];
  typesList: any[] = []
  LeaveObj: Leave;
  patternList: any;
  allowanceI: any;
  allowanceLength: any;
  patternId: any
  msg: any;
  OValue:any=0
  remainv:any
   constructor(private toastr: ToastrService, private ss: Salaryservice, private us: UserService) {
  }
  @ViewChild("salarypattern") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  ngOnInit() {
    this.getPattern();
    this.viewLeaveMaster();
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
       // this.rolename=data.data[0].rolename;
          if (data.data[0].rolename == "BranchAdmin") {
          }
      })
    }
    this.patternId=0;
  }
  toster(key:any){
    this.ss.getexitsname(key.value).subscribe((data)=>{
      this.msg=data.msg
    })
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getPattern() {
    this.ss.getSalaryPattern().subscribe((data) => {
      if(data.data!='NDF')
      this.patternList = data.data
    });
  }
  reset(){
    this.allowancesLocalList=[]
    this.showAdd=true;
    this.deductionsLocalList=[]
    this.remain=100
    this.count=0
    this.counts=0
  }
 ngDoCheck(){
    if(this.allowancesLocalList.length>0)
    this.showAdd=false
  }
  getleaves() {
    this.ss.getLeavePatternDetailsById(this.patternId).subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.typeSource = new MatTableDataSource(data.data)
      this.typeSource.sort = this.sort
      this.typeSource.paginator = this.paginator
      }
    });
  }
 deleteLeaveType(leaves: any) {
  for (let i = 0; i < this.leaveMasterLocalList.length; i++) {
    if (this.leaveMasterLocalList[i].type == leaves) {
      this.counts = this.counts - this.leaveMasterLocalList[i].numOfDays
      this.leaveMasterLocalList.splice(i, 1)
      i--;
    }
  }
}
setLeavePatternDetails(leaveType: any, days: any) {
  this.counts = this.counts + parseInt(days.value)
  if (this.counts <= 30) {
    if (JSON.stringify(this.leaveMasterLocalList).indexOf(leaveType.value) == -1) {
      this.LeaveObj = new Leave();
      this.LeaveObj.type = leaveType.value;
      this.LeaveObj.numOfDays = parseInt(days.value)
      this.leaveMasterLocalList.push(this.LeaveObj)
    }
    else {
      this.counts = this.counts - parseInt(days.value)
      this.toastr.error("Leave type already exists.")
    }
  }
  else {
    this.counts = this.counts - parseInt(days.value)
    this.toastr.error("days exceeding 31")
  }
}
viewLeaveMaster(){
  this.ss.getLeavemaster().subscribe((data)=>{
    if(data.data!='NDF')
    this.typesList= data.data
  })
}
  getallowances() {
    this.ss.getSalaryPatternDetailsById(this.patternId).subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      }
    });
      this.getleaves();
  }
  getexcel(){
    this.ss.exportAsExcelFile( this.dataSource.data, 'Salarypattern');
}
  setAllowancePatternDetails(allowanceName: any, percentage: any) {
    this.count = this.count + parseFloat(percentage.value)
    if (this.count <= 100) {
      if (JSON.stringify(this.allowancesLocalList).indexOf(allowanceName.value) == -1) {
        this.remain=this.remain-parseFloat(percentage.value);
        this.AllowanceObj = new Allowance();
        this.AllowanceObj.AlloanceName = allowanceName.value;
        this.AllowanceObj.AllowancePercentage = parseFloat(percentage.value)
        this.allowancesLocalList.push(this.AllowanceObj)
      }
      else {
        this.count = this.count - parseFloat(percentage.value)
        this.toastr.error("Allowance already exists.")
      }
    }
    else {
      this.count = this.count - parseFloat(percentage.value)
      this.toastr.error("Percentage exceeding 100%")
    }
  }
  setPrimaryAllowances(a1:any,p1:any,a2:any,p2:any,d2:any,dp2:any,d1:any,dp1:any)
  {
    this.setAllowancePatternDetails(a1,p1);
    this.setAllowancePatternDetails(a2,p2);
    this.setDeductionsPatternDetails(d2,dp2);
    this.setDeductionsPatternDetails(d1,dp1);
  }
  deleteAllowance(allowance: any) {
    for (let i = 0; i < this.allowancesLocalList.length; i++) {
      if (this.allowancesLocalList[i].AlloanceName == allowance) {
        this.count = this.count - this.allowancesLocalList[i].AllowancePercentage
        this.remain=this.remain+this.allowancesLocalList[i].AllowancePercentage;
        this.allowancesLocalList.splice(i, 1)
        i--;
      }
    }
  }
  deleteDeduction(deduction: any) {
    for (let i = 0; i < this.deductionsLocalList.length; i++) {
      if (this.deductionsLocalList[i].DeductionName == deduction) {
        this.count = this.count - this.deductionsLocalList[i].DeductionPercentage
        this.remain=this.remain+this.deductionsLocalList[i].DeductionPercentage;
        this.deductionsLocalList.splice(i, 1)
        i--;
      }
    }
  }
  setDeductionsPatternDetails(deductionName: any, percentage: any) {
    this.count = this.count + parseFloat(percentage.value)
    if (this.count <= 100) {
      if (JSON.stringify(this.deductionsLocalList).indexOf(deductionName.value) == -1) {
        this.remain=this.remain-parseFloat(percentage.value)
        this.DeductionObj = new Deductions();
        this.DeductionObj.DeductionName = deductionName.value;
        this.DeductionObj.DeductionPercentage = parseFloat(percentage.value)
        this.deductionsLocalList.push(this.DeductionObj)
      }
      else {
        this.count = this.count - parseFloat(percentage.value)
        this.toastr.error("Deduction already exists.")
      }
    }
    else {
      this.count = this.count - parseFloat(percentage.value)
      this.toastr.error("Percentage exceeding 100%")
    }
  }
  addSalaryPattern(structureName: any, frm: any) {
    this.ss.addAllowancesSalaryPattern(structureName.value, this.allowancesLocalList, this.deductionsLocalList, this.leaveMasterLocalList).subscribe((data) => {
      this.getPattern();
      if (data.result) {
        this.toastr.success(data.message)
        this.form.reset();
      }
      else
      {
        this.toastr.error(data.message)
      }
      this.allowancesLocalList = [];
      this.deductionsLocalList = [];
      this.leaveMasterLocalList = [];
      this.count=0;
      this.counts=0;
      this.getallowances();
    })
  }
  calculateAll(percentage:any){
    this.HraValue=(percentage.value)/2;
    this.PfValue=(percentage.value)*0.12;
    this.remain=100
  }
}