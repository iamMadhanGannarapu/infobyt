import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SalaryComponent } from './salary/salary.component';
import { SalaryHikeComponent } from './salary-hike/salary-hike.component';
import { SalaryPaymentComponent } from './salary-payment/salary-payment.component';
import { RouterModule, Routes } from "@angular/router";
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { BsDatepickerModule } from 'ngx-bootstrap';
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { PayslipComponent } from './payslip/payslip.component';
import { SalaryPatternComponent } from './salary-pattern/salary-pattern.component';
//import { BrowserModule } from '@angular/platform-browser'
import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';
//import { HttpClientModule } from '@angular/common/http';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatStepperModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import { ReportsComponent } from './reports/reports.component';
import { PayrollComponent } from './payroll/payroll.component';
import { TdsComponent } from './tds/tds.component';
import { SalaryRoutingModule } from './salary-routing.module';
import {CalendarModule} from 'primeng/calendar';
@NgModule({
  imports: [
    CalendarModule,
    SalaryRoutingModule,
    MatTableModule, MatTableModule, 
    MatSlideToggleModule,
    CdkTableModule,
    CdkTreeModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule, MatFormFieldModule,
    CommonModule, 
    //BrowserAnimationsModule, 
    ToastrModule.forRoot(),
    FormsModule, DataTablesModule, BsDatepickerModule.forRoot()
  ], exports: [
    RouterModule
  ], providers: [],
  declarations: [ReportsComponent, SalaryComponent, SalaryHikeComponent, PayrollComponent, SalaryPaymentComponent, TdsComponent, PayslipComponent, SalaryPatternComponent]
})
export class SalaryModule { }
