
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Salaryservice } from '../../service/salary.service';
//import * as jsPDF from 'jspdf';
import { PaySlip } from '../../modal/Salary/PaySlip';
import { LoginService } from '../../service/login.service';
import { UserService } from '../../service/user.service';
import { ToastrService } from 'ngx-toastr';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import * as config from '../../service/service_init.json';
class ExtraAllowance {
  userId: any
  allowanceName: any
  amount: any
  month: any
  type: any;
  constructor() {
  }
}
@Component({
  selector: 'app-payslip',
  templateUrl: './payslip.component.html',
  styleUrls: ['./payslip.component.scss']
})
export class PayslipComponent implements OnInit {
  dataSource;
  displayedColumns = ['ID', 'Name', 'Presentdays', 'Absentdays', 'WeekOffs', 'Salary/Month', 'CurrentSalary', 'SendPaySlip']
  rolename: any;
  public downloadPaySlip(ps: any) {
  }
  allList: any;
  extraAllowance: ExtraAllowance
  showMyPaySlip: boolean = false;
  showAllPaySlip: boolean = false;
  allowancesList: any
  payDetailsList: any = [];
  paySlipOfUserList: any = [];
  userPaySlip: any = [];
  paySlip: PaySlip
  path: any
  userId: any
  allowanceUserId: any
  allowanceType: any;
  userProfile: any
  month: any
  month1: any
  date1: any;
  date: any;
  usr:any;
  payslips:any;
  myUrl: string = (<any>config).api
  extraAllowancesList: string[] = [
    "Special Allowance", "Medical Allowance", "Meal Coupons Allowance",
    "Incentives", "Child Education Allowance", "Travel Allowance",
    "Telephone Rent Allowance", "School Transport Allowance",
    "Over Time", "Shift Allowance", "Statutory Bonus",
    "Arrears", "Sickness/Maternity"]
  extraDeductionsList: string[] = [
    'Tax Deduction At Sources/Income Tax',
    'Employee State Insurance',
    'Canteen Deduction/Meal Coupons Issued',
    'Salary Advance',
    'Retirement Insurance', 'Other Deductions']
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('payslip') content: ElementRef;
  
  monthsList: any = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  constructor(private toastr: ToastrService, private salaryService: Salaryservice, private loginServ: LoginService, private userService: UserService) {
    this.paySlip = new PaySlip();
    this.extraAllowance = new ExtraAllowance();
    this.usr= this.userProfile.userid
  }


  viewPslips() {
    this.salaryService.getPaySlips(this.usr).subscribe((data) => {
      this.payslips = data.data
     })
  }
  viewSlip(){
    this.viewPslips();
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
  ngOnInit() {
    if (sessionStorage.getItem('user') != null) {
      this.userService.getUserbySession().subscribe((data) => {
        this.userProfile = data.data[0];
        this.rolename = this.userProfile.rolename;
      })
    }
  }
  generateGrossSalary(month: any) {
    this.salaryService.generateGrossSalary(this.payDetailsList, this.month).subscribe((data) => {
      if(data.result)
      {
        this.toastr.success(data.message)
      this.getAllPaySlips();
    }
    })
  }
  getAllPaySlips() {
    ;
    this.date = new Date(this.date)
    if (parseInt(this.date.getMonth() + 1) < 10)
      this.month = this.date.getFullYear() + '-0' + parseInt(this.date.getMonth() + 1)
    else
      this.month = this.date.getFullYear() + '-' + parseInt(this.date.getMonth() + 1)
    //
    this.salaryService.getattendanceDetailsOfUser(this.month).subscribe((data) => {
      this.payDetailsList = data.data;
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      this.salaryService.addPayDetails(this.payDetailsList, this.month).subscribe((data) => {
      })
    })
    this.showAllPaySlip = true;
    this.showMyPaySlip = false;
  }
  getPaySlip(month: any) {
    this.date1 = new Date(this.date1)
    if (parseInt(this.date1.getMonth() + 1) < 10)
      this.month1 = this.date1.getFullYear() + '-0' + parseInt(this.date1.getMonth() + 1)
    else
      this.month1 = this.date1.getFullYear() + '-' + parseInt(this.date1.getMonth() + 1)
    this.salaryService.getattendanceDetailsOfUser(this.month1).subscribe((data) => {
      this.payDetailsList = data.data;
   //   this.loginServ.getUserBySessionId().subscribe((data) => {
        this.userId = this.userProfile.userid;
        this.payDetailsList.forEach(element => {
          if (element.userid == this.userId) {
            this.userPaySlip = element;
            this.toastr.success("Payslip Has Been Generated Successfully")
          }
        });
        this.salaryService.getPaySlipOfUser(this.userId).subscribe((data) => {
          this.paySlipOfUserList = data.data;
          this.showMyPaySlip = true;
        })
        this.salaryService.getSalaryAllowancesOfUser(this.userId).subscribe((data) => {
          this.allowancesList = data.data;
        })
        // this.salaryService.generatePaySlip(this.paySlipOfUserList, this.userPaySlip, this.allowancesList).subscribe((data) => {
        // })
    //  })
    })
  }
  sendPaySlips(month: any, userId: any) {
    this.paySlip.userId = userId;
    this.paySlip.month = this.month;
    this.salaryService.sendPaySlip(this.paySlip).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message)
        this.getAllPaySlips();
      }
      else {
        this.getAllPaySlips();
      }
    })
  }
  saveUserId(userId: any, type: any) {
    this.extraAllowance.userId = userId;
    this.extraAllowance.type = type;
    if (type == "Allowance")
      this.allList = this.extraAllowancesList
    else
      this.allList = this.extraDeductionsList
  }
  addExtraAllowance() {
    this.extraAllowance.month = this.month
    this.salaryService.addExtraAllowances(this.extraAllowance).subscribe((data) => {
    })
  }
  updateExtraAllowance(userId: any) {
    //
    this.salaryService.updateExtraAllowances(userId, this.month).subscribe((data) => {
      if (data.result)
        this.getAllPaySlips()
      else
        this.toastr.success(data.message + ' Error:' + data.error)
    })
  }
}
