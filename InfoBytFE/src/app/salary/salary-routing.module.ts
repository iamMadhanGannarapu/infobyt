import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalaryComponent } from './salary/salary.component';
import { SalaryHikeComponent } from './salary-hike/salary-hike.component';
import { SalaryPaymentComponent } from './salary-payment/salary-payment.component';
import { PayslipComponent } from './payslip/payslip.component';
import { SalaryPatternComponent } from './salary-pattern/salary-pattern.component';
import { RouteGuardGuard, CanDeactivateGuard } from '../service/route-guard.guard';
import { PayrollComponent } from './payroll/payroll.component';
import { TdsComponent } from './tds/tds.component';
import { ReportsComponent } from './reports/reports.component';
const routes:Routes=[
{path:'Salary',component:SalaryComponent,canDeactivate:[CanDeactivateGuard],canActivate:[RouteGuardGuard],canActivateChild:[SalaryComponent]},
{path:'Salary-hike',component:SalaryHikeComponent,canDeactivate:[CanDeactivateGuard],canActivate:[RouteGuardGuard],canActivateChild:[SalaryHikeComponent]},
{path:'Salary-payment',component:SalaryPaymentComponent,canDeactivate:[CanDeactivateGuard],canActivate:[RouteGuardGuard],canActivateChild:[SalaryPaymentComponent]},
{path:'Salary-pattern',component:SalaryPatternComponent,canDeactivate:[CanDeactivateGuard],canActivate:[RouteGuardGuard]},
{path:'Payroll',component:PayrollComponent,canDeactivate:[CanDeactivateGuard],canActivate:[RouteGuardGuard],canActivateChild:[PayrollComponent]},
{path:'Tds',component:TdsComponent,canActivate:[RouteGuardGuard],canActivateChild:[TdsComponent],canDeactivate:[CanDeactivateGuard]},
{path:'Reports',component:ReportsComponent,canActivate:[RouteGuardGuard]},
{path:'Payslip',component:PayslipComponent,canDeactivate:[CanDeactivateGuard],canActivate:[RouteGuardGuard]}
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers:[RouteGuardGuard , CanDeactivateGuard,SalaryPaymentComponent,PayrollComponent,SalaryComponent,SalaryHikeComponent,TdsComponent,ReportsComponent]
})
export class SalaryRoutingModule { }
