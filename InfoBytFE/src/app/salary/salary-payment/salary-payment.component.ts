import { Component, OnInit, ViewChild } from '@angular/core';
import { SalaryPaymentsInfo } from '../../modal/Salary/SalaryPayment'
import { Salaryservice } from "../../service/salary.service";
import { BatchService } from '../../service/batch.service';
import { FeeService } from '../../service/fee.service';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../service/user.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';

@Component({
  selector: 'app-salary-payment',
  templateUrl: './salary-payment.component.html',
  styleUrls: ['./salary-payment.component.scss']
})
export class SalaryPaymentComponent implements OnInit,CanActivateChild {
  displayedColumns = ['StaffName', 'Allowences', 'Deductions', 'Bonus', 'Payable', 'PaidAmount', 'Paydate','Excel']
  dataSource;
  spm: SalaryPaymentsInfo
  staffList: any
  salarymasterList: any
  actualSalary: any;
  maxDate = new Date();
  minDate = new Date();
  show1:boolean=true;
  viewsalarypayment:boolean;
  date = new Date();
  bsConfig: Partial<BsDatepickerConfig>;
  colorTheme = 'theme-dark-blue';
  rolename: any;
  write: any;
  read: any;
  constructor(private router:Router,private ls:LoginService,private toastr: ToastrService, private us: UserService, private ss: Salaryservice, private bs: BatchService) {
    this.spm = new SalaryPaymentsInfo()
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
  }
  @ViewChild("addSalarypayment") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  addSalaryPayment() {
    this.ss.saveSalaryPayment(this.spm).subscribe((data) => {
      this.getAllSalaryPayment();
      if (data.result) {
        this.toastr.success(data.message);
        this.form.reset();
      }
      else
      this.toastr.error(data.message)
    });
  }
  applyFilter(filterValue: string) {
   
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  salaryPaymentDetails(Id: number) {
    this.ss.getSalaryPaymentById(Id).subscribe((data) => {
    })
  }
  getAllStaffs(){
    this.bs.getAllStaff().subscribe((res) => {
      this.staffList = res.data;
    });
  }

  getIds() {
   this.getAllStaffs();
    this.ss.getAllSalary().subscribe((data) => {
      this.salarymasterList = data.data;
    })
  }
  getPaymentByStaff(month: any) {
    this.ss.getSalaryPaymentByStaffId(this.spm.staffId, month.value).subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.spm.paidAmount = this.salarymasterList[0].grosssalary;
      this.spm.payDate = Date();
      this.actualSalary = this.salarymasterList[0].salary
      this.spm.payable = this.salarymasterList[0].salaryid;
      this.spm.allowances = this.salarymasterList[0].allowances;
      this.spm.deductions = this.salarymasterList[0].deductions;
      if (this.salarymasterList[0].bonus == null)
        this.spm.bonus = 0;
      else
        this.spm.bonus = this.salarymasterList[0].bonus;
      this.spm.payAllowanceDeductionDetails = this.salarymasterList[0].patternname;
      }
    })
  }
  getAllSalaryPayment() {
    this.ss.getAllSalaryPayment().subscribe((data) => {
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
    });
  }
  getexcel(){
    this.ss.exportAsExcelFile(this.dataSource.data, 'Salarypayment');
}
getPermissions() {
  this.ls.checkRightPermissions('salary-payment').subscribe((data)=>{
    this.read=data.data[0].read;
    this.write=data.data[0].write;
    this.canActivateChild(this.read)

  })
}


canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }
  ngOnInit() {
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename=data.data[0].rolename;
      })
    }
    this.getPermissions();
    this.getAllSalaryPayment();
    this.spm.staffId=0;
  }
  SalaryPaymentComponent()
    {
      this.show1=!this.show1;
      this.viewsalarypayment=!this.viewsalarypayment
    }
}