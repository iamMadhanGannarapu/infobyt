import { Component, OnInit, ViewChild } from '@angular/core';
import { Salary } from '../../modal/Salary/Salary';
import { Salaryservice } from '../../service/salary.service';
import { ActivatedRoute } from '@angular/router';
import { BatchService } from '../../service/batch.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../service/user.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-salary',
  templateUrl: './salary.component.html',
  styleUrls: ['./salary.component.scss']
})
export class SalaryComponent implements OnInit,CanActivateChild  {
displayedColumns = ['StaffName', 'Salary', 'AllowancesPattern','branch','Excel']
  dataSource;
  sl: Salary
  salarytransactionList:any;
  staffDetails: any;
  rolename: any;

  read: any;
  write: any;
  constructor(private router:Router,private ls:LoginService,private toastr: ToastrService, private ss: Salaryservice, private us: UserService, private bs: BatchService, private ar: ActivatedRoute, private route: Router) {
    this.sl = new Salary()
  }
  @ViewChild("salary") form: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  addSalary() {
    this.ss.saveSalary(this.sl).subscribe((data) => {
      this.getAll();
      if (data.result) {
        this.toastr.success(data.message);
        this.form.reset();
      }
      else
        this.toastr.error(data.message)
    })
  }
  applyFilter(filterValue: string) {
    
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getIds() {
    this.bs.getAllStaff().subscribe((data) => {
      this.staffDetails = data.data;
    })
  }
  getAllsalarytransactions() {
    this.ss.getsalarytransactionsbyUserId().subscribe((data1) => {
      this.salarytransactionList = data1.data
    });
  }
  ngOnInit() {
    $(document).ready(function(){
      $("#datahide1").hide();
      $("#showdata1").click(function(){
         $("#viewallSal").show()
         $("#showdata1").hide()
         $("#datahide1").show();
      });
      $("#datahide1").click(function(){
        $("#viewallSal").hide()
        $("#showdata1").show()
        $("#datahide1").hide();
     });
  });

  $(document).ready(function(){
    $("#datahide2").hide();
    $("#showdata2").click(function(){
       $("#ViewSal").show()
       $("#showdata2").hide()
       $("#datahide2").show();
    });
    $("#datahide2").click(function(){
      $("#ViewSal").hide()
      $("#showdata2").show()
      $("#datahide2").hide();
   });
});
    this.getAllsalarytransactions();
    this.getAll();
    this.getPermissions();
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
          this.rolename=data.data[0].rolename;
      })
    }
    let j = this.ar.snapshot.params.id;
    this.ss.getSalaryById(j).subscribe((data) => {
    })
  }
  getPermissions() {
    this.ls.checkRightPermissions('salary').subscribe((data)=>{
      
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }
  getAll() {
    this.ss.getAllSalary().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      }
    });
  }
  getexcel(){
    this.ss.exportAsExcelFile(this.dataSource.data, 'Salary');
}
}
