import { Component, OnInit, ViewChild } from '@angular/core';
import { Tds } from 'src/app/modal/Salary/Tds';
import { Salaryservice } from '../../service/salary.service';
import { ToastrService } from 'ngx-toastr';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-tds',
  templateUrl: './tds.component.html',
  styleUrls: ['./tds.component.scss']
})
export class TdsComponent implements OnInit,CanActivateChild {
  tdsAddObj: Tds
  displayedColumns1 = ['tdsstartrange', 'tdsendrange', 'tdsname', 'tdsamount', 'Excel']
  dataSource1;
  show1:boolean=true;
  read: any;
  write: any;
  constructor(private router:Router,private ls:LoginService,private ss: Salaryservice, private toastr: ToastrService) {
    this.tdsAddObj = new Tds()
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("salaryhike") form: any;
  addtds() {
    this.ss.saveTds(this.tdsAddObj).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.ngOnInit()
      }
      else
      {
        this.toastr.error(data.message)
      }
    })
  }
  applyFilter(filterValue: string) {
    this.dataSource1.filter = filterValue.trim().toLowerCase();
  }
  getAllTdsPolicies() {
    this.ss.getAllTds().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.dataSource1 = new MatTableDataSource(data.data)
      this.dataSource1.sort = this.sort
      this.dataSource1.paginator = this.paginator
      }
    });
  }
  getexcel() {
    this.ss.exportAsExcelFile(this.dataSource1, 'tds');
  }
  getPermissions() {
    this.ls.checkRightPermissions('tds').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }

  ngOnInit() {
    $(document).ready(function(){
      $("#datahide").hide();
      $("#showdata").click(function(){
         $("#viewtds").show()
         $("#showdata").hide()
         $("#datahide").show();
      });
      $("#datahide").click(function(){
        $("#viewtds").hide()
        $("#showdata").show()
        $("#datahide").hide();
     });
  });
    this.getAllTdsPolicies();
    this.getPermissions();
  }
}
