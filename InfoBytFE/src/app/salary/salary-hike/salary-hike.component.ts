
import { Component, OnInit, ViewChild } from '@angular/core';
import { SalaryHike } from '../../modal/Salary/SalaryHike';
import { Salaryservice } from '../../service/salary.service';
import { BatchService } from '../../service/batch.service';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import {MatSort, MatTableDataSource,MatPaginator} from '@angular/material';
import { UserService } from '../../service/user.service';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-salary-hike',
  templateUrl: './salary-hike.component.html',
  styleUrls: ['./salary-hike.component.scss']
})
export class SalaryHikeComponent implements OnInit,CanActivateChild  {
  dataSource;
  displayedColumns=['StaffName','HikeDate','Increment','mail','Excel']
  salary: SalaryHike
  salaryHike: any[] = [];
  staffdetail: any;
  maxDate = new Date();
  minDate= new Date();
  show1:boolean=true;
  salaryHiketransactionList:any
  colorTheme = 'theme-dark-blue';
  bsConfig: Partial<BsDatepickerConfig>;
  rolename: any;
  read: any;
  write: any;
  constructor(private router:Router,private ls:LoginService,private toastr: ToastrService, private ss: Salaryservice, private us: UserService, private bs: BatchService) {
    this.salary = new SalaryHike
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    this.maxDate.setFullYear(this.maxDate.getFullYear() + 3);
  }
  @ViewChild("salaryhike") form: any;
  @ViewChild(MatSort) sort:MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  addSalaryHike() {
    this.ss.saveSalaryHike(this.salary).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.form.reset();
      }
      else
      this.toastr.error(data.message)
      this.getAllSalaryHike();
    });
  }
  applyFilter(filterValue: string) {

    this.dataSource.filter =filterValue.trim().toLowerCase();
  }
  getAllsalaryHiketransactions() {
    this.ss.getsalaryhiketransactionsbyUserId().subscribe((data1) => {
      if(data1.result && data1.data!='NDF')
      this.salaryHiketransactionList = data1.data
    });
  }
  getAllSalaryHike() {
    this.ss.getAllsalaryhike().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.dataSource=new MatTableDataSource(data.data)
      this.dataSource.sort=this.sort
      this.dataSource.paginator=this.paginator
      }
    });
  }
  getexcel(){
    this.ss.exportAsExcelFile(this.dataSource.data, 'SalaryHike');
}
  // salaryHikeDetails(id: number) {
  //   this.ss.getSalaryHikeById(id).subscribe((data) => {
  //     if(data.result && data.data!='NDF')
  //     {
  //     this.salaryHike = data.data
  //     }
  //   })
  // }
  getIds() {
    this.bs.getAllStaff().subscribe((data) => {
      this.staffdetail = data.data;
    })
  }
  getPermissions() {
    this.ls.checkRightPermissions('salary-hike').subscribe((data)=>{
  
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  
canActivateChild(x:any):boolean{
  if(x==true){
    return true;
  }else{
    this.router.navigate(['404']);
      return false;
  }
    }

  ngOnInit() {
    $(document).ready(function(){
      $("#datahide1").hide();
      $("#showdata1").click(function(){
         $("#viewall").show()
         $("#showdata1").hide()
         $("#datahide1").show();
      });
      $("#datahide1").click(function(){
        $("#viewall").hide()
        $("#showdata1").show()
        $("#datahide1").hide();
     });
  });

  $(document).ready(function(){
    $("#datahide2").hide();
    $("#showdata2").click(function(){
       $("#View").show()
       $("#showdata2").hide()
       $("#datahide2").show();
    });
    $("#datahide2").click(function(){
      $("#View").hide()
      $("#showdata2").show()
      $("#datahide2").hide();
   });
});
    this.getPermissions();
    this.getAllsalaryHiketransactions();
    this.getAllSalaryHike();
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
     
          this.rolename=data.data[0].rolename;
      })
    }
    this.salary.staffId=0;
    this.salary.type='HIKE';
  }
  sendHikeLetter(userId: any,id:any) {
    this.ss.sendHike(userId,id).subscribe((data) => {
    })
  } 
}
