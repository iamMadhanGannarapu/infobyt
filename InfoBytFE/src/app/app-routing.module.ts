import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
const routes: Routes = [
  { path: 'login', loadChildren: "../app/login/login.module#LoginModule" },
  { path: 'organization', loadChildren: "../app/organization/organization.module#OrganizationModule" },
  { path: 'attendance', loadChildren: "../app/attendance/attendance.module#AttendanceModule" },
  { path: 'batch', loadChildren: "../app/batch/batch.module#BatchModule" },
  { path: 'appraisal', loadChildren: "../app/appraisal/appraisal.module#AppraisalModule" },
  { path: 'fee', loadChildren: "../app/fee/fee.module#FeeModule" },
  { path: 'salary', loadChildren: "../app/salary/salary.module#SalaryModule" },
  { path: 'transport', loadChildren: "../app/transport/transport.module#TransportModule" },
  { path: 'user', loadChildren: "../app/user/user.module#UserModule" },
  { path: 'settings', loadChildren: "../app/settings/settings.module#SettingsModule" },
  {path:'career',loadChildren:"../app/career/career.module#CareerModule"},
  {path:'dashboard',loadChildren:"../app/dashboard/dashboard.module#DashboardModule"},
  {path:'services',loadChildren:"../app/services/services.module#ServicesModule"},
  {path: 'links', loadChildren: "../app/links/links.module#LinksModule"}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
