import { Component, OnInit, ViewChild } from '@angular/core';
import { TransportRoute } from '../../modal/Transport/TransportRoute';
import { TransportService } from '../../service/transport.service';
import { OrganizationService } from '../../service/organization.service'
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../service/user.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-transport-route-master',
  templateUrl: './transport-route-master.component.html',
  styleUrls: ['./transport-route-master.component.scss']
})
export class TransportRouteMasterComponent implements OnInit,CanActivateChild {
  dataSource;
  displayedColumns = ['RouteName', 'StartPoint', 'EndPoint', 'branch', 'edit', 'Excel']
  show: boolean = true
  busDetails: any;
  Transportroute: boolean;
  schoolBranch: any;
  transportroute: TransportRoute;
  show1:boolean=true;
  rolename: any;
  
  read: any;
  write: any;
  constructor(private router:Router, private ls: LoginService, private os: OrganizationService, private us: UserService, private toastr: ToastrService, private ts: TransportService) {
    this.transportroute = new TransportRoute()
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("addTransroute") form: any;
  addTransportRoute() {
    this.ts.saveTransportRoute(this.transportroute).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.form.reset();
      }
      else {
        this.toastr.error(data.message)
      }
      this.getAllTransportRoute();
    });
  }
  back() {
    location.reload()
  }
  viewAllBuses() {
    this.ts.getAllBus().subscribe((data) => {
      this.busDetails = data.data
    });
  }
  viewSchoolBranchById() {
    this.os.getBranchById().subscribe((data) => {
      this.schoolBranch = data.data;
    })
  }
  getIds() {
    this.viewAllBuses();
    this.viewSchoolBranchById();
  }
  editTransportRoute(id: number) {
    this.getIds();
    this.ts.getTransportRouteById(id).subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.transportroute.Id = data.data[0].id;
        this.transportroute.name = data.data[0].name;
        this.transportroute.startPoint = data.data[0].startpoint;
        this.transportroute.endPoint = data.data[0].endpoint;
        this.transportroute.busId = data.data[0].busid;
      }
    });
  }
  updateTransportRoute() {
    this.ts.updateTransportRoute(this.transportroute).subscribe(data => {
      if (data.result)
        this.toastr.success(data.message);
      else
        this.toastr.error(data.message)
      this.getAllTransportRoute();
    });
  }
  getAllTransportRoute() {
    this.show = false
    this.ts.getAllTransportRoute().subscribe((data) => {
      if (data.result && data.data != 'NDF') {
      
        this.dataSource = new MatTableDataSource(data.data)
        this.dataSource.sort = this.sort
        this.dataSource.paginator = this.paginator
      }
    });
  }
  getexcel() {
    this.ts.exportAsExcelFile(this.dataSource.data, 'TransportRoute')
  }
  applyFilter(filterValue: string) {
    
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getPermissions() {
    this.ls.checkRightPermissions('transport-route-master').subscribe((data) => {
 
      this.read = data.data.read;
      this.write = data.data.write;
      this.canActivateChild(this.read)
    })
  }
  canActivateChild(x:any):boolean{
    if(x==true){
      return true;
    }else{
      this.router.navigate(['404']);
        return false;
    }
      }

  ngOnInit() {
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
   
        this.rolename = data.data[0].rolename;
      })
    }
    this.getPermissions();
    this.getAllTransportRoute();
    this.transportroute.endPoint = '0';
    this.transportroute.busId = 0;
  }
  transportRoute() {
    this.show1=!this.show1;
    this.Transportroute = !this.Transportroute
  }
}
