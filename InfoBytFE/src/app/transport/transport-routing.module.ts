import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsertransportPaymentComponent } from './user-transport-payment/user-transport-payment.component';
import { UserTransportComponent } from './user-transport/user-transport.component';
import { TransportStopsComponent } from './transport-stops/transport-stops.component';
import { RouteGuardGuard, CanDeactivateGuard } from '../service/route-guard.guard';
import { TransportRouteMasterComponent } from './transport-route-master/transport-route-master.component';
import { BusMasterComponent } from './bus-master/bus-master.component';
import { BusDriverAlocationComponent } from './bus-driver-alocation/bus-driver-alocation.component';
import { TransportFeeMasterComponent } from './transport-fee-master/transport-fee-master.component';
const routes: Routes = [{ path: 'Transport-route-master', component: TransportRouteMasterComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard] },
{ path: 'User-transport-payment', component: UsertransportPaymentComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard] },
{ path: 'Bus-master', component: BusMasterComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard] },
{ path: 'Bus-driver-alocation', component: BusDriverAlocationComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard],canActivateChild:[BusDriverAlocationComponent] },
{ path: 'User-transport', component: UserTransportComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard] },
{ path: 'Transport-stops', component: TransportStopsComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard] },
{ path: 'Transport-fee-master', component: TransportFeeMasterComponent,canDeactivate:[CanDeactivateGuard], canActivate: [RouteGuardGuard] },
]
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers:[RouteGuardGuard,BusDriverAlocationComponent,BusMasterComponent,CanDeactivateGuard
   ,TransportStopsComponent,UsertransportPaymentComponent ,UserTransportComponent,TransportFeeMasterComponent,TransportStopsComponent]
})
export class TransportRoutingModule { }
