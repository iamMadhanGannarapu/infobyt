import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransportStopsComponent } from './transport-stops/transport-stops.component';
import { UserTransportComponent } from './user-transport/user-transport.component';
import { BusDriverAlocationComponent } from './bus-driver-alocation/bus-driver-alocation.component';
import { UsertransportPaymentComponent } from './user-transport-payment/user-transport-payment.component';
import { TransportRouteMasterComponent } from './transport-route-master/transport-route-master.component';
import { BusMasterComponent } from './bus-master/bus-master.component';
import { TransportFeeMasterComponent } from './transport-fee-master/transport-fee-master.component';
import { RouterModule, Routes } from "@angular/router";
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { AgmCoreModule } from '@agm/core'            
import { AgmDirectionModule } from 'agm-direction';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime'; 
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatStepperModule,
  MatProgressBarModule ,
  MatProgressSpinnerModule,
  MatSliderModule ,
  MatSlideToggleModule ,
  MatTooltipModule ,
  MatTreeModule,
} from '@angular/material';
import { TransportRoutingModule } from './transport-routing.module';
@NgModule({
  imports: [
    TransportRoutingModule,
    MatTableModule, MatTableModule,
    MatSlideToggleModule,
    CdkTableModule,
    CdkTreeModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,MatFormFieldModule,
    CommonModule, 
    OwlDateTimeModule, OwlNativeDateTimeModule,
    ToastrModule.forRoot(),
    FormsModule, DataTablesModule, BsDatepickerModule.forRoot(),
    AgmCoreModule.forRoot({ apiKey: '' }),
    AgmDirectionModule,
  ], exports: [
    RouterModule
  ],providers:[],
  declarations: [TransportStopsComponent, UserTransportComponent, BusDriverAlocationComponent, UsertransportPaymentComponent, TransportRouteMasterComponent, BusMasterComponent, TransportFeeMasterComponent]
})
export class TransportModule { }
