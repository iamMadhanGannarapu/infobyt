import { Component, OnInit, ViewChild } from '@angular/core';
import { BusDriverAllocation } from '../../modal/Transport/BusDriverAllocation'
import { TransportService } from '../../service/transport.service';
import { UserService } from '../../service/user.service';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { Router,CanActivateChild } from '@angular/router';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
@Component({
  selector: 'app-bus-driver-alocation',
  templateUrl: './bus-driver-alocation.component.html',
  styleUrls: ['./bus-driver-alocation.component.scss']
})
export class BusDriverAlocationComponent implements OnInit ,CanActivateChild {
  dataSource;
  displayedColumns = ['BusNumber', 'DriverName', 'branch', 'edit', 'Excel']
  displayedColumns1 = ['DriverName', 'BusNumber', 'Address', 'Mobile']
  busDriver: BusDriverAllocation;
  driverDetails: any;
  pipe = new DatePipe('en-US');
  busDetails: any;
  viewdriver: boolean;
  show1:boolean=true;
  maxDate = new Date();
  minDate = new Date();
  colorTheme = 'theme-dark-blue';
  bsConfig: Partial<BsDatepickerConfig>;
  rolename: any;
  dataSource1: MatTableDataSource<{}>;
  read: any;
  write: any;
  constructor(private router:Router,  private ls:LoginService,private toastr: ToastrService, private ts: TransportService, private us: UserService, private route: Router) {
    this.busDriver = new BusDriverAllocation();
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    this.maxDate.setDate(this.maxDate.getDate() + 7);
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("busDriverForm") form: any;
  addDriver() {
    this.ts.saveBusDriver(this.busDriver).subscribe((data) => {
      if (data.result) { 
      this.toastr.success(data.message); }
      else
      {
        this.toastr.error(data.message)
      }
      this.getAllDrivers();
      this.form.reset();
    });
  }
  driverBusdetails() {
    this.ts.busDetails().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.dataSource1 = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      }
    })
  }
  editDriver(id: number) {
    this.getAllBuses();
    this.getAllDriversFromApplicants();
    this.ts.getDriverById(id).subscribe(data => {
      if(data.result && data.data!='NDF')
      {
      this.busDriver.Id =data.data[0].id;
      this.busDriver.busId = data.data[0].busid;
      this.busDriver.driverId =data. data[0].driverid;
      this.us.getAllDriversFromApplicantsById(this.busDriver.driverId).subscribe((data) => {
        this.driverDetails=data.data
      })
      this.busDriver.date = this.pipe.transform(data.data[0].startdate, 'MM-dd-yyyy');
      this.busDriver.status = data.data[0].status;
    }
    })
  }
  updateBusDriver() {
    this.ts.updateBusDriver(this.busDriver)
      .subscribe((data) => {
        if(data.result)
        this.toastr.success(data.message);
        else
        this.toastr.error(data.message)
        this.getAllDrivers()
      })
  }
  getAllBuses() {
    this.ts.getAllBus().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.busDetails = data.data;
      }
    })
  }
  getAllDriversFromApplicants() {
    this.us.getAllDriversFromApplicants().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.driverDetails = data.data;
      }
    })
  
  }
  getIds() {
    this.getAllBuses();
    this.getAllDriversFromApplicants()
  }
  getAllDrivers() {
    this.ts.getAllDriver().subscribe(data => {
      if(data.result && data.data!='NDF')
      {
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      }
    })
  }
  getexcel() {
    this.ts.exportAsExcelFile(this.dataSource.data, 'Bus Driver');
  }
  addStaffUser() {
    this.route.navigate(['user/add-user/Default']);
  }
  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getPermissions() {
    this.ls.checkRightPermissions('bus-driver-alocation').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  canActivateChild(x:any):boolean{
    if(x==true){
      return true;
    }else{
      this.router.navigate(['404']);
        return false;
    }
  }
      
  ngOnInit() {
    this.getPermissions();
    this.getAllDriversFromApplicants();
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename = data.data[0].rolename;
      })
    }
    this.getAllDrivers();
    this.busDriver.busId = 0;
    this.busDriver.driverId = 0;
  }
  viewDriver() {
    this.show1=!this.show1;
    this.viewdriver = !this.viewdriver
  }
}
