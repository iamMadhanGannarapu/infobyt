import { Component, OnInit,ViewChild } from '@angular/core';
import {TransportStopFee}  from '../../modal/Transport/transportstopfee';
import { TransportService } from '../../service/transport.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../service/user.service';
import {MatSort, MatTableDataSource,MatPaginator} from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-transport-fee-master',
  templateUrl: './transport-fee-master.component.html',
  styleUrls: ['./transport-fee-master.component.scss']
})
export class TransportFeeMasterComponent implements OnInit,CanActivateChild {
  dataSource;
  displayedColumns=['routename','stopname','amount','branch','edit','Excel']
  stopFee: TransportStopFee
  stopsList: any[]= []
  show1:boolean=true;
  transportfee:boolean;
  transportRouteDetails: any
  rolename: any;
  trnFee: any;
  read: any;
  write: any;
  permissionList: any;
  constructor(private router:Router,private ls:LoginService,private toastr:ToastrService,private us:UserService,private ts: TransportService) {
    this.stopFee = new TransportStopFee()
  }
@ViewChild("addtransportfeeForm") form:any;
@ViewChild(MatSort) sort:MatSort;
@ViewChild(MatPaginator) paginator: MatPaginator;
  addTransportFee() {
    this.ts.saveTransportFee(this.stopFee).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.form.reset();
      }
      else{
        this.toastr.error(data.message)
      }
      this.getAllTransportFee();
    });
  }
  getIds() {
    this.ts.getAllTransportRoute().subscribe((data) => {
      this.transportRouteDetails = data.data;
    });
  }
  getStops(){
    this.ts.getTransportStopsByrouteId(this.stopFee.routeId).subscribe((data)=>{
      this.stopsList=data.data;
    })
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  editTransportFee(id: number) {
    this.getIds();
    this.getStops()

    this.ts.getTransportFeeById(id).subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.stopFee.Id = data.data[0].id;
      this.stopFee.stopId = data.data[0].stopid;
      this.stopFee.routeId = data.data[0].routeid;
      this.stopFee.amount = data.data[0].amount;
      }
    })
  }
  updateTransportFee() {
    this.ts.updateTransportFee(this.stopFee).subscribe(data => {
      if(data.result)
      {
      this.toastr.success(data.message);
    }
    else
    {
      this.toastr.error(data.message)
    }
      this.getAllTransportFee()
      this.form.reset();
    });
  }
  getAllTransportFee() {
    this.ts.getAllTransportFee().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.dataSource=new MatTableDataSource(data.data)
      this.dataSource.sort=this.sort
      this.dataSource.paginator=this.paginator
      }
    });
  }
  getexcel(){
    this.ts.exportAsExcelFile(this.dataSource.data,'TransportFee')
  }
  getPermissions() {
    this.ls.checkRightPermissions('transport-fee-master').subscribe((data)=>{
  
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  canActivateChild(x:any):boolean{
    if(x==true){
      return true;
    }else{
      this.router.navigate(['404']);
        return false;
    }
      }

  ngOnInit() {
    this.getPermissions();
    this.getIds();
    this.getAllTransportFee();
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename= data.data[0].rolename;
      })
    }
    this.stopFee.routeId=0;
    this.stopFee.stopId=0;
  }
  transportFee()
  {
    this.show1=!this.show1;
    this.transportfee=!this.transportfee
  }
}