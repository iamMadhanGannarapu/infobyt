
import { Component, OnInit, ViewChild } from '@angular/core';
import { UserTransportPayment } from '../../modal/Transport/UserTransportPayment';
import { TransportService } from '../../service/transport.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from "@angular/common";
import { UserService } from '../../service/user.service';

import { BsDatepickerConfig } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { FeeService } from '../../service/fee.service';
import { BatchService } from '../../service/batch.service';
import { DatePipe } from '@angular/common';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';

import { LoginService } from 'src/app/service/login.service';
@Component({
  selector: 'app-user-transport-payment',
  templateUrl: './user-transport-payment.component.html',
  styleUrls: ['./user-transport-payment.component.scss']
})
export class UsertransportPaymentComponent implements OnInit {
  dataSource;
  displayedColumns = ['UserName', 'PaidAmount', 'Due', 'receipt']
  utp: UserTransportPayment;
  userStudentList: any
  pipe = new DatePipe('en-US');
  maxDate = new Date();
  show1:boolean=true;
  colorTheme = 'theme-dark-blue';
  dueList: any;
  feeList: any;
  usertransport: boolean;
  payable: any;
  batchId: any
  batchList: any;
  bsConfig: Partial<BsDatepickerConfig>;
  rolename: any;
  utpa: any;
  receiptList: any;
  utpayment: any;
  write: any;
  read: any;
  constructor(private ls: LoginService, private toastr: ToastrService, private ts: TransportService, private batch: BatchService, private ar: ActivatedRoute, private location: Location, private userService: UserService, private feeservice: FeeService) {
    this.utp = new UserTransportPayment();
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("frm") form: any;
  addUserTransportPayment() {
    this.ts.saveUserTransportPayment(this.utp).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.form.reset();
      }
      else {
        this.toastr.error(data.message)
      }
      this.getAllTransportPayment();
    })
  }
  editUserTransportPayment(id: number) {
    this.getIds();
    this.ts.getUserTransportPaymentById(id).subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.utpa = data.data[0];
        this.utp.Id = data.data[0].id;
        this.utp.userId = data.data[0].userid;
        this.utp.paidAmount = data.data[0].paidamount;
        this.pipe.transform(data.data[0].paiddate, 'MM-dd-yyyy');
        this.utp.due = data.data[0].due;
        this.utp.status = data.data[0].transportpaymentstatus;
      }
    });
  }
  updateUserTransportPayment() {
    this.ts.updateUserTransportPayment(this.utp).subscribe((data) => {
      if (data.result)
        this.toastr.success(data.message);
      else
        this.toastr.error(data.message)
      this.getAllTransportPayment();
    })
  }
  viewAllStudentNameForAccountant() {
    this.feeservice.getAllTraineenameForAccountant().subscribe((data) => {
      this.userStudentList = data.data;
    })
  }
  getIds() {
    this.viewAllStudentNameForAccountant();
  }
  getFee() {
    this.ts.getTransportFeeByUserId(this.utp.userId).subscribe((data) => {
      this.feeList = data.data
      this.ts.getUserTransportPaymentByUserId(this.utp.userId).subscribe((data) => {
        this.dueList = data.data
        this.calulatePayable();
      });
    });
  }
  getBatch() {
    alert(this.batchId)
    this.ts.getUserTransportPaymentByBatchId(this.batchId).subscribe((data) => {
      console.log('hhhhhhhhhhhhhhhhhhi11111111111111')
      console.log(data.data)
      if(data.result && data.data!='NDF')
      {
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      console.log('hhhhhhhhhhhhhhhhhhi')
      console.log(this.dataSource)
      }
    });
  }
  getAllTransportPayment() {
    this.ts.getAllUserTransportPayment().subscribe((data) => {
     
    })
  }
  getexcel() {
    this.ts.exportAsExcelFile(this.dataSource, 'User TransportPayment');
  }
  calulatePayable() {
    this.payable = this.feeList[0].amnt - this.dueList[0].amount
  }
  calculateDue() {
    this.utp.due = this.payable - this.utp.paidAmount
  }
  getPermissions() {
    this.ls.checkRightPermissions('user-transport-payment').subscribe((data) => {
      
      this.read = data.data[0].read;
      this.write = data.data[0].write;
    })
  }
  ngOnInit() {
    this.batch.getAllBatch().subscribe((data) => {
      this.batchList = data.data;
    });
    if (sessionStorage.getItem('user') != null) {
      this.userService.getUserbySession().subscribe((data) => {
        
        this.rolename =  data.data[0].rolename;
      })
    }
    this.getPermissions();
    this.getAllTransportPayment();
    this.utp.userId = 0;
  }
  applyFilter(filterValue: string) {
  
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getReceipt(id: number) {
    this.ts.getUserTransportPaymentById(id).subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.receiptList = data.data;
      }
    })
  }
  userTransport() {
    this.show1=!this.show1;

    this.usertransport = !this.usertransport
  }
}
