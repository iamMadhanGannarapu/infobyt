import { Component, OnInit, ViewChild } from '@angular/core';
import { TransportStops } from '../../modal/Transport/TransportStops'
import { TransportService } from '../../service/transport.service'
import { OrganizationService } from '../../service/organization.service'
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../service/user.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-transport-stops',
  templateUrl: './transport-stops.component.html',
  styleUrls: ['./transport-stops.component.scss']
})
export class TransportStopsComponent implements OnInit,CanActivateChild {
  dataSource;
  displayedColumns = ['RouteName', 'StopName', 'Arrivaltime', 'departuretime', 'branch', 'edit', 'Excel']
  showMap: boolean = false;
  lat: any = 17.437462
  lng: any = 78.448288
  latitude: any
  longitude: any
  transStop: TransportStops;
  transportRouteDetails: any;
  viewstops: boolean;
  show1:boolean=true;
  schoolBranch: any;
  rolename: any;
  TransportStops: any;
  public selectedMoments = [new Date(), new Date()];
  read: any;
  write: any;
  constructor(private router:Router,   private ls: LoginService, private os: OrganizationService, private userService: UserService, private toastr: ToastrService, private ts: TransportService) {
    this.transStop = new TransportStops();
  }
  btnGetMapClick() {
    this.showMap = true;
  }
  onMapClick(ev) {
    this.transStop.latitude = ev.coords.lat + ''
    this.transStop.longitude = ev.coords.lng + ''
    this.showMap = false;
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("frm") form: any;
  addTransportStops(timeranger: any) {
    this.transStop.arrivalTime = timeranger.substring(0, timeranger.indexOf('~') - 1)
    this.transStop.departureTime = timeranger.substring(timeranger.indexOf('~') + 1, timeranger.length)
    this.ts.saveTransportStops(this.transStop).subscribe((data) => {
      this.getAllTransportStops();
      if (data.result) {
        this.toastr.success(data.message);
        this.form.reset();
      }
      else {
        this.toastr.error(data.message)
      }
    });
  }
  editTransrouteStops(id: number) {
    this.getIds();
    this.ts.getTransportStopsById(id).subscribe((res) => {
      if (res.result && res.data != 'NDF') {
        this.transStop.Id = res.data[0].id;
        this.transStop.name = res.data[0].stopname;
        this.transStop.arrivalTime = res.data[0].arrivaltime;
        this.transStop.departureTime = res.data[0].departuretime;
        this.transStop.latitude = res.data[0].latitude;
        this.transStop.longitude = res.data[0].longitude;
        this.transStop.routeId = res.data[0].routeid;
      }
    });
  }
  updateTransportStops() {
    this.ts.updateTransportStops(this.transStop).subscribe((data) => {
      this.getAllTransportStops();
      if (data.result)
        this.toastr.success(data.message);
      else
        this.toastr.error(data.message)
    })
  }
  backto() {
    location.reload();
  }
  viewTransportRoute() {
    this.ts.getAllTransportRoute().subscribe((data) => {
      this.transportRouteDetails = data.data;
    });
  }
  viewSchoolBranchById() {
    this.os.getBranchById().subscribe((data) => {
      this.schoolBranch = data.data;
    })
  }
  getIds() {
    this.viewTransportRoute();
    this.viewSchoolBranchById();
  }
  getAllTransportStops() {
    this.ts.getAllTransportStops().subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.dataSource = new MatTableDataSource(data.data)
        this.dataSource.sort = this.sort
        this.dataSource.paginator = this.paginator
      }
    }
    );
  }
  getexcel() {
    this.ts.exportAsExcelFile(this.dataSource.data, 'TransportStops')
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getPermissions() {
    this.ls.checkRightPermissions('transport-stops').subscribe((data) => {
      this.read = data.data[0].read;
      this.write = data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  ngOnInit() {
    if (sessionStorage.getItem('user') != null) {
      this.userService.getUserbySession().subscribe((data) => {
        this.rolename = data.data[0].rolename;
      })
    }
    this.getPermissions();
    this.getAllTransportStops();
    this.transStop.routeId = '0';
  }
  canActivateChild(x:any):boolean{
    if(x==true){
      return true;
    }else{
      this.router.navigate(['404']);
        return false;
    }
      }

  viewStops() {
    this.show1=!this.show1;
    this.viewstops = !this.viewstops
  }
}
