import { Component, OnInit, ViewChild } from '@angular/core';
import { Bus } from '../../modal/Transport/Bus';
import { TransportService } from '../../service/transport.service';
import { OrganizationService } from '../../service/organization.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../service/user.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';

@Component({
  selector: 'app-bus-master',
  templateUrl: './bus-master.component.html',
  styleUrls: ['./bus-master.component.scss']
})
export class BusMasterComponent implements OnInit ,CanActivateChild{
  dataSource;
  displayedColumns = ['regnum', 'description','branch', 'edit','Excel']
  bus: Bus;
  branchList: any
  viewbus:boolean;
  show1:boolean=true;
  rolename: any;
  read: any;
  write: any;
  constructor(private router:Router,   private ls:LoginService,private toastr: ToastrService, private us: UserService, private ts: TransportService, private os: OrganizationService) {
    this.bus = new Bus()
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("busForm") form: any;
  getIds() {
    this.os.getBranchById().subscribe((data) => {
      this.branchList = data.data;
    })
  }
  addBus() {
    this.ts.saveBus(this.bus).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.form.reset();
      }
      else
      {
        this.toastr.error(data.message)
      }
      this.getall();
    })
  }
  editBus(id: number) {
    this.getIds()
    this.ts.getBusById(id).subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.bus.Id = data.data[0].id;
      this.bus.regNum = data.data[0].regnum;
      this.bus.description = data.data[0].description;
      this.bus.branchId = data.data[0].branchid;
      this.bus.latitude = data.data[0].Latitude;
      this.bus.longitude = data.data[0].Longitude
      }
    });
  }
  updateBus() {
    this.ts.UpdateBus(this.bus).subscribe((data) => {
      if(data.result)
      this.toastr.success(data.message);
      else
      this.toastr.error(data.message)
      this.getall();
    })
  }
  getall() {
    this.ts.getAllBus().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      }
    });
  }
  getexcel(){
    this.ts.exportAsExcelFile(this.dataSource.data,'Bus');
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getPermissions() {
    this.ls.checkRightPermissions('bus-master').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  canActivateChild(x:any):boolean{
    if(x==true){
      return true;
    }else{
      this.router.navigate(['404']);
        return false;
    }
      }


  ngOnInit() {
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename=data.data[0].rolename;
      })
    }
    this.getPermissions();
    this.getall();
    this.bus.branchId=0;
  }
  viewBus()
  {
    this.show1=!this.show1;
    this.viewbus=!this.viewbus
  }
}  
