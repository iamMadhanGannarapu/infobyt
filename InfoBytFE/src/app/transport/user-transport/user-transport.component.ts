import { Component, OnInit, ViewChild } from '@angular/core';
import { UserTransport } from '../../modal/transport/UserTransport';
import { TransportService } from '../../service/transport.service'
import { UserService } from '../../service/user.service';
import { ToastrService } from 'ngx-toastr';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-user-transport',
  templateUrl: './user-transport.component.html',
  styleUrls: ['./user-transport.component.scss']
})
export class UserTransportComponent implements OnInit ,CanActivateChild{
  dataSource;
  displayedColumns = ['UserName', 'StopName', 'branch','edit','Excel']
  utransport: UserTransport;
  userFacultyList: any;
  show1:boolean=true;
  userStudentList: any;
  usertransport:boolean;
  userTransportList: any;
  selectList: any;
  showFilter: string = ''
  rolename: any;
  FeetransactionList:any;
  write: any;
  read: any;
  constructor(private router:Router,  private ls:LoginService,private toastr: ToastrService, private ts: TransportService, private us: UserService) {
    this.utransport = new UserTransport();
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("myfrm") form: any;
  editUserTransport(id: number) {
    this.getIds();
    this.ts.getUserTransportById(id).subscribe(data => {
      if(data.result && data.data!='NDF')
      {
      this.utransport.Id =data.data[0].id;
      this.utransport.userId = data.data[0].userid;
      this.utransport.stopId = data.data[0].stopid;
      }
    })
  }
  updateUserTransport() {
    this.ts.updateUserTransport(this.utransport).subscribe((data) => {
      if(data.result)
      this.toastr.success(data.message);
      else
      this.toastr.error(data.message)
      this.getAllUserTransport();
    })
  }
  viewAllStaff() {
    this.us.getAllStaff().subscribe((data) => {
      this.userFacultyList = data.data
    })
  }
  viewAllStudent() {
    this.us.getAllFromApplicantsForTrainees().subscribe((data) => {
      this.userStudentList = data.data
    })
  }
  viewAllTransportStops() {
    this.ts.getAllTransportStops().subscribe((data) => {
      this.userTransportList = data.data
    })
  }
  getIds() {
    this.viewAllStaff();
    this.viewAllStudent();
    this.viewAllTransportStops();
  }
getAllTransactions() {
  this.ts.gettransportfeeByUserId().subscribe((data) => {
    this.FeetransactionList = data.data
  });
}
  showFaculty() {
    if (this.showFilter == 'faculty') {
      this.selectList = this.userFacultyList
    }
    else {
      this.selectList = this.userStudentList
    }
  }
  addUserTransport() {
    this.ts.saveUserTransport(this.utransport).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.getAllUserTransport();
        this.form.reset();
      }
      else
      this.toastr.error(data.message)
    });
  }
  getAllUserTransport() {
    this.ts.getAllUserTransport().subscribe(data => {
      if(data.result && data.data!='NDF')
      {
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      }
    });
  }
  getexcel(){
    this.ts.exportAsExcelFile(this.dataSource.data,'user Transport');
  }
  getPermissions() {
    this.ls.checkRightPermissions('user-transport').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  canActivateChild(x:any):boolean{
    if(x==true){
      return true;
    }else{
      this.router.navigate(['404']);
        return false;
    }
      }

  ngOnInit() {
    this.getPermissions();
    this.getAllTransactions();
    this.getAllUserTransport();
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
          this.rolename=data.data[0].rolename;
      })
    }
    this.utransport.userId=0;
    this.utransport.stopId=0;
  }
  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  userTransport()
  {
    this.show1=!this.show1;
    this.usertransport=!this.usertransport
  }
}