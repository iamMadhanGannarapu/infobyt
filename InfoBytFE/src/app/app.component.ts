import { Component, DoCheck, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { LoginService } from './service/login.service';
import { UserService } from './service/user.service';
import { ToastrService } from 'ngx-toastr';
import { Location } from "@angular/common";
import { User } from './modal/User/User';
import { BatchService } from './service/batch.service';
import { HttpClient } from '@angular/common/http';
import * as io from 'socket.io-client'
import * as config from './service/service_init.json';
import { DashboardService } from "src/app/service/dashboard.service";
import { ImageCompressService, ResizeOptions, ImageUtilityService, IImage, SourceImage } from 'ng2-image-compress';
import { SettingsService } from './service/settings.service';
import { OrganizationService } from './service/organization.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  rolename: any = ''
  branchId: any = ''
  oPermissionList: any
  permissionList: any;
  usr: Observable<any>;
  idleTime: number = 0;
  idleInterval: any;
  userProfile: any;
  display: any = false;
  conversationId: any;
  chatFeedbackId: any
  user: User;
  count: number;
  notificationList: any;
  image: any;
  showChat: boolean = false
  showSignIn: boolean = true
  showChatPanel: boolean = false
  images: File;
  timer: any;
  supportType: any;
  showFeedback:any=false;
  showCloseButton:any=false;
  messages: any[] = []
  private socket;
  private toUser: string = "";
  private frmUser: string = "";
  h: number;
  t: number;
  chatUrl: string = (<any>config).chat;
  myurl: any;
  modules: any;
  constructor(private os: OrganizationService, private sts: SettingsService, private imgCompressService: ImageCompressService, private ht: HttpClient, private bs: BatchService, private ss: SettingsService, private ts: ToastrService, private rt: Router, private us: UserService, private ls: LoginService, private loc: Location, private ds: DashboardService) {
    this.usr = null;
    this.user = new User();
    this.image = { 'URL': '', 'valid': false };
    this.myurl = this.ls.url;
  }
  scrollToBottom() {
    $("#scroll").animate({ scrollTop: $(document).height() }, "slow");
  }
  getPermissions() {
    this.sts.getUserModules().subscribe((data) => {
      if (data.data != 'NDF')
        this.permissionList = data.data
      //console.log(this.permissionList)
    })
  }

  getOrgPermissions() {
    this.ls.getPermissionsForRole().subscribe((data) => {
      if (data.data != 'NDF')
        this.oPermissionList = data.data
    })
  }

  logout() {
    if (this.userProfile.rolename == 'Support' || this.userProfile.rolename == 'SalesSupport') {
      this.ls.logoutSupport('Logout').subscribe((data) => { })
    }
    sessionStorage.removeItem("user");
    sessionStorage.removeItem("conversationId");
    sessionStorage.removeItem("userChatId")
    sessionStorage.removeItem("outTime")
    this.userProfile = null;
    this.usr = null;
    this.loc.replaceState('/');
    this.rt.navigate(['login/Login']);
    this.ts.success("Logged Out")
  }

  private checkLoggedUser(): Observable<any> {
    this.us.getUserbySession().subscribe((data) => {
      this.userProfile = data.data[0];
      this.rolename = this.userProfile.rolename
    })
    return of(this.userProfile);
  }

  getLoggedUser() {
    if (sessionStorage.getItem('user') != null) {
      this.checkLoggedUser().subscribe((data) => {
        this.usr = data;
        this.getProfilePic();
        this.getOrgPermissions()
        this.getPermissions()
      })
    }
  }
  ngDoCheck() {
    if (sessionStorage.getItem('user') != null && this.usr == null) {
      this.getLoggedUser()
    }
  }
  onFileSelect(fileInput: any) {
    var fileName = fileInput.target.value;
    var idxDot = fileName.lastIndexOf(".") + 1;
    var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
    if (extFile == "jpg" || extFile == "jpeg" || extFile == "png") {
      ImageCompressService.filesToCompressedImageSource(fileInput.target.files).then(observableImages => {
        observableImages.subscribe((image) => {
          this.user.Id = this.userProfile.userid
          this.image.URL = image.compressedImage.imageDataUrl;
          this.user.image = image.compressedImage.imageDataUrl;
          this.user.image = this.user.image.replace("data:image/gif;base64,", "");
          this.user.image = this.user.image.replace("data:image/jpeg;base64,", "");
          this.user.image = this.user.image.replace("data:image/jpg;base64,", "");
          this.user.image = this.user.image.replace("data:image/png;base64,", "");
          this.ls.updateProfileImage(this.user).subscribe((data) => {
            if (data) {
              this.image.valid = true;
            }
            else {
              this.image.valid = false;
            }
          })
        }, (error) => {
          this.ts.error("Error while uploading");
        });
      });
      this.getProfilePic()

    } else {
      alert("Only jpg/jpeg and png files are allowed!");

    }
  }
  ChatConnect(uname: string) {
    this.socket = io(this.chatUrl, { query: "userName=" + uname });
    this.getChatMessage().subscribe((data) => {
      for (let i = 0; i < 50; i++) {
        this.getChatUser()
      }
    })
  }

  btnSignInClick() {
    this.ls.addUserToChat(this.frmUser).subscribe((data) => {
      sessionStorage.setItem('userChatId', data[0].fn_adduserchat)
      this.ls.addUsertoQueue(data[0].fn_adduserchat).subscribe((userQueue) => {
        //console.log(this.supportType, userQueue[0].userid, this.frmUser)
        this.ls.checkForSupport(this.supportType, userQueue[0].userid, this.frmUser).subscribe((data) => {
          //console.log(data)
          if (data.length != 0) {
            this.ls.assignSupporttoUser(userQueue[0].userid, this.frmUser, data[0].coalesce).subscribe((assign) => {
              //console.log(assign)
              sessionStorage.setItem('conversationId', assign[0].fn_updateconversation)
             
              this.showFeedback=true;
              console.log('this.showFeedback'+ this.showFeedback)
              this.conversationId = assign[0].fn_updateconversation;
              //console.log(sessionStorage.getItem('userChatId'))
              this.ChatConnect(sessionStorage.getItem('userChatId'));
              this.frmUser = sessionStorage.getItem('userChatId')
              this.showSignIn = false;
              this.showChatPanel = true;

            })
          }
          else {
            this.messages.push({ messages: "Please wait you're in queue" })
            this.showSignIn = false;
            this.showChatPanel = true;
            //console.log(this.chatUrl, { query: "userName=" + sessionStorage.getItem('userChatId') })
            this.socket = io(this.chatUrl, { query: "userName=" + sessionStorage.getItem('userChatId') });
            this.getChatMessage().subscribe((data) => {
            })
          }
        })
      })
    })
  }
  getSelectedUserMessages() {
    this.ls.getSelectedUserMessages(this.frmUser, this.toUser);
  }
  getChatMessage() {
    let observable = new Observable<{ user: string, msg: string }>(
      observer => {
        this.socket.on('sendClient', (data) => {
          if (sessionStorage.getItem('conversationId') == null) {
            this.messages = null;
            this.ls.getConversationIdOfUser(sessionStorage.getItem('userChatId')).subscribe((data) => {
              sessionStorage.setItem('conversationId', data[0].id)
              this.conversationId = sessionStorage.getItem('conversationId')
              this.frmUser = sessionStorage.getItem('userChatId')
              this.showSignIn = false;
              this.showChatPanel = true;
              for (let i = 0; i < 50; i++) {
                this.getChatUser();
              }
              this.ngOnInit();
            })
          }
          observer.next(data)
        });
        return () => { this.socket.disconnect(); }
      });
    return observable;
  }
  sendMessage(umsg: string, fUser: string, toUser: string) {
    var userId;
    userId = sessionStorage.getItem('userChatId')
    this.ls.getChatSupportId(userId).subscribe((support) => {
      var supportId = support[0].supportid;
      this.socket.emit('addmsg', { from: userId, msg: umsg, to: supportId })
      for (let i = 0; i < 50; i++) {
        this.getChatUser()
      }
      this.ngOnInit()
    })
  }
  getChatUser() {
    this.ls.getChatOfUser(this.conversationId).subscribe((data) => {
      this.messages = data
      for (var i = 0; i < this.messages.length; i++) {
        if (this.messages[i].messages == 'Conversation Completed') {
          sessionStorage.removeItem('userChatId')
          sessionStorage.removeItem('conversationId')
          this.showChat = false
          this.socket.emit('disconnect')
          this.socket.disconnect();
          this.messages = null;
          this.showSignIn = true;
          this.frmUser = null;
          this.showChatPanel = false;
          break;
        }
      }
    })
  }
  spnCloseClick() {
    this.showChatPanel = false;
    //this.display = true;
    this.showChat = false;
    var userId = sessionStorage.getItem('userChatId')
    this.ls.addChatFeedback(sessionStorage.getItem('conversationId')).subscribe((data) => {
      this.ls.getChatSupportId(userId).subscribe((support) => {
        var supportId = support[0].supportid;
        this.socket.emit('addmsg', { from: userId, msg: 'Conversation Completed', to: supportId })
        this.showChat = false
        this.socket.emit('disconnect')
        this.socket.disconnect();
        this.messages = null;
        this.showSignIn = true;
        this.frmUser = null;
        this.showChatPanel = false;
        //this.display = true;
        this.showChat = false;
      })
    })
  }
  updateChatFeedback(rating: any) {
    if(parseInt(rating)>0)
    {
      this.showCloseButton=true;
    }
    else
    {
      this.showCloseButton=false;
    }
    this.ls.updateChatFeedback(parseInt(rating), this.conversationId).subscribe((data) => {
      sessionStorage.removeItem('userChatId')
      sessionStorage.removeItem('conversationId')
      this.display = true;
      this.showFeedback=false;
      this.showCloseButton=false;
    })
  }
  closeChatFeedbackModal() {
    sessionStorage.removeItem('userChatId')
    sessionStorage.removeItem('conversationId')
  }
  btnSendMessageClick(msg: any) {
    this.sendMessage(msg.value, sessionStorage.getItem('userChatId'), this.toUser);
    msg.value = '';
  }
  btnShowChatClick() {
    this.showChat = true;
  }
  getProfilePic() {
    this.ls.getProfileImage().subscribe((data) => {
      if (data.path) {
        this.image.URL = JSON.parse(JSON.stringify(data)).path;
        this.image.valid = true;
      }
      else {
        this.image.valid = false;
      }
    })
  }
  timerIncrement() {
    this.idleTime = this.idleTime + 1;
    var userSession = sessionStorage.getItem('user');
    if (this.idleTime > 5 && (userSession != null || userSession != undefined)) {
      sessionStorage.removeItem('user');
      clearInterval(this.idleInterval);
      this.logout();
    }
  }
  pathRout() {
    this.rt.navigate([{ outlets: { view: ['add-user', 'SchoolAdmin'] } }]);
  }
  ngOnInit() {
    if (sessionStorage.getItem('user') != null) {
      this.updateNotification();
    }
    if (!navigator.cookieEnabled) {
    }
    this.getLoggedUser();
    this.idleInterval = setInterval(() => this.timerIncrement(), 60000);
    document.addEventListener("mousemove", () => {
      this.idleTime = 0;
    })
    document.addEventListener("keypress", () => {
      this.idleTime = 0;
    })
    if (sessionStorage.getItem('conversationId') != null) {
      this.conversationId = sessionStorage.getItem('conversationId')
      this.frmUser = sessionStorage.getItem('userChatId')
      this.ChatConnect(sessionStorage.getItem('userChatId'));
      this.ls.getChatOfUser(this.conversationId).subscribe((data) => {
        this.messages = data
        this.showSignIn = false
        this.showChatPanel = true;
      })
      this.getChatMessage().subscribe((data) => {
        this.messages.push(data)
      })
    } else {
      this.showSignIn = true;
      this.showChatPanel = false;
    }
    this.supportType = 'Support';
    $(document).ready(function () {
      $(document).scroll(function () {
        var $nav = $(".sticky-top");
        $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
      });
    });
  }
  updateNotification() {
    this.ds.updateNotification(this.us.notificationList).subscribe((data) => { })
    this.getNotifications();
  }
  getNotifications() {
    this.ds.getNotificationCount().subscribe((data) => {
      //this.count = data[0].notifications
    })
  }
}