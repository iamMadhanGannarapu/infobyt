import { Component, OnInit } from '@angular/core';
import * as io from 'socket.io-client'
import { HttpClient } from '@angular/common/http'
import { Observable, interval, Subscription } from 'rxjs';
import { LoginService } from '../../service/login.service';
import { UserService } from '../../service/user.service';
import { Router } from '@angular/router';
import { Location } from "@angular/common";
import { ToastrService } from 'ngx-toastr';
import * as config from '../../service/service_init.json';
@Component({
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
    showChat: boolean = false
    showSignIn: boolean = true
    showChatPanel: boolean = false
    uemail: string = ''
    selUserMessages: any[] =  [{
        'conversationid': 0, 'convoid': 0, 'convostatus': "",
        'currentdate': "", 'fromuser': "", 'id': 0, 'messages': "",
        'status': "", 'supporttype': "", 'suppportid': 0, 'touser': "",
        'userid': "", 'username': ""
    }];
    url = (<any>config).chat;
    private socket;
    onlineUsers: string[] = [];
    toUser: string = "";
    frmUser: string = "";
    timer: any;
    recievedMsgCount: any[];
    messages: any[] = []
    chatUser: any;
    openChat: any;
    currentUser: any;
    currentMsg: any
    value: Date;
    today: Date;
    sub: Subscription;
    roleName: any;
    myurl: any;
    constructor(private ht: HttpClient, private ts: ToastrService, private loginService: LoginService, private userService: UserService, private rt: Router, private loc: Location) {
        this.myurl = loginService.url;
    }
    scrollToBottom() {
        $(".messages").animate({
            scrollTop: $('.messages').get(0).scrollHeight
        }, "slow");
    }
    ChatConnect(uname: string) {
        this.socket = io(this.url, { query: "userName=" + uname });
        this.getChatMessage().subscribe((data) => {
            let flag: boolean = false
            this.ngOnInit();
            console.log(this.currentUser)
            if (this.currentUser) {
                for (let i = 0; i < 50; i++) {
                    this.userChatSelect(this.currentUser, 'A');
                }
            }
            this.recievedMsgCount.forEach(element => {
                if (element.user == data.user) {
                    element.msgCount = element.msgCount + 1;
                    flag = true;
                }
            });
            if (flag == false) {
                this.recievedMsgCount.push({ user: data.user, msgCount: 1 })
            }
        })
    }
    updateBreak() {
        this.loginService.updateBreak(this.value.getHours() + ':' + this.value.getMinutes() + ':' + this.value.getSeconds() + '.' + this.value.getMilliseconds()).subscribe((data) => { })
        sessionStorage.setItem('outTime', this.value.getHours() + ':' + this.value.getMinutes() + '')
        this.sub = interval(20000).subscribe((val) => {
            this.today = new Date;
            if ((this.today.getHours() + ':' + this.today.getMinutes()) == sessionStorage.getItem('outTime')) {
                sessionStorage.removeItem('outTime')
                if (confirm("It is your break time, Do you want to Logout?")) {
                    this.loginService.logoutSupport('Break').subscribe((data) => {
                        sessionStorage.removeItem("user");
                        this.loc.replaceState('/');
                        this.rt.navigate(['login/Login']);
                        this.ts.success("Logged Out");
                    })
                }
                else {
                    this.loginService.addSupportUser(parseInt(this.frmUser), this.roleName, 'Chat').subscribe((chat) => {
                        this.sub.unsubscribe();
                        if (chat.message == 'Added to conversation table') {
                            this.rt.navigate(['Chat']);
                        }
                        else {
                            sessionStorage.setItem('assignedUserId', chat[0].userid)
                            this.rt.navigate(['Chat']);
                        }
                    })
                }
            }
        })
    }
    removeChat(id: number) {
        var userId;
        userId = this.frmUser
        this.loginService.getChatClientId(id).subscribe((client) => {
            var clientId = client[0].userid;
            this.socket.emit('addmsg', { from: userId, msg: 'Conversation Completed', to: clientId });
            this.loginService.removeChat(id).subscribe((data) => {
                this.loginService.addNewConvo(id).subscribe((data) => {
                    if (data.userid != 'Added to conversation table')
                        sessionStorage.setItem('assignedUserId', data[0].userid)
                    this.loginService.getAssignedUsersForSupport().subscribe((data) => {
                        this.recievedMsgCount = data;
                        this.selUserMessages =
                            [{
                                'conversationid': 0, 'convoid': 0, 'convostatus': "",
                                'currentdate': "", 'fromuser': "", 'id': 0, 'messages': "",
                                'status': "", 'supporttype': "", 'suppportid': 0, 'touser': "",
                                'userid': "", 'username': ""
                            }];
                        this.ngOnInit();
                    })
                })
            })
        })
    }
    GetSelectedUserMessages() {
        return this.ht.get(this.url + '/' + this.frmUser + '/' + this.toUser, { responseType: 'json' });
    }
    getChatMessage() {
        let observable = new Observable<{ user: string, msg: string }>(
            observer => {
                this.socket.on('sendClient', (data) => {
                    data.status = 'u';
                    this.loginService.getAssignedUsersForSupport().subscribe((data) => {
                        this.recievedMsgCount = data;
                        observer.next(data)
                    })
                });
                return () => { this.socket.disconnect(); }
            });
        return observable;
    }
    sendMessage(umsg: string, fUser: string, toUser: string) {
        var userId;
        userId = this.frmUser
        this.loginService.getChatClientId(this.currentUser).subscribe((client) => {
            var clientId = client[0].userid;
            this.socket.emit('addmsg', { from: userId, msg: umsg, to: clientId });
            for (let i = 0; i < 50; i++)
                this.userChatSelect(this.currentUser, 'A');
        })
    }
    spnCloseClick() {
        this.showChat = false
        this.socket.emit('disconnect')
        this.socket.disconnect();
    }
    btnSendMessageClick(msg: any) {
        this.currentMsg = msg.value;
        msg.value = '';
        this.sendMessage(this.currentMsg, this.frmUser, this.currentUser)
    }
    userChatSelect(optValue, update) {
        if (update == 'U') { this.loginService.updateReadMessages(optValue).subscribe((data) => { }) }
        this.currentUser = optValue;
        this.loginService.getChatOfUser(optValue).subscribe((data) => {
            this.openChat = true;
            this.selUserMessages = data
            if (this.selUserMessages[this.selUserMessages.length - 1].messages == 'Conversation Completed') {
                this.selUserMessages = 
                [{
                    'conversationid': 0, 'convoid': 0, 'convostatus': "",
                    'currentdate': "", 'fromuser': "", 'id': 0, 'messages': "",
                    'status': "", 'supporttype': "", 'suppportid': 0, 'touser': "",
                    'userid': "", 'username': ""
                }];
                this.removeChat(optValue)
            }
            this.loginService.getAssignedUsersForSupport().subscribe((data) => {
                this.recievedMsgCount = data;
            })
        })
    }
    ngOnInit() {
        console.log(this.url)
        this.userService.getUserbySession().subscribe((user) => {
            this.chatUser = user.data[0];
            this.frmUser = user.data[0].userid;
            this.roleName = user.data[0].rolename
            this.ChatConnect(this.frmUser);
            if (sessionStorage.getItem('assignedUserId') != null) {
                this.socket = io(this.url, { query: "userName=" + sessionStorage.getItem('assignedUserId') });
                this.socket.emit('addmsg', { from: sessionStorage.getItem('assignedUserId'), msg: 'Hello', to: this.frmUser });
                sessionStorage.removeItem('assignedUserId')
                this.ngOnInit();
            }
            if (sessionStorage.getItem('outTime') != null) {
                this.sub = interval(10000).subscribe((val) => {
                    this.today = new Date;
                    if ((this.today.getHours() + ':' + this.today.getMinutes()) == sessionStorage.getItem('outTime')) {
                        if (confirm("It is your break time, Do you want to Logout?")) {
                            this.loginService.logoutSupport('Break').subscribe((data) => {
                                sessionStorage.removeItem("user");
                                sessionStorage.removeItem('outTime');
                                this.loc.replaceState('/');
                                this.rt.navigate(['login/Login']);
                                this.ts.success("Logged Out")
                            })
                        }
                        else {
                            this.loginService.addSupportUser(parseInt(this.frmUser), this.roleName, 'Chat').subscribe((chat) => {
                                this.sub.unsubscribe();
                                sessionStorage.removeItem('outTime');
                                if (chat.message == 'Added to conversation table') {
                                    this.rt.navigate(['Chat']);
                                }
                                else {
                                    sessionStorage.setItem('assignedUserId', chat[0].userid)
                                    this.rt.navigate(['Chat']);
                                }
                            })
                        }
                    }
                })
            }
            this.loginService.getAssignedUsersForSupport().subscribe((data) => {
                this.recievedMsgCount = data;
            })
        })
    }
    btnShowChatClick() {
        this.showChat = true;
    }
}
