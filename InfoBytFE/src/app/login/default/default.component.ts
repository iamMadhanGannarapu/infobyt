import { Component, OnInit, ViewChild } from '@angular/core';
import { LoginService } from '../../service/login.service'
import { ContactDetails } from '../../modal/User/ContactDetails'
import { User } from '../../modal/User/User'
import { UserService } from '../../service/user.service';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { Organization } from '../../modal/Organization/Organization';
import { OrganizationService } from '../../service/organization.service';
import { ToastrService } from 'ngx-toastr';
import { Branch } from '../../modal/Organization/Branch';
import { BatchService } from '../../service/batch.service';
import { Session } from '../../modal/Batch/SessionMaster';
import { DashboardService } from "src/app/service/dashboard.service";
import { SettingsService } from "src/app/service/settings.service";
import { Subject as rxjssubject } from "rxjs";
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from '../../modal/Batch/Subject'
import { BranchSession } from '../../modal/Organization/BranchSession';
@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss']
})
export class DefaultComponent implements OnInit {
  user: User;
  contact: ContactDetails
  school: Organization;
  schoolbranchmaster: Branch;
  classAddObj: Session;
  branchclass: BranchSession;
  subjectAddObj: Subject
  i: number = 0
  city: any = 'city';
  schoolAddButton: boolean = true;
  isLinear = true;
  isOptional = false;
  show: boolean = true;
  userProfile: any;
  contactDetails: any;
  contactdetailsList: any;
  branchclassList: any;
  getSchoolbranchmaster: any;
  schoolMasterList: any;
  subjectGetlist: any;
  occupationList: any;
  nationalitiesList: any;
  middleNameList: any;
  classGetList: any;
  placeList: any[] = [];
  listAchievements: any;
  listNotifications: any;
  sub: any;
  msg: any;
  subCaste: any;
  usercountryList: any[] = [];
  userstateList: any;
  usercitiesList: any;
  classList: any;
  branchList: any;
  myUrl: string;
  listImages: any;
  instituteTypeList: any;
  currentYear: any;
  currentDate: any;
  religionList: any[] = [
    'Hindu',
    'Christian',
    'Sikh',
    'Muslim',
    'Jain',
    'Buddhism',
    'Zoroastrianism',
    'Minoritiess'
  ];
  dtTrigger: rxjssubject<TemplateStringsArray> = new rxjssubject();
  dtOptions: DataTables.Settings = {};
  colorTheme = 'theme-dark-blue';
  bsConfig: Partial<BsDatepickerConfig>;
  maxDate = new Date();
  minDate = new Date();
  start;
  Emailvalidation;
  mobilevalidation;
  aadhaarevalidation;
  constructor(private ls: LoginService,private sts:SettingsService,private ds:DashboardService, private cService: BatchService, private us: UserService, private toastr: ToastrService, private os: OrganizationService,  private router: Router, private route: ActivatedRoute) {
    this.user = new User()
    this.school = new Organization();
    this.branchclass = new BranchSession();
    this.subjectAddObj = new Subject();
    this.schoolbranchmaster = new Branch();
    this.contact = new ContactDetails()
    this.classAddObj = new Session();
    this.myUrl = os.url
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    this.maxDate.setDate(this.maxDate.getDate() - 1);
    this.minDate.setDate(this.minDate.getDate() - 36500);
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    this.currentDate = new Date();
    this.currentYear = (new Date).getUTCFullYear();
    this.start = this.currentYear - 50;
  }
  @ViewChild("schoolFrm") form: any;
  getAllPlaces(CityId: number) {
    this.ls.getAllLocationNames(CityId).subscribe((data) => {
      this.placeList = data;
    });
  }
  addUser() {
    this.user.role = 'BranchAdmin';
    localStorage.setItem("Users", JSON.stringify(this.user));
    this.toastr.success("Successfully Added")
  }
  ngOnInit() {
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.userProfile = data[0];
      })
      this.sub = this.route.params.subscribe(params => {
        this.user.role = params['roleName'];
        this.user.gender = 'm';
        this.contact.country = 0;
        this.contact.state = 0;
        this.contact.city = 0;
      });
      this.getCountries();
      this.getMiddleNames();
      this.getNationalities();
      this.getOccupations();
    }
    this.getGalleryImages();
    this.getIds1();
    this.getIds2();
    this.getIds3();
    this.getInstituteType();
    this.getAllAchievements();
    this.getAllNotifications();
    $(document).ready(function () {
      $("video").prop('volume', 0.2);
      $('#audioControl').click(
        function () {
          if($("video").prop('muted')){
            $("video").prop('muted', false);
            $("video").prop('volume', 0.5);
            $(this).removeClass('fa-volume-off').addClass('fa-volume-down');
          }else{
            if($("video").prop('volume')==1){
              $("video").prop('muted', true);
              $("video").prop('volume', 0);
              $(this).removeClass('fa-volume-up').addClass('fa-volume-off');
            }else{
              $("video").prop('volume', 1);
              $(this).removeClass('fa-volume-down').addClass('fa-volume-up');
            }
          }
        }
      );
    });
  }
  getGalleryImages() {
    this.ds.GetallGallery().subscribe((data) => {
      this.listImages = data
    })
  }
  getAllAchievements() {
    this.os.getAllAchievements().subscribe((data) => {
      this.listAchievements = data;
    });
  }
  getAllNotifications() {
    this.ds.getNotifications().subscribe((data) => {
      this.listNotifications = data
    })
  }
  getCountries() {
    this.us.getAllCountries().subscribe((data) => {
      this.usercountryList = data
    })
  }
  navigate1() {
    this.router.navigate([{ outlets: { view: ['Institute'] } }]);
  }
  navigate2() {
    this.router.navigate([{ outlets: { view: ['School'] } }]);
  }
  navigate3() {
    this.router.navigate([{ outlets: { view: ['Admissions'] } }]);
  }
  navigate4() {
    this.router.navigate([{ outlets: { view: ['Jobs'] } }]);
  }
  navigate5() {
    this.router.navigate([{ outlets: { view: ['Holiday'] } }]);
  }
  navigate6() {
    this.router.navigate([{ outlets: { view: ['Chat'] } }]);
  }
  getCities(stateId: number) {
    this.us.getAllCities(stateId).subscribe((data) => {
      this.usercitiesList = data
    })
  }
  getStates(couId: any) {
    this.us.getAllStates(parseInt(couId)).subscribe((data) => {
      this.userstateList = data;
    });
  }
  verifyEmail(inp: string, inpData: any) {
    this.ls.verifyUserData(inp, inpData.value).subscribe((data) => {
      this.msg = data;
      this.Emailvalidation = this.msg.msg
    });
  }
  verifyMobile(inp: string, inpData: any) {
    this.ls.verifyUserData(inp, inpData.value).subscribe((data) => {
      this.msg = data;
      this.mobilevalidation = this.msg.msg
    });
  }
  verifyAuthencation(inp: string, inpData: any) {
    this.ls.verifyUserData(inp, inpData.value).subscribe((data) => {
      this.msg = data;
      this.aadhaarevalidation = this.msg.msg
    });
  }
  getMiddleNames() {
    this.us.getAllMiddleNames().subscribe((data) => {
      this.middleNameList = data
    })
  }
  getOccupations() {
    this.us.getAllOccupations().subscribe((data) => {
      this.occupationList = data
    })
  }
  getNationalities() {
    this.us.getAllNationalities().subscribe((data) => {
      this.nationalitiesList = data
    })
  }
  // School Admin Initial SetUp
  addSchool() {
    this.os.saveOrganization(this.school).subscribe((data) => {
      if (data) {
        this.form.reset();
      }
      this.getAllSchool();
    });
  }
  getAllSchool() {
    this.os.getAllOrganization().subscribe((data) => {
      if (data.length >= 1) {
        this.schoolAddButton = false
      }
      this.schoolMasterList = data;
      this.dtTrigger.next();
    });
  }
  getInstituteType() {
    this.sts.getAllInstituteType().subscribe((data) => {
      this.instituteTypeList = data;
    })
  }
  getIds1() {
    this.us.getAllContactDetails().subscribe((data) => {
      this.contactDetails = data;
    })
  }
  addContactdetails() {
    this.getBrad();
    let user = JSON.parse(localStorage.getItem("Users"));
    this.us.saveUser(user).subscribe((data) => {
      this.contact.userId = data[0].fn_addusers;
      this.us.saveContactDetails(this.contact).subscribe((data) => {
        localStorage.removeItem('Users');
      })
    })
    this.getBrad();
  }
  addSchoolBranch() {
    this.getBrad();
    this.os.saveBranch(this.schoolbranchmaster).subscribe((data) => {
      if (data) {
        this.form.reset();
      }
    });
  }
  getBrad() {
    // this.us.getAllContactDetailsByRole().subscribe((data) => {
    //   this.contactdetailsList = data;
    // })
  }
  getAllSchoolBranchs(schoolId: any) {
    this.os.getAllBranch().subscribe((data) => {
      for (let i = 0; i < data.length; i++) {
        if (data[i].branchstatus == "Active") {
          data[i].branchstatus = true
        }
        else {
          data[i].branchstatus = false
        }
        this.getSchoolbranchmaster = data;
        this.dtTrigger.next();
      }
    });
  }
  getIds2() {
    this.os.getAllOrganization().subscribe((data) => {
      this.schoolMasterList = data;
    })
  }
  addClass() {
    this.cService.saveSession(this.classAddObj).subscribe((data) => {
      this.classAddObj = data;
      if (data) {
        this.form.reset();
      }
      this.getAllClasses()
    })
  }
  getAllClasses() {
    this.cService.getAllSession().subscribe((res) => {
      this.classGetList = res;
      this.dtTrigger.next()
    });
  }
  //School Branch
  addBranchClass() {
    this.os.saveBranchSession(this.branchclass).subscribe((data) => {
      if (data) {
        this.form.reset();
      }
      this.getAllBranchClass();
    });
  }
  getAllBranchClass() {
    this.os.getAllBranchSession().subscribe((data) => {
      this.branchclassList = data;
      this.dtTrigger.next();
    })
  }
  getIds3() {
    this.cService.getAllSession().subscribe((data) => {
      this.classList = data;
    })
    this.os.getBranchById().subscribe((data) => {
      this.branchList = data;
    })
  }
  addSubject() {
    this.cService.saveSubject(this.subjectAddObj).subscribe((data) => {
      this.form.reset();
      this.getAllSubjects();
    })
  }
  getAllSubjects() {
    this.cService.getAllSubject().subscribe((res) => {
      this.subjectGetlist = res;
      this.dtTrigger.next();
    });
  }
}
