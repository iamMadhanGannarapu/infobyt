import { Component, OnInit, ViewChild } from '@angular/core';
import { User, Authentication } from '../../modal/User/User';
import { ContactDetails } from '../../modal/User/ContactDetails';
import { Router, ActivatedRoute } from '../../../../node_modules/@angular/router';
import { UserService } from '../../service/user.service';
import { Observable, of } from 'rxjs';
import { LoginService } from '../../service/login.service';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import * as $ from 'jquery';
import { DatePipe } from '@angular/common';
import { MessageService } from 'primeng/components/common/messageservice';
import { MatStepper } from '@angular/material';
import { SelectItem } from 'primeng/api';
import { AcademicDetails } from '../../modal/User/academicdetails';
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { GoogleLoginProvider, FacebookLoginProvider, LinkedInLoginProvider } from 'angularx-social-login';
import { SocialLogin } from 'src/app/modal/Login/SocialLogin';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
  styles: [`
        :host ::ng-deep button {
            margin-right: .25em;
        }
        :host ::ng-deep .custom-toast .ui-toast-message {
            color: #ffffff;
            background: #FC466B;
            background: -webkit-linear-gradient(to right, #3F5EFB, #FC466B);
            background: linear-gradient(to right, #3F5EFB, #FC466B);
        }
        :host ::ng-deep .custom-toast .ui-toast-close-icon {
            color: #ffffff;
        }
    `],
  providers: [MessageService]
})
export class SignupComponent implements OnInit {
  usercountryList: SelectItem[];
  SocialUser: SocialUser;
  socialLogin: SocialLogin;

  isEditable = false;
  usr: User;
  cdi: ContactDetails;
  isOptional = false;
  isLinear = true;
  noResult: boolean;
  noNationalityResult: boolean;
  placeList: string[];
  Emailvalidation;
  result = false;
  mobilevalidation;
  aadhaarevalidation;
  occupationList: string[];
  nationalitiesList: string[];
  middleNameList: string[];
  userSession: any;
  userProfile: any;
  msg: any;
  sub: any;
  iagree: any;
  occupationList1: SelectItem[];
  Loginvalidation;
  acadedetails: any;
  viewAcademicForm = false;
  selecteduser: any;
  userDetails: any;
  userreligionList: any[] = [];
  userList: any[] = [];
  userstateList: any;
  usercitiesList: any;
  filteredMiddleName: any[];
  pipe = new DatePipe('en-US');
  maxDate = new Date();
  minDate = new Date();
  colorTheme = 'theme-dark-blue';
  registerSuccess: boolean;
  bsConfig: Partial<BsDatepickerConfig>;
  upduname: any;
  loginid: any;
  userName: any;
  user: Authentication
  acade: AcademicDetails;
  showSubmit = false;
  arrayOne(n: number): any[] {
    return Array(n);
  }
  constructor(private as: AuthService, private toastr: ToastrService, private router: Router, private route: ActivatedRoute, private us: UserService, private ls: LoginService, private ms: MessageService) {
    this.minDate.setFullYear(this.maxDate.getFullYear() - 80);
    this.maxDate.setFullYear(this.maxDate.getFullYear() - 20);
    this.usr = new User();
    this.socialLogin = new SocialLogin();
    this.user = new Authentication();
    this.acade = new AcademicDetails();
    this.usr.role = 'OrganizationAdmin';
    this.cdi = new ContactDetails();
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
  }
  @ViewChild("signup") form: any;
  @ViewChild("adduserForm") form1: any;
  getAllPlaces() {
    this.ls.getAllLocationNames(this.cdi.city).subscribe((data) => {
      this.placeList = data.data;
    });
  }
  viewsubmit(){
    this.showSubmit=true;
  }
  addUser() {
    this.usr.fatherName = 'NA';
    this.usr.motherName = 'NA';
    localStorage.setItem("Users", JSON.stringify(this.usr));
    this.toastr.success('Redirecting to Contacts Page');
  }
 getreligion() {
    this.us.getAllReligion().subscribe((data) => {
      if (data.data != 'NDF') {
        this.userreligionList = data.data;  
      }
      this.userreligionList.push({ id: -1, name: 'Other' })
    })
  }

  getStates(couId: any): Observable<any> {
    this.us.getAllStates(parseInt(couId)).subscribe((data) => {
      this.userstateList = data.data;
      this.cdi.state = 0;
      this.cdi.city = 0;
    })
    return of(JSON.stringify(this.userstateList));
  }
  verifyloginid(inp: string) {
    if (this.user.userName != '' && this.user.userName != null) {
      if (this.user.userName.length >= 6) {
        this.ls.varifyloginid(inp, this.user.userName).subscribe((data) => {
          this.msg = data;
          if (this.msg.data == "Username already Exists!!") {
            this.toastr.error('User Name already exists!');
            this.user.userName = '';
          }
          else if (this.msg.data == "NDF") {
            this.toastr.success('It Suits For U...  :)');
          }
        });
      }
    }
  }
  addContactdetails() {
    let user = JSON.parse(localStorage.getItem("Users"));
    this.us.saveAdmin(user, 0).subscribe((data) => {
      this.cdi.userId = data.data.fn_addadmin;
      this.us.saveContactDetails(this.cdi).subscribe((cdata) => {
        localStorage.removeItem('Users');
        this.registerSuccess = cdata.result;
        if (this.registerSuccess) {
          this.toastr.success('Registered Successfully');
          this.form.reset();
          this.form1.reset();
        }
        else {
          this.toastr.error('Registeration Failed');
        }
      })
    })
  }
  showWarn() {
    this.clear();
    this.ms.add({ severity: 'info', summary: 'Please read the agreement carefully' });
  }
  showError() {
    this.clear();
    this.ms.add({ severity: 'error', summary: 'Please accept the agreement' });
  }
  clear() {
    this.ms.clear();
  }
  onRegstr() {
    this.router.navigate(['login/Login']);
  }
  getCountries() {
    this.us.getAllCountries().subscribe((data) => {
      this.usercountryList = data.data
    })
  }
  getCities(stateId: number) {
    this.us.getAllCities(stateId).subscribe((data) => {
      this.usercitiesList = data.data
      this.cdi.city = 0;
    })
  }
  verifyEmail(inp: string) {
    if (this.cdi.email != '' && this.cdi.email != null) {
      if (this.cdi.email.length > 9) {
        this.ls.verifyUserData(inp, this.cdi.email).subscribe((data) => {
          this.Emailvalidation = data.data;
          if (this.Emailvalidation != 'NDF') {
            this.toastr.error('Email already exists!');
            this.cdi.email = '';
          }
        });
      }
    }
  }
  verifyMobile(inp: string) {
    if (this.cdi.mobile != '' && this.cdi.mobile != null) {
      if (this.cdi.mobile.length > 9) {
        this.ls.verifyUserData(inp, this.cdi.mobile).subscribe((data) => {
          this.mobilevalidation = data.data
          if (this.mobilevalidation != 'NDF') {
            this.toastr.error('Mobile Number already exists!');
            this.cdi.mobile = '';
          }
        });
      }
    }
  } 
  verifyAuthencation(inp: string, inpData: any) {
    this.ls.verifyUserData(inp, inpData.value).subscribe((data) => {
      this.aadhaarevalidation=data.message
      if(this.aadhaarevalidation=='UniqueIdentificationNo already Exists!!'){
        this.toastr.error('Unique IdentificationNo Already Exits')
this.cdi.uniqueIdentificationNo=''
      }
    });
  }
 signInWithGoogle(): void {
    this.as.signIn(GoogleLoginProvider.PROVIDER_ID);
    this.as.authState.subscribe((user) => {
      this.SocialUser = user;
      this.us.getUserbySession().subscribe((data) => {
        console.log("user details: " + data.data[0].userid)
        this.socialLogin.provider = user.provider;
        this.socialLogin.socialId = user.id;
        this.socialLogin.userId = data.data[0].userid;
        this.ls.addSocialLogins(this.socialLogin).subscribe((data) => {
          if (data) {
            alert("sucessfully linked..")
          }
        })
      })
      console.log(JSON.stringify(user))
    });
  }

  signInWithFB(): void {
    alert(FacebookLoginProvider.PROVIDER_ID)
    this.as.signIn(FacebookLoginProvider.PROVIDER_ID);
  }
  signInWithLinkedIn(): void {
    alert(LinkedInLoginProvider.PROVIDER_ID)
    this.as.signIn(LinkedInLoginProvider.PROVIDER_ID);
  }
  ngOnInit() {
    this.as.authState.subscribe((user) => {
      this.SocialUser = user;
      this.usr.firstName = user.firstName;
      this.usr.lastName = user.lastName;
      this.usr.image = user.photoUrl;
      this.cdi.email = user.email;
      alert(JSON.stringify(user))
      console.log(JSON.stringify(user))
    });
    var self = this;
    if (sessionStorage.getItem('user') != null) {
      this.userSession = sessionStorage.getItem('user')
      if (this.userSession != null) {
        this.viewProfile();
      }
    }
    $(document).ready(function () {
      $('#iagree').hide();
      $('#notagree').hide();
      $('#Accept').click(function () {
        self.showError();
      });
      $('#terms').scroll(function () {
        if ($(this).scrollTop() > 2000) {
          $('#Accept').removeAttr('disabled');
          if ($('#iagree').prop("checked") == false) {
            $('#iagree').show();
            $('#notagree').show();
            self.showWarn();
          }
        }
      })
      $('#iagree').click(function () {
        $("#notagree").remove();
      });
    });
    this.getCountries();
    this.getMiddleNames();
    this.getreligion();
    this.getNationalities();
    this.getOccupations();
    $(document).ready(function () {
      $('.signIn').click(function () {
        $('.fold').toggleClass('active')
      })
    })
    this.sub = this.route.params.subscribe(params => {
      this.usr.role = 'OrganizationAdmin';
      this.usr.gender = 'm';
      this.usr.religion = '0';
      this.usr.nationality = '0';
      this.usr.fatherOccupation = '0'
    });
    if (sessionStorage.getItem('user') != null) {
      this.getLastMod();
      this.viewAcademicDetails();
      this.getUSerName();
    }
    this.acade.userId = 0;
    this.acade.experience = 0;
  }
  getMiddleNames() {
    this.us.getAllMiddleNames().subscribe((data) => {
      this.middleNameList = data;
    })
  }
  getOccupations() {
    this.us.getAllOccupations().subscribe((data) => {
      this.occupationList = data.data
      this.occupationList1 = data.data
    })
  }
  getNationalities() {
    this.us.getAllNationalities().subscribe((data) => {
      this.nationalitiesList = data.data
    })
  }
  viewProfile() {
    this.us.getUserbySession().subscribe((data) => {
      this.userDetails = data.data[0];
    })
  }
  updateProfileUser(frm: any) {
    this.result = false;
    this.ls.updateProfile(this.usr).subscribe((data) => {
      if (data) {
        this.toastr.success('Successfully Update profile ');
      }
    })
    this.viewProfile();
  }
  onKeydown(event) {
    if (this.cdi.place != '' || this.cdi.place != null) {
      this.cdi.place = this.cdi.place.replace('/[0-9]/g', '');
      if ((event.keyCode > 64 && event.keyCode < 91) || (event.keyCode > 96 && event.keyCode < 123)) { }
      else { this.cdi.place = this.cdi.place.replace(event.key, ''); }
    }
  }
  filter(query, inputData: any[]): any[] {
    let filtered: any[] = [];
    for (let i = 0; i < inputData.length; i++) {
      let search = inputData[i];
      if (search.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(search);
      }
    }
    return filtered;
  }
  typeaheadNoResults(event: boolean): void {
    this.noResult = event;
  }

  iagreed(agreement: any, stepper: MatStepper) {
    if (agreement.control.status != "VALID") {
      stepper.next();
    }
    else {
    }
  }
  editUserName() {
    this.us.getUserbySession().subscribe((data) => {
      if (data) {
        this.userProfile = data.data[0];
        this.user.Id = this.userProfile.userid;
      }
    })
  }
  updateUserName() {
    this.ls.updateUName(this.user).subscribe((data) => {
      this.upduname = data.data
      if (this.upduname) {
        this.toastr.success('Successfully UserName Updated');
      }
    })
    this.getLastMod()
  }
  getLastMod() {
    this.us.getUserbySession().subscribe((data) => {
      this.userProfile = data.data[0]
      let Id = this.userProfile.userid;
      this.ls.getLastModified(Id).subscribe((data) => {
        this.loginid = data.data[0].loginid
        this.userName = data.data[0].username;
      })
    })
  }
  viewAcademicDiv() {
    this.viewAcademicForm = true;
    this.showSubmit = true;
    if (this.viewAcademicForm) {
      this.result = false;;
    }
    else {
      this.result = true;
    }
    this.getUSerName();
  }
  addAcdemicMark() {
    this.result = false;
    this.viewAcademicForm = false;
    this.us.saveAcademic(this.acade).subscribe((data) => {
      if (data) {
        this.toastr.success('Academic Details Has Been saved');
      }
    },
      (err) => {
        this.toastr.error(err.error.message)
      })
  }
  getUSerName() {
    this.us.getUser().subscribe((data) => {
      this.selecteduser = data.data;
    })
  }
  upDateAcademicdetails() {
    this.us.updatedAcademic(this.acade).subscribe((data) => {
      if (data) {
        this.toastr.success('Academic Details Has Been updated');
      }
    }, (err) => {
      this.toastr.error(err.error.message)
    })
  }
  editProfile() {
    this.result = true;
    this.us.getUserbySession().subscribe((data) => {
      this.userProfile = data.data[0];
      if (data.result && data.data != 'NDF') {
        this.usr.Id = this.userProfile.userid
        this.usr.firstName = this.userProfile.firstname;
        this.usr.middleName = this.userProfile.middlename;
        this.usr.lastName = this.userProfile.lastname;
        this.usr.dob = this.pipe.transform(this.userProfile.dob, 'MM-dd-yyyy');
        this.usr.gender = this.userProfile.gender;
        this.usr.fatherName = this.userProfile.fathername;
        this.usr.motherName = this.userProfile.mothername;
        this.usr.fatherOccupation = this.userProfile.fatheroccupation;
        this.usr.caste = this.userProfile.caste;
        this.usr.subCaste = this.userProfile.subcaste;
        this.usr.religion = this.userProfile.religion;
        this.usr.nationality = this.userProfile.nationality;
        this.usr.status = this.userProfile.userstatus
        this.usr.loginid = this.userProfile.loginid
      }
    })
  }
  editAcademic() {
    this.showSubmit=false;
    this.us.getUserAcademicDetailsBysession().subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.acadedetails = data.data[0];
        this.acade.userId = this.acadedetails.userid;
        this.selecteduser.userid = this.acade.userId;
        this.acade.experience = this.acadedetails.experience;
        this.acade.sscPercentage = this.acadedetails.sscpercentage;
        this.acade.interPercentage = this.acadedetails.interpercentage;
        this.acade.ugPercentage = this.acadedetails.ugpercentage;
        this.acade.pgPercentage = this.acadedetails.pgpercentage;
        this.acade.otherPercentage = this.acadedetails.otherpercentage;
      }
    })
  }
  viewAcademicDetails() {
    this.us.getUserAcademicDetailsBysession().subscribe((data) => {
      this.acadedetails = data.data[0];
    })
  }
}
