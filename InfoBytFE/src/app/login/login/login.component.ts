import { Component, OnInit, ViewChild } from '@angular/core';
import { LoginService } from "../../service/login.service";
import { Router } from '@angular/router';
import { Login } from '../../modal/Login/Login';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageService } from 'primeng/api';
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { GoogleLoginProvider, FacebookLoginProvider, LinkedInLoginProvider } from 'angularx-social-login';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  SocialUser: SocialUser;
  msg: string;
  show: boolean;
  constructor(private as: AuthService,private toastr: ToastrService, private ls: LoginService, private router: Router, private spinner: NgxSpinnerService, private ms: MessageService) {
    this.show=false;
   }
   @ViewChild("loginform") form: any;
   Login(data: any) {
    {
      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide();
      }, 1200);
    }
    let lgn = new Login();
    lgn.loginId = data.loginId;
    lgn.password = data.password;
    if (lgn.loginId == null || lgn.password == '') {
      this.addSingle();
    } else if (lgn.loginId.length < 5 || lgn.password.length < 6) {
      this.addSingle();
    } else {
      this.ls.checkLogin(lgn).subscribe((data) => {
        console.log(data)
        if (data.result && data.data!="NDF") {
          sessionStorage.setItem("user", data.data.password);
          if (data.data.rolename == 'Support' || data.data.rolename == 'SalesSupport') {
            this.ls.addSupportUser(data.data.id, data.data.rolename, 'Login').subscribe((chat) => {
             console.log('chat')
             console.log(chat)
              if (chat.message == 'Added to conversation table') {
                this.router.navigate(['login/Chat']);
                this.toastr.success('Logged In');
              }
              else {
                sessionStorage.setItem('assignedUserId', chat[0].userid)
                this.router.navigate(['login/Chat']);
                this.toastr.success('Logged In');
              }
            }, (err) => {
              this.toastr.error(err.error.message)
            });
          } else {
            this.router.navigate(['login/Welcome']);
            this.toastr.success('Logged In');
          }
        }
        else {
          this.msg = data.message;
          this.toastr.error(data.message)
        }
      }, (err) => {
        this.toastr.error(err.error.message)
      });
    }
  }

  signInWithGoogle(): void {
    alert(GoogleLoginProvider.PROVIDER_ID)
    this.as.signIn(GoogleLoginProvider.PROVIDER_ID);
    this.as.authState.subscribe((user) => {
      this.SocialUser = user;
      let lgn = new Login();
      lgn.loginId = user.id;
      this.ls.checkSocialLogin(lgn).subscribe((data) => {
        if (data.result && data.data != "NDF") {
          sessionStorage.setItem("user", data.data.password);
          if (data.data.rolename == 'Support' || data.data.rolename == 'SalesSupport') {
            this.ls.addSupportUser(data.data.id, data.data.rolename, 'Login').subscribe((chat) => {
              console.log('chat')
              console.log(chat)
              if (chat.message == 'Added to conversation table') {
                this.router.navigate(['login/Chat']);
                this.toastr.success('Logged In');
              }
              else {
                sessionStorage.setItem('assignedUserId', chat[0].userid)
                this.router.navigate(['login/Chat']);
                this.toastr.success('Logged In');
              }
            }, (err) => {
              this.toastr.error(err.error.message)
            });
          } else {
            this.router.navigate(['login/Welcome']);
            this.toastr.success('Logged In');
          }
        }
        else {
          this.msg = data.message;
          this.toastr.error(data.message)
        }
      })
    });
  }
  signInWithFB(): void {
    alert(FacebookLoginProvider.PROVIDER_ID)
    this.as.signIn(FacebookLoginProvider.PROVIDER_ID);
  }
  signInWithLinkedIn(): void {
    alert(LinkedInLoginProvider.PROVIDER_ID)
    this.as.signIn(LinkedInLoginProvider.PROVIDER_ID);
  }
  ngOnInit() {
    this.as.authState.subscribe((user) => {
      this.SocialUser = user;
      alert(JSON.stringify(user))
    });
    {
      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide();
      }, 1200);
    }
  }
  addSingle() {
    this.ms.clear('emsg');
    this.ms.add({ key: 'emsg', severity: 'error', summary: 'Please enter valid credentials' });
  }
  clear() {
    this.ms.clear();
  }
  showPassword() {
    this.show = !this.show;
}
}