import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RouterModule, Routes } from "@angular/router";
import { LoginService } from '../service/login.service';
import { FormsModule } from '@angular/forms';
import { DefaultComponent } from './default/default.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { DataTablesModule } from 'angular-datatables';
import { SignupComponent } from './signup/signup.component';
import { MatStepperModule, MatIconModule, MatInputModule, MatButtonModule } from '@angular/material';
import { BsDatepickerModule } from "ngx-bootstrap";
import { TypeaheadModule } from 'ngx-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';
import { TestimonialComponent } from './testimonial/testimonial.component';
import { OrganizationModule } from '../organization/organization.module';
import { UserModule } from '../user/user.module';
import { BatchModule } from '../batch/batch.module';
import { ChatComponent } from './chat/chat.component';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { PasswordModule } from 'primeng/password';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatTooltipModule,
  MatTreeModule,
}
  from '@angular/material';
import { LoginRoutingModule } from './login-routing.module';
@NgModule({
  imports: [
    LoginRoutingModule,
    MatTableModule, MatTableModule,
    MatSlideToggleModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,CommonModule,
    MatTreeModule, MatFormFieldModule,
    ToastrModule.forRoot(),
    FormsModule, DataTablesModule, TypeaheadModule.forRoot(),
    MatStepperModule, NgxSpinnerModule, MatInputModule, MatIconModule, MatButtonModule, BsDatepickerModule.forRoot(), ToastModule,
    OrganizationModule, UserModule, BatchModule, DropdownModule, PasswordModule, AutoCompleteModule, InputTextareaModule, CalendarModule
  ], exports: [
    RouterModule
  ], providers: [
    LoginService, MessageService
  ],
  declarations: [LoginComponent, DefaultComponent, ForgotPasswordComponent, TestimonialComponent, SignupComponent, ChatComponent]
})
export class LoginModule { }