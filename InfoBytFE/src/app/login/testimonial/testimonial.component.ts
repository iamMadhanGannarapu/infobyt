import { Component, OnInit, ViewChild } from '@angular/core';
import { Testimonial } from '../../modal/User/testimonial';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../service/user.service';
import { User } from '../../modal/User/User';
import { ImageCompressService, ResizeOptions, ImageUtilityService, IImage, SourceImage } from  'ng2-image-compress';
@Component({
  selector: 'app-testimonial',
  templateUrl: './testimonial.component.html',
  styleUrls: ['./testimonial.component.scss']
})
export class TestimonialComponent implements OnInit {
  testimonial: Testimonial;
  user: User;
  List: any;
  data: any;
  image = { 'URL': '', 'valid': false };
  images: File;
  x='dasd';
  constructor(private imgCompressService: ImageCompressService,private toastr: ToastrService, private us: UserService) {
    this.testimonial = new Testimonial()
  }
  @ViewChild("addTestimonialForm") form: any;
  ngOnInit() {
    this.us.getAllTestimonial().subscribe((data) => {
      this.List = data;
    })
  }
  addTestimonial(frm: any) {
    this.us.saveTestimonial(this.testimonial).subscribe((data) => {
      this.testimonial = data
      this.toastr.success("Posted Successfully")
    })
  }
  
  fileChangeEvent(fileInput: any) {
    var fileName = fileInput.target.value;
    var idxDot = fileName.lastIndexOf(".") + 1;
    var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
    if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
      ImageCompressService.filesToCompressedImageSource(fileInput.target.files).then(observableImages => {
        observableImages.subscribe((image) => {
            this.image.URL = image.compressedImage.imageDataUrl;
            this.testimonial.image = image.compressedImage.imageDataUrl;
            this.testimonial.image = this.testimonial.image.replace("data:image/gif;base64,", "");
            this.testimonial.image = this.testimonial.image.replace("data:image/jpeg;base64,", "");
            this.testimonial.image = this.testimonial.image.replace("data:image/jpg;base64,", "");
            this.testimonial.image = this.testimonial.image.replace("data:image/png;base64,", "");
        }, (error) => {
          this.toastr.error("Error while converting");
        });
      });
    }else{
      alert("Only jpg/jpeg and png files are allowed!");
     
    }
  }

}