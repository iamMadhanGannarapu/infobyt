import { Component, OnInit, ViewChild } from '@angular/core';
import { LoginService } from '../../service/login.service'
import { ToastrService } from 'ngx-toastr';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  forgotLoginId: any;
  forgotEmailId: any;
  navi: string;
  constructor(private toastr: ToastrService,private router:Router,private login: LoginService, private ms: MessageService) {
    this.navi=router.url
   }
   @ViewChild("forgotform") form: any;
  Login(frm: any) {
    this.forgotLoginId = frm.value.loginId;
    this.forgotEmailId = frm.value.email
    if(this.forgotLoginId==null && this.forgotEmailId==''){
      this.addSingle();
    }else if((this.forgotLoginId==''||this.forgotLoginId==null || this.forgotLoginId== undefined)&& this.forgotEmailId.length>6){
      this.login.getPassword1(this.forgotEmailId).subscribe((data) => {
        this.toastr.success('Username send to your mail');
      })
    }else if(this.forgotLoginId.length<6 || this.forgotEmailId.length<6 ){
      this.addSingle();
    }
    else{
    this.login.getPassword(this.forgotLoginId, this.forgotEmailId).subscribe((data) => {
      this.toastr.success('Password send to your mail');
    })
  }
  }
  ngOnInit() {
  }
  addSingle() {
    this.ms.clear('emsg');
    this.ms.add({key: 'emsg',severity: 'error', summary: 'Please enter valid credentials'});
  }
  clear() {
    this.ms.clear();
  }
}