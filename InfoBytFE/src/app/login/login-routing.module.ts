import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { DefaultComponent } from './default/default.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { TestimonialComponent } from './testimonial/testimonial.component';
import { ChatComponent } from './chat/chat.component';
import { RouteGuardGuard, CanDeactivateGuard } from '../service/route-guard.guard';
var routes: Routes = [
    { path: 'User/SignUp', component: SignupComponent,canDeactivate:[CanDeactivateGuard] },
    { path: 'ViewProfile', component: SignupComponent,canDeactivate:[CanDeactivateGuard] },
    { path: 'Login', component: LoginComponent},
    { path: 'Welcome', component: DefaultComponent },
    { path: 'forgotpassword', component: ForgotPasswordComponent,canDeactivate:[CanDeactivateGuard] },
    { path: 'forgotusername', component: ForgotPasswordComponent,canDeactivate:[CanDeactivateGuard] },
    { path: 'Testimonial', component: TestimonialComponent,canDeactivate:[CanDeactivateGuard] },
    { path: 'Chat', component: ChatComponent, canActivate: [RouteGuardGuard] },
]
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [RouteGuardGuard,CanDeactivateGuard]
})
export class LoginRoutingModule { }
