import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HolidayMasterComponent } from './holiday-master/holiday-master.component';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from "@angular/router";
import { DataTablesModule } from 'angular-datatables';
import { UsermappingComponent } from './usermapping/usermapping.component';
import { AttendanceComponent } from './attendance/attendance.component';
import { SurvelinceComponent } from './survelince/survelince.component';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import { LateloginsComponent } from './latelogins/latelogins.component';
import {FullCalendarModule} from 'primeng/fullcalendar';
import {CalendarModule} from 'primeng/calendar';

import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatStepperModule,
  MatProgressBarModule ,
  MatProgressSpinnerModule,
  MatSliderModule ,
  MatSlideToggleModule ,
  MatTooltipModule ,
  MatTreeModule,
} from '@angular/material';
import { AttendanceRoutingModule } from './attendance-routing.module';
import { LeaveDetailsComponent } from './leave-details/leave-details.component';
@NgModule({
  imports: [
    AttendanceRoutingModule,
    MatTableModule, MatTableModule,
    MatSlideToggleModule,
    CdkTableModule,
    CdkTreeModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,CalendarModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,MatFormFieldModule,FullCalendarModule,
    CommonModule, 
    ToastrModule.forRoot(),
    FormsModule, DataTablesModule, BsDatepickerModule.forRoot()
  ], exports: [
    RouterModule
  ],providers:[],
  declarations: [HolidayMasterComponent, UsermappingComponent, AttendanceComponent, SurvelinceComponent, LateloginsComponent,LeaveDetailsComponent]
})
export class AttendanceModule { }
