import { Component, OnInit } from '@angular/core';
import { AttendanceService } from '../../service/attendance.service';
import { UserService } from 'src/app/service/user.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-latelogins',
  templateUrl: './latelogins.component.html',
  styleUrls: ['./latelogins.component.scss']
})
export class LateloginsComponent implements OnInit {
  public latelogins: AttendanceService;
  latelogin: any;
  userProfile: any
  rolename: any;
  attendanceList: any
  userId: any
  month: any
  date1: any

  constructor(private as: AttendanceService, private toastr: ToastrService) { }
  ngOnInit() {

  }
  getAllUsersByBranch() {
    this.as.getLateOfTraineesByStaff().subscribe((data) => {
      if (data.result && data.data != 'NDF')
        this.latelogin = data.data
    })
  }
  viewLateLoginsofUser(userId: any) {
    this.date1 = new Date(this.date1)
    if (parseInt(this.date1.getMonth() + 1) < 10)
      this.month = this.date1.getFullYear() + '-0' + parseInt(this.date1.getMonth() + 1)
    else
      this.month = this.date1.getFullYear() + '-' + parseInt(this.date1.getMonth() + 1)
    this.userId = userId;
    this.as.getLateLoginsOfUser(userId, this.month).subscribe((data) => {
      if (data.result && data.data != 'NDF')
        this.attendanceList = data.data
    })
  }


  editLateLoginStatus(usrcode: any, date: any, status: any) {
    this.as.updateLateLoginStatus(usrcode, date, status).subscribe((data) => {
      if (data.result)
        this.toastr.success(data.message)
      else
        this.toastr.success(data.message)
      this.viewLateLoginsofUser(this.userId)
    })
  }
}
