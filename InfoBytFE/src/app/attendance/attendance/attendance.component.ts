import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../service/user.service';
import { Attendance } from '../../modal/Attendance/Attendance';
import { AttendanceService } from '../../service/attendance.service';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { OrganizationService } from '../../service/organization.service';
import { BatchService } from 'src/app/service/batch.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { Router, CanActivateChild } from '@angular/router';
import { LoginService } from 'src/app/service/login.service';
@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.scss']
})
export class AttendanceComponent implements OnInit, CanActivateChild {
  attendance: Attendance
  list: any
  staffList: any
  studentListCT: any
  studentList: any
  manualstudentList:any
  manualstaffList:any
  showAttendanceTable: boolean = false
  pipe = new DatePipe('en-US');
  frmDate: any
  tDate: any
  userList: any;
  hidestaff: boolean
  userlist: any
  branchList: any;
  branchIdlist: any;
  organizationbranchList: any;
  maxDate: Date;
  attendanceList: any;
  attendanceStaffList;
  branchId: any
  rolename: any;
  batchList: any
  batchId: any;
  Datee: any;
  InTime: string
  OutTime: any;
  showStudent: boolean = false;
  showStaff: boolean = false
  read: any;
  write: any;
  attendanceSource:any
  constructor(private router: Router, private ls: LoginService, private toastr: ToastrService, private as: AttendanceService, private us: UserService, private os: OrganizationService, private bs: BatchService) {
    this.attendance = new Attendance()
    this.maxDate = new Date();
    this.Datee = new Date();
    this.maxDate.setDate(this.maxDate.getDate());
    this.InTime = "09:00:00"
    this.OutTime = "18:00:00"
  }
  @ViewChild("updateAttendanceForm") form: any;
  @ViewChild("getAttendanceForm") form1: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  




addAttendance() {
  this.as.saveAttendance(this.manualstudentList, this.Datee, this.InTime, this.OutTime).subscribe((data) => {
    if (data.result && data.data!="NDF") {
      this.toastr.success(data.message);
    }
    else
    this.toastr.error(data.message);
  })
}
addStaffAttendance() {
  this.as.saveAttendanceForStaff(this.manualstaffList, this.Datee, this.InTime, this.OutTime).subscribe((data) => {
    if (data.data) {
      this.toastr.success(data.message);
    }
    else
    this.toastr.error('Attendance Has Been Added Successfully');
  })
}

  getallbyBranch(frm: any) {
    this.hidestaff = false
    this.frmDate = new Date(frm._bsValue[0]);
    this.tDate = new Date(frm._bsValue[1]);
    this.attendance.fromDate = this.pipe.transform(this.frmDate, 'MM-dd-yyyy');
    this.attendance.toDate = this.pipe.transform(this.tDate, 'MM-dd-yyyy');
    this.as.getattendancebybranchadmin(this.branchId, this.attendance).subscribe((data) => {
      if (data.result && data.data != 'NDF')
        this.branchIdlist = data.data
    })
  }
  forAccountantStaff(frm: any) {
    this.frmDate = new Date(frm._bsValue[0]);
    this.tDate = new Date(frm._bsValue[1]);
    this.attendance.fromDate = this.pipe.transform(this.frmDate, 'MM-dd-yyyy');
    this.attendance.toDate = this.pipe.transform(this.tDate, 'MM-dd-yyyy');
    this.showAttendanceTable = true;
    this.as.getAttendanceOfStaffByAccountant(this.attendance).subscribe((data) => {
      this.staffList = data.data
      this.userList = null;
      this.studentList = null;
    });
  }
  viewAttendanceofUSer(userid: any) {
    this.as.attendancebyuserId(userid).subscribe((data) => {
      this.userlist = data.data
    })
  }
  forAccountantStudent(frm: any) {
    this.hidestaff = false
    this.frmDate = new Date(frm._bsValue[0]);
    this.tDate = new Date(frm._bsValue[1]);
    this.attendance.fromDate = this.pipe.transform(this.frmDate, 'MM-dd-yyyy');
    this.attendance.toDate = this.pipe.transform(this.tDate, 'MM-dd-yyyy');
    this.showAttendanceTable = true;
    this.as.getAttendanceOfTraineesByAccountant(this.attendance).subscribe((data) => {
      this.studentList = data.data
      this.staffList = null;
      this.userList = null;
    });
  }
  forBRanchAdmins(frm: any) {
    this.frmDate = new Date(frm._bsValue[0]);
    this.tDate = new Date(frm._bsValue[1]);
    this.attendance.fromDate = this.pipe.transform(this.frmDate, 'MM-dd-yyyy');
    this.attendance.toDate = this.pipe.transform(this.tDate, 'MM-dd-yyyy');
    this.showAttendanceTable = true;
    this.as.getAttendanceOfBranchadminBySchoolAdmin(this.attendance).subscribe((data) => {
      if (data.result && data.data != 'NDF')
        this.branchList = data.data
    });
    this.ngOnInit()
  }
  forTodays() {
    this.as.getTodaysAttendance(this.attendance).subscribe((data) => {
      if (data.result && data.data != 'NDF')
        this.branchList = data.data
    })
  }
  getAllSchoolBranchs() {
    this.os.getAllBranch().subscribe((data) => {
      this.organizationbranchList = data.data;
      for (let i = 0; i < this.organizationbranchList.length; i++) {
        if (this.organizationbranchList[i].branchstatus == 'Inactive') {
          this.organizationbranchList[i].branchstatus = false
        } else {
          this.organizationbranchList[i].branchstatus = true
        }
      }
    });
  }
  forStaff(frm: any) {
    this.frmDate = new Date(frm._bsValue[0]);
    this.tDate = new Date(frm._bsValue[1]);
    this.attendance.fromDate = this.pipe.transform(this.frmDate, 'MM-dd-yyyy');
    this.attendance.toDate = this.pipe.transform(this.tDate, 'MM-dd-yyyy');
    this.showAttendanceTable = true;
    this.as.getAttendanceOfTraineesByStaff(this.attendance).subscribe((data) => {
      if (data.result && data.data != 'NDF')
        this.userList = null;
    });
  }
  btnEdit(Id: number) {
    this.as.getAttendanceById(Id).subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.list = data.data
        this.attendance.attendanceLogId = data.data[0].attendancelogid;
        this.attendance.attendanceDate = data.data[0].attendancedate;
        this.attendance.deviceId = data.data[0].usrdeviceid;
        this.attendance.fromDate = data.data[0].intime;
        this.attendance.toDate = data.data[0].outtime;
        this.attendance.statusCode = data.data[0].statuscode
      }
    });
  }
  updateAttendance() {
    this.as.updateAttendance(this.attendance).subscribe((data) => {
      if (data.result)
        this.toastr.success(data.message);
      else
        this.toastr.error(data.message);
    })
  }
  searchAttendance(frm: any) {
    this.frmDate = new Date(frm._bsValue[0]);
    this.tDate = new Date(frm._bsValue[1]);
    this.attendance.fromDate = this.pipe.transform(this.frmDate, 'MM-dd-yyyy');
    this.attendance.toDate = this.pipe.transform(this.tDate, 'MM-dd-yyyy');
    this.showAttendanceTable = true;
    this.as.getAllAttendance(this.attendance).subscribe((data) => {
      this.list = data.data
    });
    this.ngOnInit()
  }
  ngOnInit() {
    $(document).ready(function(){
      $("#datahide").hide();
      $("#showdata").click(function(){
         $("#viewallatt").show()
         $("#showdata").hide()
         $("#datahide").show();
      });
      $("#datahide").click(function(){
        $("#viewallatt").hide()
        $("#showdata").show()
        $("#datahide").hide();
     });
  });

    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename = data.data[0].rolename
        if (this.rolename == 'OrganizationAdmin') {
          this.getBranchAtttendanceById()
        }
      })
    }
    this.getAttendanceOfUser1()
    this.getAllSchoolBranchs()
    this.getAllBatches()
  }
  getAttendanceOfUser1() {
    this.as.getAttendanceOfUser(this.attendance).subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.userList = data.data
        this.staffList = null;
        this.studentList = null;
        this.studentListCT = null;
      }
    })
  }
  getBranchAtttendanceById() {
    this.as.getAttendanceOfbranchId(this.attendance).subscribe((data) => {
      if (data.result && data.data != 'NDF')
        this.branchIdlist = data.data
    })
  }
  getAllBatches() {
    this.bs.getAllBatch()
      .subscribe((res) => {
        this.batchList = res;
      },
        err => {
          this.toastr.error(err.error.message);
        });
  }



  getAllStudentsByBatch() {
    alert(JSON.stringify(this.batchId))
    this.as.getAllTraineeListByBatch(this.batchId).subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
        this.manualstudentList=data.data
        console.log(this.manualstudentList)
      for (var i = 0; i < this.manualstudentList.length; i++){
        this.manualstudentList[i].checkBoxStatus = true;

        this.attendanceSource = new MatTableDataSource(data)
        this.attendanceSource.sort = this.sort
        this.attendanceSource.paginator = this.paginator
        console.log((this.attendanceSource.data))
        this.showStudent=true
        this.showStaff=false
      }
      this.showStudent = true
      this.showStaff = false
    }
  })
  }
  getAllStaff() {
    this.as.getAllStaffListByBranch().subscribe((data) => {
      this.manualstaffList=data.data
      for (var i = 0; i < this.manualstaffList.length; i++)
        this.manualstaffList[i].checkBoxStatus = true;
        this.attendanceStaffList = new MatTableDataSource(data)
        this.attendanceStaffList.sort = this.sort
        this.attendanceStaffList.paginator = this.paginator
        console.log((this.attendanceStaffList.data))
        this.showStudent=false
        this.showStaff=true
    },
      err => {
        this.toastr.error(err.error.message);
      })
  }
  getexcel() {
    this.as.getAllStaffListByBranch().subscribe((data) => {
      this.as.exportAsExcelFile(data, 'Attendance');
    })
  }
  getPermissions() {
    this.ls.checkRightPermissions('batch').subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.read = data.data[0].read;
        this.write = data.data[0].write;
        this.canActivateChild(this.read)
      }
    })
  }

  canActivateChild(x: any): boolean {
    if (x == true) {
      return true;
    } else {
      this.router.navigate(['404']);
      return false;
    }
  }
}