import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsermappingComponent } from './usermapping/usermapping.component';
import { SurvelinceComponent } from './survelince/survelince.component';
import { HolidayMasterComponent } from './holiday-master/holiday-master.component';
import { AttendanceComponent } from './attendance/attendance.component';
import { LateloginsComponent } from './latelogins/latelogins.component';
import { RouteGuardGuard, CanDeactivateGuard } from '../service/route-guard.guard';
import { LeaveDetailsComponent } from './leave-details/leave-details.component';
import { CanDeactivate } from '@angular/router/src/utils/preactivation';

const routes: Routes = [
    { path: 'Holiday-master', component: HolidayMasterComponent ,canDeactivate:[CanDeactivateGuard],canActivate:[RouteGuardGuard],canActivateChild:[HolidayMasterComponent]},
    { path: 'Usermapping', component: UsermappingComponent,canActivate:[RouteGuardGuard],canActivateChild:[UsermappingComponent],canDeactivate:[CanDeactivateGuard]},
    { path: 'Attendance', component: AttendanceComponent ,canActivate:[RouteGuardGuard],canActivateChild:[AttendanceComponent]},
    { path: 'Survelince', component: SurvelinceComponent ,canActivate:[RouteGuardGuard],canActivateChild:[SurvelinceComponent],canDeactivate:[CanDeactivateGuard]},
    { path: 'Latelogins', component: LateloginsComponent ,canActivate:[RouteGuardGuard],canActivateChild:[LateloginsComponent]},
    {path:'Leave-details',component:LeaveDetailsComponent,canDeactivate:[CanDeactivateGuard],canActivate:[RouteGuardGuard],canActivateChild:[LeaveDetailsComponent]},
  ]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers:[RouteGuardGuard,HolidayMasterComponent,UsermappingComponent,SurvelinceComponent,HolidayMasterComponent,AttendanceComponent
    ,LateloginsComponent,LeaveDetailsComponent]
})
export class AttendanceRoutingModule { }
