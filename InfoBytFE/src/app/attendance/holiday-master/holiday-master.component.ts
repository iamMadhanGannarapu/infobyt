import { Component, OnInit, ViewChild } from '@angular/core';
import { Holiday } from '../../modal/Attendance/Holiday';
import { AttendanceService } from "../../service/attendance.service";
import { OrganizationService } from '../../service/organization.service';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../service/user.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { BatchService } from '../../service/batch.service';
import { DatePipe } from '@angular/common';
import { DashboardService } from "src/app/service/dashboard.service";
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-holiday-master',
  templateUrl: './holiday-master.component.html',
  styleUrls: ['./holiday-master.component.scss']
})
export class HolidayMasterComponent implements OnInit, CanActivateChild {
  dataSource;
  displayedColumns = ['HolidayDate', 'HolidayEndDate', 'Occasion', 'branch', 'edit', 'delete', 'Excel']
  holiday: Holiday;
  branchList: any;
  holidayList: any[] = [];
  pipe = new DatePipe('en-US');
  maxDate = new Date();
  minDate = new Date();
  colorTheme = 'theme-dark-blue';
  color = "success";
  rolename: any;
  bsConfig: Partial<BsDatepickerConfig>;
  options: any
  frmDate: any;
  read: any;
  write: any
  toDate: any;
  holidaydata: any;
  constructor(private router: Router, private ls: LoginService, private toastr: ToastrService, private bs: BatchService, private as: AttendanceService, private ds: DashboardService, private os: OrganizationService, private userService: UserService) {
    this.holiday = new Holiday()
    this.minDate.setDate(this.minDate.getDate() + 1);
    this.maxDate.setDate(this.maxDate.getDate() + 3650);
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("addholidayForm") form: any;
  saveHoliday() {
    this.frmDate = new Date(this.holiday.leavesDate[0]);
    this.toDate = new Date(this.holiday.leavesDate[1]);
    this.holiday.holidayDate = this.pipe.transform(this.frmDate, 'MM-dd-yyyy');
    this.holiday.holidayEndDate = this.pipe.transform(this.toDate, 'MM-dd-yyyy');
    this.as.saveHoliday(this.holiday).subscribe((data) => {
      if (data.result) {
        this.toastr.success('Holiday Details Has Been Added Successfully ');
        this.form.reset();
      }
      else
        this.toastr.error(data.message);
      this.getAllHolidays();
      this.getAllHolidayCalender();
    });
    this.ds.saveHolidayNotifications().subscribe((data) => {
    })
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
  getIds() {
    this.os.getBranchById().subscribe((data) => {
      this.branchList = data.data;
    })
  }
  editHoliday(Id: number) {
    this.getIds()
    this.as.getHolidaysById(Id).subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.holidayList = data.data;
        this.branchList = data.data;
        this.holiday.Id = data.data[0].id;
        this.holiday.occasion = data[0].occation;
        this.holiday.branchId = data[0].branchid;
        this.holiday.holidayDate = this.pipe.transform(data.data[0].holidaydate, 'MM-dd-yyyy');
        this.holiday.holidayEndDate = this.pipe.transform(data.data[0].holidayenddate, 'MM-dd-yyyy')
      }
    })
  }
  updateHoliday() {
    this.frmDate = new Date(this.holiday.leavesDate[0]);
    this.toDate = new Date(this.holiday.leavesDate[1]);
    this.holiday.holidayDate = this.pipe.transform(this.frmDate, 'MM-dd-yyyy');
    this.holiday.holidayEndDate = this.pipe.transform(this.toDate, 'MM-dd-yyyy');
    this.as.updateHoliday(this.holiday).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.form.reset();
      }
      else
        this.toastr.error(data.message);
      this.getAllHolidays();
      this.getAllHolidayCalender();
    });
  }
  getPermissions() {
    this.ls.checkRightPermissions('holiday-master').subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.read = data.data[0].read;
        this.write = data.data[0].write;
        this.canActivateChild(this.read)
      }
    })
  }
  ngOnInit() {
    $(document).ready(function(){
      $("#datahide").hide();
      $("#showdata").click(function(){
         $("#viewholi").show()
         $("#showdata").hide()
         $("#datahide").show();
      });
      $("#datahide").click(function(){
        $("#viewholi").hide()
        $("#showdata").show()
        $("#datahide").hide();
     });
  });
    this.getAllHolidays();
    this.getPermissions();
    this.getAllHolidayCalender();
    if (sessionStorage.getItem('user') != null) {
      this.userService.getUserbySession().subscribe((data) => {
        this.rolename = data.data[0].rolename;
      })
    }
    this.options = {
      displayEventTime: false,
      header: {
        left: 'prev,next',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      }
    }
    this.holiday.branchId = 0;
  }
  getAllHolidays() {
    this.as.getAllHolidayDetails().subscribe((data) => {
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
    });
  }
  getexcel() {
    this.as.exportAsExcelFile(this.dataSource, 'Holiday Information');
  }
  deleteHoliday(Id: number) {
    this.as.deleteHoliday(Id).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
      }
      else
        this.toastr.error(data.message);
      this.getAllHolidays();
      this.getAllHolidayCalender();
    });
  }
  getAllHolidayCalender() {
    this.as.getAllHolidayCal().subscribe((data) => {
      this.holidayList = data.data
    });
  }
  canActivateChild(x: any): boolean {
    if (x == true) {
      return true;
    } else {
      this.router.navigate(['404']);
      return false;
    }
  }
}