import { Component, OnInit, ViewChild } from '@angular/core';
import { LeaveDetails } from '../../modal/Salary/LeaveDetails'
import { Salaryservice } from "../../service/salary.service";
import { UserService } from '../../service/user.service';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { LeaveMaster } from '../../modal/batch/leavemaster';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { HalfLoginRequest } from 'src/app/modal/Salary/HalfLoginrequest';
import { DashboardService } from "src/app/service/dashboard.service";
import { LoginService } from 'src/app/service/login.service';
import { Router, CanActivateChild } from '@angular/router';
@Component({
  selector: 'app-leave-details',
  templateUrl: './leave-details.component.html',
  styleUrls: ['./leave-details.component.scss']
})
export class LeaveDetailsComponent implements OnInit, CanActivateChild {
  leavetype: LeaveMaster;
  halfLoginRequestObj: HalfLoginRequest;
  displayedColumns = ['UserName', 'FromDate', 'ToDate', 'type', 'Status', 'Description', 'ApprovedBy', 'Excel']
  dataSource;
  HalfLoginSource;
  HalfLoginColumns = ['UserName', 'Requesttime', 'Description', 'Status']
  frmDate: any
  tDate: any
  pipe = new DatePipe('en-US');
  ld: LeaveDetails
  updateLeaveList: any
  typeSource;
  typeColumns = ['type']
  typesAddObj: LeaveMaster;
  leaveTypeList: any
  userList: any
  minDate = new Date();
  colorTheme = 'theme-dark-blue';
  bsConfig: Partial<BsDatepickerConfig>;
  rolename: any;
  updaterequestList: any;
  leavesList: any;
  read: any;
  write: any;
  constructor(private router: Router, private ls: LoginService, private toastr: ToastrService, private ds: DashboardService, private ss: Salaryservice, private us: UserService) {
    this.ld = new LeaveDetails()
    this.halfLoginRequestObj = new HalfLoginRequest()
    this.minDate.setDate(this.minDate.getDate());
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    this.typesAddObj = new LeaveMaster();
    this.leavetype = new LeaveMaster();
  }
  @ViewChild("addLeaveForm") form: any;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  frmSaveSubmit(frm: any) {
    this.frmDate = new Date(frm._bsValue[0]);
    this.tDate = new Date(frm._bsValue[1]);
    this.ld.fromDate = this.pipe.transform(this.frmDate, 'MM-dd-yyyy');
    this.ld.toDate = this.pipe.transform(this.tDate, 'MM-dd-yyyy');
    this.ss.saveLeave(this.ld).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.getAllLeave();
        this.form.reset();
      }
    });
    this.ds.saveLevNotifications().subscribe((data) => {
    })
  }
  addLeaveTypes() {
    this.ss.saveTypes(this.typesAddObj).subscribe((data) => {
      if (data.result) {
        this.toastr.success(data.message);
        this.form.reset();
      }
      this.viewLeaveMaster()
    }
    )
  }
  viewLeaveMaster() {
    this.ss.getLeavemaster().subscribe((data) => {
      if (data.result && data.data != 'NDF') {
        this.leaveTypeList = data.data;
        this.typeSource = new MatTableDataSource(data.data)
        this.typeSource.sort = this.sort
        this.typeSource.paginator = this.paginator
      }
    })
  }
  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getIds() {
    this.us.getAllUsers().subscribe((data) => {
      this.userList = data.data
    })
  }
  getAllLeave() {
    this.ss.getAllLeaves().subscribe((data) => {
      if (data.result && data.data != 'NDF') {

        this.dataSource = new MatTableDataSource(data.data)
        this.dataSource.sort = this.sort
        this.dataSource.paginator = this.paginator
      }
    });
  }
  getexcel() {
    this.ss.exportAsExcelFile(this.dataSource.data, 'Concession');
  }
  getPermissions() {
    this.ls.checkRightPermissions('leave-details').subscribe((data) => {
      if (data.data != 'NDF') {
        this.read = data.data[0].read;
        this.write = data.data[0].write;
        this.canActivateChild(this.read)
      }
    })
  }
  canActivateChild(x: any): boolean {
    if (x == true) {
      return true;
    } else {
      this.router.navigate(['404']);
      return false;
    }
  }
  ngOnInit() {
    $(document).ready(function(){
      $("#datahide1").hide();
      $("#showdata1").click(function(){
         $("#viewlvtyp").show()
         $("#showdata1").hide()
         $("#datahide1").show();
      });
      $("#datahide1").click(function(){
        $("#viewlvtyp").hide()
        $("#showdata1").show()
        $("#datahide1").hide();
     });

     $("#datahide2").hide();
     $("#showdata2").click(function(){
      $("#viewalldetails").show()
      $("#showdata2").hide()
      $("#datahide2").show();
   });
   $("#datahide2").click(function(){
     $("#viewalldetails").hide()
     $("#showdata2").show()
     $("#datahide2").hide();
  });

  $("#datahide3").hide();
  $("#showdata3").click(function(){
    $("#HalfLogins").show()
    $("#showdata3").hide()
    $("#datahide3").show();
 });
 $("#datahide3").click(function(){
   $("#HalfLogins").hide()
   $("#showdata3").show()
   $("#datahide3").hide();
});
  });


    this.getPermissions();
    this.viewLeaveMaster();
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
        this.rolename = data.data[0].rolename;
      })
    }
    this.getAllLeave();
    this.viewLeaveMaster();
    this.getHalfLoginrequests();
  }
  saveRequest() {
    this.ss.saveHalfLeaveRequest(this.halfLoginRequestObj).subscribe((data) => {
      if (data.result) {
        this.toastr.success('Request Has Been Added Successfully ');
        this.getAllLeave();
        this.form.reset();
      }
    })
  }
  getHalfLoginrequests() {
    this.ss.getAllHalfLoginRequests().subscribe((data) => {
      if (data.data != 'NDF') {
        this.HalfLoginSource = new MatTableDataSource(data.data)
        this.HalfLoginSource.sort = this.sort
        this.HalfLoginSource.paginator = this.paginator;
      }
    })
  }
  acceptRequest(id: number, status: any) {
    this.ss.gethalfLoginRequestById(id).subscribe((data) => {
      this.updaterequestList = data.data;
      this.halfLoginRequestObj.Id = this.updaterequestList[0].id;
      this.halfLoginRequestObj.Status = status.value;
      this.ss.updateHalfLoginRequestDetails(this.halfLoginRequestObj).subscribe((data) => {
        this.ngOnInit();
      })
    })
  }
  acceptLeave(leaveId: any, status: any) {
    this.ss.getLeaveById(leaveId).subscribe((data) => {
      this.updateLeaveList = data.data;
      this.ld.Id = this.updateLeaveList[0].id;
      this.ld.userId = this.updateLeaveList[0].userid;
      this.ld.fromDate = this.updateLeaveList[0].fromdate;
      this.ld.toDate = this.updateLeaveList[0].todate;
      this.ld.description = this.updateLeaveList[0].description;
      this.ld.status = status.value;
      this.ld.approvedBy = this.updateLeaveList[0].approvedby;
      this.ss.updateLeaveDetails(this.ld).subscribe((data) => {
        this.ngOnInit();
      })
    })
  }
}
