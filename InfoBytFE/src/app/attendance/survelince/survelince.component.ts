import { Component, OnInit, ViewChild } from '@angular/core';
import { Survelince } from '../../modal/Attendance/Survelince';
import { BatchService } from '../../service/batch.service';
import { ToastrService } from 'ngx-toastr';
import { SurvelinceService } from '../../service/survelince.service';
import * as flv from 'flv.js'
import { UserService } from '../../service/user.service';
@Component({
  selector: 'app-survelince',
  templateUrl: './survelince.component.html',
  styleUrls: ['./survelince.component.scss']
})
export class SurvelinceComponent implements OnInit {
  Cams: any[] = [
    { cam: "Select U r Cam ", url: "" },
    { cam: "Camera-01", url: "rtsp://admin:admin1122@192.168.0.174/MPEG-4/ch5/main/av_stream" },
    { cam: "Camera-02", url: "rtsp://admin:admin1122@192.168.0.174/MPEG-4/ch4/main/av_stream" },
    { cam: "Camera-03", url: "rtsp://admin:admin1122@192.168.0.174/MPEG-4/ch3/main/av_stream" },
    { cam: "Camera-04", url: "rtsp://admin:admin1122@192.168.0.174/MPEG-4/ch1/main/av_stream" }]
  pl: any
  cam: Survelince
  List: any[] = [];
  batchList: any[] = [];
  userProfile: any;
  showAddForm: Boolean = false;
  listLength: number = 0;
  rolename: any;
  myUrl:any;
  constructor(private toastr:ToastrService,private camser: SurvelinceService,private us: UserService, private bs: BatchService) {
    this.cam = new Survelince();
    this.myUrl=us.url;
  }
  @ViewChild("addForm") form: any;
  addCamera() {
    this.getIds();
    this.camser.addCameradetails(this.cam).subscribe((data) => {
      if (data) {
        this.toastr.success('Camera Has Been Synchronized' );
        this.getAllSurveliance();
      }
    });
  }
  getIds() {
    this.bs.getAllBatch().subscribe((res) => {
      this.batchList = res;
    });
  }
  btnAddForm() {
    this.showAddForm = true;
    this.getIds();
  }
  getAllSurveliance() {
    this.camser.getAllSurvelinceDetails().subscribe((data) => {
      this.List = data.data;
    })
  }
  ngOnInit() {
    this.getAllSurveliance();
    this.getIds();
    if (sessionStorage.getItem('user') != null) {
      this.us.getUserbySession().subscribe((data) => {
           this.userProfile = data.data[0];
          this.rolename=this.userProfile.rolename;
      })
    }
  }
  btnPlayClick1(vd: any ){
    vd.play()
}
btnPlayClick2(vd: any ){
  var camObj =  this.Cams[2];
          for(let i =0; i<camObj.url.length; i++){
      camObj.url = camObj.url.replace('/','-')
  }
  this.pl = flv.default.createPlayer({ type: 'flv', url: 'http://localhost:5600/stream/'+camObj.url })
  this.pl.attachMediaElement(vd)
  this.pl.load();
  this.pl.play();
}
btnPlayClick3(vd: any ){
  var camObj =  this.Cams[3];
          for(let i =0; i<camObj.url.length; i++){
      camObj.url = camObj.url.replace('/','-')
  }
  this.pl = flv.default.createPlayer({ type: 'flv', url: 'http://localhost:5600/stream/'+camObj.url })
  this.pl.attachMediaElement(vd)
  this.pl.load();
  this.pl.play();
}
btnPlayClick4(vd: any ){
  var camObj =  this.Cams[4];
          for(let i =0; i<camObj.url.length; i++){
      camObj.url = camObj.url.replace('/','-')
  }
  this.pl = flv.default.createPlayer({ type: 'flv', url: 'http://localhost:5600/stream/'+camObj.url })
  this.pl.attachMediaElement(vd)
  this.pl.load();
  this.pl.play();
}
}
