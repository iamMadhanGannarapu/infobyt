
import { Component, OnInit ,ViewChild} from '@angular/core';
import { AttendanceService } from '../../service/attendance.service';
import { ToastrService } from 'ngx-toastr';
import { BranchMapping, UserMapping } from '../../modal/User/branchmapping';
import { OrganizationService } from '../../service/organization.service';
import { Router,CanActivateChild } from '@angular/router';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { LoginService } from 'src/app/service/login.service';
@Component({
  selector: 'app-usermapping',
  templateUrl: './usermapping.component.html',
  styleUrls: ['./usermapping.component.scss']
})
export class UsermappingComponent implements OnInit ,CanActivateChild{
  addBrchMpgObj: BranchMapping
  dataSource;
  displayedColumns = ['name', 'deviceuserid', 'appuserid','Excel']
  displayedColumns1 = ['userdeviceid', 'place','Excel']
  dataSource1;
  addusermapping: UserMapping;
  list: any
  deviceuserlist: any;
  getdevicelist: any;
  branchList: any;
  usermapList: any
  
  showbranch: boolean
  showStaff: boolean = false
  read: any;
  write: any;
  constructor(private ls:LoginService,private toastr: ToastrService,private router: Router, private os: OrganizationService, private as: AttendanceService) {
    this.addBrchMpgObj = new BranchMapping()
    this.addusermapping = new UserMapping()
  }
  
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("schbrn") form: any;
  addBranchMapping() {
    this.as.addBranchmapping(this.addBrchMpgObj).subscribe((data) => {
      if(data.result)
      this.toastr.success(data.message)
      else
      this.toastr.error(data.message)
      this.getBranchMapping();
    })
  }
  skip() {
    this.router.navigate(['batch/Staff']);
  }
  Assigndevice() {
    this.as.addUserMappingmapping(this.addusermapping).subscribe((data) => {
      if(data.result)
      this.toastr.success(data.message)
      else
      this.toastr.error(data.message)
    })
    this.getUserMapping();
  }
  getdeviceusers() {
    this.as.getUserdeviceIdBydeviceid(this.getdevicelist[0].attendancelogid).subscribe((data) => {
      this.deviceuserlist = data.data
    });
  }
  addStaffUser(){
    this.router.navigate(['batch/Staff']);
  }
  skipStudent(){
    this.router.navigate(['organization/Admissions'])
  }
  getPermissions() {
    this.ls.checkRightPermissions('usermapping').subscribe((data)=>{
      this.read=data.data[0].read;
      this.write=data.data[0].write;
      this.canActivateChild(this.read)
    })
  }
  canActivateChild(x:any):boolean{
    if(x==true){
      return true;
    }else{
      this.router.navigate(['404']);
        return false;
    }
      }

  ngOnInit() {
    this.showbranch = false;
    this.as.getUserMapping().subscribe((data) => {
      this.list = data.data
      for (let i = 0; i < this.list.length; i++) {
      }
    });
    this.as.getDeviceId().subscribe((data) => {
      this.getdevicelist = data.data
    })
    this.getPermissions();
    this.getBranchMapping();
    this.getUserMapping();
    this.addBrchMpgObj.branchId=0
  }
  getUserMapping() {
    this.as.getusermappingdetails().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      
      this.dataSource = new MatTableDataSource(data.data)
      this.dataSource.sort = this.sort
      this.dataSource.paginator = this.paginator
      }
    })
  }
  applyFilter(filterValue: string) {
 
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataSource1.filter = filterValue.trim().toLowerCase();
  }
  getexcel(){
    this.as.exportAsExcelFile( this.dataSource.data, 'UserMapping');
}
  getBranchMapping() {
    this.as.getbranchmappingdetails().subscribe((data) => {
      if(data.result && data.data!='NDF')
      {
      this.dataSource1 = new MatTableDataSource(data.data)
      this.dataSource1.sort = this.sort
      this.dataSource1.paginator = this.paginator
      }
    })
  }
  getexcel1()
{
  this.as.exportAsExcelFile( this.dataSource1.data, 'BranchMapping');
}
  getIds() {
    this.showbranch = true
    this.showStaff = false
    this.os.getBranchById().subscribe((data) => {
      this.branchList = data.data;
    })
  }
  ShowStaff() {
    this.showStaff = true
    this.showbranch = false
    this.as.getDeviceId().subscribe((data) => {
      this.getdevicelist = data.data
    })
  }
}
