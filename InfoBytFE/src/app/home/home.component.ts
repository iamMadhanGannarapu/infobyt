import { Component, OnInit, ViewChild } from '@angular/core';
import { Enquiry } from '../modal/Login/Enquiry';
import { LoginService } from '../service/login.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '.././service/user.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  enquiry: Enquiry;
  List: any
  myUrl: string
  constructor(private toastr: ToastrService, private ls: LoginService, private user: UserService) {
    this.enquiry = new Enquiry()
    this.myUrl = ls.url;
  }
  @ViewChild("addEnquiryForm") form: any;
  ngOnInit() {
    var $carousel = $('.carousel'),
  currentSlide, nextSlide;

$('.next').click(function() {
currentSlide = $carousel.attr('data-slide');
nextSlide = +currentSlide === 4 ? 1 : +currentSlide + 1;
$carousel.attr('data-slide', nextSlide);
});

$('.prev').click(function() {
currentSlide = $carousel.attr('data-slide');
nextSlide = +currentSlide === 1 ? 4 : +currentSlide - 1;
$carousel.attr('data-slide', nextSlide);
});
    this.user.getAllTestimonial().subscribe((data) => {
      if(data.result && data.data!='NDF')
      this.List = data;
    })
    
  }
  addEnquiry() {
    if (this.enquiry.email.length > 10 && this.enquiry.message.length > 2) {
      this.ls.saveEnquiry(this.enquiry).subscribe((data) => {
        if (data.result) {
          this.toastr.success("Thanks for your enquiry")
          this.toastr.success("We will get in touch")
          this.form.reset();
        }
        else {
          this.toastr.error(data.message)
        }
      })
    } else {
      this.toastr.error("Please provide valid details")
    }
  }
  getImage(id: any) {
    this.user.getImage(id).subscribe((data) => {
    })
  }

}